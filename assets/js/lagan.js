$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * add_lagan_sign_Form validation
     */
    $('#add_lagan_sign_Form').validate({
        ignore: [],
        rules: {
            lagan_name: {
                required: true
            },
            life_stone_name: {
                required: true
            },
			 punya_stone_name: {
                required: true
            },
			 bhagya_stone_name: {
                required: true
            }

        },
        messages: {
            lagan_name: {
                required: "Lagan Sign Name is required"
            },
			life_stone_name: {
                required: "Life Birth Stone Name is required"
            },
			punya_stone_name: {
                required: "Punya Lucky Stone is required"
            },
            bhagya_stone_name: {
                required: "Bhagya Fortune Stone Name is required",
            }
        },
        submitHandler: function (form) {
            var lagan_name = $('#lagan_name').val();
            var life_stone_name = $('#life_stone_name').val();
            var punya_stone_name = $('#punya_stone_name').val();
            var bhagya_stone_name = $('#bhagya_stone_name').val();
          

            $.post(APP_URL + 'configure_access/add_new_lagan', {
                lagan_name: lagan_name,
                life_stone_name: life_stone_name,
                punya_stone_name: punya_stone_name,
                bhagya_stone_name: bhagya_stone_name,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_lagan_sign_form').empty();
                if (response.status == 200) {
                    $('#err_lagan_sign_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_lagan_sign_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#lagan_name').val('');
                $('#life_stone_name').val('');
                $('#punya_stone_name').val('');
                $('#bhagya_stone_name').val('');
           
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit lagan
     */

    $('body').on('click', '.edit_lagan', function () {
        $('#err_edit_lagan').empty();
        $('#edit_lagan_form label.error').empty();
        $('#edit_lagan_form input.error').removeClass('error');
        $('#edit_lagan_form select.error').removeClass('error');

        var edit_lagan_id = $(this).attr('name');
        var edit_lagan_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_life_stone_name = $(this).closest('tr').find('td:eq(2)').text();
        var edit_punya_stone_name = $(this).closest('tr').find('td:eq(3)').text();
        var edit_bhagya_stone_name = $(this).closest('tr').find('td:eq(4)').text();

        $('#edit_lagan_id').val(edit_lagan_id);
        $('#edit_lagan_name').val(edit_lagan_name).attr("selected", "selected");
        $('#edit_life_stone_name').val(edit_life_stone_name);
        $('#edit_punya_stone_name').val(edit_punya_stone_name);
        $('#edit_bhagya_stone_name').val(edit_bhagya_stone_name);
    });

    //------------------------------------------------------------------------
    /* 
     * edit_lagan_form validation
     */
    $('#edit_lagan_form').validate({
        rules: {
            edit_lagan_name: {
                required: true
            },
            edit_life_stone_name: {
                required: true
            },
			 edit_punya_stone_name: {
                required: true
            },
			 edit_bhagya_stone_name: {
                required: true
            }

        },
        messages: {
            edit_lagan_name: {
                required: "Lagan Sign Name is required"
            },
			edit_life_stone_name: {
                required: "Life Birth Stone Name is required"
            },
			edit_punya_stone_name: {
                required: "Punya Lucky Stone is required"
            },
            edit_bhagya_stone_name: {
                required: "Bhagya Fortune Stone Name is required",
            }
        },
        submitHandler: function (form) {
			
            var edit_lagan_id = $('#edit_lagan_id').val();
            var edit_lagan_name = $('#edit_lagan_name').val();
            var edit_life_stone_name = $('#edit_life_stone_name').val();
            var edit_punya_stone_name = $('#edit_punya_stone_name').val();
            var edit_bhagya_stone_name = $('#edit_bhagya_stone_name').val();

            $.post(APP_URL + 'configure_access/edit_lagan_information', {
				edit_lagan_id:edit_lagan_id,
                edit_lagan_name: edit_lagan_name,
                edit_life_stone_name: edit_life_stone_name,
                edit_punya_stone_name:edit_punya_stone_name,
                edit_bhagya_stone_name:edit_bhagya_stone_name,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_lagan').empty();
                if (response.status == 200) {
                    $('#err_edit_lagan').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
					
					$('.edit_lagan[name=' + edit_lagan_id + ']').closest("tr").find("td:eq(1)").text(edit_lagan_name);
					$('.edit_lagan[name=' + edit_lagan_id + ']').closest("tr").find("td:eq(2)").text(edit_life_stone_name);
					$('.edit_lagan[name=' + edit_lagan_id + ']').closest("tr").find("td:eq(3)").text(edit_punya_stone_name);
					$('.edit_lagan[name=' + edit_lagan_id + ']').closest("tr").find("td:eq(4)").text(edit_bhagya_stone_name);
                }
                else if (response.status == 201) {
                    $('#err_edit_lagan').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#edit_lagan_name').val('');
                $('#edit_life_stone_name').val('');
                $('#edit_punya_stone_name').val('');
                $('#edit_bhagya_stone_name').val('');
            }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove lagan from the list
     */

    $('body').on('click', '.remove_lagan', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var lagan_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_lagan', {edit_lagan_id: lagan_id}, function (response) {
            $('#err_edit_lagan').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_lagan').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_lagan[name=' + lagan_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_lagan').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})