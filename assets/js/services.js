$('document').ready(function () {
	
	$('body').on('change', '#services_name', function () {
		
		var servicesname = $('#services_name').val();
		var url =servicesname.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
		var newurl =url.toLowerCase();;
		
		$('#services_url').val(newurl);
	});

	$('body').on('change', '#premium_type', function () {
		
		var premium_type = $('#premium_type').val();
		if(premium_type == 'yes'){
			$('.single_married').removeClass('display_none')
		}else  {
			$('.single_married').addClass('display_none')
		}
	});


    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#services_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
	
	
//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image2').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
              //  $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage2').modal('hide');
                $('#services_detail_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage2').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
	
	 /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image3').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview3").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage3').modal('hide');
                $('#service_step_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage3').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#add_services_form').validate({
        ignore: [],
        rules: {
            services_name: {
                required: true
            },
			 col: {
                required: true
            },
            services_type: {
                required: true
            },
            services_main_description: {
                required: true
            },
			number_of_questions: {
                required: true
            },
			cost: {
                required: true
            },
			usdcost: {
                required: true
            },
			number_of_questions_2: {
                required: true
            },
			cost_2: {
                required: true
            },
			services_keyword_meta: {
                required: true
            },
			services_dec_meta: {
                required: true
            },
			services_title: {
                required: true
            },
			services_url: {
                required: true
            },
			faq_category: {
                required: true
            },

			
            services_description: {
                required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
            },
            services_image: {
                required: true
            },
			services_detail_image: {
                required: true
            },
			detail_image_position: {
                required: true
            },

        },
        messages: {
            services_name: {
                required: "Services Name is required"
            },
            services_type: {
                required: "Services Type is required"
            },
			col: {
                required: "Services Type is required"
            },
            faq_category: {
                required: "Faq Category is required"
            },
            services_main_description: {
                required: "Services Main Description is required"
            },
			cost: {
                required: "Cost is required"
            },
			usdcost: {
                required: "USD Cost is required"
            },
			
			number_of_questions: {
                required: "Number Of Questions is required"
            },
			number_of_questions_2: {
                required: "Number Of Questions is required"
            },
			cost_2: {
                required: "Cost is required"
            },
            services_description: {
                required: "Services Description is required"
            },
            services_image: {
                required: "Services Image is required"
            },
			services_detail_image: {
                required: "Detail Services Image is required"
            },
			detail_image_position: {
                required: "Detail  Image Position is required"
            },
			services_keyword_meta: {
                required: " Services keyword meta is required"
            },
			services_dec_meta: {
                required: " Services dec meta is required"
            },
			services_title: {
                required: " Services title is required"
            },
			services_url: {
                required: " Services url is required"
            },
        },
        submitHandler: function (form) {
            var is_question1 = $('#is_question1').val();
            var q1_label = $('#q1_label').val();
			var q1_placeholder = $('#q1_placeholder').val();
            var  is_question2= $('#is_question2').val();
            var q2_label = $('#q2_label').val();
			var q2_placeholder = $('#q2_placeholder').val();
            var services_name = $('#services_name').val();
            var faq_category = $('#faq_category').val();
            var services_type = $('#services_type').val();
            var services_main_description = $('#services_main_description').val();
            var consultant_type = $('#consultant_type').val();
            var number_of_questions = $('#number_of_questions').val();
			 var col = $('#col').val();
            var cost = $('#cost').val();
            var usdcost = $('#usdcost').val();
            var services_description = CKEDITOR.instances['services_description'].getData();
            var extra_description = CKEDITOR.instances['extra_description'].getData();
            //var service_detail_image = CKEDITOR.instances['service_detail_image'].getData();
            var services_image = $('#services_image').val();
            var services_detail_image = $('#services_detail_image').val();
            var detail_image_position = $('#detail_image_position').val();
            var service_step_image = $('#service_step_image').val();
			var service_id = $('#service_id').val();
			var cost_2 = $('#cost_2').val();
			var discounted_cost2 = $('#discounted_cost2').val();
			var discounted_cost = $('#discounted_cost').val();
			var discounted_usd_cost = $('#discounted_usd_cost').val();
			var number_of_questions_2 = $('#number_of_questions_2').val();
			var premium_type = $('#premium_type').val();
			var services_url = $('#services_url').val();
			var services_title = $('#services_title').val();
			var services_dec_meta = $('#services_dec_meta').val();
			var services_keyword_meta = $('#services_keyword_meta').val();
			var disc_logo_percent = $('#disc_logo_percent').val();
			var services_position = $('#services_position').val();
			var status = $('#status').val();
            var q_label = $('#q_label').val();
            var banner_id = $('#banner_id').val();
            var about_content_id = $('#about_content_id').val();
            var q_placeholder = $('#q_placeholder').val();
            var q_info  = $('#q_info').val();
            var single_married_label  = $('#single_married_label').val();
            var single_married_placeholder  = $('#single_married_placeholder').val();
        
			if(!discounted_cost2){
				var discounted_cost2 = 0 ;
			}
			if(!discounted_cost){
				var discounted_cost = 0 ;
			}
			
			if(!disc_logo_percent){
				var disc_logo_percent = 0 ;
			}
            
            var questions = [];
            $('.add_inputs').find('.row').each(function(){
                questions.push({
                    's_heading':$(this).find('.s_heading').val(),
                    'description':$(this).find('.description').val(),
                    'cost':$(this).find('.cost').val(),
                    'usdcost':$(this).find('.usdcost').val(),
                    'discounted_cost':$(this).find('.discounted_cost').val(),
                    'discounted_usd_cost':$(this).find('.discounted_usd_cost').val()
                });
            });
    
            console.log(questions);
            console.log(services_detail_image);
            
			
            $.post(APP_URL + 'configure_access/add_new_services', {
                services_url: services_url,
                services_title: services_title,
                services_dec_meta: services_dec_meta,
                services_keyword_meta: services_keyword_meta,
                services_name: services_name,
                services_type: services_type,
                consultant_type: consultant_type,
                number_of_questions: number_of_questions,
                extra_description: extra_description,
                cost: cost,
                faq_category: faq_category,
                about_content_id: about_content_id,
                banner_id: banner_id,
				col: col,
                services_main_description: services_main_description,
                services_description: services_description,
                services_image: services_image,
                services_detail_image: services_detail_image,
                detail_image_position: detail_image_position,
                service_step_image: service_step_image,
				service_id: service_id,
				premium_type: premium_type,
				discounted_cost: discounted_cost,
				discounted_usd_cost: discounted_usd_cost,
				disc_logo_percent: disc_logo_percent,
				services_position: services_position,
				status: status,
                usdcost: usdcost,
                questions:questions,
                is_question1:is_question1,
                q1_label:q1_label,
                q1_placeholder:q1_placeholder,
                is_question2: is_question2,
                q2_label:q2_label,
                q2_placeholder:q2_placeholder,
                q_label :q_label,
                q_placeholder : q_placeholder,
                q_info : q_info,
                single_married_label : single_married_label,
                single_married_placeholder : single_married_placeholder,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_services_form').empty();
						var mesg;
                        if (response.status == 200) {
							mesg = response.message;
							
							if(service_id != 0){
								mesg = 'services has been updated successfully';
							}
                            $('#err_services_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + mesg + "</strong></div>");
					 $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						alert(response.message);
						window.location.href= APP_URL+'configure_access/edit_services';
                        }
                        else if (response.status == 201) {
                            $('#err_services_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#services_name').val('');
						 $('#col').val('');
                         $('#discounted_usd_cost').val('');
                         $('#usdcost').val('');


                        $('#services_main_description').val('');
                        $('#services_description').val('');
                        $('#services_image').val('');
                        $('#services_detail_image').val('');
                        $('#detail_image_position').val('');
                        $('#service_step_image').val('');
                        $('#services_type').val('');
                        $('#consultant_type').val('');
                        $('#number_of_questions').val('');
                        $('#cost').val('');
                        $('#cost').val('');
                        $('#discounted_cost').val('');
                        $('#services_url').val('');
                        $('#services_title').val('');
                        $('#services_dec_meta').val('');
                        $('#services_keyword_meta').val('');
                        $('#is_question1').val('');
                        $('#q1_label').val('');
                        $('#q1_placeholder').val('');
                        $('#is_question2').val('');
                        $('#q2_label').val('');
                        $('#q2_placeholder').val('');
                        $('#q_label').val('');
                        $('#q_placeholder').val('');
                        $('#q_info').val('');
						$('.img-preview').attr('src');
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_services', function () {
        $('#err_edit_services').empty();
        $('#edit_services_form label.error').empty();
        $('#edit_services_form input.error').removeClass('error');
        $('#edit_services_form select.error').removeClass('error');

        var edit_services_id = $(this).attr('name');
        var edit_services_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_services_type = $(this).closest('tr').find('td:eq(2)').text();
        var edit_services_main_description = $(this).closest('tr').find('td:eq(3)').text();
        var edit_services_description = $(this).closest('tr').find('td:eq(4)').text();

        $('#edit_services_id').val(edit_services_id);
        $('#edit_services_name').val(edit_services_name);
		$('#edit_services_type').val(edit_services_type).attr("selected", "selected");
        $('#edit_services_main_description').val(edit_services_main_description);
        $('#edit_services_description').val(edit_services_description);

    });

    //------------------------------------------------------------------------
    /* 
     * This function is used to call for edit result information
     */
    $('#edit_services_form').validate({
        rules: {
            edit_services_name: {
                required: true
            },
			edit_services_type: {
                required: true
            },
			edit_services_main_description: {
                required: true
            },
			edit_services_description: {
                 required: true
            },
        },
        messages: {
            edit_services_name: {
                required: "Service Name is required"
            },
			edit_services_type: {
                required: "Service Type is required"
            },
			edit_services_main_description: {
                required: "Service Main Description is required"
            },
			edit_services_description: {
                required: "Service Description is required"
            },
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_services_id = $('#edit_services_id').val();
            var edit_services_name = $('#edit_services_name').val();
            var edit_services_type = $('#edit_services_type').val();
            var edit_services_main_description = $('#edit_services_main_description').val();
            var edit_services_description = $('#edit_services_description').val();

            $.post(APP_URL + 'configure_access/edit_services_information', {
                edit_services_id: edit_services_id,
                edit_services_name: edit_services_name,
                edit_services_type: edit_services_type,
                edit_services_main_description: edit_services_main_description,
                edit_services_description: edit_services_description,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_services').empty();
                if (response.status == 200) {
                    $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(1)").text(edit_services_name);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(2)").text(edit_services_type);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(3)").text(edit_services_main_description);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(4)").text(edit_services_description);

					 $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						alert(response.message);
						window.location.href= APP_URL+'configure_access/add_services';
					
					
                } else if (response.status == 201) {
                    $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
            return false;
        }
    });

    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */
    var edit_service_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#image_service_id').val($(this).attr('name'));
        edit_service_id_for_image = $(this).attr('name');
    });
	
	
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $('.editBrowseImageBtn[name=' + edit_service_id_for_image + ']').closest("tr").find(".service_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_services', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_service_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_services', {edit_service_id: edit_service_id}, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_services[name=' + edit_service_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

//---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_services1', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_service_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_bespoke_services', {edit_service_id: edit_service_id}, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_services[name=' + edit_service_id + ']').closest("tr").remove();
				window.location.href= APP_URL+'configure_access/edit_bespoke_services';
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;6
    });

	 //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form_bespoke validation
     */
    $('#add_services_form_bespoke').validate({
        ignore: [],
        rules: {
            services_name: {
                required: true
            },
            services_type: {
                required: true
            },
			 col: {
                required: true
            },
            services_main_description: {
                required: true
            },
			consultant_type: {
                required: true
            },
			
			premium_type: {
                required: true
            },
			services_keyword_meta: {
                required: true
            },
			services_dec_meta: {
                required: true
            },
			services_title: {
                required: true
            },
			services_url: {
                required: true
            },
			
			
            services_description: {
                required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
            },
            services_image: {
                required: true
            },
			service_detail_image: {
                required: true
            },
			service_step_image: {
                required: true
            },

        },
        messages: {
            services_name: {
                required: "Services Name is required"
            },
            services_type: {
                required: "Services Type is required"
            },
			col: {
                required: "Services Type is required"
            },
            services_main_description: {
                required: "Services Main Description is required"
            },
			
			consultant_type: {
                required: "Consultant Type is required"
            },
			
			premium_type: {
                required: "Premium Type is required"
            },
			
			
			
            services_description: {
                required: "Services Description is required"
            },
            services_image: {
                required: "Services Image is required"
            },
			service_detail_image: {
                required: "Detail Services Image is required"
            },
			service_step_image: {
                required: "Detail Services Image is required"
            },
			services_keyword_meta: {
                required: " Services keyword meta is required"
            },
			services_dec_meta: {
                required: " Services dec meta is required"
            },
			services_title: {
                required: " Services title is required"
            },
			services_url: {
                required: " Services url is required"
            },
        },
        submitHandler: function (form) {
            var services_name = $('#services_name').val();
			
			
			
			 var col = $('#col').val();
            var services_type = $('#services_type').val();
            var services_main_description = $('#services_main_description').val();
            var consultant_type = $('#consultant_type').val();                   
            var services_description = CKEDITOR.instances['services_description'].getData();
            var services_image = $('#services_image').val();
            var service_detail_image = $('#service_detail_image').val();
            var service_step_image = $('#service_step_image').val();
			var service_id = $('#service_id').val();						
			var premium_type = $('#premium_type').val();
				var services_url = $('#services_url').val();
			var services_title = $('#services_title').val();
			var services_dec_meta = $('#services_dec_meta').val();
			var services_keyword_meta = $('#services_keyword_meta').val();
			
            $.post(APP_URL + 'configure_access/add_new_bespoke_services', {
                services_url: services_url,
                services_title: services_title,
                services_dec_meta: services_dec_meta,
                services_keyword_meta: services_keyword_meta,
			  services_name: services_name,
                col: col,
                services_type: services_type,
                consultant_type: consultant_type,
                              
                services_main_description: services_main_description,
                services_description: services_description,
                services_image: services_image,
                service_detail_image: service_detail_image,
                service_step_image: service_step_image,
				service_id: service_id,
				premium_type: premium_type,
				
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_services_form').empty();
						var mesg;
                        if (response.status == 200) {
							mesg = response.message;
							
							if(service_id != 0){
								mesg = 'services has been updated successfully';
							}
                            $('#err_services_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + mesg + "</strong></div>");
					 $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						alert(response.message);
						//window.location.href= APP_URL+'configure_access/edit_services';
                        }
                        else if (response.status == 201) {
                            $('#err_services_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#services_name').val('');
                        $('#col').val('');
                        $('#services_main_description').val('');
                        $('#services_description').val('');
                        $('#services_image').val('');
                        $('#service_detail_image').val('');
                        $('#service_step_image').val('');
                        $('#services_type').val('');
                        $('#consultant_type').val('');
                      
                       
                        $('#services_url').val('');
                        $('#services_title').val('');
                        $('#services_dec_meta').val('');
                        $('#services_keyword_meta').val('');
						$('.img-preview').attr('src');
                    }, 'json');
            return false;
        }
    });


	//-----------------------------------------------------------------------
    /* 
     * admin count validation
     */
    $('#add_counter').validate({
        ignore: [],
        rules: {
            happy_customer: {
                required: true
            },
            birth_store: {
                required: true
            },
			 gemstone_jewellery: {
                required: true
            },
		},                        			
			 messages: {
            happy_customer: {
                required: "happy customer number is required"
            },
            birth_store: {
                required: "birth_store number required"
            },
			gemstone_jewellery: {
                required: "gemstone jewellery value required"
            }
           
					
			},			
        submitHandler: function (form) {
            var happy_customer = $('#happy_customer').val();
			 var birth_store = $('#birth_store').val();
            var gemstone_jewellery = $('#gemstone_jewellery').val();
          
			
            $.post(APP_URL + 'configure_access/add_counter', {
                happy_customer: happy_customer,
                birth_store: birth_store,
                gemstone_jewellery: gemstone_jewellery,
              			
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_services_form').empty();
						var mesg;
                        if (response.status == 200) {
							mesg = response.message;
							
							if(count_id != 0){
								mesg = 'counter has been updated successfully';
							}
                      $('#err_services_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + mesg + "</strong></div>");
					 $('#err_change_password_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						alert(response.message);
						window.location.href= APP_URL+'configure_access/edit_counter';
                        }
                        else if (response.status == 201) {
                            $('#err_services_form').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                        }
                        $('#happy_customer').val('');
                        $('#birth_store').val('');
                        $('#gemstone_jewellery').val('');
                        
                    }, 'json');
            return false;
        }
    });


	
})