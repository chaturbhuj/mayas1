$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * addMenuForm validation
     */
    $('#addSubMenuForm').validate({
        ignore: [],
        rules: {
            sub_menu_services_name: {
                required: true,
                charOnly: true
            },
            sub_menu_services_navi: {
                required: true
            }

        },
        messages: {
            sub_menu_services_name: {
                required: "Sub Menu Services Name is required",
                charOnly: "Only characters are allowed."
            },
            sub_menu_services_navi: {
                required: "Sub Menu Services Navigation URL is required",
            }
        },
        submitHandler: function (form) {
            var sub_menu_services_name = $('#sub_menu_services_name').val();
            var sub_menu_services_navi = $('#sub_menu_services_navi').val();

            $.post(APP_URL + 'configure_access/add_new_sub_menu_services', {
                sub_menu_services_name: sub_menu_services_name,
                sub_menu_services_navi: sub_menu_services_navi,
            },
            function (response) {
			alert('done');
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_sub_menu_form').empty();
                if (response.status == 200) {
                    $('#err_sub_menu_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_sub_menu_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#sub_menu_services_name').val('');
                $('#sub_menu_services_navi').val('');
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_sub_menu_services', function () {
        $('#err_edit_sub_menu_services').empty();
        $('#edit_sub_menu_services_form label.error').empty();
        $('#edit_sub_menu_services_form input.error').removeClass('error');
        $('#edit_sub_menu_services_form select.error').removeClass('error');

        var edit_sub_menu_services_id = $(this).attr('name');
        var edit_sub_menu_services_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_sub_menu_services_navi = $(this).closest('tr').find('td:eq(2)').text();

        $('#edit_sub_menu_services_id').val(edit_sub_menu_services_id);
        $('#edit_sub_menu_services_name').val(edit_sub_menu_services_name);
        $('#edit_sub_menu_services_navi').val(edit_sub_menu_services_navi);
    });

    //------------------------------------------------------------------------
    /* 
     * edit_sub_menu_services_form validation
     */
    $('#edit_sub_menu_services_form').validate({
        rules: {
            edit_sub_menu_services_name: {
                required: true,
                charOnly: true,
            },
            edit_sub_menu_services_navi: {
                required: true,               
            }
        },
        messages: {
            edit_sub_menu_services_name: {
                required: "Sub Menu Services name is required",
                charOnly: "Only characters are allowed.",
            },
            edit_sub_menu_services_navi: {
                required: "Sub Menu Services Navigation is required"
            }
        },
        submitHandler: function (form) {
            var edit_sub_menu_services_id = $('#edit_sub_menu_services_id').val();
            var edit_sub_menu_services_name = $('#edit_sub_menu_services_name').val();
            var edit_sub_menu_services_navi = $('#edit_sub_menu_services_navi').val();

            $.post(APP_URL + 'configure_access/edit_sub_menu_services_information', {
                edit_sub_menu_services_id: edit_sub_menu_services_id,
                edit_sub_menu_services_name: edit_sub_menu_services_name,
                edit_sub_menu_services_navi: edit_sub_menu_services_navi,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_edit_sub_menu_services').empty();
                        if (response.status == 200) {
                            $('#my_user_edit').modal('hide');
                            $('#edit_user_table').show();
                            $('#err_edit_sub_menu_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                            $('.edit_sub_menu_services[name=' + edit_sub_menu_services_id + ']').closest("tr").find("td:eq(1)").text(edit_sub_menu_services_name);
                            $('.edit_sub_menu_services[name=' + edit_sub_menu_services_id + ']').closest("tr").find("td:eq(2)").text(edit_sub_menu_services_navi);

                        } else if (response.status == 201) {
                            $('#err_edit_sub_menu_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_sub_menu_services', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_sub_menu_services_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_sub_menu_services', {edit_sub_menu_services_id: edit_sub_menu_services_id}, function (response) {
            $('#err_edit_sub_menu_services').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_sub_menu_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_sub_menu_services[name=' + edit_sub_menu_services_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_sub_menu_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})