$('document').ready(function () {

    //-----------------------------------------------------------------------
    /* 
     * admin_add_instance_form validation
     */
    $('#add_instance_form').validate({
        ignore: [],
        rules: {
            instance_price_inr: {
                required: true
            },
            instance_price_d: {
                required: true
            },
			instance_type_duration: {
                required: true
            },
			
            instance_detail:{
				 required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
			}
        },
        messages: {
            instance_price_inr: {
                required: "Price INR is required"
            },
            instance_price_d: {
                required: "Price $ is required"
            },
            instance_detail: {
                required: "Detail is required"
            },
			 instance_type_duration: {
                required: "Duration is required"
            },
			
        },
        submitHandler: function (form) {
            var instance_price_inr = $('#instance_price_inr').val();
            var instance_price_d = $('#instance_price_d').val();
            var instance_detail = $('#instance_detail').val();
            var instance_type = $('#instance_type').val();
            var instance_id = $('#instance_id').val();
            var instance_type_duration = $('#instance_type_duration').val();
			var instance_detail = CKEDITOR.instances['instance_detail'].getData();

            $.post(APP_URL + 'configure_access/add_new_instance_consultation', {
                instance_price_inr: instance_price_inr,
                instance_price_d: instance_price_d,
                instance_detail: instance_detail,
                instance_type: instance_type,
                instance_type_duration: instance_type_duration,
                instance_id: instance_id,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_instance_form').empty();
                        if (response.status == 200) {
                            $('#err_instance_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        else if (response.status == 201) {
                            $('#err_instance_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#instance_id').val(0);
                        $('#instance_price_inr').val('');
                        $('#instance_price_d').val('');
                        $('#instance_detail').val('');
                        $('#instance_type').val('');
                        $('#instance_type_duration').val('');
						
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit blog
     */

    $('body').on('click', '.edit_blog', function () {
        $('#err_edit_instance').empty();
        $('#edit_instance_form label.error').empty();
        $('#edit_instance_form input.error').removeClass('error');
        $('#edit_instance_form select.error').removeClass('error');

        var edit_instance_id = $(this).attr('name');
        var edit_instance_price_inr = $(this).closest('tr').find('td:eq(1)').text();
        var edit_instance_price_d = $(this).closest('tr').find('td:eq(2)').text();
        var edit_instance_detail = $(this).closest('tr').find('td:eq(3)').text();
        var edit_instance_type = $(this).closest('tr').find('td:eq(4)').text();
        var edit_instance_type_duration = $(this).closest('tr').find('td:eq(5)').text();

        $('#edit_instance_id').val(edit_instance_id);
        $('#edit_instance_price_inr').val(edit_instance_price_inr);
        $('#edit_instance_price_d').val(edit_instance_price_d);
        $('#edit_instance_detail').val(edit_instance_detail);
        $('#edit_instance_type').val(edit_instance_type);
        $('#edit_instance_type_duration').val(edit_instance_type_duration);

    });

    //------------------------------------------------------------------------
    /* 
     * This function is used to call for edit blog information
     */
    $('#edit_instance_form').validate({
        rules: {
            edit_instance_price_inr: {
                required: true
            },
			edit_instance_price_d: {
                required: true
            },
			edit_instance_detail: {
                required: true
            },
			edit_instance_type_duration: {
                required: true
            },
			
        },
        messages: {
            edit_instance_price_inr: {
                required: "Price is required"
            },
			edit_instance_price_d: {
                required: "Price is required"
            },
			edit_instance_detail: {
                required: "Detail is required"
            },
			edit_instance_type_duration: {
                required: "Detail is required"
            },
			
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_instance_id = $('#edit_instance_id').val();
            var edit_instance_price_inr = $('#edit_instance_price_inr').val();
            var edit_instance_price_d = $('#edit_instance_price_d').val();
            var edit_instance_detail = $('#edit_instance_detail').val();
            var edit_instance_type = $('#edit_instance_type').val();
            var edit_instance_type_duration = $('#edit_instance_type_duration').val();

            $.post(APP_URL + 'configure_access/edit_instance_information', {
                edit_instance_id: edit_instance_id,
                edit_instance_price_inr: edit_instance_price_inr,
                edit_instance_price_d: edit_instance_price_d,
                edit_instance_detail: edit_instance_detail,
                edit_instance_type: edit_instance_type,
                edit_instance_type_duration: edit_instance_type_duration,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_instance').empty();
                if (response.status == 200) {
                    $('#err_edit_instance').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_blog[name=' + edit_instance_id + ']').closest("tr").find("td:eq(1)").text(edit_instance_price_inr);
                    $('.edit_blog[name=' + edit_instance_id + ']').closest("tr").find("td:eq(2)").text(edit_instance_price_d);
                    $('.edit_blog[name=' + edit_instance_id + ']').closest("tr").find("td:eq(3)").text(edit_instance_detail);
                    $('.edit_blog[name=' + edit_instance_id + ']').closest("tr").find("td:eq(4)").text(edit_instance_type);
                    $('.edit_blog[name=' + edit_instance_id + ']').closest("tr").find("td:eq(5)").text(edit_instance_type_duration);

                } else if (response.status == 201) {
                    $('#err_edit_instance').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
            return false;
        }
    });

   
   
    //---------------------------------------------------------------------
    /*
     * This script is used to remove blog from the list
     */
		
    $('body').on('click', '.remove_instance_consultation', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_instance_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_instance_consultation', {edit_instance_id: edit_instance_id}, function (response) {
            $('#err_edit_instance').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_instance').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_instance_consultation[name=' + edit_instance_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_instance').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
	
	


})