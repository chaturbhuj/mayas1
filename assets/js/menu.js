$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * addMenuForm validation
     */
    $('#addMenuForm').validate({
        ignore: [],
        rules: {
            menu_name: {
                required: true,
                charOnly: true
            },
            menu_navi: {
                required: true
            }

        },
        messages: {
            menu_name: {
                required: "Menu Name is required",
                charOnly: "Only characters are allowed."
            },
            menu_navi: {
                required: "Menu Navigation URL is required",
            }
        },
        submitHandler: function (form) {
            var menu_name = $('#menu_name').val();
            var menu_navi = $('#menu_navi').val();

            $.post(APP_URL + 'configure_access/add_new_menu', {
                menu_name: menu_name,
                menu_navi: menu_navi,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_menu_form').empty();
                if (response.status == 200) {
                    $('#err_menu_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_menu_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#menu_name').val('');
                $('#menu_navi').val('');
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_menu', function () {
        $('#err_edit_menu').empty();
        $('#edit_menu_form label.error').empty();
        $('#edit_menu_form input.error').removeClass('error');
        $('#edit_menu_form select.error').removeClass('error');

        var edit_menu_id = $(this).attr('name');
        var edit_menu_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_menu_navi = $(this).closest('tr').find('td:eq(2)').text();

        $('#edit_menu_id').val(edit_menu_id);
        $('#edit_menu_name').val(edit_menu_name);
        $('#edit_menu_navi').val(edit_menu_navi);
    });

    //------------------------------------------------------------------------
    /* 
     * edit_course_form validation
     */
    $('#edit_menu_form').validate({
        rules: {
            edit_menu_name: {
                required: true,
                charOnly: true,
            },
            edit_menu_navi: {
                required: true,               
            }
        },
        messages: {
            edit_menu_name: {
                required: "Menu name is required",
                charOnly: "Only characters are allowed.",
            },
            edit_menu_navi: {
                required: "Menu Navigation is required"
            }
        },
        submitHandler: function (form) {
            var edit_menu_id = $('#edit_menu_id').val();
            var edit_menu_name = $('#edit_menu_name').val();
            var edit_menu_navi = $('#edit_menu_navi').val();

            $.post(APP_URL + 'configure_access/edit_menu_information', {
                edit_menu_id: edit_menu_id,
                edit_menu_name: edit_menu_name,
                edit_menu_navi: edit_menu_navi,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_edit_menu').empty();
                        if (response.status == 200) {
                            $('#my_user_edit').modal('hide');
                            $('#edit_user_table').show();
                            $('#err_edit_menu').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                            $('.edit_menu[name=' + edit_menu_id + ']').closest("tr").find("td:eq(1)").text(edit_menu_name);
                            $('.edit_menu[name=' + edit_menu_id + ']').closest("tr").find("td:eq(2)").text(edit_menu_navi);

                        } else if (response.status == 201) {
                            $('#err_edit_menu').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_menu', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_menu_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_menu', {edit_menu_id: edit_menu_id}, function (response) {
            $('#err_edit_menu').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_menu').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_menu[name=' + edit_menu_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_menu').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})