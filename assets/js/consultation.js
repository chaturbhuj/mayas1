 $('document').ready(function () {
	
	
	/* set active menu  */
		/*var segment3 = $(location).attr('href').split("welcome/consultation/")[1];
	console.log(segment3);
	$('.consultantType option').each(function(){
		
		if($(this).attr('name') == segment3){
			$(this).prop('selected',true);
		}
	});*/
	
	//-----------------------------------------------------------------------
    /* 
     * consultation_form validation
     */
	 $('#newFreeConsultantForm').validate({
		 rules: {
			/*consultantType: {
                required: true
            },*/
			inputName: {
                required: true
            },
			inputGender: {
                required: true
            },
			inputDateDate: {
                required: true
            },
			inputTimeMin: {
                required: true
            },
			
			
			birthCountry: {
                required: true
            },
			birthState: {
                required: true
            },
			birthCity: {
                required: true
            },
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                required: true,
				number: true,
				minlength:10,
				maxlength:12
            },
			/*educationQualification: {
                required: true
            },
			currentProfession: {
                required: true
            },
			partnerName: {
                required: true
            },
			partnerDateDate: {
                required: true
            },
			partnerTimeMin: {
                required: true
            },
			partnerBirthCountry: {
                required: true
            },
			partnerBirthState: {
                required: true
            },
			partnerBirthCity: {
                required: true
            },
			inputQuestion:{
                required: true				
			},*/
		 },
		 messages: {
			/*consultantType: {
                required: "Consultant Type is required"
            },*/
			 inputName: {
                required: "Full Name is required"
            },
			 inputGender: {
                required: "Gender is required"
            },
			 inputDateDate: {
                required: "Birth Date is required"
            },
			 inputTimeMin: {
                required: "Birth Time is required"
            },
			 birthCountry: {
                required: "Birth Country Name is required"
            },
			 birthState: {
                required: "Birth State Name is required"
            },
			 birthCity: {
                required: "Birth City Name is required"
            },
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },
			/*educationQualification: {
                required: "Highest Qualification is required"
            },
			currentProfession: {
                required: "Current Profession is required"
            },
			partnerName: {
                required: "Partner Name is required"
            },
			partnerDateDate: {
                required: "Partner Birth Day is required"
            },
			partnerTimeMin: {
                required: "Partner Birth Time is required"
            },
			partnerBirthCountry: {
                required: "Partner Birth Country is required"
            },
			partnerBirthState: {
                required: "Partner Birth State is required"
            },
			partnerBirthCity: {
                required: "Partner Birth City is required"
            },
			
			
			inputQuestion:{
                required: "Precise Question is required"				
			}*/
		},
		 submitHandler: function (form) {
			var consultantType = 'Free Instant Consultation';//$('#consultantType').val();
			//console.log(consultantType);
			/*if(consultantType=='Career'){
				var educationQualification = $('#educationQualification').val();
				var currentProfession = $('#currentProfession').val();
				var partnerName = '';
				var partnerDateDate = '';
				var partnerTimeMin = '';
				var partnerBirthCountry = '';
				var partnerBirthState = '';
				var partnerBirthCity = '';
			}else if(consultantType=='MatchMaking'){
				var educationQualification = '';
				var currentProfession = '';
				var partnerName = $('#partnerName').val();
				var partnerDateDate = $('#partnerDateDate').val();
				var partnerTimeMin = $('#partnerTimeMin').val();
				var partnerBirthCountry = $('#partnerBirthCountry').val();
				var partnerBirthState = $('#partnerBirthState').val();
				var partnerBirthCity = $('#partnerBirthCity').val();
			}else{
				var educationQualification = '';
				var currentProfession = '';
				var partnerName = '';
				var partnerDateDate = '';
				var partnerTimeMin = '';
				var partnerBirthCountry = '';
				var partnerBirthState = '';
				var partnerBirthCity = '';
			}*/
            var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
			var birthCity = $('#birthCity').val();
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            var paymentmethod = 'free';
            //var inputQuestion = $('#inputQuestion').val();
         //  var otherInfomation = $('#otherInfomation').val();
			
			
			
			$.post(APP_URL + 'welcome/add_new_free_consultant', {
				consultantType: consultantType,
				inputName: inputName,
                inputGender: inputGender,
                inputDateDate: inputDateDate,
                inputTimeMin: inputTimeMin,
                birthCountry: birthCountry,
				birthState: birthState,
				birthCity: birthCity,
				inputEmail: inputEmail,
				inputContact: inputContact,
				paymentmethod: paymentmethod,
				
				/*educationQualification: educationQualification,
                currentProfession: currentProfession,
                partnerName: partnerName,
                partnerDateDate: partnerDateDate,
				partnerTimeMin: partnerTimeMin,
                partnerBirthCountry: partnerBirthCountry,
				partnerBirthState: partnerBirthState,
				partnerBirthCity: partnerBirthCity,
				
				paymentmethod: paymentmethod,
                inputQuestion: inputQuestion,
                otherInfomation: otherInfomation,*/
             },
			 function (response) {
				 $("html, body").animate({scrollTop: 0}, "slow");
                 $('#err_consultation_div').empty();
				 if (response.status == 200) {
					$('#err_consultation_div').html("<div class='alert alert-success fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				//if(paymentmethod =='free'){	
					
					
					$('#consultantType').val('');
					$('#inputName').val('');
					$('#inputGender').val('');
					$('#inputDateDate').val('');
					$('#inputTimeMin').val('');
					$('#birthCountry').val('');
					$('#birthState').val('');
					$('#birthCity').val('');
					//$('#inputEmail').val('');
					//$('#inputEmailCrfm').val('');
					$('#inputContact').val('');
					$('#paymentmethod').val('');
					
					//$('#inputQuestion').val('');
					//$('#otherInfomation').val('');
					
					/*$('#educationQualification').val('');
					$('#currentProfession').val('');
					$('#partnerName').val('');
					$('#partnerDateDate').val('');
					$('#partnerTimeMin').val('');
					$('#partnerBirthCountry').val('');
					$('#partnerBirthState').val('');
					$('#partnerBirthCity').val('');*/
				//}else{
					/** go to payment page*/
					
					
				/*}					
					*/
				setTimeout(function() {
						window.location.href = APP_URL+'welcome/free_consultation_thankyou/'+ response.data +'';	
				}, 500);
					
				}else if (response.status == 201) {
					$('#err_consultation_div').html("<div class='alert alert-danger fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				}
				 
			},'json');
			return false;
		 },
	 });
})