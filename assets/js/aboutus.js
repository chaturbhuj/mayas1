$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * add_aboutus_form validation
     */
    $('#add_aboutus_form').validate({
        ignore: [],
        rules: {
            aboutus1: {
                required: true
            },
            aboutus2: {
                required:  true
            },
            aboutus3: {
                required: true
            },
            aboutus4: {
                required: true
            }
        },
        messages: {
            aboutus1: {
                required: "About us 1 is required",
            },
			aboutus2: {
				 required:  "About us 2 is required",
			},
			aboutus3: {
                required: "About us 3 is required",
            },
			aboutus4: {
                required: "About us 4 is required",
            }
        },
        submitHandler: function (form) {
            var aboutus1 = $('#aboutus1').val();
            var aboutus2 = $('#aboutus2').val();
            var aboutus3 = $('#aboutus3').val();
            var aboutus4 = $('#aboutus4').val();
            var aboutus_id = $('#aboutus_id').val();
            

            $.post(APP_URL + 'configure_access/add_new_aboutus', {
                aboutus1: aboutus1,
                aboutus2: aboutus2,
                aboutus3: aboutus3,
                aboutus4: aboutus4,
                aboutus_id: aboutus_id,
             },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_aboutus_form').empty();
                if (response.status == 200) {
                    $('#err_aboutus_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_aboutus_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#aboutus1').val('');
                $('#aboutus2').val('');
                $('#aboutus3').val('');
                $('#aboutus4').val('');
             
            }, 'json');
            return false;
        }
    });

    //------------------------------------------------------------------------
    /* 
     * edit_aboutus_form validation
     */
     $('#edit_aboutus_form').validate({
        ignore: [],
        rules: {
            aboutus1: {
                required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
            },
       
            aboutus2: {
                required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
            },
       
        },
        messages: {
            aboutus1: {
                required: "About us 1 is required",
            },
			aboutus2: {
                required: "About us 2 is required",
            },
			
        },
        submitHandler: function (form) {
           
            var aboutus1 = CKEDITOR.instances['aboutus1'].getData();
            var aboutus2 = CKEDITOR.instances['aboutus2'].getData();
     
			var aboutus_image1 = $('#blog_image1').val();
			var aboutus_image2 = $('#blog_image2').val();
			
            $.post(APP_URL + 'configure_access/edit_aboutus_information', {
                aboutus1: aboutus1,
                aboutus2: aboutus2,
                
                aboutus_image1: aboutus_image1,
                aboutus_image2: aboutus_image2,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_aboutus').empty();
                if (response.status == 200) {
                    $('#err_aboutus').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_aboutus').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#aboutus1').val('');
                $('#aboutus2').val('');
               
            }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_user', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_user_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_user_information', {edit_user_id: edit_user_id}, function (response) {
            $('#err_admin_edit_user').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#edit_user_table').show();
                $('#err_admin_edit_user').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_user[name=' + edit_user_id + ']').closest("tr").remove();
            }
            else {
                $('#err_admin_edit_user').empty();
                $('#err_admin_edit_user').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
	
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#blog_image1').val(response.filename);
                $('#blog_image1-error').css({"display": "none"});
				
				
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

	



//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image2').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage2').modal('hide');
                $('#blog_image2').val(response.filename);
                $('#blog_image2-error').css({"display": "none"});
				
				
            } else {
                $('#browseImage2').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

	
})





