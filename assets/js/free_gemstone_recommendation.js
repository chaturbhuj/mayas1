$('document').ready(function () {
	//-----------------------------------------------------------------------
    /* 
     * consultation_form validation
     */
	 $('#newfree_gemstone_recommendationForm').validate({
		 rules: {
			inputName: {
                required: true
            },
			inputGender: {
                required: true
            },
			inputDateDate: {
                required: true
            },
			inputDateMonth: {
                required: true
            },
			inputDateYear: {
                required: true
            },
			inputTimeHr: {
                required: true
            },
			inputTimeMin: {
                required: true
            },
			inputTimeSec: {
                required: true
            },
			birthCountry: {
                required: true
            },
			birthState: {
                required: true
            },
			birthCity: {
                required: true
            },
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                //required: true,
				number: true,
				minlength:10,
				maxlength:12
            },
		 },
		 messages: {
			 inputName: {
                required: "Full Name is required"
            },
			 inputGender: {
                required: "Gender is required"
            },
			 inputDateDate: {
                required: "Day of Birth is required"
            },
			 inputDateMonth: {
                required: "Month of Birth is required"
            },
			 inputDateYear: {
                required: "Year of birth is required"
            },
			 inputTimeHr: {
                required: "Birth Hour is required"
            },
			 inputTimeMin: {
                required: "Birth Min is required"
            },
			 inputTimeSec: {
                required: "Birth Sec is required"
            },
			 birthCountry: {
                required: "Birth Country Name is required"
            },
			 birthState: {
                required: "Birth State Name is required"
            },
			 birthCity: {
                required: "Birth City Name is required"
            },
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                //required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },
         },
		 submitHandler: function (form) {
			var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val();
            var inputDateMonth = $('#inputDateMonth').val();
            var inputDateYear = $('#inputDateYear').val();
            var inputTimeHr = $('#inputTimeHr').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var inputTimeSec = $('#inputTimeSec').val();
			var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
			var birthCity = $('#birthCity').val();
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
			
			$.post(APP_URL + 'welcome/add_new_free_gemstone_recommendation', {
				inputName: inputName,
                inputGender: inputGender,
                inputDateDate: inputDateDate,
                inputDateMonth: inputDateMonth,
                inputDateYear: inputDateYear,
                inputTimeHr: inputTimeHr,
                inputTimeMin: inputTimeMin,
                inputTimeSec: inputTimeSec,
				birthCountry: birthCountry,
				birthState: birthState,
				birthCity: birthCity,
				inputEmail: inputEmail,
				inputContact: inputContact,
				
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_free_gemstone_recommendation_div').empty();
				if (response.status == 200) {
					$('#err_free_gemstone_recommendation_div').html("<div class='alert alert-success fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
					$('#inputName').val('');
					$('#inputGender').val('');
					$('#inputDateDate').val('');
					$('#inputDateMonth').val('');
					$('#inputDateYear').val('');
					$('#inputTimeHr').val('');
					$('#inputTimeMin').val('');
					$('#inputTimeSec').val('');
					$('#birthCountry').val('');
					$('#birthState').val('');
					$('#birthCity').val('');
					$('#inputEmail').val('');
					$('#inputEmailCrfm').val('');
					$('#inputContact').val('');
				}else if (response.status == 201) {
					$('#err_free_gemstone_recommendation_div').html("<div class='alert alert-danger fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				}
				 
			},'json');
			return false;
			
          },
	 });
})