$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * addSpecialFeatureForm validation
     */
    $('#addSpecialFeatureForm').validate({
        ignore: [],
        rules: {
            special_feature_heading: {
                required: true
            },
            sub_heading: {
                required: true
            }

        },
        messages: {
            special_feature_heading: {
                required: "Special Feature Heading is required"
            },
            sub_heading: {
                required: "Sub Heading URL is required",
            }
        },
        submitHandler: function (form) {
            var special_feature_heading = $('#special_feature_heading').val();
            var sub_heading = $('#sub_heading').val();
            var small_sub_heading_message = $('#small_sub_heading_message').val();
            var point1 = $('#point1').val();
            var point2 = $('#point2').val();
            var point3 = $('#point3').val();
            var point4 = $('#point4').val();
            var point5 = $('#point5').val();            
            var point6 = $('#point6').val();
			var point7 = $('#point7').val();

            $.post(APP_URL + 'configure_access/add_new_special_feature', {
                special_feature_heading: special_feature_heading,
                sub_heading: sub_heading,
                small_sub_heading_message: small_sub_heading_message,
                point1: point1,
                point2: point2,
                point3: point3,
                point4: point4,
                point5: point5,
                point6: point6,
                point7: point7,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_special_feature_form').empty();
                if (response.status == 200) {
                    $('#err_special_feature_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_special_feature_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#special_feature_heading').val('');
                $('#sub_heading').val('');
                $('#small_sub_heading_message').val('');
                $('#point1').val('');
                $('#point2').val('');
                $('#point3').val('');
                $('#point4').val('');
                $('#point5').val('');
                $('#point6').val('');
                $('#point7').val('');
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_special_feature', function () {
        $('#err_edit_special_feature').empty();
        $('#edit_special_feature_form label.error').empty();
        $('#edit_special_feature_form input.error').removeClass('error');
        $('#edit_special_feature_form select.error').removeClass('error');

        var special_feature_id = $(this).attr('name');
        var special_feature_heading = $(this).closest('tr').find('td:eq(1)').text();
        var sub_heading = $(this).closest('tr').find('td:eq(2)').text();
        var small_sub_heading_message = $(this).closest('tr').find('td:eq(3)').text();
        var point1 = $(this).closest('tr').find('td:eq(4)').text();
        var point2 = $(this).closest('tr').find('td:eq(5)').text();
        var point3 = $(this).closest('tr').find('td:eq(6)').text();
        var point4 = $(this).closest('tr').find('td:eq(7)').text();
        var point5 = $(this).closest('tr').find('td:eq(8)').text();
        var point6 = $(this).closest('tr').find('td:eq(9)').text();
        var point7 = $(this).closest('tr').find('td:eq(10)').text();

        $('#special_feature_id').val(special_feature_id);
        $('#special_feature_heading').val(special_feature_heading);
        $('#sub_heading').val(sub_heading);
        $('#small_sub_heading_message').val(small_sub_heading_message);
        $('#point1').val(point1);
        $('#point2').val(point2);
        $('#point3').val(point3);
        $('#point4').val(point4);
        $('#point5').val(point5);
        $('#point6').val(point6);
        $('#point7').val(point7);
    });

    //------------------------------------------------------------------------
    /* 
     * edit_course_form validation
     */
    $('#edit_special_feature_form').validate({
        rules: {
            special_feature_heading: {
                required: true
            },
            sub_heading: {
                required: true
            }

        },
        messages: {
            special_feature_heading: {
                required: "Special Feature Heading is required"
            },
            sub_heading: {
                required: "Sub Heading URL is required",
            }
        },
        submitHandler: function (form) {
            var special_feature_id = $('#special_feature_id').val();
            var special_feature_heading = $('#special_feature_heading').val();
            var sub_heading = $('#sub_heading').val();
            var small_sub_heading_message = $('#small_sub_heading_message').val();
            var point1 = $('#point1').val();
            var point2 = $('#point2').val();
            var point3 = $('#point3').val();
            var point4 = $('#point4').val();
            var point5 = $('#point5').val();            
            var point6 = $('#point6').val();
			var point7 = $('#point7').val();

            $.post(APP_URL + 'configure_access/edit_special_feature_information', {
				special_feature_id:special_feature_id,
                special_feature_heading: special_feature_heading,
                sub_heading: sub_heading,
                small_sub_heading_message: small_sub_heading_message,
                point1: point1,
                point2: point2,
                point3: point3,
                point4: point4,
                point5: point5,
                point6: point6,
                point7: point7,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_special_feature').empty();
                if (response.status == 200) {
                    $('#err_edit_special_feature').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
					
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(1)").text(special_feature_heading);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(2)").text(sub_heading);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(3)").text(small_sub_heading_message);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(4)").text(point1);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(5)").text(point2);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(6)").text(point3);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(7)").text(point4);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(8)").text(point5);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(9)").text(point6);
					$('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").find("td:eq(10)").text(point7);
                }
                else if (response.status == 201) {
                    $('#err_edit_special_feature').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#special_feature_heading').val('');
                $('#sub_heading').val('');
                $('#small_sub_heading_message').val('');
                $('#point1').val('');
                $('#point2').val('');
                $('#point3').val('');
                $('#point4').val('');
                $('#point5').val('');
                $('#point6').val('');
                $('#point7').val('');
            }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_special_feature', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var special_feature_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_special_feature', {special_feature_id: special_feature_id}, function (response) {
            $('#err_edit_special_feature').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_special_feature').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_special_feature[name=' + special_feature_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_special_feature').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})