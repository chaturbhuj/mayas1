$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#admin_add_new_user_form').validate({
        ignore: [],
        rules: {
            user_email: {
                required: true,
                remote: {
                    url: APP_URL + "configure_access/check_availability",
                    type: "post",
                    data: {
                        param: 'users'
                    }
                }
            },
            user_name: {
                required: true,
                charOnly: true
            },
            user_pass: {
                required: true,
                minlength: 6
            },
            assign_role_to_user: {
                required: true
            }

        },
        messages: {
            user_email: {
                required: "Email is required",
                remote: "Email already exits"
            },
            user_name: {
                required: "Username is required",
                charOnly: "Only characters are allowed."
            },
            user_pass: {
                required: "Password is required",
                minlength: "Atleast 6 character Required"
            },
            assign_role_to_user: {
                required: "User role is required"
            }
        },
        submitHandler: function (form) {
            var user_email = $('#user_email').val();
            var user_name = $('#user_name').val();
            var user_pass = $('#user_pass').val();
            var assign_role_to_user = $('#assign_role_to_user').val();

            $.post(APP_URL + 'configure_access/add_new_user', {
                user_email: user_email,
                user_name: user_name,
                user_pass: user_pass,
                assign_role_to_user: assign_role_to_user
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_admin_add_new_user_form').empty();
                if (response.status == 200) {
                    $('#err_admin_add_new_user_form').empty();
                    $('#err_admin_add_new_user_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_admin_add_new_user_form').empty();
                    $('#err_admin_add_new_user_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#user_email').val('');
                $('#user_name').val('');
                $('#user_pass').val('');
                $('#assign_role_to_user').val('');
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_user', function () {
        $('#err_admin_edit_user').empty();
        $('#edit_user_form label.error').empty();
        $('#edit_user_form input.error').removeClass('error');
        $('#edit_user_form select.error').removeClass('error');

        var user_id = $(this).attr('name');
        var user_name = $(this).closest('tr').find('td:eq(1)').text();
        var user_email = $(this).closest('tr').find('td:eq(2)').text();
        var edit_assign_role_to_user = $(this).closest('tr').find('td:eq(3)').text();

        $('#edit_user_id').val(user_id);
        $('#edit_user_name').val(user_name);
        $('#edit_user_email').val(user_email);
        $('#edit_assign_role_to_user').val(edit_assign_role_to_user);

        $('#err_edit_user_name').empty();
        $('#err_edit_user_name').empty();
    });

    //------------------------------------------------------------------------
    /* 
     * edit_course_form validation
     */
    $('#edit_user_form').validate({
        rules: {
            edit_user_name: {
                required: true,
                charOnly: true,
            },
            edit_user_email: {
                required: true,               
            },
            edit_assign_role_to_user: {
                required: true
            }
        },
        messages: {
            edit_user_name: {
                required: "User name is required",
                charOnly: "Only characters are allowed.",
            },
            edit_user_email: {
                required: "User email is required"
            },
            edit_assign_role_to_user: {
                required: "User role is required"
            }
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_user_id = $('#edit_user_id').val();
            var edit_user_name = $('#edit_user_name').val();
            var edit_user_email = $('#edit_user_email').val();
            var edit_assign_role_to_user = $('#edit_assign_role_to_user').val();

            $.post(APP_URL + 'configure_access/edit_user_information', {
                edit_user_id: edit_user_id,
                edit_user_name: edit_user_name,
                edit_user_email: edit_user_email,
                edit_assign_role_to_user: edit_assign_role_to_user,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_admin_edit_user').empty();
                        $('#err_edit_user_name').empty();
                        $("html, body").animate({scrollTop: 0}, "slow");
                        if (response.status == 200) {
                            $('#my_user_edit').modal('hide');
                            $('#edit_user_table').show();
                            $('#err_admin_edit_user').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                            $('.edit_user[name=' + edit_user_id + ']').closest("tr").find("td:eq(1)").text(edit_user_name);
                            $('.edit_user[name=' + edit_user_id + ']').closest("tr").find("td:eq(2)").text(edit_user_email);
                            $('.edit_user[name=' + edit_user_id + ']').closest("tr").find("td:eq(3)").text(edit_assign_role_to_user);

                        } else if (response.status == 201) {
                            $('#err_admin_edit_user').empty();
                            $('#err_admin_edit_user').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_user', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_user_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_user_information', {edit_user_id: edit_user_id}, function (response) {
            $('#err_admin_edit_user').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#edit_user_table').show();
                $('#err_admin_edit_user').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_user[name=' + edit_user_id + ']').closest("tr").remove();
            }
            else {
                $('#err_admin_edit_user').empty();
                $('#err_admin_edit_user').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})