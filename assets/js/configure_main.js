$('document').ready(function(){
	
	/*Click event for hide social event */
	if ($(window).width() < 400) {
		$('.social_area').addClass('display_none');
	}
	$('body').on('click', '.social_close',function(){
		$('.social_area').addClass('display_none');
	});
	
	
	/**----------------------------------------------------------------
	  * Show model on open website
	  */

    if ($(window).width() > 400) {
        window.setTimeout(function () {
            $('#upcomingCourse').modal('show');
        }, 1000);/*100 seconds */
    }else{
        console.log($(window).width(),'noe');
    }

	//---------------------------------------------------------------------
    /*
     * This script is used for logout user
     */
	$('body').on('click','.logout',function() {
		$.get(APP_URL+'configure_access/logout',{},function(response) {
			window.location.href = APP_URL+'login';
		});			
	});

})