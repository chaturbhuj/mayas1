$('document').ready(function () {
//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage').modal('hide');
                $('#sub_menu_products_image').val(response.filename);
                $('#sub_menu_products_image-error').css({"display": "none"});
            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * addMenuForm validation
     */
    $('#addSubMenuProductsForm').validate({
        ignore: [],
        rules: {
            sub_menu_products_name: {
                required: true,
                charOnly: true
            },
            sub_menu_products_navi: {
                required: true
            }

        },
        messages: {
            sub_menu_products_name: {
                required: "Sub Menu Products Name is required",
                charOnly: "Only characters are allowed."
            },
            sub_menu_products_navi: {
                required: "Sub Menu Services Navigation URL is required",
            }
        },
        submitHandler: function (form) {
            var sub_menu_products_name = $('#sub_menu_products_name').val();
            var sub_menu_products_navi = $('#sub_menu_products_navi').val();
			var sub_menu_products_image = $('#sub_menu_products_image').val();

            $.post(APP_URL + 'configure_access/add_new_sub_menu_products', {
                sub_menu_products_name: sub_menu_products_name,
                sub_menu_products_navi: sub_menu_products_navi,
				sub_menu_products_image: sub_menu_products_image,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_sub_menu_products_form').empty();
                if (response.status == 200) {
                    $('#err_sub_menu_products_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_sub_menu_products_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#sub_menu_products_name').val('');
                $('#sub_menu_products_navi').val('');
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit products
     */

    $('body').on('click', '.edit_sub_menu_products', function () {
        $('#err_edit_sub_menu_products').empty();
        $('#edit_sub_menu_products_form label.error').empty();
        $('#edit_sub_menu_products_form input.error').removeClass('error');
        $('#edit_sub_menu_products_form select.error').removeClass('error');

        var edit_sub_menu_products_id = $(this).attr('name');
        var edit_sub_menu_products_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_sub_menu_products_navi = $(this).closest('tr').find('td:eq(2)').text();

        $('#edit_sub_menu_products_id').val(edit_sub_menu_products_id);
        $('#edit_sub_menu_products_name').val(edit_sub_menu_products_name);
        $('#edit_sub_menu_products_navi').val(edit_sub_menu_products_navi);
    });
//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */
    var edit_product_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#image_product_id').val($(this).attr('name'));
        edit_product_id_for_image = $(this).attr('name');
    });
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $('.editBrowseImageBtn[name=' + edit_product_id_for_image + ']').closest("tr").find(".product_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
    //------------------------------------------------------------------------
    /* 
     * edit_sub_menu_products form validation
     */
    $('#edit_sub_menu_products_form').validate({
        rules: {
            edit_sub_menu_products_name: {
                required: true,
                charOnly: true,
            },
            edit_sub_menu_products_navi: {
                required: true,               
            }
        },
        messages: {
            edit_sub_menu_products_name: {
                required: "Sub Menu Products name is required",
                charOnly: "Only characters are allowed.",
            },
            edit_sub_menu_products_navi: {
                required: "Sub Menu Products Navigation is required"
            }
        },
        submitHandler: function (form) {
            var edit_sub_menu_products_id = $('#edit_sub_menu_products_id').val();
            var edit_sub_menu_products_name = $('#edit_sub_menu_products_name').val();
            var edit_sub_menu_products_navi = $('#edit_sub_menu_products_navi').val();

            $.post(APP_URL + 'configure_access/edit_sub_menu_products_information', {
                edit_sub_menu_products_id: edit_sub_menu_products_id,
                edit_sub_menu_products_name: edit_sub_menu_products_name,
                edit_sub_menu_products_navi: edit_sub_menu_products_navi,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_edit_sub_menu_products').empty();
                            $('#my_user_edit').modal('hide');
                        if (response.status == 200) {
                            $('#err_edit_sub_menu_products').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                            $('.edit_sub_menu_products[name=' + edit_sub_menu_products_id + ']').closest("tr").find("td:eq(1)").text(edit_sub_menu_products_name);
                            $('.edit_sub_menu_products[name=' + edit_sub_menu_products_id + ']').closest("tr").find("td:eq(2)").text(edit_sub_menu_products_navi);

                        } else if (response.status == 201) {
                            $('#err_edit_sub_menu_products').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_sub_menu_products', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_sub_menu_products_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_sub_menu_products', {edit_sub_menu_products_id: edit_sub_menu_products_id}, function (response) {
            $('#err_edit_sub_menu_products').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_sub_menu_products').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_sub_menu_products[name=' + edit_sub_menu_products_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_sub_menu_products').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})