$('document').ready(function () {

    //-----------------------------------------------------------------------
    /* 
     * add_student_form validation
     */
    $('#add_student_form').validate({
        ignore: [],
        rules: {
            student_name: {
                required: true,
            },
        },
        messages: {
            student_name: {
                required: "Student Name is required",
            },
        },
        submitHandler: function (form) {
            var student_name = $('#student_name').val(); /*input*/
            var student_id = $('#student_id').val(); /*input*/
           /* var student_name = $('#student_name').attr('class'); read attr of input*/
           /* var student_name = $('#student_name').attr('class','value'); reset attr of input*/
           /* var student_name = $('#student_name').text(); p */
          

            $.post(APP_URL + 'configure_access/add_new_student', {
                student_name: student_name,
                student_id: student_id
            },
            function (response) {
				
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_student_form').empty();
                if (response.status == 200) {
                    $('#err_student_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message1 + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_student_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#student_name').val();  /*read*/         
                $('#student_name').val('');    /*set*/        
            }, 'json');
            return false;
        }
    });



})