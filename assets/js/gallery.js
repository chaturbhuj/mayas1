$('document').ready(function(){
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
    */
	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#status');
	$('#upload_image').ajaxForm({
		dataType: 'JSON',
		beforeSend: function() {
			console.log('1');
			$('#status').empty();
			var percentVal = '0%';
			bar.width(percentVal);
			percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			
			console.log('2');
			var percentVal = percentComplete + '%';
			bar.width(percentVal);
			percent.html(percentVal);
		},
		complete: function(response) {
			console.log('2');
			var status= (response.responseText).split('status":')[1].split(',"')[0];
			var filename_ = (response.responseText).split('"filename":"')[1].split('"}')[0];
			if(status == '200'){
				$('.image-preview').find('img').removeClass('display_none');
				$('.image-preview').find('img').attr('src',APP_URL+'uploads/'+filename_);
				$('#image').val(filename_);
				$('#image-error').css("display","none");
				$('#browseImage').modal('hide');
			}else{
                $('#browseImage').modal('hide');
                alert(response.message);
			}
			$('.progress-bar').css({'width':'0%'});
		
			$('.percent').html('0%');
			return false;
		}	   
	});
	
	//-----------------------------------------------------------------------
    /* 
     * validation of add_image_form
     */
	$('#add_image_form').validate({
		ignore: [],
        rules: {
            image: {
                required: true,
            },
        },
		 messages: {
			image: {
                required: "Image is required.",
            },
		},
		submitHandler: function (form) {
			
			$('#add_image_form').find('button[type="submit"]').prop('disabled',true);
			var img_name = $('#img_name').val();
			
            var image = $('#image').val();
			
            $.post(APP_URL + 'configure_access/add_blog_image', {
                img_name: img_name,
                
                image: image,
            },
			function (response) {
				$("html, body").animate({scrollTop: $('#headerMsg').offset().top}, "slow");
                $('#headerMsg3').empty();
                $('.err_msg').empty();
				if (response.status == 200) {
                    var message = response.message;
					
					$('#headerMsg3').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
					
					
					
					
					
					setTimeout(function() {
						window.location.href = APP_URL+'configure_access/blog_image/';	
				}, 500);
                }
                else if (response.status == 201) {
                    $('#headerMsg3').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
				$("#img_name").val('');
				$("#image").val('');
				
				$("#image-preview").find('img').addClass('display_none');
				$("#image-preview").find('img').attr('src','');
				
				
				$('#add_image_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
	
	
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
	var bar = $('.bar');
	var percent = $('.percent');
	var status = $('#status');
	$('#edit_upload_image').ajaxForm({
		dataType: 'JSON',
		beforeSend: function() {
			$('#status').empty();
			var percentVal = '0%';
			bar.width(percentVal);
			percent.html(percentVal);
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			bar.width(percentVal);
			percent.html(percentVal);
		},
		complete: function(response) {
			var status= (response.responseText).split('status":"')[1].split('","')[0];
			if(status == '200'){
                var img_id  = $('#img_id3').val();
				console.log(img_id);
				$('.edit_image_media[name="'+img_id+'"]').find(".img_pre").attr('src', APP_URL + 'uploads/miscellaneous/' + (response.responseText).split('filename":"')[1].split('"}')[0]);
                $('#browseEditImage').modal('hide');
            }else{
                $('#browseEditImage').modal('hide');
                alert(response.message);
			}
			$('#myFile').val('');
			$('#img_id3').val(0);
			$('.progress-bar').css({'width':'0%'});
			
			$('.percent').html('0%');
			return false;
		}	   
	});
	

	//---------------------------------------------------------------------
    /**
     * This script is used to remove ads from the list
     */
	$('body').on('click', '.remove_image', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
		
        var img_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'configure_access/remove_image', {img_id: img_id}, function (response) {
            $('#headerMsg').empty();
            $('.err_msg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: $('#headerMsg').offset().top}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_image[name=' + img_id + ']').closest("tr").remove();
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
		
        return false;
    });

});