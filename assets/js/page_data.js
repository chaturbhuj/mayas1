$('document').ready(function () {

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage').modal('hide');
                $('#header_image').val(response.filename);
                $('#header_image-error').css({"display": "none"});
            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * validation
     */
    $('#page_data_form').validate({
        ignore:[],
        rules: {
			page:{
				 required: true,
			},
           
			header_image: {
                required: true,
            },
					
			
			data: {
                required: function(textarea) {
					  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
					  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
					  return editorcontent.length === 0;
				}
			},
			video_link: {
                required: true,
            },
					
        },
        messages: {
			
            page: {
                required: "Page Heading  is required",
            },
			
			data: {
                required: "Content is required",
            },
			header_image: {
                required: "Image is required",
            },
			video_link: {
                required: "Vide Link is required",
            },			
			
        },

        submitHandler: function (form) {
            var page = $('#page').val();
            var idd = $('#idd').val();
            var video_link = $('#video_link').val();
            var data =  CKEDITOR.instances['data'].getData();
            var header_image =$('#header_image').val();
             
             $.post(APP_URL + 'configure_access/add_page_data', {
                 id: idd,
                 page: page,                
                 video_link: video_link,                
                 data: data,                                                             
                 header_image: header_image,                                                             
             }, 
                function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_page_form').empty();
                        if (response.status == 200) {
                            $('#err_page_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        else if (response.status == 201) {
                            $('#err_page_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#idd').val(0);
                        $('#page').val('');
                        $('#data').val('');
                        $('#video_link').val('');
                        $('#header_image').val('');
						$('.img-preview').attr('src','');
                    }, 'json');
            return false;
        }
    });


    
    


})