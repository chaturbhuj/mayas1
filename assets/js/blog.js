$('document').ready(function () {

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage').modal('hide');
                $('#blog_image').val(response.filename);
                $('#blog_image-error').css({"display": "none"});
            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_blog_form validation
     */
    $('#add_blog_form').validate({
        ignore: [],
        rules: {
            blog_heading: {
                required: true
            },
            blog_sub_heading: {
                required: true
            },
			 blog_choose: {
                required: true
            },
			blog_writer: {
                required: true
            },
			
			
            blog_description:{
				 required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
			}
        },
        messages: {
            blog_choose: {
                required: "Blog choose  is required"
            },
			 blog_writer: {
                required: "Blog writer  is required"
            },
			
			
			blog_heading: {
                required: "Blog Heading  is required"
            },
            blog_sub_heading: {
                required: "Blog Sub Heading is required"
            },
            blog_description: {
                required: "Blog Description is required"
            }
        },
        submitHandler: function (form) {
            var blog_heading = $('#blog_heading').val();
            var blog_choose = $('#blog_choose').val();
            var blog_sub_heading = $('#blog_sub_heading').val();
            var blog_description = $('#blog_description').val();
            var blog_image = $('#blog_image').val();
            var blog_writer = $('#blog_writer').val();
            var blog_id = $('#blog_id').val();
			var blog_description = CKEDITOR.instances['blog_description'].getData();

            $.post(APP_URL + 'configure_access/add_new_blog', {
                blog_choose: blog_choose,
                blog_heading: blog_heading,
                blog_sub_heading: blog_sub_heading,
                blog_description: blog_description,
                blog_image: blog_image,
                blog_id: blog_id,
                blog_writer: blog_writer,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_blog_form').empty();
                        if (response.status == 200) {
                            $('#err_blog_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        else if (response.status == 201) {
                            $('#err_blog_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#blog_id').val(0);
                        $('#blog_heading').val('');
                        $('#blog_choose').val('');
                        $('#blog_sub_heading').val('');
                        $('#blog_description').val('');
                        $('#blog_image').val('');
						$('.img-preview').attr('src','');
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit blog
     */

    $('body').on('click', '.edit_blog', function () {
        $('#err_edit_blog').empty();
        $('#edit_blog_form label.error').empty();
        $('#edit_blog_form input.error').removeClass('error');
        $('#edit_blog_form select.error').removeClass('error');

        var edit_blog_id = $(this).attr('name');
        var edit_blog_heading = $(this).closest('tr').find('td:eq(1)').text();
        var edit_blog_sub_heading = $(this).closest('tr').find('td:eq(2)').text();
        var edit_blog_description = $(this).closest('tr').find('td:eq(3)').text();

        $('#edit_blog_id').val(edit_blog_id);
        $('#edit_blog_heading').val(edit_blog_heading);
        $('#edit_blog_sub_heading').val(edit_blog_sub_heading);
        $('#edit_blog_description').val(edit_blog_description);

    });

    //------------------------------------------------------------------------
    /* 
     * This function is used to call for edit blog information
     */
    $('#edit_blog_form').validate({
        rules: {
            edit_blog_heading: {
                required: true
            },
			edit_blog_sub_heading: {
                required: true
            },
			edit_blog_description: {
                required: true
            },
        },
        messages: {
            edit_blog_heading: {
                required: "Blog Name is required"
            },
			edit_blog_sub_heading: {
                required: "Blog Sub Heading is required"
            },
			edit_blog_description: {
                required: "Blog Description is required"
            },
			
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_blog_id = $('#edit_blog_id').val();
            var edit_blog_heading = $('#edit_blog_heading').val();
            var edit_blog_sub_heading = $('#edit_blog_sub_heading').val();
            var edit_blog_description = $('#edit_blog_description').val();

            $.post(APP_URL + 'configure_access/edit_blog_information', {
                edit_blog_id: edit_blog_id,
                edit_blog_heading: edit_blog_heading,
                edit_blog_sub_heading: edit_blog_sub_heading,
                edit_blog_description: edit_blog_description,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_blog').empty();
                if (response.status == 200) {
                    $('#err_edit_blog').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_blog[name=' + edit_blog_id + ']').closest("tr").find("td:eq(1)").text(edit_blog_heading);
                    $('.edit_blog[name=' + edit_blog_id + ']').closest("tr").find("td:eq(2)").text(edit_blog_sub_heading);
                    $('.edit_blog[name=' + edit_blog_id + ']').closest("tr").find("td:eq(3)").text(edit_blog_description);

                } else if (response.status == 201) {
                    $('#err_edit_blog').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
            return false;
        }
    });

    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit blog
     */
    var edit_blog_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#image_blog_id').val($(this).attr('name'));
        edit_blog_id_for_image = $(this).attr('name');
    });
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $('.editBrowseImageBtn[name=' + edit_blog_id_for_image + ']').closest("tr").find(".blog_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove blog from the list
     */

    $('body').on('click', '.remove_blog', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_blog_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_blog', {edit_blog_id: edit_blog_id}, function (response) {
            $('#err_edit_blog').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_blog').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_blog[name=' + edit_blog_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_blog').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})