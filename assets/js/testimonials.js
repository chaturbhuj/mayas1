$('document').ready(function(){
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
        var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		$('#upload_image').ajaxForm({
			dataType: 'JSON',
			beforeSend: function() {
				$('#status').empty();
				var percentVal = '0%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			complete: function(response) {
				var status= (response.responseText).split('status":')[1].split(',"')[0];
				if(status == '200'){
					var filename_ = (response.responseText).split('filename":"')[1].split('"}')[0];				
					$(".img-preview").removeClass('display_none').addClass('display_inline');
					$(".img-preview").attr('src',APP_URL+'uploads/' + filename_);
					$('#browseImage').modal('hide');
					$('#testimonials_image').val(filename_);
					$('#testimonials_image-error').css({"display": "none"});
					$("#myFile").val('');
					
				}else{
					alert('Invalid file');
				}
				
				$('.percent').html('0%');
				$.unblockUI();
				return false;
			}
		});
	
	
	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit testimonials
     */
    $('body').on('click', '.edit_testimonials', function () {		
        $('#err_testimonials_form').empty();

        var testimonials_id = $(this).attr('name');
        var testimonials_name = $(this).closest('tr').find('td:eq(1)').text();		
        var position = $(this).closest('tr').find('td:eq(2)').text();
        var comments = $(this).closest('tr').find('td:eq(3)').text();
		var testimonials_image = $(this).closest('tr').find('td:eq(4)').find('img').attr('src');
		
        $('#testimonials_id').val(testimonials_id);
        $('#testimonials_name').val(testimonials_name);
		$('#position').val(position);
		$('#comments').val(comments);
		$(".img-preview").removeClass('display_none').addClass('display_inline');
		$(".img-preview").attr('src',testimonials_image);
		$('#testimonials_image').val(testimonials_image.split(APP_URL+'uploads/')[1]);
		
    });
	
	//------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new Slider
     */
    $('body').on('click', '.addNewTestimonials', function () {
		$("#testimonials_id").val(0);
		$("#testimonials_name").val('');
		$("#position").val('');
		$("#comments").val('');
		$("#testimonials_image").val('');
		$(".img-preview").removeClass('display_inline').addClass('display_none');
		$(".img-preview").attr('src','');
		
	});
	
	//-----------------------------------------------------------------------
    /* 
     * validation of add city
     */
	$('#testimonials_form').validate({
		ignore: [],
        rules: {
            testimonials_name: {
                required: true,
            },
            position: {
                required: true,
            },
			comments: {
                required: true,
            },
			testimonials_image: {
                required: true,
            },
         },
		 messages: {
			testimonials_name: {
                required: "Name  is required.",
            },
			position: {
                required: "Position is required.",
            },
			comments: {
                required: "Comments is required.",
            },
			testimonials_image: {
                required: "Image is required.",
            },
		},
		submitHandler: function (form) {
			var testimonials_id = $('#testimonials_id').val();
            var testimonials_name = $('#testimonials_name').val();
            var position = $('#position').val();
			var comments = $('#comments').val();
			var testimonials_image = $('#testimonials_image').val();
			$.post(APP_URL + 'configure_access/update_testimonials', {
                testimonials_id: testimonials_id,
                testimonials_name: testimonials_name,
                position: position,
				comments: comments,
				testimonials_image: testimonials_image,
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_testimonials_form').empty();
				if (response.status == 200) {
                    var message = response.message;
					if(testimonials_id!=0){
						message = "Testimonials has been updated successfully!";
						$('.edit_testimonials[name=' + testimonials_id + ']').closest("tr").find("td:eq(1)").text(testimonials_name);
						$('.edit_testimonials[name=' + testimonials_id + ']').closest("tr").find("td:eq(2)").text(position);
						$('.edit_testimonials[name=' + testimonials_id + ']').closest("tr").find("td:eq(3)").text(comments);
						$('.edit_testimonials[name=' + testimonials_id + ']').closest("tr").find("td:eq(4)").find('img').attr('src',APP_URL+'uploads/' + testimonials_image);
						
					}else{
						$('#err_testimonials_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + message + "</strong></div>");
						window.setTimeout(function(){location.reload()},3000);
					}
					$('#err_testimonials_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + message + "</strong></div>");
					
                }
                else if (response.status == 201) {
                    $('#err_testimonials_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
				$("#testimonials_id").val(0);
				$("#testimonials_name").val('');
				$("#position").val('');
				$("#comments").val('');
				$("#testimonials_image").val('');
				$(".img-preview").removeClass('display_inline').addClass('display_none');
				$(".img-preview").attr('src','');
				$('#browseNewTestimonials').modal('hide');
				
				
			}, 'json');
		return false;
		},
	});
	
	//---------------------------------------------------------------------
    /**
     * This script is used to remove testimonials from the list
     */
	$('body').on('click', '.remove_testimonials', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var testimonials_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'configure_access/remove_testimonials', {testimonials_id: testimonials_id}, function (response) {
            $('#err_testimonials_form').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#err_testimonials_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_testimonials[name=' + testimonials_id + ']').closest("tr").remove();
            }
            else {
                $('#err_testimonials_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
});