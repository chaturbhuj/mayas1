$('document').ready(function () {

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage').modal('hide');
                $('#slider_image').val(response.filename);
                $('#slider_image-error').css({"display": "none"});
            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#add_slider_form').validate({
        ignore: [],
        rules: {
            slider_info: {
                required: true
            },
			slider_input1: {
                required: true
            },
			slider_input2: {
                required: true
            },
			slider_input3: {
                required: true
            },
			content_align: {
                required: true
            },
            slider_image: {
                required: true
            }

        },
        messages: {
            slider_info: {
                required: "Slider Info is required"
            },
			slider_input1: {
                required: "Slider input1 is required"
            },
			slider_input2: {
                required: "Slider input2 is required"
            },
			slider_input3: {
                required: "Slider input3 is required"
            },
			content_align: {
                required: "Content Alignment is required"
            },
            slider_image: {
                required: "Slider Image is required"
            }
        },
        submitHandler: function (form) {
            var slider_id = $('#slider_id').val();
            var slider_info = $('#slider_info').val();
            var slider_navi = $('#slider_navi').val();  
            var slider_navi_link = $('#slider_navi_link').val();  
			var slider_input1 = $('#slider_input1').val();
			var slider_input2 = $('#slider_input2').val();
			var slider_input3 = $('#slider_input3').val();
			var content_align = $('#content_align').val();
            var slider_image = $('#slider_image').val();
            var slider_position = $('#slider_position').val();
            var status = $('#status').val();

            $.post(APP_URL + 'configure_access/add_new_slider', {
                slider_id: slider_id,
                slider_info: slider_info,
                slider_navi: slider_navi,
                slider_navi_link: slider_navi_link,
                slider_input1: slider_input1,
                slider_input2: slider_input2,
                slider_input3: slider_input3,
                content_align: content_align,
                slider_image: slider_image,
                slider_position: slider_position,
                status: status,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_slider_form').empty();
                        if (response.status == 200) {
                            $('#err_slider_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        else if (response.status == 201) {
                            $('#err_slider_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#slider_id').val('');
						$('#slider_info').val('');
                        $('#slider_image').val('');
                        $('#slider_input1').val('');
                        $('#slider_input2').val('');
                        $('#slider_input3').val('');
                        $('#slider_navi').val('');
                        $('#slider_navi_link').val('');
                        $('#content_align').val('');
                        $('.img-preview').attr('src', '');
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_slider', function () {
        $('#err_edit_slider').empty();
        $('#edit_slider_form label.error').empty();
        $('#edit_slider_form input.error').removeClass('error');
        $('#edit_slider_form select.error').removeClass('error');

        var edit_slider_id = $(this).attr('name');
        var edit_slider_info = $(this).closest('tr').find('td:eq(1)').text();
        var edit_slider_navi = $(this).closest('tr').find('td:eq(2)').text();
        var edit_slider_input1 = $(this).closest('tr').find('td:eq(3)').text();
        var edit_slider_input2 = $(this).closest('tr').find('td:eq(4)').text();

        $('#edit_slider_id').val(edit_slider_id);
        $('#edit_slider_navi').val(edit_slider_navi);
        $('#edit_slider_info').val(edit_slider_info);
        $('#edit_slider_input1').val(edit_slider_input1);
        $('#edit_slider_input2').val(edit_slider_input2);

        $('#err_edit_slider').empty();
    });

    //------------------------------------------------------------------------
    /* 
     * This function is used to call for edit result information
     */
    $('#edit_slider_form').validate({
        rules: {
            edit_slider_info: {
                required: true
            },
            edit_slider_navi: {
                required: true
            },
			edit_slider_input1: {
                required: true
            },
			edit_slider_input2: {
                required: true
            },
        },
        messages: {
            edit_slider_info: {
                required: "Slider Info is required"
            },
            edit_slider_navi: {
                required: "Slider Info is required"
            },
			 edit_slider_input1: {
                required: "Slider Info is required"
            },
			 edit_slider_input2: {
                required: "Slider Info is required"
            },
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_slider_id = $('#edit_slider_id').val();
            var edit_slider_info = $('#edit_slider_info').val();
            var edit_slider_navi = $('#edit_slider_navi').val();
            var edit_slider_input1 = $('#edit_slider_input1').val();
            var edit_slider_input2 = $('#edit_slider_input2').val();

            $.post(APP_URL + 'configure_access/edit_slider_information', {
                edit_slider_id: edit_slider_id,
                edit_slider_navi: edit_slider_navi,
                edit_slider_info: edit_slider_info,
                edit_slider_input2: edit_slider_input2,
                edit_slider_input1: edit_slider_input1,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_slider').empty();
                if (response.status == 200) {
                    $('#err_edit_slider').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_slider[name=' + edit_slider_id + ']').closest("tr").find("td:eq(1)").text(edit_slider_info);
                    $('.edit_slider[name=' + edit_slider_id + ']').closest("tr").find("td:eq(2)").text(edit_slider_navi);

                } else if (response.status == 201) {
                    $('#err_edit_slider').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
            return false;
        }
    });

    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */
    var edit_slider_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#image_slider_id').val($(this).attr('name'));
        edit_slider_id_for_image = $(this).attr('name');
    });
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $('.editBrowseImageBtn[name=' + edit_slider_id_for_image + ']').closest("tr").find(".slider_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_slider', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_slider_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_slider', {edit_slider_id: edit_slider_id}, function (response) {
            $('#err_edit_slider').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_slider').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_slider[name=' + edit_slider_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_slider').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})