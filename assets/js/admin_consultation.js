$('document').ready(function () {
	
	$('body').on('change', '#consultation_name', function () {
		
		var servicesname = $('#consultation_name').val();
		var url =servicesname.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
		var newurl =url.toLowerCase();;
		
		$('#url').val(newurl);
	});


    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#c_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image2').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage2').modal('hide');
                $('#c_detail_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
	
	
   

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_date', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var consultation_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_free_consultation', {consultation_id: consultation_id}, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_date[name=' + consultation_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
	
})