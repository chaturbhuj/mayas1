$('document').ready(function(){
	
	/*setting popup status false when close the popup*/
	$('body').on('click','.close_popup_modal',function(){
		set_popup_close_date(false);
	});
	/**----------------------------------------------------------------
	  * Show model on open website
	  */
	  /*reading popup close date from cookie*/
		var cookie_date;
		var popup_status = true;
		var cookieArray = document.cookie.split(';');
		console.log(cookieArray);
		for(var i=0; i<cookieArray.length; i++) {
			 var cookieItem = cookieArray[i]; 
			 while (cookieItem.charAt(0)==' ') cookieItem = cookieItem.substring(1);
			 if (cookieItem.indexOf('mayas_popup_date') == 0){
				  var cookieStr = cookieItem.split('mayas_popup_date=')[1];
				  //console.log(cookieStr);
				  //console.log(i);
				  var obj = JSON.parse(cookieStr);
				  var keySize = 0;
				  cookie_date = obj[0].date;
				  popup_status = obj[0].popup_status;
				  //console.log(popup_status);
			 }else{
				 /*open first time in browser */
				 set_popup_close_date(true);
			 }
		}
		//console.log(popup_status);
		var d = new Date();
	   /*gettting the current date*/
	   var month = d.getMonth()+1;
	   var day = d.getDate();
	   var current_date = d.getFullYear() + '/' +
	   (month<10 ? '0' : '') + month + '/' +
	   (day<10 ? '0' : '') + day;
	   if(current_date==cookie_date && popup_status==false){
		   /*do not show popup*/
	   }if(current_date==cookie_date && popup_status==true){
		   /*show the popup*/
		   if ($(window).width() > 400) {
				window.setTimeout(function () {
				   $('#browsePopup').modal('show');
				}, 1000);
			}	
	   }
	  
	/*setting the popup close date and status into cookie*/
	function set_popup_close_date(popup_status){
		  var domain = '.mayasastrology';
		  var d = new Date();
		  /*gettting the current date*/
		  var month = d.getMonth()+1;
		  var day = d.getDate();
		  var output = d.getFullYear() + '/' +
		  (month<10 ? '0' : '') + month + '/' +
		  (day<10 ? '0' : '') + day;
		  
		  d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		  var expires = "expires=" + d.toGMTString();		
		  if(popup_status!=false){
			  popup_status = true;
		  }
		  var popup_close_date = [];
		  //popup_close_date['date'] = output;
		  popup_close_date.push({
			  date:output,
			  popup_status:popup_status,
		  });
		  document.cookie = "mayas_popup_date= "+JSON.stringify(popup_close_date)+"; "+expires+";domain=;path=/";
		  //console.log(document.cookie);
	}
	
	$('body').on('click','.find-lagan',function(){
		alert('Pop will open, fill out form get know your lagan in pop.');
	});
	$('body').on('click','.more-gemstherapy',function(){
		alert('Pop will open, to show all gemstome therapy.');
	});
	$('body').on('click','.substitute-gems',function(){
		alert('Pop will open, show as you send me a image of table.');
	});
	
	/* set active menu  */
	 var current_url = $(location).attr('href').split("mayas/")[1];
	 if(current_url == ""){
			$('.li_home').addClass('active');
		}else if(current_url == "aboutus"){
			$('.li_aboutus').addClass('active');
		}else if(current_url == "services"){
			$('.li_sarvices').addClass('active');
		}else if(current_url == "products"){
			$('.li_products').addClass('active');
		}else if(current_url == "article"){
			$('.li_article').addClass('active');
		}else if(current_url == "contactus"){
			$('.li_contactus').addClass('active');
		}
		
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to show modal for free telephonic consultancy
     */

	
	//-----------------------------------------------------------------------
    /* 
     * add_free_Consultant_form validation
     */
    $('#freeConsultantForm').validate({
        ignore: [],
        rules: {
            inputName: {
                required: true
            },
            inputBirth: {
                required: true
            },
            inputTimeHr: {
                required: true
            },
            inputTimeMin: {
                required: true
            },
			inputTimeSec: {
                required: true
            },
            inputPlace: {
                required: true
            },
            inputContact: {
                required: true
            },
			inputEmail: {
                required: true
            },
            inputQuestion: {
                required: true
            }
        },
        messages: {
            inputName: {
                required: "Name is required",
            },
			inputBirth: {
                required: "DOB is required",
            },
			inputTimeHr: {
                required: "Hour(0 to 23 ) is required",
            },
			inputTimeMin: {
                required: "Minutes(0 to 59 ) is required",
            },
			inputTimeSec: {
                required: "Seconds(0 t0 59) is required",
            },
			inputPlace: {
                required: "Birth Place is required",
            },
			inputContact: {
                required: "Contact Number is required",
            },
			inputEmail: {
                required: "Email id is required",
            },
			inputQuestion: {
                required: "Your Question is required",
            }
        },
        submitHandler: function (form) { 
            var inputName = $('#inputName').val();
            var inputBirth = $('#inputBirth').val();
            var inputTimeHr = $('#inputTimeHr').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var inputTimeSec = $('#inputTimeSec').val();
			var inputPlace = $('#inputPlace').val();
			var inputContact = $('#inputContact').val();
			var inputEmail = $('#inputEmail').val();
			var inputQuestion = $('#inputQuestion').val();
			console.log('APP_URL = '+ APP_URL);	
            $.post(APP_URL + 'welcome/add_new_free_consultant', {
                inputName: inputName,
                inputBirth: inputBirth,
				inputEmail: inputEmail,
				inputTimeHr: inputTimeHr,
				inputTimeMin: inputTimeMin,
				inputTimeSec: inputTimeSec,
				inputPlace: inputPlace,
				inputContact: inputContact,
                inputEmail: inputEmail,
                inputQuestion: inputQuestion,
            },
            function (response) {
                $('#err_contactus_form').empty();
                if (response.status == 200) {
                    $('#err_contactus_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_contactus_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#inputName').val('');
                $('#inputName').val('');
				$('#inputBirth').val('');
				$('#inputTimeHr').val('');
				$('#inputTimeMin').val('');
				$('#inputTimeSec').val('');
				$('#inputPlace').val('');
				$('#inputContact').val('');
				$('#inputEmail').val('');
				$('#inputQuestion').val('');
            }, 'json');
            return false;
        }
    });
	
	
	//-----------------------------------------------------------------------
    /* 
     * add_free_Consultant_form validation
     */
    $('#freeGemstoneRecommendation').validate({
        ignore: [],
        rules: {
            inputName: {
                required: true
            },
            inputBirth: {
                required: true
            },
            inputTimeHr: {
                required: true
            },
            inputTimeMin: {
                required: true
            },
			inputTimeSec: {
                required: true
            },
            inputPlace: {
                required: true
            },
            inputContact: {
                required: true
            },
			inputEmail: {
                required: true
            },
            inputQuestion: {
                required: true
            }
        },
        messages: {
            inputName: {
                required: "Name is required",
            },
			inputBirth: {
                required: "DOB is required",
            },
			inputTimeHr: {
                required: "Hour(0 to 23 ) is required",
            },
			inputTimeMin: {
                required: "Minutes(0 to 59 ) is required",
            },
			inputTimeSec: {
                required: "Seconds(0 t0 59) is required",
            },
			inputPlace: {
                required: "Birth Place is required",
            },
			inputContact: {
                required: "Contact Number is required",
            },
			inputEmail: {
                required: "Email id is required",
            },
			inputQuestion: {
                required: "Your Question is required",
            }
        },
        submitHandler: function (form) { 
            var inputName = $('#inputName').val();
            var inputBirth = $('#inputBirth').val();
            var inputTimeHr = $('#inputTimeHr').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var inputTimeSec = $('#inputTimeSec').val();
			var inputPlace = $('#inputPlace').val();
			var inputContact = $('#inputContact').val();
			var inputEmail = $('#inputEmail').val();
			var inputQuestion = $('#inputQuestion').val();
			console.log('APP_URL = '+ APP_URL);	
            $.post(APP_URL + 'welcome/add_new_free_consultant', {
                inputName: inputName,
                inputBirth: inputBirth,
				inputEmail: inputEmail,
				inputTimeHr: inputTimeHr,
				inputTimeMin: inputTimeMin,
				inputTimeSec: inputTimeSec,
				inputPlace: inputPlace,
				inputContact: inputContact,
                inputEmail: inputEmail,
                inputQuestion: inputQuestion,
            },
            function (response) {
                $('#err_contactus_form').empty();
                if (response.status == 200) {
                    $('#err_contactus_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_contactus_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#inputName').val('');
                $('#inputName').val('');
				$('#inputBirth').val('');
				$('#inputTimeHr').val('');
				$('#inputTimeMin').val('');
				$('#inputTimeSec').val('');
				$('#inputPlace').val('');
				$('#inputContact').val('');
				$('#inputEmail').val('');
				$('#inputQuestion').val('');
            }, 'json');
            return false;
        }
    });
	
	//-----------------------------------------------------------------------
    /* 
     * check the type of consultation 
     */
	$('.Career').css({'display':'none'});
	$('.MatchMaking').css({'display':'none'});	
	$('body').on('change','#consultantType',function(){
		var consultantType = $(this).val();
		if(consultantType == 'Career'){
			$('.Career').css({'display':'block'});
			$('.MatchMaking').css({'display':'none'});
		}else if(consultantType == 'BirthStone' || consultantType == 'HoroscopeQuery'){
			$('.Career').css({'display':'none'});
			$('.MatchMaking').css({'display':'none'});			
		}else if(consultantType == 'MatchMaking'){
			$('.Career').css({'display':'none'});
			$('.MatchMaking').css({'display':'block'});			
		}
	});

})