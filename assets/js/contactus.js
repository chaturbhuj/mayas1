$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
/*    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    }); */

    //-----------------------------------------------------------------------
    /* 
     * add_contactus_form validation
     */
    $('#add_contact_form').validate({
        ignore: [],
        rules: {
            inputName: {
                required: true
            },
            inputEmail: {
                required: true
            },
            inputWebsite: {
                required: true
            },
			inputMessage: {
                required: true
            },
        },
        messages: {
            inputName: {
                required: "Name is required",
            },
			inputEmail: {
                required: "Email id  is required",
            },
			inputWebsite: {
                required: "Website Name is required",
            },
			inputMessage: {
                required: "Message is required",
            },
		},
        submitHandler: function (form) { 
            var inputName = $('#inputName').val();
            var inputEmail = $('#inputEmail').val();
            var inputWebsite = $('#inputWebsite').val();
            var inputMessage = $('#inputMessage').val();
            $.post(APP_URL + 'welcome/add_new_contactus', {
                inputName: inputName,
                inputEmail: inputEmail,
				inputWebsite: inputWebsite,
				inputMessage: inputMessage,
            },
            function (response) {
                $('#err_contactus_form').empty();
                if (response.status == 200) {
                    $('#err_contactus_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_contactus_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#inputName').val('');
                $('#inputEmail').val('');
                $('#inputWebsite').val('');
                $('#inputMessage').val('');
            }, 'json');
            return false;
        }
    });


})