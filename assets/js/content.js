$('document').ready(function(){

	
	//-----------------------------------------------------------------------
    /* 
     * validation of add city
     */
	$('#content_form').validate({
		ignore: [],
        rules: {
            free_consultation_content: {
                required: true,
            },
			premium_consultation_content: {
                required: true,
            },
			header_content: {
                required: true,
            },
			
         },
		 messages: {
			free_consultation_content: {
                required: "Free consultation content   is required.",
            },
			premium_consultation_content: {
                required: "Free consultation content   is required.",
            },
			header_content: {
                required: "header content   is required.",
            },
		},
		submitHandler: function (form) {
			var free_consultation_content = $('#free_consultation_content').val();
			var premium_consultation_content = $('#premium_consultation_content').val();
			var header_content = $('#header_content').val();
			var free_consultation_desc = $('#free_consultation_desc').val();
			var premium_consultation_desc = $('#premium_consultation_desc').val();
			$.post(APP_URL + 'configure_access/update_content', {
                free_consultation_content: free_consultation_content,
                premium_consultation_content: premium_consultation_content,
				header_content: header_content,
				free_consultation_desc: free_consultation_desc,
				premium_consultation_desc: premium_consultation_desc,
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_testimonials_form').empty();
				if (response.status == 200) {
                    var message = response.message;
					
						$('#err_testimonials_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + message + "</strong></div>");
						
					
                }
                else if (response.status == 201) {
                    $('#err_testimonials_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
				
				
			}, 'json');
		return false;
		},
	});
	
});