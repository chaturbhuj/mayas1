$('document').ready(function () {
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to add new input box for image upload for add event
     */
    function GetDynamicTextBox(value, src) {
        return '<img class="img-preview product_images" name="product_images" value = "' + value + '"  src="' + src + '" > &nbsp;' +
                '<input type="button" value="Remove" class="remove_image" />'
    }
    $("body").on("click", ".remove_image", function () {
        $(this).closest("div").remove();
    });

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                var div = $("<div />");
				console.log(response);
                div.html(GetDynamicTextBox(response.filename, APP_URL + 'uploads/' + response.filename));
                $("#productImageContainer").append(div);

                $('#browseImage').modal('hide');
                //$('#result_image').val(response.filename);
                //$('#err_gallery_form').css({"display":"none"});

            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_product_form validation
     */
    $('#add_product_form').validate({
        ignore: [],
        rules: {
            product_categories: {
                required: true
            },
			 product_name: {
                required: true
            },
			 product_description: {
                required: true
            },
			 product_price_ind: {
                required: true
            },

        },
        messages: {
            product_categories: {
                required: "Product Categories is required"
            },
			product_name: {
                required: "Product Name is required"
            },
			product_description: {
                required: "Product Description is required"
            },
			product_price_ind: {
                required: "Product Price is required"
            }
        },
        submitHandler: function (form) {
			var product_categories = $('#product_categories').val();
            var product_name = $('#product_name').val();
            var product_description = $('#product_description').val();
            var product_price_ind = $('#product_price_ind').val();
			var product_price_usd = $('#product_price_usd').val();
			var product_feature = $('#product_feature').val();
			var product_weight = $('#product_weight').val();
			var product_shape = $('#product_shape').val();
			var product_color = $('#product_color').val();
		    var product_clarity = $('#product_clarity').val(); 
		    var product_grade = $('#product_grade').val();
			var product_cut = $('#product_cut').val();
			var product_treatment = $('#product_treatment').val();                                                 
			var product_origin = $('#product_origin').val();
			var product_other_info = $('#product_other_info').val();
			var product_other_info2 = $('#product_other_info2').val();
			var product_other_info3 = $('#product_other_info3').val();
			var product_image_list = "";
			$("img[name=product_images]").each(function () {
                product_image_list += $(this).attr('value') + ",";
            });
            $.post(APP_URL + 'configure_access/add_new_product', {
                product_categories: product_categories,
                product_name: product_name,
				product_description: product_description,
                product_price_ind: product_price_ind,
				product_price_usd: product_price_usd,
				product_feature: product_feature,
				product_weight:product_weight,
				product_shape: product_shape,
				product_color: product_color,
				product_clarity: product_clarity,
				product_grade: product_grade,
				product_cut: product_cut,
				product_treatment:product_treatment,
				product_origin: product_origin,
				product_other_info: product_other_info,
				product_other_info2: product_other_info2,
				product_other_info3: product_other_info3,
				product_image_list:product_image_list,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_product_form').empty();
                        if (response.status == 200) {
                            $('#err_product_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        else if (response.status == 201) {
                            $('#err_product_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#product_name').val('');
                        $('#product_description').val('');
                        $('#product_price_ind').val('');
						$('#product_price_usd').val('');
						$('#product_feature').val('');
						$('#product_weight').val('');
						$('#product_shape').val('');
						$('#product_color').val('');
						$('#product_clarity').val('');
						$('#product_grade').val('');
						$('#product_cut').val('');
						$('#product_treatment').val('');
						$('#product_origin').val('');
						$('#product_other_info').val('');
						$('#product_other_info2').val('');
						$('#product_other_info3').val('');
						$('#product_image_list').val('');
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit product
     */

    $('body').on('click', '.edit_product', function () {
        $('#err_edit_slider').empty();
        $('#edit_slider_form label.error').empty();
        $('#edit_slider_form input.error').removeClass('error');
        $('#edit_slider_form select.error').removeClass('error');

        var product_id = $(this).attr('name');
        var product_categories = $(this).closest('tr').find('td:eq(1)').attr('name');
        var product_name = $(this).closest('tr').find('td:eq(2)').text();
		var product_description = $(this).closest('tr').find('td:eq(3)').text();
		var product_price_ind = $(this).closest('tr').find('td:eq(4)').text();
		var product_price_usd = $(this).closest('tr').find('td:eq(5)').text();
		var product_feature = $(this).closest('tr').find('td:eq(6)').text();
		var product_weight = $(this).closest('tr').find('td:eq(7)').text();
		var product_shape = $(this).closest('tr').find('td:eq(8)').text();
		var product_color = $(this).closest('tr').find('td:eq(9)').text();
		var product_clarity = $(this).closest('tr').find('td:eq(10)').text();
		var product_grade = $(this).closest('tr').find('td:eq(11)').text();
		var product_cut = $(this).closest('tr').find('td:eq(12)').text();
		var product_treatment = $(this).closest('tr').find('td:eq(13)').text();
		var product_origin = $(this).closest('tr').find('td:eq(14)').text();
		var product_other_info = $(this).closest('tr').find('td:eq(15)').text();
		var product_other_info2 = $(this).closest('tr').find('td:eq(16)').text();
		var product_other_info3 = $(this).closest('tr').find('td:eq(17)').text();

		$('#product_id').val(product_id);
        $('#product_name').val(product_name);
        $('#product_categories>option:eq('+product_categories+')').attr('selected',true);
        $('#product_description').val(product_description);
		$('#product_price_ind').val(product_price_ind);
		$('#product_price_usd').val(product_price_usd);
		$('#product_feature').val(product_feature);
		$('#product_weight').val(product_weight);
		$('#product_shape').val(product_shape);
		$('#product_color').val(product_color);
		$('#product_clarity').val(product_clarity);
		$('#product_grade').val(product_grade);
		$('#product_cut').val(product_cut);
		$('#product_treatment').val(product_treatment);
		$('#product_origin').val(product_origin);
		$('#product_other_info').val(product_other_info);
		$('#product_other_info2').val(product_other_info2);
		$('#product_other_info3').val(product_other_info3);

        $('#err_edit_product').empty();
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#edit_product_form').validate({
        ignore: [],
        rules: {
            product_categories: {
                required: true
            },
			 product_name: {
                required: true
            },
			 product_description: {
                required: true
            },
			 product_price_ind: {
                required: true
            },

        },
        messages: {
            product_categories: {
                required: "Product Categories is required"
            },
			product_name: {
                required: "Product Name is required"
            },
			product_description: {
                required: "Product Description is required"
            },
			product_price_ind: {
                required: "Product Price is required"
            }
        },
        submitHandler: function (form) {
			var product_id= $('#product_id').val();
			var product_categories = $('#product_categories').val();
			var sub_product_name = $('#product_categories').text();
            var product_name = $('#product_name').val();
            var product_description = $('#product_description').val();
            var product_price_ind = $('#product_price_ind').val();
			var product_price_usd = $('#product_price_usd').val();
			var product_feature = $('#product_feature').val();
			var product_weight = $('#product_weight').val();
			var product_shape = $('#product_shape').val();
			var product_color = $('#product_color').val();
		    var product_clarity = $('#product_clarity').val(); 
		    var product_grade = $('#product_grade').val();
			var product_cut = $('#product_cut').val();
			var product_treatment = $('#product_treatment').val();                                                 
			var product_origin = $('#product_origin').val();
			var product_other_info = $('#product_other_info').val();
			var product_other_info2 = $('#product_other_info2').val();
			var product_other_info3 = $('#product_other_info3').val();
			var product_image_list = "";
			$("img[name=product_images]").each(function () {
                product_image_list += $(this).attr('value') + ",";
            });
            $.post(APP_URL + 'configure_access/edit_product_information', {
			    product_id: product_id,
                product_categories: product_categories,
                product_name: product_name,
				product_description: product_description,
                product_price_ind: product_price_ind,
				product_price_usd: product_price_usd,
				product_feature: product_feature,
				product_weight:product_weight,
				product_shape: product_shape,
				product_color: product_color,
				product_clarity: product_clarity,
				product_grade: product_grade,
				product_cut: product_cut,
				product_treatment:product_treatment,
				product_origin: product_origin,
				product_other_info: product_other_info,
				product_other_info2: product_other_info2,
				product_other_info3: product_other_info3,
				product_image_list:product_image_list,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_edit_product').empty();
						$('#my_user_edit').modal('hide');
                        if (response.status == 200) {
                            $('#err_edit_product').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(1)").text(sub_product_name);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(2)").text(product_name);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(3)").text(product_description);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(4)").text(product_price_ind);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(5)").text(product_price_usd);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(6)").text(product_feature);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(7)").text(product_weight);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(8)").text(product_shape);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(9)").text(product_color);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(10)").text(product_clarity);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(11)").text(product_grade);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(12)").text(product_cut);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(13)").text(product_treatment);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(14)").text(product_origin);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(15)").text(product_other_info);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(16)").text(product_other_info2);
						$('.edit_product[name=' + product_id + ']').closest("tr").find("td:eq(17)").text(product_other_info3);
                        }
                        else if (response.status == 201) {
                            $('#err_edit_product').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });
   //------------------------------------------------------------------------//
    /**
     * This part of script is used to add new input box for image upload for edit event
     */
    function GetDynamicTextBoxForEdit(value, src) {
        return '<img class="img-preview edit_product_images" name="edit_gallery_images" value = "' + value + '"  src="' + src + '" > &nbsp;' +
                '<input type="button" value="Remove" class="edit_remove_image" />'
    }
	 //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */
    var edit_product_id_for_image = 0;
    var edit_product_upload_class = '';
    var edit_product_upload_id = '';
    var edit_product_td_id = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        edit_product_upload_class = $(this).attr('class');
        edit_product_upload_id = $(this).attr('id');
        $('#image_product_id').val($(this).attr('name'));
        edit_product_id_for_image = $(this).attr('name');
        edit_product_td_id = $(this).closest('td').attr('class');
        var edit_product_image_list = "";
        $(this).closest('td').find(".edit_product_images").each(function () {
            edit_product_image_list += $(this).attr('value') + ",";
        });
        $('#edit_product_image_list').val(edit_product_image_list);
    });
    
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                if (edit_product_upload_class == 'editBrowseImageBtn btnImg') {
                    $('#' + edit_product_upload_id + '').find('.edit_product_images').attr('src', APP_URL + 'uploads/' + response.filename);
                } else if (edit_product_upload_class == 'editBrowseImageBtn btnAdd') {
                    var div = $("<div/>");
                    div.html(GetDynamicTextBoxForEdit(response.filename, APP_URL + 'uploads/' + response.filename));
                    $('.' + edit_product_td_id + '').append(div);
                }
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
	
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to remove image of event
     */
    $("body").on("click", ".edit_remove_image", function () {
        var edit_product_id = $(this).closest('div').find('.btnImg').attr('name');
        var current_image = $(this).closest('div').find('.edit_product_images').attr('value') + ',';
        var edit_product_image_list = "";
        $(this).closest('td').find(".edit_product_images").each(function () {
            edit_product_image_list += $(this).attr('value') + ",";
        });
        var updated_product_image_list = edit_product_image_list.replace(current_image, "");

        $(this).closest("div").remove();
        $.post(APP_URL + 'configure_access/remove_product_image', {
            product_id: edit_product_id,
            updated_product_image_list: updated_product_image_list
        }, function (response) {
            $('#err_edit_product').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_product').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
            else {
                $('#err_edit_product').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_product', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var product_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_product', {product_id: product_id}, function (response) {
            $('#err_edit_product').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_product').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_product[name=' + product_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_product').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})