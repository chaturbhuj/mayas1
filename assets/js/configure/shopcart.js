$('document').ready(function () {
	
	
	
	$('body').on('change', '#single_or_married', function () {
		
		var single_or_married = $('#single_or_married').val();console.log(single_or_married);
		if(single_or_married == 'Single'){
			$('.MatchMakingInput').addClass('display_none').removeClass('display_block')
		}else  {
			$('.MatchMakingInput').removeClass('display_none').addClass('display_block')
		}
	});
	
	var consultation_type_just_read = '';
	var consultation_type_name = '';
	
	/**
	 * This script it used to set cookie when ever user doing action on cart
	 */
	
	
	//-----------------------------------------------------------------------
	
		/* instance cookke empaty */
	var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		

				document.cookie = "Instant_Consultation_order_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
				
			/* set active menu  */
	
	 import_cookie_into_li();
	var cart_order_detail=[];
	/*importing the cookie data in footer li*/
	function import_cookie_into_li(){
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		
		$('ul.cart_order_detail').empty();
		var product_cart_counter = 0;
		var shoping_cart_total = 0;
		
		var cart_total = 0;
			console.log(JSON.parse(localStorage.getItem('service_cart')));
			var obj = JSON.parse(localStorage.getItem('service_cart'));
			var keySize = 0;
			for (key in obj) {
			
				var service_id = obj[key].service_id;
				var services_name = obj[key].services_name;
				var consultant_type = obj[key].consultant_type;
				var number_of_questions = obj[key].number_of_questions;
				var cost = obj[key].cost;
				var services_image = obj[key].services_image;
				
				if(consultant_type){
				
					consultation_type_just_read = consultant_type;
					consultation_type_name = services_name;
					
				

				}
					
			}
	}
	
	
		

	/*	
		$('.consultantType option').each(function(){
		
		if($(this).attr('value') == consultation_type_just_read){
			$(this).prop('selected',true);
			$("#consultantType").val("consultation_type_just_read").change();
		}
	});*/
	//console.log(segment3);
	$('.consultantType option').each(function(){
		
		if($(this).attr('value') == consultation_type_just_read){
			
			$(this).prop('selected',true);
			$('.match').css({'display':'block'});
				
				
		
			if(consultation_type_just_read == 'Career'){
				$('.Career').css({'display':'block'});
				$('.MatchMaking').css({'display':'none'});
				
			}else if(consultation_type_just_read == 'BirthStone' ){
				$('.Career').css({'display':'none'});
				$('.MatchMaking').css({'display':'none'});	
				
			}
			else if( consultation_type_just_read == 'HoroscopeQuery'){
				$('.Career').css({'display':'none'});
				$('.MatchMaking').css({'display':'none'});	
				
			}
			
			
			else if(consultation_type_just_read == 'MatchMaking'){
					$('.Career').css({'display':'none'});
					$('.MatchMaking').css({'display':'block'});	
				
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to your Married Life or Children (Birth issue). ');
			}
			
		}
		
		
	});
	
		
	jQuery.validator.addMethod("validDate", function(value, element) {console.log('d');
        return this.optional(element) || moment(value,"DD-MM-YYYY").isValid();
    }, "Please enter a valid date in the format DD-MM-YYYY");	
	//-----------------------------------------------------------------------
    /* 
     * consultation_form validation inputDateDate
     */
	$('#newFreeConsultantForm').validate({
		rules: {
			consultantType: {
                required: true
            },
			inputName: {
                required: true
            },
			inputGender: {
                required: true
            },
			inputDateDate: {
                required: true
            },
			inputDateDate1: {
                required: true
            },
			inputDateDate2: {
                required: true
            },
			inputDateDate3: {
                required: true
            },
			inputDateDate4: {
                required: true
            },
			
			birthCountry: {
                required: true
            },
			birthState: {
                required: true
            },
			birthCity: {
                required: true
            },
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                required: true,
				number: true,
				minlength:10,
				maxlength:12
            },
			educationQualification: {
                required: true
            },
			currentProfession: {
                required: true
            },
			inputQuestion:{
                required: true				
			}
		},
		messages: {
			consultantType: {
                required: "Consultant Type is required"
            },
			inputName: {
                required: "Full Name is required"
            },
			inputGender: {
                required: "Gender is required"
            },
			inputDateDate: {
                required: "Birth day is required"
            },
			inputDateDate1: {
                required: "Birth month is required"
            },
			inputDateDate2: {
                required: "Birth year is required"
            },
			inputDateDate3: {
                required: "Birth hour is required"
            },
			inputDateDate4: {
                required: "Birth Minutes is required"
            }, 
			birthCountry: {
                required: "Birth Country Name is required"
            },
			birthState: {
                required: "Birth State Name is required"
            },
			birthCity: {
                required: "Birth City Name is required"
            },
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },
			educationQualification: {
                required: "Highest Qualification is required"
            },
			currentProfession: {
                required: "Current Profession is required"
            },
			inputQuestion:{
                required: "Precise Question is required"				
			}
		}, 
		submitHandler: function (form) {
			var currency = $('#currency').val();
			var payment_method = $('#pmethod').val();

			console.log(payment_method);

			$currmethod = 'Razor';

			if (payment_method == 'Paypal') {
				$currmethod = 'Paypal';

				
			}

			

			if ($currmethod == 'Paypal') {
				console.log('hehehehe')
				
			}
			if ($currmethod == 'Razor') {
				console.log('hehehehekdfkhdjkh')
				
			}
			var question1 = $('#question1').val();
			var question2  = $('#question2').val();
			
			if(question2 == undefined){
				question2 = '';	
			}
			if(question1 == undefined){
				question1 = '';	
			}
			console.log(question2); 
			console.log(question1); 
	 
			/*	<--------<dksjjdsksdmksmdksdmv>*/
			console.log(document.cookie);		
			var full_cart_order_detail=[];
			var final_sum = 0;
	
			//var d = new Date();
			//d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
			//var expires = "expires=" + d.toGMTString();		
		
			console.log($('#consultantType').val());
			var consultantType = $('#consultantType').val();
			console.log(consultantType);
			
			var educationQualification = $('#educationQualification').val();
			var currentProfession = $('#currentProfession').val();
			var partnerName = $('#partnerName').val();
            var partnerDateDate = $('#partnerDateDate').val()+ ' ' + $('#partnerDateDate1').val() + ' ' + $('#partnerDateDate2').val();
            var partnerTimeMin = $('#partnerDateDate3').val()+ ' : ' + $('#partnerDateDate4').val();
			var partnerBirthCountry = $('#partnerBirthCountry').val();
			var partnerBirthState = $('#partnerBirthState').val();
			var partnerBirthCity = $('#partnerBirthCity').val();
			
            var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val()+ ' ' + $('#inputDateDate1').val() + ' ' + $('#inputDateDate2').val();
            var inputTimeMin = $('#inputDateDate3').val()+ ' : ' + $('#inputDateDate4').val();
			console.log(inputDateDate);
			console.log(inputTimeMin);
            //var inputTimeMin = $('#inputTimeMin').val();
			var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
			var birthCity = $('#birthCity').val();
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            var inputQuestion = $('#inputQuestion').val();
            var otherInfomation = $('#otherInfomation').val();
            var single_or_married = $('#single_or_married').val();
            var question_label = $('.question_label').text();
            var question1_label = $('.question1_label').text();
            var question2_label = $('.question2_label').text();
			
			var sercice_detail 	=[];	
			sercice_detail.push({
				question_label: question_label,
				question1_label: question1_label,
				question2_label: question2_label,
				consultantType: consultantType,
				educationQualification: educationQualification,
				currentProfession: currentProfession,
				partnerName: partnerName,
				partnerDateDate: partnerDateDate,
				partnerTimeMin: partnerTimeMin,
				partnerBirthCountry: partnerBirthCountry,
				partnerBirthState: partnerBirthState,
				partnerBirthCity: partnerBirthCity,
				inputName: inputName,
				inputGender: inputGender,
				inputDateDate: inputDateDate,
				inputTimeMin: inputTimeMin,
				birthCountry: birthCountry,
				birthState: birthState,
				birthCity: birthCity,
				inputEmail: inputEmail,
				inputContact: inputContact,
				inputQuestion: inputQuestion,
				otherInfomation: otherInfomation,
				question1:question1,
				question2:question2,
				single_or_married:single_or_married,
			});
			
			
			var service_id = parseFloat($('#service_id').val());
			var serviceCost = parseFloat($("input[name='radioBtnClass']:checked").attr('cost'));
			var servicediscount = parseFloat($("input[name='radioBtnClass']:checked").attr('d_price'));
			var serviced_price = parseFloat($('#serviced_price').val());
			var coupon_discount = parseFloat($('#coupon_discount').val());
			var coupon_discount_amount = parseFloat($('#coupon_discount_amount').val());
			var coupan_code = $('#inputcoupan').val();
		
			var obj = JSON.parse(localStorage.getItem('service_cart'));
			var keySize = 0;
			
			$.post(APP_URL+'welcome/get_services_name_id',{
				service_id : service_id,
			},function (response) {
				var data = response.data;
				
				var TID = $('#TID').val();
				var order_id = $('#order_id').val();
				var date  = $('#date').val();
				//var order_id  = data_murchant.razorpay_payment_id;
				var name  = inputName;
				var address  = '';
				var city  = birthCity;
				var state  = birthState;
				var country  = birthCountry; 

				var currency = $('#currency').val();
				var payment_method = $('#pmethod').val();
				var contectno  = inputContact;
				var servicename  = data;//$('.span_service').text();
				// var servicecost  = amount;
				var subtotal  = amount;
				var amount  = serviceCost;
				var discount  = servicediscount;
				var grandtotal  = 0;
				if(discount>0){
					grandtotal  = parseFloat(amount) - parseFloat(servicediscount);
				}else{
					grandtotal  = parseFloat(amount);
				}
				
				console.log('amount: '+amount);
				console.log('serviceCost: '+serviceCost);
				console.log('servicediscount: '+servicediscount);
				console.log('grandtotal: '+grandtotal);
				
				if(coupon_discount_amount >0){
					console.log('coupon_discount_amount: '+coupon_discount_amount);
					grandtotal = grandtotal - coupon_discount_amount;
				}
			
				console.log('coupon_discount_amount: '+coupon_discount_amount);
				console.log('servicediscount: '+servicediscount);
				console.log('amount: '+amount);
				console.log('grandtotal: '+grandtotal);
				console.log('subtotal: '+subtotal);
				console.log(currency);
			
				$.post(APP_URL+'welcome/service_order_buy_request_data',{    
					date : date,
					order_id : order_id,
					//payment_id : payment_id,
					service_id : service_id,
					consultantType : consultantType,
					address : address,
					city : city,
					state : state,
					country : country,
					payment_method : payment_method,
					currency : currency,
					contectno : contectno,
					servicename : servicename,
					servicecost : serviceCost,
					subtotal : amount,
					discount : discount,
					grandtotal : grandtotal,
					coupan_code : coupan_code,
					coupon_discount : coupon_discount,
					coupon_discount_amount : coupon_discount_amount,
				
					sercice_detail : sercice_detail,
					// sercice_detail_insatance : sercice_detail_insatance,
					
				},function (response) {
					$.unblockUI();
					console.log(response);
					var order_payment_id = response.data;
					// console.log(order_payment_id)
					if(response.status==200){
						console.log(response);
						console.log($currmethod);
						if($currmethod == 'ccavenu'){
							$.post(APP_URL+'welcome/convertdataproductservce',
							{    
							 //merchant_data : merchant_data,
									
							},function (response) {
								var data = response.data;
								console.log(data);
								
								$('#encRequest').val(data);

								$('#finalPayentSubmitBtn').trigger('click');
							
							},'json');	
						}else if($currmethod == 'Razor'){
							console.log('Razor');
							console.log(grandtotal);
							var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
							var paid_amount = grandtotal;
							console.log(paid_amount*100);
							console.log(order_payment_id);
							
							var options = {
								"key": razor_all_key, 
								"amount": (paid_amount*100), // 2000 paise = INR 20
								"name": "Maya Astrology",
								"description": "Maya Astrology Paid Consultation",
								"image": "",
								"captured": true,
								"handler": function (response){
									//alert(response.razorpay_payment_id);
									console.log(response);
									if(response){
										$.unblockUI();
										$.post(APP_URL+'welcome/paytest', {
											amount: (paid_amount*100),
											payment_id: response.razorpay_payment_id,
											razor_key: razor_all_key,
											seceret_key: seceret_key,
											order_payment_id: order_payment_id,
										},
										function (response2) {
											console.log(response2);
											
											var data = response2.data;
											console.log(data);
											window.location.href = APP_URL+'welcome/razorResponseHandlerservice/'+data;
											
										}, 'json');
									}
									
								},
								"prefill": {
									"name": inputName,
									"contact": inputContact,
									"email": inputEmail,
								},
								"notes": {
									"address": "Paid Consultation"
								},
								"theme": {
									"color": "#F37254"
								}
							};
							var rzp1 = new Razorpay(options);

							document.getElementById('rzp-button1').onclick = function(e){
								rzp1.open();
								e.preventDefault();
							}
							$('#rzp-button1').trigger('click');
							
						}else{
							if ($currmethod == 'Paypal') {
								console.log(order_payment_id);
								console.log(order_id);
								
								
						
						var paid_amount = grandtotal;
						// console.log('orderid' + order_id + 'amount' + paid_amount);

						window.location.href = APP_URL+'paypal/index?order_id='+order_payment_id+'&total_amount='+paid_amount+'&currency=USD'+'&page=service';

								
							}
 
						}
					}
						
				},'json');	
			
			
			},'json');
		
			//document.cookie = "allcart_orderss_details= "+JSON.stringify(full_cart_order_detail)+"; "+expires+";domain=;path=/";
			//console.log(document.cookie);
			//window.location.href = APP_URL+'welcome/finalcheckoutservice';
		}
	});
})