$('document').ready(function(){
	 //------------------------------------------------------------------------
    /*
     * This script is used to set the img_cat in modal
     */
	 var image_cat='';
    $('body').on('click', '.add_image', function () {
		image_cat = $(this).attr('name');
	});
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
        var bar = $('.bar');
		var percent = $('.percent');
		var status = $('#status');
		$('#upload_image').ajaxForm({
			dataType: 'JSON',
			beforeSend: function() {
				$('#status').empty();
				var percentVal = '0%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			uploadProgress: function(event, position, total, percentComplete) {
				var percentVal = percentComplete + '%';
				bar.width(percentVal);
				percent.html(percentVal);
			},
			complete: function(response) {
				var status= (response.responseText).split('status":')[1].split(',"')[0];
				if(status == '200'){
					var filename_ = (response.responseText).split('filename":"')[1].split('"}')[0];				
					if(image_cat=='popup_ads'){
						$(".img-preview").removeClass('display_none').addClass('display_inline');
						$(".img-preview").attr('src',APP_URL+'uploads/' + filename_);
						$('#popup_ads_image').val(filename_);
						$('#popup_ads_image-error').css({"display": "none"});
					}else if(image_cat=='consultation_image'){
						$(".img-preview_consultation").removeClass('display_none').addClass('display_inline');
						$(".img-preview_consultation").attr('src',APP_URL+'uploads/' + filename_);
						$('#free_consultation_image').val(filename_);
						$('#free_consultation_image-error').css({"display": "none"});
					}else if(image_cat=='gemstone_recommendation_image'){
						$(".img-preview_gemstone_recommendation").removeClass('display_none').addClass('display_inline');
						$(".img-preview_gemstone_recommendation").attr('src',APP_URL+'uploads/' + filename_);
						$('#free_gemstone_recommendation_image').val(filename_);
						$('#free_gemstone_recommendation_image-error').css({"display": "none"});
					}
					
					
					$('#browseImage').modal('hide');
					$("#myFile").val('');
				}else{
					alert('Invalid file');
					$('#browseImage').modal('hide');
				}
				$('.percent').html('0%');
				$.unblockUI();
				return false;
			}
		});
	
	//-----------------------------------------------------------------------
    /* 
     * validation of add aboutus
     */
	 $('#popup_ads_form').validate({
		ignore: [],
        rules: {
            popup_ads_image: {
                required: true,
            },
			navigation_link: {
                required: true,
            },
		},
		 messages: {
			popup_ads_image: {
                required: "Image  is required.",
            },
			navigation_link: {
                required: "Navigation Link is required.",
            },
		},
		submitHandler: function (form) {
			var popup_ads_id = $('#popup_ads_id').val();
            var popup_ads_image = $('#popup_ads_image').val();	
            var navigation_link = $('#navigation_link').val();	
            var popup_status = $('#popup_status').val();	
            var free_consultation_image = $('#free_consultation_image').val();	
            var free_gemstone_recommendation_image = $('#free_gemstone_recommendation_image').val();	
            	
           $.post(APP_URL + 'page/update_popup_ads', {
                popup_ads_id: popup_ads_id,
                popup_ads_image: popup_ads_image,
                navigation_link: navigation_link,
                popup_status: popup_status,
                free_consultation_image: free_consultation_image,
                free_gemstone_recommendation_image: free_gemstone_recommendation_image,
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headMsg').empty();
				if (response.status == 200) {
					$('#headMsg').html("<div class='alert alert-success fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
					$('#popup_ads_form').removeClass('display_block').addClass('display_none');
					$('.view_popup_ads_div').removeClass('display_none').addClass('display_block');
					
					$('.view_img-preview').attr('src',APP_URL+'uploads/'+popup_ads_image);
					$('#view_navigation_link').text(navigation_link);
					$('#view_popup_status').text(popup_status);
					$('.view_img-preview2').attr('src',APP_URL+'uploads/'+free_consultation_image);
					$('.view_img-preview3').attr('src',APP_URL+'uploads/'+free_gemstone_recommendation_image);
				}else{
					$('#headMsg').html("<div class='alert alert-danger fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				}
			}, 'json');
		return false;
		},
	 });
	 
	 //------------------------------------------------------------------------
    /*
     * This script is used to hide the view page and show edit page
     */
    $('body').on('click', '.edit_popup_ads', function () {
		$('.view_popup_ads_div').removeClass('display_block').addClass('display_none');
		$('#popup_ads_form').removeClass('display_none').addClass('display_block');
	});
	
	
});