$('document').ready(function () {

	

    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '#selcetAll', function () {
		$('.mcheckbox').prop('checked', true);
	});
    $('body').on('click', '#selcetAllPending', function () {
		$('.mcheckbox').prop('checked', false);
		$("#consultation_table tr").each(function(){
			if($(this).find('.appot_consul').attr('value') == 'pending'){
				$(this).find('.mcheckbox').prop('checked', true);				
			}
		})
	});
    $('body').on('click', '#selcetAllCompleted', function () {
		$('.mcheckbox').prop('checked', false);
		$("#consultation_table tr").each(function(){
			if($(this).find('.appot_consul').attr('value') == 'consultation-done'){
				$(this).find('.mcheckbox').prop('checked', true);				
			}
		})
	});
    $('body').on('click', '#selcetUncechekAll', function () {
		$('.mcheckbox').prop('checked', false);
	});
	
    $('body').on('click', '.view_consultation', function () {
		$('#consultation_form .form-group').removeClass('display_none');
        $('#err_edit_sub_menu_services').empty();
        $('#consultation_form label.error').empty();
        $('#consultation_form input.error').removeClass('error');
        $('#consultation_form select.error').removeClass('error');
		
        var consultation_id = $(this).attr('name');
        var consultantType = $(this).closest('tr').find('td:eq(2)').text();
        var full_name = $(this).closest('tr').find('td:eq(3)').text();
        var gender = $(this).closest('tr').find('td:eq(4)').attr('name');
        var date_of_birth = $(this).closest('tr').find('td:eq(5)').text();
        var date_of_time = $(this).closest('tr').find('td:eq(6)').text();
        //var inputEmail = $(this).closest('tr').find('td:eq(7)').text();
        var inputContact = $(this).closest('tr').find('td:eq(8)').text();
		
        var inputQuestion = $(this).attr('inputQuestion');
        var educationQualification = $(this).attr('educationQualification');
        var currentProfession = $(this).attr('currentProfession');
        var partnerName = $(this).attr('partnerName');
        var partner_date_of_birth = $(this).attr('partnerDateDate');
        var partner_date_of_time = $(this).attr('partnerTimeMin');
        var partnerBirthCountry = $(this).attr('partnerBirthCountry');
        var partnerBirthState = $(this).attr('partnerBirthState');
        var partnerBirthCity = $(this).attr('partnerBirthCity');
        var birthCountry = $(this).attr('birthCountry');
        var birthState = $(this).attr('birthState');
        var birthCity = $(this).attr('birthCity');
        var question1 = $(this).attr('question1');
        var question2 = $(this).attr('question2');
        var question_label = $(this).attr('question_label');
        var question1_label = $(this).attr('question1_label');
        var question2_label = $(this).attr('question2_label');
        var single_or_married = $(this).attr('single_or_married');
		
		/*
								'birthCountry' => $row->birthCountry,
								'birthState' => $row->birthState,
								'birthCity' => $row->birthCity,
										 FK_order_payment_id="' . $value['FK_order_payment_id'] . '" 
									
										 inputQuestion="' . $value['inputQuestion'] . '" 
										 otherInfomation="' . $value['otherInfomation'] . '" 
										 educationQualification="' . $value['educationQualification'] . '" 
										 currentProfession="' . $value['currentProfession'] . '"
										 partnerName="' . $value['partnerName'] . '"
										 partnerDateDate="' . $value['partnerDateDate'] . '"
										 partnerTimeMin="' . $value['partnerTimeMin'] . '"
										 partnerBirthCountry="' . $value['partnerBirthCountry'] . '"
										 partnerBirthState="' . $value['partnerBirthState'] . '"
										 partnerBirthCity="' . $value['partnerBirthCity'] . '"
										 single_or_married="' . $value['single_or_married'] . '"
										 question1="' . $value['question1'] . '"
										 question2="' . $value['question2'] . '"
										 match_count="' . $value['match_count'] . '"
										 
		*/
        $('#consultantType').text(consultantType);
        $('#full_name').text(full_name);
        $('#gender').text(gender);
        $('#date_of_birth').text(date_of_birth);
        $('#date_of_time').text(date_of_time);
        $('#birthCountry').text(birthCountry);
        $('#birthState').text(birthState);
        $('#birthCity').text(birthCity);
        //$('#inputEmail').text(inputEmail);
        $('#inputContact').text(inputContact);
        $('#inputQuestion').text(inputQuestion);
        $('.question_label').text(question_label);
        
		if(question1){
			$('#question1').text(question1);
			$('.question1_label').text(question1_label);
		}else{
			$('#question1').parent().parent().removeClass('display_block').addClass('display_none');
		}
		if(question2){
			$('#question2').text(question2);
			$('.question2_label').text(question2_label);
		}else{
			$('#question2').parent().parent().removeClass('display_block').addClass('display_none');
		}
		if(educationQualification || educationQualification != 'null'){
			$('#educationQualification').text(educationQualification);
			$('#currentProfession').text(currentProfession);
		}else{
			$('#educationQualification').parent().parent().removeClass('display_block').addClass('display_none');
			$('#currentProfession').parent().parent().removeClass('display_block').addClass('display_none');
		}
		if(partnerName){
			$('#single_or_married').text(single_or_married);
			$('#partnerName').text(partnerName);
			$('#partner_date_of_birth').text(partner_date_of_birth);
			$('#partner_date_of_time').text(partner_date_of_time);
			$('#partnerBirthCountry').text(partnerBirthCountry);
			$('#partnerBirthState').text(partnerBirthState);
			$('#partnerBirthCity').text(partnerBirthCity);
		}else{
			$('#single_or_married').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partnerName').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partner_date_of_birth').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partner_date_of_time').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partnerBirthCountry').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partnerBirthState').parent().parent().removeClass('display_block').addClass('display_none');
			$('#partnerBirthCity').parent().parent().removeClass('display_block').addClass('display_none');
		}
		/*
		$('#educationQualification').parent().parent().removeClass('display_block').addClass('display_none');
        $('#currentProfession').parent().parent().removeClass('display_block').addClass('display_none');
        
		//if(consultantType=='Career'){
			$('#educationQualification').parent().parent().removeClass('display_none').addClass('display_block');
			$('#currentProfession').parent().parent().removeClass('display_none').addClass('display_block');
		//}
		//if(consultantType=='MatchMaking' || consultantType=='marriage' || consultantType =='love' ){
			$('#partnerName').parent().parent().removeClass('display_none').addClass('display_block');
			$('#partner_date_of_birth').parent().parent().removeClass('display_none').addClass('display_block');
			$('#partner_date_of_time').parent().parent().removeClass('display_none').addClass('display_block');
			$('#partnerBirthCountry').parent().parent().removeClass('display_none').addClass('display_block');
			$('#partnerBirthState').parent().parent().removeClass('display_none').addClass('display_block');
			$('#partnerBirthCity').parent().parent().removeClass('display_none').addClass('display_block');
		//}
		*/
    });

    
    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_consultation', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var consultation_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'page/remove_consultation', {consultation_id: consultation_id}, function (response) {
            $('#headerMsg').empty();
			//$("html, body").animate({scrollTop: 0}, "slow");
            if (response.status == 200) {
                $('#headerMsg').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_consultation[name=' + consultation_id + ']').closest("tr").remove();
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

	
		//-----------------------------------------------------------------------
    /* 
     * Work for sendind sms */
	 var ids = [];
	 var numbrs = [];
	 var emails_all = [];
	 var Names = [];
	 
	$('body').on('click', '#read_foo', function () {
		var for_what = $(this).attr('for_what');
		ids = [];
		numbrs =[];
		emails_all =[];
		$("#consultation_table input:checkbox[name=foo]:checked").each(function(){
			ids.push($(this).attr('consultation_id'));
			numbrs.push($(this).attr('inputContact'));
			emails_all.push($(this).attr('inputEmail'));
			Names.push($(this).attr('full_name'));
		});
		console.log(ids);
		console.log(numbrs);
		console.log(emails_all);
		console.log(Names);
		if(ids != ''){
			$('#for_what').val(for_what);
			if(for_what == 'SMS'){
				$('.pop_head').html('Send SMS');
			}else{
				$('.pop_head').html('Send Email');
			}
			$('.count_cunsultants').html('You select '+(ids.length)+' consultants');
			$('#browseNewCategory').modal('show');
		}
	});
	 

	$('#category_form').validate({
		ignore: [],
        rules: {
            for_what: {
                required: true,
            },
			typend_msg: {
                required: true,
            },
        },
		 messages: {
			for_what: {
                required: "Message is required.",
            },
			typend_msg: {
                required: "Message is required.",
            },
		},
		submitHandler: function (form) {
			$.blockUI();
			
			//var ids = ids;
            var for_what = $('#for_what').val();
            var typend_msg = $('#typend_msg').val();
			if(for_what =='SMS'){
				$.post(APP_URL + 'page/update_all_sms_consultants', {
					category_id: ids,
					typend_msg: typend_msg,
				},
				function (response) {
					
					$('#headerMsg').empty();
					if (response.status == 200) {
						var message = response.message;
						
						 
					  var i = 0;
					  for(i=0;i< ids.length;i++){
						var firstWord = Names[i].replace(/ .*/,'');  
						var content = firstWord+'(Code:'+ids[i]+') '+typend_msg+'';
						var no = numbrs[i];
						$.get('https://api.msg91.com/api/sendhttp.php?sender=MAYASA&route=4&mobiles='+no+'&authkey=220059AldeaakXZh5b1f4d70&country=91&message='+content+'&response=&campaign=', {
								genrate_otp_mobile: '',},
							   function (response) {
								   console.log('Sent');
								}, 'json');	
						  
						}
						
						
						
						
					}
					$("#typend_msg").text('');
					$("#typend_msg").val('');
					$('#browseNewCategory').modal('hide');
					//$("html, body").animate({scrollTop: 0}, "slow");
					$.unblockUI();
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#consultation_table input:checkbox[name=foo]:checked").prop("checked" , false);
					
				}, 'json');
			
			}else{
				var coming_true = 0;
					$('#headerMsg').empty();
						
						 
					  var i = 0;
					  for(i=0;i< emails_all.length;i++){
						   var msg = typend_msg;
							$.post(APP_URL + 'page/update_all_email_consultants', {
								full_name: Names[i],
								email: emails_all[i],
								id: ids[i],
								typend_msg: msg,
							},
							function (response) {
								if (response.status == 200) {
									console.log(response.message);
									 coming_true++; 
								}
							}, 'json');
					
						  
						}
						
						
						
						
					
					$("#typend_msg").text('');
					$("#typend_msg").val('');
					$('#browseNewCategory').modal('hide');
					//$("html, body").animate({scrollTop: 0}, "slow");
					
					
					setInterval(function(){
						if(coming_true == emails_all.length){
							$.unblockUI();
							$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>Email has been to all selected consultants successfully</strong></div>");
							$("#consultation_table input:checkbox[name=foo]:checked").prop("checked" , false);
							coming_true++;
						}
					}, 1000);
					
				
			}
		return false;
		},
	});

	
	
	//------------------------------------------------------------------------
    /*
     * This script is used to Change Status
     */
	 var my_this ='';
    $('body').on('click', '.appot_consul', function () {		
        my_this = this ;
		$('#headerMsg').empty();
		$('#category_form2 label.error').empty();
        $('#category_form2 input.error').removeClass('error');
        $('#category_form2 select.error').removeClass('error');
		
        var category_id = $(this).attr('name');
        var category_status = $(this).attr('value');	
        
		$('#category_id2').val(category_id);
        $('#category_status option[value='+category_status+']').prop('selected' , true);
	});
	

	$('#category_form2').validate({
		ignore: [],
        rules: {
            category_status: {
                required: true,
            },
        },
		 messages: {
			category_status: {
                required: "Category Status is required.",
            },
		},
		submitHandler: function (form) {
			var category_id = $('#category_id2').val();
            var category_status = $('#category_status').val();
			
				change_status_execute();
			
		},
	});
	
	function change_status_execute(){
		var category_id = $('#category_id2').val();
		var category_status = $('#category_status').val();
		
		
		$.post(APP_URL + 'page/update_status_of_appointment', {
			consultation_id: category_id,
			category_status: category_status,
		},
		function (response) {
			//$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {
				if(category_status == 'consultation-done'){
					$(my_this).find('span').removeClass('label-default');
					$(my_this).find('span').removeClass('label-danger');
					$(my_this).find('span').addClass('label-success');
				}else if(category_status == 'non-eligible'){
					$(my_this).find('span').removeClass('label-success');
					$(my_this).find('span').removeClass('label-default');
					$(my_this).find('span').addClass('label-danger');
				}else{
					$(my_this).find('span').removeClass('label-success');
					$(my_this).find('span').removeClass('label-danger');
					$(my_this).find('span').addClass('label-default');
				}
				$(my_this).find('span').html(category_status);
				$(my_this).find('span').attr('value' ,category_status);
				
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");	
				
			}else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
			}
			$("#category_id2").val(0);
			$("#category_status").val('');
			$('#browseChangeStatus').modal('hide');
		
			
			
		}, 'json');
		return false;
	}
	
	
	
	
	/*
     * This script is used to Add new Note
     */
	 $('body').on('click', '.add_note_consultation', function () {	
		$('#headerMsg').empty();
		
        var category_id = $(this).attr('name');
        var name = $(this).attr('value');
        
		$('#c_id').val(category_id);
		$('.name_cunsultants').html('Note For '+name);
		$('#browseNewNote').modal('show');
	});
	
	$('#note_form').validate({
		ignore: [],
        rules: {
            typed_note: {
                required: true,
            },
        },
		 messages: {
			typed_note: {
                required: "Note is required.",
            },
		},
		submitHandler: function (form) {
			var c_id = $('#c_id').val();
            var typed_note = $('#typed_note').val();
			
				$.post(APP_URL + 'page/update_notes_of_consutatnts', {
					consultation_id: c_id,
					typed_note: typed_note,
					},
					function (response) {
					//$("html, body").animate({scrollTop: 0}, "slow");
					$('#headerMsg').empty();
					if (response.status == 200) {
						var message = response.message;
						
						$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");	
						
					}else if (response.status == 201) {
						$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
					$("#c_id").val(0);
					$('#browseNewNote').modal('hide');
				
					
					
				}, 'json');
				return false;
			
		},
	});
	
	/*
     * This script is used to Add new Note
     */
	$('body').on('click', '.view_note_consuion', function () {	
		$('#headerMsg').empty();
		$('#headMsg25').empty();
		$('#all_nota_').empty();
		$('.msg_cunsultants').empty();
		var consultation_id = $(this).attr('name');
        var name = $(this).attr('value');
        $('.name_noter').html(name);
		
		$.post(APP_URL + 'page/get_all_notes_of_consutatnt_id', {
			consultation_id: consultation_id,
			},
			function (response) {
				if (response.status == 200) {
					var value = response.data;
					var i =0;
					var content = '';
					for(i=0;i < value.length;i++){
						content +='<p style="width: 100%;background: #fff;padding: 5px;border-radius: 5px;" >'+value[i]['content']+'</p>';
					}
					$('#all_nota_').html(content);
					$('.msg_cunsultants').html('Notes For '+name);
					$('#browseAllNotes').modal('show');
				}else{
					$('#headMsg25').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>No Notes For "+name+"</strong></div>");
					$('#browseAllNotes').modal('show');
				}
			}, 'json');
		return false;
	});
})