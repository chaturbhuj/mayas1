$('document').ready(function () {
	$('body').on('click', '.remove_vastu_consultation', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var consultation_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'page/remove_vastu_consultation', {consultation_id: consultation_id}, function (response) {
            $('#headerMsg').empty();
			//$("html, body").animate({scrollTop: 0}, "slow");
            if (response.status == 200) {
                $('#headerMsg').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_vastu_consultation[name=' + consultation_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
				        $('#headerMsg').remove();
						
						window.location.href = APP_URL+'page/vastu_consultations';
					});
            }
            else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
   $('body').on('click', '.vastu_consultation', function () {
		$('#consultation_form .form-group').removeClass('display_none');
        $('#err_edit_sub_menu_services').empty();
        $('#consultation_form label.error').empty();
        $('#consultation_form input.error').removeClass('error');
        $('#consultation_form select.error').removeClass('error');
		 
		 //var consultation_id = $(this).attr('name');
        var consultantType = $(this).closest('tr').find('td:eq(2)').text();
        var propertydirection = $(this).closest('tr').find('td:eq(4)').text();
        var propertyaddress = $(this).attr('propertyaddress');
        var consultationcode = $(this).closest('tr').find('td:eq(1)').text();
        var inputName = $(this).closest('tr').find('td:eq(3)').text();
        var vastufile = $(this).closest('tr').find('td:eq(5)').html();
		// var gender = $(this).closest('tr').find('td:eq(3)').text();
		var inputDateDate = $(this).closest('tr').find('td:eq(6)').text();
        var birthCountry = $(this).closest('tr').find('td:eq(7)').text();
		var birthState = $(this).closest('tr').find('td:eq(8)').text();
        var birthCity = $(this).closest('tr').find('td:eq(9)').text();
		var inputEmail = $(this).closest('tr').find('td:eq(10)').text();
		var inputContact = $(this).closest('tr').find('td:eq(11)').text();


		// var consultantType = $(this).closest('tr').find('td:eq(1)').text();
        // var inputName = $(this).closest('tr').find('td:eq(2)').text();
		// var gender = $(this).closest('tr').find('td:eq(3)').text();
		// var inputDateDate = $(this).closest('tr').find('td:eq(4)').text();
        // var birthCountry = $(this).closest('tr').find('td:eq(5)').text();
		// var birthState = $(this).closest('tr').find('td:eq(6)').text();
        // var birthCity = $(this).closest('tr').find('td:eq(7)').text();
		// var inputEmail = $(this).closest('tr').find('td:eq(8)').text();
		// var inputContact = $(this).closest('tr').find('td:eq(9)').text();
        
		
		
       // console.log(inputName);
		$('#consultantType').text(consultantType);
        $('#full_name').text(inputName);
        $('#propertyaddress').text(propertyaddress);
        $('#propertydirection').text(propertydirection);
        $('#consultantcode').text(consultationcode);
		$('#date_of_birth').text(inputDateDate);
        $('#birthCountry').text(birthCountry);
		$('#birthState').text(birthState);
        $('#birthCity').text(birthCity);
        $('#email_id').text(inputEmail);
		$('#inputContact').text(inputContact);
		$('#vastufile').html(vastufile);

   	
    });
	
	//------------------------------------------------------------------------
    /*
     * This script is used to Change Status
     */
	 var my_this ='';
    $('body').on('click', '.vastu_consult', function () {		
        my_this = this ;
		$('#headerMsg').empty();
		$('#category_form2 label.error').empty();
        $('#category_form2 input.error').removeClass('error');
        $('#category_form2 select.error').removeClass('error');
		
        var category_id = $(this).attr('name');
        var category_status = $(this).attr('value');	
        
		$('#category_id2').val(category_id);
        $('#category_status option[value='+category_status+']').prop('selected' , true);
		console.log(category_id);
	});
	$('#vastu_form2').validate({
		ignore: [],
        rules: {
            category_status: {
                required: true,
            },
        },
		 messages: {
			category_status: {
                required: "Category Status is required.",
            },
		},
		submitHandler: function (form) {
			
	
		var category_id = $('#category_id2').val();
		var category_status = $('#category_status').val();
		console.log(category_id);
		
		$.post(APP_URL + 'page/update_vastu_status_of_consultation', {
			vastu_id: category_id,
			appointment_yes: category_status,
		},
		function (response) {
			//$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {
				if(category_status == 'consultation-done'){
					$(my_this).find('span').removeClass('label-default');
					$(my_this).find('span').removeClass('label-danger');
					$(my_this).find('span').addClass('label-success');
				}else if(category_status == 'non-eligible'){
					$(my_this).find('span').removeClass('label-success');
					$(my_this).find('span').removeClass('label-default');
					$(my_this).find('span').addClass('label-danger');
				}else{
					$(my_this).find('span').removeClass('label-success');
					$(my_this).find('span').removeClass('label-danger');
					$(my_this).find('span').addClass('label-default');
				}
				$(my_this).find('span').html(category_status);
				$(my_this).find('span').attr('value' ,category_status);
				
				$('#headerMsg1').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");	
				$("#headerMsg1").fadeTo(3000, 500).slideUp(500, function(){
				        $('#headerMsg1').remove();
						$('.vastu_consult').modal('hide');
						window.location.href = APP_URL+'page/vastu_consultations';
					});
			}else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
			}
			$("#category_id2").val(0);
			$("#category_status").val('');
			$('.vastu_consult').modal('hide');
		
			
			
		}, 'json');
		return false;
	}
   
});
});



