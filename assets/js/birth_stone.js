$('document').ready(function () {

	//-------------------------------------------------------------------------
    /* 
     *  Only char validation      
     */
    $.validator.addMethod("charOnly", function (value, element) {
        return this.optional(element) || /^[A-Za-z\s]*$/.test(value)
    });

    //-----------------------------------------------------------------------
    /* 
     * addMenuForm validation
     */
    $('#addBirthStoneForm').validate({
        ignore: [],
        rules: {
            birth_stone_name: {
                required: true,
                charOnly: true
            },
            birth_stone_navi: {
                required: true,
            },
			
			main_description:{
				required: true,
			},
			description:{
				 required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
			}

        },
        messages: {
            birth_stone_name: {
                required: "Birth Stone Name is required",
                charOnly: "Only characters are allowed."
            },
            birth_stone_navi: {
                required: "Birth Stone Navigation URL is required",
            },
			main_description:{
				 required: "Services Main Description is required",
			},
			
			description:{
				 required: "Main Description is required",
			}
			
        },
        submitHandler: function (form) {
            var birth_stone_name = $('#birth_stone_name').val();
            var birth_stone_navi = $('#birth_stone_navi').val();
            var main_description = $('#main_description').val();
            var birth_stone_id = $('#birth_stone_id').val();
            var description = CKEDITOR.instances['description'].getData();
            

            $.post(APP_URL + 'configure_access/add_new_birth_stone', {
                birth_stone_name: birth_stone_name,
                birth_stone_navi: birth_stone_navi,
                main_description: main_description,
                description: description,
                birth_stone_id: birth_stone_id,
                
               },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#err_birth_stone_form').empty();
                if (response.status == 200) {
                    $('#err_birth_stone_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                else if (response.status == 201) {
                    $('#err_birth_stone_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
                $('#birth_stone_name').val('');
                $('#birth_stone_navi').val('');
                $('#main_description').val('');
                $('#description').val('');
                
            }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit birth_stone
     */

    $('body').on('click', '.edit_birth_stone', function () {
        $('#err_edit_birth_stone').empty();
        $('#edit_birth_stone_form label.error').empty();
        $('#edit_birth_stone_form input.error').removeClass('error');
        $('#edit_birth_stone_form select.error').removeClass('error');

        var edit_birth_stone_id = $(this).attr('name');
        var edit_birth_stone_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_birth_stone_navi = $(this).closest('tr').find('td:eq(2)').text();

        $('#edit_birth_stone_id').val(edit_birth_stone_id);
        $('#edit_birth_stone_name').val(edit_birth_stone_name);
        $('#edit_birth_stone_navi').val(edit_birth_stone_navi);
    });

    //------------------------------------------------------------------------
    /* 
     * edit_birth_stone_form validation
     */
    $('#edit_birth_stone_form').validate({
        rules: {
            edit_birth_stone_name: {
                required: true,
                charOnly: true,
            },
            edit_birth_stone_navi: {
                required: true,               
            }
        },
        messages: {
            edit_birth_stone_name: {
                required: "Birth Stone name is required",
                charOnly: "Only characters are allowed.",
            },
            edit_birth_stone_navi: {
                required: "Birth Stone Navigation is required"
            }
        },
        submitHandler: function (form) {
            var edit_birth_stone_id = $('#edit_birth_stone_id').val();
            var edit_birth_stone_name = $('#edit_birth_stone_name').val();
            var edit_birth_stone_navi = $('#edit_birth_stone_navi').val();

            $.post(APP_URL + 'configure_access/edit_birth_stone_information', {
                edit_birth_stone_id: edit_birth_stone_id,
                edit_birth_stone_name: edit_birth_stone_name,
                edit_birth_stone_navi: edit_birth_stone_navi,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_edit_birth_stone').empty();
                        if (response.status == 200) {
                            $('#my_user_edit').modal('hide');
                            $('#edit_user_table').show();
                            $('#err_edit_birth_stone').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                            $('.edit_birth_stone[name=' + edit_birth_stone_id + ']').closest("tr").find("td:eq(1)").text(edit_birth_stone_name);
                            $('.edit_birth_stone[name=' + edit_birth_stone_id + ']').closest("tr").find("td:eq(2)").text(edit_birth_stone_navi);

                        } else if (response.status == 201) {
                            $('#err_edit_birth_stone').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                    }, 'json');
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_birth_stone', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_birth_stone_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_birth_stone', {edit_birth_stone_id: edit_birth_stone_id}, function (response) {
            $('#err_edit_birth_stone').empty();
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
            if (response.status == 200) {
                $('#err_edit_birth_stone').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.edit_birth_stone[name=' + edit_birth_stone_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_birth_stone').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})