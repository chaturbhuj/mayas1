$('document').ready(function () {

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseBannerImage').modal('hide');
                $('#banner_image').val(response.filename);
                //$('#services_image-error').css({"display": "none"});
            } else {
                $('#browseBannerImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

  //-----------------------------------------------------------------------
    /* 
     * admin_add_new_banner_image_form validation
     */
    $('#add_banner_image_form').validate({
		ignore: [],
        rules: {
			banner_page_type: {
                required: true
            },
			banner_image: {
                required: true
            },
		},
		messages: {
			 banner_page_type: {
                required: "Banner Page Name is required"
            },
			 banner_image: {
                required: "Banner Image is required"
            },
			
		},
		submitHandler: function (form) {
			var banner_page_type = $('#banner_page_type').val();
            var banner_image = $('#banner_image').val();
			
			$.post(APP_URL + 'configure_access/add_new_banner_image', {
				banner_page_type: banner_page_type,
                banner_image: banner_image,
			},
			function (response) {
				 $("html, body").animate({scrollTop: 0}, "slow");
                 $('#err_banner_image_form').empty();
				 if (response.status == 200) {
					$('#err_banner_image_form').html("<div class='alert alert-success fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				}else if (response.status == 201) {
					$('#err_banner_image_form').html("<div class='alert alert-danger fade in'>\n\
					<button class='close' type='button' data-dismiss='alert'>x</button>\n\
					<strong>" + response.message + "</strong></div>");
				}
				 $('#banner_page_type').val('');
                 $('#banner_image').val('');
				 $('.img-preview').attr('src','');
				
			},'json');
			return false;
		},
	});
	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit banner Image.
     */
    var edit_banner_image_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#banner_image_id').val($(this).attr('name'));
        edit_banner_image_id_for_image = $(this).attr('name');
    });
	
	//------------------------------------------------------------------------//
    /**
     * This part of script is used to upload banner image for edit section
     */
    $('#edit_upload_image').ajaxForm({
		dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
				$('.editBrowseImageBtn[name=' + edit_banner_image_id_for_image + ']').closest("tr").find(".banner_image_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit banner image detail
     */

    $('body').on('click', '.edit_banner_image_detail', function () {
        $('#err_edit_banner_images').empty();
        $('#edit_banner_image_form label.error').empty();
        $('#edit_banner_image_form input.error').removeClass('error');
        $('#edit_banner_image_form select.error').removeClass('error');

        var edit_banner_image_id = $(this).attr('name');
        var edit_banner_page_type = $(this).closest('tr').find('td:eq(1)').text();
		

		$('#edit_banner_image_id').val(edit_banner_image_id);
        $('#edit_banner_page_type').val(edit_banner_page_type);
	});
	
	//------------------------------------------------------------------------
    /* 
     * This function is used to call for edit banner image information in database
     */
    $('#edit_banner_image_form').validate({
		 rules: {
            edit_banner_page_type: {
                required: true
            },
		},
		messages: {
            edit_banner_page_type: {
                required: "Banner Page Type is required"
            },
		},
		onkeyup: false,
		submitHandler: function (form) {
			var edit_banner_image_id = $('#edit_banner_image_id').val();
            var edit_banner_page_type = $('#edit_banner_page_type').val();
			
			$.post(APP_URL + 'configure_access/edit_banner_image_information', {
				edit_banner_image_id: edit_banner_image_id,
                edit_banner_page_type: edit_banner_page_type,
				
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#my_banner_image_edit').modal('hide');
                $('#err_edit_banner_images').empty();
				if (response.status == 200) {
                    $('#err_edit_banner_images').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_banner_image_detail[name=' + edit_banner_image_id + ']').closest("tr").find("td:eq(1)").text(edit_banner_page_type);
                    
                } else if (response.status == 201) {
                    $('#err_edit_banner_images').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
				
			},'json');

		return false;
		}
	});
	
	 //---------------------------------------------------------------------
    /*
     * This script is used to remove banner Image Information from the list
     */

    $('body').on('click', '.remove_banner_image', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var banner_image_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_banner_image', {banner_image_id: banner_image_id}, function (response) {
            $('#err_edit_banner_images').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_banner_images').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_banner_image[name=' + banner_image_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_banner_images').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

})