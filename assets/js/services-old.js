$('document').ready(function () {

    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage').modal('hide');
                $('#services_image').val(response.filename);
                $('#services_image-error').css({"display": "none"});
            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#add_services_form').validate({
        ignore: [],
        rules: {
            services_name: {
                required: true
            },
            services_type: {
                required: true
            },
            services_main_description: {
                required: true
            },
			consultant_type: {
                required: true
            },
			number_of_questions: {
                required: true
            },
			cost: {
                required: true
            },
			premium_type: {
                required: true
            },
			number_of_questions_2: {
                required: true
            },
			cost_2: {
                required: true
            },
			
			
            services_description: {
                required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
            },
            services_image: {
                required: true
            }

        },
        messages: {
            services_name: {
                required: "Services Name is required"
            },
            services_type: {
                required: "Services Type is required"
            },
            services_main_description: {
                required: "Services Main Description is required"
            },
			cost: {
                required: "Cost is required"
            },
			consultant_type: {
                required: "Consultant Type is required"
            },
			number_of_questions: {
                required: "Number Of Questions is required"
            },
			premium_type: {
                required: "Premium Type is required"
            },
			number_of_questions_2: {
                required: "Number Of Questions is required"
            },
			cost_2: {
                required: "Cost is required"
            },
			
			
            services_description: {
                required: "Services Description is required"
            },
            services_image: {
                required: "Services Image is required"
            }
        },
        submitHandler: function (form) {
            var services_name = $('#services_name').val();
            var services_type = $('#services_type').val();
            var services_main_description = $('#services_main_description').val();
            var consultant_type = $('#consultant_type').val();
            var number_of_questions = $('#number_of_questions').val();
            var cost = $('#cost').val();
            var services_description = CKEDITOR.instances['services_description'].getData();
            var services_image = $('#services_image').val();
			var service_id = $('#service_id').val();
			var cost_2 = $('#cost_2').val();
			var number_of_questions_2 = $('#number_of_questions_2').val();
			var premium_type = $('#premium_type').val();
			
            $.post(APP_URL + 'configure_access/add_new_services', {
                services_name: services_name,
                services_type: services_type,
                consultant_type: consultant_type,
                number_of_questions: number_of_questions,
                cost: cost,
                services_main_description: services_main_description,
                services_description: services_description,
                services_image: services_image,
				service_id: service_id,
				premium_type: premium_type,
				number_of_questions_2: number_of_questions_2,
				cost_2: cost_2,
            },
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_services_form').empty();
						var mesg;
                        if (response.status == 200) {
							mesg = response.message;
							
							if(service_id != 0){
								mesg = 'services has been updated successfully';
							}
                            $('#err_services_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + mesg + "</strong></div>");
					 $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						alert(response.message);
						//window.location.href= APP_URL+'configure_access/edit_services';
                        }
                        else if (response.status == 201) {
                            $('#err_services_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#services_name').val('');
                        $('#services_main_description').val('');
                        $('#services_description').val('');
                        $('#services_image').val('');
                        $('#services_type').val('');
                        $('#consultant_type').val('');
                        $('#number_of_questions').val('');
                        $('#cost').val('');
						$('.img-preview').attr('src');
                    }, 'json');
            return false;
        }
    });


    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */

    $('body').on('click', '.edit_services', function () {
        $('#err_edit_services').empty();
        $('#edit_services_form label.error').empty();
        $('#edit_services_form input.error').removeClass('error');
        $('#edit_services_form select.error').removeClass('error');

        var edit_services_id = $(this).attr('name');
        var edit_services_name = $(this).closest('tr').find('td:eq(1)').text();
        var edit_services_type = $(this).closest('tr').find('td:eq(2)').text();
        var edit_services_main_description = $(this).closest('tr').find('td:eq(3)').text();
        var edit_services_description = $(this).closest('tr').find('td:eq(4)').text();

        $('#edit_services_id').val(edit_services_id);
        $('#edit_services_name').val(edit_services_name);
		$('#edit_services_type').val(edit_services_type).attr("selected", "selected");
        $('#edit_services_main_description').val(edit_services_main_description);
        $('#edit_services_description').val(edit_services_description);

    });

    //------------------------------------------------------------------------
    /* 
     * This function is used to call for edit result information
     */
    $('#edit_services_form').validate({
        rules: {
            edit_services_name: {
                required: true
            },
			edit_services_type: {
                required: true
            },
			edit_services_main_description: {
                required: true
            },
			edit_services_description: {
                 required: true
            },
        },
        messages: {
            edit_services_name: {
                required: "Service Name is required"
            },
			edit_services_type: {
                required: "Service Type is required"
            },
			edit_services_main_description: {
                required: "Service Main Description is required"
            },
			edit_services_description: {
                required: "Service Description is required"
            },
        },
        onkeyup: false,
        submitHandler: function (form) {
            var edit_services_id = $('#edit_services_id').val();
            var edit_services_name = $('#edit_services_name').val();
            var edit_services_type = $('#edit_services_type').val();
            var edit_services_main_description = $('#edit_services_main_description').val();
            var edit_services_description = $('#edit_services_description').val();

            $.post(APP_URL + 'configure_access/edit_services_information', {
                edit_services_id: edit_services_id,
                edit_services_name: edit_services_name,
                edit_services_type: edit_services_type,
                edit_services_main_description: edit_services_main_description,
                edit_services_description: edit_services_description,
            },
            function (response) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#my_user_edit').modal('hide');
                $('#err_edit_services').empty();
                if (response.status == 200) {
                    $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(1)").text(edit_services_name);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(2)").text(edit_services_type);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(3)").text(edit_services_main_description);
                    $('.edit_services[name=' + edit_services_id + ']').closest("tr").find("td:eq(4)").text(edit_services_description);

					 $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						alert(response.message);
						window.location.href= APP_URL+'configure_access/add_services';
					
					
                } else if (response.status == 201) {
                    $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                }
            }, 'json');
            return false;
        }
    });

    //------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit user
     */
    var edit_service_id_for_image = 0;
    $('body').on('click', '.editBrowseImageBtn', function () {
        $('#image_service_id').val($(this).attr('name'));
        edit_service_id_for_image = $(this).attr('name');
    });
    //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for edit section
     */
    $('#edit_upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $('.editBrowseImageBtn[name=' + edit_service_id_for_image + ']').closest("tr").find(".service_photo").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#editBrowseImage').modal('hide');
            } else {
                $('#editBrowseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_services', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var edit_service_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_services', {edit_service_id: edit_service_id}, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_services[name=' + edit_service_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });


})