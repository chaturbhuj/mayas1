$('document').ready(function(){
	
	/* 
	1) footer must have this html code 
	<div style="display:none;" class="footer_cart"><ul class="product_cart_info" style="display:none"></ul></div>
	
	2) Shopping cart must have this class on table tbody tag
	.cart_body
	
	3) Header and shopping cart where we need to show cart number, must have this class
	.cart_number
	
	3) Header and shopping cart where we need to total cart price, must have this class
	.cart_total_price
	*/
	import_cookie_into_li();
	import_in_cart_table();
	
	$('body').on('click','.addtocart',function() {
	
		
		var currency_symbol = $(this).attr('currency_symbol');
		
		var product_treatment = $(this).attr('product_treatment');
		var product_weight = $(this).attr('product_weight');
		var product_name = $(this).attr('product_name');
		var status =$(this).attr('status');

		var product_category = $(this).attr('product_category');
		var product_rashi = $(this).attr('product_rashi');
		var product_disease = $(this).attr('product_disease');
		var product_free_consultation = $(this).attr('product_free_consultation');
		var product_paid_consultation = $(this).attr('product_paid_consultation');
		var product_short_description = $(this).attr('product_short_description');
		var product_long_description = $(this).attr('product_long_description');
		
		var is_new = $(this).attr('is_new');
		var product_meta_title = $(this).attr('product_meta_title');
		var product_actual_prize = parseFloat($(this).attr('product_actual_prize'));
		var product_discounted_prize = parseFloat($(this).attr('product_discounted_prize'));
		var quantity = parseInt($(this).attr('quantity'));
		var product_images = ($(this).attr('product_images'));
		var product_meta_description = ($(this).attr('product_meta_description'));
		var product_meta_keyword = ($(this).attr('product_meta_keyword'));
		var product_meta_tags = ($(this).attr('product_meta_tags'));
		var product_feature = ($(this).attr('product_feature'));
		var product_height = ($(this).attr('product_height'));
		var product_shape = ($(this).attr('product_shape'));
		var product_color = ($(this).attr('product_color'));
		var product_clarity = ($(this).attr('product_clarity'));
		var product_grade = ($(this).attr('product_grade'));
		var product_origin = ($(this).attr('product_origin'));
		var string_url = ($(this).attr('string_url'));
		var product_id = parseInt($(this).attr('product_id'));
		
		var footer_li_cart = '<li>\n\
							<span class="product_treatment">'+product_treatment+'</span>\n\
							<span class="product_id">'+product_id+'</span>\n\
							<span class="product_origin">'+product_origin+'</span>\n\
							<span class="product_grade">'+product_grade+'</span>\n\
							<span class="product_clarity">'+product_clarity+'</span>\n\
							<span class="product_color">'+product_color+'</span>\n\
							<span class="product_shape">'+product_shape+'</span>\n\
							<span class="product_height">'+product_height+'</span>\n\
							<span class="product_feature">'+product_feature+'</span>\n\
							<span class="product_meta_tags">'+product_meta_tags+'</span>\n\
							<span class="product_meta_keyword">'+product_meta_keyword+'</span>\n\
							<span class="quantity">'+quantity+'</span>\n\
							<span class="product_meta_description">'+product_meta_description+'</span>\n\
							<span class="product_images">'+product_images+'</span>\n\
							<span class="product_discounted_prize">'+product_discounted_prize+'</span>\n\
							<span class="product_actual_prize">'+product_actual_prize+'</span>\n\
							<span class="currency_symbol">'+currency_symbol+'</span>\n\
							<span class="product_meta_title">'+product_meta_title+'</span>\n\
							<span class="is_new">'+is_new+'</span>\n\
							<span class="status">'+status+'</span>\n\
							<span class="product_long_description">'+product_long_description+'</span>\n\
							<span class="product_short_description">'+product_short_description+'</span>\n\
							<span class="product_paid_consultation">'+product_paid_consultation+'</span>\n\
							<span class="product_free_consultation">'+product_free_consultation+'</span>\n\
							<span class="product_disease">'+product_disease+'</span>\n\
							<span class="product_rashi">'+product_rashi+'</span>\n\
							<span class="product_category">'+product_category+'</span>\n\
							<span class="product_name">'+product_name+'</span>\n\
							<span class="product_weight">'+product_weight+'</span>\n\
							<span class="string_url">'+string_url+'</span>\n\
							</li>';
		
		/*updating quantity and total price in header*/
		var cart_number = parseInt($('.cart_number').text());
		cart_number = cart_number+ 1;
		$('.cart_number').text(cart_number);
	
		
		var cart_total_price  = parseFloat($('.cart_total_price').text()) + + (product_discounted_prize * quantity);
		$('.cart_total_price').text(cart_total_price);
		
		$('ul.product_cart_info').append(footer_li_cart);
		console.log('add to cart click');
		//console.log(other_fees);
		//console.log(footer_li_cart);
		//console.log(cart_number);
		//console.log(addtocart);
		setCookie();
		import_in_cart_table();
	});
	
	/**
	 * This script it used to set cookie when ever user doing action on cart
	 */
	function setCookie(){
		var product_detail=[];
		$('ul.product_cart_info li').each(function(){
			product_detail.push({
				product_treatment : $(this).find('.product_treatment').text(),
				product_id : $(this).find('.product_id').text(),
				product_origin : $(this).find('.product_origin').text(),
				product_grade : $(this).find('.product_grade').text(),
				product_clarity : $(this).find('.product_clarity').text(),
				product_color : $(this).find('.product_color').text(),				
				product_shape : $(this).find('.product_shape').text(),			
				product_height : $(this).find('.product_height').text(),
				product_feature : $(this).find('.product_feature').text(),				
				product_meta_tags : $(this).find('.product_meta_tags').text(),
				product_meta_keyword : $(this).find('.product_meta_keyword').text(),
				quantity : $(this).find('.quantity').text(),				
				product_meta_description : $(this).find('.product_meta_description').text(),
				product_images : $(this).find('.product_images').text(),
				product_discounted_prize : $(this).find('.product_discounted_prize').text(),
				product_actual_prize : $(this).find('.product_actual_prize').text(),
				currency_symbol : $(this).find('.currency_symbol').text(),
				product_meta_title : $(this).find('.product_meta_title').text(),					
				is_new : $(this).find('.is_new').text(),
				status : $(this).find('.status').text(),			
				product_long_description : $(this).find('.product_long_description').text(),
				product_short_description : $(this).find('.product_short_description').text(),			
				product_paid_consultation : $(this).find('.product_paid_consultation').text(),
				product_free_consultation : $(this).find('.product_free_consultation').text(),
				product_disease : $(this).find('.product_disease').text(),
				product_rashi : $(this).find('.product_rashi').text(),
				product_category : $(this).find('.product_category').text(),
				product_name : $(this).find('.product_name').text(),				
				product_weight : $(this).find('.product_weight').text(),
				string_url : $(this).find('.string_url').text(),
			
				
				
				
				
			});
		});
		localStorage.setItem('product_cart' , JSON.stringify(product_detail));
		//console.log('set cookie function');		
		//console.log(product_detail);
		
	}
	
	/*importing the cookie data in footer li*/
	function import_cookie_into_li(){
		//localStorage.setItem('product_cart' , JSON.stringify(''));
		$('ul.product_cart_info').empty();
		var product_cart_counter = 0;
		var shoping_cart_total = 0;
		var cart_total = 0;
		var obj = JSON.parse(localStorage.getItem('product_cart'));
		//console.log(obj);
		//console.log(other_fees);
		var keySize = 0;
		for (key in obj) {
			var product_treatment = obj[key].product_treatment;
			var product_id = obj[key].product_id;
			var product_origin = obj[key].product_origin;
			var product_grade = obj[key].product_grade;
			var product_clarity = obj[key].product_clarity;
			var product_color = obj[key].product_color;
			var product_shape = obj[key].product_shape;
			var product_height = obj[key].product_height;
			var product_feature = obj[key].product_feature;
			var product_meta_tags = obj[key].product_meta_tags;
			var product_meta_keyword = obj[key].product_meta_keyword;			
			var product_meta_description = obj[key].product_meta_description;
			var currency_symbol = obj[key].currency_symbol;
			var product_meta_title = obj[key].product_meta_title;
			var is_new = obj[key].is_new;
			var product_discounted_prize = parseFloat(obj[key].product_discounted_prize);
			var product_actual_prize = parseFloat(obj[key].product_actual_prize);			
			var quantity = parseInt(obj[key].quantity);
			var status = obj[key].status;
			var product_images = obj[key].product_images;
			var product_long_description = obj[key].product_long_description;
			var product_short_description = obj[key].product_short_description;
			var product_paid_consultation = obj[key].product_paid_consultation;
			var product_free_consultation = obj[key].product_free_consultation;
			var product_disease = obj[key].product_disease;
			var product_rashi = obj[key].product_rashi;
			var product_category = obj[key].product_category;
			var product_name = obj[key].product_name;
			var product_weight = obj[key].product_weight;
			var string_url = obj[key].string_url;
			
		//console.log(obj[key]);
			var li_cart = '<li>\n\
				<span class="product_treatment">'+product_treatment+'</span>\n\
				<span class="product_id">'+product_id+'</span>\n\
				<span class="product_origin">'+product_origin+'</span>\n\
				<span class="product_grade">'+product_grade+'</span>\n\
				<span class="product_clarity">'+product_clarity+'</span>\n\
				<span class="product_color">'+product_color+'</span>\n\
				<span class="product_shape">'+product_shape+'</span>\n\
				<span class="product_height">'+product_height+'</span>\n\
				<span class="product_feature">'+product_feature+'</span>\n\
				<span class="product_meta_tags">'+product_meta_tags+'</span>\n\
				<span class="product_meta_keyword">'+product_meta_keyword+'</span>\n\
				<span class="currency_symbol">'+currency_symbol+'</span>\n\
				<span class="product_meta_description">'+product_meta_description+'</span>\n\
				<span class="product_meta_title">'+product_meta_title+'</span>\n\
				<span class="is_new">'+is_new+'</span>\n\
				<span class="product_discounted_prize">'+product_discounted_prize+'</span>\n\
				<span class="product_actual_prize">'+product_actual_prize+'</span>\n\
				<span class="quantity">'+quantity+'</span>\n\
				<span class="status">'+status+'</span>\n\
				<span class="product_images">'+product_images+'</span>\n\
				<span class="product_long_description">'+product_long_description+'</span>\n\
				<span class="product_short_description">'+product_short_description+'</span>\n\
				<span class="product_paid_consultation">'+product_paid_consultation+'</span>\n\
				<span class="product_free_consultation">'+product_free_consultation+'</span>\n\
				<span class="product_disease">'+product_disease+'</span>\n\
				<span class="product_rashi">'+product_rashi+'</span>\n\
				<span class="product_category">'+product_category+'</span>\n\
				<span class="product_name">'+product_name+'</span>\n\
				<span class="product_weight">'+product_weight+'</span>\n\
				<span class="string_url">'+string_url+'</span>\n\
			</li>';
			$('ul.product_cart_info').append(li_cart);
			
			/*updating quantity and total price in header*/
			product_cart_counter = parseInt(product_cart_counter) + 1;
			$('.cart_number').text(product_cart_counter);
			
			shoping_cart_total  = shoping_cart_total + (product_discounted_prize * quantity);
			$('.cart_total_price').text(shoping_cart_total.toFixed(2));
		}
		//console.log('import_cookie_into_li function');
		//console.log(obj);
	}
	
	function import_in_cart_table(){
		$('.cart_body').empty();
		var product_cart_counter = 0;
		var shoping_cart_total = 0;
		var product_cart_data = JSON.parse(localStorage.getItem('product_cart'));
		//console.log(product_cart_data);
		if(product_cart_data){
			if(product_cart_data.length>0){
				$('#cart_shop_id').val(product_cart_data[0].vendor_id);
			}else{
				$('#cart_shop_id').val('0');
			}
		}else{
			
		}
		//console.log('import_in_cart_table function');
		//console.log(product_cart_data)
		var obj = JSON.parse(localStorage.getItem('product_cart'));
		var keySize = 0;
		for (key in obj) {
			var product_weight = obj[key].product_weight;
			var product_name = obj[key].product_name;
			var product_category = obj[key].product_category;
			var product_rashi = obj[key].product_rashi;
			var product_disease = obj[key].product_disease;
			var product_free_consultation = obj[key].product_free_consultation;
			var product_paid_consultation = obj[key].product_paid_consultation;
			var product_short_description = obj[key].product_short_description;
			var product_long_description = obj[key].product_long_description;
			var product_images = obj[key].product_images;
			var status = obj[key].status;
			var currency_symbol = obj[key].currency_symbol;
			var product_actual_prize = parseFloat(obj[key].product_actual_prize);
			var product_meta_keyword = obj[key].product_meta_keyword;
			var product_discounted_prize = parseFloat((obj[key].product_discounted_prize));
			var quantity = parseInt((obj[key].quantity));
			var product_shape = obj[key].product_shape;
			var product_height = obj[key].product_height;
			var string_url = obj[key].string_url;
			var product_color = obj[key].product_color;
			var product_id = obj[key].product_id;
			var is_new = obj[key].is_new;
			var shown_price=(product_actual_prize-product_discounted_prize);
			var table_cart='';
			var product_detail='';
			var service_tax='';
			if(product_weight){
				product_detail+='Weight: '+product_weight+'&nbsp;';
			}if(product_color){
				product_detail+='Colour: '+product_color+' &nbsp;';
			}if(product_shape){
				product_detail+='shape: '+product_shape+'&nbsp;';
			}if(product_height){
				product_detail+='height: '+product_height+'&nbsp;';
			}
			var totalservice_tax=0.00;
			var service_tax_content='';
			if(service_tax && service_tax > 0){
				totalservice_tax=(product_actual_prize-product_discounted_prize*service_tax)/100;
				service_tax_content='<br/> <small>Service Tax'+currency_symbol+'<span>'+((totalservice_tax * quantity).toFixed(2))+'</span></small>';
			}

				table_cart += '<tr service_tax="'+service_tax+'" product_weight="'+product_weight+'" product_name="'+product_name+'" product_color="'+product_color+'" product_height="'+product_height+'" currency_symbol="'+currency_symbol+'" product_images="'+product_images+'" product_actual_prize="'+product_actual_prize+'" product_discounted_prize="'+product_discounted_prize+'" quantity="'+quantity+'" product_name="'+product_name+'" product_id="'+product_id+'">';
					table_cart += '<td class="cart_description  flex_item clear_fix">';	
						table_cart += '<a  href="' + APP_URL + '/product/detail/' +string_url + '"><img src="'+APP_URL+'uploads/'+product_images+'" style="width: 100%; max-width: 50px;"></a>';
					table_cart += '</td>';
					table_cart += '<td class="cart_description  flex_item clear_fix " style="text-align:left;">';				
						table_cart += '<a  href="' + APP_URL + '/product/detail/' +string_url + '"> <h5 class="product-name float_left">'+product_name+'</h5></a>';
						table_cart += '<a  href="' + APP_URL + '/product/detail/' +string_url + '"><h6 class="product-name">'+product_detail+'</h6></a>';
					table_cart += '</td>';
					table_cart += '<td class="qty" style="padding-left: 180px;">';
						table_cart += '<div class="input-group">';
						table_cart += '<span class="input-group-btn"><button class="btn btn-theme-round btn-number btn_minus" style="font-weight: bold; font-size: 18px; color:black;!important">-</button></span>';
						table_cart += '<input type="text" readonly name="quantity" style="font-weight: bold; padding: 6px 15px; color: #969595; border: 2px solid #f1f1f1;  box-shadow: none; height: 33px; font-size: 14px;" class="form-control text-center border-form-control form-control-sm input-number cart_input" max="10" min="1" value="'+quantity+'">';
						table_cart += '<span class="input-group-btn"><button class="btn btn-theme-round btn-number btn_plus"style="font-weight: bold; font-size: 18px; color:black;!important">+</button></span>';
						table_cart += '</div>';
						table_cart += '</td>';
						table_cart += '<td class="price  product-price-cart" style="text-align:right;">';
						table_cart +='₹<span>'+((shown_price * quantity).toFixed(2))+'</span><a style="margin-left: 10px;" class="btn btn-sm btn-danger remove_service" href="javascript:void(0);" value="undefined"><i class="fa fa-trash"></i><i class="mdi mdi-close-circle-outline"></i></a> '//service_tax_content;
					table_cart += '</td>';
				table_cart += '</tr>';
			
			$('.cart_body').append(table_cart);
			
			product_cart_counter = product_cart_counter+parseInt(1);
			$('.cart_number').text(product_cart_counter);
			
			shoping_cart_total  = shoping_cart_total+ ((product_actual_prize-product_discounted_prize) * quantity)+(totalservice_tax * quantity);
			$('.cart_total_price').text(shoping_cart_total.toFixed(2));
			$('.cart_footer_summery').removeClass('display_none')
		}
		if(product_cart_counter == 0){
		  $('.delivery_address_box').addClass('display_none');		    	
		  $('.cart_empty_page_for').removeClass('display_none');		
		  $('.cart_section').addClass('display_none');		
	      $('.card_div').addClass('display_none');		
			
		  $('.payment_box').addClass('display_none');
	      $('.cart_box').addClass('display_none');
		  $('.congratulation_box').addClass('display_none');
		  $('.first_active_circle').removeClass('display_none');
		  $('.first_common_circle').addClass('display_none');
		  $('.all_circle_remove').addClass('display_none');
		  $('.cart_empty_page').removeClass('display_none');
		  $('.cart_total_price').text('0.00');
		  $('.cart_number').text('0');
		  
		  $('.cart_footer_summery').addClass('display_none');
		}
		//console.log('import_in_cart_table function end');
		//console.log(obj[key].if_any_price_one);
	}
	
	function read_from_table_and_save_localStorage(){
		var product_detail=[];
		var remove_cart_counter=0;
		var remove_cart_total=0;
		$('.cart_body tr').each(function(){
			product_detail.push({
				product_id : $(this).attr('product_id'),
				string_url : $(this).attr('string_url'),
				product_color : $(this).attr('product_color'),
				product_images : $(this).attr('product_images'),
				product_height : $(this).attr('product_height'),
				product_shape : $(this).attr('product_shape'),
				product_weight : $(this).attr('product_weight'),
				product_name : $(this).attr('product_name'),
				product_category : $(this).attr('product_category'),
				product_rashi : $(this).attr('product_rashi'),
				product_disease : $(this).attr('product_disease'),
				product_color : $(this).attr('product_color'),
				currency_symbol : $(this).attr('currency_symbol'),
				product_actual_prize : $(this).attr('product_actual_prize'),
				product_discounted_prize : $(this).attr('product_discounted_prize'),
				status : $(this).attr('status'),
				is_new : $(this).attr('is_new'),
				product_name : $(this).attr('product_name'),
				product_image : $(this).attr('product_image'),
				product_id : $(this).attr('product_id'),
				product_paid_consultation : $(this).attr('product_paid_consultation'),
				quantity : parseInt($(this).find('.cart_input').val()),
			});
		});
		localStorage.setItem('product_cart' , JSON.stringify(product_detail));
	    var product_cart_data = JSON.parse(localStorage.getItem('product_cart'));
		console.log(product_detail);
	}
	
	$("body").on("click", ".remove_service", function () {
		$(this).parent().parent().remove();
		read_from_table_and_save_localStorage();
		import_cookie_into_li();
		import_in_cart_table();
	});
	
	$('body').on('click','.btn_plus',function(){
		console.log('btn_plus');
		var quantity = $(this).closest('tr').find('.cart_input').val();
		console.log(quantity);
		console.log(parseInt(quantity, 10)+1);
		
		$(this).closest('tr').find(".cart_input").val(parseInt(quantity, 10)+1);
		read_from_table_and_save_localStorage();
		import_cookie_into_li();
		import_in_cart_table();
	});
	$('body').on('click','.btn_minus',function(){
		var quantity = $(this).closest('tr').find('.cart_input').val();
		if(quantity > 1){
			$(this).closest('tr').find(".cart_input").val(parseInt(quantity, 10)-1);
			read_from_table_and_save_localStorage();
			import_cookie_into_li();
			import_in_cart_table();
		}
	});
	
  
	  
})

