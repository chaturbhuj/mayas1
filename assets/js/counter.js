$('document').ready(function () {

   //-----------------------------------------------------------------------
    /* 
     * admin count validation
     */
    $('#add_counter').validate({
        ignore: [],
        rules: {
            happy_customer: {
                required: true
            },
            birth_store: {
                required: true
            },
			 gemstone_jewellery: {
                required: true
            },
		},                        			
			 messages: {
            happy_customer: {
                required: "happy customer number is required"
            },
            birth_store: {
                required: "birth_store number required"
            },
			gemstone_jewellery: {
                required: "gemstone jewellery value required"
            }
           
					
			},			
        submitHandler: function (form) {
            var happy_customer = $('#happy_customer').val();
			 var birth_store = $('#birth_store').val();
            var gemstone_jewellery = $('#gemstone_jewellery').val();
          			
            $.post(APP_URL + 'configure_access/add_new_counter', {
                happy_customer: happy_customer,
                birth_store: birth_store,
                gemstone_jewellery: gemstone_jewellery,
              			
            },
			 
                    function (response) {
                        $("html, body").animate({scrollTop: 0}, "slow");
                        $('#err_counter_form').empty();
						var mesg;
                        if (response.status == 200) {
							mesg = response.message;
													
								mesg = 'counter has been updated successfully';
							
                            $('#err_counter_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + mesg + "</strong></div>");
					       $('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
						    //alert(response.message);
						//window.location.href= APP_URL+'configure_access/edit_counter';
                        }
                        else if (response.status == 201) {
                            $('#err_counter_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                        }
                        $('#happy_customer').val('');
                        $('#birth_store').val('');
                        $('#gemstone_jewellery').val('');
                        
                    }, 'json');
            return false;
        }
    });


	
})