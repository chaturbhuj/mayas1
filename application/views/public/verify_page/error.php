<div class="page_banner banner resume-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="banner-heading">Email verification error</div>    
			</div>  
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="container detailinfo">
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="formwraper" style="padding: 50px 0px; text-align: center;">
				<div class="formint" style="">
					<h4>There is some problem in verification. please try again after some time.</h4>
				</div>
			</div>
		</div>
    </div>
</div>
