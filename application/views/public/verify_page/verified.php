<div class="page_banner banner resume-banner">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="banner-heading">Email verified</div>    
			</div>  
		</div>
	</div>
</div>
<style>
.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: -0.5;
}
.modal-backdrop {
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;
    background-color: #000;
}
</style>
<div class="clearfix"></div>
<div class="container detailinfo">
  <div class="row">
	<div class="col-md-offset-3 col-md-6">
	  
      
      <!--Account info-->
      <div class="formwraper" style="padding: 50px 0px; text-align: center;">
       
        <div class="formint" style="">
			

			<br>
			<?php
			if(!$this->session->userdata('loggedIN')){
				if($_redirect_link=='customer'){
				?>
				<h4>Your email verified successfully. Please login.</h4>
				<a href="#" data-toggle="modal" data-target="#registerModal" class="btn btn-primary">Login</a>
				<?php
				}
				else{
				?>
				<h4>Your email verified successfully. Please login and post job.</h4>
				<a href="<?php echo base_url();?>vendor/login" class="btn btn-primary">Login</a>
				<?php
				}
			}else{ 
				if($_redirect_link=='customer'){
				?>
					<h4>Your email verified successfully.</h4>
					<a href="#" data-toggle="modal" data-target="#registerModal" class="btn btn-primary">Login</a>
				<?php
				} else{
				?>
					<h4>Your email verified successfully. Please add list items.</h4>
					<a href="<?php echo base_url();?>vendors/configure/add_items" class="btn btn-primary">Add Listing</a>
				<?php
				}
				
			}
			?>
			</div>
      </div>
      
    </div>
    </div>
</div>
