<style>
	html * {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
	}
	.margin_45_40 {
		padding-top: 45px;
		padding-bottom: 40px;
	}
	.container {
    max-width: 1280px !important;
	}
	.row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
	}
	article.blog {
    margin-bottom: 30px;
    background-color: #fff;
    -webkit-box-shadow: 0px 0px 20px 0px rgb(0 0 0 / 10%);
    -moz-box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.1);
    box-shadow: 0px 0px 20px 0px rgb(0 0 0 / 10%);
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
    overflow: hidden;
	}
	article.blog figure {
    height: 260px;
    overflow: hidden;
   /* position: relative; */
    margin-bottom: 0;
	}
	a {
    color: #FF3A65;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    outline: none;
	}
	article.blog figure img {
		
		transition: all 0.3s ease;
		width: 100%;
		height: 100%;
		backface-visibility: hidden;
	}
	article.blog figure .preview {
		position: absolute;
		top: 50%;
		left: 0;
		margin-top: -12px;
		transform: translateY(10px);
		text-align: center;
		opacity: 0;
		visibility: hidden;
		width: 100%;
		transition: all 0.6s;
		z-index: 2;
	}
	article.blog figure .preview span{
		background-color: #fcfcfc;
		background-color: rgba(255, 255, 255, 0.8);
		-webkit-border-radius: 20px;
		-moz-border-radius: 20px;
		-ms-border-radius: 20px;
		border-radius: 20px;
		display: inline-block;
		color: #222;
		font-size: 0.75rem;
		padding: 5px 10px;
	}
	article.blog .post_info {
		padding: 20px 30px 30px 30px;
		position: relative;
		box-sizing: border-box;
	}
	article.blog .post_info small {
		font-weight: 500;
		color: #999;
		font-size: 13px;
		font-size: 0.8125rem;
	}
	article.blog .post_info h2 {
    font-size: 21px;
    font-size: 1.3125rem;
	}
	
element.style {
}
article.blog .post_info h2 {
    font-size: 21px;
    font-size: 1.3125rem;
}
h1, h2, h3, h4, h5, h6 {
    color: #222;
    font-weight: 500;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
article.blog .post_info h2 a {
    color: #333;
}
a {
    color: #FF3A65;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    outline: none;
}
p {
    margin-bottom: 20px;
}
article.blog .post_info ul {
    margin: 0 -30px 0 -30px;
    padding: 20px 30px 0 30px;
    width: 100%;
    box-sizing: content-box;
    border-top: 1px solid #ededed;
}
article.blog .post_info ul li {
    display: inline-block;
    position: relative;
    padding: 12px 0 0 50px;
    font-weight: 500;
    font-size: 12px;
    font-size: 0.75rem;
    color: #999;
}
article.blog .post_info ul li .thumb {
    width: 40px;
    height: 40px;
    overflow: hidden;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    -ms-border-radius: 50%;
    border-radius: 50%;
    display: inline-block;
    position: absolute;
    left: 0;
    top: 0;
}
article.blog .post_info ul li .thumb img {
    width: 40px;
    height: auto;
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}
article.blog .post_info ul li:last-child {
    float: right;
    padding-left: 0;
    line-height: 1;
}
article.blog .post_info ul li:last-child i {
    font-size: 14px;
    font-size: 0.875rem;
    margin-right: 5px;
    position: relative;
    top: 3px;
}
.pagination_fg {
    text-align: center;
    margin-top: 15px;
}
.pagination_fg a {
    color: #333;
    display: inline-block;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
    margin: 0 2px;
}
.pagination_fg a.active {
    background-color: #333;
    color: white;
}
.pagination_fg a {
    color: #333;
    display: inline-block;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
    margin: 0 2px;
}
.widget {
    position: relative;
    display: block;
    margin-bottom: 15px;
}
.search_blog .form-group {
    position: relative;
}
.form-control {
    padding: 10px;
    height: 40px;
    font-size: 14px;
    font-size: 0.875rem;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    border: 1px solid #d2d8dd;
}

element.style {
}
.search_blog .form-group input[type="submit"] {
    border: 0;
    position: absolute;
    top: 5px;
    right: 5px;
    background-color: #FF3A65;
    color: #fff;
    font-weight: 600;
    height: 30px;
    line-height: 28px;
    padding: 0 10px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}
.widget-title.first {
    padding-top: 0;
}
.widget-title {
    padding: 15px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #ededed;
}
.widget-title h4 {
    padding: 0;
    margin: 0;
    font-weight: 500;
    line-height: 1;
    font-size: 16px;
    font-size: 1rem;
}
.comments-list {
    padding: 0;
    list-style: none;
}
.comments-list li {
    margin-bottom: 10px;
    display: table;
    width: 100%;
}
.alignleft {
    float: left;
    margin: 0 15px 10px 0;
    width: 80px;
    height: 80px;
    overflow: hidden;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    position: relative;
}
a {
    color: #FF3A65;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    outline: none;
}
.alignleft img {
    width: auto;
    height: 80px;
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}
.comments-list small {
    color: #555;
}
.comments-list h3 {
    font-size: 14px;
    font-size: 0.875rem;
    padding: 0 0 0;
    margin: 0;
    text-transform: capitalize;
}
.comments-list h3 a {
    color: #555;
}
a {
    color: #FF3A65;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    outline: none;
}
.comments-list h3 a {
    color: #555;
}
.widget ul.cats {
    list-style: none;
    padding: 0;
}
.widget ul.cats li {
    padding: 0 0 5px 2px;
    position: relative;
}
.widget ul.cats a {
    color: #555;
}
a {
    color: #FF3A65;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
    outline: none;
}
.widget ul.cats a span {
    position: absolute;
    right: 0;
}
.tags a {
    background-color: #f0f0f0;
    padding: 3px 10px;
    font-size: 13px;
    margin: 0 0 4px;
    letter-spacing: 0.4px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -ms-border-radius: 3px;
    border-radius: 3px;
    display: inline-block;
    color: #333;
}
</style>
   <!-- page-header -->
    <div class="page-header" style="margin-top: 100px;margin-bottom: 20px;">
        <div class="container">
            <div class="row">
                <!-- page caption -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="page-caption">
                        <h2 class="page-title" style="text-align:center;font-family:wfont_efd0e1_698b786042944acdbc6819093f611249,wf_698b786042944acdbc6819093,orig_adobe_garamond_pro_bold !important;"><strong>Blog List</strong></h2>
                    </div>
					
                </div>
                <!-- /.page caption -->
            </div>
        </div>
    </div>
    <!-- /.page-header -->
	
	
	
	
	<div class="container margin_45_40">			
			<div class="row">
				<div class="col-lg-9">
					<div id="append_blog_list" class="row">
					
					</div>
					<!-- /row -->
				</div>
				<!-- /col -->

				<aside class="col-lg-3">
					<div class="widget search_blog" style="display:none;">
						<div class="form-group">
							<input type="text" name="search" id="search" class="form-control" placeholder="Terms...">
							<span><input type="submit" value="Search"></span>
						</div>
					</div>
					<!-- /widget -->
					<div class="widget">
						<div class="widget-title first">
							<h4>Latest Post</h4>
						</div>
						<ul class="comments-list" id="append_recent_post">
						<!--	<li>
								<div class="alignleft">
									<a href="#0"><img src="img/blog-5.jpg" alt="edcsfws"></a>
								</div>
								<small>Category - 11.08.2016</small>
								<h3><a href="#" title="">Verear qualisque ex minimum...</a></h3>
							</li>
							<li>
								<div class="alignleft">
									<a href="#0"><img src="img/blog-6.jpg" alt="wsedce"></a>
								</div>
								<small>Category - 11.08.2016</small>
								<h3><a href="#" title="">Verear qualisque ex minimum...</a></h3>
							</li>
							<li>
								<div class="alignleft">
									<a href="#0"><img src="img/blog-4.jpg" alt="ZDFVDGVF"></a>
								</div>
								<small>Category - 11.08.2016</small>
								<h3><a href="#" title="">Verear qualisque ex minimum...</a></h3>
							</li>  -->
						</ul>
					</div>
					<!-- /widget -->
					<div class="widget" style="display:none;">
						<div class="widget-title">
							<h4>Categories</h4>
						</div>
						<ul class="cats">
							<li><a href="#">Dermatology <span>(12)</span></a></li>
							<li><a href="#">Consulting <span>(21)</span></a></li>
							<li><a href="#">Treatments <span>(44)</span></a></li>
							<li><a href="#">Personal care <span>(31)</span></a></li>
						</ul>
					</div>
					<!-- /widget -->
					<div class="widget" style="display:none;">
						<div class="widget-title">
							<h4>Popular Tags</h4>
						</div>
						<div class="tags">
							<a href="#">Lawyer</a>
							<a href="#">Accounting</a>
							<a href="#">Consulting</a>
							<a href="#">Doctors</a>
							<a href="#">Best Offers</a>
							<a href="#">Languages</a>
							<a href="#">Teach</a>
						</div>
					</div>  
					<!-- /widget -->
				</aside>
				<!-- /aside -->
			</div>
			<!-- /row -->	
		</div>
	<script>
	$('document').ready(function () {
		get_blog_list(0);
		function get_blog_list(page_no){
			$.post(APP_URL+'api/publicpages/fatch_blog_list_data',{
				page_no: page_no,
			},function(response){
				console.log(response);
				if (response.status == 200) {
					$('#append_blog_list').html(response.data);
					
					$('#append_pagination_data').html(response.pagination);
				} 
			},'json');
		}
		get_recent_blog_list();
		function get_recent_blog_list(){
			$.post(APP_URL+'api/publicpages/fatch_recent_blog_list_data',{
			},function(response){
				
				if (response.status == 200) {
					$('#append_recent_post').html(response.data);
				} 
			},'json');
		}
	});
	</script>