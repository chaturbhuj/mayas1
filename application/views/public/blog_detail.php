
	<input type="hidden" id="blog_data" value="<?php echo $_GET['id'];?>">
   <!-- ::::::  Start  Breadcrumb Section  ::::::  -->
    
        <div class="container">
             <div class="row m-b-20 m-t-20">
				<div class="col-md-12 m-b-20">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">blogs </li>
						</ol>
					</nav> 
				</div>   
            </div>
        </div>
   <!-- ::::::  End  Breadcrumb Section  ::::::  -->

    <!-- ::::::  Start  Main Container Section  ::::::  -->
    <main id="main-container" class="main-container" style="background: #E5E5E5; " >
        <div class="container">
            <div class="row flex-column-reverse flex-lg-row">
           
                <div class="col-lg-8">
                    <div class="blog">
						
                        <div class="row">
						
                            <!-- Start Single Blog List -->
                            <div class="col-12" style="margin-top:55px; border: 2px solid #ffff; padding: 10px; background: #ffff; ">
                              
								<div class="blog__type-single">
                                    <div class="img-responsive"><img  id="append_blog_image"  src="" alt=""></div>
                                    <div class="blog__content">
                                        <h3 id="append_blog_heading" class="title title--small title--thin"></h3>
                                        <div class="blog__archive m-t-10">
                                            <a href="#" class="link--gray link--icon-left m-r-30"><i class="far fa-calendar"></i> <span id="append_date_data"></span></a>
                                            <a href="#"  class="link--gray link--icon-left"><i class="far fa-user"></i> <span id="append_posted_by"></span></a>
											<span id="append_blog_content"></span>
											<span style="display:none;">Share:</span>
                                            <?php //$this->load->view('share_link');?>
											
										</div>
                                    </div>
                                    
                                </div> 
                            </div> <!-- End Single Blog List -->
                        </div>
                    </div>

                    <!-- Start Blog Comment -->
                    <div class="blog blog--comment blog--1 display_none">
                        <div class="section-content">
                            <h5 class="section-content__title">3 Comments</h5>
                        </div>
                        <!-- Start - Review Comment -->
                        <ul class="comment m-t-50">
                            <!-- Start - Review Comment list-->
                            <li class="comment__list">
                                <div class="comment__wrapper">
                                    <div class="comment__img">
                                        <img src="" alt=""> 
                                    </div>
                                    <div class="comment__content">
                                        <div class="comment__content-top">
                                            <div class="comment__content-left flex-column">
                                                <h6 class="comment__name">Kaedyn Fraser</h6>
                                                <span class="blog-news__postdate" id="append_date_data"></span>
                                            </div>   
                                            <div class="comment__content-right">
                                                <a href="#" class="link--gray link--icon-left m-b-5"><i class="fas fa-reply"></i>Reply</a>
                                            </div>
                                        </div>
                                        
                                        <div class="para__content">
                                            <p class="para__text" id="append_comment_data1"> </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Start - Review Comment Reply-->
                                <ul class="comment__reply">
                                    <li class="comment__reply-list">
                                        <div class="comment__wrapper">
                                            <div class="comment__img">
                                                <img src="" alt=""> 
                                            </div>
                                            <div class="comment__content">
                                                <div class="comment__content-top">
                                                    <div class="comment__content-left flex-column ">
                                                        <h6 class="comment__name">Oaklee Odom</h6>
                                                        <span class="blog-news__postdate">Oct 29, 2018</span>
                                                    </div>   
                                                    <div class="comment__content-right">
                                                        <a href="#" class="link--gray link--icon-left m-b-5"><i class="fas fa-reply"></i>Reply</a>
                                                    </div>
                                                </div>
                                                
                                                <div class="para__content">
                                                    <p class="para__text" id="append_comment_data2"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul> <!-- End - Review Comment Reply-->
                            </li> <!-- End - Review Comment list-->
                            <!-- Start - Review Comment list-->
                            <li class="comment__list">
                                <div class="comment__wrapper">
                                    <div class="comment__img">
                                        <img src="" alt=""> 
                                    </div>
                                    <div class="comment__content">
                                        <div class="comment__content-top">
                                            <div class="comment__content-left flex-column">
                                                <h6 class="comment__name">Jaydin Jones</h6>
                                                <span class="blog-news__postdate">Oct 29, 2018</span>
                                            </div>   
                                            <div class="comment__content-right">
                                                <a href="#" class="link--gray link--icon-left m-b-5"><i class="fas fa-reply"></i>Reply</a>
                                            </div>
                                        </div>
                                        
                                        <div class="para__content">
                                            <p class="para__text" id="append_comment_data1"></p>
                                        </div>
                                    </div>
                                </div>
                            </li> <!-- End - Review Comment list-->
                        </ul>  <!-- End - Review Comment -->
                    </div>  <!-- End Blog Comment -->

                    <!-- Start Blog Comment Form -->
                    <div class="blog blog--comment-form m-t-10 display_none">
                        <div class="section-content">
                            <h5 class="section-content__title">Leave A Reply</h5>
                            <p class="section-content__desc">Your email address will not be published. Required fields are marked *</p>
                        </div>	
						<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
						<input type="hidden" id="blog_id" value="">
                                            
                        <form class="form-box" action="#" method="post">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-box__single-group">
                                        <label for="form-review">Your review*</label>
                                        <textarea id="form-review" rows="8" placeholder="Write a review"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box__single-group">
                                        <label for="form-name">Your Name*</label>
                                        <input type="text" id="form-name" placeholder="Enter your name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box__single-group">
                                        <label for="form-email">Your Email*</label>
                                        <input type="email" id="form-email" placeholder="Enter your email" required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-box__single-group">
                                        <label for="form-site">Your Website</label>
                                        <input type="text" id="form-site" placeholder="Enter your website">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button class="btn btn--box btn--small btn--blue btn--uppercase btn--weight m-t-30 text-uppercase" type="submit">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div> <!-- End Blog Comment Form -->
                    
                    <!--   Start Recent Blog News    -->
                    <div class="blog blog--1 swiper-outside-arrow-hover display_none">
                        <div class="row">
                            <div class="col-12">
                                <div class="section-content section-content--border d-flex align-items-center justify-content-between">
                                    <h5 class="section-content__title">Recent Blog</h5>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="swiper-outside-arrow-fix pos-relative">
                                    <div class="blog-news blog-news-3grid overflow-hidden  m-t-50">
                                        <div class="swiper-wrapper">
                                            <!-- Single Blog News Item -->
                                            <div class="blog-news__box swiper-slide">
                                                <div class="blog-news__img-box">
                                                    <a href="blog-single-left-sidebar.html" class="blog-news__img--link">
                                                        <img src="" alt="" class="blog-news__img">
                                                    </a>
                                                </div>

                                                <div class="blog-news__archive m-t-25">
                                                    <a href="#" class="blog-news__postdate"><i class="far fa-calendar"></i> Oct 29, 2018</a>
                                                    <a href="#" class="blog-news__author"><i class="far fa-user"></i> Jhon Doe</a>
                                                </div>

                                                <a href="blog-single-left-sidebar.html" class="blog-news__link">
                                                    <h5>Vehicula Diam Potenti Imperdiet Placerat Placeat</h5>
                                                </a>
                                            </div> <!-- End Blog News Item -->
                                            <!-- Single Blog News Item -->
                                            <div class="blog-news__box swiper-slide">
                                                <div class="blog-news__img-box">
                                                    <a href="blog-single-left-sidebar.html" class="blog-news__img--link">
                                                        <img src="" alt="" class="blog-news__img">
                                                    </a>
                                                </div>

                                                <div class="blog-news__archive m-t-25">
                                                    <a href="#" class="blog-news__postdate"><i class="far fa-calendar"></i> Oct 29, 2018</a>
                                                    <a href="#" class="blog-news__author"><i class="far fa-user"></i> Jhon Doe</a>
                                                </div>

                                                <a href="blog-single-left-sidebar.html" class="blog-news__link">
                                                    <h5>Vehicula Diam Potenti Imperdiet Placerat Placeat</h5>
                                                </a>
                                            </div> <!-- End Blog News Item -->
                                            <!-- Single Blog News Item -->
                                            <div class="blog-news__box swiper-slide">
                                                <div class="blog-news__img-box">
                                                    <a href="blog-single-left-sidebar.html" class="blog-news__img--link">
                                                        <img src="" alt="" class="blog-news__img">
                                                    </a>
                                                </div>

                                                <div class="blog-news__archive m-t-25">
                                                    <a href="#" class="blog-news__postdate"><i class="far fa-calendar"></i> Oct 29, 2018</a>
                                                    <a href="#" class="blog-news__author"><i class="far fa-user"></i> Jhon Doe</a>
                                                </div>

                                                <a href="blog-single-left-sidebar.html" class="blog-news__link">
                                                    <h5>Vehicula Diam Potenti Imperdiet Placerat Placeat</h5>
                                                </a>
                                            </div> <!-- End Blog News Item -->
                                            <!-- Single Blog News Item -->
                                            <div class="blog-news__box swiper-slide">
                                                <div class="blog-news__img-box">
                                                    <a href="blog-single-left-sidebar.html" class="blog-news__img--link">
                                                        <img src="" alt="" class="blog-news__img">
                                                    </a>
                                                </div>

                                                <div class="blog-news__archive m-t-25">
                                                    <a href="#" class="blog-news__postdate"><i class="far fa-calendar"></i> Oct 29, 2018</a>
                                                    <a href="#" class="blog-news__author"><i class="far fa-user"></i> Jhon Doe</a>
                                                </div>

                                                <a href="blog-single-left-sidebar.html" class="blog-news__link">
                                                    <h5>Vehicula Diam Potenti Imperdiet Placerat Placeat</h5>
                                                </a>
                                            </div> <!-- End Blog News Item -->
                                        </div>
                                        <div class="swiper-buttons">
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next default__nav default__nav--next"><i class="fal fa-chevron-right"></i></div>
                                            <div class="swiper-button-prev default__nav default__nav--prev"><i class="fal fa-chevron-left"></i></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> <!--   End Recent Blog News  -->

                </div>

				<!-- Start Rightside - Content -->
				
				<div  style="" class="col-lg-4">
                    <div class="sidebar">
                      <div class="" style="margin-top: 45px; margin-bottom: 30px;">
                    <div class="sidebar">
                        <!-- widget-recent-post -->
                        <div class="widget widget-recent-post">
                            <h3 class="widget-title">Recent Post</h3>
                            <ul id="append_recent_post" class="">
                                
                            </ul>
                        </div>
                        <!-- widget-archives -->
                        <div class="widget widget-archives" style="display : none;">
                            <h3 class="widget-title">Archives</h3>
                            <ul class="angle">
                                <li><a href="#">September</a></li>
                                <li><a href="#">August</a></li>
                                <li><a href="#">July</a></li>
                                <li><a href="#">June</a></li>
                            </ul>
                        </div>
                        <!-- /.widget-archives -->
                        <!-- widget-tags -->
                        <div class="widget widget-tags" style="display : none;">
                            <h3 class="widget-title">Tags</h3>
                            <a href="#">Wedding</a>
                            <a href="#">Wedding Venue</a>
                            <a href="#">Cakes</a>
                            <a href="#">Wedding Vendor</a>
                            <a href="#">Wedding Flowers</a>
                            <a href="#">Photography</a>
                        </div>
                        <!-- /.widget-tags -->
                    </div>
                </div>
                          
                    </div>
                </div> 
            </div>
        </div>
    </main> <!-- ::::::  End  Main Container Section  ::::::  -->

    <!-- /.page-header -->
	<div class="content" style="background: #E5E5E5;">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
                    <!-- post-content -->
                    <div class="post-content-single">
                        <div class="row">
                            <div style="" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="post-img">
                                    <a href="#"><img id="append_blog_image" src="<?php echo base_url();?>" alt="" class="img-fluid"></a>
                                </div>
                                <div   class="post-content text-left display_none">
                                    <h2 id="append_blog_heading">  </h2>
                                    <p class="meta">
                                        <span id="append_posted_by" class="meta-posted-by">By <a href="#"></a></span>
                                        <span id="append_date_data" class="meta-date"></span>
                                        <span class="meta-comments text-primary"><span class="append_comment_count"></span> Comments</span>
                                    </p>
									<div id="append_blog_content"></div>
                                </div>

                            </div>
                            <!-- /.social-media -->
                            <div style="margin-bottom:10px;display:none;"  class="col-xl-12 mt-2">
                                <div  class="card card-body card-shadow-none border display_none">
                                 
									<h6>Share this </h6>
									<div class="social-icons">
										<?php $this->load->view('share_link');?>
									</div> 
								</div>
                            </div>
                           
                            <!-- comment-section -->
                            <div  style=" margin-bottom:10px; padding: 0px;" class="col-lg-12">
                                <!-- comment-block -->
                                <div class="card comments-block border card-shadow-none">
                                    <div class="comments-area">
                                        <h6 class="card-header bg-white">(<span class="append_comment_count"></span>) Comments</h6>
										<span id="append_comment_data">
											
										</span>
                                    </div>
                                </div>
                            </div>
                            <!-- /.comment-section -->
                            <!-- comment-form -->
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="padding: 0px;">
                                <div class="comments-reply">
                                    <div class="leave-reply ">
                                        <div class="row">
                                            <!-- comment-section -->
                                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                <h5>Leave A Comments</h5>
												<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
												<input type="hidden" id="blog_id" value="">
                                            </div>
                                            <!-- /.comment-section -->
                                        </div>
                                        <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <!-- comment-reply-form -->
                                                <form id="comment_form" method="post" class="reply-form form-row">
													<div id="error_comment_form_submit"class="w-100"></div>
                                                    <!-- Textarea -->
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="form-group">
                                                            <label class="control-label" for="user_comment">Message</label>
                                                            <textarea class="form-control" id="user_comment" name="user_comment" rows="4" placeholder=""></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <!-- Button -->
                                                        <div class="form-group">
															<button id="singlebutton" name="singlebutton" class="btn btn--box btn--small btn--blue btn--uppercase btn--weight">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.comment-form -->
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
	
<script>
$('document').ready(function () {
	
	get_blog_list();
	function get_blog_list(){
		var blog_data = $('#blog_data').val();
		$.post(APP_URL+'api/publicpages/fatch_blog_detail_data',{
			blog_data : blog_data,
		},function(response){
			
			if (response.status == 200) {
				$('#blog_id').val(response.data.blog_id);
				$('#append_blog_heading').html(response.data.blog_title);
				$('#append_blog_content').html(response.data.content);
				$('#append_date_data').html(response.data.date_data);
				$('.append_comment_count').html(response.data.comments);
				console.log(response.data.comments);
				$('#append_blog_image').attr('src',response.data.blog_img);
				$('#append_posted_by').html('By '+response.data.post_by);
				
				$('#append_comment_data').html(response.comment);
				console.log(response.comment);
			} 
		},'json');
	}
	get_recent_blog_list();
	function get_recent_blog_list(){
		$.post(APP_URL+'api/publicpages/fatch_recent_blog_list_data',{
		},function(response){
			
			if (response.status == 200) {
				$('#append_recent_post').html(response.data);
			} 
		},'json');
	}
	
	$('#comment_form').validate({
		ignore: [],
		rules: {
			user_comment: {
				required: true,
			},
		},
		messages: {
			user_comment: {
				required: "message is required"
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/img/loading.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var user_comment = $('#user_comment').val();
			var customer_id = $('#customer_id').val();
			var blog_id = $('#blog_id').val();
			if(customer_id){
				$.post(APP_URL+'api/publicpages/blog_comment_submit',{
					user_comment: user_comment,
					customer_id: customer_id,
					blog_id: blog_id,
				},function(response){
					$.unblockUI();
					$('#error_comment_form_submit').empty();
					if (response.status == 200) {
						var message = response.message;
						$('#error_comment_form_submit').html("<div class='alert alert-success fade show'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#error_comment_form_submit").fadeTo(2000, 500).slideUp(500, function(){
							$('#error_comment_form_submit').empty();
							//window.location.href = APP_URL+'jobseeker/configure/profile';
							window.location.reload();
						});
					}  else if (response.status == 201) {
						$('#error_comment_form_submit').html("<div class='alert alert-danger fade show'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				},'json');
				return  false;
			}else{
				$.unblockUI();
				$('#error_comment_form_submit').empty();
				var message = 'to comment on blog please Login first';
				$('#error_comment_form_submit').html("<div class='alert alert-danger fade show'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong></div>");
			}
		}
	});
});
</script>