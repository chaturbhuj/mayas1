
        <!-- page-header -->
    <div class="page-header">
        <div class="container">
            <div class="row">
                <!-- page caption -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="page-caption">
                        <h1 class="page-title" style="text-align: center; padding: 30px; margin-top:40px;"><?php echo $heading;?></h1>
						<input type="hidden" id="content_id" value="<?php echo $content_id;?>">
						<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
                    </div>
                </div>
                <!-- /.page caption -->
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <!-- about-descriptions -->
    <div class="space-medium pb-0">
        <div class="container">
            <div class="row" style="margin-bottom: 50px; margin-top: 40px;">
                <!--  about-details  -->
                <div class="offset-xl-2 col-xl-8 offset-lg-2 col-lg-8 col-md-12 col-sm-12 col-12 mb60">
                    <div id="append_page_content" class="about-descriptions">
                        
                    </div>
					<a style="color:#ffff;"  class="btn  purpose_a_btn btn--box btn--large btn--blue btn--uppercase btn--weight m-t-20 display_none">Propose A Product</a>
					<a style="color:#ffff;"  class="btn  add_a_product_link btn--box btn--large btn--blue btn--uppercase btn--weight m-t-20 display_none">Add A Product Link</a>
                </div>
            </div>
        </div>
    </div>
	
	<script>
	$('document').ready(function () {
		var content_id=$('#content_id').val();
		console.log(content_id);
		if(content_id == 5){
			$('.purpose_a_btn').removeClass('display_none');
		}
		if(content_id == 6){
			$('.add_a_product_link').removeClass('display_none');
		}
		$.post(APP_URL+'api/publicpages/fatch_public_content_data',{
			content_id: content_id,
		},function(response){
			$.unblockUI();
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headMsg').empty();
			if (response.status == 200) {
				$('#append_page_content').html(response.data);
			} 
		},'json');
		
		$('body').on('click', '.purpose_a_btn', function () {
			var customer_id = $('#customer_id').val(); 
            if(customer_id == 0 || !customer_id){
                $('#loginModal').modal('show'); 
                return;
				
				
				//href="<?php echo base_url(); ?>customer/configure/add_product_vendor"
				
            }
			window.location.href = APP_URL+'customer/configure/add_product_vendor';
		});
		
		$('body').on('click', '.add_a_product_link', function () {
			var customer_id = $('#customer_id').val(); 
            if(customer_id == 0 || !customer_id){
                $('#loginModal').modal('show'); 
                return;
				
				
				//href="<?php echo base_url(); ?>customer/configure/add_product_vendor"
				
            }
			window.location.href = APP_URL+'customer/configure/wishlist';
		});
		
	});
	
	</script>