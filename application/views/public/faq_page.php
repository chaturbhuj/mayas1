<style>
.head_top{
	padding-top: 50px;
    padding-bottom: 50px;
    text-align: center;
}
.btn-link:hover {
    color: #0056b3;
    text-decoration: underline;
    background-color: transparent;
    border-color: transparent;
}
.question_btn{
	border: none;
    background: transparent;
}
.h4::after{
	border:none;
}	
</style>
    <!-- page-header -->
    <div class="page-header" style="margin-top: 50px;">
        <div class="container">
            <div class="row">
                <!-- page caption -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="page-caption">
                        <h1 class="page-title head_top" style="font-family:wfont_efd0e1_698b786042944acdbc6819093f611249,wf_698b786042944acdbc6819093,orig_adobe_garamond_pro_bold !important;">Frequantly Ask Question</h1>
                    </div>
                </div>
                <!-- /.page caption -->
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <!-- faq-page-section -->
    <div class="content" style="margin-left: 80px;margin-right: 80px;margin-bottom: 80px;">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                    <div class="left-navbar mb30 display_none">
                        <ul id="append_faq_category" class="nav flex-column mb30">
                            
                        </ul>
                  
                    <div class="card display_none">
                        <div class="card-body">
                            <h3>Can’t find what you’re looking for </h3>
                            <p>Contact our customer service representative for further assistance.</p>
                            <a href="<?php echo base_url();?>publicpages/contact" class="btn btn-primary">contact us</a>
                        </div>
                        <div class="card-footer">
                            <p class="text-default">+91 7737362081</p>
                        </div>
                    </div>
                      </div>
                </div>
                <div class="col-xl-12 col-lg-9 col-md-12 col-sm-12 col-12">
					 <div id="append_faq"></div>
                </div>
            </div>
	
        </div>
    </div>
    <!-- /.faq-page-section -->
	<script>
	$('document').ready(function () {
		get_faq_data();
		function get_faq_data(){
			$.post(APP_URL+'api/publicpages/fatch_faq_page_data',{
			},function(response){
				if (response.status == 200) {
					$('#append_faq').html(response.data);
					$('#append_faq_category').html(response.category);
				} 
			},'json');
		}
	});
	</script>