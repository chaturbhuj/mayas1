
<style>
.main_body{
background: #f5f6f9;
}
.section-content__title{
font-family: Montserrat;
font-weight: bold;
font-size: 36px;
line-height: 44px;
color: #000000;
opacity: 0.8;
}
.Details_box{
background: #FFFFFF;
box-shadow: 2px 2px 40px rgba(0, 0, 0, 0.2);
}
.name{
font-family: Montserrat;
font-style: normal;
font-weight: normal;
font-size: 14px;
line-height: 17px;
color: #000000;
opacity: 0.5;
opacity: 0.5;
border: 1px solid #000000;
}
.bottom_border{
border: none;
border-bottom: 1px solid #000000;
border-radius: 0px;
opacity: 0.5;
}
.lorem_ipsum{
font-family: Montserrat;
font-style: normal;
font-weight: 500;
font-size: 12px;
line-height: 15px;
color: #000000;
opacity: 0.5;
}
.price{
font-family: Montserrat;
font-style: normal;
font-weight: bold;
font-size: 12px;
line-height: 15px;
color: #000000;
}
.img{
width: 70px;
padding: 10px;
background: #FFFFFF;
border: 1px solid #C4C4C4;
box-sizing: border-box;
border-radius: 2px;
margin-right: 30px;
}
}
.hr_line{
opacity: 0.5;
border: 1px solid #000000;
transform: matrix(1, 0, 0, 1, 0, 0);
}
.total_size{
font-family: Montserrat;
font-weight: 500;
font-size: 18px;
line-height: 22px;
color: #000000;
opacity: 0.5;
}
.total_price{
font-family: Montserrat;
font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 22px;
color: #000000;
}
.Congratulations{
font-family: Montserrat;
font-style: normal;
font-weight: bold;
font-size: 36px;
line-height: 44px;
color: #000000;
opacity: 0.8;
padding: 100px;
padding-bottom: 30px;
}
.Your_shipment{
font-family: Montserrat;
font-style: normal;
font-weight: 300;
font-size: 18px;
line-height: 22px;
color: #000000;
}
.Back_to_home_page{
font-family: Montserrat;
font-style: normal;
font-weight: 500;
font-size: 12px;
line-height: 15px;
color: #FFFFFF;

}
.active_circle{
	padding: 8px 18px;
    background: #E0317E;
    margin: 15px;
    border-radius: 50px;
    border: 3px solid white;
    box-shadow: 0px 0px 1px 2px #E0317E;
}
.common{
	padding: 10px 20px;
    border: 1px solid #6a656559;
    border-radius: 50px;
}
.{
width: 86px;
border-bottom: 1px solid;
margin: 40px 10px 10px 10px!important;
padding: 0px 99px 0px;
}

</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12 col-12">
			<button class="common">&nbsp;</button>
			<span class="">&nbsp;</span>
			<button class="active_circle">&nbsp;</button>
			<span class="">&nbsp;</span>
			<button class="common">&nbsp;</button>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style=" padding-bottom:200px;">
		<div class="col-sm-8 col-12" >
			<div class="Details_box" style="padding:50px;">
				<div class="section-content m-t-5 m-b-30">
					<h5 class="section-content__title">Details</h5>
				</div>
				<form action="#" method="post" class="form-box">
					<div class="row">
							<div class="col-sm-12 col-12">
								<div class="form-group service-form-group">
								<label class="control-label sr-only"  style="border:none;" for="fname"></label>
								<input id="fname" type="text" name="fname" placeholder="First and last name" class="form-control" required 
								style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
								</div>
							</div>
					</div>
					<div class="row">
							<div class="col-sm-12 col-12">
								<div class="form-group service-form-group ">
								<label class="control-label sr-only "  for="Address"></label>
								<input id="Address" type="text" name="Address" placeholder="Address" class="form-control" required 
								style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
								</div>
							</div>
					</div>
					<div class="row" >
						<div class="col-sm-4 col-12">
								<div class="form-box__single-group " style="margin-top:0px;">
									<label for="form-State" style="font-size: 14px;"></label>
									<input type="text" id="form-State" placeholder="State" class="form-control" required 
									style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
								</div>
						</div>
						<div class="col-sm-4 col-12" >
								<div class="form-box__single-group" style="margin-top:0px">
									<label for="form-city"></label>
									<input type="text" id="form-city" placeholder="city" class="form-control" required
									style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
								</div>
						</div>
						<div class="col-sm-4 col-12" >
								<div class="form-box__single-group" style="margin-top:0px">
									<label for="Postal_code"></label>
									<input type="text" id="Postal_code" placeholder="Postal code" class="form-control" required
									style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
								</div>
						</div>
					</div>
					<div class="row" >
							<div class="col-sm-4 col-12" >
								<div class="form-box__single-group" style="margin-top:0px; padding-top:15px">
									<label for="phone_number"></label>
									<input type="text" id="phone_number" placeholder="Phone number" class="form-control" required="" 
									style="border:none;border-bottom:1px solid #000000;border-radius:0px;padding: 0px;">
								</div>
							</div>
					</div>	
					<div class="row" >
								<div class=" col-sm-8" style="padding-bottom:40px; padding-top:40px;">
									<button type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight" style="border-radius:4px;">Continue</button>
								</div>	
					</div>	
				</form>
			</div>
		</div>
			<div class="col-sm-4 col-12">
				<div class="your-order-section">
					<div class="section-content ">
						<h5 class="section-content__title">YOUR CART</h5>
					</div>
					<div class="row" style="">
						<div class="col-12" style="padding-right: 30px;margin-bottom: 45px;">
						
							<div class=""style="display:inline-block;">
								<img class="img "  src="<?php echo base_url(); ?>assets/img/your_cart_img" alt="">
							</div>
						
							<div style="display:inline-block; margin-top:20px; padding:10px" >
								<label class="lorem_ipsum">LOREM IPSUM</label>
								<div class="price">$130</div>
							</div>
							<div style="display:inline-block; float:right; margin-top:40px;">
								<button class="fa fa-trash" style="display:inline-block" type="button" >
								</button>
							</div>
						</div>
					</div>
					<hr style="margin: 0;border-top: 1px solid #6e6767;">
					<div class="row" style="">
						<div class="col-12" style="padding-right: 30px;margin-bottom: 30px;">
						
							<div class=""style="display:inline-block;">
								<img class="img "  src="<?php echo base_url(); ?>assets/img/your_cart_img" alt="">
							</div>
						
							<div style="display:inline-block; margin-top:20px; padding:10px" >
								<label class="lorem_ipsum">LOREM IPSUM</label>
								<div class="price">$130</div>
							</div>
							<div style="display:inline-block; float:right; margin-top:40px;">
								<button class="fa fa-trash" style="display:inline-block" type="button" >
								</button>
							</div>
						</div>
					</div>
					<hr style="border-top: 1px solid #6e6767;">
					<div class=" d-flex justify-content-between">
						<h5 class="your-order-total-left total_size">Total</h5>
						<h5 class="your-order-total-right total_price">$329</h5>
					</div>		
				</div>
			</div>
	</div>
</div>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-12">
				<button></button>
			</div>
		</div>
	</div>
<div class="container">
	<div class="row" style=" padding-bottom:200px;">
		<div class="col-sm-8 col-12" >
			<div class="Details_box" style="padding:50px;">
				<div class="section-content m-t-5 m-b-30">
					<h5 class="section-content__title">PAYMENT</h5>
				</div>
				<form action="#" method="post" class="form-box">
					<div class="row">
						<div class="col-sm-12 col-12">
							<div class="form-group service-form-group">
								<label class="control-label sr-only"  style="border:none;" for="fname">Payment method</label>
								<input id="Payment method" type="text" name="Payment method" placeholder="Payment method" class="form-control" required 
								style="border:none; padding: 0px;">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12 col-12" style="padding-left: 0px;">
							<div class="form-group service-form-group">
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="customRadio" style="font-size: 12px;"name="example" value="customEx">
									<label class="custom-control-label" for="customRadio">Master card</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="customRadio2" name="example" value="customEx">
									<label class="custom-control-label" for="customRadio2">Visa</label>
								</div>
								<div class="custom-control custom-radio custom-control-inline">
									<input type="radio" class="custom-control-input" id="customRadio3" name="example" value="customEx">
									<label class="custom-control-label" for="customRadio3">Pay pal</label>
								</div>
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-sm-6 col-12">
							<div class="form-box__single-group " style="margin-top:0px;">
								<label  for="form-State" style="font-size: 14px;"></label>
								<input type="text" id="form-State" placeholder="Card number" class="form-control" required 
								style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
							</div>
						</div>
						<div class="col-sm-6 col-12" >
							<div class="form-box__single-group" style="margin-top:0px">
								<label for="form-city"></label>
								<input type="text" id="form-city" placeholder="CVV" class="form-control" required
								style="border:none; border-bottom:1px solid #000000;  border-radius:0px; padding: 0px;">
							</div>
						</div>
					</div>
					<div class="row" >
						<div class="col-sm-6  col-12" >
							<div class="form-box__single-group" style="margin-top:0px; padding-top:15px">
								<label for="phone_number"></label>
								<input type="text" id="phone_number" placeholder="Expiry date" class="form-control" required="" 
								style="border:none;border-bottom:1px solid #000000;border-radius:0px;padding: 0px;">
							</div>
						</div>
						<div class="col-sm-6  col-12" >
							<div class="form-box__single-group" style="margin-top:0px; padding-top:15px">
								<label for="phone_number"></label>
								<input type="text" id="phone_number" placeholder="E-mail" class="form-control" required="" 
								style="border:none;border-bottom:1px solid #000000;border-radius:0px;padding: 0px;">
							</div>
						</div>
					</div>	
					<div class="row" >
							<div class=" col-sm-8" style="padding-bottom:40px; padding-top:40px;">
								<button type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight" style="border-radius:4px;">Buy</button>
							</div>	
					</div>		
				</form>
			</div>
		</div>
		<div class="col-sm-4 col-12">
			<div class="your-order-section">
				<div class="section-content ">
					<h5 class="section-content__title">YOUR CART</h5>
				</div>
					<div class="row" style="">
						<div class="col-12" style="padding-right: 30px;margin-bottom: 45px;">
							<div class=""style="display:inline-block;">
								<img class="img "  src="<?php echo base_url(); ?>assets/img/your_cart_img" alt="">
							</div>
							<div style="display:inline-block; margin-top:20px; padding:10px" >
								<label class="lorem_ipsum">LOREM IPSUM</label>
								<div class="price">$130</div>
							</div>
							<div style="display:inline-block; float:right; margin-top:40px;">
								<button class="fa fa-trash" style="display:inline-block" type="button" >
								</button>
							</div>
						</div>
					</div>
					<hr style="margin: 0;border-top: 1px solid #6e6767; ">
					<div class="row" style="">
						<div class="col-12" style="padding-right: 30px;margin-bottom: 30px;">
							<div class=""style="display:inline-block;">
								<img class="img "  src="<?php echo base_url(); ?>assets/img/your_cart_img" alt="">
							</div>
							<div style="display:inline-block; margin-top:20px; padding:10px" >
								<label class="lorem_ipsum">LOREM IPSUM</label>
								<div class="price">$130</div>
							</div>
							<div style="display:inline-block; float:right; margin-top:40px;">
								<button class="fa fa-trash" style="display:inline-block" type="button" >
								</button>
							</div>
						</div>
					</div>
					<hr style="border-top: 1px solid #6e6767;">
					<div class=" d-flex justify-content-between">
						<h5 class="your-order-total-left total_size">Total</h5>
						<h5 class="your-order-total-right total_price">$329</h5>
					</div>		
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-12">
			<button></button>
		</div>
	</div>
</div>
<div class="container">
	<div class="row" style=" padding-bottom:200px;">
		<div class="col-sm-12 col-12" >
			<div class="Details_box" style="padding:50px;">
				<div class="col-sm-12 col-12">
					<div class="section-content" style="text-align:center;">
					<h5 class="section-content__title Congratulations"  >Congratulations!</h5>
					</div>
				</div>
				<form action="#" method="post" class="form-box">
					<div class="row" >
						<div class="col-sm-12 col-12">
							<div class="form-box__single-group " style="margin-top:0px; text-align:center;">
								<h5 for="Your_shipment" style="font-weight: 300;font-size: 18px;line-height: 22px;color: #000000; padding: 10px;">Your shipment is on its way.</h5>
							</div>
						</div>
					</div>
					<div class="row" >
						<div class=" col-sm-12" style="padding-bottom:40px; padding-top:40px; text-align:center;">
							<button type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight Back_to_home_page " style="border-radius:4px;">Back to home page </button>
						</div>	
					</div>		
				</form>
			</div>
		</div>
	</div>
</div>
		<!-- /.contact-form -->
		<!-- contact-map -->
		<div id="map"></div>
		<!-- /.contact-map -->
		<!-- contact-block-section -->