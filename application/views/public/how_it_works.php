
<style>
.main_body{
background: #f5f6f9;
}
.how_does_text{
margin-top-50px;
font-family: Montserrat;
font-style: normal;
font-weight: 600;
font-size: 40px;
line-height: 49px;
letter-spacing: 0.05em;
color: #FFFFFF;
}
.lorem_paragraph{
padding-right: 10px;
color: white;
margin-top: 12px;
}
.lorem_box{
background: #FFFFFF;
border: 0.5px solid #C4C4C4;
border-radius: 5px;
}
.lorem_1{
font-family: Montserrat;
font-style: normal;
font-weight: bold;
align-items: center;
text-transform: uppercase;
color: #000000;
font-size: 16px;
line-height: 20px;
margin-top: 25px;
margin-left: 30px;
margin-botom: 20px;
padding-bottom:20px;
}
.lorem_2{
font-family: Montserrat;
font-size: 14px;
line-height: 17px;
align-items: center;
color: #636363;
margin-left: 30px;
padding-bottom:20px;
}
.add_pink_color{
font-family: Montserrat;
font-weight: 600;
font-size: 24px;
line-height: 29px;
display: flex;
align-items: center;
color: #E0317E;
}
.add_black_color{
font-family: Montserrat;
font-weight: 600;
font-size: 24px;
line-height: 29px;
display: flex;
align-items: center;
color: #000000;
}
.close_sign{
position: absolute;
right: 15px;
}
.col_box{
background-color:white;
}
.black .lorem_box{background: #516177;}
.black .lorem_1{color:white;}
.black .lorem_2{color:white;}
.black .main_body{background: black;
}
.black .add_white_color{color:white;}
.black .col_box{background-color:#516177;}
.black .col_box{background-color:#516177;}



</style>
	<div class="content main_body" style="">
	    <div style="background-image: url(<?php echo base_url(); ?>assets/img/bacgroundimg.png); background-size: cover;">
            <div class="container">
                <div class="row">
			        <div class="col-sm-7 col-12" style="padding-top: 120px;">
				     <div class="how_does_text mt-10"> How does it work? </div>
				      <p class="mt-10 lorem_paragraph" style="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
					  incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis 
					  nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				      </p>
				    </div>
				    <div class=" col-sm-5  col-12">
				      <div class="box" style="border: 1px solid #000000; box-sizing: border-box; margin-top:50px;">
			            <img class="img " src="<?php echo base_url(); ?>assets/img/img.png" alt="">
				      </div>
				    </div>
			    </div>
		    </div>
        </div>
   
			<div class="container mt-20">
                <div class="row" style="margin-top: 40px;">
                        <div class=" col-sm-4 col-12 ">
						  <div class="lorem_box" style="padding-bottom:20px;">
						        <div class="lorem_1"> LorEM IPSUM</div>
								<div class="lorem_2">-Lorem ipsum</div>
								<div class="lorem_2">-Lorem ipsum</div>
						        <div class="lorem_2">-Lorem ipsum</div>
						        <div class="lorem_2">-Lorem ipsum</div>
						  </div>
						</div>
						 
						<div class=" col-sm-4 col-12" >
						    <div class="lorem_box" style="padding-bottom:20px;">
						        <div class="lorem_1" > LorEM IPSUM</div>
								 <div class="lorem_2">-Lorem ipsum</div>
								 <div class="lorem_2">-Lorem ipsum</div>
						         <div class="lorem_2">-Lorem ipsum</div>
						         <div class="lorem_2">-Lorem ipsum</div>
						    </div>
						</div>
						
						<div class=" col-sm-4 col-12">
						    <div class="lorem_box" style="padding-bottom:20px;" >
						        <div class="lorem_1" >LorEM IPSUM </div>
								<div class="lorem_2">-Lorem ipsum</div>
						        <div class="lorem_2">-Lorem ipsum</div>
						        <div class="lorem_2">-Lorem ipsum</div>
						        <div class="lorem_2">-Lorem ipsum</div>
						    </div>
						</div>        
                </div>	  
            </div>
	    <div class="container" style="padding-top:40px; padding-bottom:100px;">
		    <div class="accordion" id="accordionExample">
                <div class="card">
                    <div class="card-header col_box" id="headingOne" style="">
                        <h2 class="mb-0" style="padding:20px;">
							<button class="btn btn-link add_black_color open_close_btn add_white_color fa fa-close" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<i class="fa fa-plus i_cons close_sign" style="float: right;"></i>About Company
							</button>
						</h2>
					</div>

					<div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body" style="margin-left:45px;">
						Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. 
						Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a 
						bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
						Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, 
						raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header col_box" id="headingTwo" style="">
						<h2 class="mb-0" style="padding:20px">
							<button class="btn btn-link collapsed add_black_color add_white_color open_close_btn  fa fa-close" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="">
								<i class="fa fa-plus i_cons close_sign" style="float: right;"></i>Our Mission
							</button>
						</h2>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
						<div class="card-body" style="margin-left:45px;">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.


						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header col_box" id="headingThree" style="">
						<h2 class="mb-0" style="padding:20px">
							<button class="btn btn-link collapsed add_black_color add_white_color open_close_btn  fa fa-close" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							<i class="fa fa-plus i_cons close_sign" style="float: right;"></i>Our Vision
							</button>
						</h2>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
						<div class="card-body" style="margin-left:45px;">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. 
							Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, 
							craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, 
							raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header col_box" id="headingFour" style="">
						<h2 class="mb-0" style="padding:20px">
							<button class="btn btn-link collapsed open_close_btn  fa fa-close add_white_color add_black_color" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							<i class="fa fa-plus i_cons close_sign" style="float: right;"></i>Our Goal
							</button>
						</h2>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
						<div class="card-body" style="margin-left:45px;" style="">
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, 
							non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a 
							bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. 
							Ad vegan excepteur butcher vice lomo. 
							Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
						</div>
					</div>
				</div>
			</div>
	
		<script>
$('document').ready(function(){
	$('body').on('click', '.open_close_btn', function () {	
		$('.open_close_btn').addClass('add_black_color').removeClass('add_pink_color');
		$('.open_close_btn').find('i').addClass('fa-plus').removeClass('fa-times');
		if($(this).find('i').hasClass('fa-times')){
			$(this).find('i').removeClass('fa-times').addClass('fa-plus');
		}else{
			$(this).find('i').removeClass('fa-plus').addClass('fa-times');
		}
		if($(this).hasClass('add_pink_color')){
			$(this).removeClass('add_pink_color').addClass('add_black_color');
		}else{
			$(this).removeClass('add_black_color').addClass('add_pink_color');
		}
		
		
	});
	
});
	    </script>
		</div>
	</div>