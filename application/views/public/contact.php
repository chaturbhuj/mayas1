
<style>
.address-info{
font-family: Montserrat;
font-size: 18px;
font-style: normal;
font-weight: 400;
line-height: 22px;
letter-spacing: 0em;
text-align: left;
}
.telephone-info{
	font-family: Montserrat;
font-size: 18px;
font-style: normal;
font-weight: 400;
line-height: 22px;
letter-spacing: 0em;
text-align: left;
}
.email_info{
	font-family: Montserrat;
font-size: 18px;
font-style: normal;
font-weight: 400;
line-height: 22px;
letter-spacing: 0em;
text-align: left;
}
.contact_block{
	margin-bottom: 30px;
}
</style>
    <!-- /.header -->
    <!-- page-header -->
    <div class="page-header" style="margin-top: 30px; margin-left: 100px;">
        <div class="container">
            <div class="row">
                <!-- page caption -->
                <div class="">
                    <div class="page-caption">
                        <h1 class="page-title">Write to us</h1>
                    </div>
                </div>
                <!-- /.page caption -->
            </div>
        </div>
    </div>
    <!-- /.page-header -->
    <!-- contact-form -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class=" col-xl-6 col-lg-8 col-md-12 col-sm-12 col-12 mb60">
                    <form id="contact_form" method="post">
						<div id="headMsg"></div>
                        <!-- form -->
                        <div class="contact-form">
                            <div class="row">
                              
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                    <!-- Text input-->
                                    <div class="form-group service-form-group">
                                        <label class="control-label sr-only" for="fname"></label>
                                        <input id="fname" type="text" name="fname" placeholder="First Name" class="form-control" required>
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  ">
                                    <div class="form-group service-form-group">
                                        <label class="control-label sr-only" for="lname"></label>
                                        <input id="lname" type="text" name="lname" placeholder="Last Name" class="form-control" required>
                                    </div>
                                </div>
                                <!-- Text input-->
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="form-group service-form-group">
                                        <label class="control-label sr-only" for="contact_phone"></label>
                                        <input id="contact_phone" type="number" name="contact_phone" placeholder="Telephone" class="form-control" required>
                                    </div>
                                </div> 
								
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <div class="form-group service-form-group">
                                        <label class="control-label sr-only" for="contact_email"></label>
                                        <input id="contact_email" type="email" name="contact_email" placeholder="Email" class="form-control" required>
                                    </div>
                                </div>
                                <!-- select -->
                               
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <!-- textarea -->
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="contact_message"></label>
                                        <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="Messages"></textarea>
                                    </div>
                                </div>
                                <!--button -->
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <button type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue btn--uppercase btn--weight">submit</button>
                                </div>
                            </div>
                        </div>
                        <!-- /.form -->
                    </form>
					<script>
						$('document').ready(function () {
											
								 $('#contact_form').validate({
									ignore: [],
									rules: {
										fname: {
											required: true,
										},
										lname: {
											required: true,
										},
										contact_phone: {
											required: true,
										},
										contact_email: {
											required: true,
										},
										
										contact_message: {
											required: true,
										},
									},
									messages: {
										fname: {
											required: "First name is required",
										},
										lname: {
											required: "Last name is required"
										},
										contact_phone: {
											required: "Telephone number is required"
										},
										contact_email: {
											required: "Email is required"
										},
										
										contact_message: {
											required: "message is required"
										},
									},
									submitHandler: function (form) {
										$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
											css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
											  overlayCSS:  { cursor:'wait'} 
											});
										var first_name = $('#fname').val();
										var last_name = $('#lname').val();
										var contact_phone = $('#contact_phone').val();
										var contact_email = $('#contact_email').val();
										
										var contact_message = $('#contact_message').val();
										
										$.post(APP_URL+'api/publicpages/contact_form_submit',{
											first_name: first_name,
											last_name: last_name,
											contact_phone: contact_phone,
											contact_email: contact_email,
											
											contact_message: contact_message,
										},function(response){
											$.unblockUI();
											$("html, body").animate({scrollTop: 0}, "slow");
											$('#headMsg').empty();
											if (response.status == 200) {
												var message = response.message;
												$('#headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
												$("#headMsg").fadeTo(2000, 500).slideUp(500, function(){
													$('#headMsg').empty();
													window.location.href = APP_URL+'publicpages/contact';
											
												});
											} else if (response.status == 201) {
												$('#headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
											}
										},'json');
										return  false;
									}
								});
						})
					</script>
                </div>
                <div class="offset-xl-2 col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
				<div class="page-header">
                  <div class="container">
                     <div class="row">
                         <div class="page-caption">
                          <h1 class="page-title">Contacts</h1>
                         </div>
                     </div>
                  </div>
                </div>
                    <div class="contact_block address-info">
						<p>
						Address:<br>
						Spyrou Kyprianou, 4003,<br>
 						Limassol, Cyprus.</p>
                    </div>
               
                    <div class="contact_block telephone-info">
							 <p>
							 Telephone:<br>
							 +38 067 327 21 34<br>
							 +38 067 327 21<br>
							 21 +38 050 469 87 89
							</p>
                    </div>
                    <div class="contact_block email_info">
						<p>
						E-mail: <br>
						E-mail: dima.huz@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.contact-form -->
    <!-- contact-map -->
    <div id="map"></div>
    <!-- /.contact-map -->
    <!-- contact-block-section -->