<style>
@media only screen and (max-width: 960px){
	.for-mobile {
		display: inline;
	}
	.Invoice-main-div{
		padding: 16px 15px !important;
	}
}
</style>
<?php 
//var_dump($order_detail[0]);
?>
<div class="Invoice-main-div" style="font-family: Arial, sans-serif !important;width: 100%; background: #ffff; padding: 30px 35px; max-width: 800px; margin: auto; border: 2px solid #90cbcbf2; border-radius: 15px; box-shadow: 0px 0px 2px #dedede;">

	<div style="margin-right: -15px; margin-left: -15px; width:100%">
		<div style="position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; width: 100%;">
			<div style="width: 100%; background: none;">
				<img style="float: right; width: 170px; height: 170px;" src="<?php echo base_url().LOGO;?>" alt="">
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div style="margin-right: -15px; margin-left: -15px; margin-top: 20px; width: 100%;">
		<div class="col2" style=" width: 50%; float: left; position: relative; min-height: 1px;">
			<div style="width: 100%; background: none; padding-right: 15px; padding-left: 15px;">
				<div><?php echo $order_detail[0]->first_name.' '.$order_detail[0]->last_name;?> </div>
				<div><?php echo $order_detail[0]->address_line; ?> </div>
				<div><?php echo $order_detail[0]->zip_code; ?> </div>
				<div><?php echo $order_detail[0]->phone_number; ?> </div>
			</div>
		</div>
		<div  class="col2" style="width: 50%; float: left; position: relative; min-height: 1px;">
			<div style="width: 100%; background: none; padding-right: 15px; padding-left: 15px;">
				<div class="pull-right" style="float: right;">Invoice Date: &nbsp; <?php $date=strtotime($order_detail[0]->created_date);
					echo date("d/m/Y",$date);?></div> 
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>
	<div style=" margin-top: 50px; margin-bottom: 0px;">
		<div style="position: relative; min-height: 1px; width: 100%;">
			<div class="invoice_head" style="font-size: 22px; text-align: center; text-transform: uppercase; "><b>Invoice # (<?php echo $order_detail[0]->order_id; ?>)</b></div>
			<hr>
			<div class="invo_table" style="width: 100%; background: none; margin-top: 15px;">
				<table class="table" style="text-align: left; width: 100%; max-width: 100%; margin-bottom: 20px; background-color: transparent;border-spacing: 0; border-collapse: collapse;">
					<thead style="font-size: 20px; font-weight: bold;">
						<tr>
							<th style="border: 1px solid #333; padding: 10px 5px;">Description</th>
							<th class="" style="border: 1px solid #333; padding: 10px 5px;">Qty</th>
							<th style="border: 1px solid #333; padding: 10px 5px;">Price</th>
							<th class="" style="border: 1px solid #333; padding: 10px 5px;">Total <span class="hidden-xs">Price</span></th>
						</tr>
					</thead>
					<tbody style="font-size: 20px; color: #000; text-align: left; font-size: 17px;">
						 <?php 
							//var_dump($order_detail[0]);
							$grand_total=0.00;
							if(isset($order_detail[0]->coupone_persent)){
								$discount=$order_detail[0]->coupone_persent;
							}else{
								$discount=0.00;
							}
							$discount_amount=0.00;
							$total=0.00;
							
							$i=0;
						if(!empty($item_detail)){
							foreach($item_detail as $itm){
								//var_dump($itm);
								$i=$i+1;
								
								$total=$itm->product_total+$total;
								$charges=($itm->product_actual_prize-$itm->product_discounted_prize)
								
								
									?>
								  <tr>
									<td style="border: 1px solid #333; padding: 10px 5px;"><?php echo $itm->product_name; ?></td>
									<td class="" style="border: 1px solid #333; padding: 10px 5px;"><?php echo $itm->quantity; ?></td>
									<td style="border: 1px solid #333; padding: 10px 5px;">₹<?php echo  $charges; ?></td>
									<td class="" style="border: 1px solid #333; padding: 10px 5px;">₹<?php echo  $charges; ?></td>
								  </tr>
								<?php 
							}							
						} 
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div style="clear: both;"></div>
	</div>

	
		
</div>