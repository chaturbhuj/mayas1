<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="<?php echo base_url();?>assets/js/configure/constant.js"></script>
<script src="<?php echo base_url();?>assets/js/configure/shopcart.js"></script>
	<style type="text/css">
	  label {
		display: inline-block;
		/* margin-bottom: .5rem; */
	}
	.custom-control-label {
		position: relative;
		margin-bottom: 0;
		vertical-align: top;
	}
	.custom-radio .custom-control-input:checked~.custom-control-label::after {
    background-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='12' height='12' viewBox='-4 -4 8 8'%3e%3ccircle r='3' fill='%23fff'/%3e%3c/svg%3e);
	}
	.custom-control-label::after {
		position: absolute;
		top: .25rem;
		left: -1.5rem;
		display: block;
		width: 1rem;
		height: 1rem;
		content: "";
		background: no-repeat 50%/50% 50%;
	}
	   .checkout-step .card .card-header .btn.disabled, .checkout-step .card .card-header .btn:disabled {
      opacity: 1;
    }   
    .Delivery-datetime .nav-item{
        padding: 0 10px;
    }
    .Delivery-datetime .scrollable{
        max-height: 200px;
        overflow-y: auto;
    }
    .Delivery-datetime .nav-pills .nav-link {
        background-color: #eee;
        color:#000;
    }
    .Delivery-datetime .nav-pills .nav-link.active, .Delivery-datetime .nav-pills .show>.nav-link {
        color: #fff;
        background-color: #007bff;
    }
    .driver_tips .nav-item{
        padding: 0 10px;
    }
    .driver_tips .nav-pills .nav-link {
        background-color: #eee;
        color:#000;
    }
    .driver_tips .nav-pills .nav-link.active, .driver_tips .nav-pills .show>.nav-link {
        color: #fff;
        background-color: #007bff;
	}
		.main_body{
			background: #E5E5E5;
	}
	.section-content__title{
		font-family: Montserrat;
		font-weight: bold;
		font-size: 36px;
		line-height: 44px;
		color: #000000;
		opacity: 0.8;
	}

	.name{
		font-family: Montserrat;
		font-style: normal;
		font-weight: normal;
		font-size: 14px;
		line-height: 17px;
		color: #000000;
		opacity: 0.5;
		opacity: 0.5;
		border: 1px solid #000000;
	}
	.productname{
		font-family: Montserrat;
		font-style: normal;
		font-weight: 500;
		font-size: 12px;
		line-height: 15px;
	
	}
	.productprice{
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 12px;
		line-height: 30px;
	
	
	}
	.bottom_border{
		border: none;
		border-bottom: 1px solid #000000;
		border-radius: 0px;
		opacity: 0.5;
	}

	.lorem_ipsum{
		font-family: Montserrat;
		font-style: normal;
		font-weight: 500;
		font-size: 12px;
		line-height: 15px;
		color: #000000;
		opacity: 0.5;
	}
	.price{
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 12px;
		line-height: 15px;
		color: #000000;
	}
	.img{
		width: 70px;
		padding: 10px;
		background: #FFFFFF;
		border: 1px solid #C4C4C4;
		box-sizing: border-box;
		border-radius: 2px;
		margin-right: 30px;
	
	}

	.hr_line{
		opacity: 0.5;
		border: 1px solid #000000;
		transform: matrix(1, 0, 0, 1, 0, 0);
	}
	.border_bottom_product{
		opacity: 0.5;
		border: 1px solid #000000;
		transform: matrix(1, 0, 0, 1, 0, 0);
	}

	.total_size{
		font-family: Montserrat;
		font-weight: 500;
		font-size: 18px;
		line-height: 22px;
		color: #000000;
		opacity: 0.5;
	}
	.total_price{
		font-family: Montserrat;
		font-style: normal;
		font-weight: bold;
		font-size: 18px;
		line-height: 22px;
		color: #000000;
	}
	.Congratulations{
		font-family: Montserrat;
		font-style: normal;
		font-weight:bold;
		font-size: 26px;
		margin-top:30px;
		color: #000000;
		opacity: 0.8;

		text-align:center;
		padding-bottom: 30px;
	}
	.Your_shipment{
		font-family: Montserrat;
		font-style: normal;
		font-weight: 300;
		font-size: 18px;
		line-height: 22px;
		color: #000000;
	}
	.Back_to_home_page{
		font-family: Montserrat;
		font-style: normal;
		font-weight: 500;
		font-size: 12px;
		line-height: 15px;
		color: #FFFFFF;

	}
	.cart-list-product{
		margin-top:5px;
	}
	.common_circle{
		padding: 10px 20px;
		border: 1px solid #6a656559;
		border-radius: 50px;background: #fff;
		
		
	}
	.active_circle{
		padding: 8px 18px;
		background: #d69512;
		border-radius: 50px;
		border: 3px solid white;
		box-shadow: 0px 0px 1px 2px #d69512;
		margin-top: 10px;
	}
	.oko{
		width: 86px;
		border-bottom: 1px solid;
		margin: 40px 10px 10px 10px!important;
		padding: 0px 99px 0px;
	}
	.total{
		font-family: Montserrat;
		font-style: normal;
		font-weight: 500;
		font-size: 18px;
		line-height: 22px;
	}
    .Details_box {
		background: #FFFFFF;
		box-shadow: 2px 2px 40px rgba(0, 0, 0, 0.2);
		padding: 40px;
		margin-top: 10px;
	}
	.back_line{
		border-bottom: 4px solid #d69512; 
		transform: matrix(1, 0, 0, 1, 0, 0);
		position: absolute;

	}
	
	.left_image_slide{
		
		margin-bottom:50px;
	}	
	
	.active_class_choosed {
		border: 1px solid #69b52d;
	   
		margin-right: 13px;
		margin-left: 13px;
	}
	.active_class_before {
	   
	   
		color:#e0317e;
	}
	.add_card_payment_button{
		margin-bottom:20px;
		padding:15px;
	}
	.display_none {
	  display: none; 
	}
	.float-right{
		float:right;
	}	
	.input_buttons{
		background: #FFFFFF 0% 0% no-repeat padding-box;
		box-shadow: 0px 4px 6px #00000029;
		border: 1px solid #EFEFEF;
		border-radius: 10px;
		opacity: 1;
	}
	.submit_next_button{
		background: #0857DE 0% 0% no-repeat padding-box;
		border: 1px solid #0857DE;
		border-radius: 10px;
		opacity: 1;
	}
	.modal-backdrop.in {
		filter: alpha(opacity=50);
		opacity: -0.5;
	}
	.modal-backdrop {
	   position: inherit; 
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		z-index: 1040;
		background-color: #000;
	}
	</style>
	<div style="background: #E5E5E5;">
 
		<div class="container all_circle_remove">	
			<div class="row">
				<div class="col-md-7 col-12">
					<div class="" style="margin-left:30px;">
						<div class="" style="margin: 0px; margin-top: 60px;">
							<hr style="border-top: 2px  solid #d69512;">
						
						</div>
						<div class="row" style="position: relative; top: -40px;">
							<div class="col" style="padding: 0px;">
								<button class="common_circle active_circle first_common_circle">&nbsp;</button>
								<button class="common_circle first_active_circle display_none " style="margin-left: -50px;">&nbsp;</button>
								<h5 class="active_detail_name1 display_none" style="color:#b1a7a7; margin-bottom:10px; font-size:14px; margin-top: 10px;">details</h5>
								<h5  class="active_detail_name2" style="color:#000000; font-weight: bold; margin-bottom:10px; font-size:14px; margin-top: 10px;">details</h5>
										
							</div>
							<div class="col" style="padding: 0px;  text-align: center; margin-top: -79px;">
								<button class="common_circle  active_circle  payment_circle display_none" style="margin-top: -7px;">&nbsp;</button>
								<button class="common_circle    payment_circle address_circle" style="margin-top: -7px;">&nbsp;</button>
								<h5  class="active_payment_name1"style="color:#b1a7a7; margin-bottom:10px; font-size:14px; margin-top: 10px;">payment</h5>
								<h5  class="active_payment_name2 display_none"  style="color:#000000;  font-weight: bold; margin-bottom:10px; font-size:14px; margin-top: 10px;">payment</h5>
								
							</div>
							<div class="col" style="padding: 0px; text-align: right; margin-right: 12px;margin-top: -88px;">
								<button class="common_circle  active_circle  display_none final_circle" style="margin-right: -49px;">&nbsp;</button>
								<button class="common_circle  last_circle">&nbsp;</button>
								<h5 class="active_succes_name2" style="color:#b1a7a7; margin-bottom:10px; font-size:14px; margin-top: 10px;">succes</h5>
								<h5  class="display_none active_succes_name" style="color:#E0317E;  font-weight: bold; margin-bottom:10px; font-size:14px; margin-top: 10px;">succes</h5>
							
							</div>
							
						</div>
					</div>
				</div>
				<div class="col-md-8 col-12"></div>
				
					
			</div>
		</div>  
	<div class="container">
		<div class="row">		
			<div class="col-sm-8 col-12 ">
				<input type="hidden" id="order_id" value="">
				<input type="hidden" id="checkout_email" value="">
				<input type="hidden" id="selected_customer_address_id" value="">
				<input type="hidden" id="selected_first_name" value="">
				<input type="hidden" id="selected_last_name" value="">
				<input type="hidden" id="selected_address_line" value="">
				<input type="hidden" id="selected_city_name" value="">
				<input type="hidden" id="selected_state_name" value="">
				<input type="hidden" id="selected_zip_code" value="">
				<input type="hidden" id="selected_country_name" value="">
				<input type="hidden" id="selected_shipping_phone_number" value="">
					
				
				<div class="Details_box delivery_address_box left_image_slide">
					<div class="section-content m-t-5 m-b-30">
						<h5 class="section-content__title" style="padding-top:0px;display: inline-block; ">DELIVERY ADDRESS</h5>
					</div>
					<div id="append_customer_address"></div>
					<div class="no_data_found_address">
						<div id="checkout_headMsg"></div>
						<form id="addrees_form">
							<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
							<input type="hidden" id="customer_address_id" value="<?php echo $this->session->userdata('customer_address_id');?>">
							<div class="row" style="padding:10px;">
								<div class="col-md-6"><input type="text" id="firstt_name" name="first_name" class="form-control" placeholder="Enter First Name"></div>
								<div class="col-md-6"><input type="text" id="lastt_name" name="last_name" class="form-control" placeholder="Enter Last Name"></div>
							</div>
							<div class="row" style="padding:10px;">
								<div class="col-md-12">
									<input type="email" id="emaill_id" name="email_id"  class="form-control" placeholder="Enter email"> 
								</div>
							</div>
							<div class="row" style="padding:10px;">
								<div class="col-md-12">
									<input type="number" class="form-control"  id="phonee_numbers" name="phone_numbers" placeholder="Enter Phone Number"> 
								</div>
							</div>
							<div class="row" style="padding:10px;">
								<div class="col-md-12">
									<textarea type="text" class="form-control" id="addresss_lines" name="address_lines" placeholder="Enter Address"></textarea> 
								</div>
							</div>
							<div class="row" style="padding:10px;">
								<div class="col-md-3"><input type="text" class="form-control" id="countryy" name="country"  placeholder="Country Name"></div>
								<div class="col-md-3"><input type="text" class="form-control" id="statee" name="state"  placeholder="State Name"></div>
								<div class="col-md-3"><input type="text" class="form-control" id="cityy" name="city"   placeholder="City Name"></div>
								<div class="col-md-3"><input type="number" class="form-control" id="zipcodee" name="zipcode" placeholder="Zipcode"></div>
							</div>
							<div class="row" style="padding:10px;">
								<div class="col-md-5">
									
								</div>
								<div class="col-md-2">
									<button class="btn btn-primary">Next</button>
								</div>
								<div class="col-md-5">
								</div>
							</div>
						</form>
												
					</div>
				</div>
				
				<div class="Details_box payment payment_box display_none" style="padding:50px; margin-bottom: 50px;">
					<div class="section-content m-t-5 m-b-30">
					
						<h5 class="section-content__title" style="display: inline-block;">PAYMENT</h5>
						<div type="hidden" id="headMsg" value="">
						</div>
					</div>
						
						<div class="row">
							<div class="col-sm-12 col-12" style="padding-left: 0px;">
								
								<div class="form-group service-form-group">
									<div class="form-group" style="display: flex; ">
										<div class="custom-control custom-radio custom-control-inline" style="margin:10px;">
											<input type="radio" id="paymentCash" name="paymentRadio"  class="custom-control-input cash_option paymentRadio" value="cash">
											<label class="custom-control-label "for="paymentCash"> Cash</label>
										</div>
										<div class="custom-control custom-radio custom-control-inline" style="margin:10px;">
											<input type="radio" name="paymentRadio" id="paymentStripe" checked  class="custom-control-input card_option paymentRadio"  name="example" value="net_banking">
											<label class="custom-control-label" for="paymentStripe">card / Net-banking</label>									
										</div>									
									</div>						
								</div>
							</div>
						</div>	
						<div id="headMsg_remove_card"></div>						
						<div id="add_card_data" class=" card_div  display_nonne">
						
						</div>				
						<div class="row" >
							<div class=" col-sm-8" style="padding-bottom:40px; padding-top:40px;">									
								<button type="submit"  class="btn btn--box   btn--large btn--blue btn--uppercase btn--weight m-t-20 m-l-2 final_submit">payment</button>
							</div>	
						</div>		
						
				</div>
			</div>	
		
			<div class="col-sm-4 col-12 cart_box">
				<div class=" your-order-section">
					<div class="section-content ">
						<h5 class="section-content__title" style="font-weight:bold;">YOUR CART <span class="text-danger float-right"></h5>
					</div>				
					<div class="row" style="">
						<div class="col-12" style="padding-right: 30px;margin-bottom: 45px;">
														
							<div class="cart-list-product" style="border-bottom 1px solid #000000;">								
								<a class="card-body pt-0 pr-2 pl-2 pb-0" id="append_cart_data"></a>																		
							</div>							
							<div class="cart-list-product" >
									<h5 class="pl-2 total">Total :<span class="float-right" id="append_cart_total"></span></h5>
							</div><br/>
							<div class="cart-list-product">
									<h5 class="pl-2 total">Product Details :<span class="float-right " id="append_other_charges_total"></span></h5>
							</div><br/>
							<div class="before_coupone_applay">
									<button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addCouponModal"><strong class="h5"><i class="mdi mdi-plus-circle-outline">+</i></strong></button>
										<h5 class="pl-2 total">Have Coupon :  </h5>
										
							</div><br/>
							<div class="cart-list-product applied_coupan display_none" >
								<input type="hidden" id="other_fees" value="">
								<h5 class="pl-2 total" class=""> Discount :<span class="float-right" style="" id="append_coupon_discount_carges"></span></h5>
							</div><br/>
							<div class="after_coupone_applay display_none">
								<button class="btn btn-danger float-right remove_coupone_code" title="remove"><stromg class="h5"><i class="mdi mdi-close-circle-outline"></i></stromg></button>
								<h5>Coupon: <span class="float-right" id="append_coupon_discount_carges"></span></h5>
							</div>
							<div class="cart-list-product" >
								<h5 class="pl-2 total">Final Amount : <span class="float-right" id="append_final_amount"></span></h5>
							</div><br>											
								
						</div>
					</div>
				</div>     
			</div>
		</div>
	</div>
		
</div>
<!-- Modal -->
<div class="modal fade" id="addCustomerAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" id="exampleModalLabel">Delivery Address</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
		<div id="checkout_headMsg"></div>
		<form id="add_customer_address_data" method="post">
			<input type="hidden" id="checkout_customer_address_id" value="">
			<div class="row">
			   <div class="col-sm-6">
				  <div class="form-group">
					 <label class="control-label">First Name <span class="required">*</span></label>
					 <input class="form-control border-form-control" id="checkout_firt_name" name="checkout_firt_name" value="" placeholder="Enter First Name" type="text">
				  </div>
			   </div>
			   <div class="col-sm-6">
				  <div class="form-group">
					 <label class="control-label">Last Name <span class="required">*</span></label>
					 <input class="form-control border-form-control" id="checkout_last_name" name="checkout_last_name" value="" placeholder="Enter Last Name" type="text">
				  </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-sm-6">
				  <div class="form-group">
					 <label class="control-label">Phone <span class="required">*</span></label>
					 <input class="form-control border-form-control" id="checkout_phone_number" name="checkout_phone_number" value="" placeholder="Enter Phone Number" type="number">
				  </div>
			   </div>
			   <div class="col-sm-6">
				  <div class="form-group">
				  </div>
			   </div>
			</div>
			<div class="row">
			   <div class="col-sm-6">
				  <div class="form-group">
					
					<label class="control-label">Country <span class="required">*</span></label>
						<input class="form-control border-form-control" id="checkout_country_id" name="checkout_country_id" value="" placeholder="Enter Country Name" type="text">
				  </div>
			   </div>
			   <div class="col-sm-6">
					<div class="form-group">
						<label class="control-label">State <span class="required">*</span></label>
					 	<input class="form-control border-form-control" id="checkout_state_id" name="checkout_state_id" value="" placeholder="Enter state Name" type="text">
					</div>
			   </div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">				
						<label class="control-label">City <span class="required">*</span></label>
						<input class="form-control border-form-control" id="checkout_city_id" name="checkout_city_id" value="" placeholder="Enter city Name" type="text">					
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group">
						<label class="control-label">Zip Code <span class="required">*</span></label>
						<input class="form-control border-form-control" id="checkout_zip_code" name="checkout_zip_code" value="" placeholder="Enter Zip Code" type="text">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="control-label">Shipping Address <span class="required">*</span></label>
						<textarea class="form-control border-form-control" id="checkout_address_line" name="checkout_address_line"></textarea>
						<small class="text-danger">Please provide the number and street.</small>
					</div>
			   </div>
			</div>
		 
			<button type="submit" class="btn btn-primary mb-2 btn-lg">Add Addresss</button>
		  </form>
	  </div>
	</div>
  </div>
</div>
		
	<!-- Modal -->
	<div class="modal fade" id="addCustomerAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Delivery Address</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<div id="checkout_headMsg"></div>
			<form id="add_customer_address_data" method="post">
				<input type="hidden" id="checkout_customer_address_id" value="">
				<div class="row">
				   <div class="col-sm-6">
					  <div class="form-group">
						 <label class="control-label">First Name <span class="required">*</span></label>
						 <input class="form-control border-form-control" id="checkout_firt_name" name="checkout_firt_name" value="" placeholder="Enter First Name" type="text">
					  </div>
				   </div>
				   <div class="col-sm-6">
					  <div class="form-group">
						 <label class="control-label">Last Name <span class="required">*</span></label>
						 <input class="form-control border-form-control" id="checkout_last_name" name="checkout_last_name" value="" placeholder="Enter Last Name" type="text">
					  </div>
				   </div>
				</div>
				<div class="row">
				   <div class="col-sm-6">
					  <div class="form-group">
						 <label class="control-label">Phone <span class="required">*</span></label>
						 <input class="form-control border-form-control" id="checkout_phone_number" name="checkout_phone_number" value="" placeholder="Enter Phone Number" type="number">
					  </div>
				   </div>
				   <div class="col-sm-6">
					  <div class="form-group">
					  </div>
				   </div>
				</div>
				<div class="row">
				   <div class="col-sm-6">
					  <div class="form-group">
						<input type="hidden" id="session_country_id" value="<?php echo $this->session->userdata('country_id'); ?>">
						 <label class="control-label">Country <span class="required">*</span></label>
						 <select class="form-control border-form-control" id="checkout_country_id" name="checkout_country_id">												
						 </select>
					  </div>
				   </div>
				   <div class="col-sm-6">
					  <div class="form-group">
						<input type="hidden" id="session_state_id" value="<?php echo $this->session->userdata('state_id'); ?>">
						 <label class="control-label">State <span class="required">*</span></label>
						 <select class="form-control border-form-control" id="checkout_state_id" name="checkout_state_id">											
						 </select>
						  <select class="form-control border-form-control" id="checkout_state_id_temp" style="display:none">											
						 </select>
					  </div>
				   </div>
				</div>
				<div class="row">
				   <div class="col-sm-6">
					  <div class="form-group">
						<input type="hidden" id="session_city_id" value="<?php echo $this->session->userdata('city_id'); ?>">
						 <label class="control-label">City <span class="required">*</span></label>
						<select class="select2 form-control border-form-control" id="checkout_city_id" name="checkout_city_id">					
						</select>
						<select class="select2 form-control border-form-control" id="checkout_city_id_temp" style="display:none">					
						</select>
					  </div>
				   </div>
				   <div class="col-sm-6">
					  <div class="form-group">
						 <label class="control-label">Zip Code <span class="required">*</span></label>
						 <input class="form-control border-form-control" id="checkout_zip_code" name="checkout_zip_code" value="" placeholder="Enter Zip Code" type="text">
					  </div>
				   </div>
				</div>
				<div class="row">
				   <div class="col-sm-12">
					  <div class="form-group">
						 <label class="control-label">Shipping Address <span class="required">*</span></label>
						 <textarea class="form-control border-form-control" id="checkout_address_line" name="checkout_address_line"></textarea>
						 <small class="text-danger">Please provide the number and street.</small>
					  </div>
				   </div>
				</div>
			 
				<button type="submit" class="btn btn-secondary mb-2 btn-lg">Add Addresss</button>
			  </form>
		  </div>
		</div>
	  </div>
	</div>
<!-- Modal end -->

  <!-- Modal -->
	<div class="modal fade" id="addCouponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Coupon Code</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="applaycoupon_form" method="post">
					<div class="modal-body">
						<div id="coupon_code_headMsg"></div>
							<input type="hidden" id="checkout_customer_address_id_" value="0">              
							<div class="form-group">
								<label class="control-label">Enter Code <span class="required">*</span></label>
								<input class="form-control border-form-control" id="checkout_coupon_code" name="checkout_coupon_code" value="" placeholder="Enter code" type="text">
							</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Apply</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="container" style="margin-top: 40PX;">
		<div class="row congratulation_box display_none">
			<div class="col-sm-12 col-12">
				<div class="Details_box" style="margin-bottom: 50px;">
					<div class="section-content" style="text-align:center;">
						<h5 class="section-content__title Congratulations" style="display: inline-block;">Congratulations!</h5>
					</div>
					<form action="#" method="post" class="form-box ">
						<div class="row" >
							<div class="col-sm-12 col-12">
								<div class="form-box__single-group " style="margin-top:0px; text-align:center;">
								
									<h5 for="Your_shipment" style="font-weight: 300;font-size: 18px;line-height: 22px;color: #000000; padding: 10px;">Your shipment is on its way.</h5>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class=" col-sm-12" style="padding-bottom:40px; padding-top:40px; text-align:center;">
								<a href="<?php echo base_url(); ?>" type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight Back_to_home_page " style="border-radius:4px;">Back to home page </a>
							</div>	
						</div>		
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row cart_empty_page display_none">
			<div class="col-sm-12 col-12">
				<div class="Details_box" style="margin-bottom: 50px; margin-top: 50px; ">
					<div class="section-content" style="text-align:center;">
					
						<h2 class="section-content__title Congratulations">cart is empty  <i class="icon-shopping-cart" aria-hidden="true"></i></h2>
						
					</div>
					<form action="#" method="post" class="form-box ">
						<div class="row" >
							<div class="col-sm-12 col-12">
								<div class="form-box__single-group " style="margin-top:0px; text-align:center;">
								
									<h5 for="Your_shipment" style="font-weight: 300;font-size: 18px;line-height: 22px;color: #000000; padding: 10px; display: contents;">choose the product.</h5>
								</div>
							</div>
						</div>
						<div class="row" >
							<div class="col-sm-12" style="padding-bottom:40px; padding-top:40px; text-align:center;">
								<a  href="<?php echo base_url(); ?>"  type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight Back_to_home_page " style="border-radius:4px;">Back to home page</a> 
							
							</div>	
						</div>		
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
	<!---other charges detail modal---->
	<div class="modal fade other charges " id="add_other_charges_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Product Details</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="other_charges_detail_form" method="post">
					<div  style="padding: 10px;"class="modal-body">
						<div class="form-group">
							<div class="cart-list-product" style="border-bottom 1px solid #000000;">								
								<a class="card-body pt-0 pr-2 pl-2 pb-0" id="append_cart_other_charges_data"></a>																		
							</div>
						</div>
					 
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
					</div>
				</form>
			</div>
		</div>
	</div>
	<button class="display_none" id="rzp-button1">Pay</button>
	<button  class="Razor display_none">Razor</button>
<script>
$('document').ready(function () {
	//$('#paypal-button').trigger('click');
	var payment_status = 'pending';
	var transaction_id = '';
	var product_cart_counter = 0;
	var shoping_cart_total = 0;
	var other_charges_total =0;
	var driver_tips_amount = 0;
	var cart_total = 0;
	var product_price = 0;

	var currency_code=$('#currency_code').val();
	//console.log('currency_code:'+currency_code);
	var currency_symbol=$('#currency_symbol').val();
	var driver_commission=$('#driver_commission').val();
	var service_charge=$('#service_charge').val();
	var commission=0.00;
	var charge=0.00;
	var final_total=0.00;
	var coupon_value=0.00;
	var coupon_percent=0.00;
	var coupon_code='';
	var vendor_data=''; 
	var other_fee =$('#other_fees').val();
	var if_any_price_one =$('#if_any_price_one').val();
	var sourcing_agent_fee = $('#sourcing_agent_fee').val();
	var if_any_price_two =$('#if_any_price_two').val();
	var fullfilment_center_price =$('#fullfilment_center_price').val();
	var choosen_card_id="";
	var cart_content='';
	var cart_content1='';
	var user_email='';
	var product_cart_data = JSON.parse(localStorage.getItem('product_cart'));
	console.log(product_cart_data);		
	if(product_cart_data.length>0){
		console.log(product_cart_data);
		$. each(product_cart_data, function(index, value){
		//console.log(value);
			 
		var product_detail='';
  
		if(value.product_actual_prize){
			//product_price+=(value.value.product_actual_prize-value.product_discounted_prize)+'';
		  }
		if(value.product_image){
						
		   //console.log(value.product_image);
		
		}
		if(value.product_colour_name){
		product_detail+='Colour : '+value.product_colour_name+'';
		}
		if(value.product_size_name){
			product_detail+=' <br/>Size : '+value.product_size_name+'';
		}
		product_price=value.product_actual_prize-value.product_discounted_prize;
		var service_tax=value.service_tax;
		var totalservice_tax=0.00;
		var service_tax_content='';
		if(service_tax && service_tax > 0){
			totalservice_tax=(value.product_goods_price*service_tax)/100;
			service_tax_content='<br/> <span>Sales Tax : <span>'+((totalservice_tax * value.quantity).toFixed(2))+'</span></span>';
		}
		
			

				// console.log(cart_content);
			cart_content +='<div class="cart-list-product" style="border-bottom: 1px solid #0000007a; transform: matrix(1, 0, 0, 1, 0, 0);">';
			   cart_content +='<h5 class=" remove-cart" href="#" style="display:none;"><i class="mdi mdi-close"></i></h5>';
			   cart_content +='<div class="cart-list-product" style="display: flex; margin:10px;"><img class="img mt-4" src="'+APP_URL+'uploads/'+value.product_images+'" alt="">';
				 cart_content +='<span class="productname" style="margin-top: 20px;"><span>'+value.product_name+'<br/>';
			   cart_content +='<span class="productprice" >₹'+product_price +' X '+ value.quantity+' <i class="mdi mdi-tag-outline"></i> <br/><span class="regular-price"></span>'+'</span></span>';
			   
				cart_content +='</div>';
			   cart_content +='<div class="cart-list-product" style="display: inline-block; border-bottom 1px solid #000000; margin-bottoHm: 20px;">';
			  cart_content +='<span class="productname other_charges display_none">other fee ₹<span>'+value.product_actual_prize+'<br/>';'</span>';			   
			   cart_content +='<span class="productname other_charges display_none">quantity <span>'+value.quantity+'.00<br/>';'</span>';
			   cart_content +='<span class="productname other_charges display_none">Product Discounted Price  ₹<span>'+value.product_discounted_prize+'<br/>';'</span>';
			   cart_content +='<span class="productname other_charges display_none">Product Long Description  ₹<span>'+value.product_long_description+'<br/>';'</span>';
			   cart_content +='<span class="productname other_charges display_none">Product Disease <span>'+value.product_disease+'<br/>';'</span>';
			   
			   cart_content +='</div>';
			   cart_content +='</div>';
			   
			   <!--cart conetent for other charges detai-->
			   cart_content1 +='<div class="cart-list-product" style="border-bottom: 1px solid #0000007a; transform: matrix(1, 0, 0, 1, 0, 0);">';
			   cart_content1 +='<h5 class=" remove-cart" href="#" style="display:none;"><i class="mdi mdi-close"></i></h5>';
			   cart_content1 +='<div class="cart-list-product" style="display: inline-block;"><img class="img" style="margin-top: -190px;" src="'+APP_URL+'uploads/'+value.product_images+'" alt=""></div>';
			   cart_content1 +='<div class="cart-list-product" style="display: inline-block; border-bottom 1px solid #000000; margin-bottoHm: 20px;">';
			   cart_content1 +='<div class="productname" style="margin-top: 20px;"><span>'+value.product_name+'<br/>';
			   cart_content1 +='<span class="productprice" >Product Actual Price ₹'+value.product_actual_prize +' X '+ value.quantity+' <i class="mdi mdi-tag-outline"></i> <br/><span class="regular-price"></span>'+'</span></div>';
			   cart_content1 +='<span class="productname other_charges">Product Discounted Price ₹<span>'+value.product_discounted_prize+'<br/>';'</span>';			   
			   cart_content1 +='<span class="productname other_charges">Product Long Description <span>'+value.product_long_description+'<br/>';'</span>';
			   cart_content1 +='<span class="productname other_charges">Product Rashi <span>'+value.product_rashi+'<br/>';'</span>';
			   cart_content1 +='<span class="productname other_charges">Product Disease <span>'+value.product_disease+'<br/>';'</span>';
			   cart_content1 +='<span class="productname other_charges">product Feature <span>'+value.product_feature+'<br/>';'</span>';
			   
			   cart_content1 +='</div>';
			   cart_content1 +='</div>';
			  <!--cart conetent for other charges detai-->
			   
			  
			   
			product_cart_counter = parseInt(product_cart_counter) + 1;
			shoping_cart_total  = shoping_cart_total+((value.product_actual_prize - value.product_discounted_prize) * value.quantity)+(totalservice_tax * value.quantity);
									
		});

		charge=service_charge;
		
		final_total=shoping_cart_total;//other_charges_total+parseFloat(other_fee)+parseFloat(sourcing_agent_fee)+parseFloat(fullfilment_center_price)+parseFloat(if_any_price_one)+parseFloat(if_any_price_two);
		$('#append_cart_data').html(cart_content);
		$('#append_cart_other_charges_data').html(cart_content1);
		$('#append_cart_total').html('₹'+shoping_cart_total.toFixed(2));
		$('#append_other_charges_total').html('<i  data-toggle="modal" data-target="#add_other_charges_modal"class="fa fa-info-circle other_charges_detail" aria-hidden="true"></i>');		
		$('#append_service_charge').html('₹'+charge);
		$('#append_final_amount').html('₹'+final_total.toFixed(2));
	}else{
		
	}

  
	$('body').on('click',".cash_option",function (e) {			
		$('.card_div').addClass('display_none');
		$('.add_card_button_checkout').addClass('display_none');
	});
		  
	$('body').on('click',".card_option",function (e) {
			
		$('.card_div').removeClass('display_none');
		$('.add_card_button_checkout').removeClass('display_none');
	});
	
	$('body').on('click',".edit_address",function (e) {
		$('#checkout_country_id option').removeAttr('selected')
		$('#checkout_state_id option').removeAttr('selected')
		$('#checkout_city_id option').removeAttr('selected')
		var customer_address_id=$(this).attr('customer_address_id');
		$.post(APP_URL+'api/customer_account/fatch_customer_address_data',{
			customer_id: customer_address_id,
		},function(response){
			if (response.status == 200) {
				var result =response.data[0];				
				$('#checkout_firt_name').val(result.first_name);					
				$('#checkout_customer_address_id').val(result.customer_address_id);					
				$('#checkout_last_name').val(result.last_name);				
				$('#checkout_phone_number').val(result.shipping_phone_number);				
				$('#checkout_country_id').val(result.country_name);				
				$('#checkout_state_id').val(result.state_name);				
				$('#checkout_city_id').val(result.city_name);				
				$('#checkout_zip_code').val(result.zip_code);
				$('#checkout_address_line').val(result.address_line);							
			}
		},'json');
		
    });

	fatch_customer_address_list_data();
	function fatch_customer_address_list_data(){
		var customer_id=$('#customer_id').val();
		$.post(APP_URL+'api/customer_account/fatch_customer_address_list_data',{
			customer_id: customer_id,
		},function(response){
			if (response.status == 200) {
				$('#checkout_email').val(response.user_data.customer_email);
				//console.log(response.user_data.customer_email);
					user_email=response.user_data.customer_email;
				var result=response.data;
				var content='';
				var selected_address='';
				var selected_customer_address_id='';
				var selected_first_name='';
				var selected_last_name='';
				var selected_address_line='';
				var selected_city_name='';
				var selected_state_name='';
				var selected_zip_code='';
				var selected_country_name='';
				var selected_shipping_phone_number='';
				$. each(result, function(index, value){
					var add_class='';
					if(value.is_selected=='1'){
						add_class='border border-success';
						selected_customer_address_id=value.customer_address_id;
						selected_first_name=value.first_name;
						selected_last_name=value.last_name;
						selected_address_line=value.address_line;
						selected_city_name=value.city_name;
						selected_state_name=value.state_name;
						selected_zip_code=value.zip_code;
						selected_country_name=value.country_name;
						selected_shipping_phone_number=value.shipping_phone_number;

						selected_address='('+value.address_line+', '+value.city_name+', '+value.state_name+' '+value.zip_code+', '+value.country_name+'  '+value.shipping_phone_number+')';
					}
					content+='<div class="card mb-1 '+add_class+'">';
						content+='<div class="card-body">';
							content+='<button href="javascript:void(0)" class="btn-primary float-right select_delivery_address" customer_address_id="'+value.customer_address_id+'" first_name="'+value.first_name+'" last_name="'+value.last_name+'" address_line="'+value.address_line+'" city_name="'+value.city_name+'" state_name="'+value.state_name+'" zip_code="'+value.zip_code+'", country_name="'+value.country_name+'"  shipping_phone_number="'+value.shipping_phone_number+'">Choose</button>';
							content+='<a href="#" class="btn-primary float-right edit_address" customer_address_id="'+value.customer_address_id+'" data-toggle="modal" data-target="#addCustomerAddressModal"   style="margin-right: 6px">Edit</a>';
							content+='<h5 class="card-title" style="display:inline-block;">'+value.first_name+' '+value.last_name+'</h5>';
							content+='<p class="card-text">'+value.address_line+', '+value.city_name+', '+value.state_name+' '+value.zip_code+', '+value.country_name+'  '+value.shipping_phone_number+'</p>';
						content+='</div>';
					content+='</div>';
					
				});
				if(customer_id){
					$('#append_customer_address').html(content);
					$('.no_data_found_address').addClass('display_none');
				}
				$('.selected_address').html(selected_address);
				$('#selected_customer_address_id').val(selected_customer_address_id);
				$('#selected_first_name').val(selected_first_name);
				$('#selected_last_name').val(selected_last_name);
				$('#selected_address_line').val(selected_address_line);
				$('#selected_city_name').val(selected_city_name);
				$('#selected_state_name').val(selected_state_name);
				$('#selected_zip_code').val(selected_zip_code);
				$('#selected_country_name').val(selected_country_name);
				$('#selected_shipping_phone_number').val(selected_shipping_phone_number);
				
			}
		},'json');
	}
	
	$('#addrees_form').validate({
		ignore: [],
		rules: {
			first_name: {
				required: true,
			},
			last_name: {
				required: true,
			},
			email_id: {
				required: true,
			},
			address_lines: {
				required: true,
			},
			phone_numbers: {
				required: true,
				
			},			
			country: {
				required: true,
			},
			state: {
				required: true,
			},
			city: {
				required: true,
			},
			zipcode: {
				required: true,
			},
		},
		messages: {
			first_name: {
				required: "First name is required",
			},
			last_name: {
				required: "Last name is required",
			},
			email_id: {
				required: "email id is required"
			},
			address_lines: {
				required: "address is required"
			},
			phone_numbers: {
				required: "Phone number is required",
			},
			zipcode: {
				required: "zipcode is required",
			},
			country: {
				required: "country is required",
			},
			state: {
				required: "state is required",
			},
			city: {
				required: "city is required",
			},
			
		},
		submitHandler: function (form) {
			//$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
			//	css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
			//	  overlayCSS:  { cursor:'wait'} 
			//});
			var first_name = $('#firstt_name').val();
			var last_name = $('#lastt_name').val();
			var email_id = $('#emaill_id').val();
			var address_lines = $('#addresss_lines').val();
			var phone_numbers = $('#phones_numbers').val();
			var zipcode = $('#zipcodee').val();
			var country = $('#countryy').val();
			var state = $('#statee').val();
			var city = $('#cityy').val();
			var customer_address_id = $('#customer_address_id').val();
			var customer_id = $('#customer_id').val();
			
			
			$.post(APP_URL+'api/account/signup_for_customer',{
				first_name: first_name,
				last_name: last_name,
				email_id: email_id,
				address_lines: address_lines,
				phone_numbers: phone_numbers,
				zipcode: zipcode,
				country: country,
				state: state,
				city: city,
				customer_id: 0,
				customer_address_id: 0,
			},function(response){
					//console.log("response");
					//console.log(response);
				//console.log($result);
				$.unblockUI();
				//$("html, body").animate({scrollTop: 0}, "slow");
				$('#checkout_headMsg').empty();
				if (response.status == 200) {
					console.log("1");
					var message = response.message;
					$('.delivery_address_box').addClass('display_none');
					$('.payment_box').removeClass('display_none');
					fatch_customer_address_list_data();
				
				
				} else if (response.status == 201) {
					$('#checkout_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			//return  false;
		}
	});

	

	$('body').on('click',".goto_address_tab",function (e) {
		$('.next1').trigger('click');
		fatch_customer_address_list_data();
	});
	
	getcustomer_address_list();
    function getcustomer_address_list(){
      	var customer_id=$('#customer_id').val();
        $.post(APP_URL+'api/customer_account/fatch_customer_address_list_data',{
        	customer_id: customer_id,
        },function(response){
			if (response.status == 200) {
				$('#checkout_email').val(response.user_data.customer_email);
					//console.log(response.user_data.customer_email);
				var result=response.data;
				var content='';
				var selected_address='';
				var selected_customer_address_id='';
				var selected_first_name='';
				var selected_last_name='';
				var selected_address_line='';
				var selected_city_name='';
				var selected_state_name='';
				var selected_zip_code='';
				var selected_country_name='';
				var selected_shipping_phone_number='';
				
				$. each(result, function(index, value){
					var add_class='';
					if(value.is_selected=='1'){
						add_class='border border-success';
						selected_customer_address_id=value.customer_address_id;
						selected_first_name=value.first_name;
						selected_last_name=value.last_name;
						selected_address_line=value.address_line;
						selected_city_name=value.city_name;
						selected_state_name=value.state_name;
						selected_zip_code=value.zip_code;
						selected_country_name=value.country_name;
						selected_shipping_phone_number=value.shipping_phone_number;

						
					}
					content+=' <div class="card mb-1" style="padding: 10px; margin-bottom: 10px; border:1px solid #000;">';
						content+='<div class="card-body">';
							content+='<button href="javascript:void(0)" class="btn btn-primary float-right select_delivery_address" customer_address_id="'+value.customer_address_id+'" first_name="'+value.first_name+'" last_name="'+value.last_name+'" address_line="'+value.address_line+'" city_name="'+value.city_name+'" state_name="'+value.state_name+'" zip_code="'+value.zip_code+'", country_name="'+value.country_name+'"  shipping_phone_number="'+value.shipping_phone_number+'">Choose</button>';
							content+='<a href="#" class="btn btn-primary float-right edit_address" customer_address_id="'+value.customer_address_id+'" data-toggle="modal" data-target="#addCustomerAddressModal"   style="margin-right: 6px; color:#ffff;">Edit</a>';
							content+='<h5 class="card-title" style="display:inline-block;">'+value.first_name+' '+value.last_name+'</h5>';
							content+='<p class="card-text">'+value.address_line+', '+value.city_name+', '+value.state_name+' '+value.zip_code+', '+value.country_name+'  '+value.shipping_phone_number+'</p>';
						content+='</div>';
					content+='</div>';
					
				});
				content+=' <div class="card">';
					content+=' <div class="card-body">';
							content+=' <button  href="#" type="button" data-toggle="modal" data-target="#addCustomerAddressModal" class="btn btn-primary float-right">Add New</button>';												  
							content+=' <h5 class="card-title" style="display:inline-block;"></h5>';
							content+=' <p class="card-text"></p> ';    									        
					content+=' </div>';
				content+=' </div>';
				$('#append_customer_address').html(content);
				
			}
		},'json');
    }

	$('body').on('click',".select_delivery_address",function (e) {
		 $('.delivery_address_box').addClass('display_none');
		 $('.active_detail_name2').addClass('display_none');
		 $('.active_detail_name1').removeClass('display_none');
		 $('.active_payment_name2').removeClass('display_none');
		 $('.active_payment_name1').addClass('display_none');
		 $('.payment_circle').removeClass('display_none');
		 $('.address_circle').addClass('display_none');
		 $('.payment').removeClass('display_none');
		 $('.payment_box').removeClass('display_none');
		 
		 $('.checkout_card').removeClass('display_none');			
		// $('.add_card_button_checkout').removeClass('display_none');			
		  //console.log(delivery_address_box);
		var customer_id=$('#customer_id').val();
		var customer_address_id=$(this).attr('customer_address_id');

		$.post(APP_URL+'api/customer_account/update_customer_active_address_data',{
			customer_address_id: customer_address_id,
			customer_id: customer_id,
					
		},function(response){
			//getcustomer_address_list();
			$('.next2').trigger('click');
		},'json');
		
	});

	$('#add_customer_address_data').validate({
		ignore: [],
		rules: {
			checkout_firt_name: {
				required: true,
			},
			checkout_last_name: {
				required: true,
			},
			checkout_phone_number: {
				required: true,
				minlength: 10, 
				maxlength: 13
			},
			checkout_zip_code: {
				required: true,
			},
			checkout_country_id: {
				required: true,
			},
			checkout_state_id: {
				required: true,
			},
			checkout_city_id: {
				required: true,
			},
			checkout_address_line: {
				required: true,
			},
		},
		messages: {
			checkout_firt_name: {
				required: "First name is required",
			},
			checkout_last_name: {
				required: "Last name is required",
			},
			checkout_phone_number: {
				required: "Phone number is required",
			},
			checkout_zip_code: {
				required: "Zip Code is required",
			},
			checkout_address_line: {
				required: "address is required"
			},
			checkout_city_id: {
				required: "city is required"
			},
			checkout_state_id: {
				required: "state is required"
			},
			checkout_country_id: {
				required: "country is required"
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var customer_id = $('#customer_id').val();
			var customer_address_id = $('#checkout_customer_address_id').val();
			var first_name = $('#checkout_firt_name').val();
			var last_name = $('#checkout_last_name').val();
			var shipping_phone_number = $('#checkout_phone_number').val();
			var zip_code = $('#checkout_zip_code').val();
			var country_name = $('#checkout_country_id').val();
			var state_name = $('#checkout_state_id').val();
			var city_name = $('#checkout_city_id').val();
			var address_line = $('#checkout_address_line').val();
			
			$.post(APP_URL+'api/customer_account/update_customer_address_data',{
				customer_id: customer_id,
				customer_address_id: customer_address_id,
				first_name: first_name,
				last_name: last_name,
				address_line: address_line,
				city_name: city_name,
				state_name: state_name,
				zip_code: zip_code,
				country_name: country_name,
				shipping_phone_number: shipping_phone_number,
			},function(response){
					//console.log("response");
					//console.log(response);
				//console.log($result);
				$.unblockUI();
				//$("html, body").animate({scrollTop: 0}, "slow");
				$('#checkout_headMsg').empty();
				if (response.status == 200) {
					console.log("1");
					var message = response.message;
					$('#checkout_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#checkout_headMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#checkout_headMsg').empty();
						$('#addCustomerAddressModal').modal('hide');
						getcustomer_address_list();
						window.location.href = APP_URL+'welcome/checkout';
					});
				} else if (response.status == 201) {
					$('#checkout_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			return  false;
		}
	});

	/*$('#applaycoupon_form').validate({
		ignore: [],
		rules: {
		  checkout_coupon_code: {
			required: true,
		  },
		},
		messages: {
		  checkout_coupon_code: {
			required: "Code is required",
		  },
		},
		submitHandler: function (form) {
		  $.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
			css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
			  overlayCSS:  { cursor:'wait'} 
			});
		  var checkout_coupon_code = $('#checkout_coupon_code').val();
		  
		  $.post(APP_URL+'api/products/check_coupon_code_data',{
			code: checkout_coupon_code,
		  },function(response){
			$.unblockUI();
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#coupon_code_headMsg').empty();
			if (response.status == 200) {
			  var message = response.message;
			 // $('#coupon_code_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
			 $("#coupon_code_headMsg").fadeTo(2000, 500).slideUp(500, function(){
				$('#coupon_code_headMsg').empty();
				$('.before_coupone_applay').addClass('display_none');
			   // $('.after_coupone_applay').removeClass('display_none');
				$('.applied_coupan').removeClass('display_none');
				coupon_percent=response.data[0].coupon_discount;
				console.log(coupon_percent);
				coupon_code=response.data[0].coupon_code;
				console.log(coupon_code);
				coupon_value=(final_total*coupon_percent)/100;
				console.log(coupon_percent);
				console.log(final_total);
				console.log(shoping_cart_total);
				console.log(coupon_value);
				final_total=final_total-coupon_value;
					console.log( final_total);
				$('#append_coupon_discount_carges').html(coupon_percent+'% ($'+coupon_value+')');
				$('#append_final_amount').html('$'+final_total.toFixed(2));
				$('#addCouponModal').modal('hide');
				//getcustomer_address_list();
				//window.location.href = APP_URL+'customer/configure';
			  });
			} else if (response.status == 201) {
			  $('#coupon_code_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
			}
		  },'json');
		  return  false;
		}
	});*/

	/*$('body').on('click',".remove_coupone_code",function (e) {
	  final_total=final_total+coupon_value;
	  $('#append_final_amount').html(currency_symbol+final_total);
	  coupon_code='';
	  coupon_percent=0.00;
	  coupon_value=0.00;
	  $('.before_coupone_applay').removeClass('display_none');
	  $('.after_coupone_applay').addClass('display_none');
	});*/

	$('body').on('click',".paymentRadio",function (e) {
			  //console.log(paymentRadio);
	  $('.payment_submit').addClass('display_none');
	   
	  $(this).closest('.form-group').find('.payment_submit').removeClass('display_none');
	});

	$('body').on('click',".final_submit",function () {
		$('.next3').trigger('click');
		var payment=$("input[name='paymentRadio']:checked"). val();
		if(payment){
			console.log('if');
			if(payment=='cash'){ 
			 $('.Razor').trigger('click');
			  
			}
			if(payment=='rozerpay'){
			  payment_by_rozer_pay();
			}
			if(payment=='net_banking'){
				
				console.log('card_payment');
				if(payment == ''){
					$('#headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>Select Atleast one card</strong></div>");
   
				}else{
					console.log('net_banking');
					$('.Razor').trigger('click');
				
					
				}
			 
			  
			}
		}else{
			console.log('else');
			alert('ee');
		  $('#headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>Select Payment Method</strong></div>");
   
		}
	});

	$('body').on('click',".Razor",function () { 
		//alert('clicktriger to razor');
		var payment=$("input[name='paymentRadio']:checked"). val();
		var customer_id=$('#customer_id').val();		
		var customer_address_id=$('#selected_customer_address_id').val();		
		var first_name=$('#selected_first_name').val();
		var last_name=$('#selected_last_name').val();		
		var address_line=$('#selected_address_line').val();
		var city_name=$('#selected_city_name').val();
		var state_name=$('#selected_state_name').val();
		var zip_code=$('#selected_zip_code').val();
		var country_name=$('#selected_country_name').val();
		var shipping_phone_number=$('#selected_shipping_phone_number').val();
		
		//$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
		//css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
		//  overlayCSS:  { cursor:'wait'} 
		//});
		$.post(APP_URL+'api/products/place_order_for_item_data', {
			  product_data: product_cart_data,			 
			  customer_address_id: customer_address_id,
			  first_name: first_name,
			  last_name: last_name,
			  address_line: address_line,
			  city_name: city_name,
			  state_name: state_name,
			  zip_code: zip_code,
			  country_name: country_name,
			  shipping_phone_number: shipping_phone_number,
			  customer_id: customer_id,
			  payment: payment,
			  transaction_id: transaction_id,
			  payment_status: payment_status,
			  total_item: product_cart_counter,
			  total_amount: shoping_cart_total,			
			  final_amount: final_total.toFixed(2),
			 
		},function(response){
			$.unblockUI();
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headMsg').empty();
			if (response.status == 200) {
				var message = response.message;
				var order_id=response.order_id;
				var resonsepayment=response.payment;
				$('#order_id').val(order_id);
				$('#headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headMsg').empty();
					localStorage.setItem('product_cart' , JSON.stringify(''));
					$('ul.product_cart_info').empty();
				});
				if(resonsepayment=="cash"){
					$('.payment_box').addClass('display_none');
				    $('.cart_box').addClass('display_none');
				    $('.active_payment_name2').addClass('display_none');
				    $('.active_payment_name1').removeClass('display_none');
				    $('.active_succes_name').removeClass('display_none');
				    $('.active_succes_name2').addClass('display_none');
				    $('.congratulation_box').removeClass('display_none');
				    $('.final_circle').removeClass('display_none');
				    $('.last_circle').addClass('display_none');
				}else{
					var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
					var paid_amount = final_total.toFixed(2);
					var options = {
						"key": razor_all_key,
						"amount": (paid_amount*100), // 2000 paise = INR 20
						"name": "Maya Astrology",
						"description": "Maya Astrology Paid Consultation",
						"image": "",
						"captured": true,
						"handler": function (response){
							//alert(response.razorpay_payment_id);
							console.log(response);
							if(response){
								$.post(APP_URL+'welcome/paytest1', {
									amount: (paid_amount*100),
									payment_id: response.razorpay_payment_id,
									razor_key: razor_all_key,
									seceret_key: seceret_key,
									order_id: order_id,
									email: $('#checkout_email').val(),
								},
								function (response2) {
									
									var data = response2.data;
									//window.location.href = APP_URL+'welcome/razorResponseHandlerservice/'+data;
									if(response2){
										$('.payment_box').addClass('display_none');
										$('.cart_box').addClass('display_none');
										$('.active_payment_name2').addClass('display_none');
										$('.active_payment_name1').removeClass('display_none');
										$('.active_succes_name').removeClass('display_none');
										$('.active_succes_name2').addClass('display_none');
										$('.congratulation_box').removeClass('display_none');
										$('.final_circle').removeClass('display_none');
										$('.last_circle').addClass('display_none');
									}
								}, 'json');
							}
							
						},
						"prefill": {
							"name": $('#selected_first_name').val(),
							"contact": $('#selected_shipping_phone_number').val(),
							"email": $('#checkout_email').val(),
							
						},
						"notes": {
							"address": "Paid Consultation"
						},
						"theme": {
							"color": "#F37254"
						}
					};	
					var rzp1 = new Razorpay(options);
					document.getElementById('rzp-button1').onclick = function(e){
						rzp1.open();
						e.preventDefault();
					}
					$('#rzp-button1').trigger('click');
					
				}
					
		
			} else {
			  $('#headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
			}
			
		},'json');
			
	
	});
});
</script>
	