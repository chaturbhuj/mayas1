<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>


<script>
  $('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
  });
</script>
<style type="text/css">
</style>
<!-- ::::::  Start  Main Container Section  ::::::  -->
   <!-- ::::::  Start  Main Container Section  ::::::  -->
<div class="dashboard-content py-4">
   <main id="main-container" class="main-container">
        <div class="container">
            <div class="row"> 
                <div style="margin-left: 25px;" class="col-md-12 m-b-20">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page"> My order </li>
						</ol>
					</nav> 
				</div> 
			<div class="col-12">
				<div class="row">
                    
					<div class="col-md-12">
                        <div  style="margin-bottom:40px;"class="card card-body account-right">
							<div class="widget">
								<div class="section-header">
									<button class="btn btn-info float-right refund_request_submit" style="display:none;" >Refund request</button>
									<h5 class="heading-design-h5">
										Orders Detail
									</h5></br>
									<input type="hidden" id="id" value="<?php if($_GET['id']){echo $_GET['id'];}?>">
									
								</div>
								<div id="detailHeaderMsg"></div>
								<div class="order-list-tabel-main table-responsive">
									<table class="datatabel table table-striped table-bordered order-list-tabel">
										<thead>
											<tr role="row">
												
												<th>Product Name</th>
												<th>Product Info</th>
												<th>Amount</th>
												<th>Total</th>
											</tr>
										</thead>
										<tbody id="order_detail_data_content">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>  
			</div>
		</div>
	</main>
</div>
<script>
         $('document').ready(function () {
            fatch_profile_data();
            function fatch_profile_data(){
               var id=$('#id').val();
               $.post(APP_URL+'api/customer_account/fatch_customer_order_detail_list_data',{
                  id: id,
               },function(response){
                  if (response.status == 200) {
                     var content='';
                     var result=response.data;
					 console.log(result);
                     var counter=1;
                     $. each(result, function(index, value){
                        content +='<tr>';
						 console.log(value.product_total);
                           content +='<td>'+value.product_name+'</td>';
                           content +='<td>Height - '+value.product_height+',  weight- '+value.product_weight+'</td>';
                           content +='<td>'+value.quantity+' X ₹'+value.product_total+'</td>';
                           content +='<td>₹'+value.product_total+'</td>';
                        content +='</tr>';
                        counter=counter+1;
                     });
                     console.log(content);
                     $('#order_detail_data_content').html(content);
                  }
               },'json');
            }

            $('body').on('click','.checked_all_items',function(){
               if($(this).is(":checked")){
                  $(".item_checked").prop("checked", true);
               }else{
                  $(".item_checked").prop("checked", false);
               }
            });

            $('body').on('click', '.item_checked', function () {   
               if($(this).is(":checked")){
                  //$(".checked_all_items").prop("checked", false);
               }else{
                  $(".checked_all_items").prop("checked", false);
               }
            });

            $('body').on('click', '.refund_request_submit', function () {  
               var order_id=$('#id').val();
               var customer_id=$('#customer_id').val();
               order_data = [];
               var total=0;
               
               $('[name="item_checked"]:checked').each(function () {
                  // do stuff
                  order_data.push($(this).val());
               });
               console.log(total.toFixed(2));
               console.log(order_data);
               if(order_data.length==0){
                  $('#detailHeaderMsg').empty();
                  $("html, body").animate({scrollTop: 0}, "slow");
                  message="please select at least one checkbox";
                  $('#detailHeaderMsg').html("<div class='alert alert-danger fade in show'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;</div>");
                  $("#detailHeaderMsg").fadeTo(2000, 500).slideUp(500, function(){
                     $('#detailHeaderMsg').empty();
                  });
               }else{
                  $.post(APP_URL+'api/customer_account/refund_request_submit',{
                     customer_id: customer_id,
                     order_id: order_id,
                     order_data: order_data,
                     id: id,
					 
                  },function(response){
                     $.unblockUI();
                     $("html, body").animate({scrollTop: 0}, "slow");
                     $('#detailHeaderMsg').empty();
                     if (response.status == 200) {
                        var message = response.message;
                        $('#detailHeaderMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                        $("#detailHeaderMsg").fadeTo(2000, 500).slideUp(500, function(){
                           $('#detailHeaderMsg').empty();
                           //window.location.href = APP_URL+'vendor/login';
                        });
                     } else if (response.status == 201) {
                        $('#detailHeaderMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                     }
                  },'json');
               }
            });
         });
      </script>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_upload',$data);
?>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_only_upload',$data);
?>
