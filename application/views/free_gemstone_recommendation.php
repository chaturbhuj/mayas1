<style>
.form-control{
	    height: 34px !important;
}
label.control-label{
	color: #fff !important;
}
.control_div{
	color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.newfree_gemstone_recommendationForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 900px;
	 height: 225px;
 }
.display_none{
	 display:none !important;
}
</style>
<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
		});

	});

</style>
<script src="<?php echo base_url(); ?>assets/js/free_gemstone_recommendation.js"></script> 
<div id="content">
<div class="container">


 <!-- modal for free telephonic consultancy -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="free_gemstone_recommendation1" class="">
	
	<div class="modal-header">
		<div id="err_free_gemstone_recommendation_div"></div>
		
		<div class="col-md-12 image_div">
			<img class="img-preview" src="<?php echo base_url()?>/uploads/<?php echo $popup_ads[0]['free_consultation_image'];?>" alt="Mayas Astrology">
		</div>
		<div class="freeConsultant-form" style="padding-top: 15px;padding-bottom: 15px;">Fill form to know your suitable gemstone</div>
		<div class="clearfix"></div>
		<form class="form-horizontal newfree_gemstone_recommendationForm" id="newfree_gemstone_recommendationForm" method="post">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name <span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Gender <span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="inputGender" id="inputGender" class="form-control">
					<option value="">Gender</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" id="inputDateDate" name="inputDateDate" placeholder="Birth Date" class="form-control  date " value="">  
				</div>
				<div class="col-sm-3">
					<input type="text" id="inputTimeMin" name="inputTimeMin" placeholder="Birth Time" class="form-control time " value="">
					
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour format</label>
				</div>
		   </div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth <span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id <span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id <span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number (Optional)">
				</div>
			</div>
			<div class="form-center" style="text-align: center;">
				<button type="submit" class="submit-btn btn btn-primary" style="cursor: pointer !important;">Submit</button>	
			</div>
		</form>


	</div>
	<div class="modal-footer" style="display:none;">
		<a rel="nofollow" href="#" class="free-close-btn btn">Close</a>
	</div>
</div>

</div>  
</div>
