r<style>.thumbnail{text-align: center;} 
	   .thumbnail img{background-color: #000;} 
        #content{background-color: #ddd;} 
		#content .row{background-color: #fff;  padding: 0 !important; margin: 0 !important;}
		.prodcut_box h4{padding-top: 30px; color: black;} 
		#content .span3{  padding: 0 !important; margin: 0 !important;} 
		#content .thumb5:hover{outline: 2px solid rgb(254, 242, 73);}	
		.prodcut_box img{height:250px;} 
		a{text-decoration: none;}
		#content .thumb5{padding: 10px !important; box-shadow: 1px 1px 3px 1px;}
		.price_span{ color: rgb(128, 0, 0); margin:0px; font-weight: bold;}
		.product_detail{ padding-top: 50px !important; }
		.product_detail .span6{  background-color: #fff;padding: 15px 15px 70px 15px;}
		.product_detail .span5{  background-color: #fff;padding: 15px;}
		.product_detail .inner_right h3{color: #000; padding-top: 0px;}
		.product_detail .inner_right p{color: #9e9e9e; font-weight: normal;}
		.product_detail .inner_right td{text-align: left;}
		.product_detail .inner_right  td:first-child{border-right: none;}
		.product_detail .inner_right td:last-child{border-left: none;}
		.product_detail .inner_right table{margin-bottom: 40px;}
		.sub_img_sec li{list-style: none; display: inline-block; width: 70px; padding-top: 10px}
		.sub_img_sec img{width: 60px; height: 60px;}
		.main_img_sec{text-align: center;margin-top: 30px;margin-bottom: 30px;}
		.main_img_sec img{height:300px; width:300px;}
		.buy_now_btn{border: 1px solid #fb8903;
					  color: #fb8903 !important;
					  background-color: white;
					  padding: 18px 47px;
					  margin: 50px !important;
					  margin-top: 50px;
					  margin-bottom: 50px;
					  border-radius: 4px;
					  }
		.buy_now_btn:hover{color: white !important;background-color: #fb8903;}
</style> 								

<?php 
	
	if($product_detail){
		$product_id = $product_detail[0]['product_id'];
		$product_categories = $product_detail[0]['product_categories'];
		$product_name = $product_detail[0]['product_name'];
		$product_description = $product_detail[0]['product_description'];
		$product_price_ind = $product_detail[0]['product_price_ind'];
		$product_price_usd = $product_detail[0]['product_price_usd'];
		$product_feature = $product_detail[0]['product_feature'];
		
		$product_color = $product_detail[0]['product_color'];
		$product_grade = $product_detail[0]['product_grade'];
		$product_image_list = $product_detail[0]['product_image_list'];
		
		
	
	}else{
		$product_id = 0;
		$product_description = '';
		$product_price_ind = '';
		$product_price_usd = '';
		$product_categories = '';
		$product_name = '';
		$product_feature = '';
		$product_color = '';
		$product_grade = '';
		$product_image_list = '';
		
			
		}
		
	
?>
<script src="<?php echo base_url();?>assets/js/configure/constantpro.js"></script> 
<?php  
		
			if (sizeof($product_detail[0])>1) {
				$image = explode(",", $product_detail['0']['product_image_list']);
			echo '<div class="product_detail"> 	
						<div class="container">
							<div class="row"> 	
								<div class="span5"> 
									<div class="inner_left">
										<div class="main_img_sec">
												<img class="product_img_show" src="'. base_url().'uploads/'.$image[0].'" alt="Mayas Astrology">
										</div>
										<div class="sub_img_sec"><ul>';
										$i=1;
												foreach ($image as $value) {
														echo '<li><a rel="nofollow" class="product_img product_img'.$i.'" href="javascript:void(0);"><img src="'. base_url().'uploads/'.$value.'" alt="Mayas Astrology"></a></li>';
														$i++;
												}
										echo '/<ul></div>	
										</div> 
								</div>
					
							<div class="span6">
								<div class="inner_right">
											<h3>'.$product_detail['0']['product_name'].' </h3>
											<table class="table table-bordered table-hover">											   
												<tr><td><strong> product_description:</strong></td> <td><span>'.$product_detail['0']['product_description'].' </span></td></tr>
												<tr><td> <strong> product_price_ind:</strong></td><td> <span>'.$product_detail['0']['product_price_ind'].' </span></td></tr>
												<tr><td> <strong> product_price_usd:</strong> </td><td><span>'.$product_detail['0']['product_price_usd'].' </span></td></tr>
												<tr><td> <strong> product_feature:</strong></td><td> <span>'.$product_detail['0']['product_feature'].' </span></td></tr>
												<tr><td><strong> product_weight:</strong> </td><td><span>'.$product_detail['0']['product_weight'].' </span></td></tr>
												<tr><td> <strong> product_shape:</strong></td><td> <span>'.$product_detail['0']['product_shape'].' </span></td></tr>
												<tr><td> <strong> product_color:</strong></td><td> <span>'.$product_detail['0']['product_color'].' </span></td></tr>
												<tr><td> <strong> product_clarity:</strong></td><td> <span>'.$product_detail['0']['product_clarity'].' </span></td></tr>
												<tr><td> <strong> product_grade:</strong> </td><td><span>'.$product_detail['0']['product_grade'].' </span></td></tr>
												<tr><td> <strong> product_cut:</strong></td><td> <span>'.$product_detail['0']['product_cut'].' </span></td></tr>
												<tr><td> <strong> product_treatment:</strong></td><td> <span>'.$product_detail['0']['product_treatment'].' </span></td></tr>
												<tr><td> <strong> product_origin:</strong></td><td> <span>'.$product_detail['0']['product_origin'].' </span></td></tr>
												<tr><td> <strong> product_other_info:</strong></td><td> <span>'.$product_detail['0']['product_other_info'].' </span></td></tr>
												<tr><td> <strong> product_other_info2:</strong></td><td> <span>'.$product_detail['0']['product_other_info2'].' </span></td></tr>
												<tr><td> <strong> product_other_info3:</strong></td><td> <span>'.$product_detail['0']['product_other_info3'].' </span></td></tr>
											</table>
										
								</div>
								
							</div>
						</div>
						</div>
					</div>';	
		}	
?>

<div >

<a rel="nofollow" href="<?php echo base_url(); ?>welcome/pshopcart/" 
										
	product_id=<?php echo $product_id; ?>
	product_price_ind=<?php echo $product_price_ind; ?> 
	product_price_usd=<?php echo $product_price_usd; ?> 
	product_feature=<?php echo $product_feature; ?> 
	product_color=<?php echo $product_color; ?> 
	product_grade=<?php echo $product_grade; ?> 
	product_image_list=<?php echo $product_image_list; ?> 
	product_description=<?php echo $product_description; ?> 
	product_categories=<?php echo $product_categories; ?> 
	product_name=<?php echo $product_name; ?>
	class="btn btn-primary add_productcart"  style="margin-bottom: 20px;margin-left: 100px;"><strong>Add Cart</strong> </a>
</div>
<script> 
$('document').ready(function () {
		$('.product_img1').find('img').css({'outline':'2px solid #777'});
	$('body').on('click','.product_img',function(){
		$('.product_img').find('img').css({'outline':'none'});
		$(this).find('img').css({'outline':'2px solid #777'});
		$('.product_img_show').attr('src',$(this).find('img').attr('src'));
});
	});
 </script


