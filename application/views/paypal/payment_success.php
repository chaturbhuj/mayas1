
<div id="site-main" style="background-color: #fff;">				
	<div class="page-section v1-one">
		<div class="container">
			<div class="row cart">							
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 10px;">
					<div class="row col-md-9" style="border: 1px solid;">
						<div class="">
							<div style="">
							<h3 style="padding: 16px;color: #849EC6;text-align: center;">INVOICE</h3>
							</div>
						</div>
						<div class="row">
							<br/>
							<div class="col-md-6 col-sm-6" style="width: 45%;">		
								<h3 class="text-capitalize"><?php echo WEBSITE_NAME;?></h3>
							</div>
							<div class="col-md-6 col-sm-6" style="width: 45%;">
								<p style="margin-bottom: 6px;line-height: 1;">Date: <span class="date_span"><?php echo date("D M j Y",strtotime($order_deli_info['date'])); ?></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">ODID: <span class="invoice_id"><?php echo $order_deli_info['ODID']?></span></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6" style="width: 50%;">
								<h3 style="color: #AC2;">BILL TO</h3>
								<p style="margin-bottom: 6px;line-height: 1;">Name : <span class="span_name"><?php echo $order_deli_info['firstname'].' '.$order_deli_info['firstname']?></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Address : <span class="span_address"><?php echo $order_deli_info['address']?></span></span></p>
							</div>
							<div class="col-md-6 col-sm-6" style="width: 50%;">
							
							</div>
						</div>
						<div class="row" style="display:block; padding:10px;">
							<table class="shop_cart_table table table-bordered">
								<thead>
									<tr>
										<th class="product-empty">Product Name</th>
										<th class="product-subtotal">Product Quantity</th>
										<th class="product-subtotal">Product Price</th>
									</tr>
								</thead>
								<tbody>												
									<?php 
										if($order_prod_info){
											$content = '';
											foreach($order_prod_info as $value){
												$content .= '<tr>';
												$content .= '<td><img class="img_preview" style="width: 50px;height: 50px;" src="'.base_url().'uploads/product/'.$value['product_img'].'">&nbsp;'.$value['product_name'].'</td>';
												$content .= '<td>'.$value['product_quantity'].'</td>';
												$content .= '<td>'.DOLLER.' '.$value['product_price'].'</td>';
												$content .= '</tr>';
											}
											echo $content;
										}
									
									?>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-md-8 col-sm-6" style="width: 70%;">
							
							</div>
							<div class="col-md-4 col-sm-6" style="width: 30%;">
								<table>
									<tbody style="font-size: 16px;">
										<tr>
											<td>Sub Total</td>
											<td> : </td>
											<td style="text-align: left;"><?php echo DOLLER;?> <span class="span-subtotal"><?php echo $order_deli_info['subtotal']?></span></td>
										</tr>
										<tr style="display:none;">
											<td>Service Tax ( <span class="cart-servicetax"><?php echo SERVICE_TAX; ?></span> % )</td>
											<td> : </td>
											<td style="text-align: left;"><?php echo DOLLER;?> <span class="span-servicetax"><?php //echo $order_deli_info['servicetax']?></span></td>
										</tr>
										<tr>
											<td>Discount</td>
											<td> : </td>
											<td style="text-align: left;"><?php echo DOLLER;?> <span class="span-discount"><?php echo $order_deli_info['coupon_discount']?></span></td>
										</tr>
										<tr>
											<td>Grand Total : </td>
											<td> : </td>
											<td style="text-align: left;"><?php echo DOLLER;?> <span class="span-grandtotal"><?php echo $order_deli_info['total_price']?></span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<p style="text-align: center;">Thank you For your Business</p>
						</div>							
					</div>							
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .page-section -->				
</div>
		
<style>
.cart table.cart .product-name > *{
    display: inline;    font-size: 20px !important;color: #95c03e;
}
.product-subtotal .fa-gbp{
	font-size: 20px !important;color: #95c03e;
}
.cart_totals_inner .fa-gbp, .cart_totals_inner span.amount{
	color: #000;	
}
.shop_table{
	background-color: #fff;
}
tbody{
    font-size: 20px;
}
</style>
<script>
$('document').ready(function(){
	document.cookie = "web_product_cart=''; domain=;path=/";
});
</script>