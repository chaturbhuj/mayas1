<script>
$(function() {
    $(".date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: 'c-75:c+75',
        //minDate : 'today',
    });
    $('.time').timepicker({
        showPeriodLabels: false,
        rows: 6,
        minutes: {
            starts: 0,
            ends: 59,
            interval: 1,
            manual: []
        }
    });

});
</script>



<script src="<?php echo base_url();?>assets/js/jquery.blockUI.min.js"></script>
<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<style>
.vastu_btn {
    display: inline-block;
    color: #231E22;
    padding: 6px 11px;
    box-shadow: 7px 7px 14px #d8d8d8, -7px -7px 14px #ffffff;
    border-radius: 10px;
    margin: 0 10px;
    transition: all 0.5s;
    color: #007bff;
    text-decoration: none;
    background-color: transparent;
    border: unset;
}


.required {
    color: red;
}

/* .temp:hover{
    color: red;
} */
.vastu_submit_btn:hover {
    color: #0062cc !important;
}

.vastu_btn,
.vastu_submit_btn {
    cursor: pointer !important;
    /* width: 27%; */
    /* height: 48px; */
    padding: 10px;
    margin: 0 19px;
    font-size: 16px;
    background-image: none;
    /* color: #fff !important; */
    /* padding: 15px 30px; */
    display: inline-block;
    box-shadow: 7px 7px 14px #d8d8d8, -7px -7px 14px #ffffff;
    border-radius: 10px;
    background-color: #F0F0F0 !important;
    letter-spacing: 0.6px;
    color: #231E22;
    border: unset;
}

.footer {
    margin-top: 120px;
}

.vastu_submit_btn {
    width: 27%;
    width: 33%;
    font-size: 20px;
}
</style>
<?php
    // var_dump($vastu_data);

 if ($vastu_data) {
    // var_dump($vastu_data);
    // var_dump($datalat);
    $faq_category = $vastu_data['faq_category'];
    $banner_id = $vastu_data['banner_id'];
    $about_content_id = $vastu_data['about_content_id'];
    $slider_image = '';
    $primary_image='';
	$images='';
    $sub_folder_name="";
    $image_cat="items";

    $additional_json =$vastu_data['additional_json'];
    $property_directioncompass_msg =  $vastu_data['property_directioncompass_msg'];
    $property_directioncompass_link =  $vastu_data['property_directioncompass_link'];
    $property_directioncompass_defaultvideo =  $vastu_data['property_directioncompass_defaultvideo'];
    $compass_image =  $vastu_data['compass_image'];

    # code...
 }else{
    $additional_json =[];
    $slider_image = '';
    $primary_image='';
	$images='';
    $sub_folder_name="";
    $faq_category = 0;
    $banner_id = 0;
    $about_content_id = 0;
    $image_cat="items";
    $property_directioncompass_msg =  '';
    $property_directioncompass_link =  '';
    $property_directioncompass_defaultvideo =  '';
    $compass_image =  '';


 }
  $q_label ="Question/Concern
  ";
  $q_info="Do not combine multiple questions into one Question.";
  $q_placeholder="Mention one precise question or concern";
  $consultation_type = '';
  $is_question1 = '';
  $is_question2='';

 ?>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div id="headmsg"></div>
        <div class="row">
            <div class="col-xs-12 col-sm-8 my-4">
                <!-- <h1><?=$vastu_data['page_title']?></h1> -->
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb" style="display:none">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
            <li class="active">About Us</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->



<section class="section-wrapper">

    <?php
            $datacat = array(
            );

            $datacat['banner_id'] = $banner_id;
            $this->load->view('banner',$datacat);
            ?>


    <div class="col-md-12 image_div mt-4">
        <div class="row">
            <?php if($datalat[0]['detail_image_position']=='left'){ ?>
            <div class="col-md-5">
                <img class="img-preview" style="    height: auto;    width: 100%;   padding-bottom: 10px;"
                    src="<?php echo base_url()?>uploads/<?php echo ($datalat[0]['detail_image']);?>"
                    alt="Mayas Astrology">
            </div>
            <?php }?>
            <div class="col-md-7">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <h4><?=$vastu_data['page_heading']?></h4>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?=$vastu_data['page_content']?>
                </div>
                <!-- <?php echo $services_details[0]['services_description']; ?> -->
            </div>
            <?php if($datalat[0]['detail_image_position']=='right'){ ?>
            <div class="col-md-5">
                <img class="img-preview" style="    height: auto;    width: 100%;   padding-bottom: 10px;"
                    src="<?php echo base_url()?>uploads/<?php echo ($datalat[0]['detail_image']);?>"
                    alt="Mayas Astrology">
            </div>
            <?php }?>
        </div>
    </div>
    <!-- <div class="container">
                    <div class="row mt-5">
                        <div class="col-sm-12 col-lg-12 col-md-12">
                            <h4><?=$vastu_data['page_heading']?></h4>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <?=$vastu_data['page_content']?>
                        </div>

                    </div>
                </div> -->
</section>

<section class="vastu_links">
    <div class="container">
        <div class="row mt-5 text-center">
            <input type="hidden" class="form-control" id="consultantType" name="consultantType"
                value="<?php echo $this->uri->segment(2);?>">
            <!-- <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="<?=base_url().'vastu/home-vastu'?>" class="btn  consultantType vastu_btn">Home Vastu</a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="<?=base_url().'vastu/office-vastu'?>" class="btn consultantType vastu_btn">Office Vastu</a>
            </div> -->
            <!-- <div class="col-lg-4 col-md-4 col-sm-12">
                <a href="<?=base_url().'vastu/industry-vastu'?>" class="btn  consultantType vastu_btn">Industry
                    Vastu</a>
            </div> -->



        </div>
    </div>

</section>

<div class="section-column-wrapper" style="padding-top:50px;">
    <div class="container">
        <section class="form_section">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12" style="display:none;">
                        <span><strong>Consultant Type</strong></span>:
                        <span><?php echo $services_details[0]['consultant_type']; ?></span>
                    </div>
                    <div class="row d-none" style="padding-left: 15px;">
                        <div class="col-md-12">
                            <h4 style="font-size: 30px; padding-bottom: 0px;color: #7b6763;" class="consult">
                                <span
                                    style="width: 30px; height: 30px; background-color: #678; padding: 0px 17px; border-radius: 50%; color: #fff;">1</span>
                                <strong>Select Consultation type as per Your Need</strong>
                            </h4>
                        </div>
                        <div class="col-md-12">
                            <?php

                $data = get_location('66.249.79.122');
						$country = 'India';
						if($data){
						$country = $data['country'];
						}
					
						$method = 'Razor';
						$currency = 'INR';
						if ($country != 'India' ) {
							$method = 'Paypal';
							$currency = 'USD';
						}

                        if(!empty($additional_json)){
                           
                            foreach($additional_json as $val){ 
                                    $d_price = 0;
                                    if((float)$val->discounted_cost){
                                        $d_price = $val->cost - (float)$val->discounted_cost;

                                        if ($country != 'India') {
                                            $d_price = $val->usdcost - (float)$val->discounted_usd_cost;
                                        }else{
                                            $d_price = $val->cost - (float)$val->discounted_cost;


                                        }

                                    }
                                ?>




                            <div class="radioBtnClass1" style="padding-top: 30px;">

                                <?php if ($country != 'India') { ?>
                                <div style="padding-left: 10px;">
                                    <input type="radio" checked class="radioBtnClass" id="radioBtnClass"
                                        name="radioBtnClass" d_price="<?php echo ($d_price); ?>"
                                        cost="<?php echo $val->usdcost ?>"
                                        discounted_cost="<?php echo $val->discounted_usd_cost ?>">
                                    <strong style="text-transform: capitalize;"><?php echo $val->q_heading; ?>
                                        <span><?php echo '('.$val->q_description.')'; ?></span>
                                        , <strong>Fee</strong> :
                                        <?php if($val->discounted_usd_cost == 0){ echo $val->usdcost; }
										else{ echo '<del>$ '.$val->usdcost.'</del>&nbsp; $ '.$val->discounted_usd_cost.' , &nbsp;<span style="color:red;"> You Save $ '.((float)$val->usdcost - (float)$val->discounted_usd_cost).'</span>' ;} ?>
                                        </br></br>
                                </div>

                                <?php }else{ ?>
                                <div style="padding-left: 10px;">
                                    <input type="radio" checked class="radioBtnClass" id="radioBtnClass"
                                        name="radioBtnClass" d_price="<?php echo ($d_price); ?>"
                                        cost="<?php echo $val->cost ?>"
                                        discounted_cost="<?php echo $val->discounted_cost ?>">
                                    <strong style="text-transform: capitalize;"><?php echo $val->q_heading; ?>
                                        <span><?php echo '('.$val->q_description.')'; ?></span>
                                        , <strong>Fee</strong> :
                                        <?php if($val->discounted_cost == 0){ echo $val->cost; }
										else{ echo '<del> ₹ '.$val->cost.'</del>&nbsp; ₹  '.$val->discounted_cost.' , &nbsp;<span style="color:red;"> You Save ₹ '.((float)$val->cost - (float)$val->discounted_cost).'</span>' ;} ?>
                                        </br></br>
                                </div>

                                <?php } ?>

                                <?php	}
							}?>




                            </div>
                            <div class="row" style="float: right;">
                                <div class="col-xs-8">
                                    <div>
                                        <input type="text" class="form-control display_none" id="inputcoupan"
                                            name="inputcoupan" placeholder="Enter Coupon Code">
                                        <input type="hidden" class="form-control " id="coupon_discount"
                                            name="coupon_discount">
                                        <input type="hidden" class="form-control " id="coupon_discount_amount"
                                            name="coupon_discount_amount">
                                        <div id="coupan_err"></div>
                                    </div>
                                    <div class="">
                                        <span class="coupon_msg "></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <a href="javascript:void(0);" title="Apply Coupon"
                                        class="btn btn-success coupon_apply_btn display_none appl_coupan_code"><span
                                            class=" ">Apply</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" style="padding-left:15px">
                    <!-- <div class="row" style="margin-top: 40px;padding-left:15px"> -->
                    <div class="col-md-12">
                        <?php if ($vastu_data['owner_name_label']) {
                            # code...
                       ?>
                        <h3 class="text-center my-4 text-bold">Fill the Form Confirm Your Booking</h3>
                        <?php  } ?>
                        <div id="newFreeConsultantForm1" class="">
                            <div>
                                <form class="form-horizontal newFreeConsultantForm" id="newFreeVastuConsultantForm"
                                    method="post">

                                    <input type="hidden" value="<?php echo $currency ?>" name="currency" id="currency">
                                    <input type="hidden" value="<?php echo $method ?>" name="pmethod" id="pmethod">

                                    <?php if ($vastu_data['owner_name_label']) {
                            # code...
                       ?>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3"
                                                class="control-label"><?php echo $vastu_data['owner_name_label']  ?><span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="inputName" name="inputName"
                                                placeholder="<?php echo $vastu_data['owner_name_placeholder']  ?>">
                                        </div>
                                    </div>
                                    <?php  } ?>
                                    <?php if ($vastu_data['email_id_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="inputEmail"
                                                class=" control-label"><?php echo $vastu_data['email_id_label']  ?><span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="inputEmail" name="inputEmail"
                                                placeholder="<?php echo $vastu_data['email_id_placeholder']  ?>">
                                        </div>
                                    </div>
                                    <?php  } ?>

                                    <?php if ($vastu_data['owner_contact_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="inputContact" class=" control-label"
                                                class="required"><?php echo $vastu_data['owner_contact_label']  ?><span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="number" class="form-control" id="inputContact"
                                                name="inputContact"
                                                placeholder="<?php echo $vastu_data['owner_contact_placeholder']  ?>">
                                        </div>
                                    </div>
                                    <?php  } ?>

                                    <?php if ($vastu_data['property_address_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="address "
                                                class=" control-label  notmatch question_label "><?php echo $vastu_data['property_address_label']  ?></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <textarea rows="3" class="textarea-information-class form-control address"
                                                id="address" name="address"
                                                placeholder="<?php echo $vastu_data['property_address_placeholder']  ?>"></textarea>
                                        </div>
                                    </div>
                                    <?php  } ?>




                                    <?php if ($vastu_data['property_layout_maps_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3" style="display: flex;flex-direction: column;">
                                            <label for="comman_image_upload_class" class=" control-label"
                                                style=""><?php echo $vastu_data['property_layout_maps_label']  ?></label>
                                            <span class="error">Max 5 files</span>
                                        </div>
                                        <div class="form-group col-sm-9">
                                            <button type="button" id="comman_image_upload_class"
                                                name="comman_image_upload_class" img_count="multiple" img_primary="yes"
                                                img_prev_class="image_selected" img_input_id="image_id"
                                                class="btn btn-light comman_image_upload_class" data-toggle="modal"
                                                data-target="#browseImage">Upload File
                                            </button>
                                            <span class="imgalert" id="imgalert"></span>
                                            <input class="form-control image_id" id="image_id" name="image_id"
                                                type="hidden" value="">
                                            <div class="image-preview d-flex inline_block image_selected">
                                             
                                            </div>
                                        </div>

                                    </div>
                                    <?php  } ?>


                                    <?php if ($vastu_data['property_direction_orientation_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="propertydirection"
                                                class="control-label"><?php echo $vastu_data['property_direction_orientation_label']  ?></label>
                                            <div>
                                                <span
                                                    class="error"><?php echo $vastu_data['property_direction_orientation_message']  ?></span></br>
                                                    <?php $link = '#';
                                                 if (!empty($property_directioncompass_msg) && (!empty($property_directioncompass_defaultvideo) || !empty($property_directioncompass_link) || !empty($compass_image)) ) { 
                                                    $link = '#';
                                                    // if (!empty($property_directioncompass_defaultvideo)) {
                                                    //     $link = base_url().'uploads/'.$property_directioncompass_defaultvideo;
                                                    
                                                    // }
                                                    if (!empty($property_directioncompass_link)) {
                                                        $link = 'https://www.youtube.com/watch?v='.$property_directioncompass_link;

                                                    }elseif (!empty($compass_image)) {
                                                        $link = base_url().'uploads/'.$compass_image;
                                                    }
                                                    
                                                    ?>

                                            <span style="color:blue"><a href="<?php echo $link; ?> " target="_blank"><?php echo $property_directioncompass_msg ?></a></span>

                                                    <?php  } ?>


                                            </div>

                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="propertydirection"
                                                name="propertydirection"
                                                placeholder="<?php echo $vastu_data['property_direction_orientation_placeholder']  ?>">

                                     

                                        </div>
                                    </div>

                                    <?php  } ?>




                                    <div class="row mt-4 d-none">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3" class="control-label">Gender<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-9 d-none">
                                            <select name="inputGender" id="inputGender" class="form-control">
                                                <option value="">Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <?php if ($vastu_data['owner_date_time_birth_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3"
                                                class="control-label"><?php echo $vastu_data['owner_date_time_birth_label']  ?></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" id="inputDateDate" readonly name="inputDateDate"
                                                placeholder="Birth Date" class="form-control  date " value="">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" id="inputTimeMin" readonly name="inputTimeMin"
                                                placeholder="Birth Time" class="form-control time " value="">

                                        </div>
                                        <div class="col-sm-3">
                                            <label class="error1">Time : 24 hour format</label>
                                        </div>
                                    </div>
                                    <?php  } ?>

                                    <?php if ($vastu_data['owner_place_birth_label']) {
                            # code...
                       ?>
                                    <div class="row mt-4">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3"
                                                class=" control-label"><?php echo $vastu_data['owner_place_birth_label']  ?></label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="birthCountry"
                                                name="birthCountry" placeholder="Enter Birth Country">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="birthState" name="birthState"
                                                placeholder="Enter your Birth State">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" id="birthCity" name="birthCity"
                                                placeholder="Enter your Birth City/Distict">
                                        </div>
                                    </div>
                                    <?php  } ?>

                                    <div class="row mt-4 d-none">
                                        <div class="col-sm-3">
                                            <label for="inputEmail3" class="control-label">Confirm Email-id<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" id="inputEmailCrfm"
                                                name="inputEmailCrfm" placeholder="Re-enter your Email-id">
                                        </div>
                                    </div>


                                    <br />

                                    <?php if ($vastu_data['owner_name_label']) {
                            # code...
                       ?>
                                    <div class="form-center" style="text-align:center;">
                                        <button type="submit" class="submit-btn btn btn-primary vastu_submit_btn "
                                            id="btnSubmit"
                                            style="cursor: pointer !important;color:black! important">Submit</button>
                                    </div>

                                    <?php  } ?>
                                </form>
                            </div>
                        </div>
                        <script>
                        $('document').ready(function() {
                            var d = new Date().getTime();
                            $('#TID').val(d);
                        });
                        </script>
                        <script>
                        $('document').ready(function() {
                            var id = new Date().getTime();
                            $('#order_id').val(id);
                        });
                        </script>
                        <div class="container in_payment_process display_none" style="margin-top: 110px; height:600px;">
                            <input type="hidden" class="form-control" id="date" name="date"
                                value="<?php echo date("D M j Y G:i:s"); ?>">
                            <input type="hidden" class="form-control" id="TID" name="TID" value="tid">
                            <input type="hidden" class="form-control" id="order_id" name="order_id"
                                placeholder="order_id" value="order_id">
                            <div class="row">
                                <h3 style="text-align:center;"> Processing ....</h3>
                                <div class="col-md-12 text-center btn_home display_none" style="margin-top:20px;">
                                    <label style="font-size:20px;" class="label label-success"><a
                                            href="<?php echo base_url(); ?>" style="color: #fff;">GO TO HOMEPAGE
                                        </a></label>
                                </div>
                            </div>
                            <div style="display:none;">
                                <form method="post" name="redirect"
                                    action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction">

                                    <input type='text' name=encRequest id="encRequest" value="">
                                    <input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
                                    <button id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
                                </form>
                                <button id="rzp-button1">Pay</button>
                            </div>
                        </div>
                    </div>
                </div>


                <?php
				// echo $faq_category;
					$datacat = array(
					);

					$datacat['category'] = $faq_category;
					$this->load->view('faqlist',$datacat);
					?>

                <?php 
            $datacat = array(
            );

            $datacat['about_id'] = $about_content_id;
            $this->load->view('aboutvastu',$datacat);
            ?>


            </div>
        </section>



    </div>
</div>

<!-- File Button -->




<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <h3>Browse File</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div id="head1_msg"></div>
                <input type="hidden" value="<?php if($image_cat){echo $image_cat;}else{ echo 'menu'; }?>"
                    name="image_cat" class="image_cat">
                <input type="hidden" value="<?php if($sub_folder_name){ echo $sub_folder_name;}?>"
                    name="sub_folder_name" class="sub_folder_name">
                <div class="row demo-columns">
                    <div class="col-md-8 offset-md-2">
                        <p>File type should be JPG,PNG,jpeg,pdf</p>
                        <p>File should be 2 MB or smaller</p>
                        <!-- D&D Zone-->
                        <div id="drag-and-drop-zone" class="uploader">
                            <div>Drag &amp; Drop Images Here</div>
                            <div class="or">-or-</div>
                            <div class="browser">
                                <label>
                                    <span>Click to open the file Browser</span>
                                    <input type="file" name="files[]" multiple="multiple" title='Click to add Files'
                                        accept=".jpg,.jpeg,.png,.gif,.pdf">
                                </label>
                            </div>
                        </div>
                        <!-- /D&D Zone -->

                    </div>
                    <div class="clearfix"></div>
                    <!-- / Left column -->
                    <div class="col-md-8 offset-md-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Uploads</h3>
                            </div>
                            <div class="panel-body demo-panel-files minHeight" id='demo-files'>
                                <span class="demo-note">No Files have been selected/droped yet...</span>
                            </div>
                        </div>
                    </div>
                    <!-- / Right column -->
                </div>
            </div><!-- / modal-body -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<style>
.modal-header {
    background-color: #f5f5f5;
    /* Background color */
    color: #333;
    /* Text color */
    border-bottom: 1px solid #ddd;
    /* Border bottom */
    padding: 15px;
    /* Padding */
}

.modal-title {
    margin: 0;
    /* Remove margin */
}
</style>

<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <h3 class="modal-title float-left">Browse Image</h3>
                <button type="button" class="close float-right" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple accept=".jpg,.jpeg,.png,.gif,.pdf">

                </div>
                <div class="modal-footer d-flex justify-content-end" style="width:100%;">
                    <div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>

                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
<script>
$('document').ready(function() {


    var img_counter = 0;
    var img_prev_class = '';
    var img_input_id = '';
    var img_count = 'multiple';
    var img_primary = 'no';
    var max_upload = 5;

    $('body').on('click', '.comman_image_upload_class', function() {
        img_prev_class = $(this).attr('img_prev_class');
        img_input_id = $(this).attr('img_input_id');
        img_count = $(this).attr('img_count');
        img_primary = $(this).attr('img_primary');
    });
    /*uploading drag and drop image*/
    $('#drag-and-drop-zone').dmUploader({

        url: APP_URL + 'common/upload_file',
        dataType: 'json',
        extraData: {
            image_cat: $('.image_cat').val(),
            sub_folder_name: $('.sub_folder_name').val(),
        },
        maxFiles: '5',
        fileName: 'myFile',
        // allowedTypes: 'image/*,application/pdf', // Allow both image and PDF files

        // allowedTypes: 'application/pdf', // Allow both image and PDF files
        // allowedTypes: 'image/*,application/pdf',
        onInit: function() {
            $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
        },

        onBeforeUpload: function(id) {

            $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' +
                id);

            $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
        },
        onNewFile: function(id, file) {
            $('.demo-panel-files').empty();
            $.danidemo.addFile('#demo-files', id, file);
        },
        onComplete: function() {
            $.danidemo.addLog('#demo-debug', 'default',
                'All pending tranfers completed');
        },
        onUploadProgress: function(id, percent) {
            var percentStr = percent + '%';

            $.danidemo.updateFileProgress(id, percentStr);
        },
        onUploadSuccess: function(id, data) {
            $('#imgalert').html('');
            console.log(data);
            var status = data.status;
            var filename_ = data.filename;
            if (status == 200) {
                console.log(img_prev_class);
                console.log(img_input_id);
                if (img_count == 'single') {
                    $('.' + img_prev_class).removeClass('display_none');
                    $('.' + img_prev_class).attr('src', APP_URL + 'uploads/' + $(
                        '.sub_folder_name').val() + '/' + filename_);
                    $('#' + img_input_id).val(filename_);
                    //$('#'+img_input_id).css("display","none");
                } else {
                    if (img_counter < max_upload) {



                        img_counter++;
                        console.log(img_counter + 'increase');


                        var primary = '';
                        if (img_counter == 1 && img_primary == 'yes') {
                            primary = 'active';
                        }

                        var fileExtension = filename_.split('.').pop().toLowerCase();
                        var imgExtensions = ["png", "jpg", "jpeg"];
                        if ($.inArray(fileExtension, imgExtensions) !== -1) {

                            $('.' + img_prev_class).append('<div class="img_dives ' + primary +
                                '" addr="' + filename_ + '" ><img  src="' + APP_URL +
                                'uploads/' + filename_ +
                                '" style="width:100px;" ><button type="button" class="btn img_remover" >x</button></div>'
                            );

                        } else if (fileExtension === "pdf") {
                            // Handle PDF file
                            $('.' + img_prev_class).append('<div class="img_dives ' + primary +
                                '" addr="' + filename_ + '" ><img  src="' + APP_URL +
                                'assets/img/pdf.png" style="width:100px;" ><button type="button" class="btn img_remover" >x</button></div>'
                            );
                        }

                        if (img_counter == (max_upload)) {
                            $('.comman_image_upload_class').prop('disabled', true);

                        }

                    } else {

                        alert('you can upload only 5 files');

                        // Disable further uploads
                        // console.log("Maximum uploads reached.");
                        // return;


                    }



                }
                $('#browseImage').modal('hide');
            } else {
                $('#browseImage').modal('hide');
                alert(data.message);
            }
            $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id +
                ' completed');

            $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id +
                ': ' + JSON.stringify(data));

            $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

            $.danidemo.updateFileProgress(id, '100%');
        },
        onUploadError: function(id, message) {
            alert(message);
            $.danidemo.updateFileStatus(id, 'error', message);

            $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id +
                ': ' + message);
        },
        // onFileTypeError: function(file) {
        //     alert('File \'' + file.name + '\' cannot be added: must be an image');
        //     $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name +
        //         '\' cannot be added: must be an image');
        // },
        onFileTypeError: function(file) {
            if (file.type !== 'image/jpeg' && file.type !== 'image/png' && file.type !==
                'application/pdf') {
                alert('File \'' + file.name + '\' cannot be added: must be an image or PDF');
                $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name +
                    '\' cannot be added: must be an image or PDF');
            } else {
                alert('File \'' + file.name + '\' has an unsupported file type');
                $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name +
                    '\' has an unsupported file type');
            }
        },
        onFileSizeError: function(file) {
            alert('File \'' + file.name + '\' cannot be added: size excess limit');
            $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name +
                '\' cannot be added: size excess limit');
        },
        /*onFileExtError: function(file){
          $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
        },*/
        onFallbackMode: function(message) {
            alert('info', 'Browser not supported(do something else here!): ' + message);
            $.danidemo.addLog('#demo-debug', 'info',
                'Browser not supported(do something else here!): ' + message);
        },
        onFilesMaxError: function(file) {
            alert(' cannot be added to queue due to upload limits.');
            $.danidemo.addLog(file.name +
                ' cannot be added to queue due to upload limits.');
        }
    });
    $('body').on('click', '.img_remover', function() {
        if ($(this).parent().hasClass('active')) {
            console.log('has actvie');
            $(this).parent().remove();
            setTimeout(function() {
                $('.img_dives').first().addClass('active');
            }, 50);


        } else {
            console.log('no actvie');
            $(this).parent().remove();
        }

        img_counter--;
        var isDisabled = $('.comman_image_upload_class').prop('disabled');


        if (img_counter < (max_upload)) {

            if (isDisabled) {
                $('.comman_image_upload_class').prop('disabled', false);

            }


        }
        console.log(img_counter + 'delete');
    });
    $('body').on('click', '.img_dives img', function() {
        if (img_primary == 'yes') {
            if ($(this).parent().hasClass('active')) {
                $(this).parent().removeClass('active');
            } else {
                $('.img_dives').removeClass('active');
                $(this).parent().addClass('active');
            }
        }
    });



    $('body').on('keyup', '#inputQuestion', function() {

        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');

        current.text(characterCount);
        var maximum_size = parseInt($(this).attr('maxlength'));
        console.log(maximum_size);

        /*This isn't entirely necessary, just playin around*/
        if (characterCount < ((maximum_size / 100) * 25)) {
            current.css('color', '#666');
        }
        if (characterCount > ((maximum_size / 100) * 25) && characterCount < ((maximum_size / 100) *
                30)) {
            current.css('color', '#6d5555');
        }
        if (characterCount > ((maximum_size / 100) * 30) && characterCount < ((maximum_size / 100) *
                35)) {
            current.css('color', '#793535');
        }
        if (characterCount > ((maximum_size / 100) * 35) && characterCount < ((maximum_size / 100) *
                40)) {
            current.css('color', '#841c1c');
        }
        if (characterCount > ((maximum_size / 100) * 40) && characterCount < ((maximum_size / 100) *
                60)) {
            current.css('color', '#8f0001');
        }

        if (characterCount >= ((maximum_size / 100) * 60)) {
            maximum.css('color', '#8f0001');
            current.css('color', '#8f0001');
            theCount.css('font-weight', 'bold');
        } else {
            maximum.css('color', '#666');
            theCount.css('font-weight', 'normal');
        }
    });


    //-----------------------------------------------------------------------
    /* 
     * consultation_form validation
     */

    // $('#upload_image1').ajaxForm({
    //     dataType: 'JSON',
    //     success: function(response) {
    //         if (response.status == 200) {
    //             $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
    //             $('#browseImage1').modal('hide');
    //             $('#c_image').val(response.filename);
    //             $('#blog_image-error').css({
    //                 "display": "none"
    //             });
    //         } else {
    //             $('#head1_msg').empty();

    //             $('#head1_msg').html(
    //                 "<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
    //                 response.message + "</strong></div>");
    //             $("#head1_msg").fadeTo(2000, 500).slideUp(500, function() {
    //                 $('#head1_msg').empty();
    //                 // window.location.href = APP_URL+'';
    //             });

    //             $('#myFile').val('');
    //             $(".img-preview1").attr('src', '');
    //             $('#c_image').val('');


    //             // $('#browseImage1').modal('hide');

    //         }
    //         $.unblockUI();
    //         return false;
    //     }
    // });

    $('#newFreeVastuConsultantForm').validate({
        rules: {

            inputName: {
                required: true
            },
            comman_image_upload_class: {
                required: true
            },

            // inputGender: {
            //     required: true
            // },
            // inputDateDate: {
            //     required: true
            // },
            // inputTimeMin: {
            //     required: true
            // },


            // birthCountry: {
            //     required: true
            // },
            // birthState: {
            //     required: true
            // },
            // birthCity: {
            //     required: true
            // },
            inputEmail: {
                required: true
            },
            //propertydirection: {
            //    required: true
            //},
            // inputEmailCrfm: {
            //     required: true,
            //     equalTo: "#inputEmail"
            // },
            inputContact: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 12
            },
            // inputQuestion: {
            //     required: true,
            //     maxlength: 150
            // },



        },
        messages: {

            inputName: {
                required: "Full name is required"
            },
            comman_image_upload_class: {
                required: "atleast one image is required"
            },

            // inputGender: {
            //     required: "Gender is required"
            // },
            // inputDateDate: {
            //     required: "Birth date is required"
            // },
            // inputTimeMin: {
            //     required: "Birth time is required"
            // },
            // birthCountry: {
            //     required: "Birth country name is required"
            // },
            // birthState: {
            //     required: "Birth state name is required"
            // },
            // birthCity: {
            //     required: "Birth city name is required"
            // },
            inputEmail: {
                required: "Email id is required"
            },
            //propertydirection: {
            //    required: "propertydirection is required"
            //},
            // inputEmailCrfm: {
            //     required: "Confirm email id  is required",
            //     equalTo: "Confirm email id don't match."
            // },
            inputContact: {
                required: "Contact number is required.",
                number: "Only numbers are allowed",
                minlength: "Min. 10 digits are required",
                maxlength: "Max. 12 digits are required"
            },
            // inputQuestion: {
            //     required: "Precise question is required.",
            //     maxlength: "Max. 150 character are required"
            // },

        },
        submitHandler: function(form) {

            var currency = $('#currency').val();
            var payment_method = $('#pmethod').val();

            console.log(payment_method);

            $currmethod = 'not available';

            // if (payment_method == 'Paypal') {
            //     $currmethod = 'Paypal';


            // }




            var consultantType = $('#consultantType').val();
            var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val();
            var inputTimeMin = $('#inputTimeMin').val();
            var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
            var birthCity = $('#birthCity').val();
            var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            var address = $('#address').val();
            var propertydirection = $('#propertydirection').val();
            // var c_image = $('#c_image').val();
            var primary_image = '';

            var images = [];

            $('.image_selected .img_dives').each(function() {
                if ($(this).hasClass('active')) {
                    primary_image = $(this).attr('addr');
                }
                images.push($(this).attr('addr'));
            });

            console.log(images);

            // if (images.length < 1) {
            //     console.log('altead one pic')
            //     $('#imgalert').html('<p class="error">atleast add one File</p>');


            //     return;

            // }


            if (images.length > 5) {
                console.log('no images show');
                $('#imgalert').html('<p class="error">minimum 5 file uploaded</p>');

                return;

            }

            if (images.length >= 1 && images.length <= 5) {
                $('#imgalert').html('');


            }
            // return;

            var currency = 'no payment';
            var payment_method = 'no payment';
            // var currency = $('#currency').val();
            // var payment_method = $('#pmethod').val();
            var serviceCost = parseFloat($("input[name='radioBtnClass']:checked").attr('cost'));
            var amount = serviceCost;
            var servicediscount = parseFloat($("input[name='radioBtnClass']:checked").attr(
                'd_price'));
            var discount = servicediscount;
            var grandtotal = 0;
            if (discount > 0) {
                grandtotal = parseFloat(amount) - parseFloat(servicediscount);
            } else {
                grandtotal = parseFloat(amount);
            }
            $.blockUI();


            // console.log(c_image);
            $.post(APP_URL + 'welcome/add_new_vastu_consultant_free', {
                consultantType: consultantType,
                primary_image: primary_image,
                images: images,
                inputName: inputName,
                payment_method: payment_method,
                currency: currency,
                inputGender: inputGender,
                inputDateDate: inputDateDate,
                inputTimeMin: inputTimeMin,
                birthCountry: birthCountry,
                birthState: birthState,
                birthCity: birthCity,
                inputEmail: inputEmail,
                inputContact: inputContact,
                servicecost: serviceCost,
                propertydirection: propertydirection,
                subtotal: amount,
                discount: discount,
                grandtotal: grandtotal,
                address: address,

            }, function(response) {
                $.unblockUI();
                console.log(response);
                var order_payment_id = response.data;
                let data = response.data;
                if (response.status == 200) {
                    window.location.href = APP_URL +
                        'welcome/razorResponseHandlerservicevastu/' + data;

                    // if ($currmethod == 'ccavenu') {
                    //     $.post(APP_URL + 'welcome/convertdataproductservce_vastu', {},
                    //         function(response) {
                    //             var data = response.data;
                    //             console.log(data);
                    //             $('#encRequest').val(data);
                    //             $('#finalPayentSubmitBtn').trigger('click');
                    //         }, 'json');
                    // } else if ($currmethod == 'Razor') {
                    // console.log('Razor');
                    // console.log(grandtotal);
                    // var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
                    // var paid_amount = grandtotal;
                    // console.log(paid_amount * 100);

                    // var options = {
                    //     "key": razor_all_key,
                    //     "amount": (paid_amount * 100), // 2000 paise = INR 20
                    //     "name": "Maya Astrology",
                    //     "description": "Maya Astrology Paid Consultation",
                    //     "image": "",
                    //     "captured": true,
                    //     "handler": function(response) {
                    //         //alert(response.razorpay_payment_id);
                    //         console.log(response);
                    //         if (response) {
                    //             $.post(APP_URL + 'welcome/vastu_paytest', {
                    //                     amount: (paid_amount),
                    //                     payment_id: response
                    //                         .razorpay_payment_id,
                    //                     razor_key: razor_all_key,
                    //                     seceret_key: seceret_key,
                    //                     order_payment_id: order_payment_id,
                    //                 },
                    //                 function(response2) {
                    //                     console.log(response2);

                    //                     var data = response2.data;
                    //                     console.log(data);
                    //                     console.log('kya')
                    //                     console.log(order_payment_id +
                    //                         'hii');
                    //                     // window.location.href = APP_URL +
                    //                     //     'welcome/free_vastu_consultation_thankyou/' +
                    //                     //     response.data + consultantType +
                    //                     //     '/' + consultantType;
                    //                     window.location.href = APP_URL +
                    //                         'welcome/razorResponseHandlerservicevastu/' +
                    //                         data;

                    //                 }, 'json');
                    //         }

                    //     },
                    //     "prefill": {
                    //         "name": inputName,
                    //         "contact": inputContact,
                    //         "email": inputEmail,
                    //     },
                    //     "notes": {
                    //         "address": "Paid Consultation"
                    //     },
                    //     "theme": {
                    //         "color": "#F37254"
                    //     }
                    // };
                    // var rzp1 = new Razorpay(options);

                    // document.getElementById('rzp-button1').onclick = function(e) {
                    //     rzp1.open();
                    //     e.preventDefault();
                    // }
                    // $('#rzp-button1').trigger('click');

                    // } else {
                    //     if ($currmethod == 'Paypal') {



                    //         var paid_amount = grandtotal;
                    //         // console.log('amount' + paid_amount);

                    //         window.location.href = APP_URL + 'paypal/index?order_id=' +
                    //             order_payment_id + '&total_amount=' + paid_amount +
                    //             '&currency=USD' + '&page=service';


                    //     }
                    //     console.log('hello world');

                    // }
                    /*function (response) {
                    console.log(response);
                    	$("html, body").animate({scrollTop: 0}, "slow");
                    $('#err_consultation_div').empty();
                    	if (response.status == 200) {
                    		
                    		$('#err_consultation_div').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                    	var responsedata =(response.data);
                    		console.log(response.data.consultantType);
                    		console.log(response.data.insert_id);
                    		$('#consultantType').val('');
                    		$('#inputName').val('');
                    		$('#inputGender').val('');
                    		$('#inputDateDate').val('');
                    		$('#inputTimeMin').val('');
                    		$('#birthCountry').val('');
                    		$('#birthState').val('');
                    		$('#birthCity').val('');
                    		$('#inputEmail').val('');
                    		$('#inputContact').val('');
                    		
                    	
                    	setTimeout(function() {
                    		
                    			window.location.href = APP_URL+'welcome/free_vastu_consultation_thankyou/'+response.data+consultantType+'/'+consultantType;	
                    	}, 500);
                    		
                    	}else if (response.status == 201) {
                    		$('#err_consultation_div').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                    	}
                    	
                    },'json');
                    return false;*/

                }
            }, 'json');
        }
    });
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"
    integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous">
</script>