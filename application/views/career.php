﻿
<style>
.star_red{color:red;}
</style>
   
		<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Career</h1>
            </div>
          
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>	<!-- Page Title -->
			
			
			<section style="padding:0px 0px;">
				<div class="container " style="margin-top:50px;margin-bottom:50px;">
					<div class="row">
					  <div id="headerMsg"></div>
					  <div class="col-md-3"></div>
						<div class="col-md-6 sm-mb-30 wow bounceInDown  " data-wow-delay="0.2s">
							
						
								<div class="text-center">
									<p class="contactform nobottommargin  text_brown_color" style="    font-size:20px; padding-bottom:15px; padding-top:15px;"><strong>Please Fill The Form</strong></p>
									<div class="divider divider-center divider-dark" style="margin-top:0px;" ></div>
								</div>
								
								<form  id="add_cf_form" class="add_cf_form nobottommargin " method="post">
										<input class="cf_id form-control" id="cf_id" name="cf_id" value="" type="hidden">
										<div style="padding: 10px;">
										<div>
										    <label>Name<span class="star_red">*</span></label>
											<input type="text" id="cf_name" name="cf_name" placeholder="Name" class="form-control input-border-bottom text_brown_color ">
										</div>
										<div>
										     <label>Phone<span class="star_red">*</span></label>
											<input type="text" id="cf_phone" name="cf_phone" placeholder="Phone" class="form-control input-border-bottom text_brown_color">
										</div>
										<div>
										     <label>Address<span class="star_red">*</span></label>
											<textarea id="cf_address" name="cf_address" placeholder="Address" class="form-control input-border-bottom text_brown_color "></textarea>
										</div>
										<div>
										     <label>Qualification<span class="star_red">*</span></label>
											<input type="text" id="cf_qualification" name="cf_qualification" placeholder="Qualification" class="form-control input-border-bottom text_brown_color">
										</div>
										<div>
										      <label>Email<span class="star_red">*</span></label>
											<input type="email" id="cf_email" name="cf_email" placeholder="Email" class="form-control input-border-bottom text_brown_color">
										</div>
										
										<div>
										     <label>Experience in Years <span class="star_red">*</span></label>
											<select id="cf_experience" name="cf_experience" class="form-control input-border-bottom">
			                                      <option value="" style="color:black;">Enter your Experience in Years </option>
			                                     <option  style="color:black;" value="">Experience in Years </option>
			                                     <option value="0" style="color:black;" value="">0</option>
			                                     <option value="1" style="color:black;" value="">1</option>
			                                     <option value="2" style="color:black;" value="">2</option>
			                                     <option value="3" style="color:black;" value="">3</option>
			                                     <option value="4" style="color:black;" value="">4</option>
			                                     <option value="5" style="color:black;" value="">5</option>
			                                     <option value="6" style="color:black;" value="">6</option>
			                                     <option value="7" style="color:black;" value="">7</option>
			                                     <option value="8" style="color:black;" value="">8</option>
			                                     <option value="9" style="color:black;" value="">9</option>
			                                     <option value="10" style="color:black;" value="">10</option>
			                                     
		                                    </select>
										</div>
										<div>
										     <label>Enter Year Of Passout<span class="star_red">*</span></label>
											<input type="text" id="cf_passout" name="cf_passout" placeholder="Enter Year Of Passout" class="form-control input-border-bottom text_brown_color">
										</div>
										<div>
										     <label>Enter Your Skills<span class="star_red">*</span></label>
											<input type="text" id="cf_skills" name="cf_skills" placeholder="Enter Your Skills" class="form-control input-border-bottom text_brown_color">
										</div>
                                        
										
										<div>
										      <label>Work Experience Details(If Any)<span class="star_red">*</span></label>
											<textarea  id="cf_work" name="cf_work" placeholder="Enter your Work Experience Details(If Any)" class="form-control input-border-bottom text_brown_color"></textarea>
										</div>
										</div>
										
											<button class="btn  btn-success" type="submit" id="btnSubmit" >Submit</button>
																				
									
								</form>
								
					
							
						</div>
			             <div class="col-md-3"></div>

						
					</div>
				</div>
			</section>
							
					
					
			 <script>
	$('document').ready(function(){
		
	
					
		//-----------------------------------------------------------------------
    /* 
     * validation of add Contact
     */
	$('#add_cf_form').validate({
		ignore: [],
        rules: {
            cf_name: {
                required: true,
            },
			 cf_phone: {
                required: true,
            },
			cf_address: {
                required: true,
            },
			cf_email: {
                required: true,
            },
			cf_experience: {
                required: true,
            },
			
			cf_skills: {
                required: true,
            },
			cf_passout: {
                required: true,
            },
			cf_qualification: {
                required: true,
            },
           
         },
		 messages: {
			cf_name: {
                required: "Name is required.",
            },
			cf_phone: {
                required: "Phone No is required.",
            },
			cf_address: {
                required: "Address is required.",
            },
			cf_email: {
                required: "Email is required.",
            },
			cf_experience: {
                required: "Experience is required.",
            },
			cf_passout: {
                required: "passout is required.",
            },
			cf_qualification:{
                required: "Qualification is required.",
            },
			cf_skills:{
                required: "skills is required.",
            },
			
		},
		
       
		submitHandler: function (form) {
			
            var cf_name = $('#cf_name').val();
            var cf_phone = $('#cf_phone').val();
            var cf_address = $('#cf_address').val();
            var cf_email = $('#cf_email').val();
            var cf_work = $('#cf_work').val();
            var cf_skills = $("#cf_skills").val()
            var cf_qualification = $("#cf_qualification").val()
            var cf_passout = $("#cf_passout").val()
            var cf_experience = $("#cf_experience").val()
        
			
			$.post(APP_URL + 'career/update_career', {
                  cf_name: cf_name,
                cf_phone: cf_phone,
                cf_address: cf_address,
                cf_email: cf_email,
                cf_work: cf_work,
                cf_skills: cf_skills,
                cf_qualification: cf_qualification,
                cf_passout: cf_passout,
                cf_experience: cf_experience,
              
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.reload();
					});
					
				}else{
					 $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
					 
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.reload();
					});
				}
                        
                
			
				$("#cf_name").val('');
				$("#cf_phone").val('');
				$("#cf_address").val('');
				$("#cf_email").val('');
				$("#cf_experience").val('');
				$("#cf_passout").val('');
				$("#cf_qualification").val('');
				$("#cf_skills").val('');
				$("#cf_work").val('');
			
			}, 'json');
		return false;
		},
	});
	
	
	
	
	 
});
</script>
