

<?php
$sym = '';
if ($order_detail->currency == 'USD') {
	$sym = '$';
	# code...
}

if ($order_detail->currency == 'INR') {
	$sym = '₹';


	# code...
}
?>
<div id="site-main" style="background-color: #fff;padding: 80px 0px;">				
	<div class="page-section v1-one">
		<div class="container">
			<div class="row cart">							
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 10px;">
					<div class="row" >				
						<div class="col-md-6">
						<div   style="border: 1px solid #bcbcbc;background-color: #eee;box-shadow: inset 7px 7px 9px #bcbcbc, inset -7px -7px 13px #ffffff; padding: 15px;">
						<div class="row">
							<div class="col-md-3 col-sm-6">
							</div>
							<div class="col-md-3 col-sm-6">
								<h3 style="padding: 16px;color: #849EC6;text-align: center;">INVOICE</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6" >		
								<h3 style="word-break: break-all;font-size: 24px;">mayasastrology.com</h3>
							</div>
							<div class="col-md-6 col-sm-6"  >
								<p style="margin-bottom: 6px;line-height: 1;">Date: <span class="date_span"><?php echo $order_detail->payment_date; ?></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Invoice ID: <span class="invoice_id"><?php echo $order_detail->order_id; ?></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Payment ID: <span class="payment_id"><?php echo $order_detail->payment_id; ?></span></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6"  >
								<h5 >BILL TO</h5>
								<p style="margin-bottom: 6px;line-height: 1;">Name : <?php echo $sercice_detail->full_name; ?><span class="span_name"></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Email : <?php echo $sercice_detail->inputEmail; ?><span class="span_email"></span></p>
								
								
								<p style="margin-bottom: 6px;line-height: 1;">Contact No: <?php echo $sercice_detail->inputContact; ?><span class="span_contectno"></span></p>
							</div>
							<div class="col-md-6 col-sm-6" >
							
							</div>
						</div>
						<div class="row">
							<table class="shop_cart_table table table-bordered">
								<thead>
									<tr>
										<th class="product-empty">Service Name</th>
										<th class="product-subtotal">Service Charge</th>
									</tr>
								</thead>
								<tbody>												
									<tr>
										
									<tr class="darker-on-hover"><td><span class="span_service"><?php echo $order_detail->itemname; ?></span></td>
									<td><?php echo $sym ; ?> <span class="span_charge"><?php echo $order_detail->itemcost; ?></span></td>
													
									</tr>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="" style="width: 100%;padding: 10px;">
								<table style="width: 100%;    text-align: right;">
									<tbody style="font-size: 16px;">
										<tr>
											<td>Sub Total</td>
											<td> : </td>
											<td style="text-align: right;"><?php echo $sym ; ?> <span class="span-subtotal span_charge"><?php echo $order_detail->subtotal; ?></span></td>
										</tr>
										<tr>
											<td>Discount(-)</td>
											<td> : </td>
											<td style="text-align: right;"><?php echo $sym ; ?> <span class="span-discount"><?php echo $order_detail->discount; ?></span></td>
										</tr>
										<?php if($order_detail->coupon_discount_amount > 0){ ?>
										<tr>
											<td>Coupon(-) (<?php echo $order_detail->coupan_code;?>)</td>
											<td> : </td>
											<td style="text-align: right;"><?php echo $sym ; ?> <span class="span-discount"><?php echo $order_detail->coupon_discount_amount; ?></span></td>
										</tr>
										<?php } ?>
										<tr>
											<td>Grand Total : </td>
											<td> : </td>
											<td style="text-align: right;"><?php echo $sym ; ?> <strong><span class="span-grandtotal"><?php echo $order_detail->grandtotal; ?></span></strong></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div><br/>
						<div class="row">
							<p style="text-align: center;">Thank you For  Purchasing the Services</p>
						</div>	
						</div>	<br/><br/><br/>
						</div>	
						<div class="col-md-1"></div>
						<div class="col-md-5" >
							<div  style="border: 1px solid bisque;padding: 15px; height:auto;border-color: bisque;background-color: azure;box-shadow: inset 7px 7px 9px bisque, inset -7px -7px 13px #ffffff;">
								<h3 style="text-align:center;padding-top: 20px;"><strong style="color: blueviolet;">Thank you !<strong></h3>
								<h4>Your payemt is successful.</h4>
								<h4>(Information sent via email also)</h4>
								<h4>Paid amount : <span class="span_service1"></span> <?php echo $sym ; ?>  <span class="span-grandtotal"><?php echo $order_detail->grandtotal; ?></span> </h4>
								<h4>Consultation code :  <span class="Consultation_Access"><?php echo ($order_detail->order_payment_id).' - '.(substr($order_detail->order_id , -4))?></span> </h4>
								<h4><?php echo $massage?></h4>
							</div>					
						</div>					
					</div>
					<div class="col-md-2" >
					</div >

					<div class="col-md-4" >
						
					
					</div>
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .page-section -->				
</div>
	<!-- .page-section
<button id="generate_pdf" class="btn btn-default btn-lg button-class display_none">download Invoice</button> -->	
<div id="editor"></div>	
<style>
.cart table.cart .product-name > *{
    display: inline;    font-size: 20px !important;color: #95c03e;
}
.product-subtotal .fa-gbp{
	font-size: 20px !important;color: #95c03e;
}
.cart_totals_inner .fa-gbp, .cart_totals_inner span.amount{
	color: #000;	
}
.shop_table{
	background-color: #fff;
}
tbody{
    font-size: 20px;
}
</style>
<script>
$('document').ready(function(){
	
});	
</script>