

<script src="<?php echo base_url();?>assets/js/configure/constantpro.js"></script>

<div class="container" style="margin-top: 110px;">
	<div class="row" >
		<h3><strong>Shopping Cart</strong></h3>
	</div>

	<div class="row" >
	
		<div class="col-sm-12"  style="background-color: #183284;">
			<div class="" style="    padding: 20px;">
				<div class="row" style="color: #fff;">
					<h3>Shopping Cart</h3>
				</div>
				<div class="table-responsive" style="background-color: #fff;">
						<table class="table table-bordered table-hover 	table-striped">
							<thead class="table_head">
								<tr>
								<th >Image</th>
								<th >Product Name</th>
								<th >Price in ₹</th>
								<th >Price in $</th>
								<th >QTY</th>
								<th >Action</th>
								</tr>
							</thead>
							<tbody class="appned_dataprodut">
								
							</tbody>
						</table>
				</div>
				<form class="form-horizontal buyproductform" id="buyproductform" method="post">
					
				
					<div  style="color: #fff;font-size: 16px; text-align: right; padding: 15px; border-bottom: 1px solid #eee;">
						<div class="row">
							<div class="col-sm-10">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
									<div class="col-sm-9">
										<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
									</div>
								</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
								<div class="col-sm-9">
									<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number">
								</div>
							</div>			
							
							<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Address<span class="required">*</span></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="inputaddress" name="inputaddress" placeholder="Enter your Full Address" row=2>
									</div>
								</div>
							<div class="form-center" style="text-align: center;">
								<button type="submit" class="submit-btn btn btn-primary " style="cursor: pointer !important;">Submit</button>	
							</div>
													
							
							</div>
											
						</div>
					</div>
				<form>
			</div>
		</div>

	</div>
</div>

<style>
label.error{    color: red;
    text-align: left;
    font-size: 12px;}
</style>