<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});
	$('document').ready(function () {
		$('.Career').css({'display':'none'});
		$('.MatchMaking').css({'display':'none'});	
		$('body').on('change','#consultantType',function(){
			var consultantType = $(this).val();
			if(consultantType == 'career' || consultantType == 'finance' ){
				$('.Career').css({'display':'block'});
				$('.MatchMaking').css({'display':'none'});
			}else if(consultantType == 'gemstone' || consultantType == 'horoscope'){
				$('.Career').css({'display':'none'});
				$('.MatchMaking').css({'display':'none'});			
			}else if(consultantType == 'marriage' || consultantType == 'love'){
				$('.Career').css({'display':'none'});
				$('.MatchMaking').css({'display':'block'});			
			}
		});
	});
</script>

<style>
.form-control{
	    height: 34px !important;
}
/* label.control-label{
	color: #fff !important;
	
}*/
.control_div{
	//color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 900px;
	 height: 225px;
 }
 .display_none{
	 display:none !important;
}
</style>
<script src="<?php echo base_url(); ?>assets/js/consultation.js"></script> 
<div id="content">
<div class="container">


 <!-- modal for free telephonic consultancy -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="newFreeConsultantForm1" class="">
	
	<div class="modal-header">
		<div id="err_consultation_div"></div>
		<div hidden class="col-md-12 image_div">
			<img class="img-preview" src="<?php echo base_url()?>/uploads/<?php echo $popup_ads[0]['free_consultation_image'];?>" alt="Mayas Astrology">
		</div>
		<h3 style="text-align:center;padding-bottom: 50px;font-size:30px"><span style="width: 30px; height: 30px; background-color: #678; padding: 2px 12px; border-radius: 50%; color: #fff;">2</span> <strong> Enter Birth Information</strong></h3>
		<div hidden  class="freeConsultant-form" style="padding-top: 10px;">Fill form to ask one free Question</div>
		<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
			<div hidden class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Select type of consultation<span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="consultantType" id="consultantType" class="form-control consultantType" >
						<option value="" name="">Select the type of free consultation</option>
						<option value="career" name="career">Question Related to Career</option>
						<option value="finance" name="finance">Question Related to Finance</option>
						<option value="gemstone" name="gemstone">Question Related to Birth Stone</option>
						<option value="marriage"  name="marriage">Question Related to Marriage</option>
						<option value="love"  name="love">Question Related to Love</option>
						<option value="horoscope"  name="horoscope">Question Related to Horoscope Query</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Gender<span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="inputGender" id="inputGender" class="form-control">
					<option value="">Gender</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" id="inputDateDate" name="inputDateDate" placeholder="Birth Date" class="form-control  date " value="">  
				</div>
				<div class="col-sm-3">
					<input type="text" id="inputTimeMin" name="inputTimeMin" placeholder="Birth Time" class="form-control time " value="">
					
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour format</label>
				</div>
		  </div>
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Confirm Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number">
				</div>
			</div>
			<div hidden class="form-group Career">
				<label for="inputEmail3" class="col-sm-3 control-label">Highest Qualification<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="educationQualification" name="educationQualification" placeholder="Enter your Highest Qualification">
				</div>
			</div>
			<div hidden class="form-group Career">
				<label for="inputEmail3" class="col-sm-3 control-label">Current Profession<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="currentProfession" name="currentProfession" placeholder="Enter Your Current Profession">
				</div>
			</div>
			<div hidden class="form-group MatchMaking">
				<div class="control_div">Enter your Partner Infomation here</div>
				<label for="inputEmail3" class="col-sm-3 control-label">Partner Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="partnerName" name="partnerName" placeholder="Enter your Partner Full Name">
				</div>
				<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" id="partnerDateDate" name="partnerDateDate" placeholder="Birth Date" class="form-control  date " value="">  
				</div>
				<div class="col-sm-3">
					<input type="text" id="partnerTimeMin" name="partnerTimeMin" placeholder="Birth Time" class="form-control time " value="">
					
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour format</label>
				</div>
			</div>
			
			<div hidden  class="form-group ">
				<label for="inputEmail3" class="col-sm-3 control-label">Precise Question<span class="required">*</span></label>
				<div class="col-sm-9">
					<textarea rows="3" class="textarea-information-class" id="inputQuestion" name="inputQuestion" placeholder="Enter your one Precise Question for free consultation question"></textarea>
				</div>
			</div>
			<div hidden class="form-group  ">
				<label for="inputEmail3" class="col-sm-3 control-label">Other Infomation</label>
				<div class="col-sm-9">
					<textarea rows="3" class="textarea-information-class" id="otherInfomation" name="otherInfomation" placeholder="Enter usefull infomation Related to question"></textarea>
				</div>
			</div>
			
			<div class="form-center" style="text-align: center;">
				<button type="submit" class="submit-btn btn btn-primary" style="cursor: pointer !important;">Submit</button>	
			</div>
		</form>


	</div>
	<div class="modal-footer" style="display:none;">
		<a rel="nofollow" href="#" class="free-close-btn btn">Close</a>
	</div>
</div>

</div>  
</div>