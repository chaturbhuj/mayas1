<style>
    .new_risk_box{
        cursor: pointer;
    }
</style>


<section class="pad-top-80 pad-bottom-50 " style="background-color: #fff; padding: 50px 0;">
    <div class="container">
        <div class="section-title text-center">
            <h1 style="padding: 25px 0;">Free Consultation</h1>
        </div>
        <!--.section-title-->
        <div class="row">
            <?php
                if ($consultation_content == 0) {
                    echo 'No record found into database';
                    } else {
                    $i = 1;
                    foreach ($consultation_content as $value) {
						
					?>
            <!--.col-md-4-->
            
            <div class="col-sm-4 padding_b_20 text-center mb-4 premium_services"  url="<?php echo base_url(); ?>free-consultation/<?php echo $value['url'];?> ">
				
                <div class="new_risk_box newrisk_box1" >
                    <div class="w-100 img_box">
                        <img class="risk_first_image img-fluid w-100" src="<?= base_url().'uploads/'.$value['c_image'] ?>" alt="image">
                    </div>
                    <div class="new_risk_box_content new_risk_box_content_1" >
                        <div class="new_risk_box_content2" style="padding: 5px 10px;">
                            <h1 class="text-white mb-2" style="font-size: 22px;"><?= $value['consultation_name'] ?></h5>
                            <p><?=$value['main_description']?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
					$i++;
				}
			}	
		    ?>
        </div>
        <!--.row-->
    </div>
    <!-- .container -->
    
   
</section>

<script>
    $(document).ready(function(){
       
    });
 </script>



