<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});
	
</script>
<script>
	$('document').ready(function(){
		var d = new Date().getTime();
		$('#TID').val(d);
	});
</script>
<script>
	$('document').ready(function(){
		var id = new Date().getTime();
		$('#order_id').val(id);
	});
</script>
<style>
.form-control{
	    height: 34px !important;
}
/* label.control-label{
	color: #fff !important;
	
}*/
.control_div{
	//color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 90px;
	 height: 40px;
 }
 .display_none{
	 display:none !important;
}
</style>

<div id="content">
<div class="container">


 <!-- modal for free telephonic consultancy -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="newFreeConsultantForm1" class="">
			
                        
	<div class="modal-header">
		<div id="err_consultation_div"></div>
		
		<h3 style="text-align:center;padding-bottom: 50px;color: green;"> <strong>Bespoke</strong></h3>
		
		
		<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
			
		
			
			
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			
			
			
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Confirm Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number">
				</div>
			</div>
			<div   class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">TID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="TID" name="TID" placeholder="TID" value="tid">
				</div>
			</div>

			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">order_id<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="order_id" name="order_id" placeholder="order_id" value="order_id">
				</div>
			</div>
			
		
			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">redirect_url<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="redirect_url" name="redirect_url" placeholder="redirect_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerbesoke">
				</div>
			</div>
			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">cancel_url<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="cancel_url" name="cancel_url" placeholder="cancel_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerdonate">
				</div>
			</div>
			<div  class="form-group ">
				<label for="inputEmail3" class="col-sm-3 control-label">Amount<span class="required">*</span></label>
				<div class="col-sm-9">
					<input  type="text" class="form-control" id="amount" name="amount" placeholder="amount" value="">
				</div>
			</div>
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Currency<span class="required">*</span></label>
				<div class="col-sm-9">
				
				<select name="currency" id="currency" class="form-control "  >
					<option value="" name="">Select Currency</option>
					<option value="USD" name="">USD</option>
					<option selected value="INR" name="">INR</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
				
				
					<div class="radioBtnClass1">
						
					
						
						<input type="radio" checked class="radioBtnClass"  name="radioBtnClass" value="paypal"> <img class="img-preview" src="<?php echo base_url()?>/uploads/paypal.png" alt="Mayas Astrology">
						</br></br>
						
						<input type="radio" class="radioBtnClass"  name="radioBtnClass" value="other">  <img class="img-preview" src="<?php echo base_url()?>/uploads/card.jpg" alt="Mayas Astrology">
						</br></br></br></br>
						
						
					</div>
				</div>
			</div>
			
			
			
			<div class="form-center" style="text-align: center;">
				<button type="submit" class="submit-btn btn btn-primary" style="cursor: pointer !important;">Submit</button>	
			</div>
		</form>


	</div>
	
</div>

</div>  
</div>
<!--hide from gate way-->
<div>

	

</div>
<div style="display:none;">
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 

<input type='text' name=encRequest id="encRequest" value="">
 <input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
<button  id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
</form>

</div>

<script>
$('document').ready(function(){
	
		
			var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
			//import_cookie_into_li3();
		document.cookie = "allcart_orderss_details= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 document.cookie = "Instant_Consultation_order_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 	document.cookie = "paypal_donate_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 $('#newFreeConsultantForm').validate({
		 rules: {
			
			inputName: {
                required: true
            },
			/*amount: {
                required: true
            },
			
			
		
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                required: true,
				number: true,
				minlength:10,
				maxlength:12
            },*/
			
		 },
		 messages: {
			
			 inputName: {
                required: "Full Name is required"
            },
			/* amount: {
                required: "Amount is required"
            },
			 
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },*/
			
		},
		 submitHandler: function (form) {
	   var inputName = $('#inputName').val();
            
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            
            
			
            var TID = $('#TID').val();
            var order_id = $('#order_id').val();
            var redirect_url = $('#redirect_url').val();
            var cancel_url = $('#cancel_url').val();
			var language = 'EN';
			var amount = $('#amount').val();
			var currency = $('#currency').val();
				
		
			var payment_via = $("input:radio[name=radioBtnClass]:checked").val();
			
		
           if(payment_via != 'paypal'){
           
						var billing_name = $('#inputName').val();
						var billing_address = 'mayasastrology';
						var billing_city = 'mayasastrology';
						var billing_state = 'mayasastrology';
						var billing_zip = '11111';
						var billing_country ='India';
						var billing_tel = $('#inputContact').val();
						var billing_email =$('#inputEmail').val();
						
						var delivery_name =  $('#inputName').val();
						var delivery_address = 'mayasastrology';
						var delivery_city = 'mayasastrology';
						var delivery_state = 'mayasastrology';
						var delivery_zip = '2121210';
						var delivery_country = 'India';
						var delivery_tel = $('#inputContact').val();
						var delivery_email = $('#inputEmail').val();
						var merchant_param1 = 'mayasastrology';
						var merchant_param2 = 'mayasastrology';
						var merchant_param3 = 'mayasastrology';
						var merchant_param4 = 'mayasastrology';
						var merchant_param5 = 'mayasastrology';
						var promo_code = '';
						
						
					   
						var customer_identifier = '22;';
						
					   
					   <!--coke make-->
					  
					  
					   
					   
				   var merchant_data= [];

						merchant_data.push({
							
								TID : TID,
								order_id: order_id,
								redirect_url: redirect_url,
								cancel_url: cancel_url,
								language: language,
								billing_name: billing_name,
								billing_address: billing_address,
								billing_city: billing_city,
								billing_state: billing_state,
								billing_zip: billing_zip,
								billing_country: billing_country,
								billing_tel: billing_tel,
								billing_email: billing_email,
								delivery_name: delivery_name,
								delivery_address: delivery_address,
								delivery_city: delivery_city,
								delivery_state: delivery_state,
								delivery_zip: delivery_zip,
								delivery_country: delivery_country,
								delivery_tel: delivery_tel,
								delivery_email: delivery_email,
								merchant_param1: merchant_param1,
								merchant_param2: merchant_param2,
								merchant_param3: merchant_param3,
								merchant_param4: merchant_param4,
								merchant_param5: merchant_param5,
								amount: amount,
								promo_code: promo_code,
								customer_identifier: customer_identifier,
								currency: currency,
								
						});	
					
						$.post(APP_URL+'welcome/convertdataproductservce',
						{    
						 merchant_data : merchant_data,
								
						},function (response) {
							var data = response.data;
							//console.log(data);
								$('#encRequest').val(data);

								$('#finalPayentSubmitBtn').trigger('click');
						
						
						
						
						
						
						},'json');	
		
		   }else{
			   
			  
			   
			   <!--_paypal cookee data->
							var d = new Date();
							d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
							var expires = "expires=" + d.toGMTString();		
							
						
									var paypaldonatedetail=[];
								

							paypaldonatedetail.push({
								
								order_id: order_id,
								
							
								inputName: inputName,
							
								inputContact: inputContact,
								inputEmail: inputEmail,
								
								currency: currency,
								amount: amount,
							});
							
							
							
							document.cookie = "paypal_donate_detail= "+JSON.stringify(paypaldonatedetail)+"; "+expires+";domain=;path=/";
							console.log(document.cookie);
							window.location.href = APP_URL+'paypal/index?order_id='+order_id+'&total_amount='+amount+'';
							 
							   
							
			   
		   }
			 
			
		 },
		 
		 
		 
	 });
	
});
</script>

