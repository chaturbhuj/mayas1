
<?php 
	
	if($product_details){
		$product_id = $product_details[0]['product_id'];
		$product_categories = $product_details[0]['product_categories'];
		$product_name = $product_details[0]['product_name'];
		$product_description = $product_details[0]['product_description'];
		$product_price_ind = $product_details[0]['product_price_ind'];
		$product_price_usd = $product_details[0]['product_price_usd'];
		$product_feature = $product_details[0]['product_feature'];
		
		$product_color = $product_details[0]['product_color'];
		$product_grade = $product_details[0]['product_grade'];
		$product_image_list = $product_details[0]['product_image_list'];
		
		
	
	}else{
		$product_id = 0;
		$product_description = '';
		$product_price_ind = '';
		$product_price_usd = '';
		$product_categories = '';
		$product_name = '';
		$product_feature = '';
		$product_color = '';
		$product_grade = '';
		$product_image_list = '';
		
			
		}
		
	
?>










<script src="<?php echo base_url();?>assets/js/configure/constant.js"></script>

<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Products Details</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
                <a rel="nofollow" class="btn btn-default btn-lg btn-icon  pull-right" href="#">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i> Hire Us</a>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Products Details</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->
<div class="section-column-wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="left-sidebar">
                <div class="sidebar-menu margin-bottom-30">
                   <ul>
						<?php
							foreach ($products as $value) {
						?>
                       
                        <li><a rel="nofollow" href="<?php echo base_url(); ?>welcome/productdetails/<?php echo $value['product_id'] ?>"><?php echo $value['product_name']?></a></li>
								<?php
								}
							?>								
                    </ul>
                </div>
                <!--.sidebar-menu-->

            </div>
            <!--.left-sidebar-->
        </div>
        <div class ="col-md-9">
		
			<div class="col-md-12">
				<h3><strong><?php echo $product_details[0]['product_name']; ?></strong></h3>
			</div>
			<div class="col-md-12">
				<span><strong>Categories</strong></span>:
				<span><?php echo $product_details[0]['product_categories']; ?></span>
			</div>
			<div class="col-md-12">
				<span><strong>Description</strong></span>:
				<span><?php echo $product_details[0]['product_description']; ?></span>
			</div>
			<div class="col-md-12">
				<span><strong>Feature</strong></span>:
				<span><?php echo $product_details[0]['product_feature']; ?></span>
			</div>
			<div class="col-md-12">
				<span><strong>Colour</strong></span>:
				<span><?php echo $product_details[0]['product_color']; ?></span>
			</div>
			</div<div class="col-md-12">
				<span><strong>Colour</strong></span>:
				<span><?php echo $product_details[0]['product_color']; ?></span>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<div>
						<strong>Price</strong> : <?php echo'$', $product_details[0]['product_price_usd']; ?>
					</div>
				
					<div><strong>Price</strong> :  <?php echo'₹', $product_details[0]['product_price_ind']; ?></div>
				</div>
				<div class="col-md-6" >									
					<a rel="nofollow" href="javascript:void(0)" product_id=<?php echo $product_id; ?>
					product_price_ind=<?php echo $product_price_ind; ?> 
					product_price_usd=<?php echo $product_price_usd; ?> 
					product_feature=<?php echo $product_feature; ?> 
					product_color=<?php echo $product_color; ?> 
					product_grade=<?php echo $product_grade; ?> 
					product_image_list=<?php echo $product_image_list; ?> 
					product_description=<?php echo $product_description; ?> 
					product_categories=<?php echo $product_categories; ?> 
					product_name=<?php echo $product_name; ?>  class="btn btn-primary btn-block add_cart" style="padding: 14px 30px;margin-bottom: 20px;">Add Cart</a>
								
				</div>
			</div>
		
        </div>
    </div>
</div>
</div>
<!--.section-column-wrapper-->	 
						
			