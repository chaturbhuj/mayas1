	<script src="<?php echo base_url(); ?>assets/js/contactus.js"></script>

<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Contact Us</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
                <a class="btn btn-default btn-lg btn-icon  pull-right" href="#">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i> Hire Us</a>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Contact us</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->

<section class="pad-top-50 pad-bottom-50">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Email us</h3>
                    <p>Energistically reintermediate seamless quality vectors for progressive e-services. Globally impact functional applications.</p>

                    <p><strong>Email:</strong> contact@xcorporation.com</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Call us</h3>
                    <p>Energistically reintermediate seamless quality vectors for progressive e-services. Globally impact functional applications.</p>

                    <p><strong>Phone:</strong> 1-888-345-6789, 1-888-345-6778 </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Tweet us</h3>
                    <p>Energistically reintermediate seamless quality vectors for progressive e-services. Globally impact functional applications.</p>

                    <p><strong><a href="#">Tweet us <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></strong></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pad-bottom-80">
    <div class="container">

        <div class="section-title text-center">
            <h2>Send Us your Feedback</h2>
        </div>
        <!--.section-title-->

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form id="add_contact_form" class="form-horizontal" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Name</label>
                                <input type="text" class="form-control" required="" id="nameFive-first" placeholder="First Name">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Email</label>
                                <input type="email" class="form-control" required="" id="emailFive" placeholder="Email Address">
                            </div>


                            <div class="form-group user-phone">
                                <label for="websiteOne" class="sr-only">Website</label>
                                <input type="text" class="form-control" required="" id="websiteOne" placeholder="Phone">
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-6">
                            <div class="form-group user-message">
                                <label for="messageOne" class="sr-only">Message</label>
                                <textarea class="form-control" required="" id="messageOne" placeholder="Write Message"></textarea>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
                    <button type="submit" class="btn btn-primary">Send Message</button>
                </form>
            </div><!-- /.col-md-8 -->
        </div>
        <!--.row-->
    </div>
    <!-- .container -->
</section>
