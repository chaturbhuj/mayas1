<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1><?php echo $public_page[0]['page_heading']; ?></h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home/</a></li>
            <li class="active">Page Listing</li>
        </ol>
		
		<div class="row">
			<?php  
				$city_name = '';
				if(count($city_data)){
					$city_name = ($city_data)[0]['city'];
				}
				
				echo '<div class="col-sm-12"> '.str_replace("{{cityname}}",$city_name,$public_page[0]['page_content']).' </div>';
			?>
			
		</div>
		<br/>
		<br/>
		<div class="">
								<div class="free_consultation_div_box  row   margin_top_50px">
			<?php 
							$consultation_data ='';
						   // var_dump($public_page[0]['linked_services']);
							if($public_page[0]['linked_services'] && $public_page[0]['linked_services'] != 'null'){
								$i = 0;
								$linked_service = json_decode($public_page[0]['linked_services']);
								foreach($linked_service as $content_){
									
									// var_dump($content_);
									// var_dump($content_->linked_services_id);
									?>
											
												<?php
													$consultation_data ='';
												   //var_dump($consultation_content);
													if($services){
														$i = 0;
														foreach($services as $content){  
															$disc_logo_percent = 'display_none';
															if($content['service_id'] == $content_->linked_services_id){
																if($content['disc_logo_percent']>0){
																	$disc_logo_percent = '';
																}
																// if($i  == 3){
																$consultation_data .=
																'<div class="col-12 col-sm-6 col-md-3 " style="margin-bottom: 20px;">
																	<div class="life_box_outer life_box_blue_light premium_services" style="padding: 10px; "   url="'.base_url().'premium-consultation/detail/'.$content['services_url'].'"">
																		<div class="" >
																			<p style="text-align:center !important;font-size: 20px; font-weight: 800;">
																			<img class="risk_first_image img-fluid" src="'.base_url().'uploads/'.$content['services_image'].'"  alt="image" /></p>
																			<div class="label-group '.$disc_logo_percent.'">
																		<div class="product-label label-sale">-'.$content['disc_logo_percent'].'%</div>
																	</div>
																			<h3 style="font-size: 21px; font-weight: 800; text-align: center !important;" class="text_align_left">'.$content['services_name'].' in ' .$city_name.'</h3>
																			<p class="text_align_left">'.$content['services_main_description'].'</p>
																			<div class="text-center"><button class="btn btn-success" style="background-color: #86E0D5; border-color: #86E0D5">Book Now</button></div>
																		</div>
																	</div>
																</div>';	
																// }
																 $i++;
															}
														}
													}

													echo $consultation_data;

												?>
			
								<?php
									 
								}
								
									
							}

							echo $consultation_data;

						?>
			
									</div> 
		</div>
		
   </div>
   
	<?php $this->load->view('segment_testimonials'); ?>
	
</section>


<!--.breadcrumb-wrapper-->

