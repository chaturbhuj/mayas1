<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>List of public content</h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home/</a></li>
            <li class="active">Page Listing</li>
        </ol>
		
		<div class="row">
			<?php foreach($public_page as $item){
				if($item['url_specification'] == 'city'){
					foreach($cities as $city){
						$city_url = '';
						
						$city_name_string = substr_replace($city['city'], "", -1);
						$city_name_for_url = strtolower($city_name_string);
						$city_name_for_url = preg_replace('/[^a-zA-Z ]/s', ' ', $city_name_for_url);
						$city_name_for_url = str_replace(' ', '-', $city_name_for_url);
		
						echo '<div class="col-sm-4"><a href="'.base_url('welcome/clogs/'.$city['id'].'/'.$item['page_url'].'-in-'.$city_name_for_url).'">'.$item['page_heading'].' in '. $city['city'] .'</a></div>';
					}
				}else{
					echo '<div class="col-sm-4"><a href="'.base_url('welcome/clog/'.$item['page_url']).'">'.$item['page_heading'].'</a></div>';
				}
			?>
			
			<?php } ?>
		</div>
		
   </div>
   
</section>


<!--.breadcrumb-wrapper-->

