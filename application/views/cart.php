<!DOCTYPE html>
<html lang="zxx">


<!-- Mirrored from live.hasthemes.com/html/9/kinle-preview/kinle/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Nov 2020 13:05:16 GMT -->
<style>
.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: -0.5;
}
.modal-backdrop {
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;
    background-color: #000;
}
	.your_cart{
	font-family: Montserrat;
	font-style: normal;
	font-weight: bold;
	font-size: 28px;
	line-height: 44px;
	}
	.name{
	font-family: Montserrat;
	font-style: normal;
	font-weight: 600;
	font-size: 16px;
	line-height: 44px;
	text-align:center;
	}
	.head1{
	font-family: Montserrat;
	font-style: normal;
	font-weight: 500;
	font-size: 12px;
	line-height: 15px;
	}
	.Details_box {
    background: #FFFFFF;
    box-shadow: 2px 2px 40px rgba(0, 0, 0, 0.2);
	}
	.price_head{
		text-align:right;
	}
	.price_head_right{
		padding-right:50px !important;
	}
	.quantity{
		padding-left: 210px!important;
	}
	.cart-table-button{
		    display: flex;
	}
	.m-t-20 {
    margin-top: 20px;
	}
	.btn--weight {
		font-weight: 500;
	}
	.btn--uppercase {
		text-transform: uppercase;
	}
	.btn--blue {
		background: #d9534f;
		color: #fff;
	}
	.btn--box {
		border-radius: 3px;
	}
	.btn {
    font-size: 13px;
    line-height: 1;
    outline: none;
}

.forms-control {
    
    padding: 6px 15px;
    font-size: 15px;
    color: #969595;
    border: 2px solid #f1f1f1;
    border-radius: 0;
    box-shadow: none;
}
</style>

<div style="background: #E5E5E5;">
    <!-- ::::::  Start  Main Container Section  ::::::  -->
    <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-content">
                      
                    </div>
                </div>               
            </div>           
        </div>			
		<div class="container cart_section">
			<div class="row">
				<div class="col-sm-12 col-12">
					<div class="Details_box" style="padding:50px; margin-bottom: 40px; margin-top:50px;">
						<div class="section-content">
							<h5 class="section-content__title  your_cart">Your cart items</h5>
						</div>				
						<div class="row" >
							<div class="table-content table-responsive  cart-table-content m-t-30">
								<table class="table table-hover">
									<thead class="gray-bg" >
										<tr>                          
											<th class="name price_head_right">Product Image</th>
											<th class="name price_head_right">Product Name</th>
											<th class="name quantity">Quantity</th>
											<th class="name price_head">Total Price</th>
										</tr>
									</thead>
									<tbody class="cart_body">
									</tbody>
								</table>
							</div>
						</div>
						
						<div class="cart-table-button row">
							<div class="cart-table-button--left col-md-6">
								<a href="<?php echo base_url(); ?>/premium-consultations/newproducts" class="btn btn--box btn--large btn--blue btn--uppercase btn--weight m-t-20" style="display:block; float: left;">countinue shoping</a>
							</div>
							<div class="cart-table-button--right col-md-6">
								
								<a href="<?php echo base_url(); ?>welcome/checkout" class="btn  checkout btn--box btn--large btn--blue btn--uppercase btn--weight m-t-20" style="display:block; float: right;">checkout</a>
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
		<div class="container" style="padding:30px;">
			<div class="row cart_empty_page_for display_none">
				<div class="col-sm-12 col-12">
					<div class="Details_box" style="margin-bottom: 50px; margin-top: 50px; padding:80px;">
						<div class="section-content" style="text-align:center;">
						
							<h2 class="section-content__title Congratulations">cart is empty  <i class="icon-shopping-cart" aria-hidden="true"></i></h2>
							
						</div>
						<form action="#" method="post" class="form-box ">
							<div class="row" >
								<div class="col-sm-12 col-12">
									<div class="form-box__single-group " style="margin-top:0px; text-align:center;">
									
										<h5 for="Your_shipment" style="font-weight: 300;font-size: 18px;line-height: 22px;color: #000000; padding: 10px; display: contents;">choose the product.</h5>
									</div>
								</div>
							</div>
							<div class="row" >
								<div class="col-sm-12" style="padding-bottom:40px; padding-top:40px; text-align:center;">
									<a  href="<?php echo base_url(); ?>"  type="submit" name="singlebutton" class="btn btn--box btn--small btn--blue  btn--weight Back_to_home_page " style="border-radius:4px;">Countinue shoping</a> 
								
								</div>	
							</div>	
												
						</form>
					</div>
				</div>
			</div>
		</div>
	</main> <!-- ::::::  End  Main Container Section  ::::::  -->

<button class="material-scrolltop" type="button"></button>
    

   
</div>


<!-- Mirrored from live.hasthemes.com/html/9/kinle-preview/kinle/cart.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Nov 2020 13:05:16 GMT -->
</html>
<script>

</script>