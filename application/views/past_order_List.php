<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<style>
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.table-responsive {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
}

.contentbox:empty:before {
    content: attr(data-placeholder);
	color: gray;
}

.contentbox{
	min-height: 45px;
	padding: 5px;	
	border: 1px solid #ced4da;
}
.msgbox{
		border:solid 1px #dedede;
		padding:5px;
		display:none;
		background-color:#f2f2f2;
		}
	
		#contentbox:focus {
		outline: none !important;
		border: 1px solid #ced4da!IMPORTANT;
		box-shadow: none !important;
		}
	
.display{
			display:none;
			border-left:solid 1px #dedede;
			border-right:solid 1px #dedede;
			border-bottom:solid 1px #dedede;
			overflow:hidden;
		}
		.display_box { 
			padding: 6px 4px;
			border-top: solid 1px #dedede;
			font-size: 14px;
			height: 38px;
		}
		
		.display_box:hover{
			background: #f3eeeea6;
			color:#FFFFFF;
		}
		.display_box a{
			color:#333;
		}
		.display_box a:hover{
			color: black;
			font-weight: 400;
		}
	
.wishlist_option{
  z-index: 2;
   
    border-radius: 2px;
    display: inline-block;
    font-size: 18px;
    height: 22px;
    line-height: 19px;
    position: absolute;
    right: 25px;
    text-align: center;
    top: 20px;
    width: 22px;
    cursor: pointer;
	color:#F7CC47;
	}
	#container1
		{
		 padding:10px;
		}
	.submit_comments{
			width: 32px;
			height: 32px;
			min-width: 35px;
			padding-left: 24px;
			padding-right: 36px;
			
			font-weight: 300;
			border-radius: 6px;
			border: solid 1px #9fa3ac;
			display: -moz-inline-box;
			display: -ms-inline-flexbox;
			display: -webkit-inline-flex;
			display: inline-flex;
			justify-content: center;
			align-items: center;
			-webkit-transition: 0.7s;
			-o-transition: 0.7s;
			transition: 0.7s;
		}
</style>
<!-- ::::::  Start  Main Container Section  ::::::  -->
   <!-- ::::::  Start  Main Container Section  ::::::  -->
<div class="dashboard-content py-4">
   <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
				<div  style="margin-left: 25px;" class="col-md-12 m-b-20">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb d-none">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">My Order List</li>
						</ol>
					</nav> 
				</div>   
            </div>
                <!-- Start  Wishlist -->
			<div class="col-12">
				<div class="row">
					<div  style="margin-top: 20px; margin-bottom:20px;" class="col-md-12">
						<div  style="margin-bottom:40px;" class="card card-body account-right">
                           <div class="widget">
                              <div class="section-header">
                                 <h5 class="heading-design-h5">
                                   Past Order List
                                 </h5>
                                 <input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id')?>">
								  <input type="hidden" id="complain_type" value="">	
							  </div>
                              <div class="order-list-tabel-main table-responsive">
                                 <table class="datatabel table table-striped table-bordered order-list-tabel">
                                    <thead>
										<tr role="row">
                                          <th>Date</th>
										   <th>Product Name</th>
                                          <th>Total</th>
                                          <th>Status</th>
                                          <th >Action</th>
										</tr>
                                    </thead>
                                    <tbody id="order_data_content">
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
					</div>
				</div>  
			</div>
		</div>
	</main>
</div>

<!----end-modal of complains--->
<div id="sub_comments" class="modal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			
				<div class="modal-header" style="padding: 8px;">
					<h5 class="modal-title">Register your Complaints </h5>
					<button style="padding: 0px 15px;font-size: 25px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				
				<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
				<input type="hidden" id="type" value="">	
				<input type="hidden" id="vendor_id" value="">		
				<input type="hidden" id="product_name" value="">					
				<input type="hidden" id="product_id" value="">
				<input type="hidden" id="order_id" value="">
				<input type="hidden" id="campaign_id" value="">
				<form  id="blog_form">
					<div class="modal-body">
						<div id="headerMsg"> </div>
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="email_title">Product<span class="required">*</span></label>
							<div class="col-lg-12">
								<select   class=" form-control chnage_email_title" placeholder="Title" id="email_title"  name="email_title">
									<option>Choose Product</option>
									<?php 
								 $result =  $this->db->get_where('product_order_detail',array('customer_id'=>$this->session->userdata('customer_id')))->result();
									var_dump($this->db->last_query());
									foreach($result as $rows){
										$product_id=$rows->product_id;
										$product_name=$rows->product_name;
										$order_id=$rows->order_id;
										$vendor_id=$rows->vendor_id;
										
										//var_dump($email_title);															
									?>
									<option class="" order_id ="<?php echo $order_id; ?>" vendor_id="<?php echo $vendor_id; ?>" product_name="<?php echo $product_name; ?>" value="<?php echo $product_id; ?>" id=""><?php echo $product_name; ?></option>
									<?php
										}
									?>
								</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="subject_title">Problem <span class="required">*</span></label>
							<div class="col-lg-12">	
								<select   class=" form-control chnage_email_title" placeholder="Not suiting you,damaged,not received" id="subject_title"  name="subject_title" value="">
									<option>Select Problem type</option>
									<option value="Not suiting you">Not suiting you</option>
									<option value="damaged">damaged</option>
									<option value="not received">not received</option>
									<option value="other">other</option>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="content">Message <span class="required">*</span></label>
							<div class="col-lg-12">
								<textarea rows="3" id="content"   name="mail_content" placeholder="History/Description" class="form-control ckeditor content"></textarea>
								<div class="content-error"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary float-right" data-dismiss="modal">Close</button>&nbsp;
						<button type="submit" class="btn btn-primary  float-right send_people_mail">send</button>&nbsp;				
					</div>
					<!--<div class="modal-footer" style="padding: 8px;border: none;">
						<button style="border: none;margin-right: 25px;" type="button" class="btn btn-info submit_comments" comment_id=""><i style="padding-right: 10px;" class="icon-Submit_Tick"></i>Post</button>
					</div>-->
				</form>	
				
			</div>
		</div>
	</div>
     <!----end-modal of complains--->
<script type="text/javascript">
    $('document').ready(function () {
            fatch_order_data();
            function fatch_order_data(){
               var customer_id=$('#customer_id').val();
                $.post(APP_URL+'api/customer_account/fatch_customer_order_list_data1',{
                  customer_id: customer_id,
                },function(response){
                    if (response.status == 200) {
                     var content='';
					  var type='';
                     var result=response.data;
					 console.log(response.data);
						$. each(result, function(index, value){
						  console.log(value);
                        content +='<tr>';
                            content +='<td style="width: 106px;">'+value.created_date+'</td>';                           
						    content +='<td>'+value.itemname+'</td>';
							content +='<td>₹'+value.grandtotal+'</td>';
							content +='<td>'+value.appointment_yes+'</td>';        
                            content +='<td><a data-toggle="tooltip" data-placement="top" title="" href="'+APP_URL+'product/Order_Detail_List?id='+value.order_id+'" data-original-title="View Detail" style="margin-right:7px;" class="btn fa fa-eye btn-info btn-sm"><i class="mdi mdi-eye"></i></a>';
						    content +='<a  aria-hidden="true" data-toggle="modal" data-target="#sub_comments" data-toggle="tooltip" data-placement="top"  style="color:#ffff; margin-top: 0px;" class="btn btn-info btn-sm"><i  class="mail_box_icon" product_id="'+value.product_id+'"  order_id="'+value.order_id+'"  product_name="'+value.product_name+'">Complaint</i></a>';
						    content +='&nbsp; <a href="'+APP_URL+'product/invoice?order_id='+value.order_id+'" data-toggle="tooltip" data-placement="top"  style="color:#ffff; margin-top: 0px;" class="btn btn-info btn-sm"><i  class="mail_box_icon" product_id="'+value.product_id+'"  order_id="'+value.order_id+'"   product_name="'+value.product_name+'">Invoice</i></a></td>';
							
							
						content +='</tr>';
						});
                    
						$('#order_data_content').html(content);
					}
				},'json');
			}

            $('body').on('click','.show_vendor_detail',function(){
               var name = $(this).attr('name');
               var phone_no = $(this).attr('phone_no');
               var customer_email = $(this).attr('customer_email');
               var city_name = $(this).attr('city_name');
               var state_name = $(this).attr('state_name');
               var country_name = $(this).attr('country_name');
               $('#show_vendor_name').text(name);
               $('#show_vendor_contact').text(phone_no);
               $('#show_vendor_email').text(customer_email);
               $('#show_vendor_city').text(city_name);
               $('#show_vendor_state').text(state_name);
               $('#show_vendor_country').text(country_name);
               $('#vendorDetailModal').modal('show');
            }); 
			
			$('body').on('click','.mail_box_icon',function() {
				console.log('mail_box_clicked');
				var vendor_id = $(this).attr('vendor_id');
				var product_id = $(this).attr('product_id');
				var order_id =$(this).attr('order_id');
				var product_name =$(this).attr('product_name');
				
				console.log(product_id);
				$('#product_id').val(product_id);
				$('#order_id').val(order_id);
				$('#vendor_id').val(vendor_id);
				$('#product_name').val(product_name);
			});
			
			$('body').on('click','.submit_comments',function() {
				console.log('post clicked');
				var is_error = 'no';
				var mythis = $(this);
				//console.log($(this).parent().parent().parent().parent().find('.comments_html').html());
				var comment = $(this).parent().parent().find('.contentbox').html();
				console.log(comment);
				if(!comment){
					$(this).parent().parent().find('.commentsub_err').removeClass('display_none');
					is_error = 'yes';
				}
				//$.blockUI();
				if(is_error == 'no'){
					console.log('addcomment');
					var product_id = $('#product_id').val();
					var product_name = $('#product_name').val();
					var vendor_id = $('#vendor_id').val();
					var user_id = $('#customer_id').val();
					var type =$('#complain_type').val();
					var order_id = $('#order_id').val();
					$.post(APP_URL + 'api/account/add_complain', {
						comment:comment,
						product_id:product_id,
						user_id:user_id,
						order_id:order_id,
						vendor_id:vendor_id,
						product_name:product_name,
					},
					function (response) {

						$('#err_add_plan').empty();
						if (response.status ==200) {
							console.log(response);
							var message = response.message;
							
							$('.be_first').empty();
							$('.contentbox').html('');
							$('#contentbox').empty('');
							$("html, body").animate({
								scrollTop: 0
							}, "slow");
							$('#headerMsg').empty();
							$('#headerMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong></div>");
								$("#headerMsg").fadeTo(2000, 500).slideUp(500, function() {
								$('#headerMsg').remove();
								$('#sub_comments').modal('hide');
								});
							

						}else {
							$('#headerMsg').empty();
							$('#headerMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
							$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
								$('#headerMsg').empty();
							});
						}					
						
					}, 'json');
				}
			});
		
		var start=/@/ig;
		var word=/@(\w+)/ig;
		$('body').on('keyup','.contentbox',function() {
			//console.log('comment_err');
			$(this).parent().find('.comment_err').addClass('display_none');
			$(this).parent().find('.commentsub_err').addClass('display_none');
		});
		
		$('#blog_form').validate({
		ignore: [],
        rules: {
           
			email_title: {
                required: true,
            },
			subject_title: {
                required: true,
            },
			content: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },	
		},
		 messages: {
		
			email_title: {
                required: "Blog title is required.",
            },
			subject_title: {
                required: "Reason title is required.",
            },
			content: {
                required: "History/Description is required.",
            },
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			//var subject_title = $('#subject_title').val();				
            var comment = CKEDITOR.instances['content'].getData();
			console.log(comment);
			var user_id=$('#customer_id').val();
				console.log(user_id);
			var product_id=$('#product_id').val();
			console.log(product_name);
			var product_name = $('#email_title option:selected').attr('product_name');
			var subject_title = $('#subject_title option:selected').val();
			var vendor_id = $('#email_title option:selected').attr('vendor_id');
			var order_id = $('#email_title option:selected').attr('order_id');
			console.log(product_name);
			console.log(vendor_id);
			//var vendor_id=$('#vendor_id').val();
		
		 
          $.post(APP_URL + 'api/account/add_complain', {
              
                product_id: product_id,
				subject_title :subject_title,
                comment: comment,
				user_id:user_id	,
				product_id:product_id,
				product_name:product_name,
				vendor_id:vendor_id,
				order_id:order_id,
				
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;</div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						$('#sub_comments').modal('hide');
						window.location.href = APP_URL+'customer/configure/complains_list';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		
		}
	});
	
		
			
    });

</script>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_upload',$data);
?>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_only_upload',$data);
?>
