	<div class="content">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="mb30 card">
                        <div class="card-body">
                            <h1 style="text-align:center;">Reset Password</h1>
                            
                            <form id="customer_reset_password_form" method="post">
								<div id="headMsg"></div>
								<input type="hidden" id="verify_code" value="<?php if(isset($_GET['v'])){ echo $_GET['v'];}?>">
								<input type="hidden" id="id" value="<?php if(isset($_GET['e'])){ echo $_GET['e'];}?>">
                                <div class="forgot-form">
                                    <div class="form-group">
                                        <label class="control-label" for="customer_password">New Password</label>
                                        <input id="customer_password" type="password" name="customer_password" placeholder="New Password" class="form-control" required>
                                    </div>
									<div class="form-group">
                                        <label class="control-label" for="customer_confirm_password">Conform Password</label>
                                        <input id="customer_confirm_password" type="password" name="customer_confirm_password" placeholder="Conform Password" class="form-control" required>
                                    </div>
                                    <button type="submit" name="singlebutton" class="btn btn-default btn-block">Get New Password</button>
                                </div>
                            </form>
							<script>
							
							$('document').ready(function () {
											
								 $('#customer_reset_password_form').validate({
									ignore: [],
									rules: {
										customer_password: {
											required: true,
										},
										customer_confirm_password: {
											required: true,
											equalTo: '#customer_password',
										},
									},
									messages: {
										customer_password: {
											required: "password is required"
										},
										customer_confirm_password: {
											required: "Confirm password is required",
											equalto : "Passwords does not match."
										},
									},
									submitHandler: function (form) {
										$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
											css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
											  overlayCSS:  { cursor:'wait'} 
											});
										var customer_password = $('#customer_password').val();
										var verify_code = $('#verify_code').val();
										var id = $('#id').val();
										
										$.post(APP_URL+'api/account/customer_rest_password',{
											id: id,
											customer_password: customer_password,
											verify_code: verify_code,
										},function(response){
											$.unblockUI();
											$("html, body").animate({scrollTop: 0}, "slow");
											$('#headMsg').empty();
											if (response.status == 200) {
												$('#vendor_email').val('');
												$('#vendor_password').val('');
												var message = response.message;
												$('#headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
												$("#headMsg").fadeTo(2000, 500).slideUp(500, function(){
													$('#headMsg').empty();
													window.location.href = APP_URL+'';
												});
											} else if (response.status == 201) {
												$('#headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
											}
										},'json');
										return  false;
									}
								});
							})
							</script>
                            <div class="mt30">
                                <a href="vendor-form.html" class="btn-primary-link mr-3">Login </a> <a href="vendor-form.html" class="btn-primary-link">Register </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>