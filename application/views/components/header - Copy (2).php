<!DOCTYPE html>
<html lang="en">
<head>

	<?php
		$title_value  = 'Free Astrology, Online Vedic Horoscope Reading | MayasAstrology';
		if(isset($title)){
			$title_value = $title;
		}
		$meta_description_value  = 'Consult Best Vedic Astrologer Online. Get Instant FREE Astrology Prediction. Horoscope readings about Career, Love, Marriage, Gemstone, Health | MayasAstrology';
		if(isset($meta_description)){
			$meta_description_value = $meta_description;
		}
		$meta_keyword_value  = 'Astrology, Horoscope, Astrology consultation, Free astrology, Instant Astrology, Online Astrologer, Vedic Astrology, Indian astrology,Talk to an astrologer online, Astrology Advice on Phone, Gemstones, Career, Success, Money,  Marriage, Love, Compatibility, Health, Children, Detailed Birth chart readings, Astrology 2018, Horary, Video Horoscopes, Astrology Remedies, Astrologer and spiritual guide from Jaipur';
		if(isset($meta_keyword)){
			$meta_keyword_value = $meta_keyword;
		}
		$meta_image_value  = base_url().'assets/images/favicon.png';
		if(isset($meta_image)){
			$meta_image_value = $meta_image;
		}
	?>
	<title><?php echo $title_value; ?></title>

	<meta name="description" content="<?php echo $meta_description_value; ?>">
	<meta name="keywords" content="<?php echo $meta_keyword_value; ?>">
	<meta property="og:title" content="<?php echo $title_value; ?>" />
	<meta property="og:description" content="<?php echo $meta_description_value; ?>" />
	<meta property="og:image" content="<?php echo $meta_image_value; ?>" />

	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Mayankesh Sharma">



	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/responsive.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/all.css">
	<link href="<?php echo base_url();?>assets/plugins/jquery-ui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css">
	<!-- <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet"> -->

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

	<script src="<?php echo base_url();?>public/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url();?>public/owl.carousel-2/assets/owl.carousel.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>public/owl.carousel-2/assets/owl.theme.default.min.css" />
	<script src="<?php echo base_url();?>public/owl.carousel-2/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/constant.js"></script>
	<script src="<?php echo base_url();?>assets/js/validate.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.blockUI.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js"></script>


	<style>
	a{color: #0C2494 !important;}
	.text_green{color: #0C2494 !important;letter-spacing: 2.1px; font-weight: 600; margin-bottom: 15px;}
	.slider-icon .slider-icon-inner {
		width: 31%;
		padding: 05px;
		margin-bottom: 15px;
		box-shadow: inset 7px 7px 9px #bcbcbc, inset -7px -7px 13px #ffffff;
		background: #f0f0f0;
		margin: 1%;
		text-align: center;
		border-radius: 10px;
	}
	.slider-icon-content{background: none; text-align: center; border-radius: 0 px ; box-shadow: none; padding: 0 px ;}
	@media (min-width: 1200px){
		.container, .container-lg, .container-md, .container-sm, .container-xl {
			max-width: 1200px;
		}
	}
	.premium_services{
		cursor: pointer;position: relative;
	}
	.premium_services .label-group {
		top: 25px;
		left: 26px;
	}
	.error{
		color: red;
	}
	.life_box_blue_light {
    background: transparent linear-gradient( 180deg , #9ce2e9a1 0%, #FFFFFF 100%) 0% 0% no-repeat padding-box;
    border: 1px solid #86E0D5;}
	</style>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112457339-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-112457339-1');
	</script>
</head>
<body class="unselectable">
<style>
.display_none{
	display:none;
}
.unselectable {
        -webkit-user-select: none;
        -webkit-touch-callout: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
</style>
<?php $is_vender='';
if($this->session->userdata('loggedIN') =='11') {
	$after_login='';
	$before_login='display_none';
	$after_vender_login='display_none';
}else{
	$after_login='display_none';
	$before_login='';
	$after_vender_login='display_none';
}
?>
<div id="main-wrapper">
<!-- Page Preloader -->
<div id="preloader">
    <div id="status">
        <div class="status-mes"></div>
    </div>
</div>

<div class="uc-mobile-menu-pusher">

<div class="content-wrapper">
    <header class="navbar navbar-expand-lg ">
		<div class="container">
			<a href="<?= base_url() ?>">
				<img src="<?php echo base_url();?>assets/img/logo.png" class="logo img-fluid" alt="logo" />
			</a>
			<div class="menu-desk">
			<ul class="navbar-nav ">
			      <li class="nav-item active">
			        <a  href="<?= base_url().'aboutus'?>">About us </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="<?= base_url().'free-consultations'?>">Free Consultations </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="<?= base_url().'premium-consultations'?>">Premium Consultations </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="<?= base_url().'vastu/vastu'?>">Vastu </a>
			      </li>
			      <!-- <li class="nav-item ">
			        <a  href="<?= base_url().'products'?>">Products </a>
			      </li> -->
			      <li class="nav-item ">
			        <a  href="<?= base_url().'career'?>">Career </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="<?= base_url().'contactus'?>">Contact us </a>
			      </li>

			    </ul> 
				<!--
			    <ul class="header-icon">
			      <li class="nav-item ">
					 <?php if($this->session->userdata('customer_id')){ ?>
			       		 <a  href="<?= base_url().'profile'; ?>" class="bar-icon" >My Account</a>
					<?php }else{ ?>
			        <a  href="#" class="bar-icon" data-target="#loginModal"  data-toggle="modal"><i class="fas fa-user"></i> </a>
					<?php } ?>
			      </li>
			      <li class="nav-item ">
			        <a  href="<?=base_url().'wishlist'?>" class="bar-icon"><i class="fas fa-heart"></i> </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="#" class="bar-icon"><i class="fas fa-shopping-cart"></i> </a>
			      </li>
			    </ul> -->
			</div>
			<div class="menu-mob">
				<ul class="header-icon"> 
				<!--
			      <li class="nav-item ">
			        <a  href="#" class="m-icon" data-target="#loginModal"  data-toggle="modal"><i class="fas fa-user"></i> </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="#" class="m-icon"><i class="fas fa-heart"></i> </a>
			      </li>
			      <li class="nav-item ">
			        <a  href="#" class="m-icon"><i class="fas fa-shopping-cart"></i> </a>
			      </li> -->
			      <li class="nav-item ">
			        <a  href="#" class="m-icon"><i class="fas fa-bars"></i></a>

			        <ul class="mobile-list">
					  <li class="nav-item active">
						<a  href="<?= base_url().'aboutus'?>">About us </a>
					  </li>
					  <li class="nav-item ">
						<a  href="<?= base_url().'free-consultations'?>">Free Consultations </a>
					  </li>
					  <li class="nav-item ">
						<a  href="<?= base_url().'premium-consultations'?>">Astrology </a>
					  </li>
					  <li class="nav-item ">
						<a  href="<?= base_url().'vastu/vastu'?>">Vastu </a>
					  </li>
					  <li class="nav-item ">
						<a  href="<?= base_url().'career'?>">Career </a>
					  </li>
					  <li class="nav-item ">
						<a  href="<?= base_url().'contactus'?>">Contact us </a>
					  </li>
			        </ul>
			      </li>
			    </ul>
			</div>


	</header>
		<!-- Modal -->
	<div class="modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class=" modal-title" id="exampleModalLabel" style="text-align:center;">Register</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="register_headMsg"></div>
				<div class="modal-body">
					<form id="addrees_forms">
						<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
						<input type="hidden" id="customer_address_id" value="<?php echo $this->session->userdata('customer_address_id');?>">
						<div class="row" style="padding:10px;">
							<div class="col-md-6"><input type="text" id="first_name" name="first_name" class="form-control " placeholder="Enter First Name"></div>
							<div class="col-md-6"><input type="text" id="last_name" name="last_name" class="form-control " placeholder="Enter Last Name"></div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="email" id="email_id" name="email_id"  class="form-control " placeholder="Enter email">
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="number" class="form-control "  id="phone_numbers" name="phone_numbers" placeholder="Enter Phone Number">
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control "  id="password" name="password" placeholder="Enter Password">
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control "  id="cinfirm_password" name="cinfirm_password" placeholder="confirm password">
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<textarea type="text" class="form-control " id="address_lines" name="address_lines" placeholder="Enter Address"></textarea>
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-3"><input type="text" class="form-control " id="country" name="country"  placeholder="Country Name"></div>
							<div class="col-md-3"><input type="text" class="form-control " id="state" name="state"  placeholder="State Name"></div>
							<div class="col-md-3"><input type="text" class="form-control " id="city" name="city"   placeholder="City Name"></div>
							<div class="col-md-3"><input type="number" class="form-control " id="zipcode" name="zipcode" placeholder="Zipcode"></div>
						</div>
						<div class="row" style="padding:10px;">

							<div class="col-md-12">
								<button class="btn btn-primary "  style="width: 100%; height:45px;">register</button>
							</div>

						</div>
					</form>
					<div class="mt30">
						Already have account <a href="javascript:void(0);" data-toggle="modal"  data-target="#registerModal" class="btn-primary-link mr-3 modelsubmit">Login </a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<h5 class=" modal-title" id="exampleModalLabel" style="text-align:center;">Login</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
				</button>
				</div>
				<div id="login_headMsg"></div>
				<div class="modal-body">
					<form id="customer_login">
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="email" id="login_customer_email" name="login_customer_email"  class="form-control" placeholder="Enter email">
							</div>
						</div>

						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control"  id="login_customer_password" name="login_customer_password" placeholder="Enter Password">
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<button class="btn btn-primary" style="width: 100%; height:45px;">Login</button>
							</div>
						</div>
					</form>
					<div>
						<a class="float-right modelsubmit" href="javascript:void(0);" data-target="#forgotModal">Forgot Password ?</a>
					</div>
					<div class="mt30">
						Are you new customer? Create a New Account. <a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" class="btn-primary-link mr-3 modelsubmit">Click here </a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Forgot Password</h5>

			  </div>
			  <div class="modal-body">
					<h4>Lost Password</h4>
					<p>Follow these simple steps to reset your account:</p>
					<ul class="list-unstyled mb30">
						<li>1. Enter your email address</li>
						<li>2. Wait for your recovery details to be sent.</li>
						<li>3. Follow as given instructions in your mail account.</li>
					</ul>
					<form id="vendor_forgot_form" method="post">
						<div id="forgot_headMsg"></div>
						<div class="forgot-form">
							<div class="form-group">
								<label class="control-label" for="vendor_email">Email</label>
								<input id="forget_customer_email" type="email" name="forget_customer_email" placeholder="Email" class="form-control" required>
							</div>
							<div class="row" style="padding:10px;">

								<div class="col-md-12">
									<button class="btn btn-primary" style="width: 100%; height:45px;">Get New Password</button>
								</div>
							</div>

						</div>
					</form>
					<script>

					$('document').ready(function () {

						 $('#vendor_forgot_form').validate({
							ignore: [],
							rules: {
								forget_customer_email: {
									required: true,
									remote:{
										url: APP_URL + "api/account/customer_login_email_check",
										type: "post",
										data: {
											customer_email: function(){ return $("#forget_customer_email").val(); }
										}
									}
								},
							},
							messages: {
								forget_customer_email: {
									required: "Email is required",
									remote: "Email doesn't exist! Please register first."
								},
							},
							submitHandler: function (form) {
								$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
									css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
									  overlayCSS:  { cursor:'wait'}
									});
								var customer_email = $('#forget_customer_email').val();

								$.post(APP_URL+'api/account/customer_send_email_forget_password',{
									customer_email: customer_email,
								},function(response){
									$.unblockUI();
									$("html, body").animate({scrollTop: 0}, "slow");
									$('#forgot_headMsg').empty();
									if (response.status == 200) {
										$('#forget_customer_email').val('');
										var message = response.message;
										$('#forgot_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
										//$("#forgot_headMsg").fadeTo(2000, 500).slideUp(500, function(){
											//$('#forgot_headMsg').empty();
											//window.location.href = APP_URL+'vendors/configure';
										//});
									} else if (response.status == 201) {
										$('#forgot_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
									}
								},'json');
								return  false;
							}
						});
					})
					</script>
					<div class="mt30">
						<a  style="text-align:center;"href="javascript:void(0);" data-target="#registerModal" class="btn-primary-link mr-3 modelsubmit">Login </a> <a href="javascript:void(0);" data-target="#loginModal" class="btn-primary-link modelsubmit">Register </a>
					</div>
			  </div>
			</div>
		  </div>
	</div>
	<style> .show-new p, hr{ margin:0px;}  .show-new span{ color:orange;} </style>
		<div >
			<marquee direction="left" height="38"   onmouseover="this.stop();" onmouseout="this.start();" scrollamount="3" loop="true"  style="padding-top: 5px;">
			<a rel="nofollow" class="marquee_text" href="javascript:void(0);" class="events' . $i . '">धन्यं यश्स्यमायुष्यं श्रीमद् व्यसनसूदनं । हर्षणं काम्यमोज्स्यं रत्नाभरणधारणं ।। ग्रहद्वष्टिहरं पुष्टिकरं दुःखप्रणाशनं ।  पापदौर्भाग्यशमनं रत्नाभरणधारणं ।।</a>
		</marquee>
		</div>

<script>
$('document').ready(function () {
	$('body').on('click', '.modelsubmit', function () {
		$('.modal').modal('hide');
		var id=$(this).attr('data-target');
		//console.log(id);
		setTimeout(function(){ $(id).modal('show'); }, 1000);

	});
})
</script>
<script>
	$('document').ready(function () {
		$('#addrees_forms').validate({
			ignore: [],
			rules: {
				first_name: {
					required: true,
				},
				last_name: {
					required: true,
				},
				email_id: {
					required: true,
					remote:{
						url: APP_URL + "api/account/customer_check_email_availability",
						type: "post",
						data: {
							customer_email: function(){ return $("#email_id").val(); }
						}
					}
				},
				address_lines: {
					required: true,
				},
				phone_numbers: {
					required: true,

				},
				country: {
					required: true,
				},
				state: {
					required: true,
				},
				city: {
					required: true,
				},
				zipcode: {
					required: true,
				},
				password: {
					required: true,
				},
				cinfirm_password: {
					required: true,
					equalTo: '#password',
				},
			},
			messages: {
				first_name: {
					required: "First name is required",
				},
				last_name: {
					required: "Last name is required",
				},
				email_id: {
					required: "email id is required",
					remote: "Email already exist"
				},
				address_lines: {
					required: "address is required"
				},
				phone_numbers: {
					required: "Phone number is required",
				},
				zipcode: {
					required: "zipcode is required",
				},
				country: {
					required: "country is required",
				},
				state: {
					required: "state is required",
				},
				city: {
					required: "city is required",
				},
				password: {
					required: "password is required",
				},
				cinfirm_password: {
					required: "password is required"
				},

			},
			submitHandler: function (form) {
				//$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
				//	css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				//	  overlayCSS:  { cursor:'wait'}
				//});
				var first_name = $('#first_name').val();
				var last_name = $('#last_name').val();
				var email_id = $('#email_id').val();
				var address_lines = $('#address_lines').val();
				var phone_numbers = $('#phone_numbers').val();
				var zipcode = $('#zipcode').val();
				var country = $('#country').val();
				var state = $('#state').val();
				var city = $('#city').val();
				var customer_address_id = $('#customer_address_id').val();
				var customer_id = $('#customer_id').val();
				var password = $('#password').val();


				$.post(APP_URL+'api/account/signup_for_customer_vendor',{
					first_name: first_name,
					last_name: last_name,
					email_id: email_id,
					address_lines: address_lines,
					phone_numbers: phone_numbers,
					zipcode: zipcode,
					country: country,
					state: state,
					city: city,
					password: password,
				},function(response){
						//console.log("response");
						//console.log(response);
					//console.log($result);
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#checkout_headMsg').empty();
					if (response.status == 200) {
						console.log("1");
						var message = response.message;
						$('#register_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
									$("#register_headMsg").fadeTo(2000, 500).slideUp(500, function(){
										$('#register_headMsg').empty();
										$('#first_name').val('');
										$('#last_name').val('');
										$('#email_id').val('');
										$('#address_lines').val('');
										$('#phone_numbers').val('');
										$('#zipcode').val('');
										$('#country').val('');
										$('#state').val('');
										$('#city').val('');
										$('#password').val('');
										$('#loginModal').modal('hide');
										$('#registerModal').modal('show');
										setTimeout(function(){ $('#registerModal').modal('show'); }, 1000);
										//window.location.href = APP_URL+'vendor/login';
									});
						//$('.delivery_address_box').addClass('display_none');
						//$('.payment_box').removeClass('display_none');
						//fatch_customer_address_list_data();

					}else if (response.status == 201) {
					}
				},'json');
				return  false;
			}
		});

		fatch_order_data();
		function fatch_order_data() {
          var customer_id = $('#customer_id').val();
          var currency_symbol = $('#currency_symbol').val();
          $.post(APP_URL + 'api/customer_account/fatch_customer_wish_count_data', {
             customer_id: customer_id,
          }, function(response) {
             if (response.status == 200) {
                var content = '';
                //var result = response.data;
				//console.log(response);
				$('.fatch_customer_wish_count_data').text(response.count);
             }
          }, 'json');
		}

		$('#customer_login').validate({
			ignore: [],
			rules: {
				login_customer_email: {
					required: true,
					remote:{
						url: APP_URL + "api/account/customer_login_email_check",
						type: "post",
						data: {
							customer_email: function(){ return $("#login_customer_email").val(); }
						}
					}
				},
				login_customer_password: {
					required: true,
				},
			},
			messages: {
				login_customer_email: {
					required: "Email is required",
					remote: "Email doesn't exist! Please register first."
				},
				login_customer_password: {
					required: "password is required"
				},
			},
			submitHandler: function (form) {
				$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
					css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
					  overlayCSS:  { cursor:'wait'}
					});
				var customer_email = $('#login_customer_email').val();
				var customer_password = $('#login_customer_password').val();

				$.post(APP_URL+'api/account/signin_for_customer',{
					customer_email: customer_email,
					customer_password: customer_password,
				},function(response){
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#login_headMsg').empty();
					if (response.status == 200) {
						$('#login_headMsg').val('');
						$('#login_headMsg').val('');
						var message = response.message;
						$('#login_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#login_headMsg").fadeTo(2000, 500).slideUp(500, function(){
							$('#login_headMsg').empty();
							$('#loginModal').modal('hide');
							window.location.reload();
						});
					} else if (response.status == 201) {
						$('#login_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				},'json');
				return  false;
			}
		});


	});
</script>
