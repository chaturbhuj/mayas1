
	<div class="footer">
		<img src="<?php echo base_url();?>public/images/footer.png" class="img-fluid footer-bottom" alt="image" />
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<ul>
					    
						<li><a href="<?= base_url().'privacypolicy';?>">Privacy Policy</a></li>
						<li><a href="<?= base_url().'refundcancelation';?>">Refunds/Cancellations</a></li>
						<li><a href="<?= base_url().'termsconditions';?>">Terms of Use</a></li>
						<li><a href="<?= base_url().'welcome/public_page_list';?>">Public Content List</a></li>
						<!--
						<li><a href="<?= base_url().'order_history';?>">Order History</a></li>
						<li><a href="<?= base_url().'free_consultation_list';?>">Free Consultations</a></li>
						<li><a href="<?= base_url().'logout';?>">Logout</a></li>-->
					</ul>
				</div>
				<div class="col-lg-4">
					<div class="copyright">
						<a href="<?= base_url();?>"><img src="<?php echo base_url();?>public/images/Path 65.png" class="img-fluid" alt="image">Mayas</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="consultmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg" >
	<span class="close" data-dismiss="modal" aria-hidden="true">&times;</span>
      <div class="modal-body mbg" >
	  
	  <div id="modal_consultmodel_ifrmae" style=" width:100%; height:100%;">
	  
      </div>
    </div>
  </div>
</div>
</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("p").css({"font-family":"Poppins-Regular"});
			$("span").css({"font-family":"Poppins-Regular"});
			$("div").css({"font-family":"Poppins-Regular"});
			$('body').on('click','.consult_btn',function () {
				var  code = $(this).attr('code');
				content='<iframe frameborder="0" height="100%" width="100%" src="https://www.youtube.com/embed/'+code+'"> </iframe>'; 
				$('#modal_consultmodel_ifrmae').append(content);
				$("#consultmodel").modal('show');
			});
			$('body').on('click','.pre_consult_btn',function () {
				var  code = $(this).attr('code');
				content='<iframe frameborder="0" height="100%" width="100%" src="https://www.youtube.com/embed/'+code+'"> </iframe>'; 
				$('#modal_consultmodel_ifrmae').append(content);
				$("#consultmodel").modal('show');
			});
			$('#consultmodel').on('hidden.bs.modal', function () { 
              location.reload();
                  });

			/*$(".bar-icon").click(function(){
					$(this).toggleClass("icon");
			    $(".navbar-nav ").toggle();
			  });*/
			  
			    //$(".navbar-nav").hide();
					 
	        	$(".m-icon").click(function(){
	        		$(this).toggleClass("icon");
			    $(".mobile-list").toggle();
			  });
			  
			    $(".mobile-list").hide();
    	 
			$(".down").click(function() {
				$('html, body').animate({
		         scrollTop: $(".up").offset().top
				}, 1500);
			});
			$('.room-carousel').owlCarousel({
				loop: true,
				margin: 30,
				nav: true,
				dots: false,
				autoWidth: false,
				navText: [
					'<i class="fa fa-angle-left"></i>',
					'<i class="fa fa-angle-right"></i>'
				],
				autoplay:true,
				autoplayTimeout:3000,
				autoplayHoverPause:true,
				responsive: {
					0:{
						items:1
					},
					480:{
						items:1
					},
					600:{
						items:2
					},
					1000:{
						items:3
					}
				}
			});

			$('body').on('click','.premium_services',function(){
				console.log($(this).attr('url'));
				window.location.href = $(this).attr('url');
			});
			$('body').on('click','.life_box_outer',function(){
				console.log($(this).attr('url'));
				window.location.href = $(this).attr('url');
			});
		});
document.addEventListener('contextmenu', event => event.preventDefault());

	</script>
	
</body>
<!-- Hotjar Tracking Code for https://mayasastrology.com 
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:3084140,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
-->
</html>