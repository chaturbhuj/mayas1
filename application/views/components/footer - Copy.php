<link href="<?php echo base_url(); ?>assets/plugins/jquery/jquery-ui.min.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-ui.min.js"></script>

<script>
		
    $(function () {
        $(".date").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
<style>
.display_none{
	display:none;
}
</style>
<footer class="footer">
    <div class="footer-widget-section">
        <div class="container">
			<ul class="service_cart_info" style="display:none"></ul>
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="footer-widget widget-about">
                        <h3 class="widget-title">About us</h3>

                        <p>Mayankesh is an award-winning astrologer and gemstone expert.He completed Jyotish Praveen and Jyotish Vishrad course in minimum possible duration with gold medal from Indian Council of Astrological Science. </p>

                        <div class="group-col">
                            <div class="contact-info">
                                <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i>+91 98292 65640</span>
                                <span class="email">info@mayasastrology.com</span>
                            </div>
                            <ul class="social-links">
                                <li><a rel="nofollow"  hreflang="en-us"  href="https://www.facebook.com/mayasastrology/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li><a rel="nofollow" hreflang="en-us"  href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                <li><a rel="nofollow"  hreflang="en-us" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="footer-widget">
                        <h3 class="widget-title">About Company</h3>
                        <ul class="footer-list-menu">
                            <li><a   hreflang="en-us" href="<?php echo base_url();?>">Home</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>aboutus">About us</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>termsconditions">Terms of Use</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>privacypolicy">Privacy Policy</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>refundcancelation">Refunds/Cancellations</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>blog">Blogs</a></li>
                       
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>contactus">Contact Us</a></li>
                            <li><a  hreflang="en-us"  href="<?php echo base_url();?>premium-consultations">Services</a></li>
                        </ul>

                    </div>
                    <!--footer-widget-->
                </div>
              
                <div class="col-sm-3 col-md-3">
                    <div class="footer-widget">
                        <h3 class="widget-title">Services</h3>
						<ul class="footer-list-menu">
						<?php   foreach ($services as $value) { ?>
								<li><a   hreflang="en-us"  href="<?php echo base_url();?>premium-consultation/detail/<?php echo $value['services_url']; ?>"><?php echo $value['services_name']; ?></a></li>
						<?php   }   ?>
							
						<?php   foreach ($services1 as $value1) { ?>
								<li><a   hreflang="en-us"  href="<?php echo base_url();?>premium-consultation/details/<?php echo $value1['services_url']; ?>"><?php echo $value1['services_name']; ?></a></li>
						<?php   } ?>
							
						</ul>
                     


                    </div>
                    <!--footer-widget-->
                </div>
                <div class="col-sm-2 col-md-2">
					<div class="footer-widget">
                        
					</div>
                </div>

            </div>
        </div>
    </div>
    <!--.footer-widget-section-->

    <div class="copyright-section">
        <div class="container clearfix">
            <span class="copytext">&copy; MAYAS. All rights reserved | Design By: <a rel="nofollow"  hreflang="en-us" 
                    href="https://lennoxsoft.com" style=" padding:12px;">Https://lennoxsoft.com/</a>
		    </span>
        </div>
        <!-- .container -->
    </div>
	
	<!--Start of Tawk.to Script-
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58aee7fb6b2ec15bd9efd2b8/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>->
<!--End of Tawk.to Script-->
    <!-- .copyright-section -->
</footer>
<!-- .footer -->

	<div style="" class="footer_cart"><ul class="product_cart_info" style="display:none;"></ul></div>
	
</div>
<!-- .content-wrapper -->
</div>
	<!-- .offcanvas-pusher -->
	<?php $is_vender='';
	if($this->session->userdata('loggedIN') =='11') { 
		$after_login='';
		$before_login='display_none';
		$after_vender_login='display_none';
	}else{
		$after_login='display_none';
		$before_login='';
		$after_vender_login='display_none';
	}			
	?>
	<div class="uc-mobile-menu uc-mobile-menu-effect display_none">
		<button type="button" class="close" aria-hidden="true" data-toggle="offcanvas"
				id="uc-mobile-menu-close-btn">&times;</button>
		<div>
			<div>
				<ul id="menu">
					
					<li class=" "><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
					<li class=" "><a rel="nofollow" href="<?php echo base_url();?>aboutus">About us</a></li>
					<li class=" "><a rel="nofollow" href="<?php echo base_url(); ?>services">Services</a></li>
					<li class=" "><a rel="nofollow" href="<?php echo base_url();?>blog">Blog</a></li>
				 
					<li class=" "><a rel="nofollow" href="<?php echo base_url();?>contactus">Contact us</a></li>
					<li class=" "><a rel="nofollow" href="<?php echo base_url();?>welcome/site_map">Site Map</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="semi_footer">
		<ul style="display:flex;">
			<?php if($before_login == ''){?>
				<li class=" "><a rel="nofollow" hreflang="en-us" data-toggle="modal" data-target="#registerModal"  href="javascript:void(0);"><i class="fa fa-user"></i>&nbsp;Login</a></li>
			<?php }else{ ?>
			<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			  My account
			</a>
			<div class="dropdown-menu" aria-labelledby="navbarDropdown" style="transform: translate3d(15px, 36px, 0px);top: 0px !important;will-change: transform;left: auto;">
				<a class="dropdown-item" href="<?php echo base_url(); ?>"><i class="fa fa-home" style="color:#000;"></i>&nbsp; Home</a>
			<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo base_url(); ?>welcome/profile"><i class="fa fa-user-circle" style="color:#000;"></i>&nbsp; Account</a>
			<div class="dropdown-divider"></div>
				 <a class="dropdown-item" href="<?php echo base_url(); ?>product/past_order_List"><i class="fa fa-box" style="color:#000;"></i>&nbsp; Orders</a>
			<div class="dropdown-divider"></div>
				
				<a class="dropdown-item" href="<?php echo base_url(); ?>publicpages/contact"><i class="fa fa-info" style="color:#000;"></i> &nbsp; Help/contact</a>
			<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="<?php echo base_url()?>welcome/logout"><i class="fa fa-power-off" style="color:#000;"></i> &nbsp; Logout</a>
			</div>
			</li>
			
			<?php } ?>
			<li class=" ">
				<div class="header-add-cart pos-relative m-l-40">
					<a href="<?php echo base_url(); ?>welcome/cart">
						<i class="fa fa-shopping-cart shopping_icon" style="margin-left:5px;"></i>
						<span class="wishlist-item-count cart_number pos-absolute"></span> 
					</a>
				</div>
			</li>
			<li class=" ">
				<div class="header-add-cart pos-relative m-l-40">
					<a href="<?php echo base_url(); ?>welcome/wishlist">
						<i class="fa fa-heart-o shopping_icon" style="margin-left:5px;"></i>
						<span class="wishlist-item-count fatch_customer_wish_count_data pos-absolute"></span> 
					</a>
				</div>					
			</li>
		</ul>
	</div>
<!-- .uc-mobile-menu -->

</div>
<!-- #main-wrapper -->


</body>
</html>





