<!DOCTYPE html>
<html lang="en">
<head>

	<?php 
		$title_value  = 'Free Astrology, Online Vedic Horoscope Reading | MayasAstrology';
		if(isset($title)){
			$title_value = $title;
		}
		$meta_description_value  = 'Consult Best Vedic Astrologer Online. Get Instant FREE Astrology Prediction. Horoscope readings about Career, Love, Marriage, Gemstone, Health | MayasAstrology';
		if(isset($meta_description)){
			$meta_description_value = $meta_description;
		}
		$meta_keyword_value  = 'Astrology, Horoscope, Astrology consultation, Free astrology, Instant Astrology, Online Astrologer, Vedic Astrology, Indian astrology,Talk to an astrologer online, Astrology Advice on Phone, Gemstones, Career, Success, Money,  Marriage, Love, Compatibility, Health, Children, Detailed Birth chart readings, Astrology 2018, Horary, Video Horoscopes, Astrology Remedies, Astrologer and spiritual guide from Jaipur';
		if(isset($meta_keyword)){
			$meta_keyword_value = $meta_keyword;
		}
		$meta_image_value  = base_url().'assets/images/favicon.png';
		if(isset($meta_image)){
			$meta_image_value = $meta_image;
		}
	?>
	<title><?php echo $title_value; ?></title>
		
	<meta name="description" content="<?php echo $meta_description_value; ?>">
	<meta name="keywords" content="<?php echo $meta_keyword_value; ?>">
	<meta property="og:title" content="<?php echo $title_value; ?>" />
	<meta property="og:description" content="<?php echo $meta_description_value; ?>" />
	<meta property="og:image" content="<?php echo $meta_image_value; ?>" />
	
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Mayankesh Sharma">
	
  

	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/mobile-menu.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	 
    <link href="<?php echo base_url();?>assets/css/owl.carousel.css" rel="stylesheet">
    <!-- Style CSS -->
	 <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet">
    <!-- Style CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/plugins/jquery-ui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/style.css">
	

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	

	<!-- Jquery -->
	<script src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
	<!--Bootstrap-->
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<!--Smoothscroll-->
	<script src="<?php echo base_url();?>assets/js/smoothscroll.js"></script>
	<!--mobile menu-->
	<script src="<?php echo base_url();?>assets/js/mobile-menu.js"></script>
	<!--Theme Script-->
	<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
	<!--Theme Script-->
	<script src="<?php echo base_url();?>assets/js/scripts.js"></script>
	<script src="<?php echo base_url();?>assets/js/validate.js"></script>
	<script src="<?php echo base_url();?>assets/js/constant.js"></script>
	<script src="<?php echo base_url();?>assets/plugins/jquery-ui.min.js"></script>
	
	<script src="<?php echo base_url(); ?>assets/js/jquery.toaster.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/shopping.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.blockUI.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js"></script>
	
	<style>
	.ui-datepicker-month{background-color: #e78f08;}
	.ui-datepicker-year{background-color: #e78f08;}
	.dropdown-menu.show {
		display: block;
	}
	.dropdown-menu{
	position: absolute;
    top: 100%;
    left: 14px;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 10rem;
    padding: .5rem 0;
    margin: .125rem 0 0;
    font-size: 1rem;
    color: #212529;
    text-align: left;
    list-style: none;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: .25rem;
	}
	.dropdown-item {
    display: block;
    width: 100%;
    padding: 1.25rem 3.5rem;
    clear: both;
    font-weight: 700;
    color: #000;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
	font-size:18px;
}
.dropdown-divider {
    height: 0;
    margin: .5rem 0;
    overflow: hidden;
    border-top: 1px solid #e9ecef;
}
.nav-link {
    display: block;
    padding: .5rem 1rem;
}
.dropdown-toggle {
    white-space: nowrap;
}
.header-right-icon ul li a {
    color: #111;
}
.header-right-icon ul li {
    display: inline-block;
    font-size: 14px;
    color: #111;
    padding: 0 15px;
}
.dropdown, .dropleft, .dropright, .dropup {
    position: relative;
}
	</style>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112457339-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-112457339-1');
	</script>
</head>
<body>
<style>
.display_none{
	display:none;
}
</style>
<?php $is_vender='';
if($this->session->userdata('loggedIN') =='11') { 
	$after_login='';
	$before_login='display_none';
	$after_vender_login='display_none';
}else{
	$after_login='display_none';
	$before_login='';
	$after_vender_login='display_none';
}			
?>
<div id="main-wrapper">
<!-- Page Preloader -->
<div id="preloader">
    <div id="status">
        <div class="status-mes"></div>
    </div>
</div>
<style>
.float-right{
	float:right;
}	
.input_buttons{
	background: #FFFFFF 0% 0% no-repeat padding-box;
	box-shadow: 0px 4px 6px #00000029;
	border: 1px solid #EFEFEF;
	border-radius: 10px;
	opacity: 1;
}
.submit_next_button{
	background: #0857DE 0% 0% no-repeat padding-box;
	border: 1px solid #0857DE;
	border-radius: 10px;
	opacity: 1;
}
label.error{
	
	color:red;
	
}
.pos-absolute{
	position: relative;
}
.pos-relative {
    position: relative;
}
.m-l-40 {
    margin-left: 40px;
}
.wishlist-item-count {
    top: 2px;
    left: -15px;
    background: #D69512;
    color: #fff;
    border-radius: 50%;
    width: 20px;
    height: 20px;
    line-height: 22px;
    text-align: center;
    font-size: 10px;
}
.pos-absolute {
    position: absolute;
}
.shopping_icon{
	margin-left: 5px;
    font-size: 20px;
    padding-top: 8px;
	color: #434345;
}
</style>
<div class="uc-mobile-menu-pusher">

<div class="content-wrapper">
	<nav class="navbar m-menu navbar-default" style="padding: 0px 0;">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a rel="nofollow" hreflang="en-us" class="navbar-brand" href="<?php echo base_url();?>"><img style="width: 150px;" src="<?php echo base_url();?>assets/img/logo.png" alt="Mayas Astrology"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="#navbar-collapse-1">

            <div class="navbar-right search-btn">
                <div class="modal fade search-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i class="fa fa-times" aria-hidden="true"></i></button>
                            <div class="container">
                                <form role="form">
                                    <!-- Input Group -->
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Type Something">
										<span class="input-group-btn">
										  <button type="submit" class="btn btn-primary">Search</button>
										</span>
                                    </div>
									
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ul class="nav navbar-nav navbar-right main-nav">			
                <li class=" "><a rel="nofollow" hreflang="en-us" href="<?php echo base_url();?>">Home</a></li>
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url();?>aboutus">About us</a></li>
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url(); ?>premium-consultations">Services</a></li>
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url(); ?>search/product">Products</a></li>
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url(); ?>blog">Blog</a></li>
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url(); ?>career">Career</a></li>            
                <li class=" "><a rel="nofollow" hreflang="en-us"  href="<?php echo base_url();?>contactus">Contact us</a></li>
            </ul>			
        </div>
    </div>
		<!-- Modal -->
	<div class="modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
					<h5 class=" modal-title" id="exampleModalLabel" style="text-align:center;">Register</h5>
					
				</div>
				<div id="register_headMsg"></div>
				<div class="modal-body">
					<form id="addrees_forms">
						<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id');?>">
						<input type="hidden" id="customer_address_id" value="<?php echo $this->session->userdata('customer_address_id');?>">
						<div class="row" style="padding:10px;">
							<div class="col-md-6"><input type="text" id="first_name" name="first_name" class="form-control " placeholder="Enter First Name"></div>
							<div class="col-md-6"><input type="text" id="last_name" name="last_name" class="form-control " placeholder="Enter Last Name"></div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="email" id="email_id" name="email_id"  class="form-control " placeholder="Enter email"> 
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="number" class="form-control "  id="phone_numbers" name="phone_numbers" placeholder="Enter Phone Number"> 
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control "  id="password" name="password" placeholder="Enter Password"> 
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control "  id="cinfirm_password" name="cinfirm_password" placeholder="confirm password"> 
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<textarea type="text" class="form-control " id="address_lines" name="address_lines" placeholder="Enter Address"></textarea> 
							</div>
						</div>
						<div class="row" style="padding:10px;">
							<div class="col-md-3"><input type="text" class="form-control " id="country" name="country"  placeholder="Country Name"></div>
							<div class="col-md-3"><input type="text" class="form-control " id="state" name="state"  placeholder="State Name"></div>
							<div class="col-md-3"><input type="text" class="form-control " id="city" name="city"   placeholder="City Name"></div>
							<div class="col-md-3"><input type="number" class="form-control " id="zipcode" name="zipcode" placeholder="Zipcode"></div>
						</div>
						<div class="row" style="padding:10px;">
							
							<div class="col-md-12">
								<button class="btn btn-primary "  style="width: 100%; height:45px;">register</button>
							</div>
							
						</div>
					</form>	
					<div class="mt30">
						Already have account <a href="javascript:void(0);" data-toggle="modal"  data-target="#registerModal" class="btn-primary-link mr-3 modelsubmit">Login </a> 	
					</div>	
				</div>	
			</div>			
		</div>
	</div>
</div>
	<div class="modal" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
					</button>
					<h5 class=" modal-title" id="exampleModalLabel" style="text-align:center;">Login</h5>
					
				</div>
				<div id="login_headMsg"></div>
				<div class="modal-body">
					<form id="customer_login">
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="email" id="login_customer_email" name="login_customer_email"  class="form-control" placeholder="Enter email"> 
							</div>
						</div>
						
						<div class="row" style="padding:10px;">
							<div class="col-md-12">
								<input type="text" class="form-control"  id="login_customer_password" name="login_customer_password" placeholder="Enter Password"> 
							</div>
						</div>
						
						
						
						<div class="row" style="padding:10px;">
							
							<div class="col-md-12">
								<button class="btn btn-primary" style="width: 100%; height:45px;">Login</button>
							</div>
						</div>
					</form>	
					<div>
						<a class="float-right modelsubmit" href="javascript:void(0);" data-target="#forgotModal">Forgot Password ?</a>
					</div>
					<div class="mt30">
						Are you new customer? Create a New Account. <a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" class="btn-primary-link mr-3 modelsubmit">Click here </a> 
					</div>			
				</div>
			</div>
		</div>		
	</div>
	<div class="modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title" id="exampleModalLabel" style="text-align:center;">Forgot Password</h5>
				
			  </div>
			  <div class="modal-body">
					<h4>Lost Password</h4>
					<p>Follow these simple steps to reset your account:</p>
					<ul class="list-unstyled mb30">
						<li>1. Enter your email address</li>
						<li>2. Wait for your recovery details to be sent.</li>
						<li>3. Follow as given instructions in your mail account.</li>
					</ul>
					<form id="vendor_forgot_form" method="post">
						<div id="forgot_headMsg"></div>
						<div class="forgot-form">
							<div class="form-group">
								<label class="control-label" for="vendor_email">Email</label>
								<input id="forget_customer_email" type="email" name="forget_customer_email" placeholder="Email" class="form-control" required>
							</div>
							<div class="row" style="padding:10px;">
							
								<div class="col-md-12">
									<button class="btn btn-primary" style="width: 100%; height:45px;">Get New Password</button>
								</div>
							</div>
						
						</div>
					</form>
					<script>
					
					$('document').ready(function () {
									
						 $('#vendor_forgot_form').validate({
							ignore: [],
							rules: {
								forget_customer_email: {
									required: true,
									remote:{
										url: APP_URL + "api/account/customer_login_email_check",
										type: "post",
										data: {
											customer_email: function(){ return $("#forget_customer_email").val(); }
										}
									}
								},
							},
							messages: {
								forget_customer_email: {
									required: "Email is required",
									remote: "Email doesn't exist! Please register first."
								},
							},
							submitHandler: function (form) {
								$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
									css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
									  overlayCSS:  { cursor:'wait'} 
									});
								var customer_email = $('#forget_customer_email').val();
								
								$.post(APP_URL+'api/account/customer_send_email_forget_password',{
									customer_email: customer_email,
								},function(response){
									$.unblockUI();
									$("html, body").animate({scrollTop: 0}, "slow");
									$('#forgot_headMsg').empty();
									if (response.status == 200) {
										$('#forget_customer_email').val('');
										var message = response.message;
										$('#forgot_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
										//$("#forgot_headMsg").fadeTo(2000, 500).slideUp(500, function(){
											//$('#forgot_headMsg').empty();
											//window.location.href = APP_URL+'vendors/configure';
										//});
									} else if (response.status == 201) {
										$('#forgot_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
									}
								},'json');
								return  false;
							}
						});
					})
					</script>
					<div class="mt30">
						<a  style="text-align:center;"href="javascript:void(0);" data-target="#registerModal" class="btn-primary-link mr-3 modelsubmit">Login </a> <a href="javascript:void(0);" data-target="#loginModal" class="btn-primary-link modelsubmit">Register </a>
					</div>
			  </div>
			</div>
		  </div>
	</div>
	<style> .show-new p, hr{ margin:0px;}  .show-new span{ color:orange;} </style>
		<div style="margin-left: 166px;">
			<marquee direction="left" height="38"   onmouseover="this.stop();" onmouseout="this.start();" scrollamount="3" loop="true"  style="padding-top: 5px;">					
			<a rel="nofollow" class="marquee_text" href="javascript:void(0);" class="events' . $i . '" style="color: #D69512;font-size: 22px;text-decoration: none;">धन्यं यश्स्यमायुष्यं श्रीमद् व्यसनसूदनं । हर्षणं काम्यमोज्स्यं रत्नाभरणधारणं ।। ग्रहद्वष्टिहरं पुष्टिकरं दुःखप्रणाशनं ।  पापदौर्भाग्यशमनं रत्नाभरणधारणं ।।</a>
		</marquee>
		</div>
    <!-- .container -->
</nav>
<script>
$('document').ready(function () {
	$('body').on('click', '.modelsubmit', function () {
		$('.modal').modal('hide');
		var id=$(this).attr('data-target');
		//console.log(id);
		setTimeout(function(){ $(id).modal('show'); }, 1000);
		
	});
})
</script>
<script>
	$('document').ready(function () {
		$('#addrees_forms').validate({
			ignore: [],
			rules: {
				first_name: {
					required: true,
				},
				last_name: {
					required: true,
				},
				email_id: {
					required: true,
					remote:{
						url: APP_URL + "api/account/customer_check_email_availability",
						type: "post",
						data: {
							customer_email: function(){ return $("#email_id").val(); }
						}
					}
				},
				address_lines: {
					required: true,
				},
				phone_numbers: {
					required: true,
					
				},			
				country: {
					required: true,
				},
				state: {
					required: true,
				},
				city: {
					required: true,
				},
				zipcode: {
					required: true,
				},
				password: {
					required: true,
				},
				cinfirm_password: {
					required: true,
					equalTo: '#password',
				},
			},
			messages: {
				first_name: {
					required: "First name is required",
				},
				last_name: {
					required: "Last name is required",
				},
				email_id: {
					required: "email id is required",
					remote: "Email already exist"
				},
				address_lines: {
					required: "address is required"
				},
				phone_numbers: {
					required: "Phone number is required",
				},
				zipcode: {
					required: "zipcode is required",
				},
				country: {
					required: "country is required",
				},
				state: {
					required: "state is required",
				},
				city: {
					required: "city is required",
				},
				password: {
					required: "password is required",
				},
				cinfirm_password: {
					required: "password is required"
				},
				
			},
			submitHandler: function (form) {
				//$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
				//	css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				//	  overlayCSS:  { cursor:'wait'} 
				//});
				var first_name = $('#first_name').val();
				var last_name = $('#last_name').val();
				var email_id = $('#email_id').val();
				var address_lines = $('#address_lines').val();
				var phone_numbers = $('#phone_numbers').val();
				var zipcode = $('#zipcode').val();
				var country = $('#country').val();
				var state = $('#state').val();
				var city = $('#city').val();
				var customer_address_id = $('#customer_address_id').val();
				var customer_id = $('#customer_id').val();
				var password = $('#password').val();
				
				
				$.post(APP_URL+'api/account/signup_for_customer_vendor',{
					first_name: first_name,
					last_name: last_name,
					email_id: email_id,
					address_lines: address_lines,
					phone_numbers: phone_numbers,
					zipcode: zipcode,
					country: country,
					state: state,
					city: city,
					password: password,
				},function(response){
						//console.log("response");
						//console.log(response);
					//console.log($result);
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#checkout_headMsg').empty();
					if (response.status == 200) {
						console.log("1");
						var message = response.message;
						$('#register_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
									$("#register_headMsg").fadeTo(2000, 500).slideUp(500, function(){
										$('#register_headMsg').empty();
										$('#first_name').val('');
										$('#last_name').val('');
										$('#email_id').val('');
										$('#address_lines').val('');
										$('#phone_numbers').val('');
										$('#zipcode').val('');
										$('#country').val('');
										$('#state').val('');
										$('#city').val('');
										$('#password').val('');
										$('#loginModal').modal('hide');
										$('#registerModal').modal('show');
										setTimeout(function(){ $('#registerModal').modal('show'); }, 1000);
										//window.location.href = APP_URL+'vendor/login';
									});
						//$('.delivery_address_box').addClass('display_none');
						//$('.payment_box').removeClass('display_none');
						//fatch_customer_address_list_data();
					
					}else if (response.status == 201) {
					}
				},'json');
				return  false;
			}
		});
		
		fatch_order_data();     
		function fatch_order_data() {
          var customer_id = $('#customer_id').val();
          var currency_symbol = $('#currency_symbol').val();
          $.post(APP_URL + 'api/customer_account/fatch_customer_wish_count_data', {
             customer_id: customer_id,
          }, function(response) {
             if (response.status == 200) {
                var content = '';
                //var result = response.data;
				//console.log(response);
				$('.fatch_customer_wish_count_data').text(response.count);
             }
          }, 'json');
		}
		
		$('#customer_login').validate({
			ignore: [],
			rules: {
				login_customer_email: {
					required: true,
					remote:{
						url: APP_URL + "api/account/customer_login_email_check",
						type: "post",
						data: {
							customer_email: function(){ return $("#login_customer_email").val(); }
						}
					}
				},
				login_customer_password: {
					required: true,
				},
			},
			messages: {
				login_customer_email: {
					required: "Email is required",
					remote: "Email doesn't exist! Please register first."
				},
				login_customer_password: {
					required: "password is required"
				},
			},
			submitHandler: function (form) {
				$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
					css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
					  overlayCSS:  { cursor:'wait'} 
					});
				var customer_email = $('#login_customer_email').val();
				var customer_password = $('#login_customer_password').val();
				
				$.post(APP_URL+'api/account/signin_for_customer',{
					customer_email: customer_email,
					customer_password: customer_password,
				},function(response){
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#login_headMsg').empty();
					if (response.status == 200) {
						$('#login_headMsg').val('');
						$('#login_headMsg').val('');
						var message = response.message;
						$('#login_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#login_headMsg").fadeTo(2000, 500).slideUp(500, function(){
							$('#login_headMsg').empty();
							$('#loginModal').modal('hide');
							window.location.reload();
						});
					} else if (response.status == 201) {
						$('#login_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				},'json');
				return  false;
			}
		});
		
		
	});
</script>		