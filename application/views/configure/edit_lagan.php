<script src="<?php echo base_url(); ?>assets/js/lagan.js"></script> 
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_lagan"></div>
                    <div id="edit_team_table">
                        <h2>List of All Lagan Sign</h2>
                        <div id="table_view">             
                            <?php
                            if ($lagans == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Lagan Name</th><th class="text-center">Life Birth Stone Name </th><th class="text-center">Punya Lucky Stone</th><th class="text-center">Bhagya Fortune Stone Name</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($lagans as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['lagan_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['life_stone_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['punya_stone_name'] . '</td>';
									$content .= '<td class="text-center">' . $value['bhagya_stone_name'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="edit_lagan" data-toggle="modal" data-target="#my_user_edit" name=' . $value['lagan_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_lagan"  name=' . $value['lagan_id'] . ' value=""><span class="label label-danger">Remove lagan</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_lagan_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit lagan</h4>
                    <input type="hidden" name="edit_lagan_id" id="edit_lagan_id" value=""/>     
                </div>
                <div class="modal-body">
                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                            <label class="control-label col-md-3" for="edit_lagan_name">Lagan Name<span class="required">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control" id="edit_lagan_name" name="edit_lagan_name"> 
										<option value="0">Select Lagan Name</option>
										<option value="Aries">Aries</option>
										<option value="Taurus">Taurus</option>
										<option value="Gemini">Gemini</option>
										<option value="Cancer">Cancer</option>
										<option value="Leo">Leo</option>
										<option value="Virgo">Virgo</option>
										<option value="Libra">Libra</option>
										<option value="Scorpio">Scorpio</option>
										<option value="Sagittarius">Sagittarius</option>
										<option value="Capricorn">Capricorn</option>
										<option value="Aquarius">Aquarius</option>
										<option value="Pisces">Pisces</option>
								</select>
                            </div>
                        </div>	 
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_life_stone_name">Life Birth Stone Name<span class="required">*</span></label>
                        <div class="col-md-9">
                           <input class="form-control" id="edit_life_stone_name" name="edit_life_stone_name" placeholder="Life Birth Stone Name" type="text">
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_punya_stone_name">Punya Lucky Stone Name<span class="required">*</span></label>
                        <div class="col-md-9">
							<textarea class="form-control" id="edit_punya_stone_name" name="edit_punya_stone_name" placeholder="Punya Lucky Stone Name" row="2"></textarea>
                        </div> 
                    </div>	
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_bhagya_stone_name">Bhagya Fortune Stone Name<span class="required">*</span></label>
                        <div class="col-md-9">
							<textarea class="form-control" id="edit_bhagya_stone_name" name="edit_bhagya_stone_name" placeholder="Bhagya Fortune Stone Name" row="2"></textarea>
						</div>
                    </div>	
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

