

<style>.product_images{  width: 100px;}</style>
<script src="https://cdn.ckeditor.com/4.8.0/full/ckeditor.js"> </script>

<?php 
	
	if($pro_data){
		
		$product_id = $pro_data[0]['product_id'];
		$product_name = $pro_data[0]['product_name'];
		$product_actual_prize = $pro_data[0]['product_actual_prize'];
		$product_discounted_prize = $pro_data[0]['product_discounted_prize'];
		$product_category = $pro_data[0]['product_category'];
		$product_rashi = json_decode($pro_data[0]['product_rashi']);
		$product_disease = json_decode($pro_data[0]['product_disease']);
		$product_free_consultation = json_decode($pro_data[0]['product_free_consultation']);
		$product_paid_consultation = json_decode($pro_data[0]['product_paid_consultation']);
		$product_short_description = $pro_data[0]['product_short_description'];
		$product_long_description = $pro_data[0]['product_long_description'];
		$status = $pro_data[0]['status'];
		$is_new = $pro_data[0]['is_new'];
		$product_meta_title = $pro_data[0]['product_meta_title'];
		$product_meta_description = $pro_data[0]['product_meta_description'];
		$product_meta_keyword = $pro_data[0]['product_meta_keyword'];
		$product_meta_tags = $pro_data[0]['product_meta_tags'];
		$product_feature = $pro_data[0]['product_feature'];
		$product_height = $pro_data[0]['product_height'];
		$product_shape = $pro_data[0]['product_shape'];
		$product_color = $pro_data[0]['product_color'];
		$product_clarity = $pro_data[0]['product_clarity'];
		$product_grade = $pro_data[0]['product_grade'];
		$product_weight = $pro_data[0]['product_weight'];
		$product_treatment = $pro_data[0]['product_treatment'];
		$product_origin = $pro_data[0]['product_origin'];
		$string_url = $pro_data[0]['string_url'];
		$images = $pro_data[0]['product_images'];		
		$product_images=explode(',',$images,-1);
			
	}else{
		$product_id =0;
		$product_name = "";
		$product_actual_prize = "";
		$product_discounted_prize = "";
		$product_category = "";
		$product_rashi = "";
		$product_disease = "";
		$product_free_consultation = "";
		$product_paid_consultation = "";
		$product_short_description = "";
		$product_long_description = "";
		$status = "";
		$is_new = "";
		$product_meta_title = "";
		$product_meta_description = "";
		$product_meta_keyword = "";
		$product_meta_tags = "";
		$product_feature = "";
		$product_height = "";
		$product_shape = "";
		$product_color = "";
		$product_clarity = "";
		$product_grade = "";
		$product_weight = "";
		$product_treatment = "";
		$product_origin = "";
		$string_url = "";
		$product_images = "";
	}
	
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Product</h1>
		<a href="<?php echo base_url();?>configure_access/products_list" class="btn btn-default pull-right addAds">View All Products</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="errHeadMsg"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_product_form" method="post" class="form-horizontal">
						<input type="hidden" id="product_id" name="product_id" value="<?php echo $product_id;?> ">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Name <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product Name" value="<?php echo $product_name;?> ">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">String Url <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="string_url" name="string_url" placeholder="string url" value="<?php echo $string_url;?> ">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Actual Prize <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="product_actual_prize" name="product_actual_prize" placeholder="Actual Prize" value="<?php echo $product_actual_prize;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Discounted Prize <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="product_discounted_prize" name="product_discounted_prize" placeholder="Discounted Prize" value="<?php echo $product_discounted_prize;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Product Category <span class="required">*</span></label>
                            <div class="col-md-5">
                                <select style="width: 100%;" class="form-control" name="product_category" id="product_category">
									<option value=" ">Select Category</option>
									<?php $i=1;
									if($cat_data !=''){
										foreach($cat_data as $value){  ?>
											<option name="<?php echo $value['cat_id'];?>" value="<?php echo $value['cat_id'];?>" <?php if($product_category == $value['cat_name']){ echo 'selected'; }?>> <?php echo $value['cat_name'];?> </option>
										<?php	$i++;
										}
									} ?>
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Rashi <span class="required">*</span></label>
                            <div class="col-md-5">
								<?php $i=1;
								if($rashi_data !=''){
									foreach($rashi_data as $value){
										
										$ischecked='';
										if($product_rashi !=''){
											foreach($product_rashi as $val){ if($val == $value['rashi_id']){ $ischecked = 'checked'; } 
											} 
										}?>
										<label class="checkbox-inline">
											<input type="checkbox" name="product_rashi" class="product_rashi"  value="<?php echo $value['rashi_id'];?>" <?php echo $ischecked; ?>> <span style="margin-right: 30px;"> <?php echo $value['rashi_name'];?> </span>
										</label>
										<?php	$i++;
									}
								}?>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Product Disease <span class="required">*</span></label>
                            <div class="col-md-5">
								<?php $i=1;
								if($diease_data !=''){
									foreach($diease_data as $value){
										
										$ischecked='';
										if($product_disease !=''){
											foreach($product_disease as $val){ if($val == $value['diease_id']){ $ischecked = 'checked'; } 
											} 
										}?>
										<label class="checkbox-inline">
										<input type="checkbox" name="product_disease" class="product_disease"  value="<?php echo $value['diease_id'];?>" <?php echo $ischecked; ?>> <span style="margin-right: 30px;"> <?php echo $value['diease_name'];?> </span>
										</label>
										<?php	$i++;
									}
								} ?>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Free Consultation <span class="required">*</span></label>
                            <div class="col-md-5">
                                <?php $i=1;
								if($freeconsult_data !=''){
									foreach($freeconsult_data as $value){  
										
										$ischecked='';
										if($product_free_consultation !=''){
											foreach($product_free_consultation as $val){ if($val == $value['consultation_id']){ $ischecked = 'checked'; } 
											} 
										}?>
										<label class="checkbox-inline">
											<input type="checkbox" name="product_free_consultation" class="product_free_consultation" value="<?php echo $value['consultation_id'];?>" <?php echo $ischecked; ?>> <span> <?php echo $value['consultation_name'];?> <br/> </span>
										</label>
										<?php	$i++;
									}
								} ?>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Paid Consultation <span class="required">*</span></label>
                            <div class="col-md-5">
                                <?php $i=1;
								if($paidconsult_data !=''){
									foreach($paidconsult_data as $value){  
									
										$ischecked='';
										if($product_paid_consultation !=''){
											foreach($product_paid_consultation as $val){ if($val == $value['service_id']){ $ischecked = 'checked'; } 
											} 
										}?>									
									<label class="checkbox-inline">
										<input type="checkbox" name="product_paid_consultation" class="product_paid_consultation" value="<?php echo $value['service_id'];?>" <?php echo $ischecked; ?>> <span style="margin-right: 40px;"> <?php echo $value['services_name'];?> </span>
									</label>
									<?php	$i++;
									}
								} ?>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Short Product Description <span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea rows="4" type="text" class="form-control" name="product_short_description" id="product_short_description" Placeholder="Your Message"> <?php echo $product_short_description;?> </textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Long Product Description <span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea rows="10" class="form-control" id="product_long_description" name="product_long_description" placeholder="product_long_description"> <?php echo $product_long_description;?> </textarea>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Status <span class="required">*</span></label>
                            <div class="col-md-5">
                                <select style="width: 100%;" class="form-control" name="status" id="status">
									<option value="">Select</option>
									<option value="Active" <?php if($status == 'Active'){ echo 'selected'; }?>> Active </option>
									<option value="Inactive" <?php if($status == 'Inactive'){ echo 'selected'; }?>> Inactive </option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name"> Is New <span class="required">*</span></label>
                            <div class="col-md-5">
                                <select style="width: 100%;" class="form-control" name="is_new" id="is_new">
									<option value="">Select</option>
									<option value="Yes" <?php if($is_new == 'Yes'){ echo 'selected'; }?>> Yes </option>
									<option value="No" <?php if($is_new == 'No'){ echo 'selected'; }?>> No </option>
										
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Meta Title <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_meta_title" name="product_meta_title" placeholder="Meta Title" value="<?php echo $product_meta_title; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Meta Description <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_meta_description" name="product_meta_description" placeholder="Meta Description" value="<?php echo $product_meta_description; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Meta Keyword <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_meta_keyword" name="product_meta_keyword" placeholder="Meta Keyword" value="<?php echo $product_meta_keyword; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Meta Tags <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_meta_tags" name="product_meta_tags" placeholder="Meta Tags" value="<?php echo $product_meta_tags; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Feature <span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea rows="10" class="form-control" id="product_feature" name="product_feature" placeholder="Product Feature"><?php echo $product_feature; ?> </textarea>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Height </label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="product_height" name="product_height" placeholder="Product Height" value="<?php echo $product_height; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Shape </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_shape" name="product_shape" placeholder="Product Shape" value="<?php echo $product_shape; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Color </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_color" name="product_color" placeholder="Product Color" value="<?php echo $product_color; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Clarity </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_clarity" name="product_clarity" placeholder="Product Clarity" value="<?php echo $product_clarity; ?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Grade </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_grade" name="product_grade" placeholder="Product Grade" value="<?php echo $product_grade;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Weight </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_weight" name="product_weight" placeholder="Product Weight" value="<?php echo $product_weight;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Treatment </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_treatment" name="product_treatment" placeholder="Product Treatment" value="<?php echo $product_treatment;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Origin </label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="product_origin" name="product_origin" placeholder="Product Origin" value="<?php echo $product_origin;?>">
                            </div>
                        </div>
						<div class="form-group display_none">
                            <label class="control-label col-md-2" for="product_image">Product Image<span class="required">*</span></label>
                            <div class="col-md-5">  
                                <input id="btnAdd" type="button" value="Add" data-toggle="modal" data-target="#browseImage"/>
                                <div id="productImageContainer">
                                    <div class="img-preview product_images" id="">
										<?php if($product_images ){
												foreach($product_images as $hash_name){		
										?>
										<div><img class="img-preview product_images" name="product_images" value="<?php echo $hash_name ; ?>" src="<?php echo base_url();?>uploads/<?php echo $hash_name ; ?>"> &nbsp;<input type="button" value="Remove" class="remove_image"></div>
										<?php } }?>
									</div>
                                </div>
                                <input class="form-control" id="product_image" name="product_image" type="hidden">
								
                            </div>
                        </div> 
						
							<div class="form-group">
								<label class="control-label col-md-2"  for="">Product Image <span class="required">*</span></label>
								<div class="col-md-5 crop-avatar-image" id="crop-avatar">
									<button type="button" class="btn btn-success btn-lg avatar-view" avatarInputname="product_image" avatarImageview="avatar-images-view" avatarIsIimageMultiple="yes" avatarIsprimary="no" style="max-width:200px;">+</button>
									<input class="form-control product_image" id="product_image" name="product_image" type="hidden" value="">
									<div class="avatar-images-view">
										<?php //Multiple Images
										if ($product_images) {
											//var_dump()
											foreach ($product_images as $img) {
												//var_dump($img);
												echo '<div  class="avtar_img_dives" addr="' . $img . '" ><img  name="product_images" value="' . $img . '" src="' . base_url() . 'uploads/' . $img . '" ><button type="button" class="btn btn-danger avtar_img_remover" >x</button></div>';
											}
										}
										?>
									</div>
									<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
								</div>
								<div class="validation_error product_images_error display_none error_div">Atleast one product image is required.</div>	   
							</div>						
						  	
						<div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="product" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('cropper_image_upload'); ?>

<script>
$('document').ready(function(){
		
	function GetDynamicTextBox(value, src) {
        return '<img class="img-preview product_images" name="product_images" value = "' + value + '"  src="' + src + '" > &nbsp;' +
                '<input type="button" value="Remove" class="remove_image" />'
    }
    $("body").on("click", ".remove_image", function () {
        $(this).closest("div").remove();
    });

	
	$('#upload_image').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                var div = $("<div />");
				console.log(response);
                div.html(GetDynamicTextBox(response.filename, APP_URL + 'uploads/' + response.filename));
                $("#productImageContainer").append(div);

                $('#browseImage').modal('hide');
                //$('#result_image').val(response.filename);
                //$('#err_gallery_form').css({"display":"none"});

            } else {
                $('#browseImage').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
});	
</script>

<script>
$('document').ready(function(){
	
	CKEDITOR.replace( 'product_long_description' );
	CKEDITOR.replace( 'product_feature' );
	
  $('#add_product_form').validate({
    ignore:[],
        rules: {
            product_name: {
                required: true,
            },
			string_url: {
                required: true,
            },
			
			product_actual_prize: {
                required: true,
            },
			product_discounted_prize: {
                required: true,
            },
			product_category: {
                required: true,
            },
			product_rashi: {
                required: true,
            },
			product_disease: {
                required: true,
            },
			product_free_consultation: {
                required: true,
            },
			product_paid_consultation: {
                required: true,
            },
			product_short_description: {
                required: true,
            },
			product_long_description: {
				required: function(textarea) {
					  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
					  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
					  return editorcontent.length === 0;
				}
			},
			status: {
                required: true,
            },
			is_new: {
                required: true,
            },
			product_meta_title: {
                required: true,
            },
			product_meta_description: {
                required: true,
            },
			product_meta_keyword: {
                required: true,
            },
			product_meta_tags: {
                required: true,
            },
			product_feature: {
				required: function(textarea) {
					  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
					  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
					  return editorcontent.length === 0;
				}
			},
			
			product_images: {
                required: true,
            },
        },
        messages: {
            product_name: {
                required: "Product name is required",
            },
			string_url: {
                required: "string url  is required",
            },
			product_actual_prize: {
                required: "Product actual prize is required",
            },
			product_discounted_prize: {
                required: "Product discounted prize is required",
            },
			product_category: {
                required: "Product category is required",
            },
			product_rashi: {
                required: "Product rashi is required",
            },
			product_disease: {
                required: "product disease is required",
            },
			product_free_consultation: {
                required: "product free consultation is required",
            },
			product_paid_consultation: {
                required: "Product paid consultation is required",
            },
			product_short_description: {
                required: "Product short description is required",
            },
			product_long_description: {
                required: "product long description is required",
            },
			status: {
                required: "status is required",
            },
			is_new: {
                required: "is new is required",
            },
			product_meta_title: {
                required: "Product meta title is required",
            },
			product_meta_description: {
                required: "product meta description is required",
            },
			product_meta_keyword: {
                required: "product meta keyword is required",
            },
			product_meta_tags: {
                required: "Rashi name is required",
            },
			product_feature: {
                required: "product feature is required",
            },
		
			product_images: {
                required: "product images is required",
            },
        },

        submitHandler: function (form) {
    
           var product_id = $('#product_id').val();
           var product_name = $('#product_name').val();
           var string_url = $('#string_url').val();
		   console.log(string_url);
		    //var string_url = $.trim(string_url);
			
			string_url=  string_url.replace(/[^\w\s]/gi, '')
			console.log(string_url);
			string_url= string_url.replace( /\s/g, '')
			console.log(string_url);
		   var product_actual_prize = $('#product_actual_prize').val();
           var product_discounted_prize = $('#product_discounted_prize').val();
           var product_category = $('#product_category').val();
           var product_rashi = $('.product_rashi').val();
           var product_disease = $('.product_disease').val();
           var product_free_consultation = $('.product_free_consultation').val();
           var product_paid_consultation = $('.product_paid_consultation').val();
           var product_short_description = $('#product_short_description').val();
		   var product_long_description = CKEDITOR.instances['product_long_description'].getData();
		   var status = $('#status').val();
		   var is_new = $('#is_new').val();
		   var product_meta_title = $('#product_meta_title').val();
		   var product_meta_description = $('#product_meta_description').val();
		   var product_meta_keyword = $('#product_meta_keyword').val();
		   var product_meta_tags = $('#product_meta_tags').val();
		   var product_feature = CKEDITOR.instances['product_feature'].getData();
		   var product_height = $('#product_height').val();
		   var product_shape = $('#product_shape').val();
		   var product_color = $('#product_color').val();
		   var product_clarity = $('#product_clarity').val();
		   var product_grade = $('#product_grade').val();
		   var product_weight = $('#product_weight').val();
		   var product_treatment = $('#product_treatment').val();
		   var product_origin = $('#product_origin').val();
		   var product_images = "";
			/*$("img[name=product_images]").each(function () {
                product_images += $(this).attr('value') + ",";
            });*/
			$('.avatar-images-view .avtar_img_dives').each(function(key,val){
				console.log($(this).attr('addr'));
				
				if(!$(this).attr('addr')){
					is_error = 'yes';
					$('.product_images_error').removeClass('display_none');
				}
				
				product_images += $(this).attr('addr') + ",";
				
			});
			console.log(product_images);
			//var product_images=$('.avtar_img_dives').attr('addr');
			//console.log(product_images);
			if(product_images==''){
				$("html, body").animate({scrollTop: 0}, "slow");
				var message="Product images is required"
				$('#errHeadMsg').empty();
                $('#errHeadMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + message + "</strong></div>");
				$("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#errHeadMsg').empty();
				});
			   return false;
			}
		    var product_rashi=[];
		    $.each($("input[name='product_rashi']:checked"), function(){
                product_rashi.push($(this).val());
            });
		    console.log(product_rashi);
		   
		    var product_disease=[];
		    $.each($("input[name='product_disease']:checked"), function(){
                product_disease.push($(this).val());
            });
		    console.log(product_disease);
		   
			var product_free_consultation=[];
		    $.each($("input[name='product_free_consultation']:checked"), function(){
                product_free_consultation.push($(this).val());
            });
			console.log(product_free_consultation);
		   
			var product_paid_consultation=[];
		    $.each($("input[name='product_paid_consultation']:checked"), function(){
                product_paid_consultation.push($(this).val());
            });
			console.log(product_paid_consultation);
            
            $.post(APP_URL + 'configure_access/update_products', {
                product_id: product_id,
                product_name: product_name,                
                string_url: string_url,                
                product_actual_prize: product_actual_prize,                
                product_discounted_prize: product_discounted_prize,                
                product_category: product_category,                
                product_rashi: product_rashi,                
                product_disease: product_disease,                
                product_free_consultation: product_free_consultation,                
                product_paid_consultation: product_paid_consultation,                
                product_short_description: product_short_description,                
                product_long_description: product_long_description,                
                status: status,                
                is_new: is_new,                
                product_meta_title: product_meta_title,                
                product_meta_description: product_meta_description,                
                product_meta_keyword: product_meta_keyword,                
                product_meta_tags: product_meta_tags,                
                product_feature: product_feature,                
                product_height: product_height,                
                product_shape: product_shape,                
                product_color: product_color,                
                product_clarity: product_clarity,                
                product_grade: product_grade,                
                product_weight: product_weight,                
                product_treatment: product_treatment,                
                product_origin: product_origin,                
                product_images: product_images,                
            }, 
            function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#errHeadMsg').empty();
                if (response.status == 200) {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					  $("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#errHeadMsg').empty();
						window.location.href = APP_URL+'configure_access/products_list';
					  });
				} else {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#errHeadMsg').empty();
					});
				}
            }, 'json');
            return false;
        }
    });
});
</script>
