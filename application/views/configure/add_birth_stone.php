<script src="<?php echo base_url(); ?>assets/js/birth_stone.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 
	//print_r ($birth_stone);
	if($birth_stone){
		$birth_stone_name = $birth_stone[0]['birth_stone_name'];
		$birth_stone_navi = $birth_stone[0]['birth_stone_navi'];
		$birth_stone_id = $birth_stone[0]['birth_stone_id'];
		$status = $birth_stone[0]['status'];
		$main_description = $birth_stone[0]['main_description'];
		$description = $birth_stone[0]['description'];
		
		
	}else{
		$birth_stone_name = '';
		$birth_stone_navi = '';
		$birth_stone_id = 0;
		$status = '';
		$main_description = '';
		$description = '';
		if(isset($_GET['birth_stone_id'])){
			$birth_stone_id  = $_GET['birth_stone_id'];
			
		}
		
		
	}
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add New Birth Stone</h1>
		<a href="<?php echo base_url();?>configure_access/edit_birth_stone" class="btn btn-default pull-right addAds">View All Birth Stone</a>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_birth_stone_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="addBirthStoneForm" method="post" class="form-horizontal">
					
					<input type="hidden" id="birth_stone_id" name="birth_stone_id" value="<?php echo $birth_stone_id;?>">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="birth_stone_name">Birth Stone Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="birth_stone_name" name="birth_stone_name" value="<?php echo $birth_stone_name;?>" placeholder="Birth Stone Name" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="birth_stone_navi">Birth Stone Navigation URL<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="birth_stone_navi" name="birth_stone_navi" value="<?php echo $birth_stone_navi;?>" placeholder="Birth Stone Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="main_description">Main Description<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="main_description" name="main_description" placeholder="Main Description" row="2"><?php echo $main_description;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="description">Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor description" id="description" name="description" placeholder="Services Description" row="2"><?php echo $description;?></textarea>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
