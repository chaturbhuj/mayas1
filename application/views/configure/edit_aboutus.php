<script src="<?php echo base_url(); ?>assets/js/aboutus.js"></script> 

<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>About Us</h1>
		</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_aboutus"></div>
					
                    <div id="edit_user_table">
					
                        <form id="edit_aboutus_form" method="post" class="form-horizontal">
							<div class="form-group ">
								<label class="control-label col-md-2" for="aboutus1">Aboutus Paragraph 1<span class="required">*</span></label>
							<div class="col-md-5">
								<textarea class="form-control ckeditor aboutus1" id="aboutus1" name="aboutus1" placeholder="Aboutus Paragraph 1" rows="5"><?php print_r($aboutus[0]['aboutus1']);?></textarea>
							</div>
						</div>
						<div class="form-group">
                          <label class="control-label col-md-2" for="blog_image1">Image 1<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="blog_image_btn" name="blog_image1" class="blog_image_btn" data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="blog_image1" name="blog_image1" value="<?php print_r($aboutus[0]['aboutus_image1']);?>" type="hidden">
								<img class="img-preview1" src="<?php echo base_url().'uploads/'.($aboutus[0]['aboutus_image1']);?>" alt="" style="width: 150px;">
								
                            </div>
                        </div>
						
						
                        <div class="form-group ">
						
							<label class="control-label col-md-2" for="aboutus2">Aboutus Paragraph 2<span class="required">*</span></label>
							<div class="col-md-5">
								<textarea class="form-control ckeditor aboutus2" id="aboutus2" name="aboutus2" placeholder="Aboutus Paragraph 2" rows="5"><?php print_r($aboutus[0]['aboutus2']);?></textarea>
							</div>
						</div>
						<div class="form-group">
                          <label class="control-label col-md-2" for="blog_image2">Image 2<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="blog_image_btn" name="blog_image2" class="blog_image_btn" data-toggle="modal" data-target="#browseImage2">Upload Image</a>
                                <input class="form-control" id="blog_image2" name="blog_image2" value="<?php print_r($aboutus[0]['aboutus_image2']);?>" type="hidden">
								<img class="img-preview2" src="<?php echo base_url().'uploads/'.($aboutus[0]['aboutus_image2']);?>" alt="" style="width: 150px;">
								
                            </div>
                        </div>
					
                        
						
                        
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_user_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit User</h4>
                    <input type="hidden" name="edit_user_id" id="edit_user_id" value=""/>
                    <div id="edit_user_id" style="visibility: none;"></div>                    
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_name">User Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control capitaliseFirstLetter" id="edit_user_name" name="edit_user_name" placeholder="User Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_email">User Email<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_user_email" name="edit_user_email" placeholder="User Email" type="email">
                        </div>
                    </div>
                    <div id="err_edit_user_email" class="col-md-offset-3 clearfix"></div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_assign_role_to_user">Assign Role to user<span class="required">*</span></label>
                        <div class="col-md-9 select_role">
                            <input class="form-control" id="edit_assign_role_to_user" name="edit_assign_role_to_user" placeholder="Assign Role to user" type="text">
                        </div>
                    </div><div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image1</h3>
            </div> 
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image2</h3>
            </div> 
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

