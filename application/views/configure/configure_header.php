<!DOCTYPE html>
<html lang="en">
    <head>
			<style>
				label.error{
					color:red;
				}
				.display_none{
					dispaly:none;
				}
			</style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Maya Astrology</title>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/vendor/fontawesome.css">
		
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery2/jquery-2.0.0.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery-ui.min.js"></script>        
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/jquery.form.min.js"></script>

        <!--        <link rel="stylesheet" href="/resources/demos/style.css">-->
        <!-- Bootstrap core CSS -->

        <link href="<?php echo base_url(); ?>assets/plugins/jquery-ui.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap3/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap3/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap3/css/simple-sidebar.css" rel="stylesheet">

        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap3/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.blockUI.min.js"></script>

        <link href="<?php echo base_url(); ?>assets/css/configure_main.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/configure_main.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/constant.js"></script> 

    </head>
	
	
    <body>
        <div id="">  
			<nav class="navbar navbar-default">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="<?php echo base_url(); ?>configure_access">Mayas</a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				  <ul class="nav navbar-nav">
					<li><a href="<?php echo base_url(); ?>configure_access">Dashboard</a></li>
					
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Content SEO <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>admin/content_setting/country_view">Country</a></li>
					<li><a href="<?php echo base_url(); ?>admin/content_setting/state_view">State</a></li>
					<li><a href="<?php echo base_url(); ?>admin/content_setting/city_view">City</a></li>
					<li><a href="<?php echo base_url(); ?>admin/content_setting/public_content">Public Content</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Blog <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li class=""><a href="<?php echo base_url(); ?>configure_access/blog_image">Blog Images</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/add_blog">Blog</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/add_counter">Home Counter</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/add_menu">Add Menu</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/edit_menu">Edit Menu</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/add_sub_menu_services">Add Sub Menu Services</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/edit_sub_menu_services">Edit Sub Menu Services</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/add_sub_menu_products">Add Sub Menu Products</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/edit_sub_menu_products">Edit Sub Menu Products</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/edit_special_feature">Edit Special Feature</a></li>
							<li class="display_none"><a href="<?php echo base_url(); ?>configure_access/add_special_feature">Add Special Feature</a></li>
							
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Reports <span class="caret"></span></a>
						<ul class="dropdown-menu">
							
							<li><a href="<?php echo base_url(); ?>page/consultation">View All Consultations</a></li>
							<li><a href="<?php echo base_url(); ?>page/paid_consultation">View All Paid Consultations</a></li>
							<li><a href="<?php echo base_url(); ?>page/consultation/100">Letest Free Consultations</a></li>
							<li><a href="<?php echo base_url(); ?>page/consultation/100">Letest Free Consultations By Range</a></li>
							<li><a href="<?php echo base_url(); ?>page/consultation_search">Search Previous Consultation</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/order_list">View All Order</a></li>
							<li><a href="<?php echo base_url(); ?>page/gemstone_recommendation">View All Gemstone</a></li>
							<li><a href="<?php echo base_url(); ?>page/vastu_consultations">View All Vastu Consultations</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Products <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!--<li><a href="<?php //echo base_url(); ?>configure_access/add_product">Add Product</a></li>-->
							<!--<li><a href="<?php //echo base_url(); ?>configure_access/edit_product">Product List</a></li>-->
							<li><a href="<?php echo base_url(); ?>configure_access/category_add">Add Category</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/category_list">Category List</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/rashi_add">Add Rashi</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/rashi_list">Rashi List</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/disease_add">Add Disease</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/disease_list">Disease List</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/products_add">Add Product</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/products_list">Product List</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Content <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>configure_access/consultation_massages">Consultation Massages</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/add_top_banner_image">Upload Banner Image</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_banner_image">Edit Banner Image</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/content">Content</a></li>
							<li><a href="<?php echo base_url(); ?>page/public_content">Public Content</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/add_lagan_sign">Add Lagan Sign</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_lagan_sign">Edit Lagan Sign</a></li>
							<li><a href="<?php echo base_url(); ?>page/popup_ads">Popup Ads</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/testimonials/upload_image">Testimonials</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/add_slider">Add Slider</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_slider">Edit Slider</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_birth_stone">Birth Stone</a></li>
							<li><a href="<?php echo base_url(); ?>meta/meta_view">view Meta data</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_aboutus">About Us</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_vastu">Vastu</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/public_vastucontent">Edit Vastu Page</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/faqcatlist">Faq Category List</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/faqlist">Faq List</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/banner_list">Banner List</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/public_process">Vastu Process List</a></li>
							<li><a href="<?php echo base_url(); ?>admin/content_setting/public_vastuaboutcontent">Vastu aboutus List</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Service <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!--<li><a href="<?php echo base_url(); ?>configure_access/add_bespoke_services">Add Bespoke Services</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/edit_bespoke_services">Edit Bespoke Services</a></li>
                    
							<li><a href="<?php echo base_url(); ?>configure_access/edit_instance_consultation">Instance Services</a></li>-->
							<li><a href="<?php echo base_url(); ?>configure_access/edit_services">Premium Services</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/add_services">Add Services</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/coupan">Coupon</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Free Consultation <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>configure_access/add_free_consultation">Add Free Consultation</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/free_consultation_list">Free Consultation List</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Cronjob Setting <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>configure_access/cronjob_setting">Add Cronjob</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/cronjob_list">Cronjob List</a></li>
							
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Page Data <span class="caret"></span></a>
						<ul class="dropdown-menu">
						    <li><a href="<?php echo base_url(); ?>configure_access/page_data">Add Page Data</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/page_data_list">Page Data List</a></li>
						</ul>
					</li>
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						Oder <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>configure_access/order_lists/all">ALL</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/order_lists/inprocess">Inprocess</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/order_lists/completed">completed</a></li>
							<li><a href="<?php echo base_url(); ?>configure_access/order_lists/failed">Failed Payment</a></li>
							
						</ul>
					</li>
				  </ul>
				  
				  <ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('userName'); ?> <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						<li><a href="<?php echo base_url(); ?>configure_access/change_password">Change Password</a></li>
                    	<li role="separator" class="divider"></li>
						<li><a class="logout" href="Javascript:Void(0);">LOG OUT</a></li>
					  </ul>
					</li>
				  </ul>
				</div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
