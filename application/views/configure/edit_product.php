<script src="<?php echo base_url(); ?>assets/js/product.js"></script> 
<style>img.edit_product_images {  width: 100px;}</style>
<div class="container-fluid main-content">
<div class="page-title">
        <h1>List Of All Products</h1>
		<a href="<?php echo base_url();?>configure_access/add_product" class="btn btn-default pull-right addAds">Add Products</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_product"></div>
                    <div id="edit_team_table">
                        <div id="table_view">             
                            <?php
                            if ($all_product == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Product Categories</th><th class="text-center">Product Name</th><th class="text-center">Product description</th><th class="text-center">Product price ind</th><th class="text-center">Product price usd</th><th class="text-center">Product feature</th><th class="text-center">Product Weight</th><th class="text-center">Product shape</th><th class="text-center">Product color</th><th class="text-center">Product clarity</th>
								<th class="text-center">Product grade</th><th class="text-center">Product cut</th><th class="text-center">Product treatment</th><th class="text-center">Product origin</th><th class="text-center">Product other info1</th><th class="text-center">Product other info2</th><th class="text-center">Product other info3</th><th class="text-center">Product Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($all_product as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center" name="' . $value['product_categories'] . '">' . $value['sub_menu_products_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['product_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['product_description'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['product_price_ind'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_price_usd'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_feature'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_weight'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_shape'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_color'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_clarity'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_grade'] . '</td>';
								    $content .= '<td class="text-center">' . $value['product_cut'] . '</td>';
								    $content .= '<td class="text-center">' . $value['product_treatment'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_origin'] . '</td>';
								    $content .= '<td class="text-center">' . $value['product_other_info'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_other_info2'] . '</td>';
									$content .= '<td class="text-center">' . $value['product_other_info3'] . '</td>';
									$content .= '<td class="product_image_td_' . $value['product_id'] . '">';
                                     $event_image_array = explode(",", $value['product_image_list']);
                                    foreach ($event_image_array as $key => $val) {
                                        if ($val)
                                            $content .= '<div><a id=' . $value['product_id'] . '_' . $key . ' class="editBrowseImageBtn btnImg" name=' . $value['product_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="javascript:void(0);"><img class="edit_product_images" value="' . $val . '" src="' . base_url() . 'uploads/' . $val . '"></a><input type="button" value="Remove" class="edit_remove_image" /></div>';
                                    }
									$content .= '<input id="btnAdd" class="editBrowseImageBtn btnAdd" name=' . $value['product_id'] . ' type="button" value="Add"  data-toggle="modal" data-target="#editBrowseImage"/></td>';
                                    $content .= '</td><td class="text-center"><a href="#" class="edit_product" data-toggle="modal" data-target="#my_user_edit" name=' . $value['product_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_product"  name=' . $value['product_id'] . ' value=""><span class="label label-danger">Remove Service</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_product_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Products</h4>
                    <input type="hidden" name="product_id" id="product_id" value=""/>     
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="product_categories">Product Categories<span class="required">*</span></label>
                            <div class="col-md-9"> 
								<select class="form-control	" id="product_categories" name="product_categories">
								<option value="">select product categories</option>
								<?php
								if ($all_submenu_product_name) {
									foreach ($all_submenu_product_name as $value) {
								 echo'
									<option value="'.$value["sub_menu_products_id"].'">'.$value["sub_menu_products_name"].'</option>
								';}}?>
								</select>
                            </div>
                        </div>
						<div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="product_name">Product Name <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_name" name="product_name" placeholder="Product Name" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_description">product description <span class="required">*</span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_description" name="product_description" placeholder="product description" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_price">product price <span class="required">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" id="product_price_ind" name="product_price_ind" placeholder="Product price ind" type="text">
                            </div>
							<div class="col-md-4">
                                <input class="form-control" id="product_price_usd" name="product_price_usd" placeholder="Product price usd" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_feature">Product feature <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_feature" name="product_feature" placeholder="Product feature" type="text">
                            </div>   </div>
							<div class="clearfix"></div>
							<div class="form-group">
                            <label class="control-label col-md-3" for="product_weight">Product Weight <span class="required"></span></label>
							
                            <div class="col-md-9">
                                <input class="form-control" id="product_weight" name="product_weight" placeholder="Product Weight" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_shape">Product shape <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_shape" name="product_shape" placeholder="Product shape" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_color">Product color <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_color" name="product_color" placeholder="Product color" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_clarity">Product clarity <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_clarity" name="product_clarity" placeholder="Product clarity" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_grade">Product grade <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_grade" name="product_grade" placeholder="Product grade" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_cut">Product cut <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_cut" name="product_cut" placeholder="Product cut " type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_treatment">Product treatment <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_treatment" name="product_treatment" placeholder="Product treatment" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_origin">Product origin <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_origin" name="product_origin" placeholder="Product origin" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_other_info">Product other info1 <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_other_info" name="product_other_info" placeholder="Product other info1" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_other_info2">Product other info2 <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_other_info2" name="product_other_info2" placeholder="Product other info2" type="text">
                            </div>
                        </div>
						<div class="clearfix"></div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="product_other_info3">Product other info3 <span class="required"></span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="product_other_info3" name="product_other_info3" placeholder="Product other info3" type="text">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="product_image">Gallery Image<span class="required">*</span></label>
                            <div class="col-md-9">  
                                <input id="btnAdd" type="button" value="Add"  data-toggle="modal" data-target="#browseImage"/>
                                <div id="productImageContainer">
                                    <!--Textboxes will be added here -->
                                </div>
                                <input class="form-control" id="product_image" name="product_image" type="hidden">
                            </div>
                        </div> 
							
			  <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="edit_product_image_list" id="edit_product_image_list" value=""/>
                    <input type="hidden" name="image_product_id" id="image_product_id" value=""/>     
                    <input type="hidden" value="product" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>