



<script src="<?php echo base_url();?>assets/js/gallery.js"></script>	


			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
.img_pre{
	width:100px;
}
.br_color{
	background-color: #fff;
}
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Gallery
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="wrapper wrapper-content animated fadeInRight" style="background-color: rgba(245, 237, 237, 0.14);">
		    <div class="box box-primary">
				
				<div class="row">
				   <div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5 style="padding-left:15px;">Add Image</h5>
								<div class="ibox-tools">
									<a class="collapse-link">
										<i class="fa fa-chevron-up"></i>
									</a>
									<a class="close-link">
										<i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							
							<div class="ibox-content">
								<div id="headerMsg3" class="err_msg"></div>
								<form class="form-horizontal" id="add_image_form">
									
									<div id="headMsg"></div>
									
									<div class="form-group">
										<label class="control-label col-md-2" for="image" >Image<span class="required">*</span></label>
										<div class="col-md-6">  
											<input id="image_btn" type="button" name="main_image" class="btn btn-success inline_block image_btn" value="Upload Image"  data-toggle="modal" data-target="#browseImage"/>
											<input type="hidden" name="image" id="image" class="form-control" value="">
											<div class="image-preview inline_block">
												<img src="" class="display_none" style="width:200px;">
											</div>
										</div>
									</div>	
									<div class="clearfix"></div>
									
									<div class="form-group">
										<div class="col-lg-offset-2 col-lg-6">
											<button class="btn btn-sm btn-primary" type="submit">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
				</div>
				
				
			</div>
		</div>


		<div class="clearfix"></div>
		<div class="wrapper wrapper-content animated fadeInRight" style="background-color: rgba(245, 237, 237, 0.14);">
			<div class="row br_color">
				<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5 style="padding-left:15px;">Images</h5>
						<div class="ibox-tools">
							<a class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							
							<a class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
					</div>
					<div id="headerMsg"  class="err_msg"></div>
					
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
					<thead>
					<tr>
						<th>S. No.</th>
						<th>Image</th>
						<th>Image URL</th>
						
						<th>Date</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$i = 1;
					if ($images_list == 0) {
						echo 'No record found into database';
					} else {
						
						$content = '';
						foreach ($images_list as $value) {
							$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
							$content .= '<td class="text-center"><img class="img_pre img-rounded" src="'.base_url().'uploads/'.$value['image'].'"></td>';
							$content .= '<td class="text-center">'.base_url().'uploads/'.$value['image'].'</td>';
						
							$content .= '<td class="text-center">' . date('d F Y',strtotime($value['date'])) . '</td>';
							$content .= '<td class="text-center"><a href="#" class="remove_image"  name=' . $value['img_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
							$i++;
						}
						echo $content;
					}
					?>
					</tbody>
				
					</table>
						</div>

					</div>
				</div>
			</div>
			<br/>
			</div>
			
	

			<!---------------------------- Modal for Browse Image-------------------------->
			<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" style="background-color: #f5f5f5;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3>Browse Image 1</h3>
						</div> 
						<form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
							<div class="modal-body">
								<p>Image type should be GIF,JPG,PNG</p>
								<p>Image should be 2 MB or smaller</p>
								<div id="head1_msg"></div>
								<input type="hidden" value="image" name="image_cat" class="image_cat">
								<input type="hidden" value="" name="sub_folder_name" class="sub_folder_name">
								<input type="file" id="myFile" name="myFile" size="20" multiple> 
								<div class="progress progress_bar">
									<div class="bar progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
										<div class="percent">0%</div >
									</div>
								</div>	
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary ">Save Picture</button>
							</div>
						</form>
					</div>
				</div>
			</div>



			<!---------------------------- Modal for Browse Module-------------------------->
			<div class="modal fade" id="browseEditImagedetails" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" style="background-color: #f5f5f5;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3>Update Image</h3>
						</div> 
						<div class="modal-body row">
							<div class="col-md-12">
								<form class="well" id="img_detail_form" method="post" enctype="multipart/form-data">
									<input class="form-control" id="img_id2" name="img_id2" value=0 type="hidden">
									<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
										<label class="control-label col-md-3" for="img_name2">Image Name <span class="required">*</span></label>
										<div class="col-md-9">
											<input class="form-control" id="img_name2" name="img_name2" placeholder="Image Name" type="text">
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
             

            });
            });

            

     
    </script>
	      