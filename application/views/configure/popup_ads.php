<script src="<?php echo base_url(); ?>assets/js/configure/popup_ads.js"></script> 

<style>
.img-preview,.img-preview_consultation,.img-preview_gemstone_recommendation{
	  width: 100%;
	height: 100%;
	margin: 10px;
}
.view_img-preview{
	width: 100%;
	height: 100%;
}	
.view_img-preview2{
	width: 100%;
	height: 100%;
}	
.view_img-preview3{
	width: 100%;
	height: 100%;
}	
span.form-control{
	height: auto;
}	
</style>
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Popup Ads</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-9 col-md-11" id="err_refer_a_friend_form1"></div>
                <div id="headMsg" class="col-md-12"></div>
				<div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="popup_ads_form" method="post" class="form-horizontal display_none">
						<input class="form-control" id="popup_ads_id" name="popup_ads_id" value="<?php echo $popup_ads_list[0]['popup_ads_id']; ?>" type="hidden">
						<div class="form-group ">
                            <label class="control-label col-md-3" for="popup_status">Popup Status <span class="required">*</span></label>
                            <div class="col-md-6">
                                <select class="form-control" id="popup_status" name="popup_status" style="font-size: 16px;">
									<option value="Enable" <?php if($popup_ads_list[0]['popup_status']=='Enable') echo 'selected';?>>Enable</option>
									<option value="Disable" <?php if($popup_ads_list[0]['popup_status']=='Disable') echo 'selected';?>>Disable</option>
								</select>
							</div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-3" for="popup_ads_image">Popup Image <span class="required">*</br>(size 560*400px)</span></label>
                            <div class="col-md-9">
                                <a id="popup_ads_image_btn" name="popup_ads" class="btn btn-success popup_ads_image_btn add_image" data-toggle="modal" data-target="#browseImage" style="width: 167px;">Image </a>
                                <input class="form-control popup_ads_image" id="popup_ads_image" name="popup_ads_image" type="hidden" value="<?php echo $popup_ads_list[0]['popup_ads_image']; ?>">
								<img style=" height: 300px;width: 300px;" class="img-preview" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['popup_ads_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group display_none">
                            <label class="control-label col-md-3" for="navigation_link">Navigation Link <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input class="form-control" id="navigation_link" name="navigation_link" placeholder="Navigation Link " type="text" value="<?php echo $popup_ads_list[0]['navigation_link']; ?>">
							</div>
                        </div>
					
						
						
						<div class="form-group">
                            <label class="control-label col-md-3" for="free_consultation_image">Free Consultation Image <span class="required"></br>(size 900*225px)</span></label>
                            <div class="col-md-9">
                                <a id="free_consultation_image_btn" name="consultation_image" class="btn btn-success free_consultation_image_btn add_image" data-toggle="modal" data-target="#browseImage" style="width: 167px;">Image </a>
                                <input class="form-control free_consultation_image" id="free_consultation_image" name="free_consultation_image" type="hidden" value="<?php echo $popup_ads_list[0]['free_consultation_image']; ?>">
								<img style=" height: 300px;width: 300px;" class="img-preview_consultation" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['free_consultation_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="free_gemstone_recommendation_image">Free Gemstone Recommendation Image <span class="required"></br>(size 900*225px)</span></label>
                            <div class="col-md-9">
                                <a id="free_gemstone_recommendation_image_btn" name="gemstone_recommendation_image" class="btn btn-success free_gemstone_recommendation_image_btn add_image" data-toggle="modal" data-target="#browseImage" style="width: 167px;">Image </a>
                                <input class="form-control free_gemstone_recommendation_image" id="free_gemstone_recommendation_image" name="free_gemstone_recommendation_image" type="hidden" value="<?php echo $popup_ads_list[0]['free_gemstone_recommendation_image']; ?>">
								<img style=" height: 300px;width: 300px;" class="img-preview_gemstone_recommendation" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['free_gemstone_recommendation_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group">
                            <div class="col-md-3">
							</div>
							<div class="col-md-6">
                                <button class="btn  btn-primary" type="submit" value="Submit">Submit</button>
							</div>                            
                        </div>    
					</form>
					<!--view section-->
					<div class="view_popup_ads_div form-horizontal display_block">
						<div class="form-group ">
                            <label class="control-label col-md-3" for="popup_status">Popup Status : </label>
                            <div class="col-md-8">
                                <span class="form-control" id="view_popup_status"><?php echo $popup_ads_list[0]['popup_status']; ?></span>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="popup_ads_image">Popup Image : </label>
                            <div class="col-md-9">
                                <img style=" height: 300px;width: 300px;" class="view_img-preview" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['popup_ads_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group display_none">
                            <label class="control-label col-md-3" for="navigation_link">Navigation Link : </label>
                            <div class="col-md-8">
                                <span class="form-control" id="view_navigation_link"><?php echo $popup_ads_list[0]['navigation_link']; ?></span>
							</div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-3" for="free_consultation_image">Free Consultation Image : </label>
                            <div class="col-md-9">
                                <img style=" height: 300px;width: 300px;" class="view_img-preview2" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['free_consultation_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-3" for="free_gemstone_recommendation_image">Free Gemstone Recommendation Image : </label>
                            <div class="col-md-9">
                                <img style=" height: 300px;width: 300px;" class="view_img-preview3" src="<?php echo base_url();?>uploads/<?php echo $popup_ads_list[0]['free_gemstone_recommendation_image']; ?>" alt="">
							</div>
                        </div>
						<div class="form-group">
                            <div class="col-md-3">
							</div>
							<div class="col-md-6">
                                <button class="btn  btn-primary edit_popup_ads" type="button" value="edit">&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>
							</div>                            
                        </div>  
					</div>
					
				</div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>page/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
					<p>Image should be less then the 2MB</p>
                    <div id="head1_msg"></div>
                    <input type="hidden" value="popup_ads" name="image_cat" class="image_cat" id="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
					<div class="progress progress_bar">
						<div class="bar progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
							<div class="percent">0%</div >
						</div>
					</div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>