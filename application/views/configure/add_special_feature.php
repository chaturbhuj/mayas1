<script src="<?php echo base_url(); ?>assets/js/special_feature.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Special Feature</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_special_feature_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="addSpecialFeatureForm" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="special_feature_heading">Special Feature Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="special_feature_heading" name="special_feature_heading" placeholder="Special Feature Heading" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sub_heading">Sub Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="sub_heading" name="sub_heading" placeholder="Sub Heading" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="small_sub_heading_message">Small Sub Heading Message</label>
                            <div class="col-md-5">
                                <input class="form-control" id="small_sub_heading_message" name="small_sub_heading_message" placeholder="Small Sub Heading Message" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point1">Point 1</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point1" name="point1" placeholder="Point 1" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point2">Point 2</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point2" name="point2" placeholder="Point 2" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point3">Point 3</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point3" name="point3" placeholder="Point 3" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point4">Point 4</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point4" name="point4" placeholder="Point 4" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point5">Point 5</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point5" name="point5" placeholder="Point 5" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point6">Point 6</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point6" name="point6" placeholder="Point 6" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="point7">Point 7</label>
                            <div class="col-md-5">
                                <input class="form-control" id="point7" name="point7" placeholder="Point 7" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
