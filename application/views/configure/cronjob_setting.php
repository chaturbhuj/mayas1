

<style>.product_images{  width: 100px;}</style>
<script src="https://cdn.ckeditor.com/4.8.0/full/ckeditor.js"> </script>

<?php 
	//$cron_data="";
	//var_dump($cron_data);
	if($cron_data){
		
		//$cronjob_id =$cron_data[0]['cronjob_id'];
		//var_dump($cron_data[0]->cronjob_id);
		
		$cronjob_id =$cron_data[0]->cronjob_id;
		$consultant_type =$cron_data[0]->consultant_type;
		$c_start_id =$cron_data[0]->c_start_id;
		$c_end_id =$cron_data[0]->c_end_id;
		$title =$cron_data[0]->title;
		$Subject =$cron_data[0]->Subject;
		$bocy = $cron_data[0]->bocy;
		$Status =$cron_data[0]->status;
		$count_end =$cron_data[0]->count_end;
		$count_start =$cron_data[0]->count_start;
		//var_dump($Status);
			
	}else{
		$cronjob_id =0;
		$consultant_type ="";
		$c_start_id ="";
		$c_end_id ="";
		$title ="";
		$Subject ="";
		$bocy ="";
		$Status ="";
		$count_end ="";
		$count_start ="";
		
	}
	
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add Cron job</h1>
		<a href="<?php echo base_url();?>configure_access/cronjob_list" class="btn btn-default pull-right addAds">View All Cronjob List</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="errHeadMsg"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_product_form" method="post" class="form-horizontal">
						<input type="hidden" id="cronjob_id" name="cronjob_id" value="<?php echo $cronjob_id;?> ">
						<div class="form-group">
                            <label class="control-label col-md-2" for="consultant_type"> Consultant Type <span class="required">*</span></label>
                            <div class="col-md-5">
                                <select style="width: 100%;" class="form-control" name="consultant_type" id="consultant_type">
									<option value="">Select</option>
									<option value="free" <?php if($consultant_type == 'free'){ echo 'selected'; }else{echo '';}?>> free </option>
									<option value="paid" <?php if($consultant_type == 'paid'){ echo 'selected'; }else{echo '';}?>> paid </option>
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="c_start_id">C Start Id <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="c_start_id" name="c_start_id" placeholder="C Start Id" value="<?php echo $c_start_id;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="c_end_id">C End Id  <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="c_end_id" name="c_end_id" placeholder="C End Id " value="<?php echo $c_end_id;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="title">Title <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="<?php echo $title;?>">
                            </div>
                        </div>

						<div class="form-group">
                            <label class="control-label col-md-2" for="Subject">Subject<span class="required">*</span></label>
                            <div class="col-md-5">
								<input type="text" class="form-control" id="Subject" name="Subject" placeholder="Subject" value="<?php echo $Subject;?>"> </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="bocy">bocy <span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea rows="10" class="form-control" id="bocy" name="bocy" placeholder="product_long_description"> <?php echo $bocy;?> </textarea>
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="count_start">Count start <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="count_start" name="count_start" placeholder="count start" value="<?php echo $count_start;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="count_end">Count End <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="count_end" name="count_end" placeholder="count end" value="<?php echo $count_end;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="Status"> Status <span class="required">*</span></label>
                            <div class="col-md-5">
                                <select style="width: 100%;" class="form-control" name="status" id="status">
									<option value="">Select</option>
									<option value="Active"<?php if($Status == 'Active'){ echo 'selected'; }else{echo '';}?>> Active </option>
									<option value="Inactive"<?php if($Status == 'Inactive'){ echo 'selected'; }else{echo '';}?>> Inactive </option>
								</select>
                            </div>
                        </div>
						                       
						<div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
$('document').ready(function(){
	
	CKEDITOR.replace('bocy');
	
	
  $('#add_product_form').validate({
    ignore:[],
        rules: {
			consultant_type:{
				 required: true,
			},
            c_start_id: {
                required: true,
            },
			c_end_id: {
                required: true,
            },
			title: {
                required: true,
            },			
			Subject: {
                required: true,
            },
			bocy: {
				required: function(textarea) {
					  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
					  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
					  return editorcontent.length === 0;
				}
			},
			status: {
                required: true,
            },
			
					
        },
        messages: {
			
            consultant_type: {
                required: "consultant_type  is required",
            },
			c_start_id: {
                required: "c start id  is required",
            },
			c_end_id: {
                required: "c end id  is required",
            },
			title: {
                required: "Product discounted prize is required",
            },
			Subject: {
                required: "Subject  is required",
            },			
			bocy: {
                required: "bocy description is required",
            },
			status: {
                required: "status is required",
            },
			
        },

        submitHandler: function (form) {
    
           var cronjob_id = $('#cronjob_id').val();
           var consultant_type = $('#consultant_type').val();
           var c_start_id = $('#c_start_id').val();
           var c_end_id = $('#c_end_id').val();
           var title = $('#title').val();
           var Subject = $('#Subject').val();          
           var status = $('#status').val();          
		   var bocy = CKEDITOR.instances['bocy'].getData();
		   var status = $('#status').val();
		   var count_start = $('#count_start').val();
		   var count_end = $('#count_end').val();
            
            $.post(APP_URL + 'configure_access/add_cronjob', {
                cronjob_id: cronjob_id,
                consultant_type: consultant_type,                
                c_start_id: c_start_id,                
                c_end_id: c_end_id,                
                title: title,                
                Subject: Subject,                
                status: status,                                                             
                count_start: count_start,                                                             
                count_end: count_end,                                                             
                bocy: bocy,                                                             
            }, 
            function (response) {
			$.unblockUI();
			$("html, body").animate({scrollTop: 0}, "slow");
                $('#errHeadMsg').empty();
                if (response.status == 200) {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#errHeadMsg').empty();
					//window.location.reload();
					window.location.href = APP_URL+'configure_access/cronjob_list';
					});
				}else {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#errHeadMsg').empty();
					});
				}
            }, 'json');
            return false;
        }
    });
});
</script>
