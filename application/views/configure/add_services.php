<script src="<?php echo base_url(); ?>assets/js/services.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>

<?php 
	// var_dump($services);
	if($services){
        $faq_category = $services[0]['faq_category'];
        $banner_id = $services[0]['banner_id'];
        $about_content_id = $services[0]['about_content_id'];
        $is_question1 = $services[0]['is_question1'];
        $q1_label = $services[0]['q1_label'];
        $q1_placeholder = $services[0]['q1_placeholder'];
        $is_question2 = $services[0]['is_question2'];
        $q2_label = $services[0]['q2_label'];
        $q2_placeholder = $services[0]['q2_placeholder'];
		$service_id = $services[0]['service_id'];
		$services_name = $services[0]['services_name'];
		$services_type = $services[0]['services_type'];
		$cost = $services[0]['cost'];
		$usdcost = $services[0]['usdcost'];
		$discounted_cost = $services[0]['discounted_cost'];
		$discounted_usd_cost = $services[0]['discounted_usd_cost'];
		$col = $services[0]['col'];
		$consultant_type = $services[0]['consultant_type'];
		$number_of_questions = $services[0]['number_of_questions'];
		$status = $services[0]['status'];
		$services_main_description = $services[0]['services_main_description'];
		$services_description = $services[0]['services_description'];
		$services_image = $services[0]['services_image'];
		$services_detail_image = $services[0]['services_detail_image'];
		$detail_image_position = $services[0]['detail_image_position'];
		$service_step_image = $services[0]['service_step_image'];
		$disc_logo_percent = $services[0]['disc_logo_percent'];
		$premium_type = $services[0]['premium_type'];
		$services_url = $services[0]['services_url'];
		$services_title = $services[0]['services_title'];
		$services_dec_meta = $services[0]['services_dec_meta'];
		$services_keyword_meta = $services[0]['services_keyword_meta'];
		$services_position = $services[0]['services_position'];
        $additional_json = $services[0]['additional_json'];
        $q_label =$services[0]['q_label'];
        $q_placeholder =$services[0]['q_placeholder'];
        $q_info =$services[0]['q_info'];
        $single_married_label =$services[0]['single_married_label'];
        $single_married_placeholder =$services[0]['single_married_placeholder'];
        $extra_description =$services[0]['extra_description'];
 
       
		
	}else{
        $faq_category = 0;
        $banner_id = 0;
        $about_content_id = 0;
        $is_question1 = 'no';
        $q1_label = '';
        $q1_placeholder = '';
        $is_question2 = 'no';
        $q2_label = '';
        $q2_placeholder = '';
		$service_id = 0;
		$premium_type = 'no';
		$services_name = '';
		$services_type = '';
		$cost = '';
		$usdcost = '';
		$discounted_cost = '';
        $discounted_usd_cost = '';
		$consultant_type = '';
		$number_of_questions = '';
		$disc_logo_percent = '';
		$attachments = '';
		$status = '';
		$col = '';
		$services_main_description = '';
		$services_description = '';
		$services_image = '';
		$services_detail_image = 'dummy.png';
		$detail_image_position = '';
		$service_step_image = '';
		$services_url = '';
		$services_title ='';
		$services_dec_meta ='';
		$services_keyword_meta ='';
		$services_position ='';
		if(isset($_GET['service_id'])){
			$service_id  = $_GET['service_id'];
			
		}
        $additional_json = array();
        $q_label ='';
        $q_placeholder ='';
        $q_info ='';
        $single_married_label ='';
        $single_married_placeholder ='';
        $extra_description ='';
	} 
?>

<?php
// var_dump($services[0]['additional_json']);
?>
<style>
.inline_input {
    padding: 0 10px;
}
</style>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Services</h1>
        <a href="<?php echo base_url();?>configure_access/edit_services" class="btn btn-default pull-right addAds">View
            All Services</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_services_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_services_form" method="post" class="form-horizontal">

                        <input type="hidden" id="service_id" name="service_id" value="<?php echo $service_id;?>">

                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_name">Services Name<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_name" name="services_name"
                                    value="<?php echo $services_name;?>" placeholder="Services Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_url">Url<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" style="text-transform: lowercase;" id="services_url"
                                    name="services_url" value="<?php echo $services_url;?>" placeholder="Services Url"
                                    type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_title">Services Title<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_title" name="services_title"
                                    value="<?php echo $services_title;?>" placeholder="Services title" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_dec_meta">Services desc meta<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_dec_meta" name="services_dec_meta"
                                    value="<?php echo $services_dec_meta;?>" placeholder="Services desc meta"
                                    type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_keyword_meta">Services Keyword meta<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_keyword_meta" name="services_keyword_meta"
                                    value="<?php echo $services_keyword_meta;?>" placeholder="Services  Keyword meta"
                                    type="text">
                            </div>
                        </div>
                        <div hidden class="form-group">
                            <label class="control-label col-md-2" for="services_type">Services Type<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="services_type" name="services_type" value="">
                                    <option value="0">Select type of service</option>
                                    <option value="main" <?php if($services_type == 'main' ) echo 'selected';?>>Main
                                    </option>
                                    <option value="other" <?php if($services_type == 'other' ) echo 'selected';?>>Other
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="consultant_type" class="col-md-2 control-label">Select type of consultation<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select name="consultant_type" id="consultant_type"
                                    class="form-control consultant_type">
                                    <option value="" name="">Select the type of consultation</option>
                                    <option value="Career" <?php if($consultant_type == 'Career' ) echo 'selected';?>
                                        name="career">Question Related to Career</option>
                                    <!-- <option value="BirthStone" <?php if($consultant_type == 'BirthStone' ) echo 'selected';?> name="birth_stone">Question Related to Birth Stone</option>
									<option value="HoroscopeQuery" <?php if($consultant_type == 'HoroscopeQuery' ) echo 'selected';?>  name="horoscope">Question Related to Horoscope Query</option>
									-->
                                    <option value="MatchMaking"
                                        <?php if($consultant_type == 'MatchMaking' ) echo 'selected';?>
                                        name="match_making">Question Related to Match Making</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_main_description">Services Main
                                Description<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="services_main_description"
                                    name="services_main_description" placeholder="Services Main Description"
                                    row="2"><?php echo $services_main_description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="premium_type">Partner information required<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="premium_type" name="premium_type" value="">
                                    <option value="">Select</option>
                                    <option value="yes" <?php if($premium_type == 'yes' ) echo 'selected';?>>Yes
                                    </option>
                                    <option value="no" <?php if($premium_type == 'no' ) echo 'selected';?>>No</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group single_married <?php if($premium_type == 'no' ) echo 'display_none';?> ">
                            <label class="control-label col-md-2" for="single_married_label">Single married label<span
                                    class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="single_married_label" name="single_married_label"
                                    placeholder="Label" value="<?php echo $single_married_label;?>" type="text">
                            </div>
                        </div>
                        <div
                            class="form-group single_married  <?php if($premium_type == 'no' ) echo 'display_none';?> ">
                            <label class="control-label col-md-2" for="is_question1">Single married placeholder<span
                                    class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="single_married_placeholder"
                                    name="single_married_placeholder" placeholder="placeholder"
                                    value="<?php echo $single_married_placeholder;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="premium_type">Col<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="col" name="col" value="">
                                    <option value="">select column</option>
                                    <option value="3" <?php if($col == '3' ) echo 'selected';?>>3</option>
                                    <option value="4" <?php if($col == '4' ) echo 'selected';?>>4</option>
                                    <option value="6" <?php if($col == '6' ) echo 'selected';?>>6</option>
                                    <option value="12" <?php if($col == '12' ) echo 'selected';?>>12</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-2 text-right">Add</label>
                            <label for="" class="add_question btn btn-success">+</label>
                        </div>
                        <div class="add_inputs">
                            <?php 
                                // var_dump($additional_json);
                                if(!empty($additional_json)){
                                    foreach($additional_json as $val){

                                ?>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                        <label>Heading</label>
                                        <input type="text" class="form-control s_heading" id="s_heading"
                                            name="s_heading" placeholder="Heading"
                                            value="<?php echo $val->s_heading;?>">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                        <label>Write Description</label>
                                        <input type="text" class="form-control description" id="description"
                                            name="description" placeholder="Description"
                                            value="<?php echo $val->description;?>">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                        <label>INR Cost</label>

                                        <input class="form-control cost" id="cost" name="cost"
                                            value="<?php echo $val->cost;?>" placeholder="Cost" type="number" min="0">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                        <label>USD Cost</label>
										<?php if(isset($val->usdcost)){ ?>
                                        <input class="form-control usdcost" id="usdcost" name="usdcost"
                                            value="<?php echo $val->usdcost;?>" placeholder="USD Cost" type="number"
                                            min="0">
										<?php }else{ ?>
                                        <input class="form-control usdcost" id="usdcost" name="usdcost"
                                            value="" placeholder="USD Cost" type="number"
                                            min="0">
										<?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                        <label>Discounted INR Cost</label>
										
										<?php if(isset($val->discounted_cost)){ ?>
                                        <input class="form-control discounted_cost" id="discounted_cost"
                                            name="discounted_cost" value="<?php echo $val->discounted_cost;?>"
                                            placeholder=" Discounted Cost" type="number" min="0">
										<?php }else{ ?>
										<input class="form-control discounted_cost" id="discounted_cost"
                                            name="discounted_cost" value=""
                                            placeholder=" Discounted Cost" type="number" min="0">
										<?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                        <label>Discounted USD Cost</label>
										
										<?php if(isset($val->discounted_usd_cost)){ ?>
                                        <input class="form-control discounted_usd_cost" id="discounted_usd_cost"
                                            name="discounted_usd_cost" value="<?php echo $val->discounted_usd_cost;?>"
                                            placeholder=" Discounted USD Cost" type="number" min="0">
										<?php }else{ ?>
                                        <input class="form-control discounted_usd_cost" id="discounted_usd_cost"
                                            name="discounted_usd_cost" value=""
                                            placeholder=" Discounted USD Cost" type="number" min="0">
										<?php } ?>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <label for="" class="rem_question btn btn-sm btn-danger">&times;</label>
                                </div>
                            </div>
                            <?php       }
                                }
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_description">Services Description<span
                                    class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor services_description"
                                    id="services_description" name="services_description"
                                    placeholder="Services Description"
                                    row="2"><?php echo $services_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_image">Service heading image<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <a href="#" id="services_image_btn" name="services_image" class="services_image_btn"
                                    data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="services_image" name="services_image"
                                    value="<?php echo $services_image;?>" type="hidden">
                                <img class="img-preview1" src="<?php echo base_url().'uploads/'.$services_image;?>"
                                    alt="" style="width: 150px;">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="control-label col-md-2" for="services_image">Service detail image<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <a href="#" id="services_image_btn" name="services_image" class="services_image_btn"
                                    data-toggle="modal" data-target="#browseImage2">Upload Image</a>
                                <input class="form-control" id="services_detail_image" name="services_detail_image"
                                    value="<?php echo $services_detail_image;?>" type="hidden">
                                <img class="img-preview2"
                                    src="<?php echo base_url().'uploads/'.$services_detail_image;?>" alt=""
                                    style="width: 150px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Detail Image Position<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="detail_image_position" name="detail_image_position">
                                    <option value="">Select detail image position</option>
                                    <option value="right"
                                        <?php if($detail_image_position=='right'){echo 'selected'; }?>>Right</option>
                                    <option value="left" <?php if($detail_image_position=='left'){echo 'selected'; }?>>
                                        Left</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="service_step_image">Service process step
                                image<span class="required">*</span></label>
                            <div class="col-md-5">
                                <a href="#" id="services_image_btn" name="service_step_image" class="services_image_btn"
                                    data-toggle="modal" data-target="#browseImage3">Upload Image</a>
                                <input class="form-control" id="service_step_image" name="service_step_image"
                                    value="<?php echo $service_step_image;?>" type="hidden">
                                <img class="img-preview3" src="<?php echo base_url().'uploads/'.$service_step_image;?>"
                                    alt="" style="width: 150px;">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="disc_logo_percent"> Discount logo % <span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="disc_logo_percent" name="disc_logo_percent"
                                    value="<?php echo $disc_logo_percent;?>" placeholder="show On logo homepage"
                                    type="number" min="0" max="100">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_position">Position<span
                                    class="required">*</span> Higher will come first</label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_position" name="services_position"
                                    placeholder="services_position" value="<?php echo $services_position;?>"
                                    type="number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">status<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="status" name="status">
                                    <option value="active" <?php if($status=='active'){echo 'selected'; }?>>Active
                                    </option>
                                    <option value="old" <?php if($status=='old'){echo 'selected'; }?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question label<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_label" name="q_label" placeholder="Question Label"
                                    value="<?php echo $q_label;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question Placeholder<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_placeholder" name="q_placeholder"
                                    placeholder="Question Placeholder" value="<?php echo $q_placeholder;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question Info<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_info" name="q_info" placeholder="Question Info"
                                    value="<?php echo $q_info;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1">First question label<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="is_question1" name="is_question1"
                                    value="<?php echo $is_question1;?>">
                                    <option value="yes" <?php if($is_question1 == 'yes' ) echo 'selected';?>>yes
                                    </option>
                                    <option value="no" <?php if($is_question1 == 'no' ) echo 'selected';?>>no</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1"><span class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q1_label" name="q1_label" placeholder="Label"
                                    value="<?php echo $q1_label;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1"><span class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q1_placeholder" name="q1_placeholder"
                                    placeholder="placeholder" value="<?php echo $q1_placeholder;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2">Second question label<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="is_question2" name="is_question2"
                                    value="<?php echo $is_question2;?>">
                                    <option value="yes" <?php if($is_question2 == 'yes' ) echo 'selected';?>>yes
                                    </option>
                                    <option value="no" <?php if($is_question2 == 'no' ) echo 'selected';?>>no</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2"><span class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q2_label" name="q2_label" placeholder="label"
                                    value="<?php echo $q2_label;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2"><span class="required"></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q2_placeholder" name="q2_placeholder"
                                    placeholder="placeholder" value="<?php echo $q2_placeholder;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="extra_description">Extra Description<span
                                    class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor extra_description"
                                    id="extra_description" name="extra_description" placeholder="extra_description"
                                    row="2"><?php echo $extra_description;?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="faq_category">Faq Category ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="faq_category" name="faq_category"
                                    value="<?php echo $faq_category ; ?>" placeholder="Enter faq category">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="about_content_id">Footer Aboutus Content ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="about_content_id" name="about_content_id"
                                    value="<?php echo $about_content_id; ?>" placeholder="Enter Content ID ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="banner_id">Banner ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_id" name="banner_id"
                                    value="<?php echo $banner_id ; ?>" placeholder="Enter banner_id">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block submit" type="submit" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image1"
                action="<?php echo base_url(); ?>configure_access/upload_image" method="post"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image2"
                action="<?php echo base_url(); ?>configure_access/upload_image" method="post"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image3"
                action="<?php echo base_url(); ?>configure_access/upload_image" method="post"
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    $('body').on('click', '.add_question', function() {

        var input = '<div class="row">';
        input += '<div class="col-sm-2"></div>';
        input += '<div class="col-sm-1">';
        input += '<div class="form-group inline_input">';
        input += '<label >Heading</label>';
        input +='<input type="text" class="form-control s_heading" id="s_heading" name="s_heading" placeholder="Heading" value="">';
        input += '</div>';
        input += '</div>';
        input += '<div class="col-sm-2">';
        input += '<div class="form-group inline_input">';
        input += '<label >Write Description</label>';
        input +='<input type="text" class="form-control description" id="description" name="description" placeholder="Write Description" value="">';
        input += '</div>';
        input += '</div>';
        input += '<div class="col-sm-1">';
        input += '<div class="form-group inline_input">';
        input += '<label >INR Cost</label>';
        input +='<input class="form-control cost" id="cost" name="cost" value="" placeholder="Cost" type="number" min="0">';
        input += '</div>';
        input += '</div>';
        input += '<div class="col-sm-1">';
        input += '<div class="form-group inline_input">';
        input += '<label >USD Cost</label>';
        input +='<input class="form-control usdcost" id="usdcost" name="usdcost" value="" placeholder="USD Cost" type="number" min="0">';
        input += '</div>';
        input += '</div>';
        input += '<div class="col-sm-2">';
        input += '<div class="form-group inline_input">';
        input += '<label >Discounted INR Cost</label>';
        input +='<input class="form-control discounted_cost" id="discounted_cost" name="discounted_cost" value="" placeholder=" Discounted Cost" type="number" min="0">';
        input += '</div>';
        input += ' </div>';
        input += '  <div class="col-sm-2">';
        input += '<div class = "form-group inline_input" >';
        input += '<label >Discounted USD Cost</label>';
        input +='<input class = "form-control discounted_usd_cost" id = "discounted_usd_cost" name = "discounted_usd_cost" value = "" placeholder = " Discounted USD Cost" type = "number" min = "0" >';
        input += '</div>';
        input += '</div>';
        input += '<div class="col-sm-1">';
        input += '<label for="" class="rem_question btn btn-sm btn-danger" >&times;</label>';
        input += '</div>';
        input += '</div>';
        $('.add_inputs').append(input);

    });
    $('body').on('click', '.rem_question', function() {

        $(this).closest('.row').remove();

    });





});
</script>