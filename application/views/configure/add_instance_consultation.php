<script src="<?php echo base_url(); ?>assets/js/instance_consultation.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<?php 

	
	if($instance){
		$instance_price_before = $instance[0]['instance_price_before'];
		$instance_price_inr = $instance[0]['instance_price_inr'];
		$instance_price_d = $instance[0]['instance_price_d'];
		$instance_pic = $instance[0]['instance_pic'];
		$instance_detail = $instance[0]['instance_detail'];
		$instance_id = $instance[0]['instance_id'];
		$instance_type = $instance[0]['instance_type'];
		$instance_type_duration = $instance[0]['instance_type_duration'];
		
	}else{
		$instance_price_before = 0;
		$instance_price_inr = 0;
		$instance_price_d = 0;
		
		
		$instance_pic = '';
		$instance_detail = '';
		$instance_id = 0;
		$instance_type = '';
		$instance_type_duration = '';

		
		
	}
	
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add Instance Consultation</h1>
		<a href="<?php echo base_url();?>configure_access/edit_instance_consultation" class="btn btn-default pull-right addAds">View All Consultation</a>
    </div>
    <div class="row" style="min-height: 800px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_instance_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_instance_form" method="post" class="form-horizontal">
					
						<input type="hidden" id="user_id" name="user_id" value="<?php echo $this->session->userdata('userID');?>">
						<input type="hidden" id="instance_id" name="instance_id" value="<?php echo $instance_id ;?>">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="instance_price_before">Price Before discount (INR)<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" min="0" class="form-control" id="instance_price_before" name="instance_price_before" value="<?php echo $instance_price_before;?>" placeholder="Price Before discount">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_price_inr">Price INR<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="instance_price_inr" name="instance_price_inr" value="<?php echo $instance_price_inr;?>" placeholder="Price INR">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_price_d">Price $<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="instance_price_d" name="instance_price_d" value="<?php echo $instance_price_d;?>" placeholder="Price $">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_detail">Detail<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor instance_detail" id="instance_detail" name="instance_detail" placeholder="Detail" row="2"><?php echo $instance_detail;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_pic">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="instance_image_btn" name="instance_pic" class="instance_image_btn" data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="instance_pic" name="instance_pic" value="<?php echo $instance_pic;?>" type="hidden">
								<img class="img-preview1" src="<?php echo base_url().'uploads/'.($instance[0]['instance_pic']);?>" alt="" style="width: 150px;">
							</div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_type">Type<span class="required">*</span></label>
                            <div class="col-md-5"> 
								<select class="form-control	" id="instance_type" name="instance_type">
								<option value="">Select Type</option>
								<option value="question"<?php if($instance_type == 'question'){ echo 'selected';} ?>>Question</option>
								<option value="minutes" <?php if($instance_type == 'minutes'){ echo 'selected';} ?>>Mintues</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_type_duration">Type Duration<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="instance_type_duration" name="instance_type_duration" value="<?php echo $instance_type_duration;?>" placeholder=" Duration / No of Question">
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="image" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

