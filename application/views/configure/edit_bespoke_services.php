<script src="<?php echo base_url(); ?>assets/js/services.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
.service_photo{
	width: 60px;
}

</style>
 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of All Bespoke Services</h1>
		<a href="<?php echo base_url();?>configure_access/add_birth_stone" class="btn btn-default pull-right addAds">Add Birth Stone</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_services"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($services == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Services Name</th><th class="text-center">Services Type</th><th class="text-center">Services Main Description</th><th class="text-center">Services Description</th><th class="text-center">Col</th><th class="text-center">Services Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($services as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['services_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['services_type'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['services_main_description'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['services_description'] . '</td>';
									$content .= '<td class="text-center">' . $value['col'] . '</td>';
                                    $content .= '<td class="text-center"><a name=' . $value['service_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="#"><img class="service_photo" src="' . base_url() . 'uploads/' . $value['services_image'] . '"></a></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_bespoke_services?service_id='. $value['service_id'] . '"  name=' . $value['service_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_services1"  name=' . $value['service_id'] . ' value=""><span class="label label-danger">Remove Service</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_services_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Services</h4>
                    <input type="hidden" name="edit_services_id" id="edit_services_id" value=""/>     
                </div>
                <div class="modal-body">
                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_services_name">Services Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_services_name" name="edit_services_name" placeholder="Services Name" type="text">
                        </div>
                    </div>	 
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_services_type">Services Type<span class="required">*</span></label>
                        <div class="col-md-9">
                            <select class="form-control"  id="edit_services_type" name="edit_services_type" >
								<option value="0">Select type of service</option>
								<option value="main">Main</option>									
								<option value="other">Other</option>
							</select>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_services_main_description">Services Main Description<span class="required">*</span></label>
                        <div class="col-md-9">
							<textarea class="form-control" id="edit_services_main_description" name="edit_services_main_description" placeholder="Services Main Description" row="2"></textarea>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_services_description">Services Description<span class="required">*</span></label>
                        <div class="col-md-9">
							<textarea class="form-control " id="edit_services_description" name="edit_services_description" placeholder="Services Description" row="2"></textarea>
                        </div>
                    </div>					
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="image_service_id" id="image_service_id" value=""/>     
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>