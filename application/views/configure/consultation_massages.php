<script src="https://cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
<script>		
    $(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
	});
</script>
<?php 

	if($massages){
		//var_dump($massages);
		$massage_id = $massages[0]['massage_id'];
		$default_massage = $massages[0]['default_massage'];
		$paid_massage = $massages[0]['paid_massage'];
		$custom_message = $massages[0]['custom_message'];
		$custom_message1 = $massages[0]['custom_message1'];
		$custom_message_paid = $massages[0]['custom_message_paid'];
		$start_date = $massages[0]['start_date'];
		$end_date = $massages[0]['end_date'];
		$vastu_default_msg = $massages[0]['vastu_default_msg'];
		$vastu_leave_msg = $massages[0]['vastu_leave_msg'];
		
	}else{
		$massage_id = '';
		$default_massage = '';
		$paid_massage = '';
		$custom_message = '';
		$custom_message1 = '';
		$custom_message_paid = '';
		$start_date = '';
		$end_date = '';	
        $vastu_default_msg = '';
        $vastu_leave_msg = '';
	}
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Consultation Massages</h1>
		
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="headerMsg"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="massages_form" method="post" class="form-horizontal">
					
						<input type="hidden" id="massage_id" name="massage_id" value="<?php echo $massage_id;?>">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="default_massage">Default Massage<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control default_massage" row="3" id="default_massage" name="default_massage" placeholder="Default Massage"><?php echo $default_massage;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="paid_massage">Paid Massage<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control paid_massage" row="3" id="paid_massage" name="paid_massage" placeholder="Paid Massage"><?php echo $paid_massage;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="custom_message">Custom Message Free<span class="required">*</span></label>
                            <div class="col-md-8">
                               <textarea class="form-control custom_message" row="3" id="custom_message" name="custom_message" placeholder="Custom Message"><?php echo $custom_message;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="custom_message">Custom Message Free 1<span class="required">*</span></label>
                            <div class="col-md-8">
                               <textarea class="form-control custom_message" row="3" id="custom_message1" name="custom_message1" placeholder="Custom Message "><?php echo $custom_message1;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="custom_message_paid">Custom Message Paid<span class="required">*</span></label>
                            <div class="col-md-8">
                               <textarea class="form-control custom_message_paid" row="3" id="custom_message_paid" name="custom_message_paid" placeholder="Custom Message"><?php echo $custom_message_paid;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="vastu_default_msg">Vastu Default Message<span class="required">*</span></label>
                            <div class="col-md-8">
                               <textarea class="form-control vastu_default_msg" row="3" id="vastu_default_msg" name="v" placeholder="Vastu Default Message"><?php echo $vastu_default_msg;?></textarea>
                            </div>
                        </div>
                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="vastu_leave_msg">Vastu Leave Message<span class="required">*</span></label>
                            <div class="col-md-8">
                               <textarea class="form-control vastu_leave_msg" row="3" id="vastu_leave_msg" name="vastu_leave_msg" placeholder="Vastu Leave Message"><?php echo $vastu_leave_msg;?></textarea>
                            </div>
                        </div>

						<div class="form-group">
                            <label class="control-label col-md-2" for="start_date">Start Date<span class="required">*</span></label>
                            <div class="col-md-8">
                               <input class="form-control date" id="start_date" name="start_date" value="<?php echo $start_date;?>" placeholder="Start Date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 " for="end_date">End Date<span class="required">*</span></label>
                            <div class="col-md-8">
                                <input class="form-control date" id="end_date" name="end_date" value="<?php echo $end_date;?>" placeholder="End Date">
                            </div>
                        </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script> 
$('document').ready(function(){
	CKEDITOR.replace('default_massage');
	CKEDITOR.replace('paid_massage');
	CKEDITOR.replace('custom_message');
	CKEDITOR.replace('custom_message1');
	CKEDITOR.replace('custom_message_paid');
    CKEDITOR.replace('vastu_default_msg');
    CKEDITOR.replace('vastu_leave_msg');
	//-----------------------------------------------------------------------
    /* 
     * validation of massages_form
     */
	$('#massages_form').validate({
		
		ignore: [],
        rules: {
			default_massage: {
                required: true,
            },
			paid_massage: {
                required: true,
            },
			custom_message: {
                required: true,
            },
			custom_message1: {
                required: true,
            },
			custom_message_paid: {
                required: true,
            },
			start_date: {
                required: true,
            },
			end_date: {
                required: true,
            },
		},
		 messages: {
			default_massage: {
                required: "This Field is required.",
            },
			paid_massage: {
                required: "This Field is required.",
            },
			custom_message: {
                required: "This Field is required.",
            },
			custom_message1: {
                required: "This Field is required.",
            },
			custom_message_paid: {
                required: "This Field is required.",
            },
			start_date: {
                required: "This Field is required.",
            },
			end_date: {
                required: "This Field is required.",
            },
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		}, 
		submitHandler: function (form) {
			
			//$('#blog_form').find('button[type="submit"]').prop('disabled',true);
			
			var massage_id = $('#massage_id').val();
			
            var custom_message = CKEDITOR.instances['custom_message'].getData();
            var custom_message1 = CKEDITOR.instances['custom_message1'].getData();
            var custom_message_paid = CKEDITOR.instances['custom_message_paid'].getData();
            var paid_massage = CKEDITOR.instances['paid_massage'].getData();
			var default_massage = CKEDITOR.instances['default_massage'].getData();

            var vastu_default_msg = CKEDITOR.instances['vastu_default_msg'].getData();
            var vastu_leave_msg = CKEDITOR.instances['vastu_leave_msg'].getData();
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();
 
            $.post(APP_URL + 'configure_access/update_consultation_massages', {
                massage_id: massage_id,
                default_massage: default_massage,
                paid_massage: paid_massage,
                custom_message: custom_message,
                custom_message1: custom_message1,
                custom_message_paid: custom_message_paid,
                start_date: start_date,
                end_date: end_date,
                vastu_default_msg : vastu_default_msg,
                vastu_leave_msg : vastu_leave_msg,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					if(massage_id!=0){
						message = "service has been updated successfully!";
						$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/services_list'>Refresh Page</a></div>");
						$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
							$('#headerMsg').remove();
							window.location.reload();
						});
					}else{
						$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'>Refresh Page</a></div>");
						$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
							$('#headerMsg').remove();
							window.location.reload();
						});
					}
                } else if (response.status == 201) {
					$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'>Refresh Page</a></div>");
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
});
	
</script>
