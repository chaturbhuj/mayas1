<script src="<?php echo base_url(); ?>assets/js/add_database.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add Database</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_student_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_database_form" method="post" class="form-horizontal">
						<input class="form-control" id="student_id" name="student_id" value="0" placeholder="Student Name" type="hidden">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="student_name">Student Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="student_name" name="student_name" placeholder="Student Name" type="text">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="student_age">Student Age<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="student_age" name="student_age" placeholder="Student's Age" type="text">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="mothers_name">Mother's Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="mothers_name" name="mothers_name" placeholder="Mother's Name" type="text">
                            </div>
                        </div>
						

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
