<script src="<?php echo base_url(); ?>assets/js/instance_consultation.js"></script> 
<style>.blog_photo{   width: 60px;}</style>
<div class="container-fluid main-content">
<div class="page-title">
        <h1>List of All Instance Consultation</h1>
		<a href="<?php echo base_url();?>configure_access/add_instance_consultation" class="btn btn-default pull-right addAds">Add Instance Consultation</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_instance"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php 
                            if ($instance_consultation == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th>';
								  $content .= '<th class="text-center">Price INR</th>';
								  $content .= '<th class="text-center">Price$</th>';
								 
								  $content .= '<th class="text-center">Type</th>';
								  $content .= '<th class="text-center">Type Duration</th>';
								  // $content .= '<th class="text-center">Detail</th>';
								  $content .= '<th class="text-center">Action</th></tr></thead>';
								    $content .= '<tbody>';
                                $i = 1;
                                foreach ($instance_consultation as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['instance_price_inr'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['instance_price_d'] . '</td>';
									
                                    $content .= '<td class="text-center">' . $value['instance_type'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['instance_type_duration'] . '</td>';
                                  // $content .= '<td class="text-center"></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_instance_consultation?instance_id='.$value['instance_id'].'" class="" name=' . $value['instance_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_instance_consultation"  name=' . $value['instance_id'] . ' value=""><span class="label label-danger">Remove </span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_instance_form" method="post">
                <input type="hidden" id="edit_instance_id" name="edit_instance_id" value="">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="edit_instance_price_inr">Price INR<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="edit_instance_price_inr" name="edit_instance_price_inr" value="" placeholder="Price INR">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="edit_instance_price_d">Price $<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="edit_instance_price_d" name="edit_instance_price_d" value="" placeholder="Price $">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="edit_instance_detail">Detail<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor edit_instance_detail" id="edit_instance_detail" name="edit_instance_detail" placeholder="Detail" row="2"></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="edit_instance_type">Type<span class="required">*</span></label>
                            <div class="col-md-5"> 
								<select class="form-control	" id="edit_instance_type" name="edit_instance_type">
								<option value="">Select Type</option>
								<option value="question">Question</option>
								<option value="minutes">Mintues</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="edit_instance_type_duration">Type Duration<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="edit_instance_type_duration" name="edit_instance_type_duration" value="" placeholder="Type Duration">
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                       
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="image_service_id" id="image_service_id" value=""/>     
                    <input type="hidden" value="image" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
