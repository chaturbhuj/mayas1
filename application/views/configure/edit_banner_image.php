<script src="<?php echo base_url(); ?>assets/js/upload_banner_image.js"></script> 
<style> img{width:100px;} </style>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_banner_images"></div>
                    <div id="edit_banner_image_table">
                        <h2>List of All Banner Images </h2>
                        <div id="table_view">             
                            <?php 
								if ($banner_images == 0) {
                                echo 'No record found into database';
                            } else {
								$content = '';
								 $content .= '<table class="table table-striped table-bordered table-hover" id="banner_image_details">';
								 $content .= '<thead>';
								 $content .= '<tr>';
								 $content .= '<th class="text-center">S.No.</th>';
								 $content .= '<th class="text-center">Image Banner Page Name</th>';
								 $content .= '<th class="text-center">Image</th>';
								 $content .= '<th class="text-center">Action</th>';
								 $content .= '</tr>';
								 $content .= '</thead>';
								 $content .= '<tbody>';
								 $i = 1;
								 foreach ($banner_images as $value) {
									 $content .= '<tr class="darker-on-hover">';
									 $content .= '<td class="text-center">'.$i.'</td>';
									 $content .= '<td class="text-center">'.$value['banner_page_type'].'</td>';
									 $content .= '<td class="text-center">';
									 $content .= '<a class="editBrowseImageBtn" name="'.$value['banner_image_id'].'" data-toggle="modal" data-target="#editBrowseImage" href="#">';
									 $content .= '<img class="banner_image_photo" src="'.base_url().'uploads/'.$value['banner_image'].'">';
									 $content .= '</a>';
									 $content .= '</td>';
									 $content .= '<td class="text-center">';
									 $content .= '<a href="#" class="edit_banner_image_detail" data-toggle="modal" data-target="#my_banner_image_edit" name="'.$value['banner_image_id'].'" value=""><span class="label label-success">Edit</span>';
									 $content .= '</a>';
									 $content .= '&nbsp;&nbsp;<a href="#" class="remove_banner_image"  name="'.$value['banner_image_id'].'" value=""><span class="label label-danger">Remove Banner Image</span></a>';
									 $content .= '</td>';
									 $content .= '</tr>';
									 $i++;
								}
								 $content .= '</tbody>';
								 $content .= '</table>';
								 echo $content;
							}
								
							?>
						
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="banner_image_id" id="banner_image_id" value=""/>     
                    <input type="hidden" value="banner_image" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--------------------------------------------Model -------------------------------------------------------------------------->
<div class="modal fade" id="my_banner_image_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_banner_image_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Banner Image Detail</h4>
                    <input type="hidden" name="edit_banner_image_id" id="edit_banner_image_id" value=""/>     
                </div>
                <div class="modal-body">
                   <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_banner_page_type">Image Banner Page Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <select class="form-control"  id="edit_banner_page_type" name="edit_banner_page_type" >
								<option value="">Select Banner Page</option>
								<option value="free_consultaion">Free Consultaion</option>									
								<option value="premium_consultaion">Premium Consultaion</option>
								<option value="free_gemstone_consultaion">Free Gemstone Consultaion</option>
							</select>
                        </div>
                    </div>
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
