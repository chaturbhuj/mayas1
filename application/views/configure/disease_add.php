
<?php 
	
	if($disease_data){
		$diease_id = $disease_data[0]['diease_id'];
		$diease_name = $disease_data[0]['diease_name'];
			
	}else{
		$diease_id =0;
		$diease_name = "";
	}
	
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Disease</h1>
		<a href="<?php echo base_url();?>configure_access/disease_list" class="btn btn-default pull-right addAds">View All Disease</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="errHeadMsg"> </div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_diease_form" method="post" class="form-horizontal">
					
                        <input type="hidden" id="diease_id" name="diease_id" value="<?php echo $diease_id;?>">
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_name">disease Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="diease_name" name="diease_name" value="<?php echo $diease_name;?>" placeholder="disease Name">
                            </div>
                        </div>
																		
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('document').ready(function(){
  $('#add_diease_form').validate({
    ignore:[],
        rules: {
            diease_name: {
                required: true,
            },
        },
        messages: {
            diease_name: {
                required: "disease name is required",
            },
        },

        submitHandler: function (form) {
      $.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
          
           var diease_id = $('#diease_id').val();
           var diease_name = $('#diease_name').val();
		   console.log(diease_id);
            
            $.post(APP_URL + 'configure_access/update_disease', {
                diease_id: diease_id,
                diease_name: diease_name,                
            }, 
            function (response) {
        $.unblockUI();
        $("html, body").animate({scrollTop: 0}, "slow");
                $('#errHeadMsg').empty();
                if (response.status == 200) {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
            $('#errHeadMsg').empty();
            window.location.href = APP_URL+'configure_access/disease_list';
          });
         } else {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
            $('#errHeadMsg').empty();
          });
        }
            }, 'json');
            return false;
        }
    });
});
</script>

