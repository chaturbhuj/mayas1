<script src="<?php echo base_url(); ?>assets/js/sub_menu_products.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Sub Menu products</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_sub_menu_products_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="addSubMenuProductsForm" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sub_menu_products_name">Sub Menu products Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="sub_menu_products_name" name="sub_menu_products_name" placeholder="Sub Menu products Name" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="sub_menu_products_navi">Sub Menu products Navigation URL<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="sub_menu_products_navi" name="sub_menu_products_navi" placeholder="Sub Menu products Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="sub_menu_products_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="product_image_btn" name="product_image_btn" class="product_image_btn" data-toggle="modal" data-target="#browseImage">Upload Image</a>
                                <input class="form-control" id="sub_menu_products_image" name="sub_menu_products_image" type="hidden">
                                <img class="img-preview" src="" alt="" style="width: 150px;">     
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="sub_menu_product" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
