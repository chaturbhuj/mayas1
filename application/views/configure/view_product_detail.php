


<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>Product Detail</h2>
                       <form id="consultation_form" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" >Product Detail</h4>
						</div>
						<div class="modal-body">
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_name">Product Name: </label>
								<div class="col-md-7">
									<span class="full_namel" id="product_name"  name="product_name"><?php echo $product_list[0]['product_name'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_categories">Product Categories: </label>
								<div class="col-md-7">
									<span class="full_namel" id="product_categories"  name="product_categories"><?php echo $product_list[0]['product_categories'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_id">Product ID: </label>
								<div class="col-md-7">
									<span class="full_namel" id="product_id"  name="product_id"><?php echo $product_list[0]['product_id'];?></span>
								</div>
							</div>
							
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_price_ind">Product Price INR: </label>
								<div class="col-md-7">
									<span class="full_namel" id="product_price_ind"  name="product_price_ind"><?php echo $product_list[0]['product_price_ind'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_price_usd">Product Price USD: </label>
								<div class="col-md-7">
									<span class="full_namel" id="product_price_usd"  name="product_price_usd"><?php echo $product_list[0]['product_price_usd'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="product_image_list">Product Image : </label>
								<div class="col-md-7">
									<?php $image = explode(",",  $product_list['0']['product_image_list']); ?>
									<span class="full_namel" id="product_image_list"  name="product_image_list"><img class="img-preview" src="<?php echo base_url();?>uploads/<?php echo  $image[0]; ?>" alt=""></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="qty">Product Quantity: </label>
								<div class="col-md-7">
									<span class="full_namel" id="qty"  name="qty"><?php echo $product_list[0]['qty'];?></span>
								</div>
							</div>
							
						
							
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


-->
