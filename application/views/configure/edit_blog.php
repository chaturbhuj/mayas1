<script src="<?php echo base_url(); ?>assets/js/blog.js"></script> 
<style>.blog_photo{   width: 60px;}</style>
<div class="container-fluid main-content">
<div class="page-title">
        <h1>List of All blogs</h1>
		<a href="<?php echo base_url();?>configure_access/add_blog" class="btn btn-default pull-right addAds">Add Blog</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_blog"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($blogs == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Blog Name</th><th class="teerr_edit_blogxt-center">Blog Sub Heading </th><th class="text-center">Blog Description</th><th class="text-center">Blog Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($blogs as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['blog_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['blog_sub_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['blog_description'] . '</td>';
                                    $content .= '<td class="text-center"><a class="editBrowseImageBtn" name=' . $value['blog_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="#"><img class="blog_photo" src="' . base_url() . 'uploads/' . $value['blog_image'] . '"></a></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_blog?blog_id='.$value['blog_id'].'" class="" name=' . $value['blog_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_blog"  name=' . $value['blog_id'] . ' value=""><span class="label label-danger">Remove blog</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_blog_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit blog</h4>
                    <input type="hidden" name="edit_blog_id" id="edit_blog_id" value=""/>     
                </div>
                <div class="modal-body">
                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_blog_heading">Blog Heading<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_blog_heading" name="edit_blog_heading" placeholder="Blog Heading" type="text">
                        </div>
                    </div>	 
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_blog_sub_heading">Blog Sub Heading<span class="required">*</span></label>
                        <div class="col-md-9">
                           <input class="form-control" id="edit_blog_sub_heading" name="edit_blog_sub_heading" placeholder="Blog Sub Heading" type="text">
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_blog_description">Blog Description<span class="required">*</span></label>
                        <div class="col-md-9">
							<textarea class="form-control" id="edit_blog_description" name="edit_blog_description" placeholder="Blog Description" row="2"></textarea>
                        </div>
                    </div>				
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="image_blog_id" id="image_blog_id" value=""/>     
                    <input type="hidden" value="blog" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>