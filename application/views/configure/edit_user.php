<script src="<?php echo base_url(); ?>assets/js/add_new_user.js"></script> 
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_admin_edit_user"></div>
                    <div id="edit_user_table">
                        <h2>List of All User</h2>
                        <div id="table_view">             
                            <?php
                            if ($user == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">User Name</th><th class="text-center">User Email</th><th class="text-center">Role Assigned</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($user as $value) {
                                    if ($value['user_id'] == -1) {
                                        continue;
                                    }
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['username'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['email'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['userrole'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="edit_user" data-toggle="modal" data-target="#my_user_edit" name=' . $value['user_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_user"  name=' . $value['user_id'] . ' value=""><span class="label label-danger">Remove User</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_user_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit User</h4>
                    <input type="hidden" name="edit_user_id" id="edit_user_id" value=""/>
                    <div id="edit_user_id" style="visibility: none;"></div>                    
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_name">User Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control capitaliseFirstLetter" id="edit_user_name" name="edit_user_name" placeholder="User Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_email">User Email<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_user_email" name="edit_user_email" placeholder="User Email" type="email">
                        </div>
                    </div>
                    <div id="err_edit_user_email" class="col-md-offset-3 clearfix"></div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_assign_role_to_user">Assign Role to user<span class="required">*</span></label>
                        <div class="col-md-9 select_role">
                            <input class="form-control" id="edit_assign_role_to_user" name="edit_assign_role_to_user" placeholder="Assign Role to user" type="text">
                        </div>
                    </div><div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
