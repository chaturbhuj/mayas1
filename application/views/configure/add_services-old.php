<script src="<?php echo base_url(); ?>assets/js/services.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 
	
	if($services){
		$service_id = $services[0]['service_id'];
		$services_name = $services[0]['services_name'];
		$services_type = $services[0]['services_type'];
		$cost = $services[0]['cost'];
		$consultant_type = $services[0]['consultant_type'];
		$number_of_questions = $services[0]['number_of_questions'];
		$status = $services[0]['status'];
		$services_main_description = $services[0]['services_main_description'];
		$services_description = $services[0]['services_description'];
		$services_image = $services[0]['services_image'];
		$number_of_questions_2 = $services[0]['number_of_questions_2'];
		$cost_2 = $services[0]['cost_2'];
		$premium_type = $services[0]['premium_type'];
		
	}else{
		$service_id = 0;
		$number_of_questions_2 = 0;
		$cost_2 = 0;
		$premium_type = '';
		$services_name = '';
		$services_type = '';
		$cost = '';
		$consultant_type = '';
		$number_of_questions = '';
		$attachments = '';
		$status = '';
		$services_main_description = '';
		$services_description = '';
		$services_image = '';
		if(isset($_GET['service_id'])){
			$service_id  = $_GET['service_id'];
			
		}
		
		
	}
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Services</h1>
		<a href="<?php echo base_url();?>configure_access/edit_services" class="btn btn-default pull-right addAds">View All Services</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_services_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_services_form" method="post" class="form-horizontal">
					
                        <input type="hidden" id="service_id" name="service_id" value="<?php echo $service_id;?>">
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_name">Services Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_name" name="services_name" value="<?php echo $services_name;?>" placeholder="Services Name" type="text">
                            </div>
                        </div>
						<div hidden class="form-group">
                            <label class="control-label col-md-2" for="services_type">Services Type<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="services_type" name="services_type" value="" >
									<option value="0">Select type of service</option>
									<option value="main" <?php if($services_type == 'main' ) echo 'selected';?> >Main</option>									
									<option value="other" <?php if($services_type == 'other' ) echo 'selected';?>>Other</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
							<label for="consultant_type" class="col-md-2 control-label">Select type of consultation<span class="required">*</span></label>
							<div class="col-md-5">
								<select name="consultant_type" id="consultant_type" class="form-control consultant_type" >
									<option value="" name="">Select the type of free consultation</option>
									<option value="Career" <?php if($consultant_type == 'Career' ) echo 'selected';?> name="career">Question Related to Career</option>
									<option value="BirthStone" <?php if($consultant_type == 'BirthStone' ) echo 'selected';?> name="birth_stone">Question Related to Birth Stone</option>
									<option value="HoroscopeQuery" <?php if($consultant_type == 'HoroscopeQuery' ) echo 'selected';?>  name="horoscope">Question Related to Horoscope Query</option>
									<option value="MatchMaking" <?php if($consultant_type == 'MatchMaking' ) echo 'selected';?> name="match_making">Question Related to Match Making</option>
								</select>
							</div>
						</div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_main_description">Services Main Description<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="services_main_description" name="services_main_description" placeholder="Services Main Description" row="2"><?php echo $services_main_description;?></textarea>
                            </div>
                        </div>
						<div  class="form-group">
                            <label class="control-label col-md-2" for="premium_type">Services Type<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="premium_type" name="premium_type" value="" >
									<option value="">Select type of premium service</option>
									<option value="question"<?php if($premium_type == 'question' ) echo 'selected';?>  >Question</option>									
									<option value="minutes" <?php if($premium_type == 'minutes' ) echo 'selected';?> >Minutes</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="number_of_questions">Number Of Questions<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="number_of_questions" name="number_of_questions" placeholder="Number Of Questions" value="<?php echo $number_of_questions;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="cost">Cost<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="cost" name="cost" value="<?php echo $cost;?>" placeholder="Cost" type="number">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="number_of_questions_2">Number Of Questions<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="number_of_questions_2" name="number_of_questions_2" placeholder="Number Of Questions " value="<?php echo $number_of_questions_2;?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="cost_2">Cost<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="cost_2" name="cost_2" value="<?php echo $cost_2;?>" placeholder="Cost" type="number">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_description">Services Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor services_description" id="services_description" name="services_description" placeholder="Services Description" row="2"><?php echo $services_description;?></textarea>
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="services_image_btn" name="services_image_btn" class="services_image_btn" data-toggle="modal" data-target="#browseImage">Upload Image</a>
                                <input class="form-control" id="services_image" name="services_image" value="<?php echo $services_image;?>" type="hidden">
								
								<?php if($services_image != ''){
									echo '<img class="img-preview" src="'.base_url().'uploads/'.$services_image.'" alt="" style="width: 150px;">';
								}else{
									echo '<img class="img-preview" src="" alt="" style="width: 150px;">';
								}?>
								
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

