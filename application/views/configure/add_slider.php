 <script src="<?php echo base_url(); ?>assets/js/slider.js"></script>

<?php 


	if($slider_details){
		$heading			=	'Edit Slider Image';
		$slider_id 			= 	$slider_details[0]['slider_id'];
		$slider_info 		= 	$slider_details[0]['slider_info'];
		$slider_navi 		= 	$slider_details[0]['slider_navi'];
		$slider_navi_link 	= 	$slider_details[0]['slider_navi_link'];
		$slider_input1 		= 	$slider_details[0]['slider_input1'];
		$slider_input2 		= 	$slider_details[0]['slider_input2'];
		$slider_input3 		= 	$slider_details[0]['slider_input3'];
		$slider_image 		= 	$slider_details[0]['slider_image'];
		$status 			= 	$slider_details[0]['status'];
		$content_align 		= 	$slider_details[0]['content_align'];
		$slider_position 		= 	$slider_details[0]['slider_position'];
		
		
	}else{
		$heading			=	'Add New Slider Image';
		$slider_id  		= 	0;
		$slider_info 		= 	'';
		$slider_navi 		= 	'';
		$slider_navi_link 	= 	'';
		$slider_input1 		= 	'';
		$slider_input2 		= 	'';
		$slider_input3 		= 	'';
		$slider_image 		= 	'';
		$status 			= 	'';
		$content_align 		= 	'';
		$slider_position 		= 	'';
		
	}

?>

 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading; ?></h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_slider_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_slider_form" method="post" class="form-horizontal">
					<input type="hidden" name="slider_id" id="slider_id" value="<?php echo $slider_id;?>">
                        <div class="form-group">
						     
                            <label class="control-label col-md-2" for="slider_info">Slider Info<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_info" name="slider_info" placeholder="Slider Information" value="<?php echo $slider_info;?>" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_navi">Slider Navigation Text<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_navi" name="slider_navi" placeholder="Slider Navigation Text"  value="<?php echo $slider_navi;?>" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_navi_link">Slider Navigation Link<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_navi_link" name="slider_navi_link" placeholder="Slider Navigation URL: http://www.xxx.xx/x" value="<?php echo $slider_navi_link;?>"  type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input1">Slider Input Text1<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_input1" name="slider_input1" placeholder="Slider input1"  value="<?php echo $slider_input1;?>" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input2">Slider Input Text2<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_input2" name="slider_input2" placeholder="Slider Input" value="<?php echo $slider_input2;?>"  type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Slider Input Text3<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_input3" name="slider_input3" placeholder="Slider Input" value="<?php echo $slider_input3;?>"  type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Content Alignment<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="content_align" name="content_align">
									<option value="">Select</option>
									<option value="left" <?php if($content_align=='left'){echo 'selected'; }?>>Left</option>
									<option value="center" <?php if($content_align=='center'){echo 'selected'; }?>>Center</option>
									<option value="right" <?php if($content_align=='right'){echo 'selected'; }?>>Right</option>
								</select>
                            </div>
                        </div>

						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_position">Position<span class="required">*</span> Higher will come first</label>
                            <div class="col-md-5">
                                <input class="form-control" id="slider_position" name="slider_position" placeholder="Slider_position" value="<?php echo $slider_position;?>"  type="number">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">status<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="status" name="status">
									<option value="active" <?php if($status=='active'){echo 'selected'; }?>>Active</option>
									<option value="old" <?php if($status=='old'){echo 'selected'; }?>>Inactive</option>
								</select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_image">Slider Image(1170X330)<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="slider_image_btn" name="slider_image_btn" class="slider_image_btn" data-toggle="modal" data-target="#browseImage">Upload Slider Image</a>
                                <input class="form-control" id="slider_image" name="slider_image"  value="<?php echo $slider_image;?>"  type="hidden">
                                <img class="img-preview" src=" <?php echo base_url(). 'uploads/' .$slider_image ;?>" alt="" style="width: 150px;">     
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="slider" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
