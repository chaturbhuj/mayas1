<script src="<?php echo base_url(); ?>assets/js/product.js"></script> 
<style>.product_images{  width: 100px;}</style>
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Product</h1>
		<a href="<?php echo base_url();?>configure_access/edit_product" class="btn btn-default pull-right addAds">View All Products</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_product_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_product_form" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="product_categories">Product Categories<span class="required">*</span></label>
                            <div class="col-md-5"> 
								<select class="form-control	" id="product_categories" name="product_categories">
								<option value="">select product categories</option>
								<?php
								if ($all_submenu_product_name) {
									foreach ($all_submenu_product_name as $value) {
								 echo'
									<option value="'.$value["sub_menu_products_id"].'">'.$value["sub_menu_products_name"].'</option>
								';}}?>
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="product_name">Product Name <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_name" name="product_name" placeholder="Product Name" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_description">product description <span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_description" name="product_description" placeholder="product description" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_price">product price <span class="required">*</span></label>
                            <div class="col-md-2">
                                <input class="form-control" id="product_price_ind" name="product_price_ind" placeholder="Product price ind" type="number">
                            </div>
							<div class="col-md-2">
                                <input class="form-control" id="product_price_usd" name="product_price_usd" placeholder="Product price usd" type="number">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_feature">Product feature <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_feature" name="product_feature" placeholder="Product feature" type="text">
                            </div>   </div>
							<div class="form-group">
                            <label class="control-label col-md-2" for="product_weight">Product Weight <span class="required"></span></label>
							
                            <div class="col-md-5">
                                <input class="form-control" id="product_weight" name="product_weight" placeholder="Product Weight" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_shape">Product shape <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_shape" name="product_shape" placeholder="Product shape" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_color">Product color <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_color" name="product_color" placeholder="Product color" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_clarity">Product clarity <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_clarity" name="product_clarity" placeholder="Product clarity" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_grade">Product grade <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_grade" name="product_grade" placeholder="Product grade" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_cut">Product cut <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_cut" name="product_cut" placeholder="Product cut " type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_treatment">Product treatment <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_treatment" name="product_treatment" placeholder="Product treatment" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_origin">Product origin <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_origin" name="product_origin" placeholder="Product origin" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_other_info">Product other info1 <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_other_info" name="product_other_info" placeholder="Product other info1" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_other_info2">Product other info2 <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_other_info2" name="product_other_info2" placeholder="Product other info2" type="text">
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-2" for="product_other_info3">Product other info3 <span class="required"></span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="product_other_info3" name="product_other_info3" placeholder="Product other info3" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="product_image">Gallery Image<span class="required">*</span></label>
                            <div class="col-md-5">  
                                <input id="btnAdd" type="button" value="Add" data-toggle="modal" data-target="#browseImage"/>
                                <div id="productImageContainer">
                                    <!--Textboxes will be added here -->
                                </div>
                                <input class="form-control" id="product_image" name="product_image" type="hidden">
                            </div>
                        </div>                        
						<div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="product" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
