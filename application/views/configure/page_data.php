<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/page_data.js"></script> 
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 
	if($page_data){
        $id = $page_data[0]['id'];
		$page = $page_data[0]['page'];
		$data = $page_data[0]['data'];
		$video_link = $page_data[0]['video_link'];
		$header_image= $page_data[0]['header_image'];
	}else{
        $id =0;
		$page = '';
		$data = '';
		$video_link = '';
		$header_image = '';
    }
	
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Page Data</h1>
		<a href="<?php echo base_url();?>configure_access/page_data_list" class="btn btn-default pull-right addAds">View All Page Data</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_page_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="page_data_form" method="post" class="form-horizontal">
						<input type="hidden" id="idd" name="idd" value="<?php echo $id;?>">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="page_heading">Page Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="page" name="page" value="<?php echo $page;?>" placeholder="Page Heading">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="content">Content<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor " id="data" name="data" placeholder="content" row="2"><?php echo $data;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="header_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="blog_image_btn" name="header_image" class="blog_image_btn" data-toggle="modal" data-target="#browseImage">Upload Image</a>
                                <input class="form-control" id="header_image" name="header_image" value="<?php echo $header_image;?>" type="hidden">
								
								<?php if($header_image != ''){
									echo '<img class="img-preview" src="'.base_url().'uploads/'.$header_image.'" alt="" style="width: 150px;">';
								}else{
									echo '<img class="img-preview" src="" alt="" style="width: 150px;">';
								}?>
								
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="Video Link">Video Link<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="video_link" name="video_link" value="<?php echo $video_link;?>" placeholder="Video Link">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
