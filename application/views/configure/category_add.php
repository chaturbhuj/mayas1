

<?php 
	
	if($cat_data){
		$cat_id = $cat_data[0]['cat_id'];
		$cat_name = $cat_data[0]['cat_name'];
			
	}else{
		$cat_id =0;
		$cat_name = "";
	}
	
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Category</h1>
		<a href="<?php echo base_url();?>configure_access/category_list" class="btn btn-default pull-right addAds">View All Category</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="errHeadMsg"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_category_form" method="post" class="form-horizontal">
					
                        <input type="hidden" id="cat_id" name="cat_id" value="<?php echo $cat_id;?>">
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_name">Category Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" id="cat_name" name="cat_name" value="<?php echo $cat_name;?>" placeholder="Category Name">
                            </div>
                        </div>
																		
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$('document').ready(function(){
  $('#add_category_form').validate({
    ignore:[],
        rules: {
            cat_name: {
                required: true,
            },
        },
        messages: {
            cat_name: {
                required: "category name is required",
            },
        },

        submitHandler: function (form) {
      $.blockUI({ message: '<h1><img src="'+APP_URL + 'assets/img/loading.gif" /> Just a moment...</h1>' });
          
           var cat_id = $('#cat_id').val();
           var cat_name = $('#cat_name').val();
		   console.log(cat_id);
            
            $.post(APP_URL + 'configure_access/update_category', {
                cat_id: cat_id,
                cat_name: cat_name,                
            }, 
            function (response) {
        $.unblockUI();
        $("html, body").animate({scrollTop: 0}, "slow");
                $('#errHeadMsg').empty();
                if (response.status == 200) {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
            $('#errHeadMsg').empty();
            window.location.href = APP_URL+'configure_access/category_list';
          });
         } else {
                    $('#errHeadMsg').empty();
                    $('#errHeadMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
          $("#errHeadMsg").fadeTo(2000, 500).slideUp(500, function(){
            $('#errHeadMsg').empty();
          });
        }
            }, 'json');
            return false;
        }
    });
});
</script>

