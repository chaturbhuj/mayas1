<script src="<?php echo base_url(); ?>assets/js/sub_menu_services.js"></script> 
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>List of All Sub Menu Services Item</h2>
                        <div id="table_view">             
                            <?php
                            if ($sub_menu_services == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Sub Menu Services Name</th><th class="text-center">Sub Menu Services Navigation URL</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($sub_menu_services as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['sub_menu_services_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['sub_menu_services_navi'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="edit_sub_menu_services" data-toggle="modal" data-target="#my_user_edit" name=' . $value['sub_menu_services_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_sub_menu_services"  name=' . $value['sub_menu_services_id'] . ' value=""><span class="label label-danger">Remove Sub Menu Services</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_sub_menu_services_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Sub Menu Services</h4>
                    <input type="hidden" name="edit_sub_menu_services_id" id="edit_sub_menu_services_id" value=""/>     
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_sub_menu_services_name">Sub Menu Services Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_sub_menu_services_name" name="edit_sub_menu_services_name" placeholder="Sub Menu Services Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_sub_menu_services_navi">Sub Menu Services Navigation URL<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_sub_menu_services_navi" name="edit_sub_menu_services_navi" placeholder="Sub Menu Services Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                        </div>
                    </div>
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
