<script src="<?php echo base_url(); ?>assets/js/instance_consultation.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<?php 

	//print_r ($instance_consultation);
	if($instance){
		$instance_price_inr = $instance[0]['instance_price_inr'];
		$instance_price_d = $instance[0]['instance_price_d'];
		$instance_id = $instance[0]['instance_id'];
		$instance_detail = $instance[0]['instance_detail'];
		$instance_type = $instance[0]['instance_type'];
		$instance_type_duration = $instance[0]['instance_type_duration'];
		
		
		
	}else{
		$instance_price_inr = '';
		$instance_price_d = '';
		$instance_id = 0;
		$instance_detail = '';
		$instance_type = '';
		$instance_type_duration = '';
		
		
		
		
	}
	if(isset($_GET['instance_id'])){
			$instance_id  = $_GET['instance_id'];
			
		}else{
			$instance_id = 0;
		}
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add Instance Consultation</h1>
		<a href="<?php echo base_url();?>configure_access/edit_instance_consultation" class="btn btn-default pull-right addAds">View All Consultation</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_instance_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_instance_form" method="post" class="form-horizontal">
					
						<input type="hidden" id="instance_id" name="instance_id" value="<?php echo $instance_id;?>">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="instance_price_inr">Price INR<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="instance_price_inr" name="instance_price_inr" value="<?php echo $instance_price_inr;?>" placeholder="Price INR">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_price_d">Price $<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="instance_price_d" name="instance_price_d" value="<?php echo $instance_price_d;?>" placeholder="Price $">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_detail">Detail<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor instance_detail" id="instance_detail" name="instance_detail" placeholder="Detail" row="2"><?php echo $instance_detail;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_type">Type<span class="required">*</span></label>
                            <div class="col-md-5"> 
								<select class="form-control	" id="instance_type" name="instance_type">
								<option value="">Select Type</option>
								<option value="question"<?php if($instance_type == 'question'){ echo 'selected';} ?>>Question</option>
								<option value="minutes" <?php if($instance_type == 'minutes'){ echo 'selected';} ?>>Mintues</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="instance_type_duration">Type Duration<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="instance_type_duration" name="instance_type_duration" value="<?php echo $instance_type_duration;?>" placeholder=" Duration / No of Question">
                            </div>
                        </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


