<script src="<?php echo base_url(); ?>assets/js/admin_consultation.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 
	//var_dump($consultation_Data);
	if($consultation_Data){
		$heading='Edit Free Consultation';
		$consultation_id = $consultation_Data[0]['consultation_id'];
		$faq_category = $consultation_Data[0]['faq_category'];
		$about_content_id = $consultation_Data[0]['about_content_id'];
		$banner_id = $consultation_Data[0]['banner_id'];
		$consultation_name = $consultation_Data[0]['consultation_name'];
		
		$main_description = $consultation_Data[0]['main_description'];
		$description = $consultation_Data[0]['description'];
		$c_image = $consultation_Data[0]['c_image'];
		$c_detail_image = $consultation_Data[0]['c_detail_image'];
		$detail_image_position = $consultation_Data[0]['detail_image_position'];
		
		$url = $consultation_Data[0]['url'];
		$title = $consultation_Data[0]['title'];
		$dec_meta = $consultation_Data[0]['dec_meta'];
		$keyword_meta = $consultation_Data[0]['keyword_meta'];
        $consultation_type =$consultation_Data[0]['consultation_type'];
        $q_label =  $consultation_Data[0]['q_label'];
        $q_placeholder =  $consultation_Data[0]['q_placeholder'];
        $q_info =  $consultation_Data[0]['q_info'];

        $is_question1 = $consultation_Data[0]['is_question1'];
        $q1_label = $consultation_Data[0]['q1_label'];
        $q1_placeholder = $consultation_Data[0]['q1_placeholder'];
        $is_question2 = $consultation_Data[0]['is_question2'];
        $q2_label = $consultation_Data[0]['q2_label'];
        $q2_placeholder = $consultation_Data[0]['q2_placeholder'];
		$extra_description = $consultation_Data[0]['extra_description'];
		$services_position = $consultation_Data[0]['services_position'];
		
	}else{
        $is_question1 = 'no';
        $q1_label = '';
        $q1_placeholder = '';
        $is_question2 = 'no';
        $q2_label = '';
        $q2_placeholder = '';
		$faq_category = 0;
		$about_content_id = 0;
		$banner_id = 0;


		$heading='Add New Free Consultation';
		$consultation_id = 0;
		$consultation_name = '';
		$main_description = '';
		$extra_description = '';
		$description = '';
		$c_image = '';
        $c_detail_image = '';
		$detail_image_position = '';
		
		$url = '';
		$title ='';
		$dec_meta ='';
		$keyword_meta ='';
        $consultation_type ='';

		if(isset($_GET['consultation_id'])){
			$consultation_id  = $_GET['consultation_id'];
			
		}
        $q_label =  '';
        $q_placeholder =  '';
        $q_info =  '';
        $services_position =  0;

		
		
	}
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Free Consultation</h1>
		<a href="<?php echo base_url();?>configure_access/free_consultation_list" class="btn btn-default pull-right addAds">View All Free Consultation</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_services_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_consultation_form_new" method="post" class="form-horizontal">
					
                        <input type="hidden" id="consultation_id" name="consultation_id" value="<?php echo $consultation_id;?>">
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="consultation_name">Consultation Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="consultation_name" name="consultation_name" value="<?php echo $consultation_name;?>" placeholder="Consultation Name" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="url">Url<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" style="text-transform: lowercase;" id="url" name="url" value="<?php echo $url;?>" placeholder="Consultation Url" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="title">Title<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="title" name="title" value="<?php echo $title;?>" placeholder="title" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="dec_meta">desc meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="dec_meta" name="dec_meta" value="<?php echo $dec_meta;?>" placeholder="desc meta" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="keyword_meta">Keyword meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="keyword_meta" name="keyword_meta" value="<?php echo $keyword_meta;?>" placeholder="Keyword meta" type="text">
                            </div>
                        </div>
						
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="main_description">Home page Description<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="main_description" name="main_description" placeholder="Main Description" row="2"><?php echo $main_description;?></textarea>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="keyword_meta">Select type of consultation<span class="required">*</span></label>
                            <div class="col-md-5">
                            <select name="consultation_type" id="consultation_type" class="form-control consultation_type valid" aria-required="true" aria-invalid="false">
									<option value="" name="">Select the type of free consultation</option>
									<option value="Career"  <?php if($consultation_type=='Career')echo 'selected'?>  name="career">Question Related to Career</option>
									<option value="BirthStone" <?php if($consultation_type=='BirthStone')echo 'selected'?> name="birth_stone">Question Related to Birth Stone</option>
									<option value="HoroscopeQuery" <?php if($consultation_type=='HoroscopeQuery')echo 'selected'?> name="horoscope">Question Related to Horoscope Query</option>
									<option value="MatchMaking" <?php if($consultation_type=='MatchMaking')echo 'selected'?> name="match_making">Question Related to Match Making</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="description">Detail page Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor description" id="description" name="description" placeholder="Description" row="2"><?php echo $description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="c_image"> Home page image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="c_image_btn" name="c_image" class="c_image_btn" data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="c_image" name="c_image" value="<?php echo $c_image;?>" type="hidden">
								<img class="img-preview1" src="<?php echo base_url().'uploads/'.($c_image);?>" alt="" style="width: 150px;">
							</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="c_image"> Detail page image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="c_image_btn" name="c_image" class="c_image_btn" data-toggle="modal" data-target="#browseImage2">Upload Image</a>
                                <input class="form-control" id="c_detail_image" name="c_detail_image" value="<?php echo $c_detail_image;?>" type="hidden">
								<img class="img-preview2" src="<?php echo base_url().'uploads/'.($c_detail_image);?>" alt="" style="width: 150px;">
							</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Image Postion For Detail Page<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select name="detail_image_position" id="detail_image_position" class="form-control">
                                    <option value="">select image position</option>
                                    <option value="right" <?php if($detail_image_position=='right')echo 'selected'?> >Right</option>
                                    <option value="left" <?php if($detail_image_position=='left')echo 'selected'?> >Left</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question label<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_label" name="q_label" placeholder="Question Label" value="<?php echo $q_label;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Position* Higher will come first<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_position" name="services_position" placeholder="services_position" value="<?php echo $services_position;?>"  type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question inner Placeholder<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_placeholder" name="q_placeholder" placeholder="Question Placeholder" value="<?php echo $q_placeholder;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="slider_input4">Question Info<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="q_info" name="q_info" placeholder="Question Info" value="<?php echo $q_info;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1">Extra Box 1<span class="required">*</span></label>
                            <div class="col-md-5">
                            <select class="form-control"  id="is_question1" name="is_question1" value="<?php echo $is_question1;?>" >
									<option value="yes"  <?php if($is_question1 == 'yes' ) echo 'selected';?>>yes</option>									
									<option value="no" <?php if($is_question1 == 'no' ) echo 'selected';?>  >no</option>									
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1"><span class="required"></label>
                            <div class="col-md-5">
                            <input class="form-control" id="q1_label" name="q1_label" placeholder="Label" value="<?php echo $q1_label ;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question1"><span class="required"></label>
                            <div class="col-md-5">
                            <input class="form-control" id="q1_placeholder" name="q1_placeholder" placeholder="placeholder" value="<?php echo $q1_placeholder ;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2">Extra Box 2<span class="required">*</span></label>
                            <div class="col-md-5">
                            <select class="form-control"  id="is_question2" name="is_question2" value="<?php echo $is_question2;?>" >
									<option value="yes" <?php if($is_question2 == 'yes' ) echo 'selected';?> >yes</option>									
									<option value="no"  <?php if($is_question2 == 'no' ) echo 'selected';?> >no</option>									
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2"><span class="required"></label>
                            <div class="col-md-5">
                            <input class="form-control" id="q2_label" name="q2_label" placeholder="label" value="<?php echo $q2_label;?>"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="is_question2"><span class="required"></label>
                            <div class="col-md-5">
                            <input class="form-control" id="q2_placeholder" name="q2_placeholder" placeholder="placeholder" value="<?php echo $q2_placeholder;?>"  type="text">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="extra_description">Extra Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor extra_description" id="extra_description" name="extra_description" placeholder="extra_description" row="2"><?php echo $extra_description;?></textarea>
                            </div>
                        </div>


						<div class="form-group">
                            <label class="control-label col-md-2" for="faq_category">Faq Category</label>
                            <div class="col-md-5">
                                <input class="form-control" id="faq_category" name="faq_category"
                                    value="<?php echo $faq_category ; ?>" placeholder="Enter faq category">
                            </div>
                        </div>

						<div class="form-group">
                            <label class="control-label col-md-2" for="about_content_id">Footer Aboutus Content ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="about_content_id" name="about_content_id"
                                    value="<?php echo $about_content_id; ?>" placeholder="Enter Content ID ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="banner_id">Banner_id</label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_id" name="banner_id"
                                    value="<?php echo $banner_id ; ?>" placeholder="Enter banner_id">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse  Image-------------------------->
<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
	$('document').ready(function () {
		
		//-----------------------------------------------------------------------
		/* 
		 * admin_add_new_user_form validation
		 */
		$('#add_consultation_form_new').validate({
			ignore: [],
			rules: {
				consultation_name: {
					required: true
				},
				main_description: {
					required: true
				},
				keyword_meta: {
					required: true
				},

				faq_category: {
                required: true
            },
				dec_meta: {
					required: true
				},
				title: {
					required: true
				},
				url: {
					required: true
				},
				consultation_type: {
					required: true
				},
				description: {
					required: function(textarea) {
								  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
								  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
								  return editorcontent.length === 0;
							  }
				},
				c_image: {
					required: true
				},
				c_detail_image: {
					required: true
				},
				detail_image_position: {
					required: true
				},
				q_label: {
					required: true
				},
				q_placeholder: {
					required: true
				},
				services_position: {
					required: true
				},
				q_info: {
					required: true
				},
			},
			messages: {
				consultation_name: {
					required: "Name is required"
				},
				main_description: {
					required: "Main Description is required"
				},
				description: {
					required: "Description is required"
				},
				c_image: {
					required: "Image is required"
				},

				faq_category: {
                required: 'Faq Category is required'
            },
				c_detail_image: {
					required: "Image is required"
				},
				detail_image_position: {
					required: "Position is required"
				},
				keyword_meta: {
					required: "Keyword meta is required"
				},
				dec_meta: {
					required: "Dec meta is required"
				},
				title: {
					required: "Title is required"
				},
				url: {
					required: "URL is required"
				},
				consultation_type: {
					required: "consultation type  is required"
				},
				q_label: {
					required: "Question label is required"
				},
				services_position: {
					required: "services_position is required"
				},
				q_placeholder: {
					required: "Placeholder is required"
				},
				q_info: {
					required: "Question Info is required"
				},
				
			},
			submitHandler: function (form) {
				var consultation_name = $('#consultation_name').val();
				var main_description = $('#main_description').val();
				var description = CKEDITOR.instances['description'].getData();
				var extra_description = CKEDITOR.instances['extra_description'].getData();
				var c_image = $('#c_image').val();
				var c_detail_image = $('#c_detail_image').val();
				var detail_image_position = $('#detail_image_position').val();
				var consultation_id = $('#consultation_id').val();
				var faq_category = $('#faq_category').val();
				var banner_id = $('#banner_id').val();
            var about_content_id = $('#about_content_id').val();
				
				var url = $('#url').val();
				var consultation_type = $('#consultation_type').val();

				var title = $('#title').val();
				var dec_meta = $('#dec_meta').val();
				var keyword_meta = $('#keyword_meta').val();
				var q_label = $('#q_label').val();
				var q_placeholder = $('#q_placeholder').val();
				var q_info = $('#q_info').val();

				var is_question1 = $('#is_question1').val();
				var services_position = $('#services_position').val();
				var q1_label = $('#q1_label').val();
				var q1_placeholder = $('#q1_placeholder').val();
				var  is_question2= $('#is_question2').val();
				var q2_label = $('#q2_label').val();
				var q2_placeholder = $('#q2_placeholder').val();
				//console.log(consultation_type);
				$.post(APP_URL + 'configure_access/add_new_free_consultation', {
					url: url,
					title: title,
					dec_meta: dec_meta,
					keyword_meta: keyword_meta,
					consultation_name: consultation_name,
					main_description: main_description,
					description: description,
					extra_description: extra_description,
					c_image: c_image,
					c_detail_image: c_detail_image,
					detail_image_position: detail_image_position,
					consultation_id: consultation_id,
					faq_category: faq_category,
					banner_id: banner_id,
					about_content_id: about_content_id,

					q_label:q_label,
					q_placeholder:q_placeholder,
					q_info:q_info,
					consultation_type:consultation_type,

					is_question1:is_question1,
					services_position:services_position,
					q1_label:q1_label,
					q1_placeholder:q1_placeholder,
					is_question2: is_question2,
					q2_label:q2_label,
					q2_placeholder:q2_placeholder


				},
				function (response) {
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#err_services_form').empty();
					var mesg;
					if (response.status == 200) {
						mesg = response.message;
						
						if(consultation_id != 0){
							mesg = 'Free Consultation has been updated successfully';
						}
						$('#err_services_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + mesg + "</strong></div>");
						$('#err_change_password_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						alert(response.message);
						window.location.href= APP_URL+'configure_access/free_consultation_list';
					}
					else if (response.status == 201) {
						$('#err_services_form').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
					$('#consultation_name').val('');
					$('#main_description').val('');
					$('#description').val('');
					$('#c_image').val('');
					$('#url').val('');
					$('#title').val('');
					$('#dec_meta').val('');
					$('#keyword_meta').val('');
					$('#q_label').val('');
					$('#q_placeholder').val('');
					$('#q_info').val('');
					$('#is_question1').val('');
					$('#q1_label').val('');
					$('#q1_placeholder').val('');
					$('#consultation_type').val('');
					$('#is_question2').val('');
					$('#q2_label').val('');
					$('#q2_placeholder').val('');
					$('.img-preview').attr('src');
					}, 'json');
				return false;
			}
		});

	});
</script>
