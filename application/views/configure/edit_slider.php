<script src="<?php echo base_url(); ?>assets/js/slider.js"></script> 
<style>.slider_photo{   width: 150px;}</style>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_slider"></div>
                    <div id="edit_slider_table">
                        <h2>List of All Slider</h2>
                        <div id="table_view">             
                            <?php
                            if ($user == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Slider Info</th><th class="text-center">Slider Navigation</th><th class="text-center">Slider Input1</th><th class="text-center">Slider Input2</th><th class="text-center">slider position</th><th class="text-center">Status</th><th class="text-center">Slider Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($user as $value) {
									$status = '';
									if($value['status'] == 'old'){ $status = 'inactive'; }else{ $status = $value['status']; }
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['slider_info'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['slider_navi'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['slider_input1'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['slider_input2'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['slider_position'] . '</td>';
                                    $content .= '<td class="text-center">' . $status . '</td>';
                                    $content .= '<td class="text-center"><a class="editBrowseImageBtn" name=' . $value['slider_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="#"><img class="slider_photo" src="' . base_url() . 'uploads/' . $value['slider_image'] . '"></a></td>';
                                    //$content .= '<td class="text-center"><a href="#" class="edit_slider" data-toggle="modal" data-target="#my_user_edit" name=' . $value['slider_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_slider?slider_id='.$value['slider_id'].'"class="edit_slider" name=' . $value['slider_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="remove_slider"  name=' . $value['slider_id'] . ' value=""><span class="label label-danger">Remove Slider</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
								
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_slider_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Slider</h4>
                    <input type="hidden" name="edit_slider_id" id="edit_slider_id" value=""/>     
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_slider_info">Slider Info<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_slider_info" name="edit_slider_info" placeholder="Slider Info" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_slider_navi">Slider Navigation<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_slider_navi" name="edit_slider_navi" placeholder="Slider Navigation URL" type="text">
                        </div>
                    </div>


                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary " value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="image_slider_id" id="image_slider_id" value=""/>     
                    <input type="hidden" value="slider" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>