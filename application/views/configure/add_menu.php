<script src="<?php echo base_url(); ?>assets/js/menu.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Menu</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_menu_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="addMenuForm" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="menu_name">Menu Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="menu_name" name="menu_name" placeholder="Menu Name" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="menu_navi">Menu Navigation URL<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="menu_navi" name="menu_navi" placeholder="Menu Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
