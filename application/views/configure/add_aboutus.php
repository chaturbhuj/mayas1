<script src="<?php echo base_url(); ?>assets/js/aboutus.js"></script> 

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add About us</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_aboutus_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_aboutus_form" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="aboutus1">Aboutus Paragraph 1<span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea class="form-control" id="aboutus1" name="aboutus1" placeholder="Aboutus Paragraph 1" row="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="aboutus2">Aboutus Paragraph 2<span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea class="form-control" id="aboutus2" name="aboutus2" placeholder="Aboutus Paragraph 1" row="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="aboutus3">Aboutus Paragraph 3<span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea class="form-control" id="aboutus3" name="aboutus3" placeholder="Aboutus Paragraph 3" row="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="aboutus4">Aboutus Paragraph 4<span class="required">*</span></label>
                            <div class="col-md-5">
								<textarea class="form-control" id="aboutus4" name="aboutus4" placeholder="Aboutus Paragraph 4" row="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
