<script src="<?php echo base_url(); ?>assets/js/sub_menu_products.js"></script> 
<style> img{width:100px;} </style>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_products"></div>
                    <div id="edit_user_table">
                        <h2>List of All Sub Menu products Item</h2>
                        <div id="table_view">             
                            <?php
                            if ($sub_menu_products == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Sub Menu products Name</th><th class="text-center">Sub Menu products Navigation URL</th><th class="text-center">Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($sub_menu_products as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['sub_menu_products_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['sub_menu_products_navi'] . '</td>';
                                     $content .= '<td class="text-center"><a class="editBrowseImageBtn" name=' . $value['sub_menu_products_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="#"><img class="product_photo" src="' . base_url() . 'uploads/' . $value['sub_menu_products_image'] . '"></a></td>';
									$content .= '<td class="text-center"><a href="#" class="edit_sub_menu_products" data-toggle="modal" data-target="#my_user_edit" name=' . $value['sub_menu_products_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_sub_menu_products"  name=' . $value['sub_menu_products_id'] . ' value=""><span class="label label-danger">Remove Sub Menu Services</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_sub_menu_products_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Sub Menu Products</h4>
                    <input type="hidden" name="edit_sub_menu_products_id" id="edit_sub_menu_products_id" value=""/>     
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_sub_menu_products_name">Sub Menu Products Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_sub_menu_products_name" name="edit_sub_menu_products_name" placeholder="Sub Menu Products Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_sub_menu_products_navi">Sub Menu Products Navigation URL<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_sub_menu_products_navi" name="edit_sub_menu_products_navi" placeholder="Sub Menu Products Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                        </div>
                    </div>
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="editBrowseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="edit_upload_image" action="<?php echo base_url(); ?>configure_access/edit_upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" name="image_product_id" id="image_product_id" value=""/>     
                    <input type="hidden" value="sub_menu_product" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
