
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Order Detail List</h3>
			</div>
			
            <!-- /.box-header -->
			  <div class="box-body">
			  <div id="headerMsg" ></div>
				<table align="left" class="table table-hover">
					<thead>
						<tr>
							<th class="text-center">  S. No.  </th>
							<th class="text-center">  Product Name </th>
							<th class="text-center">  Product Amount  </th>
							<th class="text-center">  Total Amount  </th>
						</tr>
					</thead>
					<tbody>
		
					<?php
						//var_dump($contact_lists);
						$i = 1;
						if ($records == 0) {
							echo 'No record found into database';
						  } 
						else {
							$content = '';
							foreach ($records as $value) {
								//var_dump($value);
								$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
									$content .= '<td class="text-center">' . $value['product_name'] . '</td>';
									$content .= '<td class="text-center">' . $value['quantity'] . ' X ₹' . $value['product_total'] . '</td>';
									$content .= '<td class="text-center">₹' . $value['product_total'] . '</td>';
								$content .= '</tr>';
								$i++;
							}
							echo $content;
						}
			
					?>
					</tbody>
				</table>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 <script>
$('document').ready(function(){
	$('body').on('click', '.remove_image', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
		//$.blockUI();
        var contact_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/configure_access/remove_contact_us', {contact_id: contact_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                $('.remove_image[name=' + contact_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					window.location.reload();
				});
            } else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
        }, 'json');
		//$.unblockUI();
        return false;
    });

});
</script>
