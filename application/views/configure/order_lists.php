
<script type="text/javascript">
	$(function () {
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			maxDate: 'dateToday',
			yearRange : 'c-75:c+75',
		});
	
	});
</script>
<style>
.modal_background{
	background: #E0317E;
	color:#ffff;
}

</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Order List</h3>
              <input type="hidden" id="is_completed_order" value="<?php if($this->uri->segment(3)=='completed'){ echo 'yes'; }else{ echo 'no'; } ?>">
              <input type="hidden" id="is_inprocess_order" value="<?php if($this->uri->segment(3)=='inprocess'){ echo 'yes'; }else{ echo 'no'; } ?>">
              <input type="hidden" id="failed" value="<?php if($this->uri->segment(3)=='failed'){ echo 'yes'; }else{ echo 'no'; } ?>">
			</div>
            <!-- /.box-header -->
			  <div class="box-body">
			  	<div class="row">
			  		<form id="search_form">
					  	<div class="form-group col-md-4">
		                  <label for="start_date">Start Date</label>
		                  <input type="text" class="form-control date startDate" readonly id="start_date" name="start_date" placeholder="Enter Date" value="">
		                </div>
		                <div class="form-group col-md-4">
		                  <label for="end_date">End Date</label>
		                  <input type="text" class="form-control date" readonly id="end_date" name="end_date" placeholder="Enter Date" value="">
		                </div>
		                <div class="form-group col-md-4">
		                  <label for="end_date"> </label>
		                  <button type="submit" class="btn btn-primary btn-block">Filter</button>
		                </div>
	            	</form>
			  	</div>
				<div class="row display_none">
				  	<div class="form-group col-md-4">
						<label for="start_date">Country</label>
						<select class="form-control" id="country_id" name="country_id">
							<option value="">select country</option>
							<?php 
								if($country_data){
									foreach ($country_data as $value) {
									   echo '<option phone_code="'.$value['country_code'].'" country_name="'.$value['country_name'].'" value="'.$value['id'].'">'.$value['country_name'].'</option>';
									}
								}
							 ?>
						</select>
					</div>
	                <div class="form-group col-md-4">
						<label for="end_date">State</label>
						<select class="form-control" id="state_id" name="state_id">
							<option value="">select state</option>
							
						</select>
						<select class="form-control" id="state_id_temp" name="state_id_temp" style="display:none;">
							<option value="">select state</option>
							<?php 
								if($state_data){
									foreach ($state_data as $value) {
									   echo '<option country_id="'.$value['country_id'].'" state_name="'.$value['state_name'].'" value="'.$value['id'].'">'.$value['state_name'].'</option>';
									}
								}
							?>
						</select>
	                </div>
	                <div class="form-group col-md-4">
						<label for="end_date">City</label>
						<select class="form-control" id="city_id" name="city_id">
							<option value="">select city</option>
						
						</select>
						<select class="form-control" id="city_id_temp" name="city_id_temp" style="display:none;">
							<option value="">select city</option>
							<?php 
							if($city_data){
								foreach ($city_data as $value) {
								   echo '<option country_id="'.$value['country_id'].'" state_id="'.$value['state_id'].'" city_name="'.$value['city_name'].'" value="'.$value['id'].'">'.$value['city_name'].'</option>';
								}
							}
							?>
						</select>
	                </div>
			  	</div>
			  	<div id="headerMsg" ></div>
			  	<div class="table-responsive">
					<table align="left" class="table table-hover dataTables1">
						<thead>
							<tr>
								<th class="text-center">  S. No. </th>
								<th class="text-center">  Order Date </th>
								<th class="text-center">  Order Id </th>
								<th class="text-center">  Customer Name </th>
								
								<th class="text-center">  Product Amount  </th>
								<th class="text-center">  Charges Added Amount  </th>
								<th class="text-center">  Payment Type  </th>
								<th class="text-center">  Order Status  </th>								
								<th class="text-center">  Payment Status  </th>								
								<th class="text-center">  Failed Payment status</th>								
															
								<th class="text-center">  Action </th>
							</tr>
						</thead>
						<tbody id="append_order_list">
						</tbody>
					</table>
				</div>
			</div>
          </div>
          <!-- /.box -->

        </div>
        </div>

      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  	<!-- Modal -->
	<div class="modal fade" id="customerDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header modal_background">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Customer Detail</h4>
	      </div>
	      <div class="modal-body">
	        	<h4>Name : <span id="show_customer_name"></span></h4>
			  	<h4>Contact : <span id="show_customer_contact"></span></h4>
			  	<h4>Address : <span id="show_customer_address"></span></h4>
			  	<h4>City : <span id="show_customer_city"></span></h4>
			  	<h4>State : <span id="show_customer_state"></span></h4>
			  	<h4>ZIP Code : <span id="show_customer_zip_code"></span></h4>
			  	<h4>Country : <span id="show_customer_country"></span></h4>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="vendorDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header modal_background">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Customer Detail</h4>
	      </div>
	      <div class="modal-body">
	        	<h4>Name : <span id="show_vendor_name"></span></h4>
			  	<h4>Email Address : <span id="show_vendor_email"></span></h4>
			  	<h4>Contact : <span id="show_vendor_contact"></span></h4>
			  	<h4>City : <span id="show_vendor_city"></span></h4>
			  	<h4>State : <span id="show_vendor_state"></span></h4>
			  	<h4>Country : <span id="show_vendor_country"></span></h4>
	      </div>
	    </div>
	  </div>
	</div>
<!-- Modal end-->
<!-- Modal start for edit status-->
	<div class="modal fade" id="status_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header modal_background">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title text-center" id="myModalLabel">Change Status</h4>
	      </div>
		  <div id="err_delt_plan"></div>
	      <div class="modal-body">
	        	<input type="hidden" id="order_ids" value="">
				<form id="edit_status">
					<select class="form-control" id="change_id" name="option_name">
						<option value="">choose status</option>
						<option value="in process">in process</option>
						<option value="delivered">delivered</option>
						<option value="cancel">cancel</option>
					</select><br>
					<div class="text-center">
						<button class="btn btn-primary text-center">submit</button>
					</div>
				</form>
	      </div>
	    </div>
	  </div>
	</div>
<!-- Modal end for edit status-->
<!-- Modal start for edit status-->
	<div class="modal fade" id="failed_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header modal_background">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title text-center" id="myModalLabel">Change Status</h4>
	      </div>
		  <div id="failed_plan"></div>
	      <div class="modal-body">
	        	<input type="hidden" id="order_ids" value="">
				<form id="failed_status">
					<select class="form-control" id="change_ids" name="option_name">
						<option value="">choose status</option>
						<option value="yes">yes</option>
						<option value="no">no</option>						
					</select><br>
					<div class="text-center">
						<button class="btn btn-primary text-center">submit</button>
					</div>
				</form>
	      </div>
	    </div>
	  </div>
	</div>
<!-- Modal end for edit status-->

 <script>
$('document').ready(function(){
	
	get_order_list();
	var start_date = '';
	var end_date = '';
	//setInterval(function(){ get_order_list() }, PAGE_REFRESH);
	function get_order_list(){
		start_date = $('#start_date').val();
		end_date = $('#end_date').val();
		var is_completed_order=$('#is_completed_order').val();
		var is_inprocess_order=$('#is_inprocess_order').val();
		var failed=$('#failed').val();
		var country_name=$('#country_id option:selected').attr('country_name');
		var state_name=$('#state_id option:selected').attr('state_name');
		var city_name=$('#city_id option:selected').attr('city_name');

		$.post(APP_URL + 'configure_access/get_order_list', {
			is_completed_order : is_completed_order,
			is_inprocess_order : is_inprocess_order,
			failed : failed,
			start_date : start_date,
			end_date : end_date,
			country_name : country_name,
			state_name : state_name,
			city_name : city_name,
		}, function (response) {
			console.log(response.data);
			//console.log(response.data.vendor_data);
			var content='';
			var i=1;
			var time2 = new Date(); 
			console.log(time2);
			$. each(response.data, function(index, value){
				console.log(value);
				var sum=value.final_amount;
				var curency=$;
				var sum1=parseFloat(sum).toFixed(2);
				console.log(sum1);
			    var show_time='';
			    if(!value['driver_id']){
			    	var time1 = new Date( value['created_date']+" "+value['created_time'] );
					var msec = time2 - time1;
				    var mins = Math.floor(msec / 60000);
				    var hrs = Math.floor(mins / 60);
				    var days = Math.floor(hrs / 24);
				    var yrs = Math.floor(days / 365);
				     hrs = hrs % 24;
				     mins = mins % 60;
				     days = days % 365;
				    if(days>0){
				    	show_time="("+ days + " days, " + hrs + " hours, " + mins + " minutes)";
				    }else if(hrs>0){
				    	show_time= "("+hrs + " hours, " + mins + " minutes)";
				    }else{
				    	show_time= "("+mins + " minutes)";
				    }
				}
				console.log(show_time);
				console.log(yrs);
				content += '<tr class="darker-on-hover">';
					content += '<td class="text-center">' + i + '</td>';
					content += '<td class="text-center">' + value['created_date'] + '</td>';
					content += '<td class="text-center">' + value['order_id'] + '</td>';
					content += '<td class="text-center">' + value['first_name'] + ' ' + value['last_name'] + '';
						content += '<br/><a href="javascript:void(0)" class="label label-success show_customer_detail" data-toggle="tooltip" name="' + value['first_name'] + ' ' + value['last_name'] + '" phone_number="' + value['phone_number'] + '" address_line="' + value['address_line'] + '" city_name="' + value['city_name'] + '" state_name="' + value['state_name'] + '" zip_code="' + value['zip_code'] + '" country_name="' + value['country_name'] + '" ><i class="fa fa-eye"></i></a>';
					content += '</td>';
					
					content += '<td class="text-center">' + value['product_data']['product_name'] + '  <br> ' + value['total_amount'] + ' ₹</td>';
					content += '<td class="text-center">' + value['currency_symbol'] + ' ' + sum1 + ' ₹</td>';
					content += '<td class="text-center">' + value['payment_type'] + '</td>';
					content += '<td class="text-center">' + value['order_status'] + '<br/><a href="javascript:void(0)" class="label label-success edit_order_status" data-toggle="tooltip" title="edit" order_id="'+value['order_id']+'"><i class="fa fa-edit"></i></a></td>';
					content += '<td class="text-center">' + value['payment_status'] + '<br/></td>';
					content += '<td class="text-center">' + value['check_payment'] + '<br/><a href="javascript:void(0)" class="label label-success edit_failed_status" data-toggle="tooltip" title="edit" order_id="'+value['order_id']+'"><i class="fa fa-edit"></i></a></td>';
					
					
					content += '<td class="text-center">';
						content += '<a href="'+APP_URL+'configure_access/order_details?id='+value['order_id']+'" title="view order"  class="label label-success" data-toggle="tooltip" name=' + value['order_id'] + ' ><i class="fa fa-eye"></i></a>&nbsp;&nbsp;';
						content += '<a href="" class="label label-danger remove_plan" data-toggle="tooltip" name=' + value['order_id'] + ' ><i class="fa fa-trash"></i></a>';
					content += '</td>';
				content += '</tr>';
				i=i+1;
			});
			$('#append_order_list').html(content);
			$('.dataTables1').DataTable();
		}, 'json');
	}
	$('body').on('click','.edit_module',function(){
 		var organisation_id = $(this).attr('value');
 		//var password = $(this).attr('status');
 		$('#query_id2').val(organisation_id);
 		//$('#password').val(password);	
 
 	});  


		
 	$('body').on('change',"#country_id",function (e) {
		$('#state_id option').remove();
		$('#city_id option').remove();
		var optVal = $("#country_id option:selected").val();
		console.log('country_id ' + optVal);
		
		var options = $("#state_id_temp > option").clone();
		$('#state_id').append(options);

		$('#state_id option').each(function() {
			if ($(this).attr('country_id') != optVal) {
				if ($(this).attr('value') != "") {
					$(this).remove();
				}
			}
		});
		
		var options = $("#city_id_temp > option").clone();
		$('#city_id').append(options);

		$('#city_id option').each(function() {
			if ($(this).attr('country_id') != optVal) {
				if ($(this).attr('value') != "") {
					$(this).remove();
				}
			}
		});
		get_order_list();
    });

    $('body').on('change',"#state_id",function (e) {
		$('#city_id option').remove();
		var optVal= $("#state_id option:selected").val();
		console.log(optVal);
      
		var options = $("#city_id_temp > option").clone();
		$('#city_id').append(options);

		$('#city_id option').each(function() {
			if ($(this).attr('state_id') != optVal) {
				if ($(this).attr('value') != "") {
					$(this).remove();
				}
			}
		});
       get_order_list();
    });

    $('body').on('change',"#city_id",function (e) {
       get_order_list();
    });

	$('body').on('click','.show_vendor_detail',function(){
		
 		var name = $(this).attr('name');
 		var phone_number = $(this).attr('phone_number');
 		var vendor_email = $(this).attr('vendor_email');
 		var city_name = $(this).attr('city_name');
 		var state_name = $(this).attr('state_name');
 		var country_name = $(this).attr('country_name');
 		$('#show_vendor_name').text(name);
 		$('#show_vendor_contact').text(phone_number);
 		$('#show_vendor_email').text(vendor_email);
 		$('#show_vendor_city').text(city_name);
 		$('#show_vendor_state').text(state_name);
 		$('#show_vendor_country').text(country_name);
 		$('#vendorDetailModal').modal('show');
 	}); 
	$('body').on('click','.edit_order_status',function(){
		
 		var order_id = $(this).attr('order_id');
 	
 		$('#status_detail').modal('show');
		$('#order_ids').val(order_id);
 	}); 
	$('body').on('click','.edit_failed_status',function(){		
 		var order_id = $(this).attr('order_id');	
 		$('#failed_payment').modal('show');
		$('#order_ids').val(order_id);
 	});

	
	$('#edit_status').validate({
		rules: {
			option_name: {
				required: true,
			},
		},
		messages: {        
			option_name: {
				required: "choose the option first",
			},
		},
 		submitHandler: function (form) {
			$.blockUI();
			//$('#menu_form').find('button[type="submit"]').prop('disabled',true);
			var order_id= $('#order_ids').val();
			var status = $('#change_id option:selected').attr('value');
			
           
            $.post(APP_URL + 'configure_access/update_status_of_order', {
            	order_id: order_id,
            	status: status,
            	
            },
			function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#err_delt_plan').empty();
				if (response.status == 200) {
					$('#err_dlt_plan').empty();
					//$('#status_detail').modal('hide');
					$('#err_delt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_delt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_delt_plan').empty();						
						$('#status_detail').modal('hide');							
					});

				} else {
					$('#err_delt_plan').empty();
					$('#err_delt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_delt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_delt_plan').empty();
					});
				}
				
			}, 'json');
			return false;
		}
	});
	
	$('#failed_status').validate({
		rules: {
			option_name: {
				required: true,
			},
		},
		messages: {        
			option_name: {
				required: "choose the option first",
			},
		},
 		submitHandler: function (form) {
			$.blockUI();
			//$('#menu_form').find('button[type="submit"]').prop('disabled',true);
			var order_id= $('#order_ids').val();
			var status = $('#change_ids option:selected').attr('value');
			
           
            $.post(APP_URL + 'configure_access/update_status_of_failedorder', {
            	order_id: order_id,
            	status: status,
            	
            },
			function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#err_delt_plan').empty();
				if (response.status == 200) {
					$('#failed_plan').empty();
					
					$('#failed_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#failed_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#failed_plan').empty();						
						$('#failed_payment').modal('hide');							
					});

				} else {
					$('#failed_plan').empty();
					$('#failed_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#failed_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#failed_plan').empty();
					});
				}
				
			}, 'json');
			return false;
		}
	});

 	$('body').on('click','.show_driver_detail',function(){
 		var name = $(this).attr('name');
 		var phone_number = $(this).attr('phone_number');
 		var driver_email = $(this).attr('driver_email');
 		$('#show_driver_name').text(name);
 		$('#show_driver_contact').text(phone_number);
 		$('#show_driver_email').text(driver_email);
 		$('#driverDetailModal').modal('show');
 	});  

 	$('body').on('click','.show_customer_detail',function(){
 		var name = $(this).attr('name');
 		var phone_number = $(this).attr('phone_number');
 		var address_line = $(this).attr('address_line');
 		var city_name = $(this).attr('city_name');
 		var state_name = $(this).attr('state_name');
 		var zip_code = $(this).attr('zip_code');
 		var country_name = $(this).attr('country_name');
 		$('#show_customer_name').text(name);
 		$('#show_customer_contact').text(phone_number);
 		$('#show_customer_address').text(address_line);
 		$('#show_customer_city').text(city_name);
 		$('#show_customer_state').text(state_name);
 		$('#show_customer_zip_code').text(zip_code);
 		$('#show_customer_country').text(country_name);
 		$('#customerDetailModal').modal('show');
 	});  

 	$('#menu_form').validate({
		rules: {
			driver_data: {
				required: true,
			},
		},
		messages: {        
			driver_data: {
				required: "Driver is required",
			},
		},
 		submitHandler: function (form) {
			$.blockUI();
			//$('#menu_form').find('button[type="submit"]').prop('disabled',true);
			var order_id = $('#query_id2').val();
			var driver_id = $('#driver_data').val();
			var driver_name = $('#driver_data option:selected').attr('name');
			var driver_email = $('#driver_data option:selected').attr('email_id');
			var driver_phone_number = $('#driver_data option:selected').attr('phone_number');
           
            $.post(APP_URL + 'admin/configure/update_driver_assign', {
            	order_id: order_id,
            	driver_id: driver_id,
            	driver_name: driver_name,
            	driver_email: driver_email,
                driver_phone_number: driver_phone_number,
            },
			function (response) {
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#err_dlt_plan').empty();
				if (response.status == 200) {
					$('#err_dlt_plan').empty();
					$('#browseNewModule').modal('hide');
					$('#err_dlt_plan').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
						//window.location.reload();
						$('#assignDriverModal').modal('hide');
						get_order_list();
						//window.location.href = APP_URL+'account/event/user_organisation_list';
					});

			   } else {
					$('#err_dlt_plan').empty();
					$('#err_dlt_plan').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					$("#err_dlt_plan").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_dlt_plan').empty();
					});
				}
				
			}, 'json');
			return false;
		}
	});


 	$.validator.addMethod("endDate", function(value, element) {
		var startDate = $('.startDate').val();
		return Date.parse(startDate) <= Date.parse(value) || value == "";
	}, "* End date must be after start date");

	//-----------------------------------------------------------------------
    /* 
     * edit_user_profile_detail_form validation
     */
    $('#search_form').validate({
        rules: {
            start_date: {
				required: true
			}, 
			end_date: {
				endDate: true
			},
		},
        messages: {
			start_date: {
                required: "Event end Date is required",
            },
            end_date: {
                required: "Event end Date is required",
            },
		},
		submitHandler: function (form) {
			//console.log("ok");
			//$.blockUI();
			
			start_date = $('#start_date').val();
			end_date = $('#end_date').val();
			get_order_list();
			
			return false;
		},
	});

	$('body').on('click', '.remove_plan', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
		//$.blockUI();
        var contact_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'admin/configure/remove_order_data', {contact_id: contact_id}, function (response) {
            $('#headerMsg').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                $('.remove_plan[name=' + contact_id + ']').closest("tr").remove();
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					//window.location.reload();
					get_order_list();
				});
            } else {
                $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
        }, 'json');
		//$.unblockUI();
        return false;
    });
	
	$('body').on('change', '.status_change_option', function () {		
        var status=$(this).val();
        console.log(status);
		$('#driver_status_for').val(status);
	});
	
	$('#category_form2').validate({
		rules: {
			category_status: {
				required: true,
			},
			reason: {
				required: true,
			},
		},
		messages: {        
			category_status: {
				required: "Status is required",
			},
			reason: {
				required: "Reason is required",
			},
		},
 		submitHandler: function (form) {
			$.blockUI();
	       var order_id = $('#query_id2').val();
	       var status = $('#driver_status_for').val();
	        $.post(APP_URL + 'admin/configure/update_driver_status', {status: status, order_id:order_id}, function (response) {
	            $('#err_plan').empty();
	            if (response.status == 200) {
	                $("html, body").animate({scrollTop: 0}, "slow");               
	                $('#err_plan').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
	                
					$("#err_plan").fadeTo(1000, 500).slideUp(500, function(){
						$('#err_plan').empty();
						window.location.reload();
					});
	            } else {
	                $('#err_plan').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#err_plan").fadeTo(1000, 500).slideUp(500, function(){
						$('#err_plan').empty();
					});
				}
	        }, 'json');
			//$.unblockUI();
	        return false;
		}
    });
	

});
</script>
