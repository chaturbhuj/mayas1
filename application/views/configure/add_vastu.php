<script src="<?php echo base_url(); ?>assets/js/aboutus.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
    .inline_input{
        padding:0 5px;
    }
</style>
<?php
//var_dump($vastu);
if($vastu){
    $faq_category = $vastu[0]['faq_category'];
    $banner_id = $vastu[0]['banner_id'];
    $about_content_id = $vastu[0]['about_content_id'];
    $vastu_id = $vastu[0]['vastu_id'];
	$page_heading=$vastu[0]['page_heading'];
	$short_description=$vastu[0]['short_description'];
	$url=$vastu[0]['url'];
	$page_title=$vastu[0]['page_title'];
	$meta_tag=$vastu[0]['meta_tag'];
	$meta_descp=$vastu[0]['meta_descp'];
	$page_content=$vastu[0]['page_content'];
    $additional_json = array();
    $detail_image= $vastu[0]['detail_image'];
    $detail_image_position ='right';
    $title = "Edit";
    $owner_name_label = $vastu[0]['owner_name_label'];
    $owner_name_placeholder = $vastu[0]['owner_name_placeholder'];
    $email_id_label = $vastu[0]['email_id_label'];
    $email_id_placeholder = $vastu[0]['email_id_placeholder'];
    $owner_contact_label = $vastu[0]['owner_contact_label'];
    $owner_contact_placeholder = $vastu[0]['owner_contact_placeholder'];
    $property_address_label = $vastu[0]['property_address_label'];
    $property_address_placeholder = $vastu[0]['property_address_placeholder'];
    $property_layout_maps_label = $vastu[0]['property_layout_maps_label'];
    $property_direction_orientation_label = $vastu[0]['property_direction_orientation_label'];
    $property_direction_orientation_placeholder = $vastu[0]['property_direction_orientation_placeholder'];
    $owner_date_time_birth_label = $vastu[0]['owner_date_time_birth_label'];
    $owner_place_birth_label = $vastu[0]['owner_place_birth_label'];
    $property_direction_orientation_message = $vastu[0]['property_direction_orientation_message'];
    $property_directioncompass_msg = $vastu[0]['property_directioncompass_msg'];
    $property_directioncompass_link = $vastu[0]['property_directioncompass_link'];
    $property_directioncompass_defaultvideo = $vastu[0]['property_directioncompass_defaultvideo'];
    $compass_image = $vastu[0]['compass_image'];
    $ishome = $vastu[0]['is_home'];
    $home_msg = $vastu[0]['home_msg'];
  

}else{
    $vastu_id = 0;
	$page_heading='';
	$short_description='';
	$url='';
	$page_title='';
    $faq_category = 0;
    $banner_id = 0;
    $about_content_id = 0;
	$meta_tag='';
	$meta_descp='';
	$page_content='';
    $additional_json = array();
    $detail_image='';
    $detail_image_position ='';
    $title = 'Add';
    $owner_name_label = 'Property Owner(s) Name';
    $owner_name_placeholder = 'Enter your Full name';
    $email_id_label = 'Property owner Email-id';
    $email_id_placeholder = 'Enter your Email-id';
    $owner_contact_label = 'Property owner Contact No.';
    $owner_contact_placeholder = 'Enter your Contact Number';
    $property_address_label = 'Property Address';
    $property_address_placeholder = 'Enter your Property Address';
    $property_layout_maps_label = 'Property layouts or Scaled maps';
    $property_direction_orientation_label = 'Property direction/orientation';
    $property_direction_orientation_placeholder = 'Enter your Property direction/orientation';
    $owner_date_time_birth_label = 'Property owner Date And Time Of Birth';
    $owner_place_birth_label = 'Property owner Place Of Birth';
    $property_direction_orientation_message = 'If you know your property direction otherwise leave it.';
    $property_directioncompass_msg = 'How to get Property Direction with the help of Compass';
    $property_directioncompass_link = '';
    $property_directioncompass_defaultvideo = '';
    $compass_image = '';
    $ishome = 'no';
    $home_msg = '';

}

// var_dump($ishome);

?>
<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?=$title?> Vastu</h1>
		</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="widget-content padded">
                    <div id="err_page_form"></div>
                    <div id="edit_user_table">
                        <form id="edit_vastu_form" method="post" class="form-horizontal">
                            <div class="form-group">
                                <input type="hidden" name="vastu_id" id="vastu_id" value="<?=$vastu_id?>">
                                <label class="control-label col-md-3" for="">Home page heading<span class="required">*</span></label>
                                <div class="col-md-5">
                                  <input type="text" name="page_heading" id="page_heading" value="<?=$page_heading?>" class="form-control" placeholder="Page Heading">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Short Description<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="short_description" id="short_description" value="<?=$short_description?>" class="form-control" placeholder="Short Description">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Url<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="url" id="url"  value="<?=$url?>" class="form-control" placeholder="page url">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Page title<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="page_title" id="page_title" value="<?=$page_title?>" class="form-control" placeholder="Page Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Meta Tag<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="meta_tag" id="meta_tag" value="<?=$meta_tag?>" class="form-control" placeholder="Meta tag">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="">Meta Description<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="meta_descp" id="meta_descp" value="<?=$meta_descp?>" class="form-control" placeholder="Meta Description">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-md-3" for="aboutus1">Page Content<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor aboutus1" id="page_content" name="page_content"  placeholder="Vastu Detail Description" rows="5"><?=$page_content?></textarea>
                            </div>
                            </div>
                            <div class="form-group display_none">
                                <label for="" class="col-md-2 text-right">Add</label>
                                <label for="" class="add_question btn btn-success" >+</label>
                            </div>
                                                    <div class="add_inputs">
                            <?php 
                                if(!empty($additional_json)){
                                    // var_dump($additional_json);
                                    foreach($additional_json as $val){

                                ?>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                    <label>Heading</label>
                                        <input type="text" class="form-control q_heading" id="q_heading" name="q_heading" placeholder="Heading" value="<?php echo $val->q_heading;?>">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                    <label>Write Description</label>
                                        <input type="text" class="form-control q_description" id="q_description" name="q_description" placeholder="Description" value="<?php echo $val->q_description;?>">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                    <label>INR Cost</label>
                                        <input class="form-control cost" id="cost" name="cost" value="<?php echo $val->cost;?>" placeholder="Cost" type="number" min="0">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <div class="form-group inline_input">
                                    <label>USD Cost</label>
                                        <input class="form-control usdcost" id="usdcost" name="usdcost" value="<?php echo $val->usdcost;?>" placeholder="USD Cost" type="number" min="0">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                    <label>Discounted INR Cost</label>
                                        <input class="form-control discounted_cost" id="discounted_cost" name="discounted_cost" value="<?php echo $val->discounted_cost;?>" placeholder=" Discounted Cost" type="number" min="0">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group inline_input">
                                    <label>Discounted USD Cost</label>
                                        <input class="form-control discounted_usd_cost" id="discounted_usd_cost" name="discounted_usd_cost" value="<?php echo $val->discounted_usd_cost;?>" placeholder=" Discounted USD Cost" type="number" min="0">
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <label for="" class="rem_question btn btn-sm btn-danger">&times;</label>
                                </div>
                            </div>
                            <?php       }
                                }
                            ?>
                             </div>
                             <div class="form-group">
                            <label class="control-label col-md-3" for="services_image">Service detail image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="services_image_btn" name="services_image" class="services_image_btn" data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="detail_image" name="detail_image" value="<?php echo $detail_image?>" type="hidden">
								<img class="img-preview1" src="<?php echo base_url().'uploads/'.$detail_image ?>" alt="" style="width: 150px;">
							</div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="slider_input4">Detail Image Position<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="detail_image_position" name="detail_image_position">
                                    <option value="">Select detail image position</option>
									<option value="right" <?php if($detail_image_position=='right'){echo 'selected'; }?>>Right</option>
									<option value="left" <?php if($detail_image_position=='left'){echo 'selected'; }?>>Left</option>
								</select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3" for="is_home">Show Home Page<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="is_home" name="is_home">
                                <option value="">select</option>
                                    <?php
                                    $this->db->where('is_home', 'yes');
                                    $this->db->from('mayas_new_vastu');
                                    $countingdata = $this->db->count_all_results();

                                    $data_check = ($countingdata >= 3) ? 'no' : 'yes';

                                    if ($data_check == 'no') {
                                        if ($ishome == 'yes') {
                                            echo '<option value="yes" ' . ($ishome == 'yes' ? 'selected' : '') . '>yes</option>';
                                            echo '<option value="no" ' . ($ishome == 'no' ? 'selected' : '') . '>no</option>';
                                        } else {
                                            echo '<option value="no" ' . (($ishome == 'no' || $ishome == '') ? 'selected' : '') . '>no</option>';
                                        }
                                    } else {
                                        echo '<option value="yes" ' . ($ishome == 'yes' ? 'selected' : '') . '>yes</option>';
                                        echo '<option value="no" ' . ($ishome == 'no' ? 'selected' : '') . '>no</option>';
                                    }
                                    ?>


								
								</select>
                            </div>
                        </div>

                        <div class="form-group">
                                <label class="control-label col-md-3" for="home_msg">Home Message<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="home_msg" name="home_msg"
                                        placeholder="Message" value="<?php echo $home_msg;?>" type="text">
                                </div>
                            </div>

                        
                       
                        <div class="form-group">
                                <label class="control-label col-md-3" for="owner_name_label">Owner Name Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_name_label" name="owner_name_label"
                                        placeholder="Label" value="<?php echo $owner_name_label;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="owner_name_placeholder">Owner Name Placeholder<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_name_placeholder"
                                        name="owner_name_placeholder" placeholder="placeholder"
                                        value="<?php echo $owner_name_placeholder;?>" type="text">
                                </div>
                            </div>

                          
                            <div class="form-group">
                                <label class="control-label col-md-3" for="email_id_label">Owner Email Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="email_id_label" name="email_id_label"
                                        placeholder="Label" value="<?php echo $email_id_label;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="email_id_placeholder">Owner Email Placeholder<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="email_id_placeholder" name="email_id_placeholder"
                                        placeholder="placeholder" value="<?php echo $email_id_placeholder;?>"
                                        type="text">
                                </div>
                            </div>

                          
                            <div class="form-group">
                                <label class="control-label col-md-3" for="owner_contact_label">Owner Contact Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_contact_label" name="owner_contact_label"
                                        placeholder="Label" value="<?php echo $owner_contact_label;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="owner_contact_placeholder">Owner Contact Placeholder<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_contact_placeholder"
                                        name="owner_contact_placeholder" placeholder="placeholder"
                                        value="<?php echo $owner_contact_placeholder;?>" type="text">
                                </div>
                            </div>

                         
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_address_label">Property Address Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="property_address_label"
                                        name="property_address_label" placeholder="Label"
                                        value="<?php echo $property_address_label;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_address_placeholder">Property Address Placeholder<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="property_address_placeholder"
                                        name="property_address_placeholder" placeholder="placeholder"
                                        value="<?php echo $property_address_placeholder;?>" type="text">
                                </div>
                            </div>

                      
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_layout_maps_label">Property Layout And Maps Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="property_layout_maps_label"
                                        name="property_layout_maps_label" placeholder="Label"
                                        value="<?php echo $property_layout_maps_label;?>" type="text">
                                </div>
                            </div>
                         

                          
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_direction_orientation_label">Property Direction/Orientation Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="property_direction_orientation_label"
                                        name="property_direction_orientation_label" placeholder="Label"
                                        value="<?php echo $property_direction_orientation_label;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_direction_orientation_placeholder">Property Direction/Orientation Placeholder<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="property_direction_orientation_placeholder"
                                        name="property_direction_orientation_placeholder" placeholder="placeholder"
                                        value="<?php echo $property_direction_orientation_placeholder;?>" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_direction_orientation_message">
                                Property Direction/Orientation message<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                <textarea rows="3" class="textarea-information-class form-control property_direction_orientation_message"
                                        id="property_direction_orientation_message" name="property_direction_orientation_message"
                                        placeholder="Direction orientation message"><?= $property_direction_orientation_message ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_directioncompass_msg">
                                Property Compass message</label>
                                <div class="col-md-5">
                                <textarea rows="3" class="textarea-information-class form-control property_directioncompass_msg"
                                        id="property_directioncompass_msg" name="property_directioncompass_msg"
                                        placeholder="Compass message"><?= $property_directioncompass_msg ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3" for="property_directioncompass_link">
                                Property Compass if youtube_link</label>
                                <div class="col-md-5">
                                <textarea rows="3" class="textarea-information-class form-control property_directioncompass_link"
                                        id="property_directioncompass_link" name="property_directioncompass_link"
                                        placeholder="Enter if url any link"><?= $property_directioncompass_link ?></textarea>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label col-md-3" for="property_directioncompass_defaultvideo">
                                Property Compass Default Video</label>
                                <div class="col-md-5">
                                <textarea rows="3" class="textarea-information-class form-control property_directioncompass_defaultvideo"
                                        id="property_directioncompass_defaultvideo" name="property_directioncompass_defaultvideo"
                                        placeholder="Default Video"> $property_directioncompass_defaultvideo ?></textarea>
                                </div>
                            </div> -->


                            <div class="form-group">
                            <label class="control-label col-md-3" for="compass_image">Compass URL Image</label>
                            <div class="col-md-5">      
                                <a href="#" id="compass_image_btn" name="compass_image" class="compass_image_btn" data-toggle="modal" data-target="#browseImagecompass">Upload Image</a>
                                <input class="form-control" id="compass_image" name="compass_image" value="<?php echo $compass_image?>" type="hidden">
								<img class="img-previewcompass" src="<?php echo base_url().'uploads/'.$compass_image ?>" alt="" style="width: 150px;">
							</div>
                        </div>


                      
                            <div class="form-group">
                                <label class="control-label col-md-3" for="owner_date_time_birth_label">Owner Date And Time Of Birth Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_date_time_birth_label"
                                        name="owner_date_time_birth_label" placeholder="Label"
                                        value="<?php echo $owner_date_time_birth_label;?>" type="text">
                                </div>
                            </div>
                        
                          
                            <div class="form-group">
                                <label class="control-label col-md-3" for="owner_place_birth_label">Owner Place Of birth Label<span
                                        class="required">*</span></label>
                                <div class="col-md-5">
                                    <input class="form-control" id="owner_place_birth_label"
                                        name="owner_place_birth_label" placeholder="Label"
                                        value="<?php echo $owner_place_birth_label;?>" type="text">
                                </div>
                            </div>

                        <div class="form-group">
                            <label class="control-label col-md-3" for="faq_category">Faq Category ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="faq_category" name="faq_category"
                                    value="<?php echo $faq_category ; ?>" placeholder="Enter faq category">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="about_content_id">Footer Aboutus Content ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="about_content_id" name="about_content_id"
                                    value="<?php echo $about_content_id; ?>" placeholder="Enter Content ID ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="banner_id">Banner Id</label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_id" name="banner_id"
                                    value="<?php echo $banner_id ; ?>" placeholder="Enter banner_id">
                            </div>
                        </div>
                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-3">
                                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div> 

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image1</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImagecompass" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_imagecompass" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msgcompass"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        var input ='<div class="row">';
            input+='<div class="col-sm-2"></div>';
            input+='<div class="col-sm-1">';
            input+= '<div class="form-group inline_input">';
            input += '<label >Heading</label>';
            input+= '<input type="text" class="form-control q_heading" id="q_heading" name="q_heading" placeholder="Heading" value="">';
            input+= '</div>';
            input+= '</div>';
            input+='<div class="col-sm-2">';
            input+= '<div class="form-group inline_input">';
            input += '<label >Write Description</label>';
            input+= '<input type="text" class="form-control q_description" id="q_description" name="q_description" placeholder="Write Description" value="">';
            input+= '</div>';
            input+= '</div>';
            input+= '<div class="col-sm-1">';   
            input+= '<div class="form-group inline_input">' ;  
            input += '<label >INR Cost</label>';               
            input+= '<input class="form-control cost" id="cost" name="cost" value="" placeholder="Cost" type="number" min="0">';                      
            input+= '</div>';                   
            input+= '</div>';             
            input+= '<div class="col-sm-1">';   
            input+= '<div class="form-group inline_input">' ;  
            input += '<label >USD Cost</label>';               
            input+= '<input class="form-control usdcost" id="usdcost" name="usdcost" value="" placeholder="USDCost" type="number" min="0">';                      
            input+= '</div>';                   
            input+= '</div>';             
            input+= '<div class="col-sm-2">';                 
            input+= '<div class="form-group inline_input">';   
            input += '<label >Discounted INR Cost</label>';               
            input+= '<input class="form-control discounted_cost" id="discounted_cost" name="discounted_cost" value="" placeholder=" Discounted Cost" type="number" min="0">';                      
            input+= '</div>';                   
            input+= ' </div>';               
            input+= '<div class="col-sm-2">';                 
            input+= '<div class="form-group inline_input">';      
            input += '<label >Discounted USD Cost</label>';            
            input+= '<input class="form-control discounted_usd_cost" id="discounted_usd_cost" name="discounted_usd_cost" value="" placeholder=" Discounted USD Cost" type="number" min="0">';                      
            input+= '</div>';                   
            input+= ' </div>';               
            input+= '<div class="col-sm-1">';               
            input+= '<label for="" class="rem_question btn btn-sm btn-danger" >&times;</label>';                   
            input+= '</div>';                
            input+= '</div>';
            // $('.add_inputs').append(input);

        $('body').on('click','.add_question',function(){
            $('.add_inputs').append(input);

        });

        $('body').on('click','.rem_question',function(){
           
           $(this).closest('.row').remove();

       });

        //-----------------------------------------------------------------------
        /*
        * validation
        */
        $('#edit_vastu_form').validate({
            ignore:[],
            rules: {
                page_heading:{
                    required: true,
                },

                short_description: {
                    required: true,
                },

                url: {
                    required: true,
                },

                page_title: {
                    required: true,
                },
                owner_place_birth_label: {
                    required: true,
                },
                owner_date_time_birth_label: {
                    required: true,
                },
                property_direction_orientation_message: {
                    required: true,
                },
                property_direction_orientation_placeholder: {
                    required: true,
                },
                property_direction_orientation_label: {
                    required: true,
                },
                property_layout_maps_label: {
                    required: true,
                },
                property_address_placeholder: {
                    required: true,
                },
                property_address_label: {
                    required: true,
                },
                owner_contact_placeholder: {
                    required: true,
                },
                owner_contact_label: {
                    required: true,
                },
                email_id_placeholder: {
                    required: true,
                },
                email_id_label: {
                    required: true,
                },
                owner_name_placeholder: {
                    required: true,
                },
                owner_name_label: {
                    required: true,
                },
                meta_descp: {
                    required: true,
                },
                page_content: {
                        required: function(textarea) {
                                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                                return editorcontent.length === 0;
                        }
                },
                // q_heading:{
                //     required: true,
                // },
                // q_description:{
                //     required: true,
                // },
                // cost:{
                //     required: true,
                // },
                // discounted_cost:{
                //     required: true,
                // },
                detail_image: {
                    required: true,
                },
                detail_image_position: {
                    required: true,
                },
                is_home: {
                    required: true,
                },
                home_msg: {
                    required: true,
                },
                
            },
            messages: {

                page: {
                    required: "Page Heading  is required",
                },
								
                data: {
                    required: "Content is required",
                },
                header_image: {
                    required: "Image is required",
                },
                video_link: {
                    required: "Vide Link is required",
                },
                owner_place_birth_label: {
                    required: 'This Field is required',
                },
                owner_date_time_birth_label: {
                    required: 'This Field is required',
                },
                property_direction_orientation_message: {
                    required: 'This Field is required',
                },
                property_direction_orientation_placeholder: {
                    required: 'This Field is required',
                },
                property_direction_orientation_label: {
                    required: 'This Field is required',
                },
                property_layout_maps_label: {
                    required: 'This Field is required',
                },
                property_address_placeholder: {
                    required: 'This Field is required',
                },
                property_address_label: {
                    required: 'This Field is required',
                },
                owner_contact_placeholder: {
                    required: 'This Field is required',
                },
                owner_contact_label: {
                    required: 'This Field is required',
                },
                email_id_placeholder: {
                    required: 'This Field is required',
                },
                email_id_label: {
                    required: 'This Field is required',
                },
                owner_name_placeholder: {
                    required: 'This Field is required',
                },
                owner_name_label: {
                    required: 'This Field is required',
                },is_home: {
                    required: 'This Field is required',
                },
                home_msg: {
                    required: 'This Field is required',
                },

            },

            submitHandler: function (form) {
                var vastu_id = $('#vastu_id').val();
                var page_heading = $('#page_heading').val();
                var short_description = $('#short_description').val();
                var url = $('#url').val();
                var page_title = $('#page_title').val();
                var meta_tag = $('#meta_tag').val();
                var meta_descp = $('#meta_descp').val();
                var page_content =  CKEDITOR.instances['page_content'].getData();
                var detail_image = $('#detail_image').val();
                var detail_image_position = $('#detail_image_position').val();
                var faq_category = $('#faq_category').val();
				var banner_id = $('#banner_id').val();
            var about_content_id = $('#about_content_id').val();


              var owner_name_placeholder = $('#owner_name_placeholder').val();
            var owner_name_label = $('#owner_name_label').val();
    var email_id_placeholder = $('#email_id_placeholder').val();
            var email_id_label = $('#email_id_label').val();

            var owner_contact_label = $('#owner_contact_label').val();
            var owner_contact_placeholder = $('#owner_contact_placeholder').val();



    var property_address_placeholder = $('#property_address_placeholder').val();
            var property_address_label = $('#property_address_label').val();


            var property_layout_maps_label = $('#property_layout_maps_label').val();

    var property_direction_orientation_placeholder = $('#property_direction_orientation_placeholder').val();
            var property_direction_orientation_label = $('#property_direction_orientation_label').val();


            var owner_place_birth_label = $('#owner_place_birth_label').val();
            var owner_date_time_birth_label = $('#owner_date_time_birth_label').val();
            var property_direction_orientation_message = $('#property_direction_orientation_message').val();



           // var property_directioncompass_defaultvideo = $('#property_directioncompass_defaultvideo').val();
            var property_directioncompass_link = $('#property_directioncompass_link').val();
            var property_directioncompass_msg = $('#property_directioncompass_msg').val();
            var compass_image = $('#compass_image').val();
            var is_home = $('#is_home').val();
            var home_msg = $('#home_msg').val();


            console.log(owner_name_placeholder)
            console.log(owner_name_label)
            console.log(email_id_placeholder)
            console.log(email_id_label)
            console.log(owner_contact_label)
            console.log(owner_contact_placeholder)
            console.log(property_address_placeholder)
            console.log(property_address_label)
            console.log(property_layout_maps_label)

            console.log(property_direction_orientation_placeholder)
            console.log(property_direction_orientation_label)
            console.log(owner_place_birth_label)
            console.log(owner_date_time_birth_label)
            console.log(property_direction_orientation_message)
           

                // var additional_json = [];
                // $('.add_inputs').find('.row').each(function(){
                //     additional_json.push({
                //         'q_heading':$(this).find('.q_heading').val(),
                //         'q_description':$(this).find('.q_description').val(),
                //         'cost':$(this).find('.cost').val(),
                //         'usdcost':$(this).find('.usdcost').val(),
                //         'discounted_cost':$(this).find('.discounted_cost').val(),
                //         'discounted_usd_cost':$(this).find('.discounted_usd_cost').val(),
                //     });
                // });
                // console.log(additional_json);
                $.post(APP_URL + 'configure_access/add_new_vastu_data', {
                    id: vastu_id,
                    page_heading:page_heading,
                    short_description:short_description,
                    url:url,
                    faq_category: faq_category,
					banner_id: banner_id,
					about_content_id: about_content_id,
                    page_title:page_title,
                    meta_tag:meta_tag,
                    meta_descp:meta_descp,
                    page_content:page_content,
                    detail_image:detail_image,
                    detail_image_position:detail_image_position,
                    is_home:is_home,
                    home_msg:home_msg,

                                        owner_name_label:owner_name_label,
                    owner_name_placeholder:owner_name_placeholder,
                    email_id_label:email_id_label,
                    email_id_placeholder:email_id_placeholder,
                    owner_contact_placeholder: owner_contact_placeholder,
                    owner_contact_label: owner_contact_label,
                    property_address_label:property_address_label,
                    property_address_placeholder:property_address_placeholder,
                    property_layout_maps_label:property_layout_maps_label,
                    property_direction_orientation_label:property_direction_orientation_label,
                    property_direction_orientation_placeholder:property_direction_orientation_placeholder,
                    owner_place_birth_label:owner_place_birth_label,
                    owner_date_time_birth_label:owner_date_time_birth_label,
                    property_direction_orientation_message:property_direction_orientation_message,
                    compass_image:compass_image,
                    property_directioncompass_msg:property_directioncompass_msg,
                    property_directioncompass_link:property_directioncompass_link,
                 //   property_directioncompass_defaultvideo:property_directioncompass_defaultvideo,

                    // additional_json:additional_json
                   
                },

                function (response) {
                    $("html, body").animate({scrollTop: 0}, "slow");
                    $('#err_page_form').empty();
                    if (response.status == 200) {
                        $('#err_page_form').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                        setTimeout(() => {
                            window.location.href = APP_URL +'configure_access/edit_Vastu';
                        }, 1000);
                    }
                    else if (response.status == 201) {
                        $('#err_page_form').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
                    }
                    // $('#idd').val(0);
                    // $('#page').val('');
                    // $('#data').val('');
                    // $('#video_link').val('');
                    // $('#header_image').val('');
                    // $('.img-preview').attr('src','');
                }, 'json');
                return false;
            }
        });

        //------------------------------------------------------------------------//
    /**
     * This part of script is used to upload image for add section.
     */
    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#detail_image').val(response.filename);
                $('#blog_image1-error').css({"display": "none"});
				
				
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });
    $('#upload_imagecompass').ajaxForm({
        dataType: 'JSON',
        success: function (response) {
            if (response.status == 200) {
                $(".img-previewcompass").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImagecompass').modal('hide');
                $('#compass_image').val(response.filename);
                $('#blog_image1-error').css({"display": "none"});
				
				
            } else {
                $('#browseImagecompass').modal('hide');
                alert(response.message);
            }
            $.unblockUI();
            return false;
        }
    });

    });
</script>
