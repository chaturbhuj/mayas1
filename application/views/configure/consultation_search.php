
<script src="<?php echo base_url(); ?>assets/js/configure/consultation.js"></script> 
<link rel="stylesheet" href="https://mayasastrology.com/assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.css">
<script src="https://mayasastrology.com/assets/js/plugins/jquery-ui-timepicker-0.3.3/jquery.ui.timepicker.js"></script>
<script>		
    $(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});
</script>

<?php 
	$heading = 'Search Previous Consultation';
	$consultation_list= false;
?>




<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>     
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2><?php echo $heading ;?></h2>
						<div class="container">
							<div class="row">
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="full_name">Code Id</label>
									<input type="text" class="form-control" id="code_id" placeholder="Name">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="full_name">Name</label>
									<input type="text" class="form-control" id="search_full_name" placeholder="Name">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="inputEmail">Email address</label>
									<input type="email" class="form-control" id="search_inputEmail" placeholder="Email">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="inputContact">Contact</label>
									<input type="number" class="form-control" id="search_inputContact" placeholder="Contact">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="inputDateDate">Date of Birth</label>
									<input type="email" class="form-control date" id="search_inputDateDate" placeholder="Date">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="inputTimeMin">Time of Birth</label>
									<input type="email" class="form-control time" id="search_inputTimeMin" placeholder="Time">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<label for="birthCity">Place of Birth</label>
									<input type="email" class="form-control" id="search_birthCity" placeholder="Place">
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							  <div class="col-lg-2">
								<div class="form-group">
									<button type="submit" id="btn_search" class="btn btn-default">Search</button>
								</div><!-- /input-group -->
							  </div><!-- /.col-lg-6 -->
							   
							</div><!-- /.row -->
						</div>
						<div id="headerMsg"></div>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="">
                            <thead>
								<tr>
									<th class="text-center"> Sr. No. </th>			
									<th class="text-center">Consult Code</th>
									<th class="text-center">Consult Type</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Gender</th>
									<th class="text-center">Date of Birth</th>
									<th class="text-center">Time of Birth</th>
									<th class="text-center">Place of Birth</th>
									<th class="text-center">Email ID</th>
									<th class="text-center">Appointment Status</th>
									<th class="text-center">Action</th>
									
								</tr>
							</thead>
							<tbody id="table_data">
							<?php //var_dump($consultation_list);
                            if ($consultation_list == 0) {
                                //echo 'No record found into database';
                            } else {
								var_dump($consultation_list);

                                $content = '';
                                
                                $i = 1;
                                foreach ($consultation_list as $value) {
									$gender = $value['gender'];
                                    $content .= '<tr class="darker-on-hover">';
									
									
									$content .= '<td class="text-center"  >' . $i . '</br> <input type="checkbox" class="mcheckbox" name="foo" consultation_id="' . $value['consultation_id'] . '" inputContact="' . $value['inputContact'] . '" inputEmail="' . $value['inputEmail'] . '"  full_name="' . $value['full_name'] . '"></td>';
									
									
                                    $content .= '<td class="text-center">' . $value['consultation_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['consultantType'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['full_name'] . '</td>';
                                    $content .= '<td class="text-center" name="'.$value['gender'].'">' . (substr($gender, 0, 1)). '</td>';
                                    $content .= '<td class="text-center">'.$value['inputDateDate'].'</td>';
									$content .= '<td class="text-center">'.$value['inputTimeMin'].'</td>';
									$content .= '<td class="text-center">'.$value['birthCity'].'</td>';
									$content .= '<td class="text-center">' . $value['inputEmail'] . '</br>Mo - ' . $value['inputContact'] . '</td>';
									
									if($value['appointment_yes'] == 'consultation-done'){
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-success">' . $value['appointment_yes'] . '</span></a></td>';
									}else if($value['appointment_yes'] == 'non-eligible'){
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-danger">' . $value['appointment_yes'] . '</span></a></td>';
									}else{
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-default">' . $value['appointment_yes'] . '</span></a></td>';
									}
									
                                    $content .= '</tr>';
                                    $i++;
                                }
                               
                                echo $content;
                            }
                            ?>
							</tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!---------------------------- Modal for Browse Change Status-------------------------->
<div class="modal fade" id="browseChangeStatus" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Change Appointment Status</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well" id="category_form2" method="post" enctype="multipart/form-data">
						<input class="form-control" id="category_id2" name="category_id2" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="category_status"> Status<span class="required">*</span></label>
							<div class="col-md-9">
								<select class="form-control" id="category_status" name="category_status">
									<option value="">Select Status</option>
									<option value="called">Called</option>
									<option value="eligible">Eligible</option>
									<option value="non-eligible">Non-Eligible</option>
									<option value="consultation-done">Consultation - done</option>
									<option value="sms-email">SMS/Email</option>
									<option value="pending">Pending</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="consultation_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Consultation Detail</h4>
                </div>
                <div class="modal-body">
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="consultantType">Consultant Type : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="consultantType" name="consultantType"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="full_name">Full Name : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="full_name" name="full_name"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="gender">Gender : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="gender" name="gender"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="date_of_birth">Date of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="date_of_birth" name="date_of_birth"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="date_of_time">Time of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="date_of_time" name="date_of_time"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCountry">Birth Country : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCountry" name="birthCountry"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthState">Birth State : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthState" name="birthState"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCity">Birth City : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCity" name="birthCity"></span>
                        </div>
                    </div>
					<!--<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputEmail">Email ID : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputEmail" name="inputEmail"></span>
                        </div>
                    </div>-->
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputContact">Email/Contact NO. : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputContact" name="inputContact"></span>
                        </div>
                    </div>
					<!--option field-->
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="educationQualification">Education Qualification : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="educationQualification" name="educationQualification"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="currentProfession">Current Profession : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="currentProfession" name="currentProfession"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerName">Partner Name : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerName" name="partnerName"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partner_date_of_birth">Partner Date of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partner_date_of_birth" name="partner_date_of_birth"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partner_date_of_time">Partner Time of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partner_date_of_time" name="partner_date_of_time"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthCountry">Partner Birth Country : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthCountry" name="partnerBirthCountry"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthState">Partner Birth State : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthState" name="partnerBirthState"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthCity">Partner Birth City : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthCity" name="partnerBirthCity"></span>
                        </div>
                    </div>
					
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5 question1 question1_label" for="question1"></label>
                        <div class="col-md-7">
                            <span class="full_namel" id="question1" name="question1"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5 question2 question2_label" for="question2"></label>
                        <div class="col-md-7">
                            <span class="full_namel" id="question2" name="question2"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputQuestion">Question : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputQuestion" name="inputQuestion"></span>
                        </div>
                    </div>
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!---------------------------- Modal for Browse Add Notes-------------------------->
<div class="modal fade" id="browseNewNote" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3> Add New Note  </h3>
            </div> 
            <div class="modal-body row">
				<p class="name_cunsultants" style="margin-left:15px;"></p>
				<div class="col-md-12">
					<form class="well form-inline" id="note_form" method="post" enctype="multipart/form-data">
					
						<input class="form-control" id="c_id" name="c_id" value="" type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<textarea class="form-control" style="width: 100%;" rows="4" id="typed_note" name="typed_note" placeholder="Write Something Here....." type="text"></textarea>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>


<!---------------------------- Modal for Browse View Added Notes-------------------------->
<div class="modal fade" id="browseAllNotes" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3> All Notes  For &nbsp;<span class="name_noter"></span></h3>
            </div> 
            <div class="modal-body row">
				<div id="headMsg25"></div>
				<p class="msg_cunsultants" style="margin-left:15px;"></p>
				<div class="col-md-12">
					
					<div class="col-md-12" id="all_nota_" style="padding: 15px 0px 15px 0px">
						<!--<p style="width: 100%;background: #fff;padding: 5px;border-radius: 5px;" >  fcwu cuwec dcuwc c c </p>-->
					</div>
					<div class="clearfix"></div>
						
				</div>
			</div>
        </div>
    </div>
</div>


<script>
$('document').ready(function () {
	/*
     * This script is used to Add new Note
     */
	$('body').on('click', '#btn_search', function () {	
		
		var search_full_name = $('#search_full_name').val();
		var search_inputEmail = $('#search_inputEmail').val();
		var search_inputContact = $('#search_inputContact').val();
		var search_inputDateDate = $('#search_inputDateDate').val();
		var search_inputTimeMin = $('#search_inputTimeMin').val();
		var search_birthCity = $('#search_birthCity').val();
		var code_id = $('#code_id').val();
		
		$.post(APP_URL + 'page/get_search_result', {
			search_full_name: search_full_name,
			code_id: code_id,
			search_inputEmail: search_inputEmail,
			search_inputContact: search_inputContact,
			search_inputDateDate: search_inputDateDate,
			search_inputTimeMin: search_inputTimeMin,
			search_birthCity: search_birthCity,
			},
			function (response) {
				$('#headerMsg').empty();
				$('#headMsg25').empty();
				$('#all_nota_').empty();
				$('.msg_cunsultants').empty();
				$('#table_data').empty();
				if (response.status == 200) {
					var i=1;
					var content = '';
					$(response.data).each(function(key,value){
						content += '<tr class="darker-on-hover">';
						content += '<td class="text-center"  >' + i + '</br> <input type="checkbox" class="mcheckbox" name="foo" consultation_id="' + value['consultation_id'] + '" inputContact="' + value['inputContact'] + '" inputEmail="' + value['inputEmail'] + '"  full_name="' + value['full_name'] + '"></td>';
						
						content += '<td class="text-center">' + value['consultation_id'] + '</td>';
						content += '<td class="text-center">' + value['consultantType'] + '</td>';
						content += '<td class="text-center">' + value['full_name'] + '</td>';
						content += '<td class="text-center" name="'+ value['gender'] +'"></td>';
						content += '<td class="text-center">'+ value['inputDateDate'] +'</td>';
						content += '<td class="text-center">'+ value['inputTimeMin'] +'</td>';
						content += '<td class="text-center">'+ value['birthCity'] +'</td>';
						content += '<td class="text-center">' + value['inputEmail'] + '</br>Mo - ' + value['inputContact'] + '</td>';
						
						if(value['appointment_yes'] == 'consultation-done'){
							content += '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' + value['consultation_id'] + '" value="' + value['appointment_yes'] + '"><span class="label label-success">' + value['appointment_yes'] + '</span></a></td>';
						}else if(value['appointment_yes'] == 'non-eligible'){
							content += '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' + value['consultation_id'] + '" value="' + value['appointment_yes'] + '"><span class="label label-danger">' + value['appointment_yes'] + '</span></a></td>';
						}else{
							content += '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' + value['consultation_id'] + '" value="' + value['appointment_yes'] + '"><span class="label label-default">' + value['appointment_yes'] + '</span></a></td>';
						}
						content += '<td class="text-center"><a href="javascript:void(0);" class="view_consultation" data-toggle="modal" data-target="#my_user_edit" name="' + value['consultation_id'] + '" inputQuestion="' + value['inputQuestion'] + '" educationQualification="' + value['educationQualification'] + '" currentProfession="' + value['currentProfession'] + '" partnerName="'+ value['partnerName'] +'" partnerDateDate="' + value['partnerDateDate'] + '" partnerTimeMin="' + value['partnerTimeMin'] + '" partnerBirthCountry="' + value['partnerBirthCountry'] + '" partnerBirthState="' + value['partnerBirthState'] + '" partnerBirthCity="' +value['partnerBirthCity'] + '" birthCountry="' + value['birthCountry'] + '" birthState="' + value['birthState'] + '" birthCity="' + value['birthCity'] + '"  question1_label="' + value['question1_label'] + '" question2_label="' + value['question2_label'] + '" question1="' + value['question1'] + '" question2="' + value['question2'] + '"><span class="label label-success">View</span></a>';
						content += '&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_consultation"  name="' + value['consultation_id'] + '" value=""><span class="label label-danger">Remove </span></a>';
						content += '&nbsp;&nbsp;<a href="javascript:void(0);" class="add_note_consultation"  name=' + value['consultation_id'] + ' value="' + value['full_name'] + '"><span class="label label-default">Add Note </span></a>';
						content += '&nbsp;&nbsp;<a href="javascript:void(0);" class="view_note_consuion"  name=' + value['consultation_id'] + ' value="' + value['full_name'] + '"><span class="label label-default">View All Notes </span></a></td></tr>';
						content += '</tr>';
									
						i++;
					});
				
					//console.log(content);
					$('#table_data').append(content);
				}else{
					content = '<h4>No record found into database</h4>';
						
					$('#table_data').append(content);
				}
			}, 'json');
		return false;
	});
});
</script>