<script src="<?php echo base_url(); ?>assets/js/blog.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 

	//print_r ($blog);
	if($blog){
		$blog_heading = $blog[0]['blog_heading'];
		$blog_sub_heading = $blog[0]['blog_sub_heading'];
		$blog_id = $blog[0]['blog_id'];
		$blog_writer = $blog[0]['blog_writer'];
		$status = $blog[0]['status'];
		$blog_description = $blog[0]['blog_description'];
		$blog_image = $blog[0]['blog_image'];
		$blog_choose = $blog[0]['blog_choose'];
		
		
	}else{
		$blog_heading = '';
		$blog_sub_heading = '';
		$blog_id = 0;
		$blog_writer = '';
		$status = '';
		$blog_description = '';
		$blog_image = '';
		$blog_choose = '';
		if(isset($_GET['blog_id'])){
			$blog_id  = $_GET['blog_id'];
			
		}
		
		
	}
?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add New Blog</h1>
		<a href="<?php echo base_url();?>configure_access/edit_blog" class="btn btn-default pull-right addAds">View All Blogs</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_blog_form" method="post" class="form-horizontal">
					
						<input type="hidden" id="blog_id" name="blog_id" value="<?php echo $blog_id;?>">
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="blog_heading">Blog Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="blog_heading" name="blog_heading" value="<?php echo $blog_heading;?>" placeholder="Blog Heading">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="blog_sub_heading">Blog Sub Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="blog_sub_heading" name="blog_sub_heading" value="<?php echo $blog_sub_heading;?>" placeholder="Blog Sub Heading">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="blog_choose">Choose Blog Option<span class="required">*</span></label>
                            <div class="col-md-5">
                               <select class="form-control"  id="blog_choose" name="blog_choose" value="" >
									<option value="">select column</option>
									<option value="yes"  <?php if($blog_choose == 'yes' ) echo 'selected';?>>yes</option>									
									<option value="no" <?php if($blog_choose == 'no' ) echo 'selected';?>>no</option>									
																
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="blog_description">Blog Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea class="form-control ckeditor blog_description" id="blog_description" name="blog_description" placeholder="Blog Description" row="2"><?php echo $blog_description;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="blog_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="blog_image_btn" name="blog_image" class="blog_image_btn" data-toggle="modal" data-target="#browseImage">Upload Image</a>
                                <input class="form-control" id="blog_image" name="blog_image" value="<?php echo $blog_image;?>" type="hidden">
								
								<?php if($blog_image != ''){
									echo '<img class="img-preview" src="'.base_url().'uploads/'.$blog_image.'" alt="" style="width: 150px;">';
								}else{
									echo '<img class="img-preview" src="" alt="" style="width: 150px;">';
								}?>
								
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label class="control-label col-md-2" for="blog_writer">Blog writer<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="blog_writer" name="blog_writer" value="<?php echo $blog_writer;?>" placeholder="Blog writer">
                            </div>
                        </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
