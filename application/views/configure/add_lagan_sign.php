<script src="<?php echo base_url(); ?>assets/js/lagan.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Lagan Sign</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_lagan_sign_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_lagan_sign_Form" method="post" class="form-horizontal">
						<div class="form-group">
                            <label class="control-label col-md-2" for="lagan_name">Lagan Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control" id="lagan_name" name="lagan_name"> 
										<option value="0">Select Lagan Name</option>
										<option value="Aries">Aries</option>
										<option value="Taurus">Taurus</option>
										<option value="Gemini">Gemini</option>
										<option value="Cancer">Cancer</option>
										<option value="Leo">Leo</option>
										<option value="Virgo">Virgo</option>
										<option value="Libra">Libra</option>
										<option value="Scorpio">Scorpio</option>
										<option value="Sagittarius">Sagittarius</option>
										<option value="Capricorn">Capricorn</option>
										<option value="Aquarius">Aquarius</option>
										<option value="Pisces">Pisces</option>
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="life_stone_name">Life Birth Stone Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="life_stone_name" name="life_stone_name" placeholder="Life Birth Stone Name" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="punya_stone_name">Punya Lucky Stone<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="punya_stone_name" name="punya_stone_name" placeholder="Punya Lucky Stone Name" type="text">
                            </div>
							
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="bhagya_stone_name"> Bhagya Fortune Stone Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="bhagya_stone_name" name="bhagya_stone_name" placeholder="Bhagya Fortune Stone Name" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
