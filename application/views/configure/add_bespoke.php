<script src="<?php echo base_url(); ?>assets/js/services.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>

<?php 
	
	if($services){
		$service_id = $services[0]['service_id'];
		$services_name = $services[0]['services_name'];
		$services_type = $services[0]['services_type'];		
		$consultant_type = $services[0]['consultant_type'];
		$col = $services[0]['col'];
	
		$status = $services[0]['status'];
		$services_main_description = $services[0]['services_main_description'];
		$services_description = $services[0]['services_description'];
		$services_image = $services[0]['services_image'];
		$service_detail_image = $services[0]['service_detail_image'];
		$service_step_image = $services[0]['service_step_image'];
	
		
		$premium_type = $services[0]['premium_type'];
		$services_url = $services[0]['services_url'];
		$services_title = $services[0]['services_title'];
		$services_dec_meta = $services[0]['services_dec_meta'];
		$services_keyword_meta = $services[0]['services_keyword_meta'];
	}else{
		$service_id = 0;
		
		$premium_type = '';
		$col = '';
		$services_name = '';
		$services_type = '';
	
		$consultant_type = '';
		
		$attachments = '';
		$status = '';
		$services_main_description = '';
		$services_description = '';
		$services_image = '';
		$service_detail_image = '';
		$service_step_image = '';
		if(isset($_GET['service_id'])){
			$service_id  = $_GET['service_id'];
			
		}
		$services_url = '';
		$services_title ='';
		$services_dec_meta ='';
		$services_keyword_meta ='';
		
	}
?>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Bespoke Services</h1>
		<a href="<?php echo base_url();?>configure_access/edit_bespoke_services" class="btn btn-default pull-right addAds">View All Bespoke Services</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_services_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_services_form_bespoke" method="post" class="form-horizontal">
					
                        <input type="hidden" id="service_id" name="service_id" value="<?php echo $service_id;?>">
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_name">Services Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_name" name="services_name" value="<?php echo $services_name;?>" placeholder="Services Name" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_url">Url<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" style="text-transform: lowercase;" id="services_url" name="services_url" value="<?php echo $services_url;?>" placeholder="Services Url" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_title">Services Title<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_title" name="services_title" value="<?php echo $services_title;?>" placeholder="Services title" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_dec_meta">Services desc meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_dec_meta" name="services_dec_meta" value="<?php echo $services_dec_meta;?>" placeholder="Services desc meta" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_keyword_meta">Services Keyword meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="services_keyword_meta" name="services_keyword_meta" value="<?php echo $services_keyword_meta;?>" placeholder="Services  Keyword meta" type="text">
                            </div>
                        </div>
						
						<div hidden class="form-group">
                            <label class="control-label col-md-2" for="services_type">Services Type<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="services_type" name="services_type" value="" >
									<option value="0">Select type of service</option>
									<option value="main" <?php if($services_type == 'main' ) echo 'selected';?> >Main</option>									
									<option value="other" <?php if($services_type == 'other' ) echo 'selected';?>>Other</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
							<label for="consultant_type" class="col-md-2 control-label">Select type of consultation<span class="required">*</span></label>
							<div class="col-md-5">
								<select name="consultant_type" id="consultant_type" class="form-control consultant_type" >
									<option value="" name="">Select the type of free consultation</option>
									<option value="Career" <?php if($consultant_type == 'Career' ) echo 'selected';?> name="career">Question Related to Career</option>
									<option value="BirthStone" <?php if($consultant_type == 'BirthStone' ) echo 'selected';?> name="birth_stone">Question Related to Birth Stone</option>
									<option value="HoroscopeQuery" <?php if($consultant_type == 'HoroscopeQuery' ) echo 'selected';?>  name="horoscope">Question Related to Horoscope Query</option>
									<option value="MatchMaking" <?php if($consultant_type == 'MatchMaking' ) echo 'selected';?> name="match_making">Question Related to Match Making</option>
								</select>
							</div>
						</div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_main_description">Services Main Description<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control" id="services_main_description" name="services_main_description" placeholder="Services Main Description" row="2"><?php echo $services_main_description;?></textarea>
                            </div>
                        </div>
						<div  class="form-group">
                            <label class="control-label col-md-2" for="premium_type">Services Type<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="premium_type" name="premium_type" value="" >
									<option value="">Select type of premium service</option>
									<option value="question"<?php if($premium_type == 'question' ) echo 'selected';?>  >Question</option>									
									<option value="minutes" <?php if($premium_type == 'minutes' ) echo 'selected';?> >Minutes</option>
								</select>
                            </div>
                        </div>
						<div  class="form-group">
                            <label class="control-label col-md-2" for="premium_type">Col<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="col" name="col" value="" >
									<option value="">select column</option>
									<option value="3"  <?php if($col == '3' ) echo 'selected';?>>3</option>									
									<option value="4" <?php if($col == '4' ) echo 'selected';?>>4</option>									
									<option value="6" <?php if($col == '6' ) echo 'selected';?>>6</option>									
									<option value="12" <?php if($col == '12' ) echo 'selected';?>>12</option>									
									
								</select>
                            </div>
                        </div>
						
						
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="services_description">Services Description<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor services_description" id="services_description" name="services_description" placeholder="Services Description" row="2"><?php echo $services_description;?></textarea>
                            </div>
                        </div>
							
                        <div class="form-group">
                            <label class="control-label col-md-2" for="services_image">Service heading image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="services_image_btn" name="services_image" class="services_image_btn" data-toggle="modal" data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="services_image" name="services_image" value="<?php echo $services_image;?>" type="hidden">
								<img class="img-preview1" src="<?php echo base_url().'uploads/'.($services[0]['services_image']);?>" alt="" style="width: 150px;">
							</div>
                        </div>
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="service_detail_image">Service detail image<span class="required">*</span></label>
                            <div class="col-md-8">
                                <textarea rows="3" class="form-control ckeditor service_detail_image" id="service_detail_image" name="service_detail_image" placeholder="Services detail image" row="2"><?php echo $service_detail_image;?></textarea>
                            </div>
                        </div>
						
					
						<div class="form-group">
                            <label class="control-label col-md-2" for="service_step_image">Service process step image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="services_image_btn" name="service_step_image" class="services_image_btn" data-toggle="modal" data-target="#browseImage3">Upload Image</a>
                                <input class="form-control" id="service_step_image" name="service_step_image" value="<?php echo $service_step_image;?>" type="hidden">
								<img class="img-preview3" src="<?php echo base_url().'uploads/'.($services[0]['service_step_image']);?>" alt=""style="width: 150px;">
							</div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image3" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

