<script src="<?php echo base_url(); ?>assets/js/admin_consultation.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
.service_photo{
	width: 60px;
}

</style>
 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of All Consultation Content</h1>
		<a href="<?php echo base_url();?>configure_access/add_free_consultation" class="btn btn-default pull-right addAds">Add Free Consultation</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_services"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($consultation_Data == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Consultation Name</th><th class="text-center">Main Description</th><th class="text-center">Description</th><th class="text-center">Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($consultation_Data as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['consultation_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['main_description'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['description'] . '</td>';
                                    $content .= '<td class="text-center"><img class="service_photo" src="' . base_url() . 'uploads/' . $value['c_image'] . '"></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_free_consultation?consultation_id='. $value['consultation_id'] . '"  name=' . $value['consultation_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0)" class="remove_date"  name="'. $value['consultation_id'] .'" value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>