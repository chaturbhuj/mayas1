<link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script> 

<script>
    $(function () {
        var oTable = $('#consultation_table').dataTable();
    });
</script>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>List of meta data</h2>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultation_table">';
                            <thead>
								<tr>
									<th class="text-center">S.No.</th>
									<th class="text-center">Url</th>
									<th class="text-center">Title</th>
									<th class="text-center">dec meta</th>
									<th class="text-center">keyword</th>
									
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
                            if ($meta_data == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($meta_data as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['url'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['title'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['dec_meta'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['keyword_meta'] . '</td>';
                                    
                                    $content .= '<td class="text-center"><a href="'.base_url().'meta/meta_data?meta_id='.$value['meta_id'].'" class="" ><span class="label label-success">View</span></a></td>';
                                    
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





