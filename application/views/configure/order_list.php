<link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script> 

<script>
    $(function () {
        var oTable = $('#consultation_table').dataTable();
    });
	

</script>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>List of Service All Order</h2>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultation_table">';
                            <thead>
								<tr>
									<th class="text-center">S.No.</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Email</th>
									<th class="text-center">Payment ID</th>
									<th class="text-center">Order ID</th>
									<th class="text-center">Item Name </th>
									<th class="text-center">Item Cost </th>
									<th class="text-center">Grand Total </th>
									<th class="text-center">Address</th>
									<th class="text-center">City</th>
									<th class="text-center">State</th>
									<th class="text-center">Country</th>
									<th class="text-center">Contact NO.</th>
									<th class="text-center">Payment Date</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
                            if ($service_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($service_list as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['email'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['payment_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['order_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['itemname'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['itemcost'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['grandtotal'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['address'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['city'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['state'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['country'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['contectno'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['payment_date'] . '</td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/view_service_detail?order_payment_id='.$value['order_payment_id'].'" class="" name=' . $value['order_payment_id'] . ' value=""><span class="label label-success">View</span></a>';
                                    
                                    $i++;
                                }
                                //$content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
							</tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<script>
    $(function () {
        var oTable = $('#consultatidsdson_table').dataTable();
    });
</script>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>List of Product All Order</h2>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultatidsdson_table">';
                            <thead>
								<tr>
									<th class="text-center">S.No.</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Email</th>
									<th class="text-center">Payment ID</th>
									<th class="text-center">Order ID</th>
									<th class="text-center">Item Name </th>
									<th class="text-center">Item Cost </th>
									<th class="text-center">Grand Total </th>
									<th class="text-center">Address</th>
									<th class="text-center">City</th>
									<th class="text-center">State</th>
									<th class="text-center">Country</th>
									<th class="text-center">Contact NO.</th>
									<th class="text-center">Payment Date</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
                            if ($product_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($product_list as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['email'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['payment_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['order_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['itemname'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['itemcost'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['grandtotal'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['address'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['city'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['state'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['country'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['contectno'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['payment_date'] . '</td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/view_product_detail?order_payment_id='.$value['order_payment_id'].'" class="" name=' . $value['order_payment_id'] . ' value=""><span class="label label-success">View</span></a>';
                                    
                                    $i++;
                                }
                                //$content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
							</tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--modal-->

<script>
    $(function () {
        var oTable = $('#consultatidsdson_table').dataTable();
    });
</script>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>Instant Consultation Service</h2>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultatidsdson_table">';
                            <thead>
								<tr>
									<th class="text-center">S.No.</th>
									<th class="text-center">Order Id</th>
									<th class="text-center">Consultant Type</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Gender</th>
									<th class="text-center">Input Date</th>
									<th class="text-center">Input Date Month</th>
									<th class="text-center">Input Date Year</th>
									<th class="text-center">Input Time Hr</th>
									<th class="text-center">Input Time Min</th>
									<th class="text-center">Input Time Sec</th>
									<th class="text-center">Birth Country</th>
									<th class="text-center">Birth State</th>
									<th class="text-center">Birth City</th>
									<th class="text-center">Input Email</th>
									<th class="text-center">Input Contact</th>
								
									
									<th class="text-center">Created Date</th>
									<th class="text-center">Created Time</th>
									<th class="text-center">Status</th>
									<th class="text-center">Payment Method</th>
									<th class="text-center">Question Time</th>
									<th class="text-center">Questionyaminute</th>
									<th class="text-center">Amount</th>
									
									
								</tr>
							</thead>
							<tbody>
							<?php
                            if ($consultant_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($consultant_list as $value) {
                                  
								   $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
								    $content .= '<td class="text-center">' . $value['order_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['consultantType'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['full_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['gender'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputDateDate'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputDateMonth'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputDateYear'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputTimeHr'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputTimeMin'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputTimeSec'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['birthCountry'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['birthState'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['birthCity'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputEmail'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputContact'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['created_date'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['created_time'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['status'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['paymentmethod'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['questiontime'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['questionyaminute'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['amount'] . '</td>';
                                    
                                   // $content .= '<td class="text-center"><a href="'.base_url().'configure_access/view_instant_detail?consultation_id='.$value['consultation_id'].'" class="" name=' . $value['consultation_id'] . ' value=""><span class="label label-success"></span></a>';
                                    
                                    $i++;
                                }
                               // $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
							</tbody></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






