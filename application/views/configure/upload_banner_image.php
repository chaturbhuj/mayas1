<script src="<?php echo base_url(); ?>assets/js/upload_banner_image.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Image</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_banner_image_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="add_banner_image_form" method="post" class="form-horizontal">
                       <div class="form-group">
                            <label class="control-label col-md-2" for="required_banner_page">Required Banner Page<span class="required">*</span></label>
                            <div class="col-md-5">
                                <select class="form-control"  id="banner_page_type" name="banner_page_type" >
									<option value="">Select Banner Page</option>
									<option value="free_consultaion">Free Consultaion</option>									
									<option value="premium_consultaion">Premium Consultaion</option>
									<option value="free_gemstone_consultaion">Free Gemstone Consultaion</option>
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="banner_page_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a href="#" id="banner_page_image_btn" name="banner_page_image_btn" class="banner_page_image_btn" data-toggle="modal" data-target="#browseBannerImage">Upload Image</a>
                                <input class="form-control" id="banner_image" name="banner_image" type="hidden">
                                <img class="img-preview" src="" alt="" style="width: 150px;">     
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseBannerImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="banner" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
