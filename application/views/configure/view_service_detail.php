


<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>Consultation Detail</h2>
                       <form id="consultation_form" method="post">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title" >Consultation Detail</h4>
						</div>
						<div class="modal-body">
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="consultantType">Consultant Type : </label>
								<div class="col-md-7">
									<span class="full_namel" id="consultantType"  name="consultantType"><?php echo $service_list[0]['consultantType'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="full_name">Full Name : </label>
								<div class="col-md-7">
									<span class="full_namel" id="full_name" name="full_name" ><?php echo $service_list[0]['full_name'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="gender">Gender : </label>
								<div class="col-md-7">
									<span class="full_namel" id="gender" name="gender"><?php echo $service_list[0]['gender'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="date_of_birth">Date of Birth : </label>
								<div class="col-md-7">
									<span class="full_namel" id="date_of_birth" name="date_of_birth"><?php echo $service_list[0]['inputDateDate'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="date_of_time">Time of Birth : </label>
								<div class="col-md-7">
									<span class="full_namel" id="date_of_time" name="date_of_time"><?php echo $service_list[0]['inputTimeMin'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="birthCountry">Birth Country : </label>
								<div class="col-md-7">
									<span class="full_namel" id="birthCountry" name="birthCountry"><?php echo $service_list[0]['birthCountry'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="birthState">Birth State : </label>
								<div class="col-md-7">
									<span class="full_namel" id="birthState" name="birthState"><?php echo $service_list[0]['birthState'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="birthCity">Birth City : </label>
								<div class="col-md-7">
									<span class="full_namel" id="birthCity" name="birthCity"><?php echo $service_list[0]['birthCity'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="inputEmail">Email ID : </label>
								<div class="col-md-7">
									<span class="full_namel" id="inputEmail" name="inputEmail"><?php echo $service_list[0]['inputEmail'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="inputContact">Contact NO. : </label>
								<div class="col-md-7">
									<span class="full_namel" id="inputContact" name="inputContact"><?php echo $service_list[0]['inputContact'];?></span>
								</div>
							</div>
							<!--option field-->
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="educationQualification">Education Qualification : </label>
								<div class="col-md-7">
									<span class="full_namel" id="educationQualification" name="educationQualification"><?php echo $service_list[0]['educationQualification'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="currentProfession">Current Profession : </label>
								<div class="col-md-7">
									<span class="full_namel" id="currentProfession" name="currentProfession"><?php echo $service_list[0]['currentProfession'];?></span>
								</div>
							</div>
							
							<?php if($service_list[0]['consultantType'] == "MatchMaking"){ ?>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partnerName">Partner Name : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partnerName" name="partnerName"><?php echo $service_list[0]['partnerName'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partner_date_of_birth">Partner Date of Birth : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partner_date_of_birth" name="partner_date_of_birth"><?php echo $service_list[0]['partnerDateDate'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partner_date_of_time">Partner Time of Birth : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partner_date_of_time" name="partner_date_of_time"><?php echo $service_list[0]['partnerTimeMin'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partnerBirthCountry">Partner Birth Country : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partnerBirthCountry" name="partnerBirthCountry"><?php echo $service_list[0]['partnerBirthCountry'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partnerBirthState">Partner Birth State : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partnerBirthState" name="partnerBirthState"><?php echo $service_list[0]['partnerBirthState'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="partnerBirthCity">Partner Birth City : </label>
								<div class="col-md-7">
									<span class="full_namel" id="partnerBirthCity" name="partnerBirthCity"><?php echo $service_list[0]['partnerBirthCity'];?></span>
								</div>
							</div>
							<?php } ?>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="inputQuestion">Precise Question : </label>
								<div class="col-md-7">
									<span class="full_namel" id="inputQuestion" name="inputQuestion"><?php echo $service_list[0]['inputQuestion'];?></span>
								</div>
							</div>
							<div class="form-group" style="padding: 15px 0px 15px 0px">
								<label class="control-label col-md-5" for="otherInfomation">Other Question : </label>
								<div class="col-md-7">
									<span class="full_namel" id="otherInfomation" name="otherInfomation"><?php echo $service_list[0]['otherInfomation'];?></span>
								</div>
							</div>
							
							<div class="clearfix"></div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						</div>
					</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


-->
