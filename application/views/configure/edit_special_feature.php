<script src="<?php echo base_url(); ?>assets/js/special_feature.js"></script> 
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_special_feature"></div>
                    <div id="edit_user_table">
                        <h2>List of All Special Feature</h2>
                        <div id="table_view">             
                            <?php
                            if ($special_feature == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Special Feature Heading</th><th class="text-center">Sub Heading</th><th class="text-center">Small Sub Heading Message</th><th class="text-center">point2</th><th class="text-center">point3</th><th class="text-center">point4</th><th class="text-center">point4</th><th class="text-center">point5</th><th class="text-center">point6</th><th class="text-center">point7</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($special_feature as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['special_feature_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['sub_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['small_sub_heading_message'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point1'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point2'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point3'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point4'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point5'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point6'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['point7'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="edit_special_feature" data-toggle="modal" data-target="#my_user_edit" name=' . $value['special_feature_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_special_feature"  name=' . $value['special_feature_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_special_feature_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Special Feature</h4>
                    <input type="hidden" name="special_feature_id" id="special_feature_id" value=""/>                
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label col-md-3" for="special_feature_heading">Special Feature Heading<span class="required">*</span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="special_feature_heading" name="special_feature_heading" placeholder="Special Feature Heading" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="sub_heading">Sub Heading<span class="required">*</span></label>
                            <div class="col-md-9">
                                <input class="form-control" id="sub_heading" name="sub_heading" placeholder="Sub Heading" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="small_sub_heading_message">Small Sub Heading Message</label>
                            <div class="col-md-9">
                                <input class="form-control" id="small_sub_heading_message" name="small_sub_heading_message" placeholder="Small Sub Heading Message" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point1">Point 1</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point1" name="point1" placeholder="Point 1" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point2">Point 2</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point2" name="point2" placeholder="Point 2" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point3">Point 3</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point3" name="point3" placeholder="Point 3" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point4">Point 4</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point4" name="point4" placeholder="Point 4" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point5">Point 5</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point5" name="point5" placeholder="Point 5" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point6">Point 6</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point6" name="point6" placeholder="Point 6" type="text">
                            </div>
                        </div><div class="clearfix"></div>
                        <div class="form-group">
                            <label class="control-label col-md-3" for="point7">Point 7</label>
                            <div class="col-md-9">
                                <input class="form-control" id="point7" name="point7" placeholder="Point 7" type="text">
                            </div>
                        </div>

					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
