<link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/configure/consultation.js"></script> 
<script>
    $(function () {
        var oTable = $('#consultation_table').dataTable();
    });
</script>
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2>List of All Gemstone Recommendation</h2>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultation_table">';
                            <thead>
								<tr>
									<th class="text-center">S.No.</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Gender</th>
									<th class="text-center">Date of Birth</th>
									<th class="text-center">Time of Birth</th>
									<th class="text-center">Birth Country</th>
									<th class="text-center">Birth State</th>
									<th class="text-center">Birth City</th>
									<th class="text-center">Email ID</th>
									<th class="text-center">Contact NO.</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php
                            if ($gemstone_recommendation_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($gemstone_recommendation_list as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['full_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['gender'] . '</td>';
                                    $content .= '<td class="text-center">'.$value['inputDateDate'].'-'.$value['inputDateMonth'].'-'.$value['inputDateYear'].'</td>';
                                    $inputTimeHr = $value['inputTimeHr'];
									if($value['inputTimeHr']<10){
										$inputTimeHr = '0'.$value['inputTimeHr'];
									}
									$inputTimeMin = $value['inputTimeMin'];
									if($value['inputTimeMin']<10){
										$inputTimeMin = '0'.$value['inputTimeMin'];
									}
									$content .= '<td class="text-center">'.$inputTimeHr.':'.$inputTimeMin.'</td>';
									$content .= '';
									$content .= '<td class="text-center">' . $value['birthCountry'] . '</td>';
									$content .= '<td class="text-center">' . $value['birthState'] . '</td>';
									$content .= '<td class="text-center">' . $value['birthCity'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputEmail'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputContact'] . '</td>';
									$content .= '<td class="text-center">';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_gemstone_recommendation"  name=' . $value['gemstone_recommendation_id'] . ' value=""><span class="label label-danger">Remove </span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('document').ready(function () {
 
    //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */

    $('body').on('click', '.remove_gemstone_recommendation', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var gemstone_recommendation_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'page/remove_gemstone_recommendation', {gemstone_recommendation_id: gemstone_recommendation_id}, function (response) {
            $('#err_edit_sub_menu_services').empty();
			$("html, body").animate({scrollTop: 0}, "slow");
            if (response.status == 200) {
                $('#err_edit_sub_menu_services').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");

                $('.remove_gemstone_recommendation[name=' + gemstone_recommendation_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_sub_menu_services').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
});
</script>
