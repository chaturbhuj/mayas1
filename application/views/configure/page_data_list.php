 

<style>
.service_photo{
	width: 60px;
}

</style>
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of Page Data</h1>
		<a href="<?php echo base_url();?>configure_access/page_data" class="btn btn-default pull-right addAds">Add New page_data</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                           // var_dump($page_data);
                            if ($page_data == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr>
											<th class="text-center">S.No.</th>
											<th class="text-center">Page Heading</th>
											<th class="text-center">image</th>
											<th class="text-center">Video Link</th>
											<th class="text-center">Action</th></tr>
											</thead>
								<tbody>';
                                $i = 1;
                                foreach ($page_data as $value) {
									//var_dump($page_data);
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['page'] . '</td> <td class="text-center"><img class="img_pre img-rounded" src="'.base_url().'uploads/'.$value['header_image'].'"></td> <td class="text-center">' . $value['video_link'] . '</td>';
									$content .= '<td class="text-center"><a href="'.base_url().'configure_access/page_data?id='. $value['id'] . '"  name=' . $value['id'] . ' value=""><span class="label label-success">Edit</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

