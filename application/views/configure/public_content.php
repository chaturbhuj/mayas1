
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-select.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugin/bootstrap-select.min.css">

<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
	$('.selectpicker').selectpicker();
</script>
<style>
.error,.required{
	color:red;
}

.img-preview{
	width: 100px;
	margin-left: 20px;
}
</style>
<?php 
	$content_cat_id_ = 0;
	$content_cat_ = '';
	if(isset($_GET['content_cat'])){
		$content_cat_id_ = explode('-',$_GET['content_cat'])[0];
		$content_cat_ = explode('-',$_GET['content_cat'])[1];
	}
	
	if($content_detail){
		$content_id = $content_detail[0]['content_id'];
		$content_cat_id = $content_detail[0]['content_cat_id'];
		$content = $content_detail[0]['content'];
		$content_cat_ = $content_detail[0]['heading'];
	}else{
		$content_id = 0;
		$content_cat_id = '';
		$content = '';
	}

?>
<div class="container-fluid main-content">
	<div class="row" style="">
	   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	     <div class="widget-container fluid-height clearfix ">
			<div class="widget-content padded">
				<div class="box-header with-border">
					  <h2 class="">Public Content</h2>
					  <div id="headMsg"></div>
				</div>
				<div class="col-md-12 form-group" style="padding-left: 288px;padding-right: 99px;margin-top:50px;">
							<select class="form-control content_cat_" id="content_cat_" name="content_cat_">
								<option value="">Select Category</option>
								<option name="1" value="Terms of Use" <?php if($content_cat_id_==1) echo 'selected';?>>Terms of Use</option>
								<option name="2" value="Privacy Policy" <?php if($content_cat_id_==2) echo 'selected';?>>Privacy Policy</option>
								<option name="3" value="Refunds/Cancellations" <?php if($content_cat_id_==3) echo 'selected';?>>Refunds/Cancellations</option>
							</select>
				</div>
				<div class="ibox-content">
					<form class="form-horizontal" id="add_content_form">
						
						<input type="hidden" name="content_id" id="content_id" value="<?php echo $content_id;?>">
							<div class="form-group">
								<label class="control-label col-md-3" for="content">Content Category<span class="required">*</span></label>
								<div class="col-md-8">
									<span class="form-control"><?php echo $content_cat_;?></span>
									<input type="hidden" name="content_cat" id="content_cat" value="<?php echo $content_cat_id_;?>">
								</div>
							</div>	
									
							<div class="form-group">
								<label class="control-label col-md-3" for="content">Content<span class="required">*</span></label>
								<div class="col-md-8">
									<textarea rows="10" class="form-control ckeditor content" id="content" name="content" placeholder="Content"><?php echo $content;?></textarea>
									<div class="content-error"></div>
								</div>
							</div>	
							<div class="form-group">
								 <div class="input-group col-md-6 col-md-offset-3" style="margin-top: 20px;"> 
									<button id="btn-signup" type="submit" class="btn btn-primary">Submit</button>
								 </div>
							</div>	
					</form>
				</div>
			</div>
		</div>
		</div>
		
	</div>
</div>

<script>
$(document).ready(function () {
	/**
	 * This script is used to save the data from add_content_form in db
	 */
	 $('#add_content_form').validate({
		ignore: [],
		rules: {
			content_cat: {
				required: true,
			},
			content: {
				required: function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  }
			},
		},
		messages: {
			content_cat: {
				required: "Category is required"
			},
			content: {
				required: "Content is required"
			},
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
			var content_id = $('#content_id').val();
			var content_cat_id = $('#content_cat').val();
			var heading = $('#content_cat_ option:selected').attr('value');
			var content = $('textarea.ckeditor').val();
			
			
			$.post(APP_URL+'page/update_public_content',{
				content_id: content_id,
				content_cat_id: content_cat_id,
				heading: heading,
				content: content,
			},function(response){
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headMsg').empty();
                if (response.status == 200) {
					message = response.message;
					if(content_id!=0){
						message = "Content has been updated successfully!"; 
					}
					$('#headMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					
					
				}
                else if (response.status == 201) {
                    $('#headMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				
                }
           },'json');
		 
			return  false;
		}
	});
	 
	//---------------------------------------------------------------------
    /*
     * This script is used to remove blog  from the list
     */
	$('body').on('change', '.content_cat_', function () {
        if($(this).val==''){
			return false;
		}
		
		var content_cat = $(this).val().replace(/\s+/g, '-');
		var content_cat_id = $(this).find('option:selected').attr('name');
		var url = APP_URL+'page/public_content?content_cat='+content_cat_id+'-'+content_cat;
		window.location.href= url;
		
	});
	
    
});

</script>     
