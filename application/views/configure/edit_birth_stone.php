<script src="<?php echo base_url(); ?>assets/js/birth_stone.js"></script> 
<div class="container-fluid main-content">
<div class="page-title">
        <h1>List of All Birth Stone Item</h1>
		<a href="<?php echo base_url();?>configure_access/add_birth_stone" class="btn btn-default pull-right addAds">Add Birth Stone</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_birth_stone"></div>
                    <div id="edit_user_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($birth_stone == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Birth Stone Name</th><th class="text-center">Birth Stone Navigation URL</th><th class="text-center">main_description</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($birth_stone as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['birth_stone_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['birth_stone_navi'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['main_description'] . '</td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_birth_stone?stone_id='.$value['birth_stone_id'].'" class="" name=' . $value['birth_stone_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_birth_stone"  name=' . $value['birth_stone_id'] . ' value=""><span class="label label-danger">Remove Birth Stone</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_birth_stone_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit birth_stone</h4>
                    <input type="hidden" name="edit_birth_stone_id" id="edit_birth_stone_id" value=""/>     
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_birth_stone_name">Birth Stone Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_birth_stone_name" name="edit_birth_stone_name" placeholder="Birth Stone Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_birth_stone_navi">Birth Stone Navigation URL<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_birth_stone_navi" name="edit_birth_stone_navi" placeholder="Birth Stone Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                        </div>
                    </div>
					
					
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
