
<script>
	
	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			minDate : 'today',
		});

	});

</script>
<style>
.page-title h1 {
	display: inline-block;
    float: left;
}

.ads_image_btn{
	background-image: none;
}
#testimonials_form input,select,textarea{
	width: 100% !important;
}
.img-preview{
	width: 150px;
	margin-left: 20px;
}
.ads_image_pre{
	width: 60px;
}
th{text-align: center;}

.testimonials_photo{width: 60px;}



</style>

<!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
<div class="container-fluid main-content">
	<div class="box box-primary" style="margin: 50px 0;">
		<div class="box-header with-border">
        <h1 style="display: inline;">List of Coupon</h1>
		<button class="btn btn-default pull-right addNewTestimonials" data-toggle="modal" data-target="#browseNewTestimonials">Add New Coupon</button>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="widget-container fluid-height clearfix"><br/>
					<div id="err_testimonials_form"></div>
					<div class="clearfix"></div>
					<div class="widget-content padded">
						 <div id="table_view" class="table-responsive">
							<table id="testimonials_table" class="table table-hover">
								<thead class="table_head"><tr><th>S.No.</th><th>Code</th><th>Discount(%)</th><th>Maximum Users</th><th>Field</th><th>Validation Period</th><th>Applied User</th><th>Action</th></tr></thead><tbody>
									
									<?php
									if ($coupan == 0) {
										echo 'No record found into database';
									} else {
										$i = 1;
										$content = '';
										foreach ($coupan as $value) {
											$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
											$content .= '<td class="text-center">' . $value['coupan_code'] . '</td>';
											$content .= '<td class="text-center">' . $value['discount'] . '</td>';
											$content .= '<td class="text-center">' . $value['max_users'] . '</td>';
											$content .= '<td class="text-center">' . $value['code_for'] . '</td>';
											$content .= '<td class="text-center">From ' . $value['start_date'] . ' to ' . $value['end_date'] . '</td>';
											$content .= '<td class="text-center">' . $value['appyer_count'] . '</td>';
											$content .= '<td class="text-center"><a href="#" class="edit_testimonials" data-toggle="modal" data-target="#browseNewTestimonials" name="' . $value['code_id'] . '" end_date="' . $value['end_date'] . '" start_date="' . $value['start_date'] . '" max_users="' . $value['max_users'] . '" code_for="' . $value['code_for'] . '"><span class="label label-success">Edit</span></a>';
											$content .= '&nbsp;&nbsp;<a href="#" class="remove_testimonials"  name=' . $value['code_id'] . ' value=""><span class="label label-danger">Remove </span></a></td></tr>';
											$i++;
										}
										echo $content;
									}
									?>
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!---------------------------- Modal for Browse Testimonials-------------------------->
<div class="modal fade" id="browseNewTestimonials" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Coupon</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well form-inline" id="testimonials_form" method="post" enctype="multipart/form-data">
						<input class="form-control" id="code_id" name="code_id" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="code_for">Code for<span class="required">*</span></label>
							<div class="col-md-9">
								<select class="form-control" id="code_for" name="code_for">
									<option value ="">Select Coupon field</option>
									<option value ="service">Services</option>
									<option value ="product">Products</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="coupan_code">Code <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="coupan_code" name="coupan_code" placeholder="Name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="discount">Discount ( %) <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="discount" name="discount" placeholder="Discount in %" type="number">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="start_date">Valid From <span class="required">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control date" id="start_date" name="start_date" placeholder="Start Date">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="end_date">Valid To <span class="required">*</span></label>
							<div class="col-md-9">
								<input type="text" class="form-control date" id="end_date" name="end_date" placeholder="End Date">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="end_date">Max User Can apply <span class="required">*</span></label>
							<div class="col-md-9">
								<input type="number" min="1" class="form-control" id="max_users" name="max_users" placeholder="Max User Can apply this code">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>

  </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
  <script>
 

$('document').ready(function(){
	

	
	
	//------------------------------------------------------------------------
    /*
     * This script is used to fill modal for edit testimonials
     */
    $('body').on('click', '.edit_testimonials', function () {		
        $('#err_testimonials_form').empty();

        var code_id = $(this).attr('name');
        var coupan_code = $(this).closest('tr').find('td:eq(1)').text();		
        var discount = $(this).closest('tr').find('td:eq(2)').text();
        var start_date = $(this).attr('start_date');
        var max_users = $(this).attr('max_users');
        var end_date = $(this).attr('end_date');
        var code_for = $(this).attr('code_for');
		$('#code_for').find('option[value="'+code_for+'"]').prop('selected',true);
        $('#code_id').val(code_id);
        $('#coupan_code').val(coupan_code);
		$('#discount').val(discount);
		$('#start_date').val(start_date);
		$('#end_date').val(end_date);
		$('#max_users').val(max_users);
		
    });
	
	//------------------------------------------------------------------------
    /*
     * This script is used to empty the model  when click on add new Slider
     */
    $('body').on('click', '.addNewTestimonials', function () {
		$("#code_id").val(0);
		$("#coupan_code").val('');
		$("#discount").val('');
		$("#start_date").val('');
		$("#end_date").val('');
		$("#max_users").val('');
		$('#code_for').find('option[value="service"]').prop('selected',true);
	});
	
	//-----------------------------------------------------------------------
    /* 
     * validation of add city
     */
	$('#testimonials_form').validate({
		ignore: [],
        rules: {
            coupan_code: {
                required: true,
            },
			code_for: {
                required: true,
            },
            discount: {
                required: true,
            },
			start_date: {
                required: true,
            },
			end_date: {
                required: true,
            },
			max_users: {
                required: true,
            },
         },
		 messages: {
			coupan_code: {
                required: "Code is required.",
            },
			code_for: {
                required: "Field is required.",
            },
			discount: {
                required: "discount is required.",
            },
			start_date: {
                required: "Start date is required.",
            },
			end_date: {
                required: "End Date is required.",
            },
			max_users: {
                required: "Maximum users is required.",
            },
		},
		submitHandler: function (form) {
			var code_id = $('#code_id').val();
            var coupan_code = $('#coupan_code').val();
            var code_for = $('#code_for').val();
            var discount = $('#discount').val();
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();
			var max_users = $('#max_users').val();
			$.post(APP_URL + 'configure_access/add_new_coupan_code', {
                code_id: code_id,
                coupan_code: coupan_code,
                code_for: code_for,
                discount: discount,
                start_date: start_date,
                end_date: end_date,
				max_users: max_users,
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#err_testimonials_form').empty();
				if (response.status == 200) {
                    var message = response.message;
					if(code_id!=0){
						message = "Coupon has been updated successfully!";
						
					}else{
						
					}
					$('#err_testimonials_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong></div>");
					$("#err_testimonials_form").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_testimonials_form').remove();
						window.location.href = APP_URL+'configure_access/coupan';
					});
                }
                else if (response.status == 201) {
                    $('#err_testimonials_form').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#err_testimonials_form").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_testimonials_form').remove();
						window.location.href = APP_URL+'configure_access/coupan';
					});
                }
				$("#code_id").val(0);
				$("#coupan_code").val('');
				$("#discount").val('');
				$('#browseNewTestimonials').modal('hide');
				
			}, 'json');
		return false;
		},
	});
	
	//---------------------------------------------------------------------
    /**
     * This script is used to remove testimonials from the list
     */
	$('body').on('click', '.remove_testimonials', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var code_id = parseInt($(this).attr('name'));
        $.post(APP_URL + 'configure_access/remove_coupan', {code_id: code_id}, function (response) {
            $('#err_testimonials_form').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");               
                $('#err_testimonials_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");

                $('.remove_testimonials[name=' + code_id + ']').closest("tr").remove();
				
				$("#err_testimonials_form").fadeTo(2000, 500).slideUp(500, function(){
						$('#err_testimonials_form').remove();
						window.location.href = APP_URL+'configure_access/coupan';
					});
            }
            else {
                $('#err_testimonials_form').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#err_testimonials_form").fadeTo(2000, 500).slideUp(500, function(){
							$('#err_testimonials_form').remove();
							window.location.href = APP_URL+'admin/configure_access/testimonials';
						});
            }
        }, 'json');
        return false;
    });
});





 </script>
