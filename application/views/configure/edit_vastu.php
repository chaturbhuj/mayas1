<script src="<?php echo base_url(); ?>assets/js/aboutus.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<?php
// var_dump($vastu);


?>
<!-- <div class="container-fluid main-content">
    <div class="page-title">
        <h1>Edit Vastu</h1>
		</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="widget-content padded">
                    <div id="err_page_form"></div>
                    <div id="edit_user_table">
                        <form id="edit_vastu_form" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">Home page heading<span class="required">*</span></label>
                                <div class="col-md-5">
                                  <input type="text" name="home_page_heading" id="home_page_heading" value="<?=$home_page_heading?>" class="form-control" placeholder="Home Page Heading">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">How to use youtube link<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="youtube_link" id="youtube_link" value="<?=$youtube_link?>" class="form-control" placeholder="youtube link">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">Url<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="url" id="url"  value="<?=$url?>" class="form-control" placeholder="page url">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">Page title<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="page_title" id="page_title" value="<?=$page_title?>" class="form-control" placeholder="Page Title">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">Meta Tag<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="meta_tag" id="meta_tag" value="<?=$meta_tag?>" class="form-control" placeholder="Meta tag">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" for="">Meta Description<span class="required">*</span></label>
                                <div class="col-md-5">
                                    <input type="text" name="meta_descp" id="meta_descp" value="<?=$meta_descp?>" class="form-control" placeholder="Meta Description">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="control-label col-md-2" for="aboutus1">Page Content<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor aboutus1" id="home_page_content" name="home_page_content"  placeholder="Vastu Detail Description" rows="5"><?=$home_page_content?></textarea>
                            </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5 col-md-offset-2">
                                    <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class=" col-md-2">
        </div>
        <div class="col-md-5 mt-5" style="margin-top:40px">
          <a class="btn btn-primary" href="<?=base_url().'configure_access/add_new_vastu_type'?>">Add New Vastu type</a>
        </div>
    </div>
</div> -->

<!-- Model -->
<!-- <div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_user_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit User</h4>
                    <input type="hidden" name="edit_user_id" id="edit_user_id" value=""/>
                    <div id="edit_user_id" style="visibility: none;"></div>
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_name">User Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control capitaliseFirstLetter" id="edit_user_name" name="edit_user_name" placeholder="User Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_user_email">User Email<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_user_email" name="edit_user_email" placeholder="User Email" type="email">
                        </div>
                    </div>
                    <div id="err_edit_user_email" class="col-md-offset-3 clearfix"></div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_assign_role_to_user">Assign Role to user<span class="required">*</span></label>
                        <div class="col-md-9 select_role">
                            <input class="form-control" id="edit_assign_role_to_user" name="edit_assign_role_to_user" placeholder="Assign Role to user" type="text">
                        </div>
                    </div><div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div>
    </div>
</div> -->

<!-- /.modal -->
<!---------------------------- Modal for Browse Image-------------------------->
<!-- <div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image1</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div> -->
<!--+++++++++++++++++++++++++++++++++++++++++ Modal for Browse Image-------------------------->
<!-- <div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image2</h3>
            </div>
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div> -->

<!--------vastu list mayas new vastu table----->
</script>
<style>
.service_photo{
	width: 60px;
}

</style>
 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Vastu List</h1>
		<a href="<?=base_url().'configure_access/add_new_vastu_type'?>" class="btn btn-default pull-right addAds">Add New Vastu Type</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_services"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($vastu_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Vastu Heading</th><th class="text-center">Short Description</th><th class="text-center">Page Title</th><th class="text-center">Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($vastu_list as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['short_description'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_title'] . '</td>';
                                    $content .= '<td class="text-center"><img class="service_photo" src="' . base_url() . 'uploads/' . $value['detail_image'] . '"></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'configure_access/add_new_vastu_type?vastu_id='. $value['vastu_id'] . '"  name=' . $value['vastu_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0)" class="remove_date"  name="'. $value['vastu_id'] .'" value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        //-----------------------------------------------------------------------
        /*
        * validation
        */
        $('#edit_vastu_form').validate({
            ignore:[],
            rules: {
                home_page_heading:{
                    required: true,
                },

                youtube_link: {
                    required: true,
                },

                url: {
                    required: true,
                },

                page_title: {
                    required: true,
                },
                meta_tag: {
                    required: true,
                },
                meta_descp: {
                    required: true,
                },
                home_page_content: {
                        required: function(textarea) {
                                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                                return editorcontent.length === 0;
                        }
                },
            },
            messages: {

                // page: {
                //     required: "Page Heading  is required",
                // },
								//
                // data: {
                //     required: "Content is required",
                // },
                // header_image: {
                //     required: "Image is required",
                // },
                // video_link: {
                //     required: "Vide Link is required",
                // },

            },

            submitHandler: function (form) {
                var home_page_heading = $('#home_page_heading').val();
                var youtube_link = $('#youtube_link').val();
                var url = $('#url').val();
                var page_title = $('#page_title').val();
                var meta_tag = $('#meta_tag').val();
                var meta_descp = $('#meta_descp').val();
                var home_page_content =  CKEDITOR.instances['home_page_content'].getData();

                $.post(APP_URL + 'configure_access/add_vastu_data', {
                    id: 1,
                    home_page_heading:home_page_heading,
                    youtube_link:youtube_link,
                    url:url,
                    page_title:page_title,
                    meta_tag:meta_tag,
                    meta_descp:meta_descp,
                    home_page_content:home_page_content,
                   
                },
                function (response) {
                    $("html, body").animate({scrollTop: 0}, "slow");
                    $('#err_page_form').empty();
                    if (response.status == 200) {
                        $('#err_page_form').html("<div class='alert alert-success fade in'>\n\
                    <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                    <strong>" + response.message + "</strong></div>");
                    }
                    else if (response.status == 201) {
                        $('#err_page_form').html("<div class='alert alert-danger fade in'>\n\
                    <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                    <strong>" + response.message + "</strong></div>");
                    }
                    // $('#idd').val(0);
                    // $('#page').val('');
                    // $('#data').val('');
                    // $('#video_link').val('');
                    // $('#header_image').val('');
                    // $('.img-preview').attr('src','');
                }, 'json');
                return false;
            }
        });
        

          //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_date', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var vastu_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove', {
            table_name: 'mayas_new_vastu',
            primary_column_name: 'vastu_id',
            id: vastu_id
        }, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                //$("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_date[name=' + vastu_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

    });
</script>
