
<script src="<?php echo base_url(); ?>assets/js/content.js"></script> 

<style>
.page-title h1 {
	display: inline-block;
    float: left;
}

.ads_image_btn{
	background-image: none;
}
#testimonials_form input,select,textarea{
	width: 100% !important;
}
.img-preview{
	width: 150px;
	margin-left: 20px;
}
.ads_image_pre{
	width: 60px;
}
th{text-align: center;}

.testimonials_photo{width: 60px;}



</style>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of public content</h1>
		
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div id="err_testimonials_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
					<form class="well form-inline" id="content_form" method="post" enctype="multipart/form-data">
					
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="header_content">Heading Free consultation  <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="header_content" name="header_content" placeholder="enter Heading" type="text" style="width: 100%" value="<?php echo($content_list[0]['header_content'])?>">
							
							</div>
						</div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="free_consultation_content">Free consultation <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="free_consultation_content" name="free_consultation_content" placeholder="Free consultation content" type="text" style="width: 100%" Value="<?php  echo ($content_list[0]['free_consultation_content'])?>">
							
							</div>
						</div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="free_consultation_desc">Free consultation description<span class="required">*</span></label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" id="free_consultation_desc" name="free_consultation_desc" placeholder="Free consultation description" type="text" style="width: 100%"><?php  echo ($content_list[0]['free_consultation_desc'])?></textarea>
							
							</div>
						</div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="premium_consultation_content">Premium Consultation <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="premium_consultation_content" name="premium_consultation_content" placeholder="Premium consultation content" type="text" style="width: 100%" Value="<?php  echo ($content_list[0]['premium_consultation_content'])?>">
							
							</div>
						</div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="premium_consultation_desc">Premium Consultation description<span class="required">*</span></label>
							<div class="col-md-9">
								<textarea class="form-control" rows="3" id="premium_consultation_desc" name="premium_consultation_desc" placeholder="Premium consultation description" type="text" style="width: 100%"><?php  echo ($content_list[0]['premium_consultation_desc'])?></textarea>
							
							</div>
						</div>
						
						
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">submit</button>
						</div>
					</form>
					
				</div>
            </div>
        </div>
    </div>
</div>
