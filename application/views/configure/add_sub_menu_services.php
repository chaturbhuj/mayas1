<script src="<?php echo base_url(); ?>assets/js/sub_menu_services.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New Sub Menu Services</h1>
		<a href="<?php echo base_url();?>configure_access/edit_sub_menu_services" class="btn btn-default pull-right addAds">View All Services</a>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_sub_menu_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="addSubMenuForm" method="post" class="form-horizontal">
					
					<div class="form-group">
                            <label class="control-label col-md-2" for="services_type">Menu Services<span class="required">*</span></label>
						<div class="col-md-5">
                                <select class="form-control" id="sub_menu_services_name" name="sub_menu_services_name" placeholder="Sub Menu Services Name" value="">
								    <option value="0">Select Sub Menu Services Name</option>
									<option value="add_services" >Add Premium Services</option>									
									<option value="edit_services" >Edit Premium Services</option>
								</select>
							</div>
							</div>
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="sub_menu_services_name">Sub Menu Services Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="sub_menu_services_name" name="sub_menu_services_name" placeholder="Sub Menu Services Name" type="text">
                            </div>
                        </div>
						
						
						
						<div class="form-group">
                            <label class="control-label col-md-2" for="sub_menu_services_navi">Sub Menu Services Navigation URL<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="sub_menu_services_navi" name="sub_menu_services_navi" placeholder="Sub Menu Services Navigation URL(Ex. http://www.xxx.xx/x)" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
