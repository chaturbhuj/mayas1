<script src="<?php echo base_url(); ?>assets/js/menu.js"></script> 
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_menu"></div>
                    <div id="edit_user_table">
                        <h2>List of All Menu Item</h2>
                        <div id="table_view">             
                            <?php
                            if ($menu == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Menu Name</th><th class="text-center">Menu Navigation URL</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($menu as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['menu_name'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['menu_navi'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="edit_menu" data-toggle="modal" data-target="#my_user_edit" name=' . $value['menu_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#;" class="remove_menu"  name=' . $value['menu_id'] . ' value=""><span class="label label-danger">Remove Menu</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="edit_menu_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Edit Menu</h4>
                    <input type="hidden" name="edit_menu_id" id="edit_menu_id" value=""/>
                    <div id="editmenu_id" style="visibility: none;"></div>                    
                </div>
                <div class="modal-body">

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_menu_name">Menu Name<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_menu_name" name="edit_menu_name" placeholder="Menu Name" type="text">
                        </div>
                    </div>

                    <div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-3" for="edit_menu_navi">Menu Navigation URL<span class="required">*</span></label>
                        <div class="col-md-9">
                            <input class="form-control" id="edit_menu_navi" name="edit_menu_navi" placeholder="Menu Navigation URL" type="text">
                        </div>
                    </div>
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <input type="submit" class="btn btn-primary" value="Save" />
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
