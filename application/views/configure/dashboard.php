<script> 
	//---------------------------------------------------------------------
    /*
     * This script is used to remove/read contact form details  from the list
     */

    $('body').on('click', '.remove_contact_form', function () {
        if (!confirm("Do you want to remove")) {
            return false;
        }
        var edit_contact_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_contact', {edit_contact_id: edit_contact_id}, function (response) {
            $('#err_edit_contact').empty();
            if (response.status == 200) {
               // $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_contact').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");

                $('.remove_contact_form[name=' + edit_contact_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_contact').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
	//---------------------------------------------------------------------
    /*
     * This script is used to remove/read contact form details  from the list
     */

    $('body').on('click', '.remove_consultant_form', function () {
        if (!confirm("Do you want to remove")) {
            return false;
        }
        var edit_consultant_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/remove_consultant', {edit_consultant_id: edit_consultant_id}, function (response) {
            $('#err_edit_consultant').empty();
            if (response.status == 200) {
               // $("html, body").animate({scrollTop: 0}, "slow");
                $('#edit_user_table').show();
                $('#err_edit_consultant').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");

                $('.remove_consultant_form[name=' + edit_consultant_id + ']').closest("tr").remove();
            }
            else {
                $('#err_edit_consultant').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button> <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });
	</script> 
	
<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_contact_form"></div>
                    <div id="edit_team_table">
                        <h2>List of All Contact Form Details</h2>
                        <div id="table_view">             
                            <?php
                            if ($contact_form == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Name</th><th class="teerr_edit_blogxt-center">Email</th><th class="text-center">Phone Number</th><th class="text-center">Message</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($contact_form as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputName'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputEmail'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputWebsite'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputMessage'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="remove_contact_form"  name=' . $value['contact_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
			<div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_consultant_form"></div>
                    <div id="edit_team_table">
                        <h2>List of All consultant Form Details</h2>
                        <div id="table_view">             
                            <?php
                            if ($consultant_form == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Name</th><th class="teerr_edit_blogxt-center">DOB</th><th class="text-center">Birth Time</th><th class="text-center">Birth Place</th><th class="text-center">Contact</th><th class="text-center">Email</th><th class="text-center">Question</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($consultant_form as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>'; 
                                    $content .= '<td class="text-center">' . $value['inputName'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputBirth'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputTime'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputPlace'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputContact'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['inputEmail'] . '</td>';
									$content .= '<td class="text-center">' . $value['inputQuestion'] . '</td>';
                                    $content .= '<td class="text-center"><a href="#" class="remove_consultant_form"  name=' . $value['consultant_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>