<script src="<?php echo base_url(); ?>assets/js/add_new_user.js"></script> 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Add New User</h1>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_admin_add_new_user_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
                    <form id="admin_add_new_user_form" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label class="control-label col-md-2" for="user_email">User Email<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="user_email" name="user_email" placeholder="User Email" type="email">
                            </div>
                        </div>
                        <div id="err_user_email" class="col-md-offset-2"></div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="user_name">Full Name<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control capitaliseFirstLetter" id="user_name" name="user_name" placeholder="User Name" type="text">
                            </div>
                        </div>
                        <div id="err_user_name" class="col-md-offset-2"></div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="assign_role_to_user">Assign Role to user<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="assign_role_to_user" name="assign_role_to_user" placeholder="Assign Role to user" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="user_pass">Password<span class="required">*</span></label>
                            <div class="col-md-5">                        
                                <input class="form-control" id="user_pass" name="user_pass" placeholder="Password" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
