

<?php
		if($meta_data){
		$meta_id = $meta_data[0]['meta_id'];
		$url = 'https://mayasastrology.com/';
		//$url = $meta_data[0]['url'];
		$title = $meta_data[0]['title'];
		$dec_meta = $meta_data[0]['dec_meta'];
		$keyword_meta = $meta_data[0]['keyword_meta'];
		
	}else{
			
		$meta_id = 0;
		$url = 'https://mayasastrology.com/';
		$title ='';
		$dec_meta ='';
		$keyword_meta ='';
	}
?>

		
	


<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Meta data</h1>
	</div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_counter_form"></div>
                <div class="clearfix"></div>
                <div id="err_services_form"></div>
                <div class="widget-content padded">
                     <form id="add_services_form_bespoke" method="post" class="form-horizontal">
					
                    
                         <input class="form-control" id="meta_id" name="meta_id" value="<?php echo $meta_id;?>" placeholder="Services Url" type="hidden">
                        
					<div class="form-group">
                            <label class="control-label col-md-2" for="url">Url<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="url" name="url" value="<?php echo $url;?>" placeholder="Services Url" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="title"> Title<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="title" name="title" value="<?php echo $title;?>" placeholder=" Title" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="dec_meta"> Desc meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="dec_meta" name="dec_meta" value="<?php echo $dec_meta;?>" placeholder=" desc meta" type="text">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="keyword_meta"> Keyword meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="keyword_meta" name="keyword_meta" value="<?php echo $keyword_meta;?>" placeholder="Keyword meta" type="text">
                            </div>
                        </div>					
						
                        			
						<div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!---------------------------- Modal for Browse Image-------------------------->

<script>
$('document').ready(function(){
	//-----------------------------------------------------------------------
    /* 
     * admin_add_new_user_form validation
     */
    $('#add_services_form_bespoke').validate({
        ignore: [],
        rules: {
          
			keyword_meta: {
                required: true
            },
			dec_meta: {
                required: true
            },
			title: {
                required: true
            },
			url: {
                required: true
            },
			
        },
        messages: {
           
			keyword_meta: {
                required: " Services keyword meta is required"
            },
			dec_meta: {
                required: " Services dec meta is required"
            },
			title: {
                required: " Services title is required"
            },
			url: {
                required: " Services url is required"
            },
        },
        submitHandler: function (form) {
        
			var meta_id = $('#meta_id').val();
			var url = $('#url').val();
			var title = $('#title').val();
			var dec_meta = $('#dec_meta').val();
			var keyword_meta = $('#keyword_meta').val();
			
            $.post(APP_URL + 'meta/update_meta_data', {
                url: url,
                title: title,
                meta_id: meta_id,
                dec_meta: dec_meta,
                keyword_meta: keyword_meta,
              
            },
				function (response) {
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#err_services_form').empty();
					var mesg;
					if (response.status == 200) {
						mesg = response.message;
						
						
						$('#err_services_form').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + mesg + "</strong></div>");
						$('#err_change_password_form').html("<div class='alert alert-success fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>" + response.message + "</strong></div>");
					
					window.location.href= APP_URL+'meta/meta_view';
					}
					else if (response.status == 201) {
						$('#err_services_form').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				
				}, 'json');
            return false;
        }
    });
});
</script>