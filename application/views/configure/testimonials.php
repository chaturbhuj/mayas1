<link href="<?php echo base_url(); ?>assets/css/plugins/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/testimonials.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/configure/testimonials.js"></script> 
<script>
$(function () {
        var oTable = $('#testimonials_table').dataTable();
    });
</script>
<style>
.page-title h1 {
	display: inline-block;
    float: left;
}

.ads_image_btn{
	background-image: none;
}
#testimonials_form input,select,textarea{
	width: 100% !important;
}
.img-preview{
	width: 150px;
	margin-left: 20px;
}
.ads_image_pre{
	width: 60px;
}
th{text-align: center;}

.testimonials_photo{width: 60px;}



</style>
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of Testimonials</h1>
		<button class="btn btn-default pull-right addNewTestimonials" data-toggle="modal" data-target="#browseNewTestimonials">Add New Testimonials</button>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div id="err_testimonials_form"></div>
                <div class="clearfix"></div>
                <div class="widget-content padded">
					 <div id="table_view" class="table-responsive">
						<table id="testimonials_table" class="table table-hover">
							<thead class="table_head">
							<tr>
							<th>S.No.</th>
							<th>Name</th><th>Position</th><th>Comments</th><th>Image</th><th>Action</th>
							</tr>
							</thead>
							<tbody>
								
								<?php
								if ($testimonials_list == 0) {
									echo 'No record found into database';
								} else {
									$i = 1;
									$content = '';
									foreach ($testimonials_list as $value) {
										$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
										$content .= '<td class="text-center">' . $value['testimonials_name'] . '</td>';
										$content .= '<td class="text-center">' . $value['position'] . '</td>';
										$content .= '<td class="text-center">' . $value['comments'] . '</td>';
										$content .= '<td class="text-center"><img class="testimonials_photo" src="' . base_url() . 'uploads/' . $value['testimonials_image'] . '"></td>';
										$content .= '<td class="text-center"><a href="#" class="edit_testimonials" data-toggle="modal" data-target="#browseNewTestimonials" name=' . $value['testimonials_id'] . ' value=""><span class="label label-success">Edit</span></a>';
										$content .= '&nbsp;&nbsp;<a href="#" class="remove_testimonials"  name=' . $value['testimonials_id'] . ' value=""><span class="label label-danger">Remove </span></a></td></tr>';
										$i++;
									}
									echo $content;
								}
								?>
								
							</tbody>
						</table>
						<input type="hidden" id="serial_no" name="serial_no" value="<?php echo $i; ?>">
					</div>
                    
				</div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Testimonials-------------------------->
<div class="modal fade" id="browseNewTestimonials" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Testimonials</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well form-inline" id="testimonials_form" method="post" enctype="multipart/form-data">
						<input class="form-control" id="testimonials_id" name="testimonials_id" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="testimonials_name">Name <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="testimonials_name" name="testimonials_name" placeholder="Name" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="position">Position <span class="required">*</span></label>
							<div class="col-md-9">
								<input class="form-control" id="position" name="position" placeholder="Position" type="text">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="comments">Comments <span class="required">*</span></label>
							<div class="col-md-9">
								<textarea rows="5" class="form-control" id="comments" name="comments" placeholder="Comments"></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="testimonials_image">Image <span class="required">*</span></label>
							<div class="col-md-9">
								<a id="testimonials_image_btn" name="testimonials_image_btn" class="btn btn-success testimonials_image_btn" data-toggle="modal" data-target="#browseImage" style="width: 167px;">Image </a>
                                <input class="form-control testimonials_image" id="testimonials_image" name="testimonials_image" type="hidden" value="">
								<img class="img-preview display_none" src="" alt="">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
					<p>Image should be less then the 2MB</p>
                    <div id="head1_msg"></div>
                    <input type="hidden" value="testimonials" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 
					<div class="progress progress_bar">
						<div class="bar"></div >
						<div class="percent">0%</div >
					</div>
				</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
