 

<style>
.service_photo{
	width: 60px;
}

</style>
 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>List of Cron job </h1>
		<a href="<?php echo base_url();?>configure_access/cronjob_setting" class="btn btn-default pull-right addAds">Add New Cronjob</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($cronjob_data == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr>
											<th class="text-center">S.No.</th>
											<th class="text-center">Title</th>
											<th class="text-center">C Start id</th>
											<th class="text-center">C end id</th>
											<th class="text-center">Subject</th>
											<th class="text-center">consultant_type</th>
											<th class="text-center">Bocy</th>
											<th class="text-center">Created Date</th>
										 
											<th class="text-center">Action</th></tr>
											</thead>
								<tbody>';
                                $i = 1;
                                foreach ($cronjob_data as $value) {
									//var_dump($cronjob_data);
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['title'] . '</td> <td class="text-center">' . $value['c_start_id'] . '</td> <td class="text-center">' . $value['c_end_id'] . '</td> <td class="text-center">' . $value['Subject'] . '</td>  <td class="text-center">' . $value['consultant_type'] . '</td> <td class="text-center">' . $value['bocy'] . '</td><td class="text-center">' . $value['created_date'] . '</td>';
									$content .= '<td class="text-center"><a href="'.base_url().'configure_access/cronjob_setting?id='. $value['cronjob_id'] . '"  name=' . $value['cronjob_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0)" class="remove_item"  name="'. $value['cronjob_id'] .'" table_name="cronjob" primary_column_name="cronjob_id" value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('document').ready(function(){

$('body').on('click', '.remove_item', function () {		
		
		if (!confirm("Do you want to delete")) {
            return false;
        }
        var id = $(this).attr('name');
        var primary_column_name = $(this).attr('primary_column_name');
        var table_name = $(this).attr('table_name');
		
		$.post(APP_URL + 'configure_access/remove', {
			id: id,
			table_name: table_name,
			primary_column_name: primary_column_name,
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {	
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					location.reload();
				});
			} else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			$.unblockUI();
			
		}, 'json');
		return false;
	});
});
</script>