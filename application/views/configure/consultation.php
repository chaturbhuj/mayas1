<link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/js/configure/consultation.js"></script> 
<script>
/*  $(function () {
        var oTable = $('#consultation_table').dataTable();
    });
	
function toggle(source) {
  checkboxes = document.getElementsByName('foo');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
*/


</script>

<?php 
$is_list_100 = $this->uri->segment(3);
echo ($is_list_100);
if($is_list_100 == 'match'){
	$select ='style="display:none !important;"';
	$heading = 'Matched Consultations';
	//var_dump($is_list_100);
}else if($is_list_100 == null){
	$heading = 'List of All Consultations';
	$select ='style="display:none !important;"';
	echo "<script>$(function () {var oTable = $('#consultation_table').dataTable();});</script>";
}else{
	
	$heading = 'Letest '.$is_list_100.' Consultations';
	$select ='';
}
?>


<div class="container-fluid main-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>     
                <div class="widget-content padded">
                    <div id="err_edit_sub_menu_services"></div>
                    <div id="edit_user_table">
                        <h2><?php echo $heading ;?></h2>
						<div id="headerMsg"></div>
                        <div id="table_view">             
                            <table class="table table-striped table-bordered table-hover" id="consultation_table">
                            <thead>
								<tr>
									<th class="text-center"> Sr. No. </th>
									
									<th class="text-center">Consult Code</th>
									<th class="text-center">Consult Type</th>
									<th class="text-center">Full Name</th>
									<th class="text-center">Gender</th>
									<th class="text-center">Date of Birth</th>
									<th class="text-center">Time of Birth</th>
									<th class="text-center">Place of Birth</th>
									<th class="text-center">Email ID</th>
									<th class="text-center">Appointment Status</th>
									
									<?php 
										echo '<th class="text-center" '.$select.'> Counter </th>';
										 ?> 
									
									
									<th class="text-center">Action</th>
									
								</tr>
							</thead>
							<tbody>
							<?php //var_dump($consultation_list);
                            if ($consultation_list == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                
                                $i = 1;
                                foreach ($consultation_list as $value) {
									$gender = $value['gender'];
                                    $content .= '<tr class="darker-on-hover">';
									
									
									$content .= '<td class="text-center"  >' . $i . '</br> <input '.$select.' type="checkbox" class="mcheckbox" name="foo" consultation_id="' . $value['consultation_id'] . '" inputContact="' . $value['inputContact'] . '" inputQuestion="' . $value['inputQuestion'] . '" inputEmail="' . $value['inputEmail'] . '"  full_name="' . $value['full_name'] . '"></td>';
									
									
                                    $content .= '<td class="text-center">' . $value['consultation_id'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['consultantType'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['full_name'] . '</td>';
                                    $content .= '<td class="text-center" name="'.$value['gender'].'">' . (substr($gender, 0, 1)). '</td>';
                                    $content .= '<td class="text-center">'.$value['inputDateDate']. '</td>';
									$content .= '<td class="text-center">'.$value['inputTimeMin'].'</td>';
									$content .= '<td class="text-center">'.$value['birthCity'].'</td>';
									$content .= '<td class="text-center">' . $value['inputEmail'] . '</br>Mo - ' . $value['inputContact'] . '</td>';
									
									if($value['appointment_yes'] == 'consultation-done'){
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-success">' . $value['appointment_yes'] . '</span></a></td>';
									}else if($value['appointment_yes'] == 'non-eligible'){
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-danger">' . $value['appointment_yes'] . '</span></a></td>';
									}else{
										$content .= '<td class="text-center"><a href="javascript:void(0);" class="appot_consul" data-toggle="modal" data-target="#browseChangeStatus" name="' . $value['consultation_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-default">' . $value['appointment_yes'] . '</span></a></td>';
									}
									if((int)$value['match_count'] > 1){
										$content .='<td class="text-center"  '.$select.'><a href="'.base_url().'page/consultation/match?inpd='.$value['consultation_id'].'" class="match_consul" ><span class="label label-success"> '.$value['match_count'].' match</span></a></td>';
									}else{
										$content .='<td class="text-center"  '.$select.'><span class="label label-default"> No match</span></td>';
									}
									
                                    $content .= '<td class="text-center">';
									$content .= '<a href="#" class="view_consultation" data-toggle="modal" data-target="#my_user_edit" name="' . $value['consultation_id'] . '" inputQuestion="' . $value['inputQuestion'] . '" educationQualification="' . $value['educationQualification'] . '" currentProfession="' . $value['currentProfession'] . '" partnerName="' . $value['partnerName'] . '" partnerDateDate="' . date('d M, Y',strtotime($value['partnerDateDate'])) . '" partnerTimeMin="' . $value['partnerTimeMin'] . '" partnerBirthCountry="' . $value['partnerBirthCountry'] . '" partnerBirthState="' . $value['partnerBirthState'] . '" partnerBirthCity="' . $value['partnerBirthCity'] . '" birthCountry="' . $value['birthCountry'] . '" birthState="' . $value['birthState'] . '" birthCity="' . $value['birthCity'] . '" question1_label="' . $value['question1_label'] . '" question2_label="' . $value['question2_label'] . '" question1="' . $value['question1'] . '" question2="' . $value['question2'] . '"><span class="label label-success">View</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_consultation"  name="' . $value['consultation_id'] . '" value=""><span class="label label-danger">Remove </span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0);" class="add_note_consultation"  name=' . $value['consultation_id'] . ' value="' . $value['full_name'] . '"><span class="label label-default">Add Note </span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0);" class="view_note_consuion"  name=' . $value['consultation_id'] . ' value="' . $value['full_name'] . '"><span class="label label-default">View All Notes </span></a>';
                                    $content .= '&nbsp;&nbsp;<a target="_blank" href="https://api.whatsapp.com/send?phone=+91' . $value['inputContact'] . '&text=Dear ' . $value['full_name'] . '%0aYou had registered for a Free Consultation on mayasAstrology.com. To avail of this life-changing astrology session CALL ASTROLOGER MAYANKESH NOW, %0aTime: 11 am - 01 pm at 9829265640" class="whatsappmsg"  "><span class="label label-default">whatsapp</span></a>';
                                    $content .= '</td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div><?php if($select == ''){echo '<button class="btn btn-info" id="selcetAllPending">Select All Pending</button>&nbsp;&nbsp;<button class="btn btn-info" id="selcetAllCompleted">Select All Completed</button>&nbsp;&nbsp;<button class="btn btn-info" id="selcetAll">Select All</button>&nbsp;&nbsp;<button class="btn btn-info" id="selcetUncechekAll">Uncheck All</button>&nbsp;&nbsp;<button class="btn btn-primary" id="read_foo" for_what="EMAIL">Send Email(Selected)</button>&nbsp;&nbsp;<button class="btn btn-primary" id="read_foo" for_what="SMS">Send SMS(Selected)</button>&nbsp;&nbsp;<a href="'.base_url().'page/consultation/'.((int)$is_list_100 + 100).'" style="color:#333;background:#eee;" class="btn btn-primary" id="">Load 100 More</a>';} ?> 
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('document').ready(function () {
		$('textarea.ckeditor').ckeditor({
			uiColor: '#9AB8F3'
		});
	});
</script>

<!-- Model -->
<div class="modal fade" id="my_user_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="consultation_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" >Consultation Detail</h4>
                </div>
                <div class="modal-body">
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="consultantType">Consultant Type : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="consultantType" name="consultantType"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="full_name">Full Name : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="full_name" name="full_name"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="gender">Gender : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="gender" name="gender"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="date_of_birth">Date of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="date_of_birth" name="date_of_birth"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="date_of_time">Time of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="date_of_time" name="date_of_time"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCountry">Birth Country : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCountry" name="birthCountry"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthState">Birth State : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthState" name="birthState"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCity">Birth City : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCity" name="birthCity"></span>
                        </div>
                    </div>
					<!--<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputEmail">Email ID : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputEmail" name="inputEmail"></span>
                        </div>
                    </div>-->
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputContact">Email/Contact NO. : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputContact" name="inputContact"></span>
                        </div>
                    </div>
					<!--option field-->
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="educationQualification">Education Qualification : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="educationQualification" name="educationQualification"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="currentProfession">Current Profession : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="currentProfession" name="currentProfession"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerName">Partner Name : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerName" name="partnerName"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partner_date_of_birth">Partner Date of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partner_date_of_birth" name="partner_date_of_birth"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partner_date_of_time">Partner Time of Birth : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partner_date_of_time" name="partner_date_of_time"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthCountry">Partner Birth Country : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthCountry" name="partnerBirthCountry"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthState">Partner Birth State : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthState" name="partnerBirthState"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="partnerBirthCity">Partner Birth City : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="partnerBirthCity" name="partnerBirthCity"></span>
                        </div>
                    </div>
					
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5 question1 question1_label" for="question1"></label>
                        <div class="col-md-7">
                            <span class="full_namel" id="question1" name="question1"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5 question2 question2_label" for="question2"></label>
                        <div class="col-md-7">
                            <span class="full_namel" id="question2" name="question2"></span>
                        </div>
                    </div>
					<div class="form-group" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputQuestion">Question : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputQuestion" name="inputQuestion"></span>
                        </div>
                    </div>
					
					<div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!---------------------------- Modal for Browse Send Sms-------------------------->
<div class="modal fade" id="browseNewCategory" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="pop_head"> Send SMS </h3>
            </div> 
            <div class="modal-body row">
				<p class="count_cunsultants" style="margin-left:15px;"></p>
				<div class="col-md-12">
					<form class="well form-inline" id="category_form" method="post" enctype="multipart/form-data">
					
						<input class="form-control" id="for_what" name="for_what" value="" type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<textarea class="form-control ckeditor" style="width: 100%;" rows="5" id="typend_msg" name="typend_msg" placeholder="Type Your Message Here....." type="text"></textarea>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Send</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Change Status-------------------------->
<div class="modal fade" id="browseChangeStatus" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Change Appointment Status</h3>
            </div> 
            <div class="modal-body row">
				<div class="col-md-12">
					<form class="well" id="category_form2" method="post" enctype="multipart/form-data">
						<input class="form-control" id="category_id2" name="category_id2" value=0 type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<label class="control-label col-md-3" for="category_status"> Status<span class="required">*</span></label>
							<div class="col-md-9">
								<select class="form-control" id="category_status" name="category_status">
									<option value="">Select Status</option>
									<option value="called">Called</option>
									<option value="eligible">Eligible</option>
									<option value="non-eligible">Non-Eligible</option>
									<option value="consultation-done">Consultation - done</option>
									<option value="sms-email">SMS/Email</option>
									<option value="pending">Pending</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Add Notes-------------------------->
<div class="modal fade" id="browseNewNote" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3> Add New Note  </h3>
            </div> 
            <div class="modal-body row">
				<p class="name_cunsultants" style="margin-left:15px;"></p>
				<div class="col-md-12">
					<form class="well form-inline" id="note_form" method="post" enctype="multipart/form-data">
					
						<input class="form-control" id="c_id" name="c_id" value="" type="hidden">
						<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
							<textarea class="form-control" style="width: 100%;" rows="4" id="typed_note" name="typed_note" placeholder="Write Something Here....." type="text"></textarea>
						</div>
						<div class="clearfix"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
        </div>
    </div>
</div>


<!---------------------------- Modal for Browse View Added Notes-------------------------->
<div class="modal fade" id="browseAllNotes" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3> All Notes  For &nbsp;<span class="name_noter"></span></h3>
            </div> 
            <div class="modal-body row">
				<div id="headMsg25"></div>
				<p class="msg_cunsultants" style="margin-left:15px;"></p>
				<div class="col-md-12">
					
					<div class="col-md-12" id="all_nota_" style="padding: 15px 0px 15px 0px">
						<!--<p style="width: 100%;background: #fff;padding: 5px;border-radius: 5px;" >  fcwu cuwec dcuwc c c </p>-->
					</div>
					<div class="clearfix"></div>
						
				</div>
			</div>
        </div>
    </div>
</div>