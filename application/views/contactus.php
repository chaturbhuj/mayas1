	<script src="<?php echo base_url(); ?>assets/js/contactus.js"></script>

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">

  function recaptchaCallback() {
       $('#btnSubmit').prop('disabled', false);
    }
</script>
<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Contact Us</h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb display_none">
            <li><a rel="nofollow" href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Contact us</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->

<section class="pad-top-50 pad-bottom-50">
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Email us</h3>
                    <p><strong>Email :</strong> info@mayasastrology.com</p>
                    <p><strong   style=" font-size: 23px; class="lsize">Facebook :</strong> @mayasastrology</p>

                </div>
            </div>
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Call us / WhatsApp</h3>
                    <p><strong class="lsize">Phone :</strong> +91 98292 65640 </p>
					<p><strong style=" font-size: 23px;" >Skype :</strong> mayankeshsharma</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pad-bottom-30">
                    <h3>Reach to us</h3>
                    <p><strong>C-108, Savitri Path, Bapu Nagar, Jaipur, Rajasthan 302015</strong></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pad-bottom-80 pt-5">
    <div class="container">

        <div class="section-title text-center">
            <h2>Send Us your Feedback</h2>
        </div>
        <!--.section-title-->

        <div class="row pt-3">
            <div class="col-md-6">
                <form id="add_contact_form" class="form-horizontal" method="post">
                    <div class="row">
                        <div class="col-md-12 contact_form">
                            <div class="form-group user-name">
                                <label for="nameFive-first" class="sr-only">Name</label>
                                <input type="text" class="form-control" required="" id="inputName" name="inputName" placeholder="First Name">
                            </div>

                            <div class="form-group user-email">
                                <label for="emailFive" class="sr-only">Email</label>
                                <input type="email" class="form-control" required="" id="inputEmail" name="inputEmail" placeholder="Email Address">
                            </div>


                            <div class="form-group user-phone">
                                <label for="websiteOne" class="sr-only">Website</label>
                                <input type="text" class="form-control" required="" id="inputWebsite" name="inputWebsite" placeholder="Phone">
                            </div>
                        </div><!-- /.col-md-6 -->
                        <div class="col-md-12 contact_form ">
                            <div class="form-group user-message">
                                <label for="messageOne" class="sr-only">Message</label>
                                <textarea class="form-control" required="" id="inputMessage" name="inputMessage" placeholder="Write Message"></textarea>
                            </div>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row-->
					 <div class="g-recaptcha" data-sitekey="6LeciF4UAAAAABGWgvw9414eQ2mBw262po0E9bVc" data-callback="recaptchaCallback"></div>
                    <button type="submit" class="btn btn-primary" disabled id="btnSubmit">Send Message</button>
                </form>
            </div>
			<div class="col-md-6">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14233.801044699114!2d75.8093813!3d26.8892018!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xbccd55597d7ffefc!2sMayas+Astrology!5e0!3m2!1sen!2sin!4v1496240117872" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div><!-- /.col-md-8 -->
        </div>
        <!--.row-->
    </div>
    <!-- .container -->
</section>
