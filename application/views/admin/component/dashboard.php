<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<style type="text/css">
	.highcharts-figure, .highcharts-data-table table {
	  min-width: 310px; 
	  max-width: 800px;
	  margin: 1em auto;
	}

	.highcharts-data-table table {
		font-family: Verdana, sans-serif;
		border-collapse: collapse;
		border: 1px solid #EBEBEB;
		margin: 10px auto;
		text-align: center;
		width: 100%;
		max-width: 500px;
	}
	.highcharts-data-table caption {
	  padding: 1em 0;
	  font-size: 1.2em;
	  color: #555;
	}
	.highcharts-data-table th {
		font-weight: 600;
	  padding: 0.5em;
	}
	.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
	  padding: 0.5em;
	}
	.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
	  background: #f8f8f8;
	}
	.highcharts-data-table tr:hover {
	  background: #f1f7ff;
	}
</style>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-12">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
  		<div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box ">
              <div class="inner">
                 <h3><a href="<?php echo base_url();?>admin/content_setting/country_view">Country</a></h3>
                 <h3><a href="<?php echo base_url();?>admin/content_setting/state_view">State</a></h3>
                 <h3><a href="<?php echo base_url();?> content_setting/city_view">City</a></h3>
              </div>
             
            </div>
          </div>
         
        </div>
       

      </div>
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
