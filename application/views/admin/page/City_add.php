
<style>
.error,.required{
	color:red;
}

.img-preview{
	width: 100px;
	margin-left: 20px;
}
</style>
<?php 

   
	if($details){
		$city_id = $details['id'];
		$city_name = $details['city'];
		$state_id = $details['state_id'];
		$country_id = $details['country_id'];
		
		
	}else{
		$city_id = 0;
		$city_name = '';
		$state_id = 0;
		$country_id = 0;
	}

?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add City</h1>
		<a href="<?php echo base_url();?>admin/content_setting/city_view" class="btn btn-default pull-right addAds"> View City</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
                <form class="form-horizontal" id="city_form">
						<div id="headerMsg"></div>
						<input type="hidden" name="city_id" id="city_id" value="<?php echo $city_id;?>">
						
						
						<div class="form-group ">
						<label class="col-lg-3 control-label"  for="state_name">Country Name <span class="required">*</span></label>
						<div class="col-lg-6">
						<select id="country_id" name="country_id" class="form-control " > 
									<option value="">Select country</option>
									<?php	
									
									 
									 foreach($countries as $val)
									 { 
									 if($country_id == $val['id'])
									 {
									  echo '<option selected value="'.$val["id"].'" name="'.$val["name"].'">' . $val['name'] . '</option>';
									 }
									 else
									 {
										 echo '<option value="'.$val["id"].'" name="'.$val["name"].'" >' . $val['name'] . '</option>';
									 }
									 }
									 
									?>
						</select>
						</div>
						</div>
						<div class="form-group ">
						<label class="col-lg-3 control-label"  for="state_name">State Name <span class="required">*</span></label>
						<div class="col-lg-6">
						<select id="stat_id" name="stat_id" class="form-control " > 
									<option value="">Select state</option>
									<?php	
									
									 foreach($states as $val)
									 { 
									 if($state_id == $val['id'])
									 {
									  echo '<option selected value="'.$val["id"].'" name="'.$val["name"].'">' . $val['name'] . '</option>';
									 }
									 else
									 {
										 echo '<option value="'.$val["id"].'" name="'.$val["name"].'" >' . $val['name'] . '</option>';
									 }
									 }
									?>
						</select>
						</div>
						
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="city_name">City Name <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text"  id="city_name" name="city_name" placeholder=" Enter city name" class="form-control" value="<?php echo $city_name;?>"> 
							</div>
						</div>
						<div class="form-group">
							 <div class="col-lg-offset-3 col-lg-6">
								<button class="btn btn-sm btn-primary" type="submit">Submit</button>
								<?php if($state_id){?>
								&nbsp;&nbsp;<a href="<?php echo base_url();?>admin/content_setting/city_view" class="btn btn-sm btn-warning">&nbsp;Back&nbsp;</a>
								<?php }?>
							</div>
						</div>
						<br><br>
					</form>
            </div>
        </div>
    </div>
</div>

<script>	
$(document).ready(function(){

	$('#city_form').validate({
		ignore: [],
        rules: {
            city_name: {
                required: true,
            },
			stat_id: {
                required: true,
            },
			country_id: {
                required: true,
            },
		},
		 messages: {
			city_name: {
                required: "City name is required.",
            },
			stat_id: {
                required: "State name is required.",
            },
			country_id: {
                required: "Country name is required.",
            },
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
			var state_id = $('#stat_id').val();
			var city_name = $('#city_name').val();
			var city_id = $('#city_id').val();
			var country_id = $('#country_id').val();
			console.log(state_id);
			
            $.post(APP_URL + 'admin/Content_setting/city_update', {
                state_id: state_id,
                city_name: city_name,
				city_id: city_id,
				country_id: country_id,
               
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/Content_setting/city_view';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
});
</script>	