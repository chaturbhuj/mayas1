<script src="<?php echo base_url(); ?>assets/js/blog.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>

<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>
<?php
  // print_r($public_page);
  
  
//   $public_page = true;
if($public_page){

    $heading = 'Edit Banner';

    $banner_id =  $public_page['banner_id'];
	
	$banner_content = $public_page['banner_content'];
	$banner_title = $public_page['banner_title'];
	$buttontext = $public_page['button_text'];
    $banner_image = $public_page['banner_image'];
    $button_url = $public_page['button_url'];

		  
} else {
    $banner_id =  0;
    $heading = 'Add Banner';

	
	$banner_content = '';
	$banner_title = '';
	$buttontext = '';
    $banner_image = '';
    $button_url = '';


	
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading ?></h1>
        <a href="<?php echo base_url();?>admin/content_setting/banner_list"
            class="btn btn-default pull-right addAds">View Banner list</a>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>

                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">

                        <input type="hidden" id="banner_id" name="banner_id" value=<?php echo  $banner_id; ?>>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="banner_title">Banner title<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_title" name="banner_title"
                                    value="<?php echo $banner_title ; ?>" placeholder="Enter Banner title">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="banner_image">Banner Image<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImage2">Upload Image</a>
                                <input class="form-control" id="banner_image" name="banner_image"
                                    value="<?php echo $banner_image;?>" type="hidden">
                                <?php if($banner_image != ''){
									echo '<img class="img-preview2" src="'.base_url().'uploads/'.$banner_image.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview2" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="content_banner">Banner Content</label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor content_banner" id="content_banner"
                                    name="content_banner" placeholder="Enter Banner Content"
                                    row="2"><?php echo $banner_content;?></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2" for="buttontext">Banner Button Text</label>
                            <div class="col-md-5">
                                <input class="form-control" id="buttontext" name="buttontext"
                                    value="<?php echo $buttontext ; ?>" placeholder="Button text">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2" for="button_url">Banner button_url</label>
                            <div class="col-md-5">
                                <input class="form-control" id="button_url" name="button_url"
                                    value="<?php echo $button_url;?>" placeholder="Enter button url">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImagesection3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_imagesection3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImagesection2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_imagesection2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    // Initialize CKEditor for all textareas with class ckeditor
    $('textarea.ckeditor').ckeditor({
        uiColor: '#9AB8F3'
    });
});

$('#upload_image1').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage1').modal('hide');
            $('#section_image1').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage1').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_image2').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage2').modal('hide');
            $('#banner_image').val(response.filename);
            $('#banner_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage2').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_image3').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview3").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage3').modal('hide');
            $('#meta_image').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_imagesection2').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-previewsection2").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImagesection2').modal('hide');
            $('#section_image2').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_imagesection3').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-previewsection3").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImagesection3').modal('hide');
            $('#section_image3').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
</script>


<script>
$(document).ready(function() {

    $('#add_page_form').validate({
        ignore: [],
        rules: {
           
            banner_title: {
                required: true
            },
            banner_image: {
                required: true
            },
            content_banner: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },
            buttontext: {
                required: false
            },
            button_url: {
                required: function(element) {
                    return $('#buttontext').val().trim().length > 0; // button_url required if button_text has a value
                }
            }
          
          
          
          




        },
        messages: {
            
            banner_title: {
                required: 'banner title is required'
            },
            banner_image: {
                required: 'banner image is required'
            },
            content_banner:{
                required: 'Banner content is required'

            },
            button_url: {
                required: 'Button url is required if button text is provided'
            }
            
          



        },
        errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
                error.insertAfter(element.closest('div.form-group').find('.content-error'));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {

            var banner_id = $('#banner_id').val();
   
            var button_url = $('#button_url').val();
            var buttontext = $('#buttontext').val();
            var banner_image = $('#banner_image').val();
            var banner_title = $('#banner_title').val();
            // var section_content = $('#section_content').val();



            // Access content of the second CKEditor instance
            var content_banner = CKEDITOR.instances['content_banner'].getData();
            console.log('Extra Description:', content_banner);




            console.log(content_banner);



            $.post(APP_URL + 'admin/content_setting/banner_update', {
                    banner_id: banner_id,
                
                    button_url: button_url,
              
                    buttontext: buttontext,
                    banner_image: banner_image,
                    banner_content: content_banner,
                    banner_title: banner_title,

                },
                function(response) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                    $('#headerMsg').empty();
                    if (response.status == 200) {
                        var message = response.message;

                        $('#headerMsg').html(
                            "<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            message + "</strong>&nbsp;&nbsp;<a href='" + APP_URL +
                            "page/blog_view'></a></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').remove();
                            window.location.href = APP_URL +
                                'admin/content_setting/banner_list';
                        });

                    } else if (response.status == 201) {
                        $('#headerMsg').html(
                            "<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            response.message + "</strong></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').empty();
                        });
                    }

                    //$('#blog_form').find('button[type="submit"]').prop('disabled',false);

                }, 'json');
            return false;
        },
    });



});
</script>