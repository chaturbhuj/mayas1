<script src="<?php echo base_url(); ?>assets/js/blog.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>

<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>
<?php
  // print_r($public_page);
  
  
//   $public_page = true;
if($public_page){

    $heading = 'Edit Vastu Content';

    $page_id =  $public_page['content_id'];
	$section_title1 = $public_page['section_title1'];
	$section_content1 = $public_page['section_content1'];
	$section_image1 = $public_page['section_image1'];
	$section_title2 = $public_page['section_title2'];
	$section_content2 = $public_page['section_content2'];
	$section_image2 = $public_page['section_image2'];
	$section_title3 = $public_page['section_title3'];
	$section_content3 = $public_page['section_content3'];
	$section_image3 = $public_page['section_image3'];
	$banner_content = $public_page['banner_content'];
	$banner_title = $public_page['banner_title'];
	$buttontext = $public_page['button_text'];
	$faq_category = $public_page['faq_category'];
	$banner_id = $public_page['banner_id'];
	$about_content_id = $public_page['about_content_id'];
    $metatitle = $public_page['meta_title'];
    $meta_description = $public_page['meta_description'];
    $meta_keyword = $public_page['meta_keyword'];
    $meta_image = $public_page['meta_image'];
    $banner_image = $public_page['banner_image'];
    $button_url = $public_page['button_url'];
    $section1position = $public_page['section1position'];
    $section2position = $public_page['section2position'];
    $section3position = $public_page['section3position'];
    $vastuwarning = $public_page['vastu_propertydirection_warning'];
		  
} else {
    $heading = 'Add Vastu Content';
	$page_id =  0;
	$section_keyword = '';
	$banner_content = '';
	$buttontext = '';
	$faq_category = 0;
    $vastuwarning = '';
    $banner_id = 0;
	$about_content_id = 0;
    $metatitle = '';
    $meta_description = '';
    $meta_keyword = '';
    $meta_image = '';
    $banner_image = '';
    $button_url = '#';
    $section_title1 = '';
	$section_content1 = '';
	$section_image1 = '';
	$section_title2 = '';
	$section_content2 = '';
	$section_image2 = '';
	$section_title3 = '';
	$section_content3 = '';
	$section_image3 = '';
    $banner_title = '';
    $section1position = '';
    $section2position = '';
    $section3position = '';


	
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading ?></h1>
        <a href="<?php echo base_url();?>admin/content_setting/public_vastucontent"
            class="btn btn-default pull-right addAds">View Vastu Content</a>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>

                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">

                        <input type="hidden" id="page_id" name="page_id" value=<?php echo  $page_id; ?>>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="banner_title">Banner title</label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_title" name="banner_title"
                                    value="<?php echo $banner_title ; ?>" placeholder="Enter Banner title">
                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="banner_image">Banner Image</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImage2">Upload Image</a>
                                <input class="form-control" id="banner_image" name="blog_image"
                                    value="<?php echo $banner_image;?>" type="hidden">
                                <?php if($banner_image != ''){
									echo '<img class="img-preview2" src="'.base_url().'uploads/'.$banner_image.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview2" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="content_banner">Banner Content</label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor content_banner" id="content_banner"
                                    name="content_banner" placeholder="Enter Banner Content"
                                    row="2"><?php echo $banner_content;?></textarea>
                            </div>
                        </div>


                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="buttontext">Banner Button Text</label>
                            <div class="col-md-5">
                                <input class="form-control" id="buttontext" name="buttontext"
                                    value="<?php echo $buttontext ; ?>" placeholder="Button text">
                            </div>
                        </div>


                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="button_url">Banner button_url</label>
                            <div class="col-md-5">
                                <input class="form-control" id="button_url" name="button_url"
                                    value="<?php echo $button_url;?>" placeholder="Enter button url">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_title1">About Vastu Section title</label>
                            <div class="col-md-5">
                                <input class="form-control" id="section_title1" name="section_title1"
                                    value="<?php echo $section_title1 ; ?>" placeholder="Enter Section Heading">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_image1">About Vastu Section Image</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="section_image1" name="section_image1"
                                    value="<?php echo $section_image1;?>" type="hidden">
                                <?php if($section_image1 != ''){
									echo '<img class="img-preview1" src="'.base_url().'uploads/'.$section_image1.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview1" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_content1">About Vastu Section Content</label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor section_content1" id="section_content1"
                                    name="section_content" value="" placeholder="Enter Section Content"
                                    row="2"><?php echo $section_content1;?></textarea>
                            </div>
                        </div>


                        <div class="form-group" >
                            <label class="control-label col-md-2" for="section1position">About Vastu Section Image position</label>
                            <div class="col-md-5">
                               <select style="width: 100%;" class="form-control" name="section1position"
												id="section1position">
												<!--<option value="">Select</option>-->
                                                <option value="" <?php if ($section1position == '') { echo 'selected';
                                                            # code...
                                                        } ?> selected>Select</option>
														<option <?php if ($section1position == 'left') { echo 'selected';
                                                            # code...
                                                        } ?> value="left">Left</option>
														<option <?php if ($section1position == 'right') { echo 'selected';
                                                            # code...
                                                        } ?> value="right">Right</option>
														<option <?php if ($section1position == 'none') { echo 'selected';
                                                            # code...
                                                        } ?> value="none">none</option>
													
											</select>								
															
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_title2">Hire Us Section title</label>
                            <div class="col-md-5">
                                <input class="form-control" id="section_title2" name="section_title2"
                                    value="<?php echo $section_title2 ; ?>" placeholder="Enter Section Heading">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_image2">Hire us Section Image</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImagesection2">Upload Image</a>
                                <input class="form-control" id="section_image2" name="section_image"
                                    value="<?php echo $section_image2;?>" type="hidden">
                                <?php if($section_image2 != ''){
									echo '<img class="img-previewsection2" src="'.base_url().'uploads/'.$section_image2.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-previewsection2" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="section_content2">Hire us Section Content</label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor section_content2" id="section_content2"
                                    name="section_content2" value="" placeholder="Enter Section Content"
                                    row="2"><?php echo $section_content2;?></textarea>
                            </div>
                        </div>

                        <div class="form-group" >
                            <label class="control-label col-md-2" for="section2position">Hire us Section Image position</label>
                            <div class="col-md-5">
                               <select style="width: 100%;" class="form-control" name="section2position"
												id="section2position">
                                                <option value="" <?php if ($section2position == '') { echo 'selected';
                                                            # code...
                                                        } ?> selected>Select</option>
														<option <?php if ($section2position == 'left') { echo 'selected';
                                                            # code...
                                                        } ?> value="left">Left</option>
														<option <?php if ($section2position == 'right') { echo 'selected';
                                                            # code...
                                                        } ?> value="right">Right</option>
														<option <?php if ($section2position == 'none') { echo 'selected';
                                                            # code...
                                                        } ?> value="none">none</option>
													
											</select>								
															
                            </div>
                        </div>


                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="vastualert">Vastu Direction Warning</label>
                            <div class="col-md-5">
                                <input class="form-control" id="vastualert" name="vastualert"
                                    value="<?php echo $vastuwarning ; ?>" placeholder="Enter Property Direciton Input Warning">
                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="section_title3">Section title3</label>
                            <div class="col-md-5">
                                <input class="form-control" id="section_title3" name="section_title3"
                                    value="<?php echo $section_title3 ; ?>" placeholder="Enter Section Heading">
                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="section_image3">section Image3</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImagesection3">Upload Image</a>
                                <input class="form-control" id="section_image3" name="section_image3"
                                    value="<?php echo $section_image3;?>" type="hidden">
                                <?php if($section_image3 != ''){
									echo '<img class="img-previewsection3" src="'.base_url().'uploads/'.$section_image3.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-previewsection3" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                        <div class="form-group display_none">
                            <label class="control-label col-md-2" for="section_content3">Section Content3</label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor section_content3" id="section_content3"
                                    name="section_content3" value="" placeholder="Enter Section Content"
                                    row="2"><?php echo $section_content3;?></textarea>
                            </div>
                        </div>

                        <div class="form-group display_none" >
                            <label class="control-label col-md-2" for="section3position">section3 Image position</label>
                            <div class="col-md-5">
                               <select style="width: 100%;" class="form-control" name="section3position"
												id="section3position">
												<!--<option value="">Select</option>-->
											
                                                <option value="" <?php if ($section3position == '') { echo 'selected';
                                                            # code...
                                                        } ?> selected>Select</option>
														<option <?php if ($section3position == 'left') { echo 'selected';
                                                            # code...
                                                        } ?> value="left">Left</option>
														<option <?php if ($section3position == 'right') { echo 'selected';
                                                            # code...
                                                        } ?> value="right">Right</option>
														<option <?php if ($section3position == 'none') { echo 'selected';
                                                            # code...
                                                        } ?> value="none">none</option>
													
											</select>								
															
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="metatitle">meta title</label>
                            <div class="col-md-5">
                                <input class="form-control" id="metatitle" name="metatitle"
                                    value="<?php echo $metatitle;?>" placeholder="Enter section Meta">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="meta_keyword">meta keyword</label>
                            <div class="col-md-5">
                                <input class="form-control" id="meta_keyword" name="meta_keyword"
                                    value="<?php echo $meta_keyword;?>" placeholder="Enter section Meta">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="meta_description">meta description</label>
                            <div class="col-md-5">
                                <input class="form-control" id="meta_description" name="meta_description"
                                    value="<?php echo $meta_description;?>" placeholder="Enter section Meta">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2" for="meta_image">meta Image</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImage3">Upload Image</a>
                                <input class="form-control" id="meta_image" name="meta_image"
                                    value="<?php echo $meta_image;?>" type="hidden">
                                <?php if($meta_image != ''){
									echo '<img class="img-preview3" src="'.base_url().'uploads/'.$meta_image.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview3" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>

                       

                        <div class="form-group">
                            <label class="control-label col-md-2" for="faq_category">Faq Category ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="faq_category" name="faq_category"
                                    value="<?php echo $faq_category ; ?>" placeholder="Enter faq category">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="about_content_id">Aboutus Content ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="about_content_id" name="about_content_id"
                                    value="<?php echo $about_content_id; ?>" placeholder="Enter Content ID ">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="banner_id">Banner ID</label>
                            <div class="col-md-5">
                                <input class="form-control" id="banner_id" name="banner_id"
                                    value="<?php echo $banner_id ; ?>" placeholder="Enter banner_id">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImagesection3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_imagesection3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImagesection2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_imagesection2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    // Initialize CKEditor for all textareas with class ckeditor
    $('textarea.ckeditor').ckeditor({
        uiColor: '#9AB8F3'
    });
});

$('#upload_image1').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage1').modal('hide');
            $('#section_image1').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage1').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_image2').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview2").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage2').modal('hide');
            $('#banner_image').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage2').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_image3').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-preview3").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImage3').modal('hide');
            $('#meta_image').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_imagesection2').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-previewsection2").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImagesection2').modal('hide');
            $('#section_image2').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
$('#upload_imagesection3').ajaxForm({
    dataType: 'JSON',
    success: function(response) {
        if (response.status == 200) {
            $(".img-previewsection3").attr('src', APP_URL + 'uploads/' + response.filename);
            $('#browseImagesection3').modal('hide');
            $('#section_image3').val(response.filename);
            $('#blog_image-error').css({
                "display": "none"
            });
        } else {
            $('#browseImage3').modal('hide');
            alert(response.message);

        }
        $.unblockUI();
        return false;
    }
});
</script>


<script>
$(document).ready(function() {

    $('#add_page_form').validate({
        ignore: [],
        rules: {
           
            meta_description: {
                required: true
            },
            meta_keyword: {
                required: true
            },
            metatitle: {
                required: true
            },
          




        },
        messages: {
            meta_description: {
                required: "Meta description is required"
            },
            meta_keyword: {
                required: "Meta keyword is required"
            },
            metatitle: {
                required: "Meta title is required"
            },



        },
        errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
                error.insertAfter(element.closest('div.form-group').find('.content-error'));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {

            var page_id = $('#page_id').val();
            var meta_image = $('#meta_image').val();
            var meta_description = $('#meta_description').val();
            var meta_keyword = $('#meta_keyword').val();
            var button_url = $('#button_url').val();
            var metatitle = $('#metatitle').val();
            var vastualert = $('#vastualert').val();
            var buttontext = $('#buttontext').val();
            var banner_image = $('#banner_image').val();
            var banner_title = $('#banner_title').val();
            // var section_content = $('#section_content').val();
            var faq_category = $('#faq_category').val();
            var banner_id = $('#banner_id').val();
            var about_content_id = $('#about_content_id').val();

            var section_image1 = $('#section_image1').val();
            console.log(section_image1 + 'this is image');
            var section_title1 = $('#section_title1').val();
            // var content_banner = $('#content_banner').val();

            var section_content1 = CKEDITOR.instances['section_content1'].getData();
            console.log('Section Content:', section_content1);

            var section_image2 = $('#section_image2').val();
            var section_title2 = $('#section_title2').val();
            // var content_banner = $('#content_banner').val();

            var section_content2 = CKEDITOR.instances['section_content2'].getData();
            console.log('Section Content:', section_content2);
            var section_image3 = $('#section_image3').val();
            var section1position = $('#section1position').val();
            console.log(section1position);
            var section2position = $('#section2position').val();
            var section3position = $('#section3position').val();
            var section_title3 = $('#section_title3').val();
            // var content_banner = $('#content_banner').val();

            var section_content3 = CKEDITOR.instances['section_content3'].getData();
            console.log('Section Content:', section_content2);

            // Access content of the second CKEditor instance
            var content_banner = CKEDITOR.instances['content_banner'].getData();
            console.log('Extra Description:', content_banner);




            console.log(content_banner);



            $.post(APP_URL + 'admin/content_setting/page_vastu_update', {
                    page_id: page_id,
                    meta_image: meta_image,
                    meta_keyword: meta_keyword,
                    button_url: button_url,
                    vastualert: vastualert,
                    meta_description: meta_description,
                    metatitle: metatitle,
                    buttontext: buttontext,
                    banner_id: banner_id,
                about_content_id: about_content_id,
                    banner_image: banner_image,
                    banner_content: content_banner,
                    banner_title: banner_title,
                    section_content1: section_content1,
                    section_image1: section_image1,
                    section_title1: section_title1,
                    section_content2: section_content2,
                    section_image2: section_image2,
                    section_title2: section_title2,
                    section_content3: section_content3,
                    section_image3: section_image3,
                    section_title3: section_title3,
                    section1position: section1position,
                    section2position: section2position,
                    section3position: section3position,

                    faq_category: faq_category,


                },
                function(response) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                    $('#headerMsg').empty();
                    if (response.status == 200) {
                        var message = response.message;

                        $('#headerMsg').html(
                            "<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            message + "</strong>&nbsp;&nbsp;<a href='" + APP_URL +
                            "page/blog_view'></a></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').remove();
                            window.location.href = APP_URL +
                                'admin/content_setting/public_vastucontent';
                        });

                    } else if (response.status == 201) {
                        $('#headerMsg').html(
                            "<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            response.message + "</strong></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').empty();
                        });
                    }

                    //$('#blog_form').find('button[type="submit"]').prop('disabled',false);

                }, 'json');
            return false;
        },
    });



});
</script>