<script src="<?php echo base_url(); ?>assets/js/blog.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>

<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>
<?php
  // print_r($public_page);
  
  
//   $public_page = true;
if($public_page){

    $heading = 'Edit Vastu about content';

    $page_id =  $public_page['id'];
	$about_title = $public_page['about_title'];
	$about_content = $public_page['about_content'];
	$button_text = $public_page['button_text'];
	$button_url = $public_page['button_url'];

		  
} else {
    $heading = 'Add Vastu about content';

    $page_id =  0;
	$about_title = '';
	$about_content = '';
	$button_text = '';
	$button_url = '';

	
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading ?></h1>
        <a href="<?php echo base_url();?>admin/content_setting/public_vastuaboutcontent"
            class="btn btn-default pull-right addAds">View Vastu About Content</a>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>

                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">

                        <input type="hidden" id="page_id" name="page_id" value=<?php echo  $page_id; ?>>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="about_title">About Title<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="about_title" name="about_title"
                                    value="<?php echo $about_title ; ?>" placeholder="Enter About Title">
                            </div>
                        </div>

                    

                   


                        <div class="form-group">
                            <label class="control-label col-md-2" for="buttontext">Button Text<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="buttontext" name="buttontext"
                                    value="<?php echo $button_text ; ?>" placeholder="Enter Button Text">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-2" for="button_url">Button Url<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="button_url" name="button_url"
                                    value="<?php echo $button_url;?>" placeholder="Enter Button Url">
                            </div>
                        </div>

                

                

                        <div class="form-group">
                            <label class="control-label col-md-2" for="about_content">About Content<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor about_content" name="about_content" id="about_content"
                                    name="section_content" value="" placeholder="Enter About Content"
                                    row="2"><?php echo $about_content;?></textarea>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function() {
    // Initialize CKEditor for all textareas with class ckeditor
    $('textarea.ckeditor').ckeditor({
        uiColor: '#9AB8F3'
    });
});

</script>


<script>
$(document).ready(function() {

    $('#add_page_form').validate({
        ignore: [],
       
            rules: {


               
            button_url: {
                required: true
            },
            buttontext: {
                required: true
            },
            about_title: {
                required: true
            },
           
            about_content: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },
          




        },
        messages: {
            about_content: {
                required: "About content is required"
            },
             button_url: {
                required: "Button Url is required"
            },
             buttontext: {
                required: "Button Text is required"
            },
            about_title: {
                required: "About title is required"
            },



        },
        errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
                error.insertAfter(element.closest('div.form-group').find('.content-error'));
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {

            var page_id = $('#page_id').val();
            var button_url = $('#button_url').val();
            var buttontext = $('#buttontext').val();
            var about_title = $('#about_title').val();
            // var content_banner = $('#content_banner').val();

            var about_content = CKEDITOR.instances['about_content'].getData();
            console.log('Section Content:', about_content);

   



            $.post(APP_URL + 'admin/content_setting/page_vastuabout_update', {
                    page_id: page_id,
                    button_url: button_url,
                    buttontext: buttontext,
                    about_title: about_title,
                    about_content: about_content,
                 


                },
                function(response) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                    $('#headerMsg').empty();
                    if (response.status == 200) {
                        var message = response.message;

                        $('#headerMsg').html(
                            "<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            message + "</strong>&nbsp;&nbsp;<a href='" + APP_URL +
                            "page/blog_view'></a></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').remove();
                            window.location.href = APP_URL +
                                'admin/content_setting/public_vastuaboutcontent';
                        });

                    } else if (response.status == 201) {
                        $('#headerMsg').html(
                            "<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            response.message + "</strong></div>");
                        $("#headerMsg").fadeTo(3000, 500).slideUp(500, function() {
                            $('#headerMsg').empty();
                        });
                    }

                    //$('#blog_form').find('button[type="submit"]').prop('disabled',false);

                }, 'json');
            return false;
        },
    });



});
</script>