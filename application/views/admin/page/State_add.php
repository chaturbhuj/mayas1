
<style>
.error,.required{
	color:red;
}

.img-preview{
	width: 100px;
	margin-left: 20px;
}
</style>
<?php 

   
	if($details){
		$state_id = $details['id'];
		$state_name = $details['name'];
		$country_id = $details['country_id'];
		
		
	}else{
		$state_id = 0;
		$state_name = '';
		$country_id = '';
	}

?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add State</h1>
		<a href="<?php echo base_url();?>admin/content_setting/state_view" class="btn btn-default pull-right addAds"> View State</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
                <form class="form-horizontal" id="state_form">
						<div id="headerMsg"></div>
						<input type="hidden" name="state_id" id="state_id" value="<?php echo $state_id;?>">
						
						
						<div class="form-group ">
						<label class="col-lg-3 control-label"  for="country_name">Country Name <span class="required">*</span></label>
						<div class="col-lg-6">
						<select id="country_id" name="country_id" class="form-control " > 
									<option value="">Select country</option>
									<?php	
									if($country_id == $val['zone_name'])
									{ $selected ='selected';}
							     
									 
									 foreach($countries as $val)
									 { 
									 if($country_id == $val['id'])
									 {
									  echo '<option selected value="'.$val["id"].'" name="'.$val["name"].'">' . $val['name'] . '</option>';
									 }
									 else
									 {
										 echo '<option value="'.$val["id"].'" name="'.$val["name"].'" >' . $val['name'] . '</option>';
									 }
									 
									 }
									?>
						</select>
						</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="country_name">State Name <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text"  id="state_name" name="state_name" placeholder=" Enter state name" class="form-control" value="<?php echo $state_name;?>"> 
							</div>
						</div>
						<div class="form-group">
							 <div class="col-lg-offset-3 col-lg-6">
								<button class="btn btn-sm btn-primary" type="submit">Submit</button>
								<?php if($state_id){?>
								&nbsp;&nbsp;<a href="<?php echo base_url();?>admin/settings/zone_view" class="btn btn-sm btn-warning">&nbsp;Back&nbsp;</a>
								<?php }?>
							</div>
						</div>
						<br><br>
					</form>
            </div>
        </div>
    </div>
</div>

<script>	
$(document).ready(function(){

	$('#state_form').validate({
		ignore: [],
        rules: {
            state_name: {
                required: true,
            },
			country_id: {
                required: true,
            },
			
		},
		 messages: {
			state_name: {
                required: "State name is required.",
            },
			country_id: {
                required: "Country name is required.",
            },
			
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
			var state_id = $('#state_id').val();
			var state_name = $('#state_name').val();
			var country_id = $('#country_id').val();
			
            $.post(APP_URL + 'admin/Content_setting/state_update', {
                state_id: state_id,
                state_name: state_name,
                country_id: country_id,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/Content_setting/state_view';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
});
</script>	