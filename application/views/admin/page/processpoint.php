<script src="<?php echo base_url(); ?>assets/js/blog.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
    
<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>
<?php
  
  
//   $public_page = true;
if($public_page){

    $heading = 'Edit Process Content';
    $process_id =  $public_page['process_id'];
	$process_title = $public_page['process_title'];
	$process_content = $public_page['process_content'];
	$process_image = $public_page['process_image'];

		  
} else {
    $heading = 'Add Process Content';

    $process_id =  0;
	$process_title = '';
	$process_content = '';
	$process_image = '';
	
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading ?></h1>
        <a href="<?php echo base_url();?>admin/content_setting/public_process"
            class="btn btn-default pull-right addAds">View Process list</a>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>

                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">

                        <input type="hidden" id="process_id" name="process_id" value=<?php echo  $process_id; ?>>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="process_title">Process Title<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="process_title" name="process_title"
                                    value="<?php echo $process_title ; ?>" placeholder="Enter Process Title">
                            </div>
                        </div>
                     

                        <div class="form-group">
                            <label class="control-label col-md-2" for="process_image">Process Image</label>
                            <div class="col-md-5">
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal"
                                    data-target="#browseImage1">Upload Image</a>
                                <input class="form-control" id="process_image" name="process_image"
                                    value="<?php echo $process_image;?>" type="hidden">
                                <?php if($process_image != ''){
									echo '<img class="img-preview1" src="'.base_url().'uploads/'.$process_image.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview1" src="" alt="" style="width: 100px;">';
								}?>



                            </div>
                        </div>


                    
                        <div class="form-group my-2">
                            <label class="control-label col-md-2" for="process_content">Process Content<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor process_content" id="process_content"
                                    name="process_content" value="" placeholder="Enter Process Content"
                                    row="2"><?php echo $process_content;?></textarea>
                            </div>
                        </div>
                     

                     




                <div class="form-group">
                    <div class="col-md-5 col-md-offset-2">
                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        // Initialize CKEditor for all textareas with class ckeditor
        $('textarea.ckeditor').ckeditor({
            uiColor: '#9AB8F3'
        });
    });

    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function(response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#process_image').val(response.filename);
                $('#blog_image-error').css({
                    "display": "none"
                });
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);

            }
            $.unblockUI();
            return false;
        }
    });

  

</script>


<script>	
$(document).ready(function(){

	$('#add_page_form').validate({
		ignore: [],
        rules: {
            
         
            process_title: {
                required: true
            },
            process_content: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },
           
			
			
		},
		 messages: {
            process_title: {
                required: "Process title is required"
            },
            process_content: {
                required: "Process content is required"
            },
            
          
		
			
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
            var process_id = $('#process_id').val();
            var process_image = $('#process_image').val();
            var process_title = $('#process_title').val();

                        var process_content = CKEDITOR.instances['process_content'].getData();
    console.log('Section Content:', process_content);





			
		
		    		
            $.post(APP_URL + 'admin/content_setting/page_process_update', {
                process_id: process_id,
                    process_content: process_content,
                    process_image:process_image,
                    process_title: process_title,
                
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/content_setting/public_process';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	
	
});
</script>

