<link href="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.css" rel="stylesheet">

<script>
$(function() {
    var oTable = $('#vastu_table').dataTable();
});
</script>
<style>
.error,
.required {
    color: red;
}
</style>


<style>
#headerMsg {
    margin: 20px 0px;
}

.dataTables-example th {
    text-align: center;
}

.display_none {
    display: none;
}
</style>

<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Vastu Consultations View</h1>
        <a style="float: right;" href="<?php echo base_url();?>admin/content_setting/state_add"
            class="btn btn-default pull-right"> </a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
                <div id="headerMsg"></div>
                <div class="table-responsive">
                    <table id="vastu_table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">S. No.</th>
                                <th class="text-center">Consult Code</th>
                                <th class="text-center">Consultant Type </th>
                                <th class="text-center"> Name </th>
                                <!-- <th class="text-center"> Property Address </th> -->
                                <th class="text-center"> Property Direction </th>
                                <th class="text-center"> Files </th>
                                <!-- <th class="text-center">Gender </th> -->
                                <th class="text-center">Date And Time Of Birth </th>
                                <th class="text-center">Country Name </th>
                                <th class="text-center">State Name </th>
                                <th class="text-center">Birth City </th>
                                <th class="text-center">Email Id</th>
                                <th class="text-center">Contact No</th>
                                <th class="text-center">Created Date </th>
                                <th class="text-center">Created Time </th>
                                <th class="text-center">Appointment Status </th>
                                <th class="text-center"> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
			     $i = 1;
				//  var_dump($vastu_consultation);
			    if($vastu_consultation == 0)
				{
					echo "No record found into database";
					
				} else {
					$content = '';
					foreach($vastu_consultation as $value){
						$address = '';
					    //var_dump($value);
                    	$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
						$content .= '<td class="text-center" >V-' . $value['vastu_id'].'</td>';
						$content .= '<td class="text-center" >' . $value['consultantType']. '</td>';
						$content .= '<td class="text-center" >' . $value['inputName']. '</td>';
						if(!empty($value['address'])){
							$address = $value['address'];
						}else{
							$address = 'No Address';
						}
						if(!empty($value['propertydirection'])){
						$content .= '<td class="text-center" >' . $value['propertydirection']. '</td>';
					   }else{
						$content .= '<td class="text-center" >No Direction</td>';
					   }
						$file_names = $value['file_names']; // Decode JSON array of file names
						$content .= '<td class="text-center">';
						if (!empty($file_names)) {
							foreach ($file_names as $file) {
								$content .= '<a target="_blank" href="'.base_url().'uploads/' . $file . '">' . $file . '</a><br>';
							}
						} else {
							$content .= 'No files';
						}
						$content .= '</td>';
						// $content .= '<td class="text-center" >' . $value['inputGender']. '</td>';
						if(empty($value['inputDateDate']) && empty($value['inputTimeMin']) ){
							$content .= '<td class="text-center" >No Date and Time of Birth</td>';
					} else {
						$content .= '<td class="text-center" >' . $value['inputDateDate']. $value['inputTimeMin'].'</td>';
					}


					if(!empty($value['birthCountry'])){
						$content .= '<td class="text-center" >' . $value['birthCountry']. '</td>';
					}else{
						$content .= '<td class="text-center" >No BirthCountry</td>';
					   }

					   if(!empty($value['birthState'])){
						$content .= '<td class="text-center" >' . $value['birthState']. '</td>';
					}else{
						$content .= '<td class="text-center" >No BirthState</td>';


					   }

					   if(!empty($value['birthCity'])){
						$content .= '<td class="text-center" >' . $value['birthCity']. '</td>';
					}else{
						$content .= '<td class="text-center" >No BirthCity</td>';
					   }
						$content .= '<td class="text-center" >' . $value['inputEmail']. '</td>';
						$content .= '<td class="text-center" >' . $value['inputContact']. '</td>';
						$content .= '<td class="text-center" >' . $value['created_date']. '</td>';
						$content .= '<td class="text-center" >' . $value['created_time']. '</td>';
						
						if($value['appointment_yes'] == 'consultation-done'){
						$content .= '<td class="text-center"><a href="javascript:void(0);" class="vastu_consult" data-toggle="modal" data-target="#my_vastu_change" name="' . $value['vastu_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-success">' . $value['appointment_yes'] . '</span></a></td>';
						}else if($value['appointment_yes'] == 'non-eligible'){
						$content .= '<td class="text-center"><a href="javascript:void(0);" class="vastu_consult" data-toggle="modal" data-target="#my_vastu_change" name="' . $value['vastu_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-danger">' . $value['appointment_yes'] . '</span></a></td>';
						}else{
						$content .= '<td class="text-center"><a href="javascript:void(0);" class="vastu_consult" data-toggle="modal" data-target="#my_vastu_change" name="' . $value['vastu_id'] . '" value="' . $value['appointment_yes'] . '"><span class="label label-default">' . $value['appointment_yes'] . '</span></a></td>';
						}
						
					    $content .= '<td class="text-center"><a href="#" class="vastu_consultation"  data-toggle="modal" data-target="#my_vastu_edit"  propertyaddress="'.$address.'"><span class="label label-success">View</span></a>'; 
					    $content .= '&nbsp;&nbsp;<a href="javascript:void(0);" class="remove_vastu_consultation"  name=' . $value['vastu_id'] . ' value="'. $value['vastu_id'] .'"><span class="label label-danger">Remove </span></a></td>';
					   $i++;
					}
					
					echo $content;
				}	
			?>

                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------------------- Modal for Browse Change Status-------------------------->
<div class="modal fade" id="my_vastu_change" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">

            <div class="modal-header">

                <div id="headerMsg1"></div>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h3>Change Appointment Status</h3>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <form class="well" id="vastu_form2" method="post" enctype="multipart/form-data">
                        <input class="form-control" id="category_id2" name="category_id2" value=0 type="hidden">
                        <div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
                            <label class="control-label col-md-3" for="category_status"> Status<span
                                    class="required">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control" id="category_status" name="category_status">
                                    <option value="">Select Status</option>
                                    <option value="called">Called</option>
                                    <option value="eligible">Eligible</option>
                                    <option value="non-eligible">Non-Eligible</option>
                                    <option value="consultation-done">Consultation - done</option>
                                    <option value="sms-email">SMS/Email</option>
                                    <option value="pending">Pending</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Model -->
<div class="modal fade" id="my_vastu_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="consultation_form" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Consultation Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="consultantType">Consultant Type : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="consultantType" name="consultantType"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="consultantcode">Consultant code : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="consultantcode" name="consultantcode"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="full_name">Full Name : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="full_name" name="full_name"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="propertyaddress">Property Address : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="propertyaddress" name="propertyaddress"></span>
                        </div>
                    </div>



                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="propertydirection">Property Direction : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="propertydirection" name="propertydirection"></span>
                        </div>
                    </div>


                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="vastufile">Files : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="vastufile" name="vastufile"></span>
                        </div>
                    </div>
				
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCountry">Birth Country : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCountry" name="birthCountry"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthState">Birth State : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthState" name="birthState"></span>
                        </div>
                    </div>


                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="date_of_birth">Date of Birth & Time: </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="date_of_birth" name="date_of_birth"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="birthCity">Birth City : </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="birthCity" name="birthCity"></span>
                        </div>
                    </div>

                    <!--option field inputQuestion educationQualification -->
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="email_id">Email ID </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="email_id" name="email_id"></span>
                        </div>
                    </div>
                    <div class="row" style="padding: 15px 0px 15px 0px">
                        <label class="control-label col-md-5" for="inputEmail">contact: </label>
                        <div class="col-md-7">
                            <span class="full_namel" id="inputContact" name="inputContact"></span>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
			</div>
            </form>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/plugins/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/configure/vastu_consultation.js"></script>