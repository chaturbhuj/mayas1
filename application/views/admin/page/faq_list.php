<script src="<?php echo base_url(); ?>assets/js/aboutus.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<?php
// var_dump($vastu);


?>

</script>
<style>
.service_photo{
	width: 60px;
}

</style>
 
<div class="container-fluid main-content">
    <div class="page-title">
        <h1>Faq List</h1>
		<a href="<?=base_url().'admin/content_setting/getdata'?>" class="btn btn-default pull-right addAds">Add New Faq</a>
    </div>
	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_services"></div>
                    <div id="edit_team_table">
                        
                        <div id="table_view">             
                            <?php
                            if ($faq_list == 0) {
                                echo 'No record found into database';
                            } else {
                                // var_dump($faq_list);
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Question</th><th class="text-center">Faq Category</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($faq_list as $value) {

                               
                                    $vastufaqcategory = ' - ';
                                    
                            $this->db->where('status','active');    
                            $this->db->where('faq_category_id ' ,$value['faq_category_id']);    
                            $category = $this->db->get('admin_faq_category')->result_array();
        
                            if (count($category) > 0) {
                                $single_result = $category[0];
                                $vastufaqcategory  =  $single_result['faq_category_name'];
                            }

                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['question'] . '</td>';
                                    $content .= '<td class="text-center">' . $vastufaqcategory . '</td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'admin/content_setting/getdata?id='. $value['faq_id'] . '"  name=' . $value['faq_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="javascript:void(0)" class="remove_date"  name="'. $value['faq_id'] .'" value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        //-----------------------------------------------------------------------
        /*
        * validation
        */
        $('#edit_vastu_form').validate({
            ignore:[],
            rules: {
                home_page_heading:{
                    required: true,
                },

                youtube_link: {
                    required: true,
                },

                url: {
                    required: true,
                },

                page_title: {
                    required: true,
                },
                meta_tag: {
                    required: true,
                },
                meta_descp: {
                    required: true,
                },
                home_page_content: {
                        required: function(textarea) {
                                CKEDITOR.instances[textarea.id].updateElement(); // update textarea
                                var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
                                return editorcontent.length === 0;
                        }
                },
            },
            messages: {

                // page: {
                //     required: "Page Heading  is required",
                // },
								//
                // data: {
                //     required: "Content is required",
                // },
                // header_image: {
                //     required: "Image is required",
                // },
                // video_link: {
                //     required: "Vide Link is required",
                // },

            },

            submitHandler: function (form) {
                var home_page_heading = $('#home_page_heading').val();
                var youtube_link = $('#youtube_link').val();
                var url = $('#url').val();
                var page_title = $('#page_title').val();
                var meta_tag = $('#meta_tag').val();
                var meta_descp = $('#meta_descp').val();
                var home_page_content =  CKEDITOR.instances['home_page_content'].getData();

                $.post(APP_URL + 'configure_access/add_vastu_data', {
                    id: 1,
                    home_page_heading:home_page_heading,
                    youtube_link:youtube_link,
                    url:url,
                    page_title:page_title,
                    meta_tag:meta_tag,
                    meta_descp:meta_descp,
                    home_page_content:home_page_content,
                   
                },
                function (response) {
                    $("html, body").animate({scrollTop: 0}, "slow");
                    $('#err_page_form').empty();
                    if (response.status == 200) {
                        $('#err_page_form').html("<div class='alert alert-success fade in'>\n\
                    <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                    <strong>" + response.message + "</strong></div>");
                    }
                    else if (response.status == 201) {
                        $('#err_page_form').html("<div class='alert alert-danger fade in'>\n\
                    <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                    <strong>" + response.message + "</strong></div>");
                    }
                    // $('#idd').val(0);
                    // $('#page').val('');
                    // $('#data').val('');
                    // $('#video_link').val('');
                    // $('#header_image').val('');
                    // $('.img-preview').attr('src','');
                }, 'json');
                return false;
            }
        });
        

          //---------------------------------------------------------------------
    /*
     * This script is used to remove user from the list
     */
    $('body').on('click', '.remove_date', function () {
        if (!confirm("Do you want to delete")) {
            return false;
        }
        var faq_id = $(this).attr('name');
        $.post(APP_URL + 'configure_access/removewithcache', {
            table_name: 'admin_faq',
            primary_column_name: 'faq_id',
            id: faq_id
        }, function (response) {
            $('#err_edit_services').empty();
            if (response.status == 200) {
                $("html, body").animate({scrollTop: 0}, "slow");
                // $('#edit_user_table').show();

					

                $('#err_edit_services').html("<div class='alert alert-success fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");

                $('.remove_date[name=' + faq_id + ']').closest("tr").remove();
                $("#err_edit_services").fadeTo(3000, 500).slideUp(500, function(){
						$('#err_edit_services').remove();
                        location.reload();


					});
            }
            else {
                $('#err_edit_services').html("<div class='alert alert-danger fade in'>\n\
                        <button class='close' type='button' data-dismiss='alert'>x</button>\n\
                        <strong>" + response.message + "</strong></div>");
            }
        }, 'json');
        return false;
    });

    });
</script>
