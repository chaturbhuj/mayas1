<script src="<?php echo base_url(); ?>assets/js/blog.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
    
<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<script>
$('textarea.ckeditor').ckeditor({
    uiColor: '#9AB8F3'
});
</script>
<?php
  // print_r($public_page);
  
  
//   $public_page = true;
if ($records) {
    $heading = 'Edit FAQ';
    $faq_id = $records['faq_id'];
    $faq_category_id = $records['faq_category_id'];
    $faq_category_name = $records['faq_category_name'];
    $question = $records['question'];
    $answers = $records['answers'];
    $priority = $records['priority'];


} else {
    $heading = 'Add FAQ';
    $faq_id = 0;
    $faq_category_id = 0;
    $faq_category_name = '';
    $question = '';
    $answers = '';
    $status = '';
    $priority = 0;
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
    <div class="page-title">
        <h1><?php echo $heading ?></h1>
        <a href="<?php echo base_url();?>admin/content_setting/faqlist"
            class="btn btn-default pull-right addAds">View Faq list</a>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br />
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>

                <div class="widget-content padded">
                    <div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">

                        <input type="hidden" id="faq_id" name="faq_id" value=<?php echo  $faq_id; ?>>
                        <div class="form-group" >
                            <label class="control-label col-md-2" for="faqcategory_id">Faq Category</label>
                            <div class="col-md-5">
                               <select style="width: 100%;" class="form-control" name="faq_category_name"
												id="faq_category_name">
												<!--<option value="">Select</option>-->
												<?php $i = 1;

												if ($category_list != '') {
													foreach ($category_list as $value) { ?>
														<option name="<?php echo $value['faq_category_id']; ?>"
															value="<?php echo $value['faq_category_name']; ?>" <?php if ($faq_category_name == $value['faq_category_name']) {
																  echo 'selected';
															  } ?>><?php echo $value['faq_category_name']; ?></option>
														<?php $i++;
													}
												} ?>
											</select>								
															
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2" for="question">Question<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="question" name="question"
                                    value="<?php echo $question ; ?>" placeholder="Enter Question">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2" for="priority">Priority</label>
                            <div class="col-md-5">
                                <input type="number" class="form-control" id="priority" name="priority"
                                    value="<?php echo $priority; ?>" placeholder="Enter Priority">
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label class="control-label col-md-2" for="answers">Answer<span
                                    class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor answers" id="answers"
                                    name="answers" value="" placeholder="Enter Answers"
                                    row="2"><?php echo $answers;?></textarea>
                            </div>
                        </div>
                     

                     




                <div class="form-group">
                    <div class="col-md-5 col-md-offset-2">
                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage1" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image1" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="browseImage2" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image2" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="browseImage3" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div>
            <form class="well form-inline" id="upload_image3" action="<?php echo base_url(); ?>Vastu/upload_image"
                method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="free_Consultation" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading ">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){
        // Initialize CKEditor for all textareas with class ckeditor
        $('textarea.ckeditor').ckeditor({
            uiColor: '#9AB8F3'
        });
    });

    $('#upload_image1').ajaxForm({
        dataType: 'JSON',
        success: function(response) {
            if (response.status == 200) {
                $(".img-preview1").attr('src', APP_URL + 'uploads/' + response.filename);
                $('#browseImage1').modal('hide');
                $('#process_image').val(response.filename);
                $('#blog_image-error').css({
                    "display": "none"
                });
            } else {
                $('#browseImage1').modal('hide');
                alert(response.message);

            }
            $.unblockUI();
            return false;
        }
    });

  

</script>


<script>	
$(document).ready(function(){

	$('#add_page_form').validate({
		ignore: [],
        rules: {
            // answers: {
            //     required: true
            // },
            answers: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },
            question: {
                required: true
            },
            faqcategory_id: {
                required: true
            },
          
		
			
			
		},
		 messages: {
            answers: {
                required: "Answer is required"
            }, question: {
                required: "Question is required"
            }, faqcategory_id: {
                required: "Faqcategory is required"
            },
		
			
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
            var faq_id = $('#faq_id').val();
				var question = $('#question').val();
				var priority = $('#priority').val();
				var answers = CKEDITOR.instances['answers'].getData();
				var faq_category_name = $('#faq_category_name').find(":selected").val();
				var faq_category_id = $('#faq_category_name').find(":selected").attr("name");

			





			
		
		    		
            $.post(APP_URL + 'admin/content_setting/faq_update', {
                faq_id: faq_id,
					faq_category_id: faq_category_id,
					question: question,
					answers: answers,
					priority: priority,
					faq_category_name: faq_category_name,
                
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/content_setting/faqlist';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	
	
});
</script>

