<script src="<?php echo base_url(); ?>assets/js/blog.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<?php
  // print_r($public_page);
  
  
  
if($public_page){
    $heading = 'Edit Public Content';

   $page_id = $public_page['content_id'];
    $content_id = $public_page['content_id'];
	$linked_service = $public_page['linked_services'];
	  $page_heading = $public_page['page_heading'];
	   $page_url = $public_page['page_url'];
	    $url_specification = $public_page['url_specification'];
		 $page_content = $public_page['page_content'];
		  $page_title = $public_page['page_title'];
		    $page_meta = $public_page['page_meta'];
			  $page_keyword = $public_page['page_title'];
			  $page_image= $public_page['image'];
			  $linked_service = json_decode($linked_service);
		  
} else {
    $heading = 'Add Public Content';
	$page_id = 0;
	$content_id = 0;
	$linked_service = '';
	$page_heading = '';
	$page_url = '';
	$url_specification = '';
	$page_content = '';
	$page_title = '';
	$page_meta = '';
	$page_keyword = '';
	$page_image = '';
	
}
 // var_dump($linked_services);
?>


<div class="container-fluid main-content">
<div class="page-title">
        <h1><?php echo $heading ?></h1>
		<a href="<?php echo base_url();?>admin/content_setting/public_content" class="btn btn-default pull-right addAds">View Public Content</a>
		
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
				
                <div class="widget-content padded">
				<div id="headerMsg"></div>
				<div id="headerMsg"></div>
                    <form id="add_page_form" method="post" class="form-horizontal">
					
						<input type="hidden" id="page_id" name="page_id" value=<?php echo  $page_id; ?>>
					
                        <div class="form-group">
                            <label class="control-label col-md-2" for="page_heading">Page Heading<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="page_heading" name="page_heading" value="<?php echo $page_heading ; ?>" placeholder="Enter Page Heading">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="page_url">Page Url<span class="required">*</span></label>
                            <div class="col-md-5">
                               <input class="form-control" id="page_url" name="page_url" value="<?php echo $page_url;?>" placeholder="Enter Page Url">
                            </div>
                        </div>
						<div class="form-group" >
                            <label class="control-label col-md-2" for="url_specification">Url Specification</label>
                            <div class="col-md-5">
                               <select class="form-control"  id="url_specification" name="url_specification" value="" >
									<option value="">Select</option>
									<option value="city" <?php if($url_specification == 'city' ) echo 'selected';?>>city</option>									
															
								</select>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="page_content">Content<span class="required">*</span></label>
                            <div class="col-md-5">
                                <textarea class="form-control ckeditor page_Content" id="page_content" name="page_content" value="" placeholder="Enter Page Content" row="2"><?php echo $page_content;?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-2" for="page_image">Image<span class="required">*</span></label>
                            <div class="col-md-5">      
                                <a name="" class="blog_image_btn btn btn-default " data-toggle="modal" data-target="#browseImage">Upload Image</a>
                                <input class="form-control" id="blog_image" name="blog_image" value="<?php echo $page_image;?>" type="hidden">
								<?php if($page_image != ''){
									echo '<img class="img-preview" src="'.base_url().'uploads/'.$page_image.'" alt="" style="width: 100px;">';
								}else{
									echo '<img class="img-preview" src="" alt="" style="width: 100px;">';
								}?>
								
									
								
                            </div>
                        </div>
						<div class="form-group">
						     <label class="control-label col-md-2" for="page_heading">Linked Services</label>
                            <div class="col-md-5">
							      
							
							     <?php  
								   foreach($linked_services as $val){
										 $checked = '';  
										 if($linked_service){
											foreach($linked_service as $det){
											
											  if($val['service_id'] == $det->linked_services_id ){
												$checked = 'checked';
											  }	else {
											   //$checked = '';
											  }	
										}									  
										}
										
								  ?>
							      
                                 <input  class="form-check-input linked_services" type="checkbox" name="linked_services" id="linked_services"  value="<?php echo $val['service_id']; ?>"<?php echo $checked ?>>
								  <label><?php echo $val['services_name']; ?></label><br>
								 <?php
								   }
								 ?>
                            </div>
                            
                        </div>
						
                        <div class="form-group">
                            <label class="control-label col-md-2" for="page_title">Page Title<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="page_title" name="page_title" value="<?php echo $page_title;?>" placeholder="Enter Page Title">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="page_meta">Page Meta<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="page_meta" name="page_meta" value="<?php echo $page_meta;?>" placeholder="Enter Page Meta">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="control-label col-md-2" for="page_keyword">Page Keyword<span class="required">*</span></label>
                            <div class="col-md-5">
                                <input class="form-control" id="page_keyword" name="page_keyword" value="<?php echo $page_keyword;?>" placeholder="Enter Page Keyword">
                            </div>
                        </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-5 col-md-offset-2">
                                <input class="btn btn-lg btn-primary btn-block" type="submit" value="Submit">  
                            </div>                            
                        </div>                            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background-color: #f5f5f5;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Browse Image</h3>
            </div> 
            <form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>configure_access/upload_image" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div id="head1_msg"></div>
                    <input type="hidden" value="service" name="image_cat" class="image_cat">
                    <input type="file" id="myFile" name="myFile" size="20" multiple> 

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary save-img-loading">Save Picture</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>	
$(document).ready(function(){

	$('#add_page_form').validate({
		ignore: [],
        rules: {
            page_heading: {
                required: true,
            },
			page_url: {
                required: true,
            },
			
			page_content: {
                required:function(textarea) {
							  CKEDITOR.instances[textarea.id].updateElement(); // update textarea
							  var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags
							  return editorcontent.length === 0;
						  },
            },
			page_title: {
                required: true,
            },
			page_meta: {
                required: true,
            },
			page_keyword: {
                required: true,
            },
			
			
		},
		 messages: {
			page_heading: {
                required: "Page heading is required.",
            },
			page_url: {
                required: "Page url is required.",
            },
			
			page_content: {
                required: " Content is required.",
            },
			
			page_title: {
                required: "Page title is required.",
            },
			page_meta: {
                required: "Page meta is required.",
            },
			page_keyword: {
                required: "Page Keyword is required.",
            },
			
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
			var page_id = $('#page_id').val();
			var page_heading = $('#page_heading').val();
			var page_url = $('#page_url').val();
			var url_specification = $('#url_specification').val();
			var page_content = $('#page_content').val();
			var blog_image = $('#blog_image').val();
			var linked_services = $('#linked_services').val();
			var page_title = $('#page_title').val();
			var page_meta = $('#page_meta').val();
			var page_keyword = $('#page_keyword').val();
			
			var access_define = [];
			$('input[type="checkbox"]:checked').each(function(){
					//console.log($(this).parent().find('span').text());
					access_define.push({
						
						linked_services_id : $(this).val(),
						
					});
				});
		    		
            $.post(APP_URL + 'admin/content_setting/page_update', {
				page_id:page_id,
                page_heading: page_heading,
                page_url: page_url,
				url_specification: url_specification,
				page_content: page_content,
				public_content_image: blog_image,
				linked_services: linked_services,
				page_title: page_title,
				page_meta: page_meta,
				page_keyword: page_keyword,
				access_define: access_define,
                
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						//window.location.href = APP_URL+'admin/content_setting/country_view';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	
	
});
</script>