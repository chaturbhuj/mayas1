
<script src="<?php echo base_url(); ?>assets/js/blog.js"></script> 
<style>.blog_photo{   width: 60px;}</style>
<div class="container-fluid main-content">
<div class="page-title">
        <h1>List Of Public Content</h1>
		<a href="<?php echo base_url();?>admin/content_setting/page_add" class="btn btn-default pull-right addAds">Add Public Content</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>                
                <div class="widget-content padded">
                    <div id="err_edit_blog"></div>
                    <div id="edit_team_table">
                        <div id="headerMsg"></div>
                        <div id="table_view">             
                            <?php
                            if ($public_page == 0) {
                                echo 'No record found into database';
                            } else {
                                $content = '';
                                $content .= '<table class="table table-striped table-bordered table-hover" id="user_details">';
                                $content .= '<thead><tr><th class="text-center">S.No.</th><th class="text-center">Page Heading </th><th class="teerr_edit_blogxt-center">Page Url </th><th class="text-center">Url Specification</th><th class="text-center">Page Content</th><th class="text-center">Page Title</th><th class="text-center">Page Meta</th><th class="text-center">Page Keyword</th><th class="text-center">Image</th><th class="text-center">Action</th></tr></thead><tbody>';
                                $i = 1;
                                foreach ($public_page as $value) {
                                    $content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_heading'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_url'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['url_specification'] . '</td>';
									$content .= '<td class="text-center">' . $value['page_content'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_title'] . '</td>';
                                    $content .= '<td class="text-center">' . $value['page_meta'] . '</td>';
									$content .= '<td class="text-center">' . $value['page_keyword'] . '</td>';
                                    $content .= '<td class="text-center"><a class="editBrowseImageBtn" name=' . $value['content_id'] . ' data-toggle="modal" data-target="#editBrowseImage" href="#"><img class="blog_photo" src="' . base_url() . 'uploads/' . $value['image'] . '"></a></td>';
                                    $content .= '<td class="text-center"><a href="'.base_url().'admin/content_setting/page_add?id='.$value['content_id'].'" class="" name=' . $value['content_id'] . ' value=""><span class="label label-success">Edit</span></a>';
                                    $content .= '&nbsp;&nbsp;<a href="#" class="public_page_remove"  name=' . $value['content_id'] . ' value=""><span class="label label-danger">Remove</span></a></td></tr>';
                                    $i++;
                                }
                                $content .= '</tbody></table>';
                                echo $content;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$('document').ready(function(){

	
	$('body').on('click', '.public_page_remove', function () {		
		
        var page_id = $(this).attr('name');
		 
		  
        $(this).closest('tr').remove();
		$.post(APP_URL + 'admin/content_setting/public_page_remove', {
			page_id: page_id,
			
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {	
			
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
                    $("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
                        location.reload();


					});
				});
			} else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			$.unblockUI();
			
		}, 'json');
	});
});
</script>