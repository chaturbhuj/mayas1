
<style>
.error,.required{
	color:red;
}
</style>

			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
</style>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>City View</h1>
		<a style="float: right;" href="<?php echo base_url();?>admin/content_setting/city_add"  class="btn btn-default pull-right">Add New City </a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
				<div id="headerMsg"></div>
                <div class="table-responsive">
			<table id="blog_view_table" class="table table-striped table-bordered table-hover" >
			<thead>
			<tr>
				<th class="text-center">S. No.</th>
				<th class="text-center">City Name </th>
				<th class="text-center">State Name </th>
				<th class="text-center">Country Name </th>
				<th class="text-center"> Action </th>
			</tr>
			</thead>
			<tbody>
			<?php
			     $i = 1;
			    if($details == 0)
				{
					echo "No record found into database";
					
				} else {
					$content = '';
					foreach($details as $value){
						$content .= '<tr class="darker-on-hover"><td class="text-center">' . $i . '</td>';
						$content .= '<td class="text-center" >' . $value['city']. '</td>';
						$content .= '<td class="text-center" >' . $value['Sname']. '</td>';
						$content .= '<td class="text-center" >' . $value['Cname']. '</td>';
					    $content .= '<td class="text-center"><a href="'.base_url().'admin/content_setting/city_add?id='.$value['id'].'" class="edit_module"  name=' . $value['id'] . ' value=""><span class="label label-success">Edit</span></a>';
					    $content .= '&nbsp;&nbsp;<a href="#" class="city_remove"  name=' . $value['id'] . '  value =' . $value['id'] . ' ><span class="label label-danger">Remove</span></a></td></tr>';
					   $i++;
					}
					
					echo $content;
				}	
			?>
			
			</tbody>
			
			</table>
				</div>
            </div>
        </div>
    </div>
</div>

<script>
$('document').ready(function(){

	
	$('body').on('click', '.city_remove', function () {		
		
        var city_id = $(this).attr('value');
		 
		  
        $(this).closest('tr').remove();
		$.post(APP_URL + 'admin/content_setting/city_remove', {
			city_id: city_id,
			
		},
		function (response) {
			$("html, body").animate({scrollTop: 0}, "slow");
			$('#headerMsg').empty();
			if (response.status == 200) {	
			
				$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'></a></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
					location.reload();
				});
			} else if (response.status == 201) {
				$('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
					$('#headerMsg').empty();
				});
			}
			$.unblockUI();
			
		}, 'json');
	});
});
</script>


