

<style>
.error,.required{
	color:red;
}

.img-preview{
	width: 100px;
	margin-left: 20px;
}
</style>
<?php 

   
	if($details){
		$country_id = $details['id'];
		$country_name = $details['name'];
		
		
	}else{
		$country_id = 0;
		$country_name = '';
	}

?>

<div class="container-fluid main-content">
<div class="page-title">
        <h1>Add Country</h1>
		<a style="float: right;" href="<?php echo base_url();?>admin/content_setting/country_view"  class="btn btn-default pull-right"> View Country</a>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget-container fluid-height clearfix"><br/>
                <div class="col-lg-7 col-md-7" id="err_blog_form"></div>
                <div class="clearfix"></div>
                <form class="form-horizontal" id="country_form">
						<div id="headerMsg"></div>
						<input type="hidden" name="country_id" id="country_id" value="<?php echo $country_id;?>">
						
						<div class="form-group">
							<label class="col-lg-3 control-label"  for="country_name">Country Name <span class="required">*</span></label>
							<div class="col-lg-6">
								<input type="text"  id="country_name" name="country_name" placeholder=" Enter country name" class="form-control" value="<?php echo $country_name;?>"> 
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-lg-offset-3 col-lg-6">
								<button class="btn btn-sm btn-primary" type="submit">Submit</button>
								<?php if($country_id){?>
								&nbsp;&nbsp;<a href="<?php echo base_url();?>admin/settings/zone_view" class="btn btn-sm btn-warning">&nbsp;Back&nbsp;</a>
								<?php }?>
							</div>
						</div>
						<br><br>
				</form>
            </div>
        </div>
    </div>
</div>

<script>	
$(document).ready(function(){

	$('#country_form').validate({
		ignore: [],
        rules: {
            country_name: {
                required: true,
            },
			
		},
		 messages: {
			country_name: {
                required: "Country name is required.",
            },
			
			
		},
		errorPlacement: function(error, element) {
            if (element.hasClass('content')) {
					error.insertAfter(element.closest('div.form-group').find('.content-error'));
			}else  {
                error.insertAfter(element);
            }
		},
		submitHandler: function (form) {
			
			var country_id = $('#country_id').val();
			var country_name = $('#country_name').val();
			
			
            $.post(APP_URL + 'admin/Content_setting/country_update', {
                country_id: country_id,
                country_name: country_name,
                
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status ==200) {
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a href='"+APP_URL+"page/blog_view'></a></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.href = APP_URL+'admin/Content_setting/country_view';
					});
					
                }
                else if (response.status == 201) {
                    $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headerMsg").fadeTo(3000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
					});
                }
				
				//$('#blog_form').find('button[type="submit"]').prop('disabled',false);
				
			}, 'json');
		return false;
		},
	});
	
	

	
	
	
});
</script>	