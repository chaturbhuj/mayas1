<link href="<?php echo base_url(); ?>assets/css/cropper.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/cropper.min.js"></script>

<style>
.crop-avatar-image .avatar-img-preview {
	width:200px;
	display: block;
    border: 3px solid #fff;
    border-radius: 5px;
    box-shadow: 0 0 5px rgba(0,0,0,.15);
    cursor: pointer;
    overflow: hidden;
}
.crop-avatar-image .avtar_img_dives{
	display: block;
    border: 3px solid #fff;
    border-radius: 5px;
    box-shadow: 0 0 5px rgba(0,0,0,.15);
    cursor: pointer;
    overflow: hidden;
    width:100px;
    text-align:center;
    margin: 10px;
    display: inline-block;
}
.crop-avatar-image .avtar_img_dives img{
	width:100%;
}
.crop-avatar-image .avtar_img_dives.active{
	box-shadow: 0 0 5px #F44336;
    border: 2px solid red;
}
.crop-avatar-image .avtar_img_dives .avtar_img_remover{
	font-size: 16px;
    font-weight: 600;
    line-height: unset;
    border-radius: 50%;
    margin: 10px;
    padding: 0px 10px;
}
</style>

<!---------------------------- Modal for Browse Image-------------------------->
<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="avatar-form" action="<?php echo base_url('common/crop_upload_image'); ?>" enctype="multipart/form-data" method="post">
				<div class="modal-header">
					<h4 class="modal-title" id="avatar-modal-label">Upload Image</h4>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					
				</div>
				<div class="modal-body" style="position: relative;">
					<div id="loader"></div>
					<div class="avatar-body">
						
						<!-- Upload image and data -->
						<div class="avatar-upload">
							<input type="hidden" class="avatar-src" name="avatar_src">
							<input type="hidden" class="avatar-data" name="avatar_data">
							<label for="avatarInput">Upload Image</label>
							<input type="file" class="avatar-input" id="avatarInput" name="avatar_file">
						</div>

						<!-- Crop and preview -->
						<div class="row">
							<div class="col-md-2" style="min-height: 270px;"></div>
							<div class="col-md-8">
								<div class="avatar-wrapper"></div>
							</div>
							<div class="col-md-2">
								<div style="display:none;">
									<p>Image Preview</p>
									<div class="avatar-preview preview-lg"></div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-md-9 hide display_none">
								<div class="btn-group">
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="-90" title="Rotate -90 degrees">Rotate Left</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="-15">-15deg</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="-30">-30deg</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="-45">-45deg</button>
								</div>
								<div class="btn-group">
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="90" title="Rotate 90 degrees">Rotate Right</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="15">15deg</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="30">30deg</button>
									<button type="button" class="btn btn-primary" data-method="rotate" data-option="45">45deg</button>
								</div>
							</div>
						</div>
						<div class="row avatar-btns ">
							<div class="col-md-5"></div>
							<div class="col-md-2">
								<button type="submit" class="btn btn-primary btn-block avatar-save">Done</button>
							</div>
							<div class="col-md-5"></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script>  
$(document).ready(function(){

  	$('body').on('click','.avtar_img_remover',function(){
		if($(this).parent().hasClass('active')){
			console.log('has actvie');
			
			$(this).parent().parent().find('.avtar_img_dives').first().addClass('active');
			$(this).parent().remove();
		}else{
			console.log('no actvie');
			$(this).parent().remove();
		}
	});

	$('body').on('click','.avtar_img_dives img',function(){
		console.log('hello');
		console.log($(this).closest('.avatar-view'));
		console.log($(this).closest('.avatar-view').attr('avatarIsprimary'));
		if($(this).closest('.crop-avatar-image').find('.avatar-view').attr('avatarIsprimary')== 'yes'){
			console.log('hello1');
			if($(this).parent().hasClass('active')){
				$(this).parent().removeClass('active');
			}else{
				$(this).parent().parent().find('.avtar_img_dives').removeClass('active');
				$(this).parent().addClass('active');
				var image_name=$(this).parent().attr('addr');
				console.log(image_name);
				$(this).closest('.crop-avatar-image').find('input[type="hidden"]').val(image_name);
			}
		}
	});
});  
</script>