﻿   <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
</script>
<style>
.star_red{color:red;}
</style>
   
		<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>send mail page</h1>
            </div>
          
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>	<!-- Page Title -->
			
			
			<section style="padding:0px 0px;">
				<div class="container " style="margin-top:50px;margin-bottom:50px;">
					<div class="row">
					  <div id="headerMsg"></div>
					  <div class="col-md-3"></div>
						<div class="col-md-6 sm-mb-30 wow bounceInDown  " data-wow-delay="0.2s">
							
						
								<div class="text-center">
									<p class="contactform nobottommargin  text_brown_color" style="    font-size:20px; padding-bottom:15px; padding-top:15px;"><strong>Please Fill The Form</strong></p>
									<div class="divider divider-center divider-dark" style="margin-top:0px;" ></div>
								</div>
								
								<form  id="email_sender_form" class="add_cf_form nobottommargin " method="post">
										
										<div style="padding: 10px;">
										<div>
										    <label> SMTP<span class="star_red">*</span></label>
											<select id="email_sender" name="email_sender" class="form-control input-border-bottom">
			                                      <option value="" style="color:black;">Email sender </option>
			                                     <option   value="SendGrid">SendGrid </option>
			                                     <option   value="spark_post"> spark post</option>
			                                     <option   value="elastic"> Elastic</option>
			                                    
			                                     
		                                    </select>
										</div>
										<div>
										     <label>Subject<span class="star_red">*</span></label>
											<input type="text" id="email_subject" name="email_subject" placeholder="Subject" class="form-control input-border-bottom text_brown_color">
										</div>
										
										<div>
										      <label>Email<span class="star_red">*</span></label>
											<input type="email" id="email" name="email" placeholder="Email" class="form-control input-border-bottom text_brown_color">
										</div>
										<div>
										      <label>primary_key<span class="star_red">*</span></label>
											<input type="text" id="primary_key" name="primary_key" placeholder="Primary Key" class="form-control input-border-bottom text_brown_color">
										</div>
										
										
										
                                       
											<button class="btn  btn-success"    type="submit" id="submit" name="submit" >Submit</button>
																				
									
								</form>
								
					
							
						</div>
			             <div class="col-md-3"></div>

						
					</div>
				</div>
			</section>
							
					
					
			 <script>
	$('document').ready(function(){
		
	
					
		//-----------------------------------------------------------------------
    /* 
     * validation of add Contact
     */
	$('#email_sender_form').validate({
		ignore: [],
        rules: {
            email_sender: {
                required: true,
            },
			
			 email_subject: {
                required: true,
            }, 
			email: {
                required: true,
            },
			
			primary_key: {
                required: true,
            },
			
			
			 
			
         },
		 messages: {
			email_sender: {
                required: "SMTP is required.",
            },
			email_subject: {
                required: "Subject is required.",
            },
			email: {
                required: "Email is required.",
            },
			
			 blog_description: {
                required: "Message is required"
            }
			 primary_key: {
                required: "Message is required"
            }
			
		},
		
       
		submitHandler: function (form) {
            var email_sender = $("#email_sender").val();
            var email_subject = $("#email_subject").val();
            var primary_key = $("#primary_key").val();
            var email = $("#email").val();
			
			
			$.post(APP_URL + 'sendmail/send_through_mail', {
					email_subject: email_subject,
					email_sender: email_sender,
					email: email,
					primary_key: primary_key,
			},
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.reload();
					});
					
				}else{
					 $('#headerMsg').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
					 
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').remove();
						window.location.reload();
					});
				}
                
			}, 'json');
		return false;
		},
	});
	
	
	
	
	 
});
</script>
