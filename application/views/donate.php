
<?php 
	
	if($services_details){
		$service_id = $services_details[0]['service_id'];
		$services_name = $services_details[0]['services_name'];
		$services_type = $services_details[0]['services_type'];
		//$cost = $services_details[0]['cost'];
		$consultant_type = $services_details[0]['consultant_type'];
		//$number_of_questions = $services_details[0]['number_of_questions'];
		
		$services_main_description = $services_details[0]['services_main_description'];
		$services_description = $services_details[0]['services_description'];
		$services_image = $services_details[0]['services_image'];
		//$number_of_questions_2 = $services_details[0]['number_of_questions_2'];
		//$cost_2 = $services_details[0]['cost_2'];
		$premium_type = $services_details[0]['premium_type'];
		
		
	}else{
		$service_id = 0;
		$services_name = '';
		$services_type = '';
		//$cost = '';
		$consultant_type = '';
		//$number_of_questions = '';
		//$number_of_questions_2 = '';
		//$cost_2 = '';
		$premium_type = '';
		
	
		$services_main_description = '';
		$services_description = '';
		$services_image = '';
		
			
		}
		
	
?>


<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});
	
</script>
<script>
	$('document').ready(function(){
		var d = new Date().getTime();
		$('#TID').val(d);
	});
</script>
<script>
	$('document').ready(function(){
		var id = new Date().getTime();
		$('#order_id').val(id);
	});
</script>
<style>
.form-control{
	    height: 34px !important;
}
/* label.control-label{
	color: #fff !important;
	
}*/
.control_div{
	//color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 90px;
	 height: 40px;
 }
 .display_none{
	 display:none !important;
}
</style>

<div id="content">
<script src="<?php echo base_url();?>assets/js/configure/constant.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1> Bespoke Services Details</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
               
                    
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Services Details</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>


<!--.breadcrumb-wrapper-->
<div class="section-column-wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="left-sidebar">
                <div class="sidebar-menu margin-bottom-30">
                   <ul>
						<?php
							foreach ($services as $value) {
						?>
                       
                        <li><a rel="nofollow"  href="<?php echo base_url(); ?>premium-consultation/detail/<?php echo $value['services_url'] ?>"><?php echo $value['services_name']?></a></li>
								<?php
								}
							?>								
                    </ul>
                </div>
                <!--.sidebar-menu-->

            </div>
            <!--.left-sidebar-->
        </div>
        <div class ="col-md-9">
		
			<div class="col-md-12" >
				<h3 style="  text-align: center;font-size: 40px;color: #582e2e;padding-bottom: 20px;"><strong><?php echo $services_details[0]['services_name']; ?></strong></h3>
			</div>
			
			<!--make this image -->
		<!--make this image -->
			<div class="col-md-12 image_div"  style="display:none">
				<img class="img-preview" style="    height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($services_details[0]['services_image']);?>" alt="Mayas Astrology">
			</div>
			<!---<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				<img class="img-preview" src="<?php echo base_url()?>/uploads/cs.png">
			</div>--->
			<div class="col-md-12 image_div" >
				<!---<img class="img-preview" style="    height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($services_details[0]['service_detail_image']);?>" alt="Mayas Astrology">--->
				
			<?php echo ($services_details[0]['service_detail_image']);?>
			
			</div>
			<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				
			</div>
			<div class="col-md-12 image_div" >
				<img class="img-preview" style="    height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($services_details[0]['service_step_image']);?>" alt="Mayas Astrology">
			</div>
			<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				
			</div>
			
			<div class="col-md-12" style="display:none;">
				<span><strong>Consultant Type</strong></span>:
				<span><?php echo $services_details[0]['consultant_type']; ?></span>
			</div>
			
			<div class="col-md-12">
				<span><strong>Description</strong></span>:
				<span><?php echo $services_details[0]['services_main_description']; ?></span>
			</div>
			
			<div class="row">
				
				
					
						<!--<div class="checkbox">
							<label>
							  <input type="checkbox"> <strong>Minutes</strong> : <?php echo $services_details[0]['number_of_questions']; ?>
							</label>
							<div><strong>cost</strong> :  <?php echo'', $services_details[0]['cost']; ?></div>
						</div>
						<div class="checkbox">
							<label>
							  <input type="checkbox"> <strong>Minutes</strong> : <?php echo $services_details[0]['number_of_questions_2']; ?>
							</label>
							<div><strong>cost</strong> :  <?php echo'', $services_details[0]['cost_2']; ?></div>
						</div> -->
				
					
					<div class="col-md-3">
									
					</div>
				</div>
			</div>
		
        </div>
    </div>
</div>

<!--.section-column-wrapper-->	

<div class="container">

 <!-- modal for free telephonic consultancy -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="newFreeConsultantForm1" class="">
			
                        
	<div class="modal-header">
		<div id="err_consultation_div"></div>
		
		
		<div class="col-md-10 pull-right">
		
		<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
			
			<div class="form-group">
				<h5 style="font-size: 30px;    padding-top: 50px; padding-bottom: 30px;" class="consult"><span style="width: 30px; height: 30px; background-color: #678; padding: 1px 14px; border-radius: 50%; color: #fff;">1</span>   <strong>Call on +91-9829265640  & Know “Your Consulting Fee”</strong></h5>
			</div>
			<div class="form-group">
				<h5 style="font-size: 30px;    padding-top: 50px; padding-bottom: 30px;" class="consult"><span style="width: 30px; height: 30px; background-color: #678; padding: 1px 14px; border-radius: 50%; color: #fff;">2</span>   <strong>Select your prefered currency</strong></h5>
			</div>
			<div class="form-group" style="display:none">
				<label for="inputEmail3" class="col-sm-3 control-label">Currency<span class="required">*</span></label>
				<div class="col-sm-9">
				
				<select name="currency" id="currency" class="form-control "  >
					
					<option value="USD" name="USD" id="USD">USD</option>
					<option selected value="INR" name="INR" id="INR">INR</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label"></label>
				<div class="col-sm-9">
				
				
					<div class="radioBtnClass1">
									
						<div class="col-md-4"><input type="radio"  class="radioBtnClass"   name="radioBtnClass" value="paypal"> <strong>$ (USD)</strong></br><img class="img-preview" src="<?php echo base_url()?>/uploads/paypal.png" alt="Mayas Astrology">
							</div>
							<div class="col-md-2">	</div>
							<div class="col-md-4">
						<input type="radio" class="radioBtnClass"  checked name="radioBtnClass" value="other"><strong>₹(INR)</strong></br>  <img class="img-preview" src="<?php echo base_url()?>/uploads/card.jpg" alt="Mayas Astrology">
						
						</div>
						
					</div>
				</div>
			</div>
			<div class="form-group">
				<h5 style="font-size: 30px;    padding-top: 50px; padding-bottom: 30px;" class="consult"><span style="width: 30px; height: 30px; background-color: #678; padding: 1px 14px; border-radius: 50%; color: #fff;">3</span>   <strong>Enter “Your Consulting Fee” </strong></h5>
			</div>
			<div  class="form-group ">
				<label for="inputEmail3" class="col-sm-3 control-label"><span class="required">(Numerical digits only, No symbol /Alphabets)</span></label>
				<div class="col-sm-9">
					<input  type="text" class="form-control" id="amount" name="amount" placeholder="amount" value="">
				</div>
			</div>
						
			<div class="form-group">
				
				
					<h5 style="font-size: 30px;    padding-top: 50px; padding-bottom: 30px;" class="consult"><span style="width: 30px; height: 30px; background-color: #678; padding: 1px 14px; border-radius: 50%; color: #fff;">4</span>   <strong>Enter Your Birth Details</strong></h5>
			
			</div>
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			
			
			
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Confirm Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number">
				</div>
			</div>
			<div   class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">TID</label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="TID" name="TID" placeholder="TID" value="tid">
				</div>
			</div>
			
			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">order_id<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="order_id" name="order_id" placeholder="order_id" value="order_id">
				</div>
			</div>
			
		
			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">redirect_url<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="redirect_url" name="redirect_url" placeholder="redirect_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerdonate">
				</div>
			</div>
			<div  class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">cancel_url<span class="required">*</span></label>
				<div class="col-sm-6">
					<input type="text" class="form-control" id="cancel_url" name="cancel_url" placeholder="cancel_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerdonate">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" id="inputDateDate" name="inputDateDate" placeholder="Birth Date" class="form-control  date " value="">  
				</div>
				<div class="col-sm-3">
					<input type="text" id="inputTimeMin" name="inputTimeMin" placeholder="Birth Time" class="form-control time " value="">
					
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour format</label>
				</div>
		  </div>
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Questions & Concerns<span class="required">*</span></label>
				<div class="col-sm-9">
					<textarea rows="3" class="textarea-information-class" id="inputQuestion" name="inputQuestion" placeholder="Mention your Questions and Concerns, ( You may also provide Birth-Details of other concerning person.)"></textarea>
				</div>
			</div>
			
			
			<div class="form-center" style="text-align: center;">
				<button type="submit" class="submit-btn btn btn-primary" style="cursor: pointer !important;">Submit</button>	
			</div>
		</form>
       
        </div>

	</div>
	
</div>

</div>  
</div>
<!--hide from gate way-->
<div>

	

</div>
<div style="display:none;">
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 

<input type='text' name=encRequest id="encRequest" value="">
 <input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
<button  id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
</form>
<button id="rzp-button1">Pay</button>
</div>

<script>
$('document').ready(function(){
	var paypaldonatedetail = [];
	var currency ='';
	 $('body').on('click', '.radioBtnClass1', function () {
			console.log($("input:radio[name=radioBtnClass]:checked").val());
			
			var gateway =($("input:radio[name=radioBtnClass]:checked").val());
			if(gateway == 'paypal'){
				 currency ='USD';
			}else{
				currency ='INR';
			}
			
		});
		
			var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
			//import_cookie_into_li3();
		document.cookie = "allcart_orderss_details= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 document.cookie = "Instant_Consultation_order_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 	document.cookie = "paypal_donate_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
	 $('#newFreeConsultantForm').validate({
		 rules: {
			
			inputName: {
                required: true
            },
			amount: {
                required: true
            },
			
			inputDateDate: {
                required: true
            },
			inputTimeMin: {
                required: true
            },
			inputQuestion: {
                required: true
            },
			
			birthCountry: {
                required: true
            },
			birthState: {
                required: true
            },
			birthCity: {
                required: true
            },
		
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                required: true,
				number: true,
				minlength:10,
				maxlength:12
            },
			
		 },
		 messages: {
			
			 inputName: {
                required: "Full Name is required"
            },
			 inputDateDate: {
                required: "Birth Date is required"
            },
			 inputTimeMin: {
                required: "Birth Time is required"
            },
			 birthCountry: {
                required: "Birth Country Name is required"
            },
			 birthState: {
                required: "Birth State Name is required"
            },
			 birthCity: {
                required: "Birth City Name is required"
            },
			 amount: {
                required: "Amount is required"
            },
			  inputQuestion: {
                required: "Question is required"
            },
			 
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },
			
		},
		 submitHandler: function (form) {
	   var inputName = $('#inputName').val();
            
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            
            console.log(currency);
			
            var TID = $('#TID').val();
            var order_id = $('#order_id').val();
            var redirect_url = $('#redirect_url').val();
            var cancel_url = $('#cancel_url').val();
			var language = 'EN';
			var amount = $('#amount').val();
			
			var inputDateDate = $('#inputDateDate').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
			var birthCity = $('#birthCity').val();
		   var inputQuestion = $('#inputQuestion').val();
			var payment_via = $("input:radio[name=radioBtnClass]:checked").val();
			
		
           if(payment_via != 'paypal'){
           
						var billing_name = $('#inputName').val();
						var billing_address = 'mayasastrology';
						var billing_city = 'mayasastrology';
						var billing_state = 'mayasastrology';
						var billing_zip = '11111';
						var billing_country ='India';
						var billing_tel = $('#inputContact').val();
						var billing_email =$('#inputEmail').val();
						
						var delivery_name =  $('#inputName').val();
						var delivery_address = 'mayasastrology';
						var delivery_city = 'mayasastrology';
						var delivery_state = 'mayasastrology';
						var delivery_zip = '2121210';
						var delivery_country = 'India';
						var delivery_tel = $('#inputContact').val();
						var delivery_email = $('#inputEmail').val();
						var merchant_param1 = 'mayasastrology';
						var merchant_param2 = 'mayasastrology';
						var merchant_param3 = 'mayasastrology';
						var merchant_param4 = 'mayasastrology';
						var merchant_param5 = 'mayasastrology';
						var promo_code = '';
						
						
					   
						var customer_identifier = '22;';
						
					   
					   <!--coke make-->
					  
					  
					   
					   
				   var merchant_data= [];

						merchant_data.push({
							
								TID : TID,
								order_id: order_id,
								redirect_url: redirect_url,
								cancel_url: cancel_url,
								language: language,
								billing_name: billing_name,
								billing_address: billing_address,
								billing_city: billing_city,
								billing_state: billing_state,
								billing_zip: billing_zip,
								billing_country: billing_country,
								billing_tel: billing_tel,
								billing_email: billing_email,
								delivery_name: delivery_name,
								delivery_address: delivery_address,
								delivery_city: delivery_city,
								delivery_state: delivery_state,
								delivery_zip: delivery_zip,
								delivery_country: delivery_country,
								delivery_tel: delivery_tel,
								delivery_email: delivery_email,
								merchant_param1: merchant_param1,
								merchant_param2: merchant_param2,
								merchant_param3: merchant_param3,
								merchant_param4: merchant_param4,
								merchant_param5: merchant_param5,
								amount: amount,
								promo_code: promo_code,
								customer_identifier: customer_identifier,
								currency: currency,
								
						});	
						paypaldonatedetail.push({
								
								
								inputDateDate: inputDateDate,
								inputTimeMin: inputTimeMin,
								birthCountry: birthCountry,
								birthState: birthState,
								birthCity: birthCity,
								
							
								inputName: inputName,
							
								inputContact: inputContact,
								inputEmail: inputEmail,
								
								inputQuestion: inputQuestion,
								currency: currency,
								amount: amount,
							});
							
							
							
							document.cookie = "paypal_donate_detail= "+JSON.stringify(paypaldonatedetail)+"; "+expires+";domain=;path=/";
						
						
						
						if(pay_via_in_all_mayas == 'ccavenu'){
							$.post(APP_URL+'welcome/convertdataproductservce',
							{    
							 merchant_data : merchant_data,
									
							},function (response) {
								var data = response.data;
								console.log(data);
								
							$('#encRequest').val(data);

						 $('#finalPayentSubmitBtn').trigger('click');
							
							},'json');	
						}else{
							console.log(merchant_data);
							var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
							var options = {
								"key": razor_all_key,
								"amount": merchant_data[0]['amount']+'00', // 2000 paise = INR 20
								"name": "Maya Astrology",
								"description": "Maya Astrology Services",
								"image": "",
								"handler": function (response){
									//alert(response.razorpay_payment_id);
									console.log(response);
									$.post(APP_URL+'welcome/paytest', {
										amount: (paid_amount*100),
										payment_id: response.razorpay_payment_id,
										razor_key: razor_all_key,
										seceret_key: seceret_key,
										},
									   function (response2) {
										  merchant_data[0]['razorpay_payment_id'] = response.razorpay_payment_id ;
										localStorage.setItem("rozer_pay_details", JSON.stringify(merchant_data[0]));
										window.location.href = APP_URL+'welcome/razorResponseHandlerbesoke';
										   
										}, 'json');
									
								},
								"prefill": {
									"name": merchant_data[0]['billing_name'],
									"contact": merchant_data[0]['billing_tel'],
									"email": merchant_data[0]['billing_email'],
								},
								"notes": {
									"address": "Donate bespok"
								},
								"theme": {
									"color": "#F37254"
								}
							};
							var rzp1 = new Razorpay(options);

							document.getElementById('rzp-button1').onclick = function(e){
								rzp1.open();
								e.preventDefault();
							}
							$('#rzp-button1').trigger('click');
							
						}
		
		   }else{
			   
			  
			   
			   <!--_paypal cookee data->
							var d = new Date();
							d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
							var expires = "expires=" + d.toGMTString();		
							
						
									//var paypaldonatedetail=[];
								

							paypaldonatedetail.push({
								
								order_id: order_id,
								inputDateDate: inputDateDate,
								inputTimeMin: inputTimeMin,
								birthCountry: birthCountry,
								birthState: birthState,
								birthCity: birthCity,
								inputQuestion: inputQuestion,
								
							
								inputName: inputName,
							
								inputContact: inputContact,
								inputEmail: inputEmail,
								
								currency: currency,
								amount: amount,
							});
							
							
							
							document.cookie = "paypal_donate_detail= "+JSON.stringify(paypaldonatedetail)+"; "+expires+";domain=;path=/";
							console.log(document.cookie);
							window.location.href = APP_URL+'paypal/index?order_id='+order_id+'&total_amount='+amount+'&currency='+currency+'&page=donate';
							 
							   
							
			   
		   }
			 
			
		 },
		 
		 
		 
	 });
	 
	 
	 
	 
	
});
</script>

	