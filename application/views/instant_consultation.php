<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});
	
</script>
<script>
	$('document').ready(function(){
		var d = new Date().getTime();
		$('#TID').val(d);
	});
</script>
<script>
	$('document').ready(function(){
		var id = new Date().getTime();
		$('#order_id').val(id);
	});
</script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<style>
.form-control{
	    height: 34px !important;
}
/* label.control-label{
	color: #fff !important;
	
}*/
.control_div{
	//color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 900px;
	 height: 225px;
 }
 .display_none{
	 display:none !important;
}
</style>

<div id="content">
<div class="container">


 <!-- modal for free telephonic consultancy -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="newFreeConsultantForm1" class="">
			<div class="modal-header">
		
		
				<h3 style="text-align:center;padding-bottom: 50px;color: #7b6763;font-size: 30px;"><strong> Your Consultation</strong></h3>		
					<div class="row orders">
                            <div class="col-md-8">
                                <table class="table" id="shop_Cart_table">
                                    <thead>
                                        <tr>
											<th class="text-center" >Services</th>
                                            <th class="text-center"><?php echo $this->uri->segment(2); ?></th>
                                            <th class="text-center">Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody id="fghj">
										<tr>
											<td class="text-center">Instant Consultation </td>
											<td class="text-center"><?php echo $this->uri->segment(3); ?></td>
											<td class="text-center"><del id="hashno2"></del>&nbsp;<span id="hashno"></span></td>
										</tr>
									</tbody>
									
                                </table>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Have a Coupon Code?</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="inputcoupan" name="inputcoupan" placeholder="Enter Coupon Code">
										<div id="coupan_err"></div>
									</div>
									<a href="javascript:void(0);" title="Apply Coupon"><span style="font-size: 21px;" class="label col-sm-2 label-success appl_coupan_code">Apply</span></a>
								</div>
                            </div>
                            <div class="col-sm-4" style="text-align: center; border: 2px solid #bcbcd0;height:200px;background-color: aliceblue;">
								<!--<h4 style="color: #9ba588;padding-top: 10px;">Talk to Astrologer Mayankesh Now</h4>-->
								<h4 style="color: #9ba588;padding-top: 25px;">Customer support </h4>
								<h4 style="color: #9ba588;">( for online Payment Process) </h4>
								<h4 style="color: #9ba588;">+91-98292-65640 </h4>
								<h5 style="color: #9ba588;">10am to 8pm (Indian Time) </h5>
							
							</div>	
                        </div>
                        </div>
                        
	<div class="modal-header">
		<div id="err_consultation_div"></div>
		
		<h3 style="text-align:center;padding-bottom: 50px;font-size: 30px;color: #7b6763;f"><span style="width: 30px; height: 30px; background-color: #678; padding: 2px 12px; border-radius: 50%; color: #fff;">2</span> <strong>Enter Birth Information</strong></h3>
		<div hidden  class="freeConsultant-form" style="padding-top: 10px;">Fill form to ask one free Question</div>
		
		
		
		<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
			
			<div class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="questionyaminute" value="<?php echo $this->uri->segment(5); ?>" name="questionyaminute" placeholder="Enter your Full name">
				</div>
			</div>
			<div class="form-group hide">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="questiontime" value="<?php echo $this->uri->segment(3); ?>" name="questiontime" placeholder="Enter your Full name">
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Gender<span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="inputGender" id="inputGender" class="form-control">
					<option value="">Gender</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" id="inputDateDate" name="inputDateDate" placeholder="Birth Date" class="form-control  date " value="">  
				</div>
				<div class="col-sm-3">
					<input type="text" id="inputTimeMin" name="inputTimeMin" placeholder="Birth Time" class="form-control time " value="">
					
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour format</label>
				</div>
		  </div>
			
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div  class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Confirm Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number">
				</div>
			</div>
			<div   class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">TID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="TID" name="TID" placeholder="TID" value="tid">
									</div>
								</div>
								
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">order_id<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="order_id" name="order_id" placeholder="order_id" value="order_id">
									</div>
								</div>
								
							
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">redirect_url<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="redirect_url" name="redirect_url" placeholder="redirect_url" value="<?php echo base_url();?>welcome/ccavResponseHandlInstant_consultation">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">cancel_url<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="cancel_url" name="cancel_url" placeholder="cancel_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerservice">
									</div>
								</div>
								<div  class="form-group display_none">
									<label for="inputEmail3" class="col-sm-3 control-label">Amount<span class="required">*</span></label>
									<div class="col-sm-6">
										<input disabled type="text" class="form-control" id="amount" name="amount" discount="0" coupan="" placeholder="amount" value="">
									</div>
								</div>
								<div class="form-center" style="text-align:center;">
										<button type="submit" class="submit-btn btn btn-primary"  id="btnSubmit" style="cursor: pointer !important;">Submit</button>
								</div>
		</form>


	</div>
	
</div>

</div>  
</div>
<!--hide from gate way-->
<div>

	

</div>
<div style="display:none;">
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 

<input type='text' name=encRequest id="encRequest" value="">
 <input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
<button  id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
</form>
<button id="rzp-button1">Pay</button>
</div>

<script>
$('document').ready(function(){
	
		var convertBase = function () {

							function convertBase(baseFrom, baseTo) {
								return function (num) {
									return parseInt(num, baseFrom).toString(baseTo);

								};
							}

							// binary to decimal
							convertBase.bin2dec = convertBase(2, 10);

							// binary to hexadecimal
							convertBase.bin2hex = convertBase(2, 16);

							// decimal to binary
							convertBase.dec2bin = convertBase(10, 2);

							// decimal to hexadecimal
							convertBase.dec2hex = convertBase(10, 16);

							// hexadecimal to binary
							convertBase.hex2bin = convertBase(16, 2);

							// hexadecimal to decimal
							convertBase.hex2dec = convertBase(16, 10);

							return convertBase;
						}();
						var number ='<?php echo $this->uri->segment(4); ?>';
						var number2 ='<?php echo $this->uri->segment(5); ?>';
						
					var newhash =convertBase.hex2dec(number);
					
						if(number2){
							var newhash2 =convertBase.hex2dec(number2);
						}else{
							var newhash2 = 0 ;
						}
					
					console.log(newhash);
	
					
					
					if(newhash2 > 0){
						var total_cost = parseInt(newhash);
						$('#amount').val(newhash);
						$('#amount').attr('discount' ,0);
						$('#hashno2').text('₹ '+newhash2);
						$('#hashno').text('₹ '+newhash);
					}else{
						$('#amount').val(newhash);
						$('#hashno').text('₹ '+newhash);
						var total_cost = parseInt(newhash2);
					}
	
	
	var total_discount = 0;
	var coupan_for = 'service';
	
	$('body').on('click', '.appl_coupan_code', function () {
		
        var code = $('#inputcoupan').val();
		$('#coupan_err').empty();
		var my_this = this ;
		console.log(total_cost);
		console.log(coupan_for);
		if(code){
			
			$.post(APP_URL+'welcome/applied_coupan_check',{    
				 total_cost : total_cost,
				 coupan_for : coupan_for,
				 code : code,
				},function (response) {
					if(response.status ==200){
						var val = response.data;
						total_discount = parseInt(response.data) ;
						$('#inputcoupan').prop('disabled' ,true);
						$(my_this).html('<i class="fa fa-times-circle-o" aria-hidden="true"></i>');
						$(my_this).removeClass('appl_coupan_code');
						$(my_this).addClass('remove_coupan_code');
						$(my_this).parent().attr('title','Remove Coupon');
						$(my_this).css('background-color', 'rgb(209, 68, 68);');
						$('#coupan_err').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						if(total_discount > 0){
						var content2 = '<tr class="for_coupan">\n\
											<td class="text-center">Discount</td>\n\
											<td></td>\n\
											<td class="text-center"> -  ₹'+total_discount+'</td>\n\
										</tr>\n\
										<tr class="for_coupan">\n\
											<td class="text-center">Total Pay</span></td>\n\
											<td></td>\n\
											<td class="text-center"> ₹'+(total_cost - total_discount)+'</td>\n\
										</tr>';
						$('#fghj').append(content2);
						
						$('#amount').attr('discount',total_discount);
						$('#amount').attr('coupan',code);
						}
					}else{
						$('#coupan_err').html("<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
						$("#coupan_err").fadeTo(2000, 500).slideUp(500, function(){
							$('#coupan_err').empty();
						});
						
				},'json');	
		}
	}); 
	
	
	
	$('body').on('click', '.remove_coupan_code', function () {
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();	
		
		$('#inputcoupan').val('');
		$('#inputcoupan').prop('disabled',false);
		$(this).html('Apply');
		$(this).removeClass('remove_coupan_code');
		$(this).addClass('appl_coupan_code');
		$(this).parent().attr('title','Apply Coupon');
		$(this).css('background-color', '#5cb85c');
		$('#coupan_err').empty();
		total_discount = 0 ;
		$('.for_coupan').remove();
		if(newhash2 > 0){
			$('#amount').attr('discount' ,0);
			$('#amount').attr('coupan' ,'');
			$('#hashno2').text('₹ '+newhash2);
			$('#hashno').text('₹ '+newhash);
		}else{
			$('#amount').attr('discount' ,0);
			$('#amount').attr('coupan' ,'');
			$('#hashno').text('₹ '+newhash);
		}
	}); 
	
	
	
	
	 $('#newFreeConsultantForm').validate({
		 rules: {
			
			inputName: {
                required: true
            },
			inputGender: {
                required: true
            },
			inputDateDate: {
                required: true
            },
			inputTimeMin: {
                required: true
            },
			
			
			birthCountry: {
                required: true
            },
			birthState: {
                required: true
            },
			birthCity: {
                required: true
            },
			inputEmail: {
                required: true
            },
			inputEmailCrfm: {
                required: true,
				equalTo : "#inputEmail"
            },
			inputContact: {
                required: true,
				number: true,
				minlength:10,
				maxlength:12
            },
			
		 },
		 messages: {
			
			 inputName: {
                required: "Full Name is required"
            },
			 inputGender: {
                required: "Gender is required"
            },
			 inputDateDate: {
                required: "Birth Date is required"
            },
			 inputTimeMin: {
                required: "Birth Time is required"
            },
			 birthCountry: {
                required: "Birth Country Name is required"
            },
			 birthState: {
                required: "Birth State Name is required"
            },
			 birthCity: {
                required: "Birth City Name is required"
            },
			inputEmail: {
                required: "Email ID is required"
            },
			inputEmailCrfm: {
                required: "Confirm Email Id  is required",
				equalTo: "Confirm Email Id don't match."
            },
			inputContact: {
                required: "Contact Number is required.",
				number: "only Numbers are allowed",
				minlength:"Min. 10 digits are required",
				maxlength:"Max. 12 digits are required"
            },
			
		},
		 submitHandler: function (form) {
	 	var consultantType = 'Instant Consultation';//$('#consultantType').val();
			
           var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val();
            var inputTimeMin = $('#inputTimeMin').val();
			var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
			var birthCity = $('#birthCity').val();
			var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            var questionyaminute = $('#questionyaminute').val();
            var questiontime = $('#questiontime').val();
            var paymentmethod = 'paid';
            
            
			
            var TID = $('#TID').val();
            var order_id = $('#order_id').val();
            var redirect_url = $('#redirect_url').val();
            var cancel_url = $('#cancel_url').val();
			var language = 'EN';
			var amount = $('#amount').val();
			 var discount = $('#amount').attr('discount');
            var coupan = $('#amount').attr('coupan');
		
		
		
           
            var billing_name = $('#inputName').val();
            var billing_address = 'mayaastrology';
            var billing_city = 'mayaastrology';
            var billing_state = 'mayaastrology';
            var billing_zip = '11111';
            var billing_country ='India';
            var billing_tel = $('#inputContact').val();
            var billing_email =$('#inputEmail').val();
			
            var delivery_name =  $('#inputName').val();
            var delivery_address = 'mayaastrology';
            var delivery_city = 'mayaastrology';
            var delivery_state = 'mayaastrology';
            var delivery_zip = '2121210';
            var delivery_country = 'India';
            var delivery_tel = $('#inputContact').val();
            var delivery_email = $('#inputEmail').val();
            var merchant_param1 = 'Mayaastrology';
            var merchant_param2 = 'Mayaastrology';
            var merchant_param3 = 'Mayaastrology';
            var merchant_param4 = 'Mayaastrology';
            var merchant_param5 = 'Mayaastrology';
            var promo_code = '';
			
			
           
            var customer_identifier = '22;';
            var currency = 'INR';
           
		   
		   <!--coke make-->
		   
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		   document.cookie = "Instant_Consultation_order_detail= "+JSON.stringify()+"; "+expires+";domain=;path=/";
		   console.log(document.cookie);		
	var InstantConsultationorderdetail=[];
	
	
	
			
			
			

		InstantConsultationorderdetail.push({
			 consultantType: consultantType,
				
				inputName: inputName,
                inputGender: inputGender,
                inputDateDate: inputDateDate,
                inputTimeMin: inputTimeMin,
                birthCountry: birthCountry,
				birthState: birthState,
				birthCity: birthCity,
				inputEmail: inputEmail,
				inputContact: inputContact,
				
				questiontime: questiontime,
				questionyaminute: questionyaminute,
				amount: amount,
                
			
		});
		
		
		
		document.cookie = "Instant_Consultation_order_detail= "+JSON.stringify(InstantConsultationorderdetail)+"; "+expires+";domain=;path=/";
		console.log(document.cookie);
		//window.location.href = APP_URL+'welcome/finalcheckoutservice';
		 
		   
		   <!---cokee ens->
		   
		   
		   
		   
		   
		   
		   
		   
		   
		   
       var merchant_data= [];

			merchant_data.push({
				
					TID : TID,
					order_id: order_id,
					redirect_url: redirect_url,
					cancel_url: cancel_url,
					language: language,
					billing_name: billing_name,
					billing_address: billing_address,
					billing_city: billing_city,
					billing_state: billing_state,
					billing_zip: billing_zip,
					billing_country: billing_country,
					billing_tel: billing_tel,
					billing_email: billing_email,
					delivery_name: delivery_name,
					delivery_address: delivery_address,
					delivery_city: delivery_city,
					delivery_state: delivery_state,
					delivery_zip: delivery_zip,
					delivery_country: delivery_country,
					delivery_tel: delivery_tel,
					delivery_email: delivery_email,
					merchant_param1: merchant_param1,
					merchant_param2: merchant_param2,
					merchant_param3: merchant_param3,
					merchant_param4: merchant_param4,
					merchant_param5: merchant_param5,
					amount: amount,
					discount: discount,
					coupan: coupan,
					promo_code: promo_code,
					customer_identifier: customer_identifier,
					currency: currency,
					
			});	
			
				if(pay_via_in_all_mayas == 'ccavenu'){
					$.post(APP_URL+'welcome/convertdataproductservce',
					{    
					 merchant_data : merchant_data,
							
					},function (response) {
						var data = response.data;
						console.log(data);
						
					$('#encRequest').val(data);

				 $('#finalPayentSubmitBtn').trigger('click');
					
					},'json');	
				}else if(pay_via_in_all_mayas == 'Razor'){
					console.log(merchant_data);
					var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
					var paid_amount = parseFloat(merchant_data[0]['amount']) - parseFloat(merchant_data[0]['discount']) ;
					var options = {
						"key": razor_all_key,
						"amount":(paid_amount*100), // 2000 paise = INR 20
						"name": "Maya Astrology",
						"description": "Maya Astrology Instant Consultation",
						"image": "",
						"handler": function (response){
							//alert(response.razorpay_payment_id);
							console.log(response);
							$.post(APP_URL+'welcome/paytest', {
								amount: (paid_amount*100),
								payment_id: response.razorpay_payment_id,
								razor_key: razor_all_key,
								seceret_key: seceret_key,
								},
							   function (response2) {
								   merchant_data[0]['razorpay_payment_id'] = response.razorpay_payment_id ;
									localStorage.setItem("rozer_pay_details", JSON.stringify(merchant_data[0]));
									window.location.href = APP_URL+'welcome/razorResponseHandlInstant_consultation';
								   
								}, 'json');
							
						},
						"prefill": {
							"name": merchant_data[0]['billing_name'],
							"contact": merchant_data[0]['billing_tel'],
							"email": merchant_data[0]['billing_email'],
						},
						"notes": {
							"address": "Instant Consultation"
						},
						"theme": {
							"color": "#F37254"
						}
					};
					var rzp1 = new Razorpay(options);

					document.getElementById('rzp-button1').onclick = function(e){
						rzp1.open();
						e.preventDefault();
					}
					$('#rzp-button1').trigger('click');
					
				}else{
					//Paypal Payment
					var paid_amount = parseFloat(merchant_data[0]['amount']) - parseFloat(merchant_data[0]['discount']) ;
					localStorage.setItem("papal_pay_details", JSON.stringify(merchant_data[0]));
					window.location.href = APP_URL+'paypal/index?order_id='+merchant_data[0]['order_id']+'&total_amount='+paid_amount+'&currency='+merchant_data[0]['currency']+'&page=instant';
				}
		
		
			 
			
		 },
	 });
	
});
</script>

