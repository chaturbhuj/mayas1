<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<script>
	$('textarea.ckeditor').ckeditor({
		uiColor: '#9AB8F3'
	});
	$(function () {
        $(".date").datepicker({
			changeMonth:true,
			changeYear:true,
			dateFormat: 'yy-mm-dd',
			changeYear: true, yearRange : 'c-75:c+75',
		});
    });
</script>


<style>

.form-control{
	    height: 34px !important;
}
label.control-label{
	color: #fff !important;
}
.control_div{
	color: #fff !important;
}
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{
	width: 27%;
  height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important;}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 900px;
	 height: 225px;
 }
 .display_none{
	 display:none !important;
}
</style>
<script src="<?php echo base_url(); ?>assets/js/consultation.js"></script> 
<div id="content">
<div class="container">


 <!-- modal for free telephonic consultancy -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
<div id="newFreeConsultantForm1" class="">
	
	<div class="modal-header">
		<div id="err_consultation_div"></div>
		<div class="col-md-12 image_div">
			<img class="img-preview" src="<?php echo base_url()?>/uploads/<?php //echo $popup_ads[0]['free_consultation_image'];?>">
		</div>
		<div class="freeConsultant-form" style="padding-top: 10px;">Fill form to ask one free Question</div>
		<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Select type of consultation<span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="consultantType" id="consultantType" class="form-control consultantType" >
						<option value="" name="">Select the type of free consultation</option>
						<option value="Career" name="career">Question Related to Career</option>
						<option value="BirthStone" name="birth_stone">Question Related to Birth Stone</option>
						<option value="HoroscopeQuery"  name="horoscope">Question Related to Horoscope Query</option>
						<option value="MatchMaking"  name="match_making">Question Related to Match Making</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Gender<span class="required">*</span></label>
				<div class="col-sm-9">
					<select name="inputGender" id="inputGender" class="form-control">
					<option value="">Gender</option>
					<option value="Male">Male</option>
					<option value="Female">Female</option>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3" for="pro_expire_date">Date of Birth<span class="required">*</span></label>
				<div class="col-md-6">
					<input class="form-control date" id="pro_expire_date" name="pro_expire_date" placeholder="Date of Birth" type="text" value="<?php //echo $pro_expire_date;?>">
				</div>
			</div>
						<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Time Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<select name="inputTimeHr" id="inputTimeHr" class="form-control">
						<option value="">Hours</option>
						<option value="0">00</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>			
					</select>
				</div>
				<div class="col-sm-3">
					<select name="inputTimeMin" id="inputTimeMin" class="form-control">
						<option value="">Min</option>
						<option value="0">00</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
					</select>
				</div>
				<div class="col-sm-3">
					<label class="error1">Time : 24 hour formet</label>
					<select name="inputTimeSec" id="inputTimeSec" class="form-control display_none">
						<option value="">Seconds</option>
						<option value="0" selected>00</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
				</div>
			</div>
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
				<div class="col-sm-9">
					<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number (Optional)">
				</div>
			</div>
			<div class="form-group Career">
				<label for="inputEmail3" class="col-sm-3 control-label">Highest Qualification<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="educationQualification" name="educationQualification" placeholder="Enter your Highest Qualification">
				</div>
			</div>
			<div class="form-group Career">
				<label for="inputEmail3" class="col-sm-3 control-label">Current Profession<span class="required">*</span></label>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="currentProfession" name="currentProfession" placeholder="Enter Your Current Profession">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-md-3" for="pro_expire_date">Date of Birth<span class="required">*</span></label>
				<div class="col-md-6">
					<input class="form-control date" id="pro_expire_date" name="pro_expire_date" placeholder="Date of Birth" type="text" value="<?php //echo $pro_expire_date;?>">
				</div>
			</div>
			<div class="form-group ">
				<label for="inputEmail3" class="col-sm-3 control-label">Precise Question<span class="required">*</span></label>
				<div class="col-sm-9">
					<textarea rows="3" class="textarea-information-class" id="inputQuestion" name="inputQuestion" placeholder="Enter your one Precise Question for free consultation question"></textarea>
				</div>
			</div>
			<div class="form-group ">
				<label for="inputEmail3" class="col-sm-3 control-label">Other Infomation</label>
				<div class="col-sm-9">
					<textarea rows="3" class="textarea-information-class" id="otherInfomation" name="otherInfomation" placeholder="Enter usefull infomation Related to question"></textarea>
				</div>
			</div>
			<div class="form-center" style="text-align: center;">
				<button type="submit" class="submit-btn btn btn-primary" style="cursor: pointer !important;">Submit</button>	
			</div>
		</form>


	</div>
	<div class="modal-footer" style="display:none;">
		<a href="#" class="free-close-btn btn">Close</a>
	</div>
</div>

</div>  
</div>