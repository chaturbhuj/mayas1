
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>
<link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/jeditable/jquery.jeditable.js"></script>
<!-- Custom and plugin javascript -->
<script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>	
<script src="<?php echo base_url();?>assets/js/admin/blog_gallery.js"></script>	


			
<style>
#headerMsg{
	margin:20px 0px;
}
.dataTables-example th{
	text-align:center;
}
.display_none{
	display:none;
}
.img_pre{
	width:100px;
}
.br_color{
	background-color: #fff;
}
</style>
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Gallery
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
		<div class="wrapper wrapper-content animated fadeInRight" style="background-color: rgba(245, 237, 237, 0.14);">
		    <div class="box box-primary">
				<div class="box-header with-border">
					
					<button class="btn btn-default pull-right addNewItem" data-toggle="modal" data-target="#browseNewFAQ">Add New Image</button>
				</div>
				<div class="row">
				   <div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Add Image</h5>
								<div class="ibox-tools">
									<a rel="nofollow" class="collapse-link">
										<i class="fa fa-chevron-up"></i>
									</a>
									<a rel="nofollow" class="close-link">
										<i class="fa fa-times"></i>
									</a>
								</div>
							</div>
							
							<div class="ibox-content">
								<div id="headerMsg3" class="err_msg"></div>
								<form class="form-horizontal" id="add_image_form">
									
									<div id="headMsg"></div>
									
									<div class="form-group">
										<label class="control-label col-md-2" for="image">Image<span class="required">*</span></label>
										<div class="col-md-6">  
											<input id="image_btn" type="button" name="main_image" class="btn btn-success inline_block image_btn" value="Upload Image"  data-toggle="modal" data-target="#browseImage"/>
											<input type="hidden" name="image" id="image" class="form-control" value="">
											<div class="image-preview inline_block">
												<img src="" class="display_none" style="width:200px;" alt="Mayas Astrology">
											</div>
										</div>
									</div>	
									<div class="clearfix"></div>
									
									<div class="form-group">
										<div class="col-lg-offset-2 col-lg-6">
											<button class="btn btn-sm btn-primary" type="submit">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					
				</div>
				
				
			</div>
		</div>


		<div class="clearfix"></div>
		<div class="wrapper wrapper-content animated fadeInRight" style="background-color: rgba(245, 237, 237, 0.14);">
			<div class="row br_color">
				<div class="col-lg-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<h5>Images</h5>
						<div class="ibox-tools">
							<a rel="nofollow" class="collapse-link">
								<i class="fa fa-chevron-up"></i>
							</a>
							
							<a rel="nofollow" class="close-link">
								<i class="fa fa-times"></i>
							</a>
						</div>
					</div>
					<div class="ibox-content">
					</div>
					<div id="headerMsg"  class="err_msg"></div>
					
					<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
					<thead>
					<tr>
						<th>S. No.</th>
						<th>Image</th>
						<th>Image URL</th>
						
						<th>Date</th>
						<th>Action</th>
					</tr>
					</thead>
					<tbody>
					
					
					</tbody>
				
					</table>
						</div>

					</div>
				</div>
			</div>
			<br/>
			</div>
			
	

			<!---------------------------- Modal for Browse Image-------------------------->
			<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" style="background-color: #f5f5f5;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3>Browse Image 1</h3>
						</div> 
						<form class="well form-inline" id="upload_image" action="<?php echo base_url(); ?>admin/configure_access/upload_image" method="post" enctype="multipart/form-data">
							<div class="modal-body">
								<p>Image type should be GIF,JPG,PNG</p>
								<p>Image should be 2 MB or smaller</p>
								<div id="head1_msg"></div>
								<input type="hidden" value="image" name="image_cat" class="image_cat">
								<input type="hidden" value="" name="sub_folder_name" class="sub_folder_name">
								<input type="file" id="myFile" name="myFile" size="20" multiple> 
								<div class="progress progress_bar">
									<div class="bar progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
										<div class="percent">0%</div >
									</div>
								</div>	
							</div>
							
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary ">Save Picture</button>
							</div>
						</form>
					</div>
				</div>
			</div>



			<!---------------------------- Modal for Browse Module-------------------------->
			<div class="modal fade" id="browseEditImagedetails" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content" style="background-color: #f5f5f5;">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3>Update Image</h3>
						</div> 
						<div class="modal-body row">
							<div class="col-md-12">
								<form class="well" id="img_detail_form" method="post" enctype="multipart/form-data">
									<input class="form-control" id="img_id2" name="img_id2" value=0 type="hidden">
									<div class="form-group col-md-12" style="padding: 15px 0px 15px 0px">
										<label class="control-label col-md-3" for="img_name2">Image Name <span class="required">*</span></label>
										<div class="col-md-9">
											<input class="form-control" id="img_name2" name="img_name2" placeholder="Image Name" type="text">
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Save</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

	</section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

            /* Init DataTables */
            var oTable = $('#editable').DataTable();

            /* Apply the jEditable handlers to the table */
            oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );


        });

        function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "Custom row",
                "New row",
                "New row",
                "New row",
                "New row" ] );

        }
    </script>
	      