
<!-- .nav -->

<div id="x-corp-carousel" class="carousel slide hero-slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#x-corp-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#x-corp-carousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img src="assets/img/hero-slide-3.jpg" alt="alt="Mayas Astrology"">
            <!--Slide Image-->

            <div class="container">
                <div class="carousel-caption">
                    <small class="animated fadeIn">CALL US TODAY</small>
                    <div class="phone animated lightSpeedIn">1-800-1234-567</div>
                    <h1 class="animated lightSpeedIn">Title of Your Product <br>Service or Event</h1>

                    <p class="lead animated lightSpeedIn hidden-xs">It's a tag line, where you can write a key point of
                        your idea.
                        It is a long
                        established fact that a reader will be distracted.</p>

                    <a class="btn btn-default animated lightSpeedIn" href="#">Work With Us Today</a>
                </div>
                <!--.carousel-caption-->
            </div>
            <!--.container-->
        </div>
        <!--.item-->

        <div class="item">
		<?php
			if ($slider_list == 0) {
				echo 'No record found into database';
				} else {
					$i = 1;
					foreach ($slider_list as $value){
					
		?>
            <img src="<?php echo base_url();?><?php echo 'uploads/'.$value['slider_image'];?>" alt="Mayas Astrology">
            <!--Slide Image-->

            <div class="container">
                <div class="carousel-caption">

                    <h1 class="animated bounceIn"><?php echo $value['slider_info'];?></h1>
					<a class="btn btn-primary animated bounceIn" href="<?php echo base_url();?><?php $value['slider_navi'];?>">Work With Us Today</a>
                </div>
                <!--.carousel-caption-->
            </div>
            <?php
								$i++;
							}
					
					
					}
				?>
        </div>
        <!--.item-->
    </div>
    <!--.carousel-inner-->

    <!-- Controls -->
    <a class="left carousel-control" href="#x-corp-carousel" role="button" data-slide="prev">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#x-corp-carousel" role="button" data-slide="next">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- #x-corp-carousel-->


<!--call-to-action-wrapper-->

<section class="pad-top-80 pad-bottom-50 gray-bg">
    <div class="container">
        <div class="section-title text-center">
            <h2>Our Services</h2>
            <p>We provide expert tax and advisory services to individuals and small businesses.</p>
        </div>
        <!--.section-title-->

        <div class="row">
            <div class="col-md-4">
                <div class="thumbnail-variant-1 thumbnail red-top-border red-shadow">
                    
					<div style="padding: 20px; height: 120px;background-color: #009688 !important;">
						<h3 style="color:#fff;">Career Question <em><i class="fa fa-rupee" aria-hidden="true"></i>700</em></h3>
					</div>
                    <div style="position: absolute; top: 65px; left: 37%; margin-left: -45px;">
						<img src="assets/img/Career-Options.jpg" style="width: 200px; height: 200px; border: 3px solid #fff; border-radius: 50%;" alt="Mayas Astrology">
					</div>

                    <div class="caption" style="padding: 140px 30px 20px;">
                        <p>Objectively extend intermandated web-readiness whereas plug-and-play technologies. Credibly
                            harness highly efficient infomediaries after e-business.</p>
                        <p><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-chevron-right"                                                                                      aria-hidden="true"></i> </a></p>
                    </div>
                    <!--.caption-->
                </div>
                <!--.thumbnail-->
            </div>
            <!--.col-md-4-->
            <div class="col-md-4">
                <div class="thumbnail-variant-1 thumbnail green-top-border green-shadow">
                    <div style="padding: 20px; height: 120px;background-color: #009688 !important;">
						<h3 style="color:#fff;">Marriage Question <em><i class="fa fa-rupee" aria-hidden="true"></i>700</em></h3>
					</div>
                    <div style="position: absolute; top: 65px; left: 37%; margin-left: -45px;">
						<img src="assets/img/marriage-astrology-600x600.jpg" style="width: 200px; height: 200px; border: 3px solid #fff; border-radius: 50%;">
					</div>

                    <div class="caption" style="padding: 140px 30px 20px;">
                        

                        <p>Holisticly impact empowered convergence rather than collaborative action items.
                            Monotonectally benchmark exceptional results after.</p>

                        <p><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-chevron-right"
                                                                                       aria-hidden="true"></i> </a></p>
                    </div>
                    <!--.caption-->
                </div>
                <!--.thumbnail-->
            </div>
            <!--.col-md-4-->
            <div class="col-md-4">
                <div class="thumbnail-variant-1 thumbnail blue-top-border blue-shadow">
                    
					<div style="padding: 20px; height: 120px;background-color: #009688 !important;">
						<h3 style="color:#fff;">Birthstore query <em><i class="fa fa-rupee" aria-hidden="true"></i>700</em></h3>
					</div>
                    <div style="position: absolute; top: 65px; left: 37%; margin-left: -45px;">
						<img src="assets/img/584cbfbfa5d6fa0920ab1d62.jpg" style="width: 200px; height: 200px; border: 3px solid #fff; border-radius: 50%;" alt="Mayas Astrology">
					</div>

                    <div class="caption" style="padding: 140px 30px 20px;">
					    <p>Collaboratively iterate impactful scenarios whereas orthogonal applications. Proactively
                            benchmark competitive infomediaries rather than business.</p>

                        <p><a href="#" class="btn btn-link" role="button">Read More <i class="fa fa-chevron-right"
                                                                                       aria-hidden="true"></i> </a></p>
                    </div>
                    <!--.caption-->
                </div>
                <!--.thumbnail-->
            </div>
            <!--.col-md-4-->
        </div>
        <!--.row-->
    </div>
    <!-- .container -->
</section>
<!-- .section-wrapper -->

<section class="section-wrapper">
    <div class="container">
        <div class="section-title text-center">
            <h2>About Us</h2>
            <p>We are dedicated to providing professional services</p>
        </div>
        <!--.section-title-->
        <div class="row">
            <div class="col-md-6">
                <div class="block-wrapper">
                    <div class="column-block-title">
                        <h3>What we do</h3>
                    </div>
                    <!--.block-title-->

                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        Monitor the changing business situation.
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore.
                                </div>
                            </div>
                        </div>
                        <!--.panel-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Highlight the chronic and continuing gaps in the related data.
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore.
                                </div>
                            </div>
                        </div>
                        <!--.panel-->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Support and expand efforts to fill these gaps.
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                    richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                    brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                    aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                    Nihil anim keffiyeh helvetica, craft beer labore.
                                </div>
                            </div>
                        </div>
                        <!--.panel-->
                    </div>
                    <!--.panel-group-->

                </div>
                <!--.block-wrapper-->
            </div>
            <!--.col-md-6-->

            <div class="col-md-6">
                <div class="block-wrapper">
                    <div class="column-block-title">
                        <h3>Our Capabilities</h3>
                    </div>
                    <!--.block-title-->

                    <div class="media left-icon-style">
                        <div class="media-left media-middle">
                            <i class="fa fa-area-chart" aria-hidden="true"></i>
                        </div>
                        <!--.media-left-->
                        <div class="media-body media-middle">
                            <h3 class="media-heading">We teach you how to improve your business</h3>

                            <p>Lorem ipsum dolor sit consectetuer adipiscing elit nonummy ib uismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat.</p>
                        </div>
                        <!--.media-body-->
                    </div>
                    <!--.media-->

                    <div class="media left-icon-style">
                        <div class="media-left media-middle">
                            <i class="fa fa-cubes" aria-hidden="true"></i>
                        </div>
                        <!--.media-left-->
                        <div class="media-body media-middle">
                            <h3 class="media-heading">We make the technology affordable for you</h3>

                            <p>Lorem ipsum dolor sit consectetuer adipiscing elit nonummy ib uismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat.</p>
                        </div>
                        <!--.media-body-->
                    </div>
                    <!--.media-->

                    <div class="media left-icon-style">
                        <div class="media-left media-middle">
                            <i class="fa fa-diamond" aria-hidden="true"></i>
                        </div>
                        <!--.media-left-->
                        <div class="media-body media-middle">
                            <h3 class="media-heading">Seamlessly grow wireless human capital before turnkey.</h3>

                            <p>Lorem ipsum dolor sit consectetuer adipiscing elit nonummy ib uismod tincidunt ut laoreet
                                dolore magna aliquam erat volutpat.</p>
                        </div>
                        <!--.media-body-->
                    </div>
                    <!--.media-->
                </div>
            </div>
            <!--.col-md-6-->
        </div>
        <!--.row-->
    </div>
</section>
<!-- .section-wrapper -->

<section class="section-statistics">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="statistics-group">
                    <div class="statistics-item">
                        <div class="statistics-number">
                            590
                        </div>
                        <div class="statistics-title">
                            HAPPY<br>CUSTOMERS
                        </div>
                    </div>
                    <!--.statistics-item-->
                    <div class="statistics-item">
                        <div class="statistics-number">
                            830
                        </div>
                        <div class="statistics-title">
                            BIRTH<br>STORE
                        </div>
                    </div>
                    <!--.statistics-item-->
                    <div class="statistics-item">
                        <div class="statistics-number">
                            76
                        </div>
                        <div class="statistics-title">
                            GEMSTONE<br>JEWELLERY
                        </div>
                    </div>
                    <!--.statistics-item-->
                </div>
                <!--.statistics-group-->
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.section-statistics-->

<section class="section-wrapper gray-bg">
    <div class="container">

        <div class="section-title text-center">
            <h2>Satisfied Customer</h2>
            <p>Energistically promote cooperative quality vectors and tactical.</p>
        </div>
        <!--.section-title-->

        <div class="row">
			
			<div class="col-md-12">
			
                <div class="testimonial owl-carousel owl-theme">
				<?php
			//var_dump($testimonials_list);
			if ($testimonials_list == 0) {
				echo 'No record found into database';
			} else {
					$i = 1;
					foreach ($testimonials_list as $value ) {
				
			?>
                    <div class="item">
                        <div class="content">
                            <?php echo $value['comments']?>
                        </div>
                        <div class="author">
                            <div class="img-author">
                                <img src="<?php echo base_url()?>uploads/<?php echo $value['testimonials_image'];?>" alt="Mayas Astrology"/>
                            </div>
                            <div class="author-meta">
                                <h4><?php echo $value['testimonials_name']?></h4>
                                <span><?php echo $value['position']?></span>
                            </div>
                        </div>
                    </div>
					<?php
								$i++;
										
							}
						}
					?>
						
                </div>
               
            </div>
            <!--.col-md-12-->
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!-- .section-wrapper -->
