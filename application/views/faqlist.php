

<style>
 
.img-container img {
    max-width: 100%;
    height: 70%;
}


/* .jumbotron {
    background-image: url('your-image-url.jpg');
    background-size: cover;
    color: #fff;
    text-align: center;
    padding: 100px 0;
} */

.card {
    margin-bottom: 10px;
}





/* .jumbotronbanner {
    background-image: url('your-image-url.jpg');
    background-size: cover;
    color: #fff;
    text-align: center;
} */

.card_bod {}

.card-header {
    padding: 0;
}




.card-body.card_bod {
    padding: 0.8rem 0.4rem 0.2rem 2rem !important;
}

.btn-link {
    text-align: left;
    width: 100%;
}

/* Adjusting the margin between items */
#accordion .card {
    margin-bottom: 10px;
}





.card {
    flex: 1 1 30%;
    /* Adjust the width of the card as needed */
    margin: 10px;
}

.card .card-body {
    height: 100%;
}

.card {
    display: flex;
    flex-direction: column;
    height: 100%;
}

.card .card-body {
    flex: 1;
}

</style>

<?php $faq_data = array();  ?>


<?php 
if (isset($category) || $category == 0) {
    $CI =& get_instance();
    $CI->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

    $cache_key = 'faq_data_' . $category;
    if (!$faq_data = $CI->cache->get($cache_key)) {
        $sql = "SELECT * FROM admin_faq WHERE faq_category_id = ? ORDER BY priority DESC";
        $result = $this->db->query($sql, array($category));
        
        if ($result) {
            $faq_data = $result->result_array();

            $CI->cache->save($cache_key, $faq_data, 8640000); // Cache for 5 minutes
        }
    }else{


    }

    if (count($faq_data) > 0) {
        // Display FAQ data
        ?>
        <div style="background-color: text-light;padding-top: 20px;padding-bottom:20px">
            <div class="container mt-4">
                <h3 class="heading text-center my-4 text-bold">Frequently Asked Question</h3>
                <div id="accordion">
                    <?php 
                    $i = 1;
                    foreach ($faq_data as $value) { ?>
                        <div class="card">
                            <div class="card-header" id="heading<?php echo $i?>">
                                <h5 class="mb-0">
                                    <button class="btn btn-link text-dark text-decoration-none" data-toggle="collapse" data-target="#collapse<?php echo $i?>" aria-expanded="true" aria-controls="collapse<?php echo $i?>">
                                        <strong> <?php echo 'Q'.$i.'.'.'&nbsp;&nbsp;'. $value['question']; ?></strong>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse<?php echo $i?>" class="collapse <?php echo ($i == 1) ? 'show' : ''; ?>" aria-labelledby="heading<?php echo $i?>" data-parent="#accordion">
                                <div class="card-body card_bod">
                                    <?php echo $value['answers']; ?>
                                </div>
                            </div>
                        </div>
                        <?php 
                        $i++;
                    } ?>
                </div>
            </div>
        </div>
        <?php 
    }
} 
?>
