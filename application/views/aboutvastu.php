

<style>


.green-box {
    background-color: #00bf82;
    /* Green color */
    border-radius: 20px;
    /* Rounded corners */
    padding: 20px;
    /* Add some padding */
    margin-top: 50px;
    /* Add margin for spacing */
}

.content {
    margin-top: 20px;
    /* Add margin between heading and content */
}
</style>
<?php $about_data = array();  ?>

<?php if ($about_id || $about_id ==0) {
    // var_dump($about_id);

// Execute SQL query
$sql = "SELECT * FROM vastu_about_content WHERE id = $about_id";
$result = $this->db->query($sql);

// Fetch result into an array
$about_data = $result->result_array();
// var_dump($about_data);

# code...
}?>

 


<?php if (count($about_data) > 0) {?>

<div style=" ">
    <div class="container">
        <div class="green-box" style="padding: 4rem; margin:1rem 0 1rem 0;">
            <h1 class="text-center text-light text-bold"><?php echo $about_data[0]['about_title']; ?></h1>
            <div class="content text-center text-light">
                <p><?php echo $about_data[0]['about_content']; ?></p>

                <?php if ($about_data[0]['button_text']) {?>
     <a href="<?php echo base_url() . $about_data[0]['button_url']; ?>" class="btn btn-danger btn-lg text-light"><?php echo $about_data[0]['button_text']; ?></a>

   
  <?php } ?>
                
            </div>
        </div>
    </div>

</div>

<?php } ?>