<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Blog</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
                
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper d-none">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Blog</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container--> 
</section>
<!--.breadcrumb-wrapper-->

<div class="section-column-wrapper margin-top-30 margin-bottom-30" style="padding-bottom: 200px;">
    <div class="container">
   		<div class="row">
   			<div class="col-xs-12 col-md-12">
			   <div class="row">
					<?php
					if ($blogs == 0) {
					echo 'No record found into database';
					} else {
					$i = 1;
						foreach ($blogs as $value) {
						//var_dump($value);
					?>
						<div class="col-md-12" style="margin-top: 80px;">	
							<div class="row">
							<?php 	if($i%2==0){ ?>
								<div class="col-md-6">
									<a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>"><img src="<?php echo base_url();?>uploads/<?php echo $value['blog_image'];?>" class="img-responsive w-100 " alt="Mayas Astrology"></a>
								</div>
							<?php } ?>
							<div class="col-md-6" style="display: flex;flex-direction: column;justify-content: center;align-items:center;">
								<div class="caption">
									<h3><a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>"><?php echo $value['blog_heading']?></a></h3>
									<h5><a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_sub_heading'])) ?>"><?php echo $value['blog_sub_heading']?></a></h5>
								</div>
								<div class="pull-right" style="padding-right:20px;">
									<span class="the-time" style="padding-right:10px;"><a href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>"><?php echo $value['date']?></a></span>
									<a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>"><span class="the-author"> By: <?php echo $value['blog_writer']?></span></a>
								</div>
								<div class="clearfix"></div>		
								<div><a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>" class="btn btn-primary" style="float:left; padding:5px 20px; margin-top:10px;border-radius:5px;">Click Here</a></div>
							</div>
								<?php 	if($i%2 !==0){ ?>
									<div class="col-md-6">
										<a rel="nofollow" href="<?php echo base_url(); ?>blog/detail/<?php echo $value['blog_id'] ?>/<?php echo strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$value['blog_heading'])) ?>"><img src="<?php echo base_url();?>uploads/<?php echo $value['blog_image'];?>" class="img-responsive w-100 " alt="Mayas Astrology"></a>
									</div>
								<?php } ?>
							</div>
						</div>	
						<?php
						$i++;
						}
						
					}
					?>
				</div>
			</div>
 		</div>
    </div>
</div>
				
								
								
									