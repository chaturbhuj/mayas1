

<?php $banner_data = array(); ?>
<?php if ($banner_id && $banner_id > 0) {
    // Execute SQL query
    $sql = "SELECT * FROM mayas_banner WHERE banner_id = $banner_id";
    $result = $this->db->query($sql);

    // Fetch result into an array
    $banner_data = $result->result_array();
} ?>

<?php if (count($banner_data) > 0) { ?>
<div class="jumbotronbanner d-flex justify-content-center align-items-center" style="color: rgb(255, 255, 255); text-align: center; min-height: 60vh; font-family: Poppins-Regular; position: relative; overflow: hidden;">
    <img class="imgbanner" src="<?php echo base_url() . 'uploads/' . $banner_data[0]['banner_image']; ?>" alt="Banner Image" style="width: 100%; height: 100%; position: absolute;  top: 0; left: 0; z-index: -1;">
    <div class="">
        <h1 class="display-4 text-light"><?php echo $banner_data[0]['banner_title']; ?></h1>
        <p class="lead text-light"><?php echo $banner_data[0]['banner_content']; ?></p>
        <?php if ($banner_data[0]['button_text']) {
            echo '<a class="btn-light btn" href="' . base_url() . '/' . $banner_data[0]['button_url'] . '" role="button">' . $banner_data[0]['button_text'] . '</a>';
        } ?>
    </div>
</div>
<?php } ?>
