<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>Blog</h1>
            </div>
            </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<section class="breadcrumb-wrapper d-none">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
            <li><a rel="nofollow" href="<?php echo base_url();?>welcome/blog">Blog</a></li>
            <li class="active"><?php echo $blogfull[0]['blog_heading']; ?></li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->
<div id="dcacl" class="section-column-wrapper margin-top-30 margin-bottom-30"  oncopy="return false" oncut="return false" onpaste="return false">
	<div class="container pt-5" style="padding-bottom: 100px;"> 
       <div class="row">
	  
			<div class="col-xs-12 col-md-12">
			<div id="target"></div>
			    <?php
		            foreach($blogfull as $value)
		           {
	            ?>
				 <input type="hidden" name="blog_id" id="blog_id" value="<?php echo $value['blog_id'];?>">
				<article class="blog-post-wrapper single-article">
					<header class="entry-header">
						
						 <h2 class="entry-title"><a rel="nofollow" href=""><?php echo $value['blog_heading']; ?></a></h2>
						 <span class="the-author"> By: <span>Mayankesh Sharma</span></span>
                    </header><!-- /.entry-header -->
					<div class="post-thumbnail">
					   <div class="item">
						   <img src="<?php echo base_url().'uploads/'.$value['blog_image'];?>" class="img-responsive w-100 " alt="Mayas Astrology">
					   </div>
			        </div><!-- /.post-thumbnail -->
						<div class="entry-content text-center">
					      <p><?php echo $value['blog_description']; ?></p>
				        </div><!-- /.entry-content -->
				</article>
				<?php
		}
		?>
			<!-- 
		<div id="blog<?php echo $value['blog_id'];?>"></div>
			<section class="comments-wrapper">
                       <h2>Comments</h2>
				    <?php $xx = 11;
										//var_dump($blog_comment);
										if($blog_comment && $xx == 10){
											foreach($blog_comment as $comment){
												
											?>
					<div class="col-md-12" style="border:1px solid #f0f0f0; padding:10px; margin-top:15px;">	
						
				    <div class="media-body">
                        <span class="comment-author"><i class="fa fa-user"></i><?php echo $comment['name'];?>&nbsp;&nbsp;</span>
                        <span class="comment-date"><i class="fa fa-calendar"></i> <?php echo date('d F Y',strtotime($comment['date']));?></span>
                        <div class="comment-body"><?php echo $comment['comment'];?></div>
                        
                    </div><!-- /.media-body -->
						<!--</div>
											<?php
											}
										}
									?>	
									
					 <h2 style="margin-top:126px;margin-bottom:31px;">Add Comment</h2>
            <hr>
			<div id="headerMsg"></div>
            <form method="post" name="comments-form" id="comments-form">
			          
						<div class="form-group display_none">
						<input type="text" placeholder="Your name and surname" class="form-control" title="comments-form-name" name="name" id="name" value="">
						</div>
						<div class="form-group display_none">
							<input type="text" placeholder="Your email adress" class="form-control" title="comments-form-email" name="email" id="email" value="">
						</div>
						<div class="form-group">
							<textarea placeholder="Your message" class="form-control" title="comments-form-comments" name="comment" rows="6" id="comment"></textarea>
					    </div>
											
						<div class="form-group">
												
							<button type="submit" class="btn btn-theme btn-theme-transparent btn-icon-left" id="submit"><i class="fa fa-comment"></i> Submit Message</button>
																								
						</div>
			</form>		
			</section>--->
			</div>
		</div>
	</div>

</div>




<!-- PAGE -->
	 

<script>

$(document).ready(function() {
    $("body").on("contextmenu",function(){
       return false;
    }); 
	$('body').bind('copy paste',function(e) {
		e.preventDefault(); return false; 
	});
	$("#dcacl").prop('disabled',true);
}); 

$('document').ready(function(){
	
	
	
	var blog_id = $('#blog_id').val();
	
		$(window).scroll(function() {
		
		if($(window).scrollTop() + $(window).height() > $(document).height() - 500) {
	
	$.post(APP_URL + 'welcome/scroll_blog_full_data', {
                blog_id: blog_id,
                
            },
			function (response) {
				var content = '';
				if (response.status == 200) {
					
                    var data = response.data;
					
					
					  


				content += '<article class="blog-post-wrapper single-article">';
									content += '<header class="entry-header">';
										content += '<div class="entry-meta">';
											content += '<ul class="list-inline">';
												content += '<li><span class="the-time"><a rel="nofollow" href="#">'+data[0]['date']+'</a></span></li>';
												content += '<li><span class="the-author"> By: <a rel="nofollow" class="url" href="#">Admin</a></span></li>';
												
										  content += '</ul>';
										content += '</div><!-- /.entry-meta -->';
										content += ' <h2 class="entry-title"><a rel="nofollow" href="">'+data[0]['blog_heading']+'</a></h2>';
									content += '</header>';
									content += '<div class="post-thumbnail">';
									  content += ' <div class="item">';
										content += '   <img src="<?php echo base_url() ?>uploads/'+data[0]['blog_image']+'" class="img-responsive " alt="Mayas Astrology">';
									content += '   </div>';
									content += '</div>';
										content += '<div class="entry-content">';
										content += '  <p>'+data[0]['blog_description']+'</p>';
										content += '</div>';
				content += '</article>';					  
				content += '<div id="blog'+data[0]['blog_id']+'"></div>';
	

				var	ii ='#blog'+blog_id;
				$(ii).append(content);
				blog_id =data[0]['blog_id'] ;
				}
                else if (response.status == 201) {
					
				}
				
			}, 'json');
	
	
	}
});
	
	

	
	
	
	
	//-----------------------------------------------------------------------
    /* 
     * validation of comments-form
     */
	$('#comments-form').validate({
		ignore: [],
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            comment: {
                required: true
            },
         },
		 messages: {
			name: {
                required: "Name is required"
            },
            email: {
                required: "Email ID is required",
            },
            comment: {
                required: "Comment is required",
            },
        },
		submitHandler: function (form) {
			var blog_id = $('#blog_id').val();
            var  name = $('#name').val();
			var email = $('#email').val();
            var comment = $('#comment').val();
            
			$.post(APP_URL + 'welcome/leave_blog_comment', {
                blog_id: blog_id,
                name:name,
                email: email,
                comment: comment,
            },
			function (response) {
				$("html, body").animate({scrollTop: 0}, "slow");
                $('#headerMsg').empty();
				if (response.status == 200) {
					
                    var message = response.message;
					
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
					
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						location.reload();
					});
					
					//var url = APP_URL+'welcome/blogfull';
					//window.location.href= url;
					
									
				
				}
                else if (response.status == 201) {
					$('#headerMsg').html("<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;<a onclick='location.reload();' href='javascript:void(0);'>Refresh!</a></div>");
					
					$("#headerMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#headerMsg').empty();
						//window.location.href = APP_URL+'admin/configure_access/faq';
					});
					
				}
				$('#comment').val('');
				
			}, 'json');
		return false;
		},
	});
	
});
</script>
            