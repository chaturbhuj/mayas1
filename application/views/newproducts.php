<!-- .nav -->

<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1>Products</h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section> 
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
            <li class="active">Products</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->

<section class="pad-top-80 pad-bottom-50 gray-bg">
    <div class="container">
        <div class="section-title text-center">
            <h2>Premium Products</h2>
            <p>In-depth Astrology Consultation & Counseling .</p>
        </div>
        <!--.section-title-->

        <div class="row" id="productval">
            
        </div>
        <!--.row-->
    </div>
    <!-- .container -->
</section>
<script> 
	$('document').ready(function () {
		get_product_data();
		function get_product_data(){
            $.post(APP_URL + 'welcome/get_product_data', {
                
            },function (response) {
             $('#productval').html(response.data);
			
                
            }, 'json');
		}
	});
</script>


