<style>
.img-container img {
    max-width: 100%;
    height: 70%;
}


/* .jumbotron {
    background-image: url('your-image-url.jpg');
    background-size: cover;
    color: #fff;
    text-align: center;
    padding: 100px 0;
} */

.card {
    margin-bottom: 10px;
}

.card-img-top {
    max-height: 200px;
    object-fit: cover;
}

.read-more-line {
    border-bottom: 1px solid #dee2e6;
    text-align: center;
    margin-bottom: 30px;
}

/* .jumbotronbanner {
    background-image: url('your-image-url.jpg');
    background-size: cover;
    color: #fff;
    text-align: center;
} */

.card_bod {}

.card-header {
    padding: 0;
}




.card-body.card_bod {
    padding: 0.8rem 0.4rem 0.2rem 2rem !important;
}

.btn-link {
    text-align: left;
    width: 100%;
}

/* Adjusting the margin between items */
#accordion .card {
    margin-bottom: 10px;
}


.image-container {
    width: 59px;
    height: 59px;
    /* Adjust height of the image container */
    overflow: hidden;
    border-radius: 50%;
    /* To make it circular */
    margin: 0 auto;
    /* Center the image */
}

.image-container img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    /* Ensure the image covers the container */
}

.card-title {
    margin-top: 10px;
    /* Adjust spacing */
}



.card-deck {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
}

.card {
    flex: 1 1 30%;
    /* Adjust the width of the card as needed */
    margin: 10px;
}

.card .card-body {
    height: 100%;
}

.card {
    display: flex;
    flex-direction: column;
    height: 100%;
}

.card .card-body {
    flex: 1;
}

.green-box {
    background-color: #00bf82;
    /* Green color */
    border-radius: 20px;
    /* Rounded corners */
    padding: 20px;
    /* Add some padding */
    margin-top: 50px;
    /* Add margin for spacing */
}

.content {
    margin-top: 20px;
    /* Add margin between heading and content */
}
</style>

<?php
if ($vastu_data) {
    $image_url = $vastu_data[0]['banner_image'];
    $banner_title = $vastu_data[0]['banner_title'];
    $banner_content = $vastu_data[0]['banner_content'];
    $button_text = $vastu_data[0]['button_text'];
    $button_url = $vastu_data[0]['button_url'];
    $button_url = $vastu_data[0]['button_url'];
    $section1position = $vastu_data[0]['section1position'];
    $about_id = $vastu_data[0]['about_content_id'];
    $banner_id = $vastu_data[0]['banner_id'];
    $section_title1 = $vastu_data[0]['section_title1'];
    $section_image1 = $vastu_data[0]['section_image1'];
    $section_content1 = $vastu_data[0]['section_content1'];
    $section2position = $vastu_data[0]['section2position'];
    $section_title2 = $vastu_data[0]['section_title2'];
    $section_image2 = $vastu_data[0]['section_image2'];
    $section_content2 = $vastu_data[0]['section_content2'];
    $section3position = $vastu_data[0]['section3position'];
    $section_title3 = $vastu_data[0]['section_title3'];
    $section_image3 = $vastu_data[0]['section_image3'];
    $section_content3 = $vastu_data[0]['section_content3'];
    # code...
}
  
?>

<?php
$datacat = array(
);

$datacat['banner_id'] = $banner_id;
$this->load->view('banner',$datacat);
?>



<?php if ($vastuservices) {?>
<section class="pad-top-80 pad-bottom-50 " style="background-color: #fff; padding: 50px 0;">
    <div class="container">
        <div class="section-title text-center">
            <h1>Vastu</h1>
            <p>In-depth Astrology Consultation & Counseling .</p>
        </div>
        <!--.section-title-->
        <div class="row">
            <?php
                if ($vastuservices == 0) {
                    echo 'No record found into database';
                    } else {
                    $i = 1;
                    foreach ($vastuservices as $value) {
						//var_dump($value);
						// $disc_logo_percent = 'display_none';
						// if($value['disc_logo_percent']>0){
						// 	$disc_logo_percent = '';
						// }
					?>
            <!--.col-md-4-->
            <div class="col-sm-4 padding_b_20 text-center mb-4 premium_services"
                url="<?= base_url().'vastu/'.$value['services_url']?>">
                <a href="<?= base_url().'vastu/'.$value['services_url']?>">
                    <!-- <div class="label-group <?=$disc_logo_percent ?>">
					<div class="product-label label-sale">-<?=$value['disc_logo_percent'] ?>%</div>
				</div> -->
                    <div class="new_risk_box newrisk_box1">
                        <div class="w-100 img_box">
                            <img class="risk_first_image img-fluid w-100" "
                                src="<?= base_url().'uploads/'.$value['services_image'] ?>" alt="image">
                        </div>
                        <!-- <div class="new_risk_box_content new_risk_box_content_1" style="background:<?= $value['bottom_border_color'].'!important;'?>"> -->
                        <div class="new_risk_box_content new_risk_box_content_1">
                            <div class="new_risk_box_content2" style="padding: 5px 10px;">
                                <h1 class="text-white mb-2 text-center" style="font-size: 22px;">
                                    <?= $value['services_name'] ?></h5>
                                    <p class="text-center"><?=$value['services_description']?></p>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-light my-2">Read More</button>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php 
					$i++;
				}
			}	
		    ?>
        </div>
        <!--.row-->
    </div>
    <!-- .container -->


</section>

<?php } ?>


<?php if ($vastu_data) {?>
<div style="background-color: #fff5f5;padding-top: 20px;">


    <div style="background-color: #fff5f5;padding-top: 20px;">
        <div class="container my-4" style="min-height: 50vh;">
            <h3 class="text-center my-4 text-bold"><?php echo $section_title2 ?></h3>

            <div class="row">
                <?php if ($section2position == 'left' || $section2position == 'right') : ?>
                <div
                    class="justify-content-center align-items-center <?php echo $section2position == 'left' ? 'order-md-1 col-md-5' : 'order-md-2 col-md-5' ?>">
                    <div class="img-container">
                        <img src="<?php echo base_url() . 'uploads/'. $section_image2; ?>" alt="Image">
                    </div>
                </div>
                <div class="<?php echo $section2position == 'left' ? 'order-md-2 col-md-7' : 'order-md-1 col-md-7' ?>">
                    <div class="content-container">
                        <span><?php echo $section_content2 ?></span>
                    </div>
                </div>
                <?php elseif ($section2position == 'none') : ?>
                <div class="order-md-1 col-md-12 text-center">
                    <div class="content-container">
                        <span><?php echo $section_content2 ?></span>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="my-1">

    </div>
</div>

<?php }?>


<?php
$datacat = array(
);

$datacat['about_id'] = $about_id;
$this->load->view('aboutvastu',$datacat);
?>
</div>


<div class="my-1">

</div>



<?php if ($process_data && $process_data != 0) {?>
<section class="pad-top-80 pad-bottom-50 " style="background-color: #fff; padding: 50px 0;">
    <div class="container">
        <div class="section-title text-center">
            <h1>Our Vastu consultation Process</h1>
            <p></p>
        </div>
        <!--.section-title-->
        <div class="row">
            <?php
                if ($process_data == 0) {
                    echo 'No record found into database';
                    } else {
                    $i = 1;
                    foreach ($process_data as $value) {
						//var_dump($value);
						// $disc_logo_percent = 'display_none';
						// if($value['disc_logo_percent']>0){
						// 	$disc_logo_percent = '';
						// }
					?>
            <!--.col-md-4-->


            <div class="col-md-4 mt-1">
                <div class="card" style="padding: 1rem;">


                    <?php if ($value['process_image'] != '') {?>
                    <div class="image-container">
                        <img src="<?php echo base_url() . 'uploads/'. $value['process_image']; ?>" class="card-img-top"
                            alt="Image">
                    </div>
                    <?php } ?>



                    <div class="card-body text-center" style="padding:0!important;">
                        <h5 class="card-title"><strong><?= ''.$i.'. ' . $value['process_title'] ?></strong></h5>
                        <p class="card-text"><?=$value['process_content']?></p>
                    </div>
                </div>
            </div>
            <?php 
					$i++;
				}
			}	
		    ?>
        </div>
        <!--.row-->
    </div>

</section>
<?php } ?>



<?php
$datacat = array(
);

$datacat['category'] = $category_id;
$this->load->view('faqlist',$datacat);
?>
</div>



<?php if ($vastu_data) {?>

<div style="background-color: #fff5f5;padding-top: 20px;">


    <div class="container my-4" style="min-height: 50vh;">
        <h3 class="text-center my-4 text-bold"><?php echo $section_title1 ?></h3>

        <div class="row">
            <?php if ($section1position == 'left' || $section1position == 'right') : ?>
            <div
                class=" d-flex justify-content-center align-items-center<?php echo $section1position == 'left' ? 'order-md-1 col-md-5' : 'order-md-2 col-md-5' ?>">
                <div class="img-container">
                    <img src="<?php echo base_url() . 'uploads/'. $section_image1; ?>" alt="Image">
                </div>
            </div>
            <div class="<?php echo $section1position == 'left' ? 'order-md-2 col-md-7' : 'order-md-1 col-md-7' ?>">
                <div class="content-container">
                    <span><?php echo $section_content1 ?></span>
                </div>
            </div>
            <?php elseif ($section1position == 'none') : ?>
            <div class="order-md-1 col-md-12 text-center">
                <div class="content-container">
                    <span><?php echo $section_content1 ?></span>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php }?>





<!-- <?php if ($vastu_data) {?>





<div class="container my-4" style="min-height: 50vh;">
    <h3 class="text-center my-4 text-bold"><?php echo $section_title3 ?></h3>

    <div class="row">
        <?php if ($section3position == 'left' || $section3position == 'right') : ?>
        <div class="<?php echo $section3position == 'left' ? 'order-md-1 col-md-6' : 'order-md-2 col-md-6' ?>">
            <div class="img-container">
                <img src="<?php echo base_url() . 'uploads/'. $section_image3; ?>" alt="Image">
            </div>
        </div>
        <div class="<?php echo $section3position == 'left' ? 'order-md-2 col-md-6' : 'order-md-1 col-md-6' ?>">
            <div class="content-container">
                <span><?php echo $section_content3 ?></span>
            </div>
        </div>
        <?php elseif ($section3position == 'none') : ?>
        <div class="order-md-1 col-md-12 text-center">
            <div class="content-container">
                <span><?php echo $section_content3 ?></span>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>

<div class="my-1">

</div>

<?php } ?> -->



<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<script>
$(document).ready(function() {
    // Collapse other accordion items when a new one is clicked
    $('#accordion .card-header').click(function() {
        var target = $(this).attr('data-target');

        // Check if the target is not already collapsed
        if (!$(target).hasClass('show')) {
            // Collapse all other accordion items
            // $('#accordion .collapse').collapse('hide');

            // Show the clicked accordion item
            // $(target).collapse('show');
        }
    });
});
</script>