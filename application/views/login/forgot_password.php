<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Udaan | Make your skills on top level</title>
        <!--link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' /-->
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap3/css/bootstrap.css" rel='stylesheet' type='text/css' />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--script src="<?php echo base_url(); ?>assets/plugins/jquery-1.11.1.min.js"></script-->
        <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/constant.js"></script>
        <!--script src="<?php echo base_url(); ?>assets/plugins/bootstrap.min.js"></script-->
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.validate.min.js"></script>
        <!-- Custom css --> 
        <style>
            body{
                background: url(assets/images/bg5.jpg);
                background-size: 100%;
            }
            .row{	
                margin: 0 !important;
            }
            #loginbox{
                margin-top:50px;
            }
            .display_none{
                display: none;
            }
            .forgot_pass{
                float:right; font-size: 80%; position: relative; top:-10px;
            }
            .panel-body{
                padding-top:30px;
            }
            #loginform div.input-group{
                margin-bottom: 25px;
            }
            .marTop10{
                margin-top: 10px;
            }
            .signUpBtn div{
                border-top: 1px solid#888; padding-top:15px; font-size:85%;                
            }
            .marTop30{
                margin-top: 30px;
            }
            .marTop50{
                margin-top: 50px;
            }
            .signUpTitle{
                float:right; font-size: 85%; position: relative; top:-10px;
            }
            .SignUpFacebook{
                border-top: 1px solid #999; padding-top:20px;
            }
            .error{
                color: red;
            }

        </style>
    <div class="container">    
        <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Rest My Password</div>
                    <div class="forgot_pass"><a rel="nofollow" href="<?php echo base_url(); ?>login">Sign In</a></div>
                </div>     

                <div class="panel-body">
                    <div id="headMsg" class="col-sm-12"></div>
                    <div class="clearfix"></div>                          
                    <form id="forgotPasswordForm" class="form-horizontal" role="form">                                    
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="loginUserEmail" type="text" class="form-control" name="loginUserEmail" value="" placeholder="username or email">                                        
                        </div>
                        <div class="form-group marTop30">
                            <div class="col-sm-12 controls">
                                <input id="btn-login" type="submit" class="btn btn-success" >
                            </div>
                        </div>
                    </form>  
                </div>                     
            </div>  
        </div>
    </div>

    <script>
        $('document').ready(function () {
			$('#forgotPasswordForm').validate({
				rules:{
					loginUserEmail: {
						required: true
					},
				},
				onkeyup: false,
				messages: {
                    loginUserEmail: {
						required: 'Email is required.'
                    }
				},
				submitHandler: function (form) {
					var username = $("#loginUserEmail").val();
					/*Validating user supplied credentials*/
					$.post(APP_URL + 'authorization/authorization/send_link_for_reset_password', {
						username: username,
						password: password
					},
					function (response) {
						$("#headMsg").empty();
						if (response.status == 1000) {
							window.location.replace(response.mydirect);
						}
						else {
							$('#headMsg').empty();
							$('#headMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>Invalid Username or Password!</strong></div>");
						}
					}, 'json');
					return false;
				}
            });
        });
	</script>

</body>
</html>