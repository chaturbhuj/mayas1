<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Maya Astrology</title>
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap3/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/constant.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/jquery.validate.min.js"></script>
        <!-- Custom css --> 
        <style>
            body{
                background: url(assets/images/bg5.jpg);
                background-size: 100%;
            }
            .row{	
                margin: 0 !important;
            }
            #loginbox{
                margin-top:50px;
            }
            .display_none{
                display: none;
            }
            .forgot_pass{
                float:right; font-size: 80%; position: relative; top:-10px;
            }
            .panel-body{
                padding-top:30px;
            }
            #loginform div.input-group{
                margin-bottom: 25px;
            }
            .marTop10{
                margin-top: 10px;
            }
            .signUpBtn div{
                border-top: 1px solid#888; padding-top:15px; font-size:85%;                
            }
            .marTop30{
                margin-top: 30px;
            }
            .marTop50{
                margin-top: 50px;
            }
            .signUpTitle{
                float:right; font-size: 85%; position: relative; top:-10px;
            }
            .SignUpFacebook{
                border-top: 1px solid #999; padding-top:20px;
            }
            .error{
                color: red;
            }

        </style>
    <div class="container">    
        <div id="loginbox" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">                    
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title">Sign In</div>
                    <div class="forgot_pass"><a rel="nofollow" href="<?php echo base_url(); ?>forgot_password">Forgot password?</a></div>
                </div>     

                <div class="panel-body">
                    <div id="headMsg" class="col-sm-12"></div>
                    <div class="clearfix"></div>                          
                    <form id="userLoginForm" class="form-horizontal" role="form">                                    
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="loginUserEmail" type="text" class="form-control" name="loginUserEmail" value="" placeholder="username or email">                                        
                        </div>
                        <div class="input-group marTop30">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="loginPassword" type="password" class="form-control" name="loginPassword" placeholder="password">
                        </div>
                        <div class="input-group display_none">
                            <div class="checkbox">
                                <label><input id="login-remember" type="checkbox" name="remember" value="1"> Remember me</label>
                            </div>
                        </div>
                        <div class="form-group marTop30">
                            <div class="col-sm-12 controls">
                                <input id="btn-login" type="submit" class="btn btn-success" value="Login">
                                <a rel="nofollow" style="display:none;" id="btn-fblogin" href="#" class="btn btn-primary">Login with Facebook</a>
                            </div>
                        </div>
                        <div class="form-group display_none">
                            <div class="col-md-12 control signUpBtn">
                                <div>Don't have an account! 
                                    <a rel="nofollow" href="#" onClick="$('#loginbox').hide();
                                            $('#signupbox').show()">
                                        Sign Up Here
                                    </a>
                                </div>
                            </div>
                        </div>    
                    </form>  
                </div>                     
            </div>  
        </div>
        <div id="signupbox" class="marTop50 display_none mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">Sign Up</div>
                    <div class="signUpTitle">
                        <a rel="nofollow" id="signinlink" href="#" onclick="$('#signupbox').hide();
                                $('#loginbox').show()">Sign In</a></div>
                </div>  
                <div class="panel-body" >
                    <form id="signupform" class="form-horizontal" role="form">
                        <div id="signupalert" class="display_none alert alert-danger">
                            <p>Error:</p>
                            <span></span>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" placeholder="Email Address">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-3 control-label">First Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="firstname" placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastname" class="col-md-3 control-label">Last Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="lastname" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="passwd" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="icode" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <!-- Button -->                                        
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>
                                <span style="margin-left:8px;">or</span>  
                            </div>
                        </div>

                        <div class="SignUpFacebook form-group">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>   Sign Up with Facebook</button>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </div>

    <script>
        $(document).ready(function () {

            $('#userLoginForm').validate({
                rules: {
                    loginUserEmail: {
                        required: true
                    },
                    loginPassword: {
                        required: true
                    }
                },
                onkeyup: false,
                messages: {
                    loginUserEmail: {
                        required: 'User Email is required.'
                    },
                    loginPassword: {
                        required: 'Password is required.'
                    }
                },
                submitHandler: function (form) {
                    var username = $("#loginUserEmail").val();
                    var password = $("#loginPassword").val();
                    /*Validating user supplied credentials*/
                    $.post(APP_URL + 'authorization/authorization/check_login_credentials', {
                        username: username,
                        password: password
                    },
                    function (response) {
                        $("#headMsg").empty();
                        if (response.status == 1000) {
                            window.location.replace(response.mydirect);
                        }
                        else {
                            $('#headMsg').empty();
                            $('#headMsg').html("<div class='alert alert-danger fade in'>\n\<button class='close' type='button' data-dismiss='alert'>x</button>\n\<strong>Invalid Username or Password!</strong></div>");
                        }
                    }, 'json');
                    return false;
                }
            });
        });


    </script>

</body>
</html>