
	<!--header-->

	<div class="perfect sec-gap120">
		<img src="<?php echo base_url();?>assets/images/Path67.png" class="img-fluid per-left" alt="image" />
		<div class="container">
			<div class="row align-items-center margin_top_50px">
			<div class="col-lg-6">
					<style>
						.owl-carousel{
							display: block;
						}
					</style>
					<!-- <div class="what-img">
						<img src="<?php echo base_url();?>public/images/banner2.png" class="img-fluid" alt="image" />
					</div> 	 -->
					<?php
						//var_dump($slider_list);
						if($slider_list){
							echo '<div class="what-img maya-slider-owl-carousel owl-carousel owl-theme" style="padding:0px;">';
							foreach($slider_list as $slider){
								echo
									'<div class="item" style="padding: 15px;">
										<img class="img-fluid" src="'.base_url().'uploads/'.$slider['slider_image'].'"  alt="image" />
									</div>';
							}
						     echo '</div>';
						}
					?>

				</div>
				<div class="col-lg-6">
					<div class="social-text margin_bottom100">
						<?php
						 //var_dump($page_data);
						 foreach($page_data as $data){
							if($data['id'] == 1){
								echo'<h3 class="heading_title text_green">'.$data['page'].'</h3>';
								echo'<div class="text-left slider_text">'.$data['data'].'</div>';
							}
						}
						?>
						<a href="<?php echo base_url();?>premium-consultations"   class="slide-btn slider_btn">See All Atrology & vastu Consultation <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){

			$(".maya-slider-owl-carousel").owlCarousel({
				center: true,
				loop:true,autoplay:true, 
				margin:10,
				merge:true,
				responsive:{
					200:{
						items:1
					},
					678:{
						items:1
					},
					1000:{
						items:1
					}
				}
			});

		});

	</script>
	<style>
		.left-perfect p {
			text-align: left !important;
		}
		.position_relative{position: relative;}
		.owl-nav button{height: 60px; position: absolute; width: 30px; cursor: pointer !important; background-color: #8697915c !important; z-index: 999999999; top: 40%; color: #fff !important; font-size: 40px !important; padding: 0px 0px !important;}
		.owl-prev{ left: -15px; }
		.owl-next{ right: -35px; }
	</style>
	<div class=" left-perfect sec-gap bg_image_layer">
		<div class="container">
			<div class="row align-items-center margin_top_50px">
				<div class="col-md-12 perfect">
					<?php
					//var_dump($page_data);
					foreach($page_data as $data){
						if($data['id'] == 5){
							echo'<h3 class="text_green">'.$data['page'].'</h3>';
							echo'<div>'.$data['data'].'<a class="consult_btn" code="'.$data['video_link'].'">How to Book Free Consultation </a>
							<img src="'.  base_url().'public/images/youtube.webp" class="img-fluid " style="    width: 23px;" alt="image" /></div>';
						}
					}
					?>

					</div>
			</div>
			
			<br/>
			<div class="free_consultation_div_box  row   margin_top_50px">
					
						<?php
							$consultation_data ='';
						   //var_dump($consultation_content);
							if($consultation_content){
								$i = 0;
								foreach($consultation_content as $content){
									if($i <3){
									$consultation_data .=
									'<div class="col-12 col-sm-6  col-md-3 "><div class="life_box_outer life_box_blue" style="padding: 10px; "  url="'.base_url().'free-consultation/'.$content['url'].'">
										<div class="" style="background:"'.$content['bg_color'].';color:'.$content['text_color'].'">
											<p style="text-align:center !important;font-size: 20px; font-weight: 800;">'.$content['consultation_name'].'</p>
											<p class="text_align_left">'.$content['main_description'].'</p>
											<div class="text-center"><button class="btn btn-success" style="background-color: #86E0D5; border-color: #86E0D5">Book Now</button></div>
										</div>
									</div></div>';	
									}
									 $i++;
								}
								
									$consultation_data .=
									'<div class="col-12   col-sm-6 col-md-3"><div class="life_box_outer life_box_blue" style="padding: 10px;width: 100%; "  ><a href="'.base_url('free-consultations').'">
										<div class=" ">
											<p style="text-align:center !important;font-size: 23px;    margin-top: 100px;color: #0C2494 !important; font-weight: 800;">See All Free Consultations</p>
										</div></a>
									</div></div>';
									
							}

							echo $consultation_data;

						?>
			</div> 
			
			<div class="position_relative row align-items-center margin_top_50px display_none">
				<div class="col-md-12 ">
					<div class="">
					<div class="free-owl-carousel owl-carousel owl-theme ">
						<?php
							$consultation_data ='';
						   //var_dump($consultation_content);
							if($consultation_content){
								foreach($consultation_content as $content){
									if($i >3){
									$consultation_data .=
									'<div class="life_box_outer item" style="min-height: 300px;" url="'.base_url().'free-consultation/'.$content['url'].'">
										<div class="life_box life_box_blue" style="background:"'.$content['bg_color'].';color:'.$content['text_color'].'">
											<img style="margin-top: 20px;margin-bottom: 20px;" src="'.base_url().'uploads/'.$content['c_image'].'" class=" img-fluid" alt="logo">
											<p style="text-align:center !important;font-size: 20px; font-weight: 800;">'.$content['consultation_name'].'</p>
										
											
										</div>
									</div>';
									}
									 $i++;

								}
							}
							/* <p class="text_align_left">'.$content['main_description'].'</p> */
							echo $consultation_data;

						?>
					</div>
					</div>
				</div>
			</div> 
			
			<div class="text-center display_none">
				<a href="<?= base_url().'free-consultations'?>"  class="slide-btn">See All Free Consultation<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
			</div>
		</div>
	</div>
	<script type="text/javascript">
    $(function () {
        $("[rel='tooltip']").tooltip();
    });
	$('document').ready(function(){
        $('.free-owl-carousel').owlCarousel({
			//center: true,
			loop:true,
			margin:10,
			nav:true,
			merge:true,
			autoWidth:false,
			responsive:{
				200:{
					items:1.5
				},
				678:{
					items:2.5
				},
				1000:{
					items:3.5
				}
			}
		});
    });
</script>
	
	<div class="  "></div>
	<div class="perfect sec-gap ">
	
		<div class="container">
			<div class="row">
			<div class="col-sm-12 perfect">
				<?php
						foreach($page_data as $data){
							if($data['id'] == 2){
								echo'<h3 class="heading_title margin0  text_green">'.$data['page'].'</h3>';
								echo'<div class="text-left margin0">'.$data['data'].'<a class="pre_consult_btn" code="'.$data['video_link'].'">Premium Consultation<i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>';
							}
						}
						?>
			</div>
			</div>
		<div class="free_consultation_div_box  row   margin_top_50px">
					
						<?php
							$consultation_data ='';
						   //var_dump($consultation_content);
							if($services){
								$i = 0;
								foreach($services as $content){
									$disc_logo_percent = 'display_none';
									if($content['disc_logo_percent']>0){
										$disc_logo_percent = '';
									}
									if($i <3){
									$consultation_data .=
									'<div class="col-12 col-sm-6  col-md-3 ">
										<div class="life_box_outer life_box_blue_light premium_services" style="padding: 10px; "   url="'.base_url().'premium-consultation/detail/'.$content['services_url'].'"">
											<div class="" >
												<p style="text-align:center !important;font-size: 20px; font-weight: 800;">
												<img class="risk_first_image img-fluid" src="'.base_url().'uploads/'.$content['services_image'].'"  alt="image" /></p>
												<div class="label-group '.$disc_logo_percent.'">
											<div class="product-label label-sale">-'.$content['disc_logo_percent'].'%</div>
										</div>
												<h3 style="font-size: 21px; font-weight: 800; text-align: center !important;" class="text_align_left">'.$content['services_name'].'</h3>
												<p class="text_align_left">'.$content['services_main_description'].'</p>
												<div class="text-center"><button class="btn btn-success" style="background-color: #86E0D5; border-color: #86E0D5">Book Now</button></div>
											</div>
										</div>
									</div>';	
									}
									 $i++;
								}
								
									$consultation_data .=
									'<div class="col-12   col-sm-6 col-md-3"><div class="life_box_outer life_box_blue_light" style="padding: 10px;width: 100%; "  ><a href="'.base_url('premium-consultations').'">
										<div class=" ">
											<p style="text-align:center !important;font-size: 23px;    margin-top: 100px;color: #0C2494 !important; font-weight: 800;">See All Premium Consultations</p>
										</div></a>
									</div></div>';
									
							}

							echo $consultation_data;

						?>
			</div> 
			
			
			<div class="row">
				<div class="col-md-12 risk_carousel">
					<div class="team_outer_div">
						
						<br/>
						<div class="position_relative premium-owl-carousel owl-carousel owl-theme ">
							<?php
							//var_dump($services);
							if($services){
								$service_data='';$i = 0;
								foreach($services as $val){
									 $i++;
									if($i > 3){
									$disc_logo_percent = 'display_none';
									if($val['disc_logo_percent']>0){
										$disc_logo_percent = '';
									}
									$service_data .='
									<div class="item padding_b_20 premium_services" style="" url="'.base_url().'premium-consultation/detail/'.$val['services_url'].'">
										
										<div class="risk_box risk_box1" style="border-radius: 10px;background-color: #fff; border-bottom: 3px solid '.$val['bottom_border_color'].'!important;">
											
												<h5 class="padding_b_0 padding10 margin0">'.$val['services_name'].'</h5>
												<p class="text-left padding10 font_size_14">'.$val['services_main_description'].'</p>
										</div>
									</div>';
								}
								}
								echo $service_data;
							}
							?>
						</div>
						<div class="text-center">
							<a href="<?= base_url().'premium-consultations'?>"  class="slide-btn">See All Premium Consultation<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$('document').ready(function(){

        $('.premium-owl-carousel').owlCarousel({
			//center: true,
			loop:true,
			margin:10,
			merge:true,
			autoWidth:false,
    nav:true,
			responsive:{
				200:{
					items:1.5
				},
				678:{
					items:1.5
				},
				1000:{
					items:3.5
				}
			}
		});
    });
</script>
	<!--what is-->
	<div class="what-is sec-gap bg_image_layer">
		<img src="<?php echo base_url();?>public/images/Mask Group 3.png" class="img-fluid what-right" alt="image" />
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="what-left">
					<h3 class="heading_title margin0  text_green">Vastu Consultation</h3>
					<div class="what-img">

						<img src="<?php echo base_url();?>public/images/1.jpg" class="img-fluid" alt="image" />
						<a href="#" class="icon">
							<img src="<?php echo base_url();?>public/images/Group 6.png" class="img-fluid" alt="image" />
						</a>

					</div>
					<a href="#" class="slide-btn">See all vastu consultation<img src="<?php echo base_url();?>public/images/Group 2.png" class="img-fluid" alt="arrow"></a>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="what-right-sec">
						<div class="circle1">
							<div>
								<a class="text-decoration-none" href="<?=base_url().'vastu/office-vastu'?>">	<h6>  Office Vastu </h6></a>
							<p>Purpose | Excellence | Innovation | Inclusivity | Growth</p>
						</div>
						</div>
						<div class="circle2">
							<div>
							<a class="text-decoration-none" href="<?=base_url().'vastu/home-vastu'?>">	<h6>  Home Vastu </h6></a>
							<p>Customer Engagement & Experience | Ecosystem | Realtime Scorecard | Integrity | Leadership</p>
						</div>
						</div>
						<div class="circle3">
							<div>
							<a class="text-decoration-none" href="<?=base_url().'vastu/industry-vastu'?>">	<h6>  Industry Vastu </h6></a> 
							<p>Customer Engagement & Experience | Ecosystem | Realtime Scorecard | Integrity | Leadership</p>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--perfect-->
	<div class="perfect sec-gap bg_image_layer display_none">
		<img src="<?php echo base_url();?>public/images/Path 63.png" class="img-fluid per-left" alt="image" />
		<img src="<?php echo base_url();?>public/images/Path 62.png" class="img-fluid per-right" alt="image" />
		<div class="container">
			<div class="row">
			<div class="col-12">
				<?php
				foreach($page_data as $data){
							if($data['id'] == 3){
								echo'<h3 class="heading_title margin0  text_green">'.$data['page'].'</h3>';
								echo'<div class="text-left">'.$data['data'].'</div>';
							}
						}
						?>
					<div class="icon-main">
						<div class="icon-inner">
							<div class="icon-content">
								<div class="icon-round">
									<img src="<?php echo base_url();?>public/images/i1.png" class="img-fluid" alt="image" />
								</div>
								<div class="label-group">
                                        <div class="product-label label-hot">HOT</div>
                                        <div class="product-label label-sale">-30%</div>
                                    </div>
								<span>Product Name</span>
								<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
								<span>₹999.99</span>
						    </div>
						</div>
						<div class="icon-inner">
							<div class="icon-content">
								<div class="icon-round">
									<img src="<?php echo base_url();?>public/images/i1.png" class="img-fluid" alt="image" />
								</div>
								<div class="label-group">
                                        <div class="product-label label-hot">HOT</div>
                                        <div class="product-label label-sale">-30%</div>
                                    </div>
								<span>Product Name</span>
								<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
								<span>₹999.99</span>
						    </div>
						</div>
						<div class="icon-inner">
							<div class="icon-content">
								<div class="icon-round">
									<img src="<?php echo base_url();?>public/images/i1.png" class="img-fluid" alt="image" />
								</div>
								<span>Product Name</span>
								<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
								<span>₹999.99</span>
						    </div>
						</div>
						<div class="icon-inner">
							<div class="icon-content">
								<div class="icon-round">
									<img src="<?php echo base_url();?>public/images/i1.png" class="img-fluid" alt="image" />
								</div>
								<div class="label-group">
                                        <div class="product-label label-hot">HOT</div>
                                        <div class="product-label label-sale">-30%</div>
                                    </div>
								<span>Product Name</span>
								<span><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>
								<span>₹999.99</span>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="perfect sec-gap ">
		<div class="testimonial_clients">
			<div class="container">
				<h3 class="heading_title margin0  text_green">Your Views</h3>
				<div class="">
					<div class="">
						<div class="" >
							<div class="" style=" margin-right: 30px;">
								<div class="row">
									<div class=" col-md-12 ">
										<div id="" class="position_relative owl-carousel testimonial_carousel owl-theme">
											<?php
												if($testimonials_list){
													foreach($testimonials_list as $val){
														echo
														'<div class="item">
															<div class="testimonial">
																<div class="pic">
																	<img src="'.base_url().'uploads/'.$val['testimonials_image'].'" alt="">
																	<div>'.$val['testimonials_name'].'</div>
																	<div class="testimonial-content">
																	<span class="upper_quote">“</span>
																	<p class="description">'.$val['comments'].'</p>
																	<span class="lower_quote">”</span>
																</div>
																</div>

															</div>
														</div>';
													}
												}
											?>

								    	</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function() {

			$(".testimonial_carousel").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				loop:true,
				nav:true,
			    //items : 3,
				//itemsDesktop : false,
				//itemsDesktopSmall : false,
				//itemsTablet: false,
				//itemsMobile : false

                responsive:{
                        0:{
                             items:1,

      					  },
      				    600:{
         				     items:2.5,

       					   },
     				   1000:{
       					     items:3.5,

     					   }
  					  }
			});

		});
	</script>
	<div class="perfect sec-gap bg_image_layer">
		<img src="<?php echo base_url();?>assets/images/Path67.png" class="img-fluid per-left" alt="image" />
		<div class="container">
			<div class="row">
				<div class="col-md-12 ">
					<div class="col-md-12">
						<div class="team_outer_div"><?php
				        foreach($page_data as $data){
							if($data['id'] == 4){
								echo'<h3 class="heading_title margin0  text_green">'.$data['page'].'</h3>';
								echo'<div class="text-left margin0">'.$data['data'].'</div>';
							}
						}
						?>
						</div>
					</div>
					<div class="col-md-12 mt-4">
						<div id="owl-demo" class="blog_carousel owl-carousel owl-theme">
							<?php
								if($blog_list){
									foreach($blog_list as $blog){
										?>
											<div class="item">
												<div class=" " style="border: 1px solid #e3e0e0; border-radius: 10px; background-color: #fff;    padding: 10px;width: -webkit-fill-available;">
												<div class=" ">
														<img src="<?php echo base_url().'uploads/'.$blog['blog_image'];?>" class="img-fluid width100 " alt="arrow">
													
														<br><br><br><h2 class="text-left text-center"><?= $blog['blog_heading']?></h2>
															<p class="text-left text-center"><?= $blog['blog_sub_heading']?></p>
														<div class="text-left text-center">
														<a href="<?php echo base_url().'blog/detail/'.$blog['blog_id'].'/'. strtolower(preg_replace('/[^a-zA-Z0-9]/s','-',$blog['blog_heading']))?>"  class="slide-btn">Read More <img src="<?php echo base_url();?>public/images/Group 2.png" class="img-fluid" alt="arrow" style="width: auto;display:inherit"></a>
														</div>
														<br><br><br>
													</div>

												</div>
											</div>
										<?php
									}
								}
							?>

						</div>
						<div class="text-center">
							<a href="<?= base_url().'blog';?>" class="slide-btn">See All Blogs &nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>

		$(document).ready(function() {
			$(".blog_carousel").owlCarousel({

				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				loop:true,
				//itemsDesktop : false,
				//itemsDesktopSmall : false,
				//itemsTablet: false,
				//itemsMobile : false
				responsive:{
                        0:{
                            items:1,
                          },
                        600:{
                              items:1.2,
                            },
                        1000:{
                               items:3,

                             }
                          }
			});

		});
	</script>

	<!--
	mobile -> banner -> before

	blogs -> from home page
	testimonals

	service listing  -> model of operation slide
	detail + form => combine in one page
	-->


	<!--footer-->
	<!--modal -->
