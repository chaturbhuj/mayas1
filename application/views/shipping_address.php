<style>
    #add_customer_address_data .form-control{
        height: 45px !important;
    }
    .required{
        color: red;
    }
    .address_container{
        padding-bottom: 100px !important;
        padding-top: 100px !important;
    }
</style>
<div class="container pt-5 address_container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="mb-3">Shipping Address</h3>
            <div class="col-sm-12 d-inline-block">
               <button  href="#" type="button" data-toggle="modal" data-target="#addCustomerAddressModal" class="btn btn-primary float-right"><i class="fas fa-plus"></i> &nbsp;Add New</button>												  
            </div>
            <div id="headMsg_remove_profile"></div>												
            <div id="append_customer_address"></div><br/><!--first row-->

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCustomerAddressModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add Shipping Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div id="checkout_headMsg"></div>
            <form id="add_customer_address_data" method="post">
                <input type="hidden" id="checkout_customer_address_id" value="">
                <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">First Name <span class="required">*</span></label>
                        <input class="form-control border-form-control" id="checkout_firt_name" name="checkout_firt_name" value="" placeholder="Enter First Name" type="text">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Last Name <span class="required">*</span></label>
                        <input class="form-control border-form-control" id="checkout_last_name" name="checkout_last_name" value="" placeholder="Enter Last Name" type="text">
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Phone <span class="required">*</span></label>
                        <input class="form-control border-form-control" id="checkout_phone_number" name="checkout_phone_number" value="" placeholder="Enter Phone Number" type="number">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        
                        <label class="control-label">Country <span class="required">*</span></label>
                            <input class="form-control border-form-control" id="checkout_country_id" name="checkout_country_id" value="" placeholder="Enter Country Name" type="text">
                    </div>
                </div>
                <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">State <span class="required">*</span></label>
                            <input class="form-control border-form-control" id="checkout_state_id" name="checkout_state_id" value="" placeholder="Enter state Name" type="text">
                        </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">				
                            <label class="control-label">City <span class="required">*</span></label>
                            <input class="form-control border-form-control" id="checkout_city_id" name="checkout_city_id" value="" placeholder="Enter city Name" type="text">					
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">Zip Code <span class="required">*</span></label>
                            <input class="form-control border-form-control" id="checkout_zip_code" name="checkout_zip_code" value="" placeholder="Enter Zip Code" type="text">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">Shipping Address <span class="required">*</span></label>
                            <textarea class="form-control border-form-control" id="checkout_address_line" name="checkout_address_line"></textarea>
                            <small class="text-danger">Please provide the number and street.</small>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                     <button type="submit" class="btn btn-primary mb-2 w-50">Add Addresss</button>
                </div>
            </form>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(){
        getcustomer_address_list();
    function getcustomer_address_list(){
      	var customer_id=$('#customer_id').val();
        $.post(APP_URL+'api/customer_account/fatch_customer_address_list_data',{
        	customer_id: customer_id,
        },function(response){
			if (response.status == 200) {
				$('#checkout_email').val(response.user_data.customer_email);
					//console.log(response.user_data.customer_email);
				var result=response.data;
				var content='';
				var selected_address='';
				var selected_customer_address_id='';
				var selected_first_name='';
				var selected_last_name='';
				var selected_address_line='';
				var selected_city_name='';
				var selected_state_name='';
				var selected_zip_code='';
				var selected_country_name='';
				var selected_shipping_phone_number='';
				$. each(result, function(index, value){
					var add_class='';
					if(value.is_selected=='1'){
						add_class='border border-success';
						selected_customer_address_id=value.customer_address_id;
						selected_first_name=value.first_name;
						selected_last_name=value.last_name;
						selected_address_line=value.address_line;
						selected_city_name=value.city_name;
						selected_state_name=value.state_name;
						selected_zip_code=value.zip_code;
						selected_country_name=value.country_name;
						selected_shipping_phone_number=value.shipping_phone_number;

						
					}
					content+=' <div class="card mb-1">';
						content+='<div class="card-body">';
							content+='<button href="javascript:void(0)" class="btn btn-danger float-right remove_address" customer_address_id="'+value.customer_address_id+'"  ><i class="fas fa-trash-alt"></i>&nbsp;Remove</button>';
							content+='<button href="#" class="btn btn-success float-right edit_address" customer_address_id="'+value.customer_address_id+'" data-toggle="modal" data-target="#addCustomerAddressModal"   style="margin-right: 6px; color:#ffff;"><i class="fas fa-edit"></i>&nbsp;Edit</button>';
							content+='<h5 class="card-title" style="display:inline-block;">'+value.first_name+' '+value.last_name+'</h5>';
							content+='<p class="card-text">'+value.address_line+', '+value.city_name+', '+value.state_name+' '+value.zip_code+', '+value.country_name+'  '+value.shipping_phone_number+'</p>';
						content+='</div>';
					content+='</div>';
				});
				$('#append_customer_address').html(content);
				
			}
		},'json');
    }
	$('body').on('click',".edit_address",function (e) {
		$('#checkout_country_id option').removeAttr('selected')
		$('#checkout_state_id option').removeAttr('selected')
		$('#checkout_city_id option').removeAttr('selected')
	 var customer_address_id=$(this).attr('customer_address_id');
	 	
       //console.log('edit address');
       //console.log(customer_address_id);
		$.post(APP_URL+'api/customer_account/fatch_customer_address_data',{
			customer_id: customer_address_id,
		},function(response){
			if (response.status == 200) {
				var result =response.data[0];
				//var results =response.user_data;
				//console.log(result);
				//console.log(results);				
				$('#checkout_firt_name').val(result.first_name);					
				$('#checkout_customer_address_id').val(result.customer_address_id);					
				$('#checkout_last_name').val(result.last_name);				
				$('#checkout_phone_number').val(result.shipping_phone_number);				
				$('#checkout_country_id').val(result.country_name);				
				$('#checkout_state_id').val(result.state_name);				
				$('#checkout_city_id').val(result.city_name);				
				$('#checkout_zip_code').val(result.zip_code);
				$('#checkout_address_line').val(result.address_line);							
			}
		},'json');
		
			
    })
	$('body').on('click',".remove_address",function (e) {
        if(confirm("Are you sure, you want to delete adress?")){
            var customer_address_id=$(this).attr('customer_address_id');
       console.log('edit address');
       console.log(customer_address_id);
		$.post(APP_URL+'api/customer_account/remove_shipping_address',{
			customer_address_id: customer_address_id,
			
		},function(response){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#headMsg_remove_profile').empty();
				if (response.status == 200) {			
					var message = response.message;
					$('#headMsg_remove_profile').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg_remove_profile").fadeTo(2000, 500).slideUp(500, function(){
						$('#headMsg_remove_profile').empty();
						window.location.reload();
					});
				}else if (response.status == 201) {
					$('#headMsg_remove_profile').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
	    },'json');
		
        }
    });
	
	$('#add_customer_address_data').validate({
		ignore: [],
		rules: {
			checkout_firt_name: {
				required: true,
			},
			checkout_last_name: {
				required: true,
			},
			checkout_phone_number: {
				required: true,
				minlength: 10, 
				maxlength: 13
			},
			checkout_zip_code: {
				required: true,
			},
			checkout_country_id: {
				required: true,
			},
			checkout_state_id: {
				required: true,
			},
			checkout_city_id: {
				required: true,
			},
			checkout_address_line: {
				required: true,
			},
		},
		messages: {
			checkout_firt_name: {
				required: "First name is required",
			},
			checkout_last_name: {
				required: "Last name is required",
			},
			checkout_phone_number: {
				required: "Phone number is required",
			},
			checkout_zip_code: {
				required: "Zip Code is required",
			},
			checkout_address_line: {
				required: "address is required"
			},
			checkout_city_id: {
				required: "city is required"
			},
			checkout_state_id: {
				required: "state is required"
			},
			checkout_country_id: {
				required: "country is required"
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var customer_id = $('#customer_id').val();
			var customer_address_id = $('#checkout_customer_address_id').val();
			var first_name = $('#checkout_firt_name').val();
			var last_name = $('#checkout_last_name').val();
			var shipping_phone_number = $('#checkout_phone_number').val();
			var zip_code = $('#checkout_zip_code').val();
			var country_name = $('#checkout_country_id').val();
			var state_name = $('#checkout_state_id').val();
			var city_name = $('#checkout_city_id').val();
			var address_line = $('#checkout_address_line').val();
			
			$.post(APP_URL+'api/customer_account/update_customer_address_data',{
				customer_id: customer_id,
				customer_address_id: customer_address_id,
				first_name: first_name,
				last_name: last_name,
				address_line: address_line,
				city_name: city_name,
				state_name: state_name,
				zip_code: zip_code,
				country_name: country_name,
				shipping_phone_number: shipping_phone_number,
			},function(response){
					//console.log("response");
					//console.log(response);
				//console.log($result);
				$.unblockUI();
				//$("html, body").animate({scrollTop: 0}, "slow");
				$('#checkout_headMsg').empty();
				if (response.status == 200) {
					console.log("1");
					var message = response.message;
					$('#checkout_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#checkout_headMsg").fadeTo(2000, 500).slideUp(500, function(){
						$('#checkout_headMsg').empty();
						$('#addCustomerAddressModal').modal('hide');
						getcustomer_address_list();
						//window.location.href = APP_URL+'account/profile';
					});
				} else if (response.status == 201) {
					$('#checkout_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			return  false;
		}
	});
	

	
	$('body').on('change', "#checkout_country_id", function(e) {
		$('#checkout_state_id option').remove();
		var optVal1 =  $('#checkout_state_id_temp > option').clone();
		var counrty_id =  $('#checkout_country_id option:selected').val();
		//console.log(optVal1);
		//console.log(counrty_id);
		$('#checkout_state_id').append(optVal1);
		$('#checkout_state_id option').each(function() {
			if ($(this).attr('country_id') != counrty_id) {
				if ($(this).attr('value') != "") {
					$(this).remove();
				}
			}
		});
	});
	 
	$('body').on('change', "#checkout_state_id", function(e) {
		$('#checkout_city_id option').remove();
		var optVal2 =  $('#checkout_city_id_temp > option').clone();
		var state_id2 =  $('#checkout_state_id  option:selected').val();
		//console.log(optVal2);
		//console.log(state_id2);
		$('#checkout_city_id').append(optVal2);
		$('#checkout_city_id option').each(function() {
			if ($(this).attr('state_id') != state_id2) {
				if ($(this).attr('value') != "") {
					$(this).remove();
				}
			}
		});
	});	
	
    });
</script>