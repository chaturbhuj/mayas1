<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/ckeditor/adapters/jquery.js"></script>
<style>
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.table-responsive {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
}
.table {
    width: 100%;
    margin-bottom: 1rem;
    color: #212529;
}

.contentbox:empty:before {
    content: attr(data-placeholder);
	color: gray;
}

.contentbox{
	min-height: 45px;
	padding: 5px;	
	border: 1px solid #ced4da;
}
.msgbox{
		border:solid 1px #dedede;
		padding:5px;
		display:none;
		background-color:#f2f2f2;
		}
	
		#contentbox:focus {
		outline: none !important;
		border: 1px solid #ced4da!IMPORTANT;
		box-shadow: none !important;
		}
	
.display{
			display:none;
			border-left:solid 1px #dedede;
			border-right:solid 1px #dedede;
			border-bottom:solid 1px #dedede;
			overflow:hidden;
		}
		.display_box { 
			padding: 6px 4px;
			border-top: solid 1px #dedede;
			font-size: 14px;
			height: 38px;
		}
		
		.display_box:hover{
			background: #f3eeeea6;
			color:#FFFFFF;
		}
		.display_box a{
			color:#333;
		}
		.display_box a:hover{
			color: black;
			font-weight: 400;
		}
	
.wishlist_option{
  z-index: 2;
   
    border-radius: 2px;
    display: inline-block;
    font-size: 18px;
    height: 22px;
    line-height: 19px;
    position: absolute;
    right: 25px;
    text-align: center;
    top: 20px;
    width: 22px;
    cursor: pointer;
	color:#F7CC47;
	}
	#container1
		{
		 padding:10px;
		}
	.submit_comments{
			width: 32px;
			height: 32px;
			min-width: 35px;
			padding-left: 24px;
			padding-right: 36px;
			
			font-weight: 300;
			border-radius: 6px;
			border: solid 1px #9fa3ac;
			display: -moz-inline-box;
			display: -ms-inline-flexbox;
			display: -webkit-inline-flex;
			display: inline-flex;
			justify-content: center;
			align-items: center;
			-webkit-transition: 0.7s;
			-o-transition: 0.7s;
			transition: 0.7s;
		}
</style>
<!-- ::::::  Start  Main Container Section  ::::::  -->
   <!-- ::::::  Start  Main Container Section  ::::::  -->
<div class="dashboard-content py-4">
   <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
                <div  style="margin-top: 20px; margin-bottom:20px;" class="col-md-12">
                    <div  style="margin-bottom:40px;" class="card card-body account-right">
                        <div class="widget">
                            <div class="section-header">
                                <h5 class="heading-design-h5 mb-3">Free Consultation List</h5>
                                <input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id')?>">
                                <input type="hidden" id="complain_type" value="">	
                            </div>
                            <div class="order-list-tabel-main table-responsive">
                                <table class="datatabel table order-list-tabel text-center">
                                <thead>
                                    <tr role="row">
                                        <th>Consultant Type</th>
                                        <th>Full Name</th>
                                        <th>Appointment Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody id="order_data_content">
                                    <?php //var_dump($free_consulation_list);
                                    $output ='';
                                    if(!empty($free_consulation_list)){

                                        foreach($free_consulation_list as $val){
                                            $badge = $val['appointment_yes']=='pending' ? 'badge-warning' : 'badge-success';
                                            $output.='<tr>
                                                <td>'.$val['consultantType'].'</td>
                                                <td>'.$val['full_name'].'</td>
                                                <td>'.$val['created_date'].'</td>
                                                <td><span class="badge '.$badge.' ">'.$val['appointment_yes'].'</span></td>
                                            </tr>';
                                        }
                                    }else{
                                        $output =' No record found in database';
                                    }
                                    echo $output;
                                    ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
			
		</div>
	</main>
</div>