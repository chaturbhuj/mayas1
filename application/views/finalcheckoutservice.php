<style>

.display_none { display:none;}
</style>
<script>
	$('document').ready(function(){
		var d = new Date().getTime();
		$('#TID').val(d);
	});
</script>
<script>
	$('document').ready(function(){
		var id = new Date().getTime();
		$('#order_id').val(id);
	});
</script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<div class="container" style="margin-top: 110px; height:600px;">
	

	<div class="row" >
		<h3 style="text-align:center;"> Processing ....</h3>
		<div class="col-md-12 text-center btn_home display_none" style="margin-top:20px;">
			<label style="font-size:20px;" class="label label-success"><a href="<?php echo base_url(); ?>" style="color: #fff;">GO TO HOMEPAGE </a></label>
		</div>
		<div class="col-sm-12"  style="   display:none; font-style: italic;">
			<div class="" style="    padding: 20px;">
				<div class="row" >
					<h3><strong style="color: #827d7d;">Final Check out</strong></h3>
				</div>
			
				<form class="form-horizontal buyproductform" id="buyproductform" method="post">
					
				
					<div  style="color: #827d7d;font-size: 16px; text-align: right; padding: 15px; border-bottom: 1px solid #eee;">
						<div class="row">
							<div class="col-sm-8">
								<div   class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">TID</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="TID" name="TID" placeholder="TID" value="tid">
									</div>
								</div>
								
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">order_id<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="order_id" name="order_id" placeholder="order_id" value="order_id">
									</div>
								</div>
								
							
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">redirect_url<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="redirect_url" name="redirect_url" placeholder="redirect_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerservice">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">cancel_url<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="cancel_url" name="cancel_url" placeholder="cancel_url" value="<?php echo base_url();?>welcome/ccavResponseHandlerservice">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">language<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="language" name="language" placeholder="language" value="EN">
									</div>
								</div>
								<div class="form-group ">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Name<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" disabled class="form-control" id="billing_name" name="billing_name" placeholder="billing name" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Address<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="billing_address" name="billing_address" placeholder="billing address" value="mayasastrology">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing City<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="billing_city" name="billing_city" placeholder="billing city" value="mayasastrology">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing State<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="billing_state" name="billing_state" placeholder="billing state" value="mayasastrology">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Zip<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="number" class="form-control" id="billing_zip" name="billing_zip" placeholder="billing zip" value="111111">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Country<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="billing_country" name="billing_country" placeholder="billing country" value="India">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Tel<span class="required">*</span></label>
									<div class="col-sm-6">
										<input type="number" class="form-control" id="billing_tel" name="billing_tel" placeholder="billing tel" value="9829265640">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Billing Email<span class="required">*</span></label>
									<div class="col-sm-6">
										<input disabled type="email" class="form-control" id="billing_email" name="billing_email" placeholder="billing email" value="">
									</div>
								</div>
								
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">merchant_param1</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="merchant_param1" name="merchant_param1" placeholder="additional Info." value="additional Info.">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">merchant_param2</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="merchant_param2" name="merchant_param2" placeholder="additional Info." value="additional Info.">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">merchant_param3</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="merchant_param3" name="merchant_param3" placeholder="additional Info." value="additional Info.">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">merchant_param4</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="merchant_param4" name="merchant_param4" placeholder="additional Info." value="additional Info.">
									</div>
								</div>
								<div  class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">merchant_param5</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="merchant_param5" name="merchant_param5" placeholder="additional Info." value="additional Info.">
									</div>
								</div>
									<div  class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Amount<span class="required">*</span></label>
									<div class="col-sm-6">
										<input disabled type="text" class="form-control" id="amount" name="amount" placeholder="amount" discount="0" coupan="" d_price ="0" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Currency<span class="required">*</span></label>
									<div class="col-sm-6">
									
									<select name="currency" id="currency" class="form-control "  >
										<option value="" name="">Select Currency</option>
										<option value="USD" name="">USD</option>
										<option selected value="INR" name="">INR</option>
									</select>
									</div>
								</div>
								<div class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">Promo Code</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="promo_code" name="promo_code" placeholder="promo code" value="">
									</div>
								</div>
								<div class="form-group hide">
									<label for="inputEmail3" class="col-sm-3 control-label">Customer Identifier</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="customer_identifier" name="customer_identifier" placeholder="customer identifier" value="">
									</div>
								</div>
								
								<div class="form-center" style="text-align: center;">
									<button type="submit" id="Process_Payment"  class="submit-btn btn btn-primary " style="cursor: pointer !important;">Process Payment</button>	
								</div>
													
							
							</div>
							<div class="col-sm-4" style="text-align: center; border: 2px solid #bcbcd0;height:200px;background-color: aliceblue;">
								<h4 style="color: #9ba588;padding-top: 35px;">Customer support </h4>
								<h4 style="color: #9ba588;">( for online Payment Process) </h4>
								<h4 style="color: #9ba588;">+91-98292-65640 </h4>
								<h5 style="color: #9ba588;">10am to 8pm (Indian Time) </h5>
							
							</div>				
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>

<div style="display:none;">
<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 

<input type='text' name=encRequest id="encRequest" value="">
 <input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
<button  id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
</form>
<button id="rzp-button1">Pay</button>
</div>

<style>
label.error{    color: red;
    text-align: left;
    font-size: 12px;}
</style>
<script>
$('document').ready(function(){
	setTimeout(function() {
	$('#Process_Payment').trigger('click');
	
	}, 1000);
	
	setTimeout(function() {
		$('.btn_home').removeClass('display_none');
	
	}, 30000);
	
//-----------------------------------------------------------------------
    /* 
     * consultation_form validation
     */
	 $('#buyproductform').validate({
		 rules: {
			
			billing_name: {
                required: true
            },
			
			billing_address: {
                required: true
            },
			billing_city: {
                required: true
            },
			billing_state: {
                required: true
            },
			billing_zip: {
                required: true
            },
			billing_country: {
                required: true
            },
			billing_tel: {
                required: true
            },
			billing_email: {
                required: true
            },
			amount: {
                required: true
            },
		},
		 messages: {
			
			 billing_name: {
                required: "Name is required"
            },
			 
			billing_address: {
                required: "Address  is required"
            },
			billing_city: {
                required: "City is required"
            },
			billing_state: {
                required: "State is required"
            },
			billing_zip: {
                required: "Zip is required"
            },
			billing_country: {
                required: "Country is required"
            },
			billing_tel: {
                required: "Contect No is required"
            },
			billing_email: {
                required: "Email is required"
            },
			amount: {
                required: "Amount is required"
            },
			
			
			
		},
		 submitHandler: function (form) {
	 


            var TID = $('#TID').val();
            var order_id = $('#order_id').val();
            var redirect_url = $('#redirect_url').val();
            var cancel_url = $('#cancel_url').val();
            var language = $('#language').val();
            var billing_name = $('#billing_name').val();
            var billing_address = $('#billing_address').val();
            var billing_city = $('#billing_city').val();
            var billing_state = $('#billing_state').val();
            var billing_zip = $('#billing_zip').val();
            var billing_country = $('#billing_country').val();
            var billing_tel = $('#billing_tel').val();
            var billing_email = $('#billing_email').val();
			
            var delivery_name = $('#billing_name').val();
            var delivery_address = $('#billing_address').val();
            var delivery_city = $('#billing_city').val();
            var delivery_state = $('#billing_state').val();
            var delivery_zip = $('#billing_zip').val();
            var delivery_country = $('#billing_country').val();
            var delivery_tel = $('#billing_tel').val();
            var delivery_email = $('#delivery_email').val();
            var merchant_param1 = $('#merchant_param1').val();
            var merchant_param2 = $('#merchant_param2').val();
            var merchant_param3 = $('#merchant_param3').val();
            var merchant_param4 = $('#merchant_param4').val();
            var merchant_param5 = $('#merchant_param5').val();
            var amount = $('#amount').val();
            var discount = $('#amount').attr('discount');
            var coupan = $('#amount').attr('coupan');
            var d_price = $('#amount').attr('d_price');
            var promo_code = $('#promo_code').val();
            var amount = $('#amount').val();
            var customer_identifier = $('#customer_identifier').val();
            var currency = $('#currency').val();
           
           
       var merchant_data= [];

			merchant_data.push({
				
					TID : TID,
					order_id: order_id,
					redirect_url: redirect_url,
					cancel_url: cancel_url,
					language: language,
					billing_name: billing_name,
					billing_address: billing_address,
					billing_city: billing_city,
					billing_state: billing_state,
					billing_zip: billing_zip,
					billing_country: billing_country,
					billing_tel: billing_tel,
					billing_email: billing_email,
					delivery_name: delivery_name,
					delivery_address: delivery_address,
					delivery_city: delivery_city,
					delivery_state: delivery_state,
					delivery_zip: delivery_zip,
					delivery_country: delivery_country,
					delivery_tel: delivery_tel,
					delivery_email: delivery_email,
					merchant_param1: merchant_param1,
					merchant_param2: merchant_param2,
					merchant_param3: merchant_param3,
					merchant_param4: merchant_param4,
					merchant_param5: merchant_param5,
					amount: amount,
					discount: discount,
					coupan: coupan,
					d_price: d_price,
					promo_code: promo_code,
					customer_identifier: customer_identifier,
					currency: currency,
					
			});	
				if(pay_via_in_all_mayas == 'ccavenu'){
					$.post(APP_URL+'welcome/convertdataproductservce',
					{    
					 merchant_data : merchant_data,
							
					},function (response) {
						var data = response.data;
						console.log(data);
						
					$('#encRequest').val(data);

				 $('#finalPayentSubmitBtn').trigger('click');
					
					},'json');	
				}else if(pay_via_in_all_mayas == 'Razor'){
					console.log(merchant_data);
					var seceret_key = 'noRmSfAJJohrz5W5QIkwQLNH';
					var paid_amount = parseFloat(merchant_data[0]['amount']);
					if(parseFloat(merchant_data[0]['discount']) > 0){
					  paid_amount = parseFloat(merchant_data[0]['amount']) - parseFloat(merchant_data[0]['discount']) ;
					}else{
						if(parseFloat(merchant_data[0]['d_price']) > 0){
							paid_amount = parseFloat(merchant_data[0]['d_price']);
						}else{}
					}
					var options = {
						"key": razor_all_key,
						"amount": (paid_amount*100), // 2000 paise = INR 20
						"name": "Maya Astrology",
						"description": "Maya Astrology Paid Consultation",
						"image": "",
						"captured": true,
						"handler": function (response){
							//alert(response.razorpay_payment_id);
							console.log(response);
							$.post(APP_URL+'welcome/paytest', {
								amount: (paid_amount*100),
								payment_id: response.razorpay_payment_id,
								razor_key: razor_all_key,
								seceret_key: seceret_key,
								},
							   function (response2) {
								   merchant_data[0]['razorpay_payment_id'] = response.razorpay_payment_id ;
									localStorage.setItem("rozer_pay_details", JSON.stringify(merchant_data[0]));
									window.location.href = APP_URL+'welcome/razorResponseHandlerservice';
								   
								}, 'json');
							
						},
						"prefill": {
							"name": merchant_data[0]['billing_name'],
							"contact": merchant_data[0]['billing_tel'],
							"email": merchant_data[0]['billing_email'],
						},
						"notes": {
							"address": "Paid Consultation"
						},
						"theme": {
							"color": "#F37254"
						}
					};
					var rzp1 = new Razorpay(options);

					document.getElementById('rzp-button1').onclick = function(e){
						rzp1.open();
						e.preventDefault();
					}
					$('#rzp-button1').trigger('click');
					
				}else{
					var paid_amount = parseFloat(merchant_data[0]['amount']);
					if(parseFloat(merchant_data[0]['discount']) > 0){
					  paid_amount = parseFloat(merchant_data[0]['amount']) - parseFloat(merchant_data[0]['discount']) ;
					}else{
						if(parseFloat(merchant_data[0]['d_price']) > 0){
							paid_amount = parseFloat(merchant_data[0]['d_price']);
						}else{}
					}
					
					localStorage.setItem("papal_pay_details", JSON.stringify(merchant_data[0]));
					window.location.href = APP_URL+'paypal/index?order_id='+merchant_data[0]['order_id']+'&total_amount='+paid_amount+'&currency='+merchant_data[0]['currency']+'&page=service';
				}
		
		 }					
	 });		
		
	


	
	//<--service data-->	
		
import_cookie_into_li3();
	
	/*importing the cookie data in footer li*/
	function import_cookie_into_li3(){
		
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		
	
		
		var cookieArray = document.cookie.split(';');
		var cart_total = 0;
		
		for(var i=0; i<cookieArray.length; i++) {
			var cookieItem = cookieArray[i]; 	
				console.log(cookieItem);	
			//while (cookieItem.charAt(0)=='') 
				cookieItem = cookieItem.substring(1);
			if (cookieItem.indexOf('allcart_orderss_details') == 0){	
				var cookieStr = cookieItem.split('allcart_orderss_details=')[1];
					//console.log("</br>"+cookieStr);
				if(cookieStr==""){/* if cookie is empty*/
					 console.log(1);
					 $('.cart_number').text(0);
					 $('.cart_total_price').text(0);						 
				}else{
					//console.log(JSON.parse(cookieStr)); 
					var obj = JSON.parse(cookieStr);
					 console.log('not empty');
					var keySize = 0;
					for (key in obj) {
						var inputName = obj[key].inputName;
						var inputEmail = obj[key].inputEmail;
						var inputContact = obj[key].inputContact;
						
					}	
						$('#billing_name').val(inputName);					
						$('#billing_email').val(inputEmail);					
						$('#billing_tel').val(inputContact);					
						
				}
			}
		}
		
	}
	
	
	
	
	
	import_cookie_into_li();
	
	/*importing the cookie data in footer li*/
	function import_cookie_into_li(){
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		
		var serviceCost = 0;
		var servicediscount = 0;
		var serviced_price = 0;
		var servicecoupan = '';
		
		
		var cookieArray = document.cookie.split(';');
		var cart_total = 0;
		
		for(var i=0; i<cookieArray.length; i++) {
			var cookieItem = cookieArray[i]; 	
				//console.log(cookieItem);	
			while (cookieItem.charAt(0)==' ') 
				cookieItem = cookieItem.substring(1);
			if (cookieItem.indexOf('cart_order_details') == 0){	
				var cookieStr = cookieItem.split('cart_order_details=')[1];
					console.log("</br>"+cookieStr);
				if(cookieStr=="''"){/* if cookie is empty*/
					 console.log(1);
					 $('.cart_number').text(0);
					 $('.cart_total_price').text(0);						 
				}else{
					//console.log(JSON.parse(cookieStr)); 
					var obj = JSON.parse(cookieStr);
					console.log(obj[0].cost);
					var keySize = 0;
					for (key in obj) {
						
						var service_id = obj[key].service_id;
						var services_name = obj[key].services_name;
						var consultant_type = obj[key].consultant_type;
						var number_of_questions = obj[key].number_of_questions;
						var coupan_code = obj[key].coupan_code;
						var discount = parseFloat(obj[key].coupan_discount);
						var cost = parseFloat(obj[key].cost);
						var d_price = parseFloat(obj[key].d_price);
						var services_image = obj[key].services_image;
						
						serviceCost = parseFloat(cost);
						servicediscount = parseFloat(discount);
						serviced_price = parseFloat(d_price);
						servicecoupan = coupan_code;
					}	console.log('cost = '+cost);
				}
			}
		}
		$('#amount').val(serviceCost);
		$('#amount').attr('discount' ,servicediscount);
		$('#amount').attr('coupan' ,servicecoupan);
		$('#amount').attr('d_price' ,d_price);
		if(d_price >0){
			$('#amount').val(d_price);
		}
	}
	

});
</script>





