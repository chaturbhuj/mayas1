<?php 
$order_id = $this->uri->segment(3);
$token = $this->uri->segment(4);
$payer_id = $this->uri->segment(5); 
$amount = $this->uri->segment(6); 
$currency = $this->uri->segment(7); 
?>
<div id="site-main" style="background-color: #fff;">				
	<div class="page-section v1-one">
		<div class="container">
			<div class="row cart">							
				<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 10px;">
					<div class="row col-md-6" style="border: 1px solid;background-color: #eee;">
						<div class="row">
							<div class="col-md-3 col-sm-6">
							</div>
							<div class="col-md-3 col-sm-6">
								<h3 style="padding: 16px;color: #849EC6;text-align: center;">INVOICE</h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6" style="width: 45%;">		
								<h3 style="">mayasastrology.com</h3>
							</div>
							<div class="col-md-6 col-sm-6" style="width: 45%;">
								<p style="margin-bottom: 6px;line-height: 1;">Date: <span class="date_span"><?php echo date("D M j Y G:i:s"); ?></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Invoice ID: <span class="invoice_id"></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Payment ID: <span class="payment_id"></span></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6" style="width: 50%;">
								<h3 >BILL TO</h3>
								<p style="margin-bottom: 6px;line-height: 1;">Name : <span class="span_name"></span></p>
								<p style="margin-bottom: 6px;line-height: 1;">Email : <span class="span_email"></span></p>
								
								
								<p style="margin-bottom: 6px;line-height: 1;">Contect No:<span class="span_contectno"></span></p>
							</div>
							<div class="col-md-6 col-sm-6" style="width: 50%;">
							
							</div>
						</div>
						<div class="row">
							<table class="shop_cart_table table table-bordered">
								<thead>
									<tr>
										<th class="product-empty">Service Name</th>
										<th class="product-subtotal">Service Charge</th>
									</tr>
								</thead>
								<tbody>												
									<tr>
										
									<tr class="darker-on-hover"><td><span class="span_service"></span></td>
									<td>₹ <span class="span_charge"></span></td>
													
									</tr>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-md-8 col-sm-6" style="width: 70%;">
							
							</div>
							<div class="col-md-4 col-sm-6" style="width: 30%;">
								<table>
									<tbody style="font-size: 16px;">
										<tr>
											<td>Sub Total</td>
											<td> : </td>
											<td style="text-align: right;">₹ <span class="span-subtotal span_charge"></span></td>
										</tr>
										<tr>
											<td>Discount(-)</td>
											<td> : </td>
											<td style="text-align: right;">₹ <span class="span-discount">0%</span></td>
										</tr>
										
										<tr>
											<td>Grand Total : </td>
											<td> : </td>
											<td style="text-align: right;">₹ <strong><span class="span-grandtotal"></span></strong></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="row">
							<p style="text-align: center;">Thank you For  Purchasing the Services</p>
						</div>							
					</div>
					<div class="col-md-2" >
					</div >

					<div class="col-md-4" style="border: 2px solid; height:300px;border-color: bisque;background-color: azure;">
						
						<h3 style="text-align:center;padding-top: 20px;"><strong style="color: blueviolet;">Thank you !<strong></h3>
						<h4>your Payemt  was successful.</h4>
						<h4>(Information also send on Email)</h4>
						<h4>Payment summery : <span class="span_service1"></span> of ₹ <span class="span-grandtotal"></span> </h4>
						<h4>Consultation Access code :  <span class="invoice_id"></span> </h4>
						
						<h4>Call on:+91-9829265640 (11am to 8pm) To Fix your Appointment time as per your Convenience.</h4>
					
					
					</div>
				</div>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- .page-section -->				
</div>
	<!-- .page-section
<button id="generate_pdf" class="btn btn-default btn-lg button-class display_none">download Invoice</button> -->	
<div id="editor"></div>	
<style>
.cart table.cart .product-name > *{
    display: inline;    font-size: 20px !important;color: #95c03e;
}
.product-subtotal .fa-gbp{
	font-size: 20px !important;color: #95c03e;
}
.cart_totals_inner .fa-gbp, .cart_totals_inner span.amount{
	color: #000;	
}
.shop_table{
	background-color: #fff;
}
tbody{
    font-size: 20px;
}
</style>
<script>
$('document').ready(function(){
var data_murchant = JSON.parse(localStorage.getItem("papal_pay_details")) ;
console.log(data_murchant);
var invoice_id = '<?php echo $token ?>';
$('.payment_id').text(data_murchant.order_id);
$('.invoice_id').text(invoice_id);
$('.span_name').text(data_murchant.billing_name);
$('.span_email').text(data_murchant.billing_email);
$('.span_contectno').text(data_murchant.billing_tel);
$('.span_charge').text(data_murchant.amount);

var diescount = 0;
if(parseFloat(data_murchant.discount) > 0){
	diescount = data_murchant.discount ;
}else{
	if(parseFloat(data_murchant.d_price) > 0){
		diescount = parseFloat(data_murchant.amount) - parseFloat(data_murchant.d_price) ;
	}
}
$('.span-discount').text(diescount);
$('.span-grandtotal').text((parseFloat(data_murchant.amount) - diescount));


	var sercice_detail 	=[];	
	import_cookie_into_li1();
	
//var sercice_detail_insatance 	=[];	
	//import_cookie_into_li1();
	var service_id=0;
console.log('11');
	function import_cookie_into_li1(){
		console.log('33');
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		
	
		
		var cookieArray = document.cookie.split(';');
		var cart_total = 0;
		
		for(var i=0; i<cookieArray.length; i++) {
			var cookieItem = cookieArray[i]; 	
				//console.log(cookieItem);	
			while (cookieItem.charAt(0)==' ') 
				cookieItem = cookieItem.substring(1);
			if (cookieItem.indexOf('allcart_orderss_details') == 0){	
				var cookieStr = cookieItem.split('allcart_orderss_details=')[1];
					console.log("</br>"+cookieStr);
				if(cookieStr=="''"){/* if cookie is empty*/
					 console.log('1');
					 console.log(1);
					// $('.cart_number').text(0);
					// $('.cart_total_price').text(0);						 
				}else{ console.log('2');
					console.log(JSON.parse(cookieStr)); 
					var obj = JSON.parse(cookieStr);
					
					var keySize = 0;
					for (key in obj) {
						
						
						//console.log($('#consultantType').val());
						var consultantType = obj[key].consultantType;
						
						
						var inputName = obj[key].inputName;
						var inputEmail = obj[key].inputEmail;
						var inputGender = obj[key].inputGender;
						var inputDateDate = obj[key].inputDateDate;
						var inputTimeMin = obj[key].inputTimeMin;
						var birthCountry = obj[key].birthCountry;
						var birthState = obj[key].birthState;
						var birthCity = obj[key].birthCity;
						
						
						
						var educationQualification = obj[key].educationQualification;
						var currentProfession = obj[key].currentProfession;
						var partnerName = obj[key].partnerName;
						var inputContact = obj[key].inputContact;
						var partnerDateDate = obj[key].partnerDateDate;
						var partnerTimeMin = obj[key].partnerTimeMin;
						var partnerBirthCountry = obj[key].partnerBirthCountry;
						var partnerBirthState = obj[key].partnerBirthState;
						var partnerBirthCity = obj[key].partnerBirthCity;
						var inputQuestion = obj[key].inputQuestion;
						
						
						
					
						
						
					}	$('.span_name').text(inputName);
						$('.span_contectno').text(inputContact);
						$('.span_email').text(inputEmail);
						
						sercice_detail.push({				
							consultantType: consultantType,
							
							
							educationQualification: educationQualification,
							currentProfession: currentProfession,
							partnerName: partnerName,
							partnerDateDate: partnerDateDate,
							partnerTimeMin: partnerTimeMin,
							partnerBirthCountry: partnerBirthCountry,
							partnerBirthState: partnerBirthState,
							partnerBirthCity: partnerBirthCity,
							inputQuestion: inputQuestion,
							inputName: inputName,
							inputGender: inputGender,
							inputDateDate: inputDateDate,
							inputTimeMin: inputTimeMin,
							birthCountry: birthCountry,
							birthState: birthState,
							birthCity: birthCity,
							inputEmail: inputEmail,
							inputContact: inputContact,
							
							
							//amount: amount,
							//questiontime: questiontime,
							//questionyaminute: questionyaminute,
								
									
						});
				}
			}
		}
		
	}
	

	console.log(sercice_detail);
	
	
	
	import_cookie_into_li();
	
	/*importing the cookie data in footer li*/
	function import_cookie_into_li(){
		var d = new Date();
		d.setTime(d.getTime() + (10*24*60*60*1000)); /* 10 days */
		var expires = "expires=" + d.toGMTString();		
		
		
		var serviceCost = 0;
		
		
		var cookieArray = document.cookie.split(';');
		var cart_total = 0;
		
		for(var i=0; i<cookieArray.length; i++) {
			var cookieItem = cookieArray[i]; 	
				//console.log(cookieItem);	
			while (cookieItem.charAt(0)==' ') 
				cookieItem = cookieItem.substring(1);
			if (cookieItem.indexOf('cart_order_details') == 0){	
				var cookieStr = cookieItem.split('cart_order_details=')[1];
					console.log("</br>"+cookieStr);
				if(cookieStr=="''"){/* if cookie is empty*/
					 console.log(1);
					 $('.cart_number').text(0);
					 $('.cart_total_price').text(0);						 
				}else{
					console.log(JSON.parse(cookieStr)); 
					var obj = JSON.parse(cookieStr);
					console.log(obj[0].cost);
					var keySize = 0;
					for (key in obj) {
						
						 service_id = obj[key].service_id;
						var services_name = obj[key].services_name;
						var consultant_type = obj[key].consultant_type;
						var number_of_questions = obj[key].number_of_questions;
						var cost = obj[key].cost;
						var services_image = obj[key].services_image;
						console.log('cost = '+cost);
						serviceCost = parseFloat(cost);
					}
					
				}
			}
		}
	//	$('.span_charge').text(serviceCost);
		//$('.span-grandtotal').text(serviceCost);
		//$('.span-subtotal').text(serviceCost);
		
	}
	
	
		$.post(APP_URL+'welcome/get_services_name_id',
				{ service_id : service_id,
				},
		function (response) {
				var data = response.data;
				
					$('.span_service').text(data);
					$('.span_service1').text(data);
			
	
	
				var date  = $('.date_span').text();
				var order_id  = invoice_id;
				var payment_id  = data_murchant.order_id;
				var name  = data_murchant.billing_name;
				var address  = 'q';
				var city  = 'q';
				var state  = 'q';
				var country  = 'q';
				var currency = data_murchant.currency ;
				var payment_method = 'paypal' ;
				var contectno  = data_murchant.billing_tel;
				var servicename  = data ;//$('.span_service').text();
				var servicecost  = data_murchant.amount;
				var subtotal  = data_murchant.amount;
				var discount  = diescount;
				var grandtotal  = parseFloat(data_murchant.amount) - diescount;
	
			$.post(APP_URL+'welcome/service_order_buy_data',
				{    
				 date : date,
				 order_id : order_id,
				 payment_id : payment_id,
				 service_id : service_id,
				 address : address,
				 city : city,
				 state : state,
				 country : country,
				  payment_method : payment_method,
				 currency : currency,
				 contectno : contectno,
				 servicename : servicename,
				 servicecost : servicecost,
				 subtotal : subtotal,
				 discount : discount,
				 grandtotal : grandtotal,
				 coupan_code : data_murchant.coupan,
				 
				 sercice_detail : sercice_detail,
				// sercice_detail_insatance : sercice_detail_insatance,
						
				},function (response) {
				var data = response.data;
				
				
			},'json');	
	
	
		},'json');	
	
	
	
	
});	
</script>