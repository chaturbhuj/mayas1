<link href="<?php echo base_url();?>assets/css/plugins/uploader.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/plugins/demo.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/plugins/demo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugins/dmuploader.min.js"></script>
<!--<script type="text/javascript" src="https://js.stripe.com/v2/"></script>-->


<style>
.card{margin-bottom: 20px;
    border-bottom: 1px solid rgba(0,0,0,.125) !important;}
.error,.required{
	color:red;
	
}	
.add_button{
	margin:5px;
	
	padding: 15px;
}
.cards_show {
   
    border-radius: 7px;
    padding: 21px 14px 7px 14px;
    font-size: 16px;
  
    height: 60px;
}
.accordion>.card>.card-header {
    /* border-radius: 0; */
    margin-bottom: -1px;
}
.card-header {
    padding: .75rem 1.25rem;
    margin-bottom: 0;
    background-color: rgb(251 248 248 / 3%);
    border-bottom: 1px solid rgba(0,0,0,.125);
}
.card-header:first-child {
    border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
}
.accordion>.card {
    overflow: hidden;
}
.card {
    margin-bottom: 20px;
    border-bottom: 1px solid rgba(0,0,0,.125) !important;
}

element.style {
}
.accordion>.card:not(:last-of-type) {
    border-bottom: 0;
    border-bottom-right-radius: 0;
    border-bottom-left-radius: 0;
}
.accordion>.card {
    overflow: hidden;
}
.card {
    margin-bottom: 20px;
    border-bottom: 1px solid rgba(0,0,0,.125) !important;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.form-group {
    margin-bottom: 1rem;
}


label {
    display: inline-block;
    margin-bottom: .5rem;
}
.btn-link {
    font-weight: 400;
    color: #007bff;
    text-decoration: none;
	border:none;
}
.input_buttons{
	background: #FFFFFF 0% 0% no-repeat padding-box;
	box-shadow: 0px 4px 6px #00000029;
	border: 1px solid #EFEFEF;
	border-radius: 10px;
	opacity: 1;
}
.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: -0.5;
}
.modal-backdrop {
   position: inherit; 
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;
    background-color: #000;
}

</style>
 <?php
 ?>
    <!-- ::::::  Start  Main Container Section  ::::::  -->
<main id="main-container" class="main-container">
    <div class="container">
		<div class="row">
			<div class="col-md-12 m-b-20">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb d-none">
					  <li class="breadcrumb-item"><a href="#">Home</a></li>
					  <li class="breadcrumb-item active" aria-current="page"> edit profile </li>
					</ol>
				</nav> 
			</div>   
		</div>
                <!-- Start Product Details Gallery -->
	<div class="col-12">              
		<div class="row">
			<div class="col-md-12">
				<div style="margin-bottom:40px;margin-top:50px" class="customer_left_box">
					<div class="tab-content" id="myTabContent">
						<div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
							<div class="accordion" id="accordionExample">
								<div class="card">
									<div class="card-header" id="headingOne">
										<h2 class="mb-0">
											<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												Change Contact Information
											</button>
										</h2>
									</div>

									<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
										<div class="card-body">
											<div id="headMsg_edit_profile"></div>
											<form id="customer_edit_profile">
												<div class="row">
													<div class="form-group col-md-3">
														<label for="first_name">Upload Profile </label>
														<i class="fa fa-upload" data-toggle="modal" data-target="#browseImage"></i>
														<div id="image-preview">
															<img style="" src="" alt="" class="rounded-circle rounded mx-auto d-block w-100">
                                                        </div>
													</div>
													<div class="col-md-9">
														<div class="row">
															<div class="form-group col-md-6">
																<label for="first_name">First Name  <span class="required">*</span></label>
																<input type="text" class="form-control" name="first_name" id="first_names" placeholder="First Name" value="">
															</div>
															<div class="form-group col-md-6">
																<label for="last_name">Last Name <span class="required">*</span></label>
																<input type="text" class="form-control" name="last_name" id="last_names" placeholder="Last Name" value="">
															</div>
														</div>
														<div class="row">
															<div class="form-group col-md-6">
																<label for="country">Country <span class="required">*</span></label>
																<input type="text" class="form-control" name="country_name" id="country_name" placeholder="country name" value="">
															</div>
															<div class="form-group col-md-6">
																<label for="state_name">State <span class="required">*</span></label>
																<input type="text" class="form-control" name="state_name" id="state_name" placeholder="state name" value="">
															</div>
															<div class="form-group col-md-6">
																<label for="city_name">City <span class="required">*</span></label>
																<input type="text" class="form-control" name="city_name" id="city_name" placeholder="city name" value="">
															</div>
															<div class="form-group col-md-6">
																<label for="postal_code">Postal Code <span class="required">*</span></label>
																<input type="number" class="form-control" name="postal_code" id="postal_code" placeholder="Postal Code" autocomplete="off">
															</div>
															<div class="form-group col-md-6">
																<label for="address">Address <span class="required">*</span></label>
																<input type="text" class="form-control" name="address" id="address" placeholder="Address" autocomplete="off" value="">
															</div>
															<div class="form-group col-md-6">
																<label for="number">Phone no.</label>
																<input type="number" class="form-control" name="phone_num" id="phone_num" placeholder="phone number" autocomplete="off" value="">
															</div>
															<div class="col-md-12 text-center">
																<button type="submit" class="btn btn-warning w-50">Save</button>
															</div>
														</div>
													</div><!--first row-->
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="card">
									<div class="card-header" id="headingThree">
										<h2 class="mb-0">
											<button class="btn btn-link" type="button" data-toggle="modal" data-target="#changepasswordsModal" aria-expanded="false">
											Change Password
											</button>
										</h2>
									</div>
								</div>
								<div class="card headingfive">
									<div class="card-header" id="headingfive">
										<h2 class="mb-0" >
											<a  href="<?= base_url().'shipping_address'; ?>" class="btn btn-link"style="color:#007bff !important;" >
												Shipping Address
											</a>
										</h2>
									</div>

									<div id="collapsefive" class="collapse " aria-labelledby="headingfive" data-parent="#accordionExample">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main> <!-- ::::::  End  Main Container Section  ::::::  -->
<!-- Modal -->



	
	<!---------------------------- Modal for Browse Image-------------------------->
	<div class="modal fade" id="browseImage" tabindex="-1" course_package="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="background-color: #f5f5f5;">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3>Browse Image</h3>
					
				</div>
				<div class="modal-body">
					<div id="head1_msg"></div>
					<input type="hidden" value="profile" name="image_cat" class="image_cat">
					<input type="hidden" value="" name="sub_folder_name" class="sub_folder_name">
					<div class="row demo-columns">
						<div class="col-md-12 offset-md-2">
							<p>Image type should be GIF,JPG,PNG</p>
							<p>Image should be 2 MB or smaller</p>
							<!-- D&D Zone-->
							<div id="drag-and-drop-zone" class="uploader">
								<div>Drag &amp; Drop Images Here</div>
								<div class="or">-or-</div>
								<div class="browser">
									<label>
										<span>Click to open the file Browser</span>
										<input type="file" name="files[]" multiple="multiple" title='Click to add Files'>
									</label>
								</div>
							</div>
							<!-- /D&D Zone -->
							
						</div>
						<div class="clearfix"></div>
						<!-- / Left column -->
						<div class="col-md-12 offset-md-2">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Uploads</h3>
								</div>
								<div class="panel-body demo-panel-files minHeight" id='demo-files'>
									<span class="demo-note">No Files have been selected/droped yet...</span>
								</div>
							</div>
						</div>
						<!-- / Right column -->
					</div>
				</div><!-- / modal-body -->
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Button trigger modal -->
	
<!-- change password Modal -->
<div class="modal fade" id="changepasswordsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Change Password </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div id="headMsg_change_password"></div>
				<form id="customer_password_change_form">
					<div class="form-group">
						<label for="currentpassword">Current Password<span class="required">*</span></label>
						<input type="password" class="form-control" name="currentpassword" id="currentpassword" placeholder="Current Password">
					</div>
					<div class="form-group">
						<label for="newpassword">New Password<span class="required">*</span></label>
						<input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
					</div>
					<div class="form-group">
						<label for="retypepassword">Confirm Password<span class="required">*</span></label>
						<input type="password" class="form-control" name="retypepassword" id="retypepassword" placeholder="Confirm Password">
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-warning w-50 text-center">Save</button>
					</div>
				</form>									  
			</div>
		</div>
	</div>
</div>

								  
<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id')?>">	
<script>
    $(document).ready(function() {
		fatch_profile_data();
        function fatch_profile_data(){ 
			var customer_id=$('#customer_id').val();
			$.post(APP_URL+'api/customer_account/fatch_profile',{
				customer_id: customer_id,
			},function(response){
				if (response.status == 200) {
					var result=response.data[0];
					console.log(result);
					$('#customer_email').val(result.customer_email);
					$('#first_names').val(result.first_name);
					$('#last_names').val(result.last_name);
					$('#country_name').val(result.country_name);
					$('#state_name').val(result.state_name);
					$('#city_name').val(result.city_name);
					$('#postal_code').val(result.postal_code);
                    $('#address').val(result.address);
                    $('#phone_num').val(result.phone_no); 
                   // $('#image-preview').val(result.customer_img);
					$('#image-preview').find('img').attr('src',APP_URL+'uploads/'+result.customer_img);
								
				}
			},'json');
		}
		
		$('#customer_edit_profile').validate({
			ignore: [],
			rules: {
				first_name: {
					required: true,
				},
				last_name: {
					required: true,
				},
				country_name: {
					required: true,
				},
				state_name: {
					required: true,
				},
				city_name: {
					required: true,
				},
				postal_code: {
					required: true,
				},
				address: {
					required: true,
				},
				address_line: {
					required: true,
				},
			},
			messages: {
				first_name: {
					required: "First name is required"
				},
				last_name: {
					required: "Last name is required",
				},
				country_name: {
					required: "Country name is required",
				},
				state_name: {
					required: "State name is required",
				},
				city_name: {
					required: "City name is required",
				},
				postal_code: {
					required: "Postal code is required",
				},
				address: {
					required: "Address is required",
				},
				address_line: {
					required: "Address is required",
				},
			},
			submitHandler: function (form) {
				/*$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
					css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
					overlayCSS:  { cursor:'wait'} 
				});*/
				var customer_id=$('#customer_id').val();	
				var first_name = $('#first_names').val();
				var last_name = $('#last_names').val();
				var country_name = $('#country_name').val();
				var state_name = $('#state_name').val();
				var city_name = $('#city_name').val();
				var postal_code = $('#postal_code').val();
				var address = $('#address').val();
				var phone_num = $('#phone_num').val();
				
				$.post(APP_URL+'api/customer_account/update_profile',{
					customer_id: customer_id,
					first_name: first_name,
					last_name: last_name,					
					country_name: country_name,					
					state_name: state_name,					
					city_name: city_name,
					postal_code: postal_code,
					address: address,
					phone_num: phone_num,
					
				},function(response){
					$.unblockUI();
					$("html, body").animate({scrollTop: 0}, "slow");
					$('#headMsg_edit_profile').empty();
					if (response.status == 200) {
						var message = response.message;
						$('#headMsg_edit_profile').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#headMsg_edit_profile").fadeTo(2000, 500).slideUp(500, function(){
							$('#headMsg_edit_profile').empty();
							window.location.href = APP_URL+"account/profile";
							//window.location.href = "api/customer_account/update_profile";
						});
					} else if (response.status == 201) {
						$('#headMsg_edit_profile').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				},'json');
				return  false;
			}
		});
		//for email update and check

	$('#customer_change_email').validate({
		ignore: [],
		rules: {
			current_email: {
				required: true,
				remote:{
					url: APP_URL + "api/customer_account/check_customer_old_email",
					type: "post",
					data: {
						param: function(){ return $("#customer_id").val(); },
					}
				}
			
			},
			new_email: {
				required: true,
			},
			confirm_new_email: {
				required: true,
				equalTo: '#new_email',
			},
		},
		messages: {
			current_email: {
				required: "Current email is required",
				remote: "Current email is incorrect! please try again.",
			},
			new_email: {
				required: "New email is required",
			},
			confirm_new_email: {
				required: "Confirm email is required",
				equalto : "Email does not match."
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var new_email = $('#new_email').val();
			var customer_id=$('#customer_id').val();
			$.post(APP_URL+'api/customer_account/update_email',{
				customer_id: customer_id,
				new_email: new_email,
			},function(response){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#headMsg_change_email').empty();
				if (response.status == 200) {
					
					$("#exampleModal7").modal('show');
				  
					var message = response.message;
					$('#headMsg_change_email').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg_change_email").fadeTo(2000, 500).slideUp(500, function(){
					$('#headMsg_change_email').empty();
						//window.location.reload();
					});
				} else if (response.status == 201) {
					$('#headMsg_change_email').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			return  false;
		}
	});
		//popup
	$('#checking_submit_email').validate({
		ignore: [],
		rules: {
			checking_random_no: {
				required: true,
			},
			
		},
		messages: {
			checking_random_no: {
				required: "verification code is required",
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var checking_random_no = $('#checking_random_no').val();
			var customer_id=$('#customer_id').val();
			var new_email=$('#new_email').val();
			$.post(APP_URL+'api/customer_account/checking_valid_email',{
				customer_id: customer_id,
				new_email: new_email,
				checking_random_no: checking_random_no,
			},function(response){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#headMsg_verification_email').empty();
				if (response.status == 200) {
										  
					var message = response.message;
					$('#headMsg_verification_email').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg_verification_email").fadeTo(2000, 500).slideUp(500, function(){
					$('#headMsg_verification_email').empty();
						window.location.reload();
					});
				} else if (response.status == 201) {
					$('#headMsg_verification_email').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			return  false;
		}
	});

	$('#customer_password_change_form').validate({
		ignore: [],
		rules: {
			currentpassword: {
				required: true,
				remote:{
					url: APP_URL + "api/customer_account/check_customer_old_password",
					type: "post",
					data: {
						param: function(){ return $("#customer_id").val(); },
					}
				}
			},
			newpassword: {
				required: true,
			},
			retypepassword: {
				required: true,
				equalTo: '#newpassword',
			},
		},
		messages: {
			currentpassword: {
				required: "Current password is required",
				remote: "Current password is incorrect! please try again.",
			},
			newpassword: {
				required: "New password is required",
			},
			retypepassword: {
				required: "Confirm password is required",
				equalto : "Passwords does not match."
			},
		},
		submitHandler: function (form) {
			$.blockUI({ message: '<img src="'+APP_URL+'assets/images/uiblock_loading_image1.gif" />' ,
				css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
				  overlayCSS:  { cursor:'wait'} 
				});
			var newpassword = $('#newpassword').val();
			var customer_id=$('#customer_id').val();
			$.post(APP_URL+'api/customer_account/update_password',{
				customer_id: customer_id,
				new_password: newpassword,
			},function(response){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#headMsg_change_password').empty();
				if (response.status == 200) {
					

					$('#currentpassword').val('');
					$('#newpassword').val('');
					$('#retypepassword').val('');
					
			
					var message = response.message;
					$('#headMsg_change_password').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg_change_password").fadeTo(2000, 500).slideUp(500, function(){
						$('#headMsg_change_password').empty();
						window.location.reload();
						//$('#changepasswordsModal').modal('hide');
					});
				} else if (response.status == 201) {
					$('#headMsg_change_password').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
			},'json');
			return  false;
		}
	});
   
	/*uploading drag and drop image*/
	$('#drag-and-drop-zone').dmUploader({
		url: APP_URL+'api/customer_account/upload_profile_image',
		dataType: 'json',
		extraData: {
		  image_cat:$('.image_cat').val(),
		  sub_folder_name:$('.sub_folder_name').val(),
		  customer_id:$('#customer_id').val(),
		},
		maxFiles :'1',
		fileName : 'myFile',
		//maxFileSize :'1',
		allowedTypes: 'image/*',
		/*extFilter: 'jpg;png;gif',*/
		onInit: function(){
		  $.danidemo.addLog('#demo-debug', 'default', 'Plugin initialized correctly');
		},
		onBeforeUpload: function(id){
		  
		  $.danidemo.addLog('#demo-debug', 'default', 'Starting the upload of #' + id);

		  $.danidemo.updateFileStatus(id, 'default', 'Uploading...');
		},
		onNewFile: function(id, file){
			$('.demo-panel-files').empty();
			$.danidemo.addFile('#demo-files', id, file);
		},
		onComplete: function(){
		  
		  $.danidemo.addLog('#demo-debug', 'default', 'All pending tranfers completed');
		},
		onUploadProgress: function(id, percent){
		  var percentStr = percent + '%';

		  $.danidemo.updateFileProgress(id, percentStr);
		},
		onUploadSuccess: function(id, data){
			var status= data.status;
			var filename_ = data.filename;
			if(status == 200){
				$('#image-preview').find('img').removeClass('display_none');
				$('#image-preview').find('img').attr('src',APP_URL+'uploads/'+filename_);
				
				$('#browseImage').modal('hide');
			}else{
				$('#browseImage').modal('hide');
				alert(data.message);
			}
		  $.danidemo.addLog('#demo-debug', 'success', 'Upload of file #' + id + ' completed');

		  $.danidemo.addLog('#demo-debug', 'info', 'Server Response for file #' + id + ': ' + JSON.stringify(data));

		  $.danidemo.updateFileStatus(id, 'success', 'Upload Complete');

		  $.danidemo.updateFileProgress(id, '100%');
		},
		onUploadError: function(id, message){
			alert(message);
		  $.danidemo.updateFileStatus(id, 'error', message);

		  $.danidemo.addLog('#demo-debug', 'error', 'Failed to Upload file #' + id + ': ' + message);
		},
		onFileTypeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: must be an image');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: must be an image');
		},
		onFileSizeError: function(file){
			alert('File \'' + file.name + '\' cannot be added: size excess limit');
			$.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' cannot be added: size excess limit');
		},
		/*onFileExtError: function(file){
		  $.danidemo.addLog('#demo-debug', 'error', 'File \'' + file.name + '\' has a Not Allowed Extension');
		},*/
		onFallbackMode: function(message){
			alert('info', 'Browser not supported(do something else here!): ' + message);
		  $.danidemo.addLog('#demo-debug', 'info', 'Browser not supported(do something else here!): ' + message);
		},
		onFilesMaxError: function(file){
			alert(' cannot be added to queue due to upload limits.');
		  $.danidemo.addLog(file.name + ' cannot be added to queue due to upload limits.');
		}
	});

		
	/*get_card_detail();
	function get_card_detail(){
		var user_id=$('#customer_id').val();
		$.post(APP_URL+'api/customer_account/get_card_detail',{
			user_id: user_id,
		},function(response){
			if (response.status == 200) {
				var contents="";
				console.log(response);
				console.log(response.data.length);
				
					var result=response.data;
					var content="";
					$. each(result, function(index, value){
						console.log(value);
						content +='<div class="card-body">';										
							content +='<div class="row">';
								content +='<div class="col-md-10 active_class cards_show">';
								content +='<i class="fas fa-circle"></i>';
								content +='<span class="normal_text"> '+value.cardnumber+'</span>';
								content +='<!--<img src="<?php echo base_url(); ?>assets/img/icon_5e07275ae51ea.png" alt="" style="width: 50px; float:right;">-->';
								content +='<i class="fa fa-trash float-right remove_card" id="'+value.id+'" aria-hidden="true"></i>';
								content +='</div>';						
																
								content +='<div class="col-md-1"></div>';
								content +='<div class="col-md-1"></div>';
																
							content +='</div>';
														
						content +='</div>';
						
				
						$('#add_card_data').html(content);
					});
				
			}
		
			//$('#checkout_city_id_temp').html(city_content);
		},'json');
	}*/
	
	$('body').on('click',".remove_card",function (e) {
	 var id=$(this).attr('id');
       console.log('edit address');
       //console.log(customer_address_id);
		$.post(APP_URL+'api/customer_account/remove_card',{
			id: id,
			
		},function(response){
				$.unblockUI();
				$("html, body").animate({scrollTop: 0}, "slow");
				$('#headMsg_remove_card').empty();
				if (response.status == 200) {			
					var message = response.message;
					$('#headMsg_remove_card').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					$("#headMsg_remove_card").fadeTo(2000, 500).slideUp(500, function(){
						$('#headMsg_remove_card').empty();
						window.location.reload();
					});
				}else if (response.status == 201) {
					$('#headMsg_remove_card').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
				}
	    },'json');
			
    })
	
	
		
});
    </script>