<style type="text/css">

.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: -0.5;
}
.modal-backdrop {
    position: inherit;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1040;
    background-color: #000;
}
.card-body {
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-height: 1px;
    padding: 1.25rem;
}
.card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
}
.page_title {
    background-color: #eee;
    padding: 15px;
}


.breadcrumb {
    margin-top: 25px;
    background: transparent;
    font-size: 11px;
    margin-bottom: 0;
    padding: 0;
}
</style>
<!-- ::::::  Start  Main Container Section  ::::::  -->
   <!-- ::::::  Start  Main Container Section  ::::::  -->
<div class="dashboard-content py-4">
    <main id="main-container" class="main-container">
        <div class="container">
            <div class="row">
				<div style="margin-left: 25px;" class="col-md-12 m-b-20">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb d-none">
							<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page"> Wishlist </li>
						</ol>
					</nav> 
				</div>   
            </div>
                <!-- Start  Wishlist -->
			<div class="col-12">
				<div class="row">
                    
					<div  style="margin-top: 20px; margin-bottom:20px;" class="col-md-12">
						<div class="card card-body account-right">
							<div class="widget">
								<div class="section-header">
									<h5 class="heading-design-h5 page_title">
										Wishlist
									</h5>
									<div id="Wishlist_head_msg"></div>
									<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id') ?>">
								</div>
								<div class="order-list-tabel-main table-responsive">
									<table class="datatabel table table-striped table-bordered order-list-tabel">
										
										<thead>
										<?php 
										if($data_wishlist){
											echo'<tr role="row">
												<th>Image</th>
												<th>Product Name</th>
												<th>Price</th>
												<th>Action</th>
											</tr>';
										}
											?>
										</thead>
										<tbody id="order_data_content">
										<?php 
										if($data_wishlist){
											//var_dump($data_wishlist);
											foreach($data_wishlist as $row){
												$fav_id=$row->fav_id;
												$product_id=$row->product_id;
												
												$product_good_id=$row->product_good_id;
												//var_dump($product_id);
												$prodct_result = $this->db->get_where('mayas_newproduct', array('product_id' => $product_id))->result();
												//var_dump($prodct_result);
												foreach($prodct_result as $value){
													//var_dump($value);
													$product_image=$value->product_images;
													$string_url=$value->string_url;
													$product_name=$value->product_name;
													$product_discounted_prize=$value->product_discounted_prize;
													//var_dump($product_image);
														//$images=json_decode($product_image);
														$images=explode(",",$product_image);
													//var_dump($images);
													
														
										?>
										<tr>
											<td><img alt="" src="<?php echo base_url().'uploads/'.($images[0]);?>" class="img-fluid img-center" style="width:80px;"></td>
											<td><a href="<?php echo base_url().'product/detail/'.$string_url?>"> <?php echo $product_name;?></a></td>
											<td>₹<?php echo $product_discounted_prize;?></td>
											<td style="display: inline-flex; border: none;">
											<a data-toggle="tooltip" data-placement="top" title="" href="<?php  echo base_url().'product/detail/'.$string_url?>" data-original-title="View Detail" class="btn fa fa-eye btn-info btn-sm"><i class="mdi mdi-eye"></i></a>
											&nbsp;&nbsp;
											<a data-toggle="tooltip" data-placement="top" title="remove" href="javascript:void(0)"value="<?php echo $fav_id;?> " data-original-title="Remove item" class="btn fa fa-trash btn-danger btn-sm remove_item"><i class="mdi mdi-close"></i></a>&nbsp;&nbsp;
											<a data-toggle="tooltip" data-placement="top" title="add to cart"   product_id="<?php echo $product_id;?>" product_name="<?php echo $value->product_name; ?>" product_actual_prize="<?php echo $value->product_actual_prize; ?>" product_discounted_prize="<?php echo $value->product_discounted_prize; ?>" product_category="<?php echo $value->product_category; ?>" product_rashi="<?php echo $value->product_rashi; ?>" product_disease="<?php echo $value->product_disease; ?>" product_free_consultation="<?php echo $value->product_free_consultation; ?>" product_paid_consultation="<?php echo $value->product_paid_consultation; ?>" product_short_description="<?php echo $value->product_short_description; ?>" product_long_description="<?php echo $value->product_long_description; ?>" status="<?php echo $value->status; ?>" is_new="<?php echo $value->is_new; ?>" product_meta_title="<?php echo $value->product_meta_title; ?>" product_meta_description="<?php echo $value->product_meta_description; ?>" product_meta_keyword="<?php echo $value->product_meta_keyword; ?>" product_meta_tags="<?php echo $value->product_meta_tags; ?>" product_feature="<?php echo $value->product_feature; ?>" product_height="<?php echo $value->product_height; ?>" product_shape="<?php echo $value->product_shape; ?>" product_color="<?php echo $value->product_color; ?>" product_clarity="<?php echo $value->product_clarity; ?>" product_grade="<?php echo $value->product_grade; ?>" product_weight="<?php echo $value->product_weight; ?>" product_treatment="<?php echo $value->product_treatment; ?>" product_origin="<?php echo $value->product_origin; ?>" product_images="<?php echo $images[0] ?>" href="javascript:void(0)" value="<?php echo $fav_id;?> " data-original-title="add to cart" class="btn fa fa-shopping-cart btn-warning btn-sm open_quntity_modal" style="color:#ffff;"><i class="mdi mdi-cart-outline"></i></a>
											
											 
											
											 
											</td>
										</tr>
										<?php 
														
												}
											}
										}else{
											echo '<p style="text-align:center; padding:10px; border: 1px solid #e0e0e0;"> Not selected any favourite  option </p>';
											
										}
										?>																										                
										</tbody>
									</table>
								</div>
							</div>
							<div class="section-header">
								
								
								<div id="Wishlist_head_msg"></div>
								<input type="hidden" id="customer_id" value="<?php echo $this->session->userdata('customer_id') ?>">
							</div>
							
					</div>
					
				</div>  
			</div>
		</div>
	</main>
</div>
<?php $this->load->view('add_to_cart_view'); ?>
<script type="text/javascript">
$('document').ready(function() {
        $("body").on("click", '.remove_item', function() {
		   console.log('remove');
          var fav_id = $(this).attr('value');

          $.post(APP_URL + 'api/customer_account/wishlist_item_remove', {
                fav_id: fav_id,
             },
             function(response) {
                $("html, body").animate({
                   scrollTop: 0
                }, "slow");
                $('#Wishlist_head_msg').empty();
                if (response.status == 200) {
                   var message = response.message;

                   $('#Wishlist_head_msg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + message + "</strong>&nbsp;&nbsp;</div>");
                   $("#Wishlist_head_msg").fadeTo(2000, 500).slideUp(500, function() {
                      $('#Wishlist_head_msg').remove();
                    window.location.href = APP_URL + 'welcome/wishlist';
                   });

                } else if (response.status == 201) {
                   $('#Wishlist_head_msg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
                   $("#Wishlist_head_msg").fadeTo(2000, 500).slideUp(500, function() {
                      $('#Wishlist_head_msg').empty();
                   });
                }

             }, 'json');
        });
	   
	    $('body').on('click', '.add_a_link', function () {
			$('.proudct_you_need').removeClass('display_none');
		});
		
		$('#form_you_need_product').validate({
			ignore: [],
			rules: {
				product_name: {
					required: true,
				},
				share_your_link: {
					required: true,
				},		
			},
			messages: {
				product_name: {
					required: "product name is required",
				},
				share_your_link: {
					required: "Link name is required",
				},
			},
			submitHandler: function (form) {
				$.blockUI({ message: '<img src="'+APP_URL+'assets/imgages/uiblock_loading_image1.gif" />' ,
					css: { width: '20%', border:'0px solid #FFFFFF', background:'none',cursor:'wait'},
					  overlayCSS:  { cursor:'wait'} 
					});
				var customer_id = $('#customer_id').val();
				var product_name = $('#product_name').val();
				var share_your_link = $('#share_your_link').val();
				
				$.post(APP_URL+'api/customer_account/add_custome_wished_product',{
					customer_id: customer_id,
					product_name: product_name,
					share_your_link: share_your_link,
					
				},function(response){
						//console.log("response");
						//console.log(response);
					//console.log($result);
					$.unblockUI();
					//$("html, body").animate({scrollTop: 0}, "slow");
					$('#checkout_headMsg').empty();
					if (response.status == 200) {
						console.log("1");
						var message = response.message;
						$('#checkout_headMsg').html("<div class='alert alert-success'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
						$("#checkout_headMsg").fadeTo(2000, 500).slideUp(500, function(){
							$('#checkout_headMsg').empty();
							
							
							window.location.href = APP_URL+'customer/configure/wishlist';
						});
					} else if (response.status == 201) {
						$('#checkout_headMsg').html("<div class='alert alert-danger'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" + response.message + "</strong></div>");
					}
				},'json');
				return  false;
			}
		});
	
	   
});
 </script>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_upload',$data);
?>
<?php 
    $data['sub_folder_name']="";
    $data['image_cat']="product";
    $this->load->view('product_image_only_upload',$data);
?>
