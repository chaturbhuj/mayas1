<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<script src="<?php echo base_url();?>assets/js/configure/constant.js"></script>
<script src="<?php echo base_url();?>assets/js/configure/shopcart.js"></script>
<style>
.img_preview {
    width: 70px;
    height: 70px;
}
.td_style{
	font-size: 15px;
    font-weight: 700;
    color: #232323;
    vertical-align: middle !important;
}
.display_none{
	display:none!important;
}
</style>
	<script>	
	$(function () {		
		$(".date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'yy-mm-dd',
			yearRange : 'c-75:c+75',
			//minDate : 'today',
		});
		$('.time').timepicker({
			showPeriodLabels: false,
			rows: 6,
			minutes: {
				starts: 0,
				ends: 59,
				interval: 1, 
				manual: []  
			}
		});

	});

</script>

<script>
	//window.PUM.setData(100, 5665,"sdsd");
</script>
<style>
.form-control{
	    height: 34px !important;
}
/* label.control-label{
	color: #fff !important;
	
}*/
/* .control_div{
	color: #fff !important;
} */
label.error{
	color: red !important;
}
.freeConsultantForm div{
	    margin: 10px 0px !important;
}
.required{
	color: red;
}
.error1{
	color: red !important;
}
.textarea-information-class{
	width:100%;
}
.newFreeConsultantForm .submit-btn{

	width: 27%;
  /* height: 48px;
  padding: 10px;
  margin: 0 19px;
  font-size: 22px;
  background-image: none;
  color: #fff !important; */
  padding: 15px 30px;
    display: inline-block;
    box-shadow: 7px 7px 14px #d8d8d8, -7px -7px 14px #ffffff;
    border-radius: 10px;
    background-color: #F0F0F0 !important;
    letter-spacing: 0.6px;
    color: #231E22;
    border: unset;
}
 .image_div{
	 text-align: center;
	 margin: 15px 0px 15px 0px;
 }
 .img-preview{
	 width: 900px;
	 height: 225px;
 }
 .display_none{
	 display:none !important;
}
</style>		
			<!-- CONTENT AREA -->
            <div class="content-area">

                <!-- BREADCRUMBS -->
                <section class="page-section breadcrumbs">
                    <div class="container">
                        <div class="page-header">
                            <h1>Service Cart</h1>
                        </div>
                        <ul class="breadcrumb">
                            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
                            <li class="active">Service Cart</li>
                        </ul>
                    </div>
                </section>
                <!-- /BREADCRUMBS -->

                <!-- PAGE -->
                <section class="page-section color">
                    <div class="container">
                        <h3 class="block-title alt">Your Service</h3>
                        <div class="row orders">
                            <div class="col-md-8">
                                <table class="table" id="shop_Cart_table">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
											<th>Service</th>
                                           <!---- <th>Consultant Type </th>--->
                                            <th> <?php if($this->uri->segment(3)=='question'){ echo 'Number of Questions'; }else {echo 'Number of Minute';} ?>
											
											</th>
                                            <th>Cost</th>
                                        </tr>
                                    </thead>
                                    <tbody class="appned_data">
										<!--tr will be added here-->
									</tbody>
									
                                </table>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Have a Coupon Code?</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="inputcoupan" name="inputcoupan" placeholder="Enter Coupon Code">
										<div id="coupan_err"></div>
									</div>
									<a href="javascript:void(0);" title="Apply Coupon"><span style="font-size: 21px;" class="label col-sm-2 label-success appl_coupan_code">Apply</span></a>
								</div>
                            </div>
                            <div class="col-sm-4" style="text-align: center; border: 2px solid #bcbcd0;height:200px;background-color: aliceblue;">
								
								<h4 style="color: #9ba588;padding-top: 30px;">Customer support </h4>
								<h4 style="color: #9ba588;">( for online Payment Process) </h4>
								<h4 style="color: #9ba588;">+91-98292-65640 </h4>
								<h5 style="color: #9ba588;">10am to 8pm (Indian Time) </h5>
							
							</div>
                        </div>
							
						
						<h3 style="padding-bottom: 50px;font-size: 30px;color: #7b6763;"><span style="width: 30px; height: 30px; background-color: #678; padding: 2px 12px; border-radius: 50%; color: #fff;">2</span> <strong>Enter Birth Information</strong></h3>
						<div id="content">                      
							<div class="container">
								<!-- modal for free telephonic consultancy -->
								<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" >
								<div id="newFreeConsultantForm1" class="">
									<div class="modal-header">
										<form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
											<div hidden class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Select type of consultation<span class="required">*</span></label>
												<div class="col-sm-9">
													<select name="consultantType" id="consultantType" class="form-control consultantType"  >
														<option value="" name="">Select the type of free consultation</option>
														<option value="Career" name="career">Question Related to Career</option>
														<option value="BirthStone" name="birth_stone">Question Related to Birth Stone</option>
														<option value="HoroscopeQuery"  name="horoscope">Question Related to Horoscope Query</option>
														<option value="MatchMaking"  name="match_making">Question Related to Match Making</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Full Name<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Enter your Full name">
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Gender<span class="required">*</span></label>
												<div class="col-sm-9">
													<select name="inputGender" id="inputGender" class="form-control">
														<option value="">Gender</option>
														<option value="Male">Male</option>
														<option value="Female">Female</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
												<div class="col-sm-3">
													<input type="text" id="inputDateDate" readonly name="inputDateDate" placeholder="Birth Date" class="form-control  date " value="">  
												</div>
												<div class="col-sm-3">
													<input type="text" id="inputTimeMin" readonly name="inputTimeMin" placeholder="Birth Time" class="form-control time " value="">
													
												</div>
												<div class="col-sm-3">
													<label class="error1">Time : 24 hour format</label>
												</div>
											</div>
											
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
												<div class="col-sm-3">
													<input type="text" class="form-control" id="birthCountry" name="birthCountry" placeholder="Enter Birth Country">
												</div>
												<div class="col-sm-3">
													<input type="text" class="form-control" id="birthState" name="birthState" placeholder="Enter your Birth State">
												</div>
												<div class="col-sm-3">
													<input type="text" class="form-control" id="birthCity" name="birthCity" placeholder="Enter your Birth City/Distict">
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Email-id<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="Enter your Email-id">	
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Confirm Email-id<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm" placeholder="Re-enter your Email-id">	
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-3 control-label">Contact No.</label>
												<div class="col-sm-9">
													<input type="number" class="form-control" id="inputContact" name="inputContact" placeholder="Enter your Contact Number (Optional)">
												</div>
											</div>
											<div hidden class="form-group Career">
												<label for="inputEmail3" class="col-sm-3 control-label">Highest Qualification<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="educationQualification" name="educationQualification" placeholder="Enter your Highest Qualification">
												</div>
											</div>
											<div hidden class="form-group Career">
												<label for="inputEmail3" class="col-sm-3 control-label">Current Profession<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" class="form-control" id="currentProfession" name="currentProfession" placeholder="Enter Your Current Profession">
												</div>
											</div>
											<div  class="form-group MatchMaking">
												<div class="control_div">Enter your Partner Infomation here</div>
												<label for="inputEmail3" class="col-sm-3 control-label">Partner Name<span class="required">*</span></label>
												<div class="col-sm-9" >
													<input type="text" style="padding-bottom:10px" class="form-control" id="partnerName" name="partnerName" placeholder="Enter your Partner Full Name">
												</div>
												<label  style="padding-top:10px" for="inputEmail3" class="col-sm-3 control-label">Date And Time Of Birth<span class="required">*</span></label>
												<div class="col-sm-3" style="padding-top:10px">
													<input type="text" id="partnerDateDate" name="partnerDateDate" placeholder="Birth Date" class="form-control  date " value="">  
												</div>
												<div class="col-sm-3" style="padding-top:10px">
													<input type="text" id="partnerTimeMin" name="partnerTimeMin" placeholder="Birth Time" class="form-control time " value="">											
												</div>
												<div class="col-sm-3" style="padding-top:10px">
													<label class="error1">Time : 24 hour format</label>
												</div>
												<div class="clearfix"></div>
												
												<div class="form-group" style="padding-top:10px">
													<label for="inputEmail3" class="col-sm-3 control-label">Place Of Birth<span class="required">*</span></label>
													<div class="col-sm-3">
														<input type="text" class="form-control" id="partnerBirthCountry" name="partnerBirthCountry" placeholder="Enter Partner Birth Country">
													</div>
													<div class="col-sm-3">
														<input type="text" class="form-control" id="partnerBirthState" name="partnerBirthState" placeholder="Enter Partner Birth State">
													</div>
													<div class="col-sm-3">
														<input type="text" class="form-control" id="partnerBirthCity" name="partnerBirthCity" placeholder="Enter Partner Birth City/Distict">
													</div>
												</div>
											</div>									
											<div class="form-group " >
												<label for="inputEmail3" class="col-sm-3 control-label  match display_none">Your Questions <span class="required">*</span></label>
												<label for="inputEmail3 " class="col-sm-3 control-label  notmatch display_none">Questions & Concerns<span class="required">*</span></label>
												<div class="col-sm-9">
													<textarea rows="3" class="textarea-information-class" id="inputQuestion" class="inputQuestion" name="inputQuestion" placeholder=""></textarea>
												</div>
												<p style="color: red;padding-left: 300px;">Do not combine multiple questions into one Question.</p>
											</div>
											<div hidden class="form-group ">
												<label for="inputEmail3" class="col-sm-3 control-label">Other Information</label>
												<div class="col-sm-9">
													<textarea rows="3" class="textarea-information-class"  value="mayasastrology" id="otherInfomation" name="otherInfomation" placeholder="Enter usefull infomation Related to question"></textarea>
												</div>
											</div>
											<div class="form-center" style="text-align:center;">
												<button type="submit" class="submit-btn btn btn-primary"  id="btnSubmit" style="cursor: pointer !important;">Submit</button>										 
											</div>
										</form>
									</div>							
								</div>
						</div>
						</div>                        
					</div>
				</section>
                <!-- /PAGE -->
				<script>
					$('document').ready(function(){
						var d = new Date().getTime();
						$('#TID').val(d);
					});
				</script>
				<script>
					$('document').ready(function(){
						var id = new Date().getTime();
						$('#order_id').val(id);
					});
				</script>
				<div class="container in_payment_process display_none" style="margin-top: 110px; height:600px;">
					<input type="hidden" class="form-control" id="date" name="date" value="<?php echo date("D M j Y G:i:s"); ?>">
					<input type="hidden" class="form-control" id="TID" name="TID" value="tid">
					<input type="hidden" class="form-control" id="order_id" name="order_id" placeholder="order_id" value="order_id">
					<div class="row">
						<h3 style="text-align:center;"> Processing ....</h3>
						<div class="col-md-12 text-center btn_home display_none" style="margin-top:20px;">
							<label style="font-size:20px;" class="label label-success"><a href="<?php echo base_url(); ?>" style="color: #fff;">GO TO HOMEPAGE </a></label>
						</div>
					</div>
					<div style="display:none;">
						<form method="post" name="redirect" action="https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction"> 

							<input type='text' name=encRequest id="encRequest" value="">
							<input type='text' name=access_code value="<?php echo ACCESS_CODE;  ?>">
							<button  id="finalPayentSubmitBtn" class="finalPayentSubmit">Submit</button>
						</form>
						<button id="rzp-button1">Pay</button>
					</div>
				</div>
                
            </div>
            <!-- /CONTENT AREA -->

<script>
$('document').ready(function () {
	
	setTimeout(function() {
		var	services_name=$('#consultantType').val();
		//alert(services_name);
			
			if(services_name=='BirthStone'){ 
				
				$('.match').removeClass('display_none');
				

				$('.textarea-information-class').attr('placeholder','Ask One/Two Precise Question. (as per the consultation selected by you)');
			
			
			}else if(services_name=='HoroscopeQuery'){	
				
				$('.match').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Ask One/Two Precise Question. (as per the consultation selected by you)');
			
			
			}else if(services_name=='Career'){
				
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to your Job/Business/Career.');
			
			
			}else if(services_name=='Gemstone'){
			
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Ask your Questions related to Success in life, Suitable Gemstones, Mantra, Yantra, astrological remedies for you.');
				
				
			}else if(services_name=='Health'){
				
				
				$('.notmatch').removeClass('display_none');
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to your Health & Personality issues, Fear & anxiety or Age.');
		

			}else if(services_name=='MatchMaking'){
				
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to your Married Life, Extramarital affairs, or Children (Birth issue). ');
			
			}else if(services_name=='Love'){
				
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to your Love/Sex Life,Relationship or Compatibility (Matchmaking).');
			}else if(services_name=='Money'){
			
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Describe your Problems, Concerns, and Questions related to Finance, Debt and  Investment Prospects. ');
			}else if(services_name=='Detailed'){	
				
				$('.notmatch').removeClass('display_none');
				
				$('.textarea-information-class').attr('placeholder','Describe your All Problems, Concerns, and Questions related to All Aspects of Your Life or Horoscope .');
			
			}
			
			}, 500);
	});
</script>