<script>
$(function() {
    $(".date").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: 'c-75:c+75',
        //minDate : 'today',
    });
    $('.time').timepicker({
        showPeriodLabels: false,
        rows: 6,
        minutes: {
            starts: 0,
            ends: 59,
            interval: 1,
            manual: []
        }
    });

});
</script>

<script src="<?php echo base_url();?>assets/js/jquery.blockUI.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript">
function recaptchaCallback() {
    $('#btnSubmit').prop('disabled', false);
}
</script>
<style>
.form-control {
    height: 34px !important;
}

/* label.control-label{
	color: #fff !important;
	
}*/
/* .control_div{
	//color: #fff !important;
} */
label.error {
    color: red !important;
}

.freeConsultantForm div {
    margin: 10px 0px !important;
}

.required {
    color: red;
}

.error1 {
    color: red !important;
}

.textarea-information-class {
    width: 100%;
}

#content {
    background: #f5f5f5 !important;
}

.newFreeConsultantForm .submit-btn {
    /* height: 48px; */
    padding: 10px;
    margin: 0 19px;
    font-size: 22px;
    background-image: none;
    /* color: #fff !important; */
    /* padding: 15px 30px; */
    display: inline-block;
    box-shadow: 7px 7px 14px #d8d8d8, -7px -7px 14px #ffffff;
    border-radius: 10px;
    background-color: #F0F0F0 !important;
    letter-spacing: 0.6px;
    color: #231E22;
    border: unset;
}

.image_div {
    text-align: center;
    margin: 15px 0px 15px 0px;
}

.img-preview {
    width: 900px;
    height: 225px;
}

.display_none {
    display: none !important;
}

.form-control {
    font-size: 12px;
    border-radius: 10px;
    height: 52px !important;
}

#newFreeConsultantForm1 {
    /* margin-top: 100px; */
    padding-top: 100px;
    padding-bottom: 200px;
    /* margin-bottom: 200px; */
}

.modal-header {
    border-bottom: unset;
    display: block;
}

.display_block {
    display: flex !important;
}
</style>
<?php
// var_dump($consultation);
if($consultation){
	$consultation_type = $consultation[0]['consultation_type'];
	$faq_category = $consultation[0]['faq_category'];
	$banner_id = $consultation[0]['banner_id'];
	$about_content_id = $consultation[0]['about_content_id'];
}else{
	$consultation_type ='';
	$faq_category = 0;
	$banner_id = 0;
	$about_content_id = 0;

}


?>
<div id="content">

    <?php
            $datacat = array(
            );

            $datacat['banner_id'] = $banner_id;
            $this->load->view('banner',$datacat);
            ?>
    <div class="container">

        <!-- modal for free telephonic consultancy -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" > -->
        <div id="newFreeConsultantForm1" class="row">

            <div class="">
                <div id="err_consultation_div"></div>
                <h3 style="text-align:center;padding-bottom: 10px;font-size:30px"><strong> <span
                            style="text-transform:capitalize; "><?php echo $consultation[0]['consultation_name'];?></span>
                    </strong></h3>
                <div class="row" style="color: #272733; padding: 10px; font-size: 17px;">
                    <?php if($consultation[0]['detail_image_position']=='left'){ ?>
                    <div class="col-sm-5 col-sm-offset-2 pull-right">
                        <img class="w-100" src="<?=base_url().'/uploads/'.$consultation[0]['c_detail_image']?>" alt="">
                    </div>
                    <?php }?>
                    <div class="col-sm-7 col-sm-offset-2">
                        <?php echo $consultation[0]['description']; ?>
                    </div>
                    <?php if($consultation[0]['detail_image_position']=='right'){ ?>
                    <div class="col-sm-5 col-sm-offset-2 pull-right"">
				<img class=" w-100" src="<?=base_url().'/uploads/'.$consultation[0]['c_detail_image']?>" alt="">
                    </div>
                    <?php }?>

                    <div class="col-sm-8 col-sm-offset-2 display_none">
                    </div>
                </div>
                <div class="col-sm-12">
                    <h2 style="text-align:center;padding-bottom: 10px;font-size:25px;margin-top:120px">Enter Birth
                        Information</h2>
                    <form class="form-horizontal newFreeConsultantForm" id="newFreeConsultantForm" method="post">
                        <?php 
				$consultantType = 'basic';
				if($consultation[0]['consultation_type'] == 'MatchMaking'){
					$consultantType = 'marriage';
				}
				if($consultation[0]['consultation_type'] == 'HoroscopeQuery'){
					$consultantType = 'horoscope';
				}
				if($consultation[0]['consultation_type'] == 'Career'){
					$consultantType = 'career';
				}
				if($consultation[0]['consultation_type'] == 'BirthStone'){
					$consultantType = 'birthstone';
				}
				$consultation_name = $consultation[0]['consultation_name'];
			?>
                        <input type="hidden" class="form-control" id="consultantType" name="consultantType"
                            value="<?php echo $consultantType;?>">
                        <input type="hidden" class="form-control" id="consultation_name" name="consultation_name"
                            value="<?php echo $consultation_name;?>">
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class="control-label">Full Name<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="inputName" name="inputName"
                                    placeholder="Enter your Full name">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3 ">
                                <label for="inputEmail3" class="control-label">Gender<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <select name="inputGender" id="inputGender" class="form-control">
                                    <option value="">Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3 ">
                                <label for="inputEmail3" class=control-label">Date And Time Of Birth<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="inputDateDate" name="inputDateDate" placeholder="Birth Date"
                                    class="form-control  date " readonly value="">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="inputTimeMin" name="inputTimeMin" placeholder="Birth Time"
                                    class="form-control time " readonly value="">

                            </div>
                            <div class="col-sm-3">
                                <label class="error1">Time : 24 hour format</label>
                            </div>
                        </div>
                        <!-- <div class="row mt-4 <?= $consultation_type=='Career'? 'display_block': 'display_none';?>"> 
			<div class="col-sm-3 ">
				<label for="inputEmail3" class="control-label">Highest Qualification<span class="required">*</span></label>
			</div>
			<div class="col-sm-9">
				<input type="text" class="form-control" id="educationQualification" name="educationQualification" placeholder="Enter your Highest Qualification">
			</div>
			</div>
			<div  class="row mt-4 <?= $consultation_type=='Career'? 'display_block': 'display_none';?>">
				<div class="col-sm-3">
					<label for="inputEmail3" class="control-label">Current Profession<span class="required">*</span></label>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="currentProfession" name="currentProfession" placeholder="Enter Your Current Profession">
				</div>
			</div> -->

                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class="control-label">Place Of Birth<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="birthCountry" name="birthCountry"
                                    placeholder="Enter Birth Country">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="birthState" name="birthState"
                                    placeholder="Enter your Birth State">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="birthCity" name="birthCity"
                                    placeholder="Enter your Birth City/Distict">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class="control-label">Email-id<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail" name="inputEmail"
                                    placeholder="Enter your Email-id">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class="control-label">Confirm Email-id<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmailCrfm" name="inputEmailCrfm"
                                    placeholder="Re-enter your Email-id">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class="control-label">Contact No.</label>
                            </div>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="inputContact" name="inputContact"
                                    placeholder="Enter your Contact Number">
                            </div>
                        </div>


                        <?php if($consultation_type == 'MatchMaking'   ){ ?>
                        <div class="row mt-4">
                            <div class=" col-sm-12 control_div"><b>Enter your Partner Information here</b></div>
                            <div class="col-sm-3 mt-1">
                                <label for="inputEmail3" class="control-label">Partner Name<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9" style="padding-top:10px; ">
                                <input type="text" class="form-control" id="partnerName" name="partnerName"
                                    placeholder="Enter your Partner Full Name">
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3" class=" control-label">Date And Time Of Birth<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-3" style="padding-top:10px; ">
                                <input type="text" id="partnerDateDate" name="partnerDateDate" placeholder="Birth Date"
                                    class="form-control  date " value="">
                            </div>
                            <div class="col-sm-3" style="padding-top:10px; ">
                                <input type="text" id="partnerTimeMin" name="partnerTimeMin" placeholder="Birth Time"
                                    class="form-control time " value="">

                            </div>
                            <div class="col-sm-3">
                                <label class="error1">Time : 24 hour format</label>
                            </div>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-3 ">
                                <label for="inputEmail3" class="control-label">Place Of Birth<span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="partnerBirthCountry"
                                    name="partnerBirthCountry" placeholder="Enter Partner Birth Country">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="partnerBirthState" name="partnerBirthState"
                                    placeholder="Enter Partner Birth State">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" id="partnerBirthCity" name="partnerBirthCity"
                                    placeholder="Enter Partner Birth City/Distict">
                            </div>
                        </div>
                        <?php } ?>
                        <?php 

			if($consultation[0]['is_question1'] == 'yes')
			{
			    echo'<div  class="row mt-4">
				<div class="col-sm-3">
					<label for="question1" class="control-label question1_label">'.$consultation[0]['q1_label'].'<span class="required">*</span></label>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="question1" name="question1" placeholder="'.$consultation[0]['q1_placeholder'].'.">	
				</div>
			</div>';
			}
			if($consultation[0]['is_question2'] == 'yes')
			{
			echo '<div  class="row mt-4">
				<div class="col-sm-3">
					<label for="question2" class="control-label question2_label">'.$consultation[0]['q2_label'].'<span class="required">*</span></label>
				</div>
				<div class="col-sm-9">
					<input type="text" class="form-control" id="question2" name="question2" placeholder="'.$consultation[0]['q2_placeholder'].'">	
				</div>
			</div>';
			}
			?>
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="inputEmail3"
                                    class="control-label"><?php echo $consultation[0]['q_label']; ?> <span
                                        class="required">*</span></label>
                            </div>
                            <div class="col-sm-9">
                                <textarea rows="5" class="textarea-information-class inputQuestion form-control"
                                    maxlength="100" id="inputQuestion" name="inputQuestion"
                                    placeholder="<?php echo $consultation[0]['q_placeholder']; ?>"></textarea>
                                <p style="color: red;" class="text-center"> <?php echo $consultation[0]['q_info']; ?>
                                </p>
                                <p id="the-count" class="text-right"><span id="current">0</span> <span id="maximum">/
                                        100</span></p>
                            </div>
                        </div>

                        <div class="form-center text-center">
                            <button type="submit" class="slide-btn" id="btnSubmit"
                                style="cursor: pointer !important;">Submit</button>

                        </div>
                    </form>
                </div>

                <br /><br />
                <div>
                    <?php if($consultation[0]['extra_description']){ 
				echo $consultation[0]['extra_description'];
				}
			?>
                </div>
                <br /><br />


                <?php
				// echo $faq_category;
					$datacat = array(
					);

					$datacat['category'] = $faq_category;
					$this->load->view('faqlist',$datacat);
					?>

<?php 
            $datacat = array(
            );

            $datacat['about_id'] = $about_content_id;
            $this->load->view('aboutvastu',$datacat);
            ?>


           

            </div>
			


        </div>

    </div>
</div>
<script>
$('document').ready(function() {

    $('body').on('keyup', '#inputQuestion', function() {

        var characterCount = $(this).val().length,
            current = $('#current'),
            maximum = $('#maximum'),
            theCount = $('#the-count');

        current.text(characterCount);
        var maximum_size = parseInt($(this).attr('maxlength'));
        console.log(maximum_size);

        /*This isn't entirely necessary, just playin around*/
        if (characterCount < ((maximum_size / 100) * 25)) {
            current.css('color', '#666');
        }
        if (characterCount > ((maximum_size / 100) * 25) && characterCount < ((maximum_size / 100) *
            30)) {
            current.css('color', '#6d5555');
        }
        if (characterCount > ((maximum_size / 100) * 30) && characterCount < ((maximum_size / 100) *
            35)) {
            current.css('color', '#793535');
        }
        if (characterCount > ((maximum_size / 100) * 35) && characterCount < ((maximum_size / 100) *
            40)) {
            current.css('color', '#841c1c');
        }
        if (characterCount > ((maximum_size / 100) * 40) && characterCount < ((maximum_size / 100) *
            60)) {
            current.css('color', '#8f0001');
        }

        if (characterCount >= ((maximum_size / 100) * 60)) {
            maximum.css('color', '#8f0001');
            current.css('color', '#8f0001');
            theCount.css('font-weight', 'bold');
        } else {
            maximum.css('color', '#666');
            theCount.css('font-weight', 'normal');
        }
    });


    //-----------------------------------------------------------------------
    /* 
     * consultation_form validation
     */
    $('#newFreeConsultantForm').validate({
        rules: {

            inputName: {
                required: true
            },
            inputGender: {
                required: true
            },
            inputDateDate: {
                required: true
            },
            inputTimeMin: {
                required: true
            },


            birthCountry: {
                required: true
            },
            birthState: {
                required: true
            },
            birthCity: {
                required: true
            },
            inputEmail: {
                required: true
            },
            inputEmailCrfm: {
                required: true,
                equalTo: "#inputEmail"
            },
            inputContact: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 12
            },
            inputQuestion: {
                required: true,
                maxlength: 150
            },

        },
        messages: {

            inputName: {
                required: "Full Name is required"
            },
            inputGender: {
                required: "Gender is required"
            },
            inputDateDate: {
                required: "Birth Date is required"
            },
            inputTimeMin: {
                required: "Birth Time is required"
            },
            birthCountry: {
                required: "Birth Country Name is required"
            },
            birthState: {
                required: "Birth State Name is required"
            },
            birthCity: {
                required: "Birth City Name is required"
            },
            inputEmail: {
                required: "Email ID is required"
            },
            inputEmailCrfm: {
                required: "Confirm Email Id  is required",
                equalTo: "Confirm Email Id don't match."
            },
            inputContact: {
                required: "Contact Number is required.",
                number: "only Numbers are allowed",
                minlength: "Min. 10 digits are required",
                maxlength: "Max. 12 digits are required"
            },
            inputQuestion: {
                required: "Precise question is required.",
                maxlength: "Max. 150 character are required"
            },

        },
        submitHandler: function(form) {

            $.blockUI();
            var question1 = $('#question1').val();
            var question2 = $('#question2').val();

            var question1_label = $('.question1_label').text();
            var question2_label = $('.question2_label').text();

            if (question2 == undefined) {
                question2 = '';
            }
            if (question1 == undefined) {
                question1 = '';
            }
            if (question1_label == undefined) {
                question1_label = '';
            }
            if (question2_label == undefined) {
                question2_label = '';
            }
            console.log(question2);
            console.log(question1);
            var consultantType = $('#consultantType').val();
            if (consultantType == 'love') {
                var partnerName = $('#partnerName').val();
                var partnerDateDate = $('#partnerDateDate').val();
                var partnerTimeMin = $('#partnerTimeMin').val();
                var partnerBirthCountry = $('#partnerBirthCountry').val();
                var partnerBirthState = $('#partnerBirthState').val();
                var partnerBirthCity = $('#partnerBirthCity').val();
            } else if (consultantType == 'marriage') {

                var partnerName = $('#partnerName').val();
                var partnerDateDate = $('#partnerDateDate').val();
                var partnerTimeMin = $('#partnerTimeMin').val();
                var partnerBirthCountry = $('#partnerBirthCountry').val();
                var partnerBirthState = $('#partnerBirthState').val();
                var partnerBirthCity = $('#partnerBirthCity').val();
            } else {

                var partnerName = '';
                var partnerDateDate = '';
                var partnerTimeMin = '';
                var partnerBirthCountry = '';
                var partnerBirthState = '';
                var partnerBirthCity = '';
            }
            var inputName = $('#inputName').val();
            var inputGender = $('#inputGender').val();
            var inputDateDate = $('#inputDateDate').val();
            var inputTimeMin = $('#inputTimeMin').val();
            var birthCountry = $('#birthCountry').val();
            var birthState = $('#birthState').val();
            var birthCity = $('#birthCity').val();
            var inputEmail = $('#inputEmail').val();
            var inputContact = $('#inputContact').val();
            var inputQuestion = $('#inputQuestion').val();
            var paymentmethod = 'free';


            var consultantType = $('#consultation_name').val();

            $.post(APP_URL + 'welcome/add_new_free_consultant_free', {
                    consultantType: consultantType,
                    inputName: inputName,
                    inputGender: inputGender,
                    inputDateDate: inputDateDate,
                    inputTimeMin: inputTimeMin,
                    birthCountry: birthCountry,
                    birthState: birthState,
                    birthCity: birthCity,
                    inputEmail: inputEmail,
                    inputContact: inputContact,
                    inputQuestion: inputQuestion,
                    paymentmethod: paymentmethod,

                    partnerName: partnerName,
                    partnerDateDate: partnerDateDate,
                    partnerTimeMin: partnerTimeMin,
                    partnerBirthCountry: partnerBirthCountry,
                    partnerBirthState: partnerBirthState,
                    partnerBirthCity: partnerBirthCity,
                    question1: question1,
                    question2: question2,
                    question1_label: question1_label,
                    question2_label: question2_label,

                },
                function(response) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, "slow");
                    $('#err_consultation_div').empty();
                    if (response.status == 200) {
                        $('#err_consultation_div').html(
                            "<div class='alert alert-success fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            response.message + "</strong></div>");
                        var responsedata = (response.data);
                        console.log(response.data.consultantType);
                        console.log(response.data.insert_id);
                        $('#consultantType').val('');
                        $('#inputName').val('');
                        $('#inputGender').val('');
                        $('#inputDateDate').val('');
                        $('#inputTimeMin').val('');
                        $('#birthCountry').val('');
                        $('#birthState').val('');
                        $('#birthCity').val('');
                        $('#inputEmail').val('');
                        $('#inputContact').val('');
                        $('#partnerName').val('');
                        $('#partnerDateDate').val('');
                        $('#partnerTimeMin').val('');
                        $('#partnerBirthCountry').val('');
                        $('#partnerBirthState').val('');
                        $('#partnerBirthCity').val('');
                        $('#question1').val('');
                        $('#question2').val('');

                        setTimeout(function() {
                            window.location.href = APP_URL +
                                'welcome/free_consultation_thankyou2/' + response.data
                                .insert_id + '/' + response.data.consultantType;
                        }, 500);

                    } else if (response.status == 201) {
                        $('#err_consultation_div').html(
                            "<div class='alert alert-danger fade in'><button class='close' type='button' data-dismiss='alert'>x</button><strong>" +
                            response.message + "</strong></div>");
                    }

                }, 'json');
            return false;
        },
    });
})
</script>