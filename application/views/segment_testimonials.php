

	<div class="perfect sec-gap ">
		<div class="testimonial_clients">
			<div class="container">
				<h3 class="heading_title margin0  text_green">Your Views</h3>
				<div class="">
					<div class="">
						<div class="" >
							<div class="" style=" margin-right: 30px;">
								<div class="row">
									<div class=" col-md-12 ">
										<div id="" class="position_relative owl-carousel testimonial_carousel owl-theme">
											<?php 
												$testimonials_list =  $this->db->get_where('mayas_testimonials',array('status'=> 'active'))->result_array();
												if($testimonials_list){
													foreach($testimonials_list as $val){
														echo
														'<div class="item">
															<div class="testimonial">
																<div class="pic">
																	<img src="'.base_url().'uploads/'.$val['testimonials_image'].'" alt="">
																	<div>'.$val['testimonials_name'].'</div>
																	<div class="testimonial-content">
																	<span class="upper_quote">“</span>
																	<p class="description">'.$val['comments'].'</p>
																	<span class="lower_quote">”</span>
																</div>
																</div>

															</div>
														</div>';
													}
												}
											?>

								    	</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function() {

			$(".testimonial_carousel").owlCarousel({
				navigation : true, // Show next and prev buttons
				slideSpeed : 300,
				paginationSpeed : 400,
				loop:true,
				nav:true,
			    //items : 3,
				//itemsDesktop : false,
				//itemsDesktopSmall : false,
				//itemsTablet: false,
				//itemsMobile : false

                responsive:{
                        0:{
                             items:1,

      					  },
      				    600:{
         				     items:2.5,

       					   },
     				   1000:{
       					     items:3.5,

     					   }
  					  }
			});

		});
	</script>