<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1>About us</h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li>
            <li class="active">About Us</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->

<section class="section-wrapper">
    <div class="container">
        <div class="row">
		<?php
				if ($aboutus_list == 0) {
					echo 'No record found into database';
					} else {
						$i = 1;
						foreach ($aboutus_list as $value) {
						
					?>
		<div class="col-md-12">			
			<div class="row" >	
				<h2 style ="text-align: center;">Company overview</h2>
			</div>
			<div class="col-md-6">
                <p><?php print_r($value['aboutus1']);?></p>

            </div>

            <div class="col-md-6">
                <div class="image-block">
                    <a rel="nofollow">
                        <img src="<?php echo base_url();?><?php echo 'uploads/'.$value['aboutus_image1'];?>" class="img-responsive" alt="Mayas Astrology">
                    </a>
                </div>
            </div>
		 </div>	
			
	<div class="col-md-12"   style =" padding-top: 20px" >	
			<div class="col-md-6">
                <div class="image-block">
                    <a rel="nofollow" >
                        <img src="<?php echo base_url();?><?php echo 'uploads/'.$value['aboutus_image2'];?>" class="img-responsive" alt="Mayas Astrology">
                    </a>
                </div>
            </div>
			<div class="col-md-6">

                <p><?php echo $value['aboutus2']?></p>

            </div>
	</div >		
			<?php 
					$i++;
					
					
				}
				
			}	
		?>
        </div>
    </div>
</section>
