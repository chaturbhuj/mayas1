

<?php
	
	if($bespoke_details){
		$services_name = $bespoke_details[0]['services_name'];
		$service_id = $bespoke_details[0]['service_id'];
		$services_main_description = $bespoke_details[0]['services_main_description'];
		$services_description = $bespoke_details[0]['services_description'];
		$services_image = $bespoke_details[0]['services_image'];
		$service_detail_image = $bespoke_details[0]['service_detail_image'];
		$service_step_image = $bespoke_details[0]['service_step_image'];
	
		
		
	}else{
		$service_id = 0;
		$services_name = '';
		$services_main_description = '';
		$services_description = '';
		$services_image = '';
		$service_detail_image = '';
		$service_step_image = '';
		
		
			
	}
		
	
?>












<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8">
                <h1> Bespoke Services Details</h1>
            </div>
            <div class="col-sm-4 hidden-xs">
               
                    
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url(); ?>">Home</a></li>
            <li class="active">Bespoke Services Details</li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->
<div class="section-column-wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="left-sidebar">
                <div class="sidebar-menu margin-bottom-30">
                   <ul>
						<?php
							foreach ($services as $value) {
						?>
                       
                        <li><a rel="nofollow"  href="<?php echo base_url(); ?>welcome/servicedetails/<?php echo $value['service_id'] ?>"><?php echo $value['services_name']?></a></li>
								<?php
								}
							?>								
                    </ul>
                </div>
                <!--.sidebar-menu-->

            </div>
            <!--.left-sidebar-->
        </div>
        <div class ="col-md-9">
		
			<div class="col-md-12" >
				<h3 style="  text-align: center;font-size: 40px;color: #582e2e;padding-bottom: 20px;"><strong><?php echo $services_name; ?></strong></h3>
			</div>
			
			<!--make this image -->
		<!--make this image -->
			<div class="col-md-12 image_div"  style="display:none">
				<img class="img-preview" style="    height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($services_image);?>" alt="Mayas Astrology">
			</div>
			<!---<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				<img class="img-preview" src="<?php echo base_url()?>/uploads/cs.png">
			</div>--->
			<div class="col-md-12 image_div" >
				<!---<img class="img-preview" style="height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($service_detail_image);?>" alt="Mayas Astrology">--->
			<?php echo ($service_detail_image);?>
			</div>
			<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				
			</div>
			<div class="col-md-12 image_div" >
				<img class="img-preview" style="height: 200px;    width:100%;   padding-bottom: 10px;" src="<?php echo base_url()?>/uploads/<?php echo ($service_step_image);?>" alt="Mayas Astrology">
			</div>
			<div class="col-md-12 image_div" style="    padding-bottom: 40px;">
				
			</div>
			<h4 style="font-size: 30px;    padding-top: 200px; padding-bottom: 30px;" class="consult"><span style="width: 30px; height: 30px; background-color: #678; padding: 1px 14px; border-radius: 50%; color: #fff;">1</span>  <?php echo $services_main_description; ?></h4>
			
			<div class="col-md-12" style="display:none;">
				<span><strong>Description</strong></span>:
				<span><?php echo $services_main_description; ?></span>
			</div>
			<div class="col-md-12" style="padding-bottom: 10px;">
				<span><strong>Description</strong></span>:
				
				<span><?php echo $services_description; ?></span>
				
			</div>
			
			<div class="row">
				
				<div class="col-md-3">
					<a rel="nofollow" href="<?php echo base_url(); ?>welcome/bespoke"  class="btn btn-primary btn-block " style="background: #f47d31;">Next Step</a>
								
				</div>
			</div>
			</div>
		
        </div>
    </div>
</div>

<!--.section-column-wrapper-->	 
						
			