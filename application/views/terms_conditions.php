<!-- .nav -->
<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1><?php echo $content_data[0]['heading']; ?></h1>
            </div>
        </div>
        <!--.row-->
    </div>
    <!--.container-->
</section>
<!--.page-header-->
<section class="breadcrumb-wrapper">
    <div class="container">
        <ol class="breadcrumb">
            <li><a rel="nofollow" href="<?php echo base_url();?>">Home</a></li> / 
            <li class="active"><?php echo $content_data[0]['heading']; ?></li>
        </ol>
        <!--.breadcrumb-->
    </div>
    <!--.container-->
</section>
<!--.breadcrumb-wrapper-->

<section class="section-wrapper">
    <div class="container">
        <div class="row">
		<div class="col-md-12">
			<?php echo $content_data[0]['content']; ?>
		 </div>	
        </div>
    </div>
</section>
