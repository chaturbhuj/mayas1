<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'welcome/error';
$route['login'] = "authorization/authorization";
$route['aboutus'] = 'welcome/aboutus';
$route['public_page_list'] = 'welcome/public_page_list';
$route['termsconditions'] = 'welcome/termsconditions';
$route['privacypolicy'] = 'welcome/privacypolicy';
$route['refundcancelation'] = 'welcome/refundcancelation';
$route['premium-consultations'] = 'welcome/services';
$route['premium-consultations/newproducts'] = 'welcome/newproducts';
$route['blog'] = 'welcome/blog';
$route['contactus'] = 'welcome/contactus';
$route['products/(:any)'] = "welcome/products";
$route['translate_uri_dashes'] = FALSE;
$route['free-consultations'] = 'welcome/free_consultations';
$route['vastu/vastu'] = 'welcome/vastuall';
$route['vastu/vastumain'] = 'welcome/vastumainpage';
$route['free-consultation/gemstone'] = 'welcome/free_consultation/gemstone';
$route['free-consultation/career'] = 'welcome/free_consultation/career';
$route['free-consultation/finance'] = 'welcome/free_consultation/finance';
$route['free-consultation/marriage'] = 'welcome/free_consultation/marriage';
$route['free-consultation/love'] = 'welcome/free_consultation/love';
$route['free-consultation/horoscope'] = 'welcome/free_consultation/horoscope';
$route['free-consultation/(:any)'] = 'welcome/free_consultation';

$route['premium-consultation/detail/(:any)'] = 'premium_consultation/detail';
$route['instant-consultation'] = 'welcome/consultation_step1';
$route['instant-consultation/free'] = 'welcome/consultation';

$route['instant-consultation/(:any)/(:any)/(:any)/(:any)'] = 'welcome/instant_consultation';
$route['blog/detail/(:any)/(:any)'] = 'welcome/blogfull';
$route['premium-consultation/details/(:any)'] = 'welcome/bespokeservicedetails';
$route['career'] = 'career/career';
$route['profile'] = 'account/profile';
$route['wishlist'] = 'account/wishlist';
$route['order_history'] = 'account/past_orders';
$route['order_detail'] = 'account/orders_list';
$route['free_consultation_list'] = 'account/free_consultation_list';
$route['logout'] = 'account/logout';
$route['shipping_address'] = 'account/shipping_address';
$route['vastu/(:any)'] = 'vastu/vastu';

// $route['vastu/vastu'] = 'vastu/vastuall';

$route['sitemap.xml'] = 'mapping';

/*old website url
$route['Horoscope.aspx'] = 'welcome/free_consultation/horoscope';
$route['Category.aspx'] = 'welcome/services';
$route['default.aspx'] = 'welcome/services';
$route['Default.aspx'] = 'welcome/services';
$route['PremimumConsultancy.aspx'] = 'welcome/services';
$route['Matchmacking.aspx'] = 'welcome/free_consultation/marriage';
$route['ViewProduct.aspx'] = 'welcome/services/';
$route['gis.asp'] = 'welcome/services';*/
