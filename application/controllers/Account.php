<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {
	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
       
        $this->load->library('session');
	}

    public function profile()	{
			
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();

        //var_dump($data);

        $this->load->view('components/header');
        $this->load->view('edit_profile'	);
        $this->load->view('components/footer',$data);	
	}

    public function wishlist(){
		$customer_id = $this->session->userdata('customer_id');
		//var_dump($customer_id);
		$this->db->order_by('fav_id', 'desc');
		$data['data_wishlist']  = $this->db->get_where('favourite_product', array('customer_id' => $customer_id))->result();
		//var_dump($data['data_wishlist'] );
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		
		
		$this->load->view('components/header');
		$this->load->view('wishlist',$data);
		$this->load->view('components/footer',$data);
	}

    public function past_orders(){

        $this->load->view('components/header');
		$this->load->view('past_order_List');
		$this->load->view('components/footer');
        
    }
    public function orders_list(){

        $this->load->view('components/header');
		$this->load->view('order_detail_List');
		$this->load->view('components/footer');
        
    }
    public function free_consultation_list(){
        $user_email = $this->session->userdata['customer_email'];
        $data['free_consulation_list'] = $this->db->get_where('mayas_consultation',array('inputEmail'=>$user_email))->result_array();
        $this->load->view('components/header');
		$this->load->view('free_consultation_list',$data);
		$this->load->view('components/footer');
        
    }
    public function shipping_address(){
        $user_email = $this->session->userdata['customer_email'];
        $this->load->view('components/header');
		$this->load->view('shipping_address');
		$this->load->view('components/footer');
        
    }
    public function logout(){
        $data = array(
            'customer_id' => '',
            'customer_email' => '',
            'loggedIN' => '',
            'first_name' => '',
            'last_name' => '',
            'customer_img' =>'',
            
        );
        $this->session->unset_userdata('customer_id');
		redirect(base_url());
       
    }


}