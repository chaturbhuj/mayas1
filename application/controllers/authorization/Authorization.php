<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');

/**
 * This controller class handles requests for user authorization
 * to access the application.
 *
 * @version     0.0.1
 * @since       0.0.1
 * @author      Parth Shukla <shuklaparth@hotmail.com>
 */
class Authorization extends CI_Controller {

    /**
     * Default Constructor
     * 
     * @access      public
     * @since       0.0.1
     */
    public function __construct() {

        parent::__construct();
        #loading the required helper
        $this->load->helper('url');
        #loading the required library
        #$this->load->library('session');
    }

    //--------------------------------------------------------------------------  
    /**
     * Index Page for this controller.
     * 
     * Maps to the following URL
     *      http://BASE_URL/authorization/authorization
     * - or -
     *      http://BASE_URL/authorization/authorization/index
     */
    public function index() {
        $this->load->view('login/login');
    }

    //--------------------------------------------------------------------------   
    /**
     * This function is used to logging in into the system after verifying the user credentials
     * 
     * @version     0.0.1
     * @since       0.0.1
     * @access      public
     * @author      arthur
     */
    public function check_login_credentials() {
        #array to store response to be displayed
        $arr_response = array();

        #default response status message
        $arr_response['status'] = 1100;

        #loading the required model
        $this->load->model('Authorization_model', 'obj_auth', TRUE);
        #matching the user credentials
        $returned_value = $this->obj_auth->check_login_credentials();
        //var_dump($_POST);exit();
        if ($returned_value) {
            #login credentials are correct
            $arr_response['status'] = 1000;

            #if redirect url exists in the session
            $redirect_url = base_url() . 'page/consultation_search';
            $arr_response['mydirect'] = $redirect_url;
        } else {
            $arr_response['status'] = 1300;
            $arr_response['mydirect'] = '';
        }
        echo json_encode($arr_response);
    }
    //--------------------------------------------------------------------------
    /**
     * Handles request for sending link for forgot password.
     * 
     * @access      public
     * @since       0.0.1
     */
    public function forgot_password() {
        $this->load->view('login/forgot_password');
    }

    //--------------------------------------------------------------------------
    /**
     * Handles request for sending link for reset password to user email id.
     * 
     * @access      public
     * @since       0.0.1
     */
    public function send_link_for_reset_password() {
        #array to store response to be displayed
        $arr_response = array();

        #default response status message
        $arr_response['status'] = 1100;

        #loading the required model
        $this->load->model('Authorization_model', 'obj_auth', TRUE);
        #matching the user credentials
        $returned_value = $this->obj_auth->send_link_for_reset_password();
        //var_dump($_POST);exit();
        if ($returned_value) {
            #login credentials are correct
            $arr_response['status'] = 1000;
        } else {
            $arr_response['status'] = 1300;
        }
        echo json_encode($arr_response);
    }

}

//End of class Authorization

//End of file authorization.php
/* Location: ../../application/controllers/authorization/authorization.php */