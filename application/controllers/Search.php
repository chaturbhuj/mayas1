<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	/**
	 * Index Page for this controller.
	 */
	public function index()	{
		
		$this->load->view('components/header');
		
		$this->load->view('search_store');
		
		$this->load->view('components/footer');
	}

	/**
	 * Index Page for this controller.
	 */
	public function product()	{

		if(isset($_GET['main_cate_id'])){
			$main_cate_id=$_GET['main_cate_id'];
		}else{
			$main_cate_id=0;
		}
		if(isset($_GET['cat_id'])){
			$cat_id=$_GET['cat_id'];
		}else{
			$cat_id=0;
		}
		if(isset($_GET['id'])){
			$product_sub_category_id=$_GET['id'];
		}else{
			$product_sub_category_id=0;
		}
		if(isset($_GET['search'])){
			$search_poduct_data=$_GET['search'];
		}else{
			$search_poduct_data='';
		}
		
		
		/*$data = array(
				'user_id' 		=> $this->session->userdata('customer_id'),
				'search_poduct_data'	=>$search_poduct_data,
				'created_date' 	=> date('Y-m-d'),
				'created_time' 	=> date('H:i:s'),	
			);
			//var_dump($data);
		$result =	$this->db->insert('user_search_details', $data);*/
		
		$other_filter_data=array();
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		//$main_category = $this->obj_comman->get_by('product_sub_category',array('product_sub_category_id' => $product_sub_category_id),false,true);

		/*if($main_category['percentage']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'percentage'));
			array_push($other_filter_data,array('categries_name'=>'percentage','categries_data'=>$categries));
		}
		if($main_category['brand']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'brand'));
			array_push($other_filter_data,array('categries_name'=>'brand','categries_data'=>$categries));
		}
		if($main_category['colour']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'colour'));
			array_push($other_filter_data,array('categries_name'=>'colour','categries_data'=>$categries));
		}
		if($main_category['size']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'size'));
			array_push($other_filter_data,array('categries_name'=>'size','categries_data'=>$categries));
		}
		if($main_category['specialty_sizes']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'specialty_sizes'));
			array_push($other_filter_data,array('categries_name'=>'specialty_sizes','categries_data'=>$categries));
		}
		if($main_category['smart']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'smart'));
			array_push($other_filter_data,array('categries_name'=>'smart','categries_data'=>$categries));
		}
		if($main_category['voice_assistant']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'voice_assistant'));
			array_push($other_filter_data,array('categries_name'=>'voice_assistant','categries_data'=>$categries));
		}
		if($main_category['display']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'display'));
			array_push($other_filter_data,array('categries_name'=>'display','categries_data'=>$categries));
		}
		if($main_category['screen_type']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'screen_type'));
			array_push($other_filter_data,array('categries_name'=>'screen_type','categries_data'=>$categries));
		}
		if($main_category['resolutions']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'resolutions'));
			array_push($other_filter_data,array('categries_name'=>'resolutions','categries_data'=>$categries));
		}
		if($main_category['hdr']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'hdr'));
			array_push($other_filter_data,array('categries_name'=>'hdr','categries_data'=>$categries));
		}
		if($main_category['screen_size']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'screen_size'));
			array_push($other_filter_data,array('categries_name'=>'screen_size','categries_data'=>$categries));
		}
		if($main_category['processor']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'processor'));
			array_push($other_filter_data,array('categries_name'=>'processor','categries_data'=>$categries));
		}
		if($main_category['processor_type']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'processor_type'));
			array_push($other_filter_data,array('categries_name'=>'processor_type','categries_data'=>$categries));
		}
		if($main_category['hard_drive_type']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'hard_drive_type'));
			array_push($other_filter_data,array('categries_name'=>'hard_drive_type','categries_data'=>$categries));
		}
		if($main_category['touchscreen']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'touchscreen'));
			array_push($other_filter_data,array('categries_name'=>'touchscreen','categries_data'=>$categries));
		}
		if($main_category['operating_system']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'operating_system'));
			array_push($other_filter_data,array('categries_name'=>'operating_system','categries_data'=>$categries));
		}
		if($main_category['refurbished']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'refurbished'));
			array_push($other_filter_data,array('categries_name'=>'refurbished','categries_data'=>$categries));
		}
		if($main_category['ram']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'ram'));
			array_push($other_filter_data,array('categries_name'=>'ram','categries_data'=>$categries));
		}
		if($main_category['material']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'material'));
			array_push($other_filter_data,array('categries_name'=>'material','categries_data'=>$categries));
		}
		if($main_category['age']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'age'));
			array_push($other_filter_data,array('categries_name'=>'age','categries_data'=>$categries));
		}
		if($main_category['age']=='yes'){
			$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id,'detail_categries_name' => 'age'));
			array_push($other_filter_data,array('categries_name'=>'age','categries_data'=>$categries));
		}*/
		//$categries = $this->obj_comman->get_by('product_detail_categries',array('product_sub_category_id' => $product_sub_category_id),array('detail_categries_name' => 'asc'));
		//$vendor_id=$this->session->userdata('vendor_id');
		//$sql="SELECT psc.* FROM `product_sub_category` as psc INNER JOIN products as p on psc.product_sub_category_id = p.product_sub_category_id WHERE  psc.`main_cate_id` = '$main_cate_id' AND psc.`product_category_id` = '$cat_id' AND `category_status` = 'active' AND p.`product_status` = 'active' AND p.`admin_permission` = 'active' GROUP BY p.`product_sub_category_id`";
		//$data['subcategory'] = $this->obj_comman->get_query($sql);
		$data['subcategory'] = $this->obj_comman->get_by('mayas_category',array('cat_id'=>$cat_id));


		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		
		$data['other_filter_data']=$other_filter_data;
		$this->load->view('components/header');
		$this->load->view('search_product',$data);
		$this->load->view('components/footer',$data);
	}

	
	
}
