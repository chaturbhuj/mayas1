<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vastu extends CI_Controller {
	/**
     * Default constructor.
     *
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();

	  }

    public function index()
    {
      $vastu_data = $this->db->get_where('admin_section_content',array('content_id'=>1))->result_array();
      $process_data = $this->db->get_where('admin_process_point')->result_array();
      $this->load->model('Welcome_model', 'obj_wa', TRUE);
      $data['vastuservices'] = $this->obj_wa->get_list_of_all_vastuservices();
      if (count($vastu_data) >= 1) {
        $data['vastu_data'] = $vastu_data;
        $data['category_id'] = $vastu_data[0]['faq_category'];

        $data['meta_title'] = $vastu_data[0]['meta_title'];
        $data['meta_keyword'] = $vastu_data[0]['meta_keyword'];
        $data['meta_description'] = $vastu_data[0]['meta_description'];
        $data['meta_image'] = $vastu_data[0]['meta_image'];
        // $faq_data = $this->db->get_where('admin_faq',array('faq_category_id'=>$vastu_data[0]['faq_category']))->result_array();
        // if (count($faq_data) >= 1) {
        //   $data['faq_data'] = $faq_data;
        // }else{
        //   $data['faq_data'] = false;
        // }
      }else{
        $data['vastu_data'] = false;
      }
      if (count($process_data) >= 1) {
        $data['process_data'] = $process_data;
      }else{
        $data['process_data'] = false;
      }
      $this->load->view('components/header');
          $this->load->view('vastulandingpage',$data);
          $this->load->view('components/footer');
    
    }

    // public function vastupage(){
    
      
    // }


    public function vastuall(){
      $this->load->model('Welcome_model', 'obj_wa', TRUE);
      $data['vastuservices'] = $this->obj_wa->get_list_of_all_vastuservices();
      $this->load->view('components/header');
      $this->load->view('vastuservice',$data);
      $this->load->view('components/footer');

    }


    public function upload_image() {
      #array to hold the response values to be displayed
      $arr_response = array();

      $info = pathinfo($_FILES['myFile']['name']);
      $ext = $info['extension']; // get the extension of the file

      if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
              ($_FILES["myFile"]["size"] < 30485760)) {
          $file = $info['filename'];
          $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
          $target = './uploads/' . $filename;
          $file = $_FILES['myFile']['tmp_name'];
          move_uploaded_file($file, $target);

          $arr_response['status'] = 200;
          $arr_response['filename'] = $filename;
      } else {
          $arr_response['status'] = FAILED;
          $arr_response['message'] = "Not a valid File";
      }
      echo json_encode($arr_response);
  }


  public function vastumainpage(){
    // $vastu_data = $this->db->get_where('admin_section_content',array('content_id'=>3))->result_array();
    //   var_dump($vastu_data);
      //var_dump($this->uri->segment(2));
      //var_dump($vastu_data[0]['home_vastu_url']);
        // $data['page_heading'] = $vastu_data[0]['page_heading'];
        // $data['short_description'] = $vastu_data[0]['short_description'];
        // $data['url'] = $vastu_data[0]['url'];
        // $data['page_title'] = $vastu_data[0]['page_title'];
        // $data['meta_tag'] = $vastu_data[0]['meta_tag'];
        // $data['meta_descp'] = $vastu_data[0]['meta_descp'];
        // $data['page_content'] = $vastu_data[0]['page_content'];
        // $data['additional_json'] = json_decode($vastu_data[0]['additional_json']);

  }

    public function vastu()
    {
      $url = $this->uri->segment(2); 
      //var_dump($url);
  	  $vastu_data = $this->db->get_where('mayas_new_vastu',array('url'=>$url))->result_array();
      //var_dump($data);
      //var_dump($this->uri->segment(2));
      //var_dump($vastu_data[0]['home_vastu_url']);
        $data['page_heading'] = $vastu_data[0]['page_heading'];
        $data['about_content_id'] = $vastu_data[0]['about_content_id'];
        $data['banner_id'] = $vastu_data[0]['banner_id'];
        $data['faq_category'] = $vastu_data[0]['faq_category'];
        $data['short_description'] = $vastu_data[0]['short_description'];
        $data['url'] = $vastu_data[0]['url'];
        $data['page_title'] = $vastu_data[0]['page_title'];
        $data['meta_tag'] = $vastu_data[0]['meta_tag'];
        $data['meta_descp'] = $vastu_data[0]['meta_descp'];
        $data['page_content'] = $vastu_data[0]['page_content'];
        $data['additional_json'] = json_decode($vastu_data[0]['additional_json']);
        $data['owner_name_label'] = $vastu_data[0]['owner_name_label'];
        $data['owner_name_placeholder']  = $vastu_data[0]['owner_name_placeholder'];
        $data['email_id_label']  = $vastu_data[0]['email_id_label'];
        $data['email_id_placeholder']  = $vastu_data[0]['email_id_placeholder'];
        $data['owner_contact_label']  = $vastu_data[0]['owner_contact_label'];
        $data['owner_contact_placeholder']  = $vastu_data[0]['owner_contact_placeholder'];
        $data['property_address_label']  = $vastu_data[0]['property_address_label'];
        $data['property_address_placeholder']  = $vastu_data[0]['property_address_placeholder'];
        $data['property_layout_maps_label']  = $vastu_data[0]['property_layout_maps_label'];
        $data['property_direction_orientation_label']  = $vastu_data[0]['property_direction_orientation_label'];
        $data['property_direction_orientation_placeholder']  = $vastu_data[0]['property_direction_orientation_placeholder'];
        $data['owner_date_time_birth_label']  = $vastu_data[0]['owner_date_time_birth_label'];
        $data['owner_place_birth_label']  = $vastu_data[0]['owner_place_birth_label'];
        $data['property_direction_orientation_message']  = $vastu_data[0]['property_direction_orientation_message'];


        $data['property_directioncompass_msg']  = $vastu_data[0]['property_directioncompass_msg'];
        $data['property_directioncompass_link']  = $vastu_data[0]['property_directioncompass_link'];
        $data['property_directioncompass_defaultvideo']  = $vastu_data[0]['property_directioncompass_defaultvideo'];
        $data['compass_image']  = $vastu_data[0]['compass_image'];
    
        //var_dump($data);
        
        $data1['vastu_data'] = $data;
        $data1['datalat'] = $vastu_data;
        $this->load->view('components/header');
        $this->load->view('vastu',$data1);
        $this->load->view('components/footer',$data);

        // $this->load->view('components/footer',$data);
    }

    // public function home_vastu()
    // {
    //   // code...
    // }
    //
    // public function industry_vastu()
    // {
    //   // code...
    // }





}
