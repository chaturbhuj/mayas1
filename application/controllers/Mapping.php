<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Mapping extends CI_Controller {

    //------------------------------------------------------------------------
    /*
     * Default constructor
     */
    public function __construct() {
        parent::__construct();
    }

    //------------------------------------------------------------------------
    /*
     * function to load dashboard view 
     */
    public function index() {
		
        $count = 0;
        $data = array();
        $branch = array();
		
        $general = $this->db->query("select * from mayas_services where status = 'active'")->result();
		foreach($general as $key => $row){
			$branch[$count] = base_url().'premium-consultation/detail/'.$row->services_url;
			$count++;
		}
        $general = $this->db->query("select * from mayas_blog where status = 'active'")->result();
		foreach($general as $key => $row){
			$blog_url = $row->blog_heading;
			$blog_url = strtolower($blog_url);
			$blog_url = preg_replace('/[^0-9a-zA-Z ]/s', ' ', $blog_url);
			$blog_url = str_replace(' ', '-', $blog_url); 
			$branch[$count] = base_url().'blog/detail/'.$row->blog_id.'/'.$blog_url;
			$count++;
		}
		
        $general = $this->db->query("select * from mayas_consultation_content")->result();
		foreach($general as $key => $row){
			$branch[$count] = base_url().'free-consultation/'.$row->url;
			$count++;
		}
		
		

		header('Content-type: application/xml; charset=utf-8');
 		echo '<?xml version="1.0" encoding="UTF-8"?>'; 
		echo '<urlset xmlns="http://www.google.com/schemas/sitemap/0.84" 
		xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
		xsi:schemaLocation="http://www.google.com/schemas/sitemap/0.84 
		http://www.google.com/schemas/sitemap/0.84/sitemap.xsd">
		<url>
		  <loc>https://mayasastrology.com/</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/aboutus</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/blog</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/contactus</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/career</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/privacypolicy</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/refundcancelation</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/termsconditions</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/premium-consultations</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>
		<url>
		  <loc>https://mayasastrology.com/free-consultations</loc>
		  <changefreq>daily</changefreq><priority>1.00</priority>
		</url>'; 
		
			foreach ($branch as $value) {
				echo '<url><loc>' . $value . '</loc>
				<changefreq>daily</changefreq><priority>1.00</priority>
			</url>';
			}
		   
		echo '</urlset>';


        // $this->load->view('sitemap/sitemapping', $data);
    }

}
