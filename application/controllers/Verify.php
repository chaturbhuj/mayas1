<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {
	public $_tableName = 'mysoft_recruiter_info';
    function __construct() {
        parent::__construct();
		$this->load->model('Comman_model', 'comman_model', TRUE);
    }

	
	public function v(){
		if($_GET){
			$id = $this->input->get('e');
			$key = $this->input->get('v');
			if($id&&$key){
				$check = $this->comman_model->get_by('vendor_account',array('vendor_id'=>$id,'verify_code'=>$key),false,true);
				
				if($check){
					$capital = implode('',range('A','Z'));
					$small = implode('',range('a','z'));
					$number = implode('',range('0','20'));
					$token = substr(str_shuffle($capital.$small.$number),40);
		
					$data11 = array(
						'email_verified'=>'1',
						'verify_code'=>$token
					);
					$this->db->update('vendor_account', $data11,array('vendor_id'=>$check['vendor_id'], 'verify_code'=>$verify_code));
					
					redirect('verify/verified?id='.$check['vendor_id'].'-'.$token.'&type=v');
				}
				else{
					redirect('verify/error');
				}
			}
		}else{
			redirect('verify/error');
		}
		redirect('verify/error');
	}

	public function vc(){
		if($_GET){
			$id = $this->input->get('e');
			$key = $this->input->get('v');
			if($id&&$key){
				$check = $this->comman_model->get_by('customer_account',array('customer_id'=>$id,'verify_code'=>$key),false,true);
				if($check){
					
					$capital = implode('',range('A','Z'));
					$small = implode('',range('a','z'));
					$number = implode('',range('0','20'));
					$token = substr(str_shuffle($capital.$small.$number),40);
		
					$data11 = array(
						'email_verified'=>'1',
						'verify_code'=>$token
					);
					$this->db->update('customer_account', $data11,array('customer_id'=>$check['customer_id'], 'verify_code'=>$key));
					
					redirect('verify/verified?id='.$check['customer_id'].'&type=c');
				} else{
					redirect('verify/error');
				}
			}
		}else{
			redirect('verify/error');
		}
		redirect('verify/error');
	}
	
	public function verified(){
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			
		$this->data['_redirect_link'] = 'customer';
		if($_GET){
			$id = $this->input->get('type');
			if($id=='v'){
				$this->data['_redirect_link'] = 'vendor';
			}
		}
		
		$this->load->view('components/header', $this->data);
		$this->load->view('public/verify_page/verified' ,$this->data);
		$this->load->view('components/footer',$data);
	}
	
	public function error(){
		$this->load->view('components/header');
		$this->load->view('public/verify_page/error');
		$this->load->view('components/footer');
		//redirect('welcome');
	}
}
