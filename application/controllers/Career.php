<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Career extends CI_Controller {
	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
       
	}
	
	
	//-------------------------------------------------------------
    /**
     * This function is used to load aquatic_management page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function career() {
						
		$meta_id=4;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		
		$this->load->view('career');
		$this->load->view('components/footer',$data);
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free Consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_career() {
        //array to store ajax responses
  

        #loading the require model
        $this->load->model('Career_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->update_career();
        if ($return_value) {
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' Your application is saved. We will contact you soon after reviewing your profile.';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    
	
	
  
		
		
		 
	}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */