<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Customer_account extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
    }
	
	public function update_profile_post() {
		if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		/*else if(!$this->post('customer_email')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
			
		}*/
		else{			
			$data = array(
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'country_id' => $this->post('country_id'),
				'country_name' => $this->post('country_name'),
				'state_id' => $this->post('state_id'),
				'state_name' => $this->post('state_name'),
				'city_name' => $this->post('city_name'),
				'city_id' => $this->post('city_id'),
				'zipcode' => $this->post('postal_code'),
				'address' => $this->post('address'),
				'phone_no' => $this->post('phone_num'),
			);
			//echo"fsd";
			$result = $this->db->update('customer_account', $data, array('customer_id' => $this->post('customer_id')));
			//$result = $this->db->update('customer_account', $data);
			
			$status = ['status' => 200 , 'message' => 'Profile updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_profile_post() {
		if(!$this->post('customer_id')){
			$status = [
				['status' => 400 ,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$customer_id = $this->post('customer_id');
			$db_result = $this->db->get_where('customer_account', array('customer_id' => $customer_id));
			
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					//var_dump($row);
							
					if (!array_key_exists($row->customer_id, $data)) {
						$data[$row->customer_id] = array();
					}
					if (array_key_exists($row->customer_id, $data)) {
						$data[$row->customer_id] = array(
							
							'customer_id' => $row->customer_id,
							'customer_email' => $row->customer_email,
							'first_name' => $row->first_name,
							'last_name' => $row->last_name,
							'country_name' => $row->country_name,
							'country_id' => $row->country_id,
							'state_name' => $row->state_name,
							'state_id' => $row->state_id,
							'city_name' => $row->city_name,
							'city_id' => $row->city_id,
							'postal_code' => $row->zipcode,
							'address' => $row->address,
							'phone_no' => $row->phone_no,
							'customer_img' => $row->customer_img,
						);
						array_push($data_value, $data[$row->customer_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	

	public function check_customer_old_password_post() {
		
		$pass = sha1('admin' . (md5('admin' . $this->post('currentpassword'))));
        $db_result = $this->db->get_where('customer_account', array('customer_password' => $pass,'customer_id' => $this->post('param'),));
        
        if ($db_result->num_rows() > 0) {
            echo 'true';
        } else {
            echo 'false';
        }
	}
	//for change email
	public function check_customer_old_email_post() {
		
		//$pass = sha1('admin' . (md5('admin' . $this->post('currentpassword'))));
        
		$user_email = $this->post('current_email');
        $db_result = $this->db->get_where('customer_account', array('customer_email' => $user_email,'customer_id' => $this->post('param'),));
		//var_dump($db_result);
        if ($db_result->num_rows() > 0) {
            echo 'true';
        } else {
            echo 'false';
        }
	}	 
    //for update email
	public function update_email_post() {
		if(!$this->post('new_email')){
			$status = ['status' => 400 ,  'message' => 'New email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			$random_value = rand(1000,9999);
			//$pass=$_POST['user_password'];
			$data = array(
				'email_verification_code' => $random_value
			);
			$result = $this->db->update('customer_account', $data, array('customer_id' => $_POST['customer_id']));
												
			$status = ['status' => 200 , 'message' => 'Email updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	//
	public function checking_valid_email_post() {
		if(!$this->post('new_email')){
			$status = ['status' => 400 ,  'message' => 'New email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			//$random_value = rand(1000,9999);
			//$pass=$_POST['user_password'];
			$new_email = $_POST['new_email'];
			$checking_random_no = $_POST['checking_random_no'];
			
			$result = $this->db->get_where('customer_account', array('email_verification_code' => $checking_random_no,'customer_id' => $_POST['customer_id']));
			//var_dump($result->num_rows());
			if($result->num_rows() != 0){
				$data = array(
					'customer_email' => $new_email,
				);
				$result = $this->db->update('customer_account', $data, array('customer_id' => $_POST['customer_id']));
			
				$status = ['status' => 200 , 'message' => 'Email updated successfully'];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{
				
				$status = ['status' => 201 ,  'message' => 'Invalid verifcation code.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
			
		}
    }
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_customer_address_list_data_post()
	{	
	//	var_dump($this->post('customer_id'));
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$db_result = $this->db->get_where('customer_address', array('customer_id' => $customer_id));

			$user_data = $this->db->get_where('customer_account', array('customer_id' => $customer_id));

				
			$data = $db_result->result();
			//var_dump($data);
			//var_dump($user_data->row());
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				'user_data' => $user_data->row(),
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	
	public function fatch_customer_address_data_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$db_result = $this->db->get_where('customer_address', array('customer_address_id' => $customer_id));

			//$user_data = $this->db->get_where('customer_account', array('customer_id' => $customer_id));

				
			$data = $db_result->result();
			//var_dump($data);
			//var_dump($user_data->row());
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				//'user_data' => $user_data->row(),
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

  
	public function update_password_post() {
		if(!$this->post('new_password')){
			$status = ['status' => 400 ,  'message' => 'New Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			#Generate hashed password.
			$pass = sha1('admin' . (md5('admin' . $this->post('new_password'))));
			//$pass=$_POST['user_password'];
			$data = array(
				'customer_password' => $pass
			);
			$result = $this->db->update('customer_account', $data, array('customer_id' => $_POST['customer_id']));
												
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }

	//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_profile_image_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $this->post('image_cat') . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
			
            move_uploaded_file($file, $target);
			$data = array(
				'customer_img' => $filename
			);
			$result = $this->db->update('customer_account', $data, array('customer_id' => $this->post('customer_id')));
			
            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }
	
	/*public function fatch_customer_wish_list_data_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$this->db->order_by('fav_id', 'desc');
			$db_result = $this->db->get_where('favourite_product', array('customer_id' => $customer_id));

			$data_wishlist = $db_result->result();
			$product_data='';
			$product_id ='';
			$quentity_data ='';
			foreach ($data_wishlist as $value) {
				
				
				
			}
			
				
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data_wishlist,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}*/
	
	
	public function wishlist_item_remove_post()
	{
		if (!$this->post('fav_id')) {
			$status = ['status' => 400,  'message' => 'id is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			#Generate hashed password.
			$fav_id = $this->post('fav_id');

			$return_val = $this->db->delete('favourite_product', array('fav_id' =>  $fav_id));

			$status = ['status' => 200, 'message' => 'Item removed successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	public function remove_proposed_product_post()
	{
		if (!$this->post('product_id')) {
			$status = ['status' => 400,  'message' => 'id is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			#Generate hashed password.
			$fav_id = $this->post('fav_id');

			$return_val = $this->db->delete('favourite_product', array('fav_id' =>  $fav_id));

			$status = ['status' => 200, 'message' => 'Item removed successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	public function remove_shipping_address_post()
	{
		
			#Generate hashed password.
			$customer_address_id = $this->post('customer_address_id');

			$return_val = $this->db->delete('customer_address', array('customer_address_id' =>  $customer_address_id));

			$status = ['status' => 200, 'message' => 'Item removed successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		
	}
	
	public function orders_item_remove_post()
	{
		if (!$this->post('product_id')) {
			$status = ['status' => 400,  'message' => 'id is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			#Generate hashed password.
			$product_id = $this->post('product_id');

			$return_val = $this->db->delete('product_order_detail', array('product_id' =>  $product_id));

			$status = ['status' => 200, 'message' => 'Item removed successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}


	public function fatch_customer_wish_count_data_post()
	{
		if (!$this->post('customer_id')) {
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'count' => 0,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			$customer_id = $this->post('customer_id');
			$this->db->order_by('fav_id', 'desc');
			$db_result = $this->db->get_where('favourite_product', array('customer_id' => $customer_id));

			$data = $db_result->result();


			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'count' => $db_result->num_rows(),
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	public function fatch_customer_order_list_data_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			
			$this->db->order_by('order_id', 'desc');
			$db_result = $this->db->get_where('product_order_detail', array('customer_id' => $customer_id,'check_payment'=>'yes'));
			
			
			$data = $db_result->result();
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				//'data' => $data1,
				
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	public function fatch_customer_order_list_data1_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$this->db->select('customer_id,customer_email');
			$customer_email = $this->db->get_where('customer_account',array('customer_id'=>$customer_id))->result();

			$this->db->order_by('order_id', 'desc');
			$db_result = $this->db->get_where('mayas_order_payment', array('email' => $customer_email[0]->customer_email));
			$data = $db_result->result();
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				//'data' => $data1,
				
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	public function fatch_customer_orders_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			
			$product_id =0;
			
			if(($this->post('product_id'))){
				$product_id =$this->post('product_id');
				$this->db->where('product_id', $product_id);
			}
			$this->db->order_by('order_id', 'desc');
			$db_result = $this->db->get_where('product_order_detail', array('vendor_id' => $customer_id));
			//var_dump($this->db->last_query());
			//print_r($this->db->last_query());
			$data = $db_result->result();
			//var_dump($data);
			foreach ($data as $key => $value) {
				$vendor_id = $value->vendor_id;
				$sql1 = "SELECT customer_account.*,(select country_name from country_list where country_list.id=customer_account.country_id) as 'country_name',(select state_name from state_list where state_list.id=customer_account.state_id) as 'state_name', (select city_name from city_list where city_list.id=customer_account.city_id) as 'city_name' FROM `customer_account` where customer_id = $vendor_id";
				$vendor_data = $this->db->query($sql1);
				//var_dump($vendor_data->row());
				$data[$key]->vendor_data = $vendor_data->row();
			}

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				//'data1'=>$data1,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
		
		
	}
	
	
	public function fatch_complain_data_post()
	{
		
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$check_toggle = $this->post('check_toggle');
			$parent_id=$this->post('product_id');
			$complain_id=$this->post('complain_id');
			$vendor_id=$this->post('vendor_id');
			
			
			//$this->db->order_by('complain_id', 'desc');
			$vendor = $this->db->get_where('complains' , array('user_id'=> $vendor_id ,'type'=>'vendor'))->result();
			//var_dump($vendor);
			//var_dump($this->db->last_query());
			$this->db->order_by('complain_id', 'ASC');
			$vendor_complains = $this->db->get_where('complains' , array('product_id'=> $parent_id))->result();
			//var_dump($vendor_complains);
			
		
			$vendor_detail = $this->db->get_where('customer_account' , array('customer_id'=> $vendor_id ))->result();
			
			if($vendor_detail){
				foreach($vendor_detail as $vel){
					$vendor_first_name=$vel->first_name;
					$vendor_last_name=$vel->last_name;
					$vendor_customer_img=$vel->customer_img;
					
				}
			}

			 //var_dump($vendor_complains);
			 $content = '';
			$content_vendor='';
			if($vendor_complains){
				foreach($vendor_complains as $value){
					$user_id=$value->user_id;
					$vendor_id=$value->vendor_id;
					$product_id=$value->product_id;
					$created_date=$value->created_date;
					$current_dat=strtotime($created_date);
					$change_date = date(" j M Y ",$current_dat);
					$created_time=$value->created_time;
					$user_comment=$value->user_comment;
					$status=$value->status;
					$type=$value->type;
						$customer_detail = $this->db->get_where('customer_account' , array('customer_id'=> $user_id ))->result();	
						foreach($customer_detail as $val){
							$first_name=$val->first_name;
							$last_name=$val->last_name;
							$customer_img=$val->customer_img;
							//var_dump($customer_img);
							if($type =="customer" && $check_toggle=="vendor"){
								
								$view_type="(customer)";
								
							}elseif($type =="vendor" && $check_toggle=="customer"){
								
								$view_type="(vendor)";
							}else{
								$view_type="";
							}
						
							$content .='<div class="second_post_section">
											<div class="media media-chat"><img class="avatar" src="'.base_url().'uploads/'.$customer_img.'" alt="...">
												<div class="media-body">
													<span class="meta"><time datetime="2018">'.$change_date.' <br/>'.$created_time.'</time></span>';
													
														//var_dump($customer_id);
														//var_dump($user_id);
														$content .='<h6>'.$first_name.''.$last_name.' '.$view_type.'</h6>';

									
														

													
													$content .='<p>'.$user_comment.'</p>
												</div>
											</div>
										</div>'; 
								 
							
						}	
				}
					
						/*if($vendor){
							foreach($vendor as $ven){
								//var_dump($ven);
								$user_comments=$ven->user_comment;
								$type=$ven->type;
								$vendor_id=$ven->vendor_id;
								$created_date=$ven->created_date;
								$created_date=$ven->created_date;
								//var_dump($vendor_id);
								$creted_dates=$ven->created_date;
								$current_dates=strtotime($creted_dates);
								$change_dates = date(" j M Y ",$current_dates);
								
											
								$content_vendor .='<div class="first_comment post_content ps-container ps-theme-default ps-active-y" id="chat-content">
													<div class="media media-chat"><img class="avatar" src="'.base_url().'uploads/'.$vendor_first_name.'" alt="...">
														<div class="media-body">
															<span class="meta"><time datetime="2018">'.$change_dates.' <br/>'.$created_time.'</time></span>
															<h6>'.$vendor_first_name.' '.$vendor_first_name.' ('.$type.')</h6>
															<p>'.$user_comments.'</p>
														</div>
													</div>			 
												</div>';
										
									
								
							
							}
						}*/
					
			
				
			
			}
			//$data['$type'] = $type;
			 //$data['customer_detail'] = $customer_detail;
			 
		
		$arr_response['body'] = $content;
		$arr_response['complaint_status'] = $status;
		$arr_response['content_vendor'] = $content_vendor;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Data get Successfully';
		echo json_encode($arr_response);
		}
		
		
	}
	
	

	
	
	public function fatch_list_of_people_post()
	{
		if (!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			
			$product_id =0;
			
			if(($this->post('product_id'))){
				$product_id =$this->post('product_id');
				$this->db->where('product_id', $product_id);
			}
			$this->db->order_by('order_id', 'desc');
			$db_result = $this->db->get_where('product_order_detail', array('product_id' => $product_id));
			
			//var_dump($this->db->last_query());
			//print_r($this->db->last_query());
			$data = $db_result->result();
			//var_dump($data);
			foreach ($data as $key => $value) {
				$vendor_id = $value->vendor_id;
				$sql1 = "SELECT customer_account.*,(select country_name from country_list where country_list.id=customer_account.country_id) as 'country_name',(select state_name from state_list where state_list.id=customer_account.state_id) as 'state_name', (select city_name from city_list where city_list.id=customer_account.city_id) as 'city_name' FROM `customer_account` where customer_id = $vendor_id";
				$vendor_data = $this->db->query($sql1);
				//var_dump($vendor_data->row());
				$data[$key]->vendor_data = $vendor_data->row();
			}

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				//'data1'=>$data1,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
		
		
	}
	
	public function fatch_customer_order_detail_list_data_post()
	{
		if (!$this->post('id')) {
			$status = [
				['status' => 400,  'message' => 'id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$order_id = $this->post('id');
			$db_result = $this->db->get_where('product_order_detail', array('order_id' => $order_id));
			$data = $db_result->result();
			//var_dump($data);
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	public function update_customer_address_data_post()
	{
		if (!$this->post('customer_id')) {
			$status = ['status' => 400,  'message' => 'login is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_address_id = $this->post('customer_address_id');
			$data = array(
				'customer_id' => $this->post('customer_id'),
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'address_line' => $this->post('address_line'),
				'city_name' => $this->post('city_name'),
				'state_name' => $this->post('state_name'),
				'zip_code' => $this->post('zip_code'),
				'country_name' => $this->post('country_name'),
				'shipping_phone_number' => $this->post('shipping_phone_number'),
				'is_selected' => '1',
			);

			$result = $this->db->update('customer_address', array('is_selected' => '0'), array('customer_id' => $this->post('customer_id')));
			
			if ($customer_address_id > 0) {
				$result = $this->db->update('customer_address', $data, array('customer_address_id' => $this->post('customer_address_id')));
			} else {
				$result = $this->db->insert('customer_address', $data);
			}

			$status = ['status' => 200, 'message' => 'successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			//console.log($result);
		}
	}

	public function update_vendor_bank_details_post()
	{
		if (!$this->post('customer_id')) {
			$status = ['status' => 400,  'message' => 'login is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$customer_id = $this->post('customer_id');
			$data = array(
				'customer_id' => $this->post('customer_id'),
				'bank_name' => $this->post('bank_name'),
				'account_number' => $this->post('account_number'),
				'ifsc_code' => $this->post('ifsc_code'),
				'other_info' => $this->post('other_info'),
			);

			$result = $this->db->update('customer_account', $data, array('customer_id' => $this->post('customer_id')));
			
			$status = ['status' => 200, 'message' => ' Bank details update successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			//console.log($result);
		}
	}

	
	
	public function update_customer_active_address_data_post()
	{
		if (!$this->post('customer_address_id')) {
			$status = ['status' => 400,  'message' => 'New Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			#Generate hashed password.
			$customer_address_id = $this->post('customer_address_id');
			$customer_id = $this->post('customer_id');
			//$pass=$_POST['user_password'];
			$data = array(
				'is_selected' => '0',
			);
			$result = $this->db->update('customer_address', $data, array('customer_id' => $_POST['customer_id']));

			$data = array(
				'is_selected' => '1',
			);
			$result = $this->db->update('customer_address', $data, array('customer_address_id' => $_POST['customer_address_id']));

			$status = ['status' => 200, 'message' => 'address updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	public function add_card_post() {
		require_once(APPPATH . 'libraries/stripe-php/init.php');
		
		$user_id= $this->session->userdata('customer_id');
		$user_email= $this->session->userdata('customer_email');
		$card_number='************'.substr($_POST['cardnumber'],-4);
		
		try {
			//Stripe::setApiKey('sk_test_ScdD9CbyXlWsqq6og6QQqx1N');
			//Stripe::setApiKey('sk_live_u3aPXmrlxLdhO7x8Qucm5MDj');
			//echo"gfgfg";
			//\Stripe\Stripe::setApiKey(STRIPEKEY);
			\Stripe\Stripe::setApiKey($this->config->item('PRIVATESTRIPEKEY'));
			
			//var_dump($this->config->item('PRIVATESTRIPEKEY'));
			// Get the token from the JS script
			$token = $this->input->post('stripeToken');
			//var_dump($token);
			//var_dump($user_email);
			//var_dump($card_number);
			// Create a Customer
			$customer = \Stripe\Customer::create(array(
				"email" => $user_email,
				"source" => $token,
			));
			//var_dump($customer);
		
			$carddetail =  array(
				'user_id' =>$user_id,
				'cardnumber' => $card_number ,
				'customer_id' => $customer->id ,
				'card_primary' => 1,
				'created_on'=>date("Y-m-d H:i:s"),
			) ;
			//echo "hh";
			$result=$this->db->insert('user_card_detail',$carddetail);
			//var_dump($result);
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Card added successfully';
			
		} catch (Stripe_CardError $e) {
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 101;
			$arr_response['message'] = 'Payment Failed Due to card Reason ..! Please try again';
		} catch (Stripe_InvalidRequestError $e) {
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 102;
			$arr_response['message'] = 'Payment Failed Due to server problem ..! Please try again';
		} catch (Stripe_AuthenticationError $e) {
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 103;
			$arr_response['message'] = 'Payment Failed Due to Invalid autentication ..! Please try again';
		} catch (Stripe_ApiConnectionError $e) {
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 104;
			$arr_response['message'] = 'Payment Failed Due to server problem ..! Please try again';
		 } catch (Stripe_Error $e) {
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 105;
			$arr_response['message'] = 'Payment Failed Due to server problem ..! Please try again';
		} catch (Exception $e) {
			//echo $e;
			$arr_response['status'] = 201;
			$arr_response['error_code'] = 106;
			$arr_response['message'] = 'Payment Failed Due to server problem ..! Please try again';
		}
		
		echo json_encode($arr_response); 
    }
	
	public function get_card_detail_post()
	{
		if (!$this->post('user_id')) {
			$status = [
				['status' => 400,  'message' => 'id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('user_card_detail', array('user_id' => $user_id));
			$data = $db_result->result();
			//var_dump($data);
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
	public function remove_card_post()
	{
		
			#Generate hashed password.
			$id = $this->post('id');

			$return_val = $this->db->delete('user_card_detail', array('id' =>  $id));

			$status = ['status' => 200, 'message' => 'Item removed successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		
	}
	
	public function stripepayment_post(){ 
		//var_dump('gfg');
		require_once(APPPATH . 'libraries/stripe-php/init.php');
		$arr_response = array('status' => 500);
		$user_id = $this->post('user_id');
		//var_dump($user_id);
		$user_email = $this->post('user_email');	
		//var_dump($user_email);
			$card_id=$_POST['choosen_card_id'];
			$amount= $this->post('final_total');
			//var_dump($amount);
			
			try { 
				\Stripe\Stripe::setApiKey($this->config->item('PRIVATESTRIPEKEY'));
				if($card_id>0){
					$card_data = $this->db->get_where("user_card_detail",array('id'=>$card_id))->result();
					if($card_data){
						foreach($card_data as $row){
								//var_dump($row);
								$card_nuber=$row->cardnumber;
								$customer_id=$row->customer_id;
						}
					//	var_dump($card_nuber);
						
					}
				}else{
					$card_nuber='************'.substr($_POST['cardnumber'],-4);						
					// Get the token from the JS script
					//$token = $_POST['stripeToken'];
					// Create a Customer
					$customer = \Stripe\Customer::create(array(
						"email" => $user_email,
						//"source" => $token,
					));
					
					$customer_id=$customer->id;
				}
				
				// Charge the Customer instead of the card
				$charge = \Stripe\Charge::create(array(
					"amount" => floatval($amount)*100,
					"currency" => CURRENCY_CODE,
					"customer" => $customer_id
				));
				//var_dump($charge);
				// this line will be reached if no error was thrown above				
				//print_r($charge) ;
				//print_r($charge->id) ;
				//print_r($charge->status) ;die();
				if ($charge->status == "succeeded") {
					$transition_data = $charge->id;
					 
						
					$arr_response['status'] = 200;
					$arr_response['transition_data'] = $transition_data;
					$arr_response['message'] = 'Order has been received successfully! Thank You';
				}else{
					//redirect('order');
					$arr_response['status'] = 201;
					$arr_response['message'] = 'Payment Failed try again';
				}
				
			} catch (Stripe_CardError $e) { 
				//echo 'Stripe_CardError';
				//$this->session->set_flashdata("message","Payment Failed Due to card Reason ..!") ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 101;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
			} catch (Stripe_InvalidRequestError $e) {
				//echo 'Stripe_InvalidRequestError';
				
				// Invalid parameters were supplied to Stripe's API
				//$this->session->set_flashdata("message","Payment Failed Due to Internal Reason ..!".$e->getMessage()) ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 102;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
			} catch (Stripe_AuthenticationError $e) {
				//echo 'Stripe_AuthenticationError';
				// Authentication with Stripe's API failed
				//$this->session->set_flashdata("message","Payment Failed Due to Invalid authentication ..!") ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 103;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
			} catch (Stripe_ApiConnectionError $e) {
				//echo 'Stripe_ApiConnectionError';
				// Network communication with Stripe failed
			
				//$this->session->set_flashdata("message","Payment Failed Due to api Reason ..!") ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 104;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
			} catch (Stripe_Error $e) {
				//echo 'Stripe_Error';
				// Display a very generic error to the user, and maybe send
				//$this->session->set_flashdata("message","Payment Failed Due to server problem ..!") ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 105;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
			} catch (Exception $e) {
				//echo  $e;
				// Something else happened, completely unrelated to Stripe
				//$this->session->set_flashdata("message","Payment Failed Due to server problem ..!") ;
				//$this->session->set_flashdata("message","There is some issue in your card, please check and place order again ..!") ;
				//var_dump($this->session->set_flashdata("message"));die();
				//redirect('order');
				//exit();
				$arr_response['status'] = 201;
				$arr_response['error_code'] = 106;
				$arr_response['message'] = 'There is some issue in your card, please check and place order again ..!';
				//exit();
			}
			//redirect('order/success/'.$insert_id);
			
			echo json_encode($arr_response);
		
	}
	
	public function add_custome_wished_product_post()
	{
		if(!$this->post('customer_id')){
			$status = ['status' => 400,  'message' => 'Login is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$product_name = $this->post('product_name');
			$customer_id = $this->post('customer_id');
			$share_your_link = $this->post('share_your_link');
			//$pass=$_POST['user_password'];
			$data = array(
				'wished_product_name' => $product_name,
				'customer_id' => $customer_id,
				'shared_link' =>$share_your_link,
				'created_date' => date('Y-m-d'),
				'created_time' => date('H:i:s'),
			);
			$result = $this->db->insert('wished_product', $data);

			if($result){
				$status = ['status' => 200, 'message' => 'product detail  updated successfully'];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{
				$status = ['status' => 201, 'message' => 'Something went  wrong'];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}
			
		}
	}
	
	public function fatch_wished_product_post()
	{
		if(!$this->post('customer_id')) {
			$status = [
				['status' => 400,  'message' => 'customer_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$customer_id = $this->post('customer_id');
			$db_result = $this->db->get_where('wished_product', array('customer_id' => $customer_id));


				
			$data = $db_result->result();
			//var_dump($data);
			//var_dump($user_data->row());
			
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}


}
