<?php defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);

header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');


// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Uploadpic extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
    }
	
	

	/**update user profile picture*/
    public function update_image_new_post() {
		if(!$this->post('base64')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again.'];
			$this->response($status, REST_Controller::HTTP_BAD_REQUEST);
		}else{
			
			$decode = base64_decode($this->post('base64'));			
			$filename = date("YmdHisu");
			$new_img = $filename.'.JPG';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
				
												
			$status = ['status' => 200 , 'filename' => $new_img, 'message' => 'Profile Picture updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	/**update user csv picture*/
   /**update user profile picture*/
    public function update_pdf_new_post() {
		if(!$this->post('base64')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again.'];
			$this->response($status, REST_Controller::HTTP_BAD_REQUEST);
		}else{
			
			$decode = base64_decode($this->post('base64'));			
			$filename = date("YmdHisu");
			$new_img = $filename.'.pdf';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
				
												
			$status = ['status' => 200 , 'filename' => $new_img, 'message' => 'Pdf uploded successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
		
	/**update user csv picture*/
   /**update user profile picture*/
    public function update_mp3_new_post() {
		if(!$this->post('base64')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again.'];
			$this->response($status, REST_Controller::HTTP_BAD_REQUEST);
		}else{
			
			$decode = base64_decode($this->post('base64'));			
			$filename = date("YmdHisu");
			$new_img = $filename.'.mp3';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
				
												
			$status = ['status' => 200 , 'filename' => $new_img, 'message' => ' mp3 uploded successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	
	/**update user profile picture*/
    public function upload_images_post() {
		$data = array();
		foreach($_REQUEST['files'] as $files){
			$decode = base64_decode($files['base64']);			
			$filename = date("YmdHisu").mt_rand() ;
			$new_img = $filename.'.JPG';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);

			array_push($data, $new_img);
		}			
		$status = ['status' => 200 , 'filename' => $data, 'message' => 'Images updated successfully'];
		$this->set_response($status, REST_Controller::HTTP_OK);
    }
	
	
	/**update single picture*/
    public function upload_single_image_post() {
			
			$files = $_REQUEST['files'];
			$decode = base64_decode($files['base64']);			
			$filename = date("YmdHisu").mt_rand() ;
			$new_img = $filename.'.JPG';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
			
			$status = ['status' => 200 , 'filename' => $new_img, 'message' => 'Images updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
    }
	
	/**update user profile picture*/
    public function update_attandace_post() {
		if(!$this->post('user_id')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again1'];
			$this->response($status, REST_Controller::HTTP_BAD_REQUEST);
		}else{
			//print_r($this->post('base64'));
			$status = ['status' => 200];
			$this->set_response($status, REST_Controller::HTTP_OK);
			//return true;
			
			
			
			
			//$decode = //base64_decode(implode("/",$this->post('base64')));//($_REQUEST['base64']);	
			//$decode = base64_decode($_REQUEST['base64']);	
			$decode = base64_decode($this->post('base64'));	
					
			$filename = date("YmdHisu");
			$new_img = $filename.'.JPG';
			$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
			
			if(date('l', strtotime(date('Y-m-d'))) == 'Monday'){
				$data = array(
					'attendance_mon_photo' => $new_img,
					'attendance_mon_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Tuesday'){
				$data = array(
					'attendance_tue_photo' => $new_img,
					'attendance_tue_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Wednesday'){
				$data = array(
					'attendance_wed_photo' => $new_img,
					'attendance_wed_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Thursday'){
				$data = array(
					'attendance_thus_photo' => $new_img,
					'attendance_thus_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Friday'){
				$data = array(
					'attendance_fri_photo' => $new_img,
					'attendance_fri_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Saturday'){
				$data = array(
					'attendance_sat_photo' => $new_img,
					'attendance_sat_time' =>  date('H:i:s'),
				);	
			}else if(date('l', strtotime(date('Y-m-d'))) == 'Sunday'){					
				$data = array(
					'attendance_sun_photo' => $new_img,
					'attendance_sun_time' =>  date('H:i:s'),
				);				
			}
			
			//$result = $this->db->update('ps_task', $data, array('task_id' => $this->post('task_id')));
							
			$status = ['status' => 200 , 'filename' => $new_img, 'message' => ' Your Attendance Picture updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
		//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image_test_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file
     //print_r($info);
        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 90485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            
				$target = './uploads/' . $filename;
			
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);
				$status = ['status' => 200 , 'filename' => $filename, 'message' => ' Your Attendance Picture updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
           
        } else {
           $status = ['status' => 201 , 'message' => ' Your Attendance Picture not updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
        }       
    }
	
		//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file
		$file = $info['filename'];
        if (($ext == "mp4" || $ext == "PDF" || $ext == "mp3" || $ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg')) {
            $file = $info['filename'];
            $filename = $this->post('image_cat') . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            //move_uploaded_file($file, $target);
             
			if(move_uploaded_file($file, $target)){
			   echo "upload complete";
			} else {
			   echo "move_uploaded_file failed";
			   
			}
			 
			$status = ['filename' => $filename ,'status' => 200 , 'message' => ' Your Attendance Picture updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
        } else {
			$status = ['status' => 201 ,'ext' => $ext ,'info' => $info , 'message' => 'Not a valid File'.$_FILES["myFile"]["type"]];
			$this->set_response($status, REST_Controller::HTTP_OK);
            
        }
        
    }
	
	
}
