<?php

defined('BASEPATH') or exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit', '64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Products extends REST_Controller
{

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		if ($this->session->userdata('country') == 'India') {
			date_default_timezone_set('Asia/Singapore');
		} else {
			date_default_timezone_set('Canada/Saskatchewan');
		}

		// Configure limits on our controller methods 
		// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}
		// Load Pagination library
		$this->load->library('pagination');
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_store_list_post()
	{

		$country_data = $this->post('country_data');
		$city_data = $this->post('city_data');
		$categoryArray = $this->post('categoryArray');

		$this->db->where('country_name', $country_data);
		$country_list = $this->db->get('country_list')->row();


		$this->db->where('city_name', $city_data);
		$city = $this->db->get('city_list')->row();


		if ($country_list) {
			$this->db->where('country_id', $country_list->id);
		}
		if ($city) {
			$this->db->where('city_id', $city->id);
		}

		$this->db->group_by('vendor_id');
		$this->db->where('store_status', 'active');
		$this->db->where('admin_permission', 'active');
		$db_result = $this->db->get('vendor_stores');
		//var_dump($this->db->last_query());
		if ($db_result && $db_result->num_rows() > 0) {
			$data = $db_result->result();


			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_store_subcategory_list_item_post()
	{
		$vendor_id = $this->post('vendor_id');

		$sql = "SELECT count(`product_id`) as 'items',`product_sub_category`.* FROM `products` join `product_sub_category` on `product_sub_category`.product_sub_category_id=`products`.product_sub_category_id WHERE `vendor_id` = '$vendor_id' And category_status='active' And is_favorite='yes' GROUP by `products`.`product_sub_category_id` ORDER BY `product_id` ASC";
		//var_dump($sql);
		$db_result = $this->db->query($sql);


		$data = $db_result->result();

		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'data' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_vendor_best_product_list_post()
	{
		$vendor_id = $this->post('vendor_id');
		$customer_id = $this->post('customer_id');

		$sql = "SELECT p.* FROM `products` as p INNER JOIN product_order_detail as pod on p.product_id = pod.product_id WHERE pod.`vendor_id` = '$vendor_id' AND `product_status` = 'active' AND `admin_permission` = 'active' GROUP BY pod.`product_id` LIMIT 10";

		$other_products = $this->db->query($sql)->result();

		$query = $this->db->query($sql);

		$num_rows = $query->num_rows();

		if ($num_rows < 5) {

			$sql = "SELECT * FROM `products` WHERE `vendor_id` = '$vendor_id' AND `product_status` = 'active' AND `admin_permission` = 'active' GROUP BY `product_id` ORDER BY RAND() LIMIT 10";

			$other_products = $this->db->query($sql)->result();
		}

		if ($other_products) {
			foreach ($other_products as $row) {

				$is_fav = 'no';
				//var_dump($key);
				$this->db->where('product_id', $row->product_id);
				$quentity_data = $this->db->get('product_goods_info')->result();

				$this->db->where('product_sub_category_id', $row->product_sub_category_id);
				$sub_category_data = $this->db->get('product_sub_category')->row();

				if ($customer_id > 0) {
					$this->db->where('customer_id', $customer_id);
					$this->db->where('product_id', $row->product_id);
					$this->db->where('vendor_id', $row->vendor_id);
					$favourite_product = $this->db->get('favourite_product')->result();
					if ($favourite_product) {
						$is_fav = 'yes';
					}
				}

				$row->product_url = base64_encode($row->product_id);

				$row->quentity_multiple = count($quentity_data);
				$row->quentity_data = $quentity_data;
				$row->is_fav = $is_fav;
				$row->service_tax = $sub_category_data->service_tax;
				//var_dump($row);
			}
		}
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'other_products' => $other_products,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_vendor_new_product_list_post()
	{
		$vendor_id = $this->post('vendor_id');
		$customer_id = $this->post('customer_id');

		$this->db->where('vendor_id', $vendor_id);
		$this->db->where('product_status', 'active');
		$this->db->where('admin_permission', 'active');
		$this->db->order_by('product_id', 'desc');
		$this->db->limit(10);
		$other_products = $this->db->get('products')->result();
		//var_dump($this->db->last_query());
		if ($other_products) {
			foreach ($other_products as $key => $row) {
				//var_dump($key);
				$this->db->where('product_id', $row->product_id);
				$quentity_data = $this->db->get('product_goods_info')->result();
				$row->product_url = base64_encode($row->product_id);

				$this->db->where('product_sub_category_id', $row->product_sub_category_id);
				$sub_category_data = $this->db->get('product_sub_category')->row();

				$row->service_tax = $sub_category_data->service_tax;

				$row->quentity_multiple = count($quentity_data);
				$row->quentity_data = $quentity_data;
				//var_dump($row);

				$is_fav = 'no';
				if ($customer_id > 0) {
					$this->db->where('customer_id', $customer_id);
					$this->db->where('product_id', $row->product_id);
					$this->db->where('vendor_id', $row->vendor_id);
					$favourite_product = $this->db->get('favourite_product')->result();
					if ($favourite_product) {
						$is_fav = 'yes';
					}
				}
				$row->is_fav = $is_fav;
			}
		}
		$status = [
			'status' => 200,
			'message' => 'fetched successfully data',
			'other_products' => $other_products,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_product_category_list_post()
	{
		$category_id = $this->post('url_categery');
		//$this->db->where('category_status', 'active');
		$db_result = $this->db->get('mayas_category');
		
		$data = $db_result->result();
		//$subcategory = $subcategory->result();
			//var_dump($subcategory);
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'subcategory' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	
	public function fatch_rashi_list_post()
	{
		
		$db_result = $this->db->get('mayas_rashi');
		$data = $db_result->result();
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'rashi' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	public function fatch_desease_list_post(){
	
		
		$db_result = $this->db->get('mayas_diease');
		$data = $db_result->result();
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'desease' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	public function fatch_p_consult_list_post(){
		
		
		$data['paidconsult_data'] = $this->db->get_where('mayas_services',array('status'=>'active'))->result();
		$data =$data['paidconsult_data'];
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'p_consult' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_product_list_by_store_post()
	{
		$search_data = $this->post('search_data');
		$customer_id = $this->post('customer_id');
		$store_id = $this->post('store_data');
		$rowno = $this->post('pagno');
		$product_other_categoryArray_rashi = $this->post('product_other_categoryArray_rashi');
		$product_other_categoryArray_diease = $this->post('product_other_categoryArray_diease');
		$product_other_categoryArray_consultation = $this->post('product_other_categoryArray_consultation');
		
		$sql= 'SELECT  * FROM mayas_newproduct 
			INNER JOIN mayas_product_rashi ON mayas_newproduct.product_id = mayas_product_rashi.product_id 
			INNER JOIN mayas_product_diease ON mayas_newproduct.product_id = mayas_product_diease.product_id 		
			INNER JOIN mayas_product_paid_consultation ON mayas_newproduct.product_id = mayas_product_paid_consultation.product_id 		
		';
		
		
		$is_rashi = 'no';
		$is_diease = 'no';
		$is_consultation = 'no';
		$is_where = 'no';
		
		
		if ($product_other_categoryArray_rashi != '') {
			$i = 0;
			$is_rashi = 'yes';
			$rashi_length = '';
			$rashi_length=count($product_other_categoryArray_rashi[0]['id']);
			$is_where="yes";
			if($is_rashi == 'yes'){
				if($is_where=="yes"){
					$sql .=" WHERE ";
				}else{
					$sql .=" AND ";
				}
				
				foreach ($product_other_categoryArray_rashi[0]['id'] as $row) {
					//var_dump($row);
					if($i == 0){
						$sql .="(mayas_product_rashi.rashi_id= ".$row." ";
					}
					if($i != 0){
						$sql .=" OR mayas_product_rashi.rashi_id= ".$row."  ";
					}
					$i++;
					if($i == $rashi_length){
						$sql .=" ) ";
					}
					
				}
			}
		}
		
		if ($product_other_categoryArray_diease != '') {
			$i = 0;
			$is_diease = 'yes';
			$diease_lenght = '';
			$is_where="yes";
			$diease_lenght=count($product_other_categoryArray_diease[0]['id']);			
			if($is_rashi == 'yes'){
				if($is_where=="yes"){
					
				}else{
					$sql .=" WHERE ";
				}
				$sql .=" AND ";
				foreach ($product_other_categoryArray_diease[0]['id'] as $row) {	
				
					
					if($i == 0){
						$sql .=" (mayas_product_diease.diease_id= ".$row." ";
					}
					if($i != 0){
						$sql .=" OR mayas_product_diease.diease_id= ".$row."  ";
					}
					
					$i++;
					if($i == $diease_lenght){
						$sql .=" )";
					}
				
					
				}
							
			}
			
		}
		if ($product_other_categoryArray_consultation != '') {
			$i = 0;
			$is_consultation="yes";
			$consultation_length = '';
			$is_where="yes";
			$consultation_length=count($product_other_categoryArray_consultation[0]['id']);			
			if($is_diease == 'yes'){
				if($is_where=="yes"){
					
				}else{
					$sql .=" WHERE ";
				}
				$sql .=" AND ";
				foreach ($product_other_categoryArray_consultation[0]['id'] as $row) {	
				
					
					if($i == 0){
						$sql .=" (mayas_product_paid_consultation.service_id= ".$row." ";
					}
					if($i != 0){
						$sql .=" OR mayas_product_paid_consultation.service_id= ".$row."  ";
					}
					
					$i++;
					if($i == $consultation_length){
						$sql .=" ) ";
					}
				
					
				}				
			}
			
		}		
		if (isset($_POST['url_id'])) {
			
			if (($_POST['url_id'])) {
				$cat_id=$_POST['url_id'];
				
				if($is_where=="yes"){
					$sql .=" AND ";
				}else{
					$sql .=" WHERE ";
				}
				$sql.="  product_category = ".$cat_id."  ";
				
				$is_where="yes";
			}
			
		}
		
		if ($search_data) {
			if($is_where=="yes"){
				$sql .=" AND ";	
			}else{
				$sql .=" WHERE ";
			}
			$sql.=" product_name LIKE '%$search_data%' ";
			
			
		}
		
		$sql .="GROUP BY mayas_newproduct.product_id"; 
		$db_result = $this->db->query($sql)->result();
		$total_rows = count($db_result);
		$rowperpage = 10;
		// Row position
		if($rowno != 0) {
			$rowno = ($rowno - 1) * $rowperpage;
		}

 
		$db_result = $this->db->query($sql);
		$this->db->limit($rowperpage, $rowno);
		
		if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
			//var_dump($data);
			$data_value = array();
			//var_dump($data_value);
			foreach ($db_result->result() as $row) {
				
				$this->db->where('cat_id', $row->product_category);
				$sub_category_data = $this->db->get('mayas_category')->row();
				
				$is_fav = 'no';
				if ($customer_id > 0) {
					$this->db->where('customer_id', $customer_id);
					$this->db->where('product_id', $row->product_id);
					//$this->db->where('vendor_id', $row->vendor_id);
					$favourite_product = $this->db->get('favourite_product')->result();
					if ($favourite_product) {
						$is_fav = 'yes';
					}
				}
				
				$whileimg= explode(",", $row->product_images);				
				$row->is_fav = $is_fav;
		
				if (!array_key_exists($row->product_id, $data)) {
					$data[$row->product_id] = array();
				}
				if (array_key_exists($row->product_id, $data)) {
					$data[$row->product_id] = array(					 
						'product_url' => base64_encode($row->product_id),
						'product_id' => $row->product_id,
						'string_url' => $row->string_url,
						'product_name' => $row->product_name,
						'main_category_id' => $row->product_category,
						'product_category_id' => $row->product_category,
						'product_sub_category_id' => $row->product_category,
						'product_image' => $whileimg[0],
						'product_status' => $row->status,
						'product_price' => $row->product_actual_prize,
						'product_discounted_prize' => $row->product_discounted_prize,
				
						'is_fav' => $is_fav,
						//'service_tax' => $sub_category_data->service_tax,
					);
					array_push($data_value, $data[$row->product_id]);
				}
				
				
			}

			// Pagination Configuration
			$config['base_url'] = 'javascript:void(0);';
			$config['use_page_numbers'] = TRUE;
			$config['total_rows'] = $total_rows;
			$config['uri_segment'] = $rowno;
			$config['per_page'] = $rowperpage;
			$config['cur_page'] = $rowno;
			$config['page'] = $rowno;
	
			$this->pagination->initialize($config);

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data_value,
				'pagination' => $this->pagination->create_links(),
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_product_quantity_data_post()
	{

		$product_id = $this->post('product_id');

		$this->db->where('product_id', $product_id);
		$db_result = $this->db->get('product_goods_info');

		if ($db_result && $db_result->num_rows() > 0) {
			$data = $db_result->result();


			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
	
	
	public function get_email_data_post(){

	//$optval = $this->post('optval');
	$email_id = $_POST['optVal'];
		$result = $this->db->get_where('emails',array('email_id' => $email_id))->result();
		//var_dump($result);
		if ($result) {
			$data = $result;

			//var_dump($data);
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				
			];
			//var_dump($data);
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
		
		
	public function get_all_views_count_post(){
	$product_id = $_POST['post_id'];
		$result = $this->db->get_where('products',array('product_id' => $product_id))->result();
		//var_dump($result);
		if ($result) {
			foreach($result as $row){
				$views_count=$row->views_count;
			}
			$data = $views_count;


			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				
			];
			//var_dump($data);
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
		
		

	public function add_favourite_post()
	{
		//var_dump($_POST['product_good_id']);
		$customer_id = $_POST['customer_id'];
		$product_id = $_POST['product_id'];
		$is_fav = $_POST['is_fav'];
		$check_fav = '';
		
		$order_id= 0;
		
		
		$log_details="";

		if ($is_fav == 'yes') {
			
			$this->db->delete('favourite_product', array('customer_id' => $customer_id, 'product_id' => $product_id,));
			$check_fav = 'no';
		} else {
			
			$data = array(
				'customer_id' => $customer_id,
				'product_id' => $_POST['product_id'],
				'created_date' => date('Y-m-d'),
			);
			
			$result =	$this->db->insert('favourite_product', $data);
			$check_fav = 'yes';
		}
		$status = ['status' => 200, 'check_fav' => $check_fav, 'message' => 'Fav mark successfully'];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_product_detail_data_post()
	{
		$customer_id = $this->post('customer_id');
		$product_id = ($this->post('product_data'));
			
		$this->db->where('product_id', $product_id);
		$this->db->where('product_status', 'gathering');
		$db_result = $this->db->get('products');
		if ($db_result && $db_result->num_rows() > 0) {
			$data = $db_result->row();
			// var_dump($data);
			//var_dump($db_result->row());
			//foreach($db_result as $row1){
				//var_dump($row1);
				//$shiping_cost=$row1->$shiping_cost;
				//var_dump($shiping_cost);
			//$shiping_cost=(json_decode($shiping_cost));
			//var_dump((json_decode($shiping_cost)));
			//}
			///$this->db->where('product_id', $data->product_id);
			//$quentity_data = $this->db->get('product_goods_info')->result();

			$this->db->where('product_sub_category_id', $data->product_sub_category_id);
			$sub_category = $this->db->get('product_sub_category')->row();

			//$data->quentity_data = $quentity_data;
			$data->quentity_multiple = count($quentity_data);
			//$data->service_tax = $sub_category->service_tax;
			//$data->subcategory_name = $sub_category->product_sub_category_name;
			//

			$is_fav = 'no';
			if ($customer_id > 0) {
				$this->db->where('customer_id', $customer_id);
				$this->db->where('product_id', $data->product_id);
				$this->db->where('vendor_id', $data->vendor_id);
				$favourite_product = $this->db->get('favourite_product')->result();
				if ($favourite_product) {
					$is_fav = 'yes';
				}
			}
			$data->is_fav = $is_fav;

			$this->db->where('product_id !=', $data->product_id);
			$this->db->where('product_sub_category_id', $data->product_sub_category_id);
			$this->db->where('vendor_id', $data->vendor_id);
			$this->db->where('product_status', 'active');
			$this->db->where('admin_permission', 'active');
			$this->db->order_by('product_id', 'desc');
			$this->db->limit(10);
			$other_products = $this->db->get('products')->result();
			if ($other_products) {
				foreach ($other_products as $key => $row) {
					//var_dump($key);
					$this->db->where('product_id', $row->product_id);
					$quentity_data = $this->db->get('product_goods_info')->result();
					$row->product_url = base64_encode($row->product_id);

					$row->quentity_multiple = count($quentity_data);
					$row->quentity_data = $quentity_data;
					//var_dump($row);
					$is_fav = 'no';
					if ($customer_id > 0) {
						$this->db->where('customer_id', $customer_id);
						$this->db->where('product_id', $row->product_id);
						$this->db->where('vendor_id', $row->vendor_id);
						$favourite_product = $this->db->get('favourite_product')->result();
						if ($favourite_product) {
							$is_fav = 'yes';
						}
					}
					$row->is_fav = $is_fav;

					$this->db->where('product_sub_category_id', $row->product_sub_category_id);
					$sub_category_data = $this->db->get('product_sub_category')->row();

					$row->service_tax = $sub_category_data->service_tax;
				}
			}
			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
				'other_products' => $other_products,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
			 
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function get_checkout_delivery_datetime_post()
	{
		$vendor_id = $this->post('vendor_id');
		if ($vendor_id) {
			$this->db->where('vendor_id', $vendor_id);
			$vendor_data = $this->db->get('vendor_account')->row();
			if ($vendor_data) {
				$store_open_date = $vendor_data->store_open_time;
				$store_close_date = $vendor_data->store_close_time;
			} else {
				$store_open_date = '10:00:00';
				$store_close_date = '20:00:00';
			}
		} else {
			$store_open_date = '10:00:00';
			$store_close_date = '20:00:00';
		}
		$time_duration = 1;

		$today = date('Y-m-d');
		$date = $today;

		$timeslots = array();
		$time_data = array();

		$booking_start_time          = $store_open_date;
		$booking_end_time            = $store_close_date;
		$booking_frequency           = $time_duration * 60;
		$day_format					 = 1;
		$current = date('H:i');
		$time_slote = '';
		$timeslot = [];
		for ($i = strtotime($booking_start_time); $i < strtotime($booking_end_time); $i = $i + $booking_frequency * 60) {
			$slots = date("H:i", $i) . "-" . date("H:i", $i + $booking_frequency * 60);
			$formaterslots = date("h:i A", $i) . " to " . date("h:i A", $i + $booking_frequency * 60);
			$arrayName = array('time_lots' => $slots,  'formaterslots' => $formaterslots);
			array_push($timeslots, $arrayName);
		}
		$cur = date('H');
		$max = 23;
		//print_r($timeslots);
		foreach ($timeslots as $sl) {
			if ($cur < $max) {
				$slotcur = $cur;
				$nn =  substr($sl['time_lots'], 0, 2);
				if ($slotcur <= $nn && $nn < $max) {
					array_push($time_data, $sl);
				}
			} else {
				//$time_slote.= '<option disabled>Not available</option>';
			}
		}
		//print_r($time_data);

		$date_data = array();
		$data = array('date' => $today, 'formate_date' => date("M d", strtotime($today)), 'formate_day' => 'Today', 'time' => $time_data);
		array_push($date_data, $data);
		for ($n = 1; $n < 5; $n++) {
			$date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
			$formate_date = date("M d", strtotime($date));
			$formate_day = date("D", strtotime($date));
			$data = array('date' => $date, 'formate_date' => $formate_date, 'formate_day' => $formate_day, 'time' => $timeslots);
			array_push($date_data, $data);
		}
		//var_dump($date_data);

		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'data' => $date_data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function check_coupon_code_data_post()
	{

		$coupon_code = $this->post('code');
		$today = date('Y-m-d');
		$this->db->where('coupon_validity_start <=', $today);
		$this->db->where('coupon_validity_end >=', $today);
		$this->db->where('coupon_code', $coupon_code);
		$db_result = $this->db->get('coupon_code');
		///var_dump($this->db->last_query());
		if ($db_result && $db_result->num_rows() > 0) {
			$data = $db_result->result();


			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201,  'message' => 'There is no such code is active now.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function make_strip_payment_post()
	{
		require_once(APPPATH . 'libraries/stripe-php/init.php');

		$token = $this->post('token');
		$amount = $this->post('final_total');
		$email_id = $this->post('email_id');
		$currency = $this->post('currency');
		try {

			\Stripe\Stripe::setApiKey($this->config->item('PRIVATESTRIPEKEY'));

			// Get the token from the JS script
			$token = $this->post('token');
			// Create a Customer
			$customer = \Stripe\Customer::create(array(
				"email" => $this->post('email_id'),
				"source" => $token,
			));
			//var_dump($customer);	
			$customer_id = $customer->id;
			//var_dump($customer_id);			
			// Charge the Customer instead of the card
			$charge = \Stripe\Charge::create(array(
				"amount" => floatval($amount) * 100,
				"currency" => $currency,
				"customer" => $customer_id
			));
			// this line will be reached if no error was thrown above

			//print_r($charge) ;
			//print_r($charge->id) ;
			//print_r($charge->status) ;die();
			if ($charge->status == "succeeded") {
				//$transition_data=$charge->balance_transaction;
				$transition_data = $charge->id;

				$status = ['status' => 200, 'transition_data' => $transition_data, 'message' => 'successfully completed.'];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				//var_dump('payment process fail');
				$status = ['status' => 201,  'message' => 'Payment process fail'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		} catch (Stripe_CardError $e) {
			$status = ['status' => 201,  'message' => 'Payment Failed Due to card Reason ..!'];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		} catch (Stripe_InvalidRequestError $e) {
			// Invalid parameters were supplied to Stripe's API
			$status = ['status' => 201,  'message' => 'Payment Failed Due to Internal Reason ..!' . $e->getMessage() . ''];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		} catch (Stripe_AuthenticationError $e) {
			// Authentication with Stripe's API failed
			$status = ['status' => 201,  'message' => 'Payment Failed Due to Invalid autentication ..!'];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		} catch (Stripe_ApiConnectionError $e) {
			// Network communication with Stripe failed
			$status = ['status' => 201,  'message' => 'Payment Failed Due to api Reason ..!'];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		} catch (Stripe_Error $e) {
			// Display a very generic error to the user, and maybe send
			$status = ['status' => 201,  'message' => 'Payment Failed Due to server problem ..!' . $e->getMessage()];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Stripe
			$status = ['status' => 201,  'message' => 'Payment Failed Due to server problem1 ..!' . $e->getMessage()];
			$this->response($status, REST_Controller::HTTP_CREATED);

			exit();
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function place_order_for_item_data_post()
	{
		
		if (!$this->post('payment')) {
			$status = ['status' => 400,  'message' => 'select Payment is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else if (!$this->post('customer_id')) {
			$status = ['status' => 400,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$product_data = $this->post('product_data');

			$data = array(
				'customer_id' => $this->post('customer_id'),
				'customer_address_id' => $this->post('customer_address_id'),
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'address_line' => $this->post('address_line'),
				'city_name' => $this->post('city_name'),
				'state_name' => $this->post('state_name'),
				'zip_code' => $this->post('zip_code'),
				'country_name' => $this->post('country_name'),
				'phone_number' => $this->post('shipping_phone_number'),
				'delivery_date' => $this->post('delivery_date'),
				'delivery_time' => $this->post('delivery_time'),
				'currency_symbol' => $product_data[0]['currency_symbol'],
				'total_item' => $this->post('total_item'),
				'total_amount' => $this->post('total_amount'),
				'service_charge' => $this->post('service_charge'),
				'service_charge_amount' => $this->post('service_charge_amount'),
				'driver_tips_amount' => $this->post('driver_tips_amount'),
				'final_amount' => $this->post('final_amount'),				
				'payment_type' => $this->post('payment'),
				'transaction_id' => $this->post('transaction_id'),
				'payment_status' => $this->post('payment_status'),				
				'order_status' => 'Pending',
				'created_date' => date('Y-m-d'),
				'created_time' => date('H:i:s'),
			);
			//var_dump($data);
			$result = $this->db->insert('product_order', $data);
			$order_id = $this->db->insert_id();
			
			if ($product_data) {
				foreach ($product_data as $value) {
					//var_dump($value);
					//shoping_cart_total  = shoping_cart_total+(value.product_goods_price * value.quantity)+(totalservice_tax * value.quantity);
					//other_charges_total=other_charges_total+parseFloat(other_fees)+parseFloat(fullfilment_center_price)+parseFloat(if_any_price_two)+parseFloat(if_any_price_one)+parseFloat(sourcing_agent_fee);
					//$other_fees=$value->other_fees;
					$service_tax_percent =0;// $value['service_tax'];
					$service_tax_value = 0;//($value['service_tax'] * $value['product_goods_price']) / 100;
					$product_total = $value['quantity'] * $value['product_discounted_prize'];
					$data1 = array(
						'order_id' => $order_id,
						'customer_id' => $this->post('customer_id'),
						'customer_address_id' => $this->post('customer_address_id'),
						'first_name' => $this->post('first_name'),
						'last_name' => $this->post('last_name'),
						'address_line' => $this->post('address_line'),
						'city_name' => $this->post('city_name'),
						'state_name' => $this->post('state_name'),
						'zip_code' => $this->post('zip_code'),
						'country_name' => $this->post('country_name'),
						'phone_number' => $this->post('shipping_phone_number'),						
						'product_name' => $value['product_name'],
						'product_image' => $value['product_images'],						
						'product_discounted_prize' => $value['product_discounted_prize'],
						'product_currency_symbol' => '$',
						'product_actual_prize' => $value['product_actual_prize'],
						'product_weight' => $value['product_weight'],
						
						//'product_treatment' => $value['product_treatment'],		
						'product_id' => $value['product_id'],						
						//'product_origin' => $value['product_origin'],
						//'product_grade' => $value['product_grade'],
						//'product_clarity' => $value['product_clarity'],
						'product_color' => $value['product_color'],
						//'product_shape' => $value['product_shape'],
						'product_height' => $value['product_height'],
						//'product_feature' => $value['product_feature'],
						'quantity' => $value['quantity'],
						'product_category' => $value['product_category'],
						
						'payment_type' => $this->post('payment'),
						'payment_status' => $this->post('payment_status'),
						'product_total' =>$product_total,
						'service_tax_percent' => $service_tax_percent,
						'service_tax_value' => $service_tax_value,
						'detail_status' => 'Pending',
						'created_date' => date('Y-m-d'),
						'created_time' => date('H:i:s'),
					);
					//var_dump($data1);
					$result = $this->db->insert('product_order_detail', $data1);
									
					

				}
			}
				
				$this->load->model('Email_model', 'obj_email', TRUE);
			
				$customer_id = $this->post('customer_id');
				$this->db->where('customer_id', $customer_id);
				$customer_account = $this->db->get('customer_account')->result();
				//var_dump($customer_account);		
				foreach($customer_account as $values )
				{
				$customer_email_id=$values->customer_email;
				$customer_first_name =$values->first_name;
				$customer_last_name =$values->last_name;
				//var_dump($customer_email_id);
				//var_dump($vendor_first_name);
				//var_dump($vendor_last_name);
					$content ='Dear ' . $customer_first_name . ' <br /><br /><p>Congratulations! and Thankyou for shoping at</p><p>Your  shippment is on its way.we will send  a cinfirmation once your item has shiped.your items details are indicated below. </p>';
					$data112 = array(
						'subject' => 'Order cinfirmation ',
						'to' => $customer_email_id,
						'to_name' =>$customer_first_name . ' '. $customer_last_name,
						'from' => PROJECT_EMAIL,
						'msg' => $content,
					);
					//print_r($content);
					//var_dump($data112);
					//$result = $this->obj_email->send_mail_in_sendinblue($data11);
				}
		
				//'customer_id' => $this->post('customer_id'),
				//echo "order placed"; 
				$status = ['status' => 200, 'order_id'=>$order_id, 'payment'=>$this->post('payment'),'message' => 'Order Placed Successfully'];
				$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
	
		
	
	  /*
     * This function is used to get recent blog List
     */
    public function fatch_recent_blog_list_data_post() {
		$content='';
		
		$this->db->order_by('blog_id','DESC');
		$this->db->limit(10);
		$db_result = $this->db->get('admin_blogs');
		if ($db_result && $db_result->num_rows() > 0) {
			foreach($db_result->result() as $row){
				$blog_title = $row->blog_title;
				$blog_for_url = strtolower($blog_title);
				$blog_for_url = preg_replace('/[^a-zA-Z ]/s', '', $blog_for_url);
				$blog_for_url = str_replace(' ', '-', $blog_for_url);
				
				$content.=' <!-- recent-post-block -->';
				$content.='	<li>';
				$content.='	<a href="'.base_url().'publicpages/detail?id='.$row->blog_id.'-'.$blog_for_url.'">';
					$content.='	<div class="row">';
						$content.='	<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">';
							$content.='	<div style="margin-bottom: 25px; border: 3px solid #fff;;" class="recent-post-img zoomimg">';
								$content.='	<img src="'.base_url().'uploads/'.$row->blog_img.'" alt="">';
							$content.='	</div>';
						$content.='	</div>';
						$content.='	<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">';
							$content.='	<div class="recent-post-content">';
								$content.='	<h6 style="color:#5b5959;"class="recent-title">'.$row->blog_title.'</h6>';
								$content.='	<p  style="color:#5b5959; " class="meta font-italic">'.date("d M,Y", strtotime($row->date)).'</p>';
							$content.='	</div>';
						$content.='	</div>';
					$content.='	</div>';
				$content.='	</a>';
				$content.='	</li>';
				$content.='	<!-- /.recent-post-block --> ';
			}
			
			$status = ['status' => 200 ,  
						'message' => 'fetched successfully',
						'data' => $content,
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201 ,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
	

	
}
