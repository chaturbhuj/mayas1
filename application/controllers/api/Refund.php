<?php

defined('BASEPATH') or exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit', '64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Refund extends REST_Controller
{

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		if ($this->session->userdata('country') == 'India') {
			date_default_timezone_set('Asia/Singapore');
		} else {
			date_default_timezone_set('Canada/Saskatchewan');
		}

		// Configure limits on our controller methods 
		// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}
		
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
	}

	public function refund_campaign_post(){
		$refund_approval=$_POST['refund_approval'];	
		$refund_approval_id=json_encode($refund_approval);
		
		if (!$_POST['product_id_value']) {
			$status = ['status' => 400,  'message' => 'prodct details are required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$product_id_value = $_POST['product_id_value'];
			//var_dump($product_id_value);
			$data = array(
				'refund_pyament_status' => 'pending for approval',
				'refund_customer'=>$refund_approval_id,
				'refund_created_date'=>date('Y-m-d'),
				'product_status'=>'waiting for refund approval' 
			);
			
			$return_val = $this->db->update('products', $data, array('product_id' => $_POST['product_id_value']));
			
				
				//var_dump($refund_approval);
				
				foreach($refund_approval as $rows){
					//var_dump($rows);
					$customer_id=$rows;
					//var_dump($customer_id);
					$data=array(
						'asked_refund'=>'pending for approval',
					);
					
					$return_val = $this->db->update('product_order_detail', $data, array('customer_id' =>$customer_id , 'product_id' => $_POST['product_id_value']));
					//var_dump($return_val);
				}
			
			
		}
		
		$arr_response = array('status' => 500);
		//$refund_approval_id=json_encode($refund_approval);
		//$refund_approval_id=json_decode($refund_approval);
		//var_dump($refund_approval_id);
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['refund_approval'] = $refund_approval;
			$arr_response['message'] = 'refund initiated successfully';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
		echo json_encode($arr_response);
	}
	
	
	

	
}
