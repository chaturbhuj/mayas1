<?php

defined('BASEPATH') or exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit', '64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Vendor_account extends REST_Controller
{

	function __construct()
	{
		// Construct the parent class
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');

		// Configure limits on our controller methods 
		// Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method == "OPTIONS") {
			die();
		}
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
	}

	public function update_profile_post()
	{
		if (!$this->post('full_name')) {
			$status = ['status' => 400,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else if (!$this->post('vendor_email')) {
			$status = ['status' => 400,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$data = array(
				'full_name' => $this->post('full_name'),
				'currency' => $this->post('currency'),
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $this->post('vendor_id')));

			$status = ['status' => 200, 'message' => 'Profile updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_profile_post()
	{
		if (!$this->post('vendor_id')) {
			$status = [
				['status' => 400,  'message' => 'vendor_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$vendor_id = $this->post('vendor_id');
			$db_result = $this->db->get_where('vendor_account', array('vendor_id' => $vendor_id));

			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {


					if (!array_key_exists($row->vendor_id, $data)) {
						$data[$row->vendor_id] = array();
					}
					if (array_key_exists($row->vendor_id, $data)) {
						$data[$row->vendor_id] = array(
							'vendor_id' => $row->vendor_id,
							'vendor_email' => $row->vendor_email,
							'full_name' => $row->full_name,
							'currency' => $row->currency,
							'store_open_time' => $row->store_open_time,
							'store_close_time' => $row->store_close_time,
						);
						array_push($data_value, $data[$row->vendor_id]);
					}
				}
				$status = [
					'status' => 200,
					'message' => 'fetched successfully',
					'data' => $data_value,
				];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}

	public function check_vendor_old_password_post()
	{

		$pass = sha1('admin' . (md5('admin' . $this->post('currentpassword'))));
		$db_result = $this->db->get_where('vendor_account', array('vendor_password' => $pass, 'vendor_id' => $this->post('param'),));

		if ($db_result->num_rows() > 0) {
			echo 'true';
		} else {
			echo 'false';
		}
	}

	public function update_password_post()
	{
		if (!$this->post('new_password')) {
			$status = ['status' => 400,  'message' => 'New Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			#Generate hashed password.
			$pass = sha1('admin' . (md5('admin' . $this->post('new_password'))));
			//$pass=$_POST['user_password'];
			$data = array(
				'vendor_password' => $pass
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $_POST['vendor_id']));

			$status = ['status' => 200, 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	//--------------------------------------------------------------------------
	/**
	 * This function is used to upload image.
	 */
	public function upload_profile_image_post()
	{
		#array to hold the response values to be displayed
		$arr_response = array();

		$info = pathinfo($_FILES['myFile']['name']);
		$ext = $info['extension']; // get the extension of the file

		if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" || $_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
			($_FILES["myFile"]["size"] < 30485760)
		) {
			$file = $info['filename'];
			$filename = $this->post('image_cat') . '_' . uniqid() . '.' . $ext;
			$target = './uploads/' . $filename;
			$file = $_FILES['myFile']['tmp_name'];

			move_uploaded_file($file, $target);
			$data = array(
				'vendor_img' => $filename
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $this->post('vendor_id')));

			$arr_response['status'] = 200;
			$arr_response['filename'] = $filename;
		} else {
			$arr_response['status'] = FAILED;
			$arr_response['message'] = "Not a valid File";
		}
		echo json_encode($arr_response);
	}

	public function update_list_item_post()
	{
		if (!$this->post('title')) {
			$status = ['status' => 400,  'message' => 'Tital is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else if (!$this->post('vendor_id')) {
			$status = ['status' => 400,  'message' => 'something went worng.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$item_id = $this->post('item_id');
			$image = json_encode($this->post('images'));
			$amenities_id = json_encode($this->post('amenities_id'));
			$event_type_id = json_encode($this->post('event_type_id'));
			$data = array(
				'vendor_id' 		=> $this->post('vendor_id'),
				'title' 			=> $this->post('title'),
				'guest_capacity' 	=> $this->post('guest_capacity'),
				'area' 				=> $this->post('area'),
				'price_hour' 		=> $this->post('price_hour'),
				'price_day'			=> $this->post('price_day'),
				'amenities_id' 		=> $amenities_id,
				//'amenities' 		=> $this->post('amenities'),
				'event_type_id' 	=> $event_type_id,
				//'event_type' 		=> $this->post('event_type'),
				'discription' 		=> $this->post('discription'),
				'feactures' 		=> $this->post('feactures'),
				'address' 			=> $this->post('address'),
				'city' 				=> $this->post('city'),
				'state' 			=> $this->post('state'),
				'country' 			=> $this->post('country'),
				'latitude' 			=> $this->post('latitude'),
				'longitude' 		=> $this->post('longitude'),
				'primary_image' 	=> $this->post('primary_image'),
				'images' 			=> $image,
				'video_link' 		=> $this->post('video_link'),
				'created_date' 		=> date('Y-m-d'),
				'created_time' 		=> date('H:i:s'),
			);
			if ($item_id == 0) {
				$msg = 'added successfully';
				$result = $this->db->insert('vendor_list_items', $data);
			} else {
				$msg = 'updated successfully';
				$result = $this->db->update('vendor_list_items', $data, array('item_id' => $item_id));
			}
			// var_dump($this->db->last_query());
			if ($result) {
				$status = ['status' => 200, 'message' => $msg];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}

	public function product_submit1_post(){
		
		$order_id=0;
		$customer_id=0;
		
		if (!$this->post('product_name')) {
			$status = ['status' => 400,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$product_id = $this->post('product_id');
			
			$vendor_id=$this->session->userdata('customer_id');
			
			$data = array(
				'vendor_id' 				=> $this->session->userdata('customer_id'),
				'main_category_id' 			=> $this->post('main_category_id'),
				'product_category_id' 	 	=> $this->post('product_category_id'),
				'product_name_other' 	 	=> $this->post('other_product_category'),
				'product_sub_category_other'=> $this->post('other_product_sub_category'),
				'product_sub_category_id'	=> $this->post('product_sub_category_id'),
				'product_name' 				=> $this->post('product_name'),
				'product_image'				=> json_encode($this->post('product_image')),
				'other_ecommerce_price'		=> json_encode($this->post('other_ecommerce_price')),//
				'sourcing_agent'			=> json_encode($this->post('sourcing_agent')),//
				'shiping_cost'				=> json_encode($this->post('shiping_cost')),//
				'factory_list'				=> json_encode($this->post('factory_list')),//
				'influencer_list'			=> json_encode($this->post('influencer_list')),//
				'discount_of'				=> $this->post('discount_of'),//
				'needed_input'				=> $this->post('needed_input'),//
				'sending_fullfilment_price'	=> $this->post('sending_fullfilment_price'),//
				'fullfilment_center_price'	=> $this->post('fullfilment_center_price'),//
				'can_returned'				=> $this->post('can_returned'),//
				'can_changed'				=> $this->post('can_changed'),//
				'if_any_price_one'			=> $this->post('if_any_price_one'),//
				'if_any_price_two'			=> $this->post('if_any_price_two'),//
				'sourcing_agent_fee'		=> $this->post('sourcing_agent_fee'),//
				'other_fees'				=> $this->post('other_fees'),//
				'product_description'		=> $this->post('product_description'),
				'shiping_details'			=> $this->post('shiping_details'),
				//'product_status'			=> $this->post('product_status'),
				'product_tags'				=> $this->post('product_tags'),
				'product_other_tags'		=> $this->post('product_other_tags'),
				'fullfilment_center'		=> $this->post('fullfilment_center'),
				'sending_fullfilment_steps'	=> $this->post('sending_fullfilment_steps'),
				'created_date' 				=> date('Y-m-d'),
				'created_time' 				=> date('H:i:s'),
				'percentage_id'				=> $this->post('percentage_id'),
				'percentage_name'			=> $this->post('percentage_name'),
				'percentage_name_other'			=> $this->post('other_percentage'),
				'brand_id'					=> $this->post('brand_id'),
				'brand_name'				=> $this->post('brand_name'),
				'brand_name_other'				=> $this->post('other_brand'),
				'colour_id'					=> $this->post('colour_id'),
				'colour_name'				=> $this->post('colour_name'),
				'colour_name_other'				=> $this->post('other_colour'),
				'size_id'					=> $this->post('size_id'),
				'size_name'					=> $this->post('size_name'),
				'size_name_other'					=> $this->post('other_size'),
				'specialty_sizes_id'		=> $this->post('specialty_sizes_id'),
				'specialty_sizes_name'		=> $this->post('specialty_sizes_name'),
				'smart_id'					=> $this->post('smart_id'),
				'smart_name'				=> $this->post('smart_name'),
				'voice_assistant_id'		=> $this->post('voice_assistant_id'),
				'voice_assistant_name'		=> $this->post('voice_assistant_name'),
				'display_id'				=> $this->post('display_id'),
				'display_name'				=> $this->post('display_name'),
				'display_name_other'				=> $this->post('other_display'),
				'screen_type_id'			=> $this->post('screen_type_id'),
				'screen_type_name'			=> $this->post('screen_type_name'),
				'screen_type_name_other'			=> $this->post('other_screen_type'),
				'resolutions_id'			=> $this->post('resolutions_id'),
				'resolutions_name'			=> $this->post('resolutions_name'),
				'hdr_id'					=> $this->post('hdr_id'),
				'hdr_name'					=> $this->post('hdr_name'),
				'screen_size_id'			=> $this->post('screen_size_id'),
				'screen_size_name'			=> $this->post('screen_size_name'),
				'screen_size_name_other'			=> $this->post('other_screen_size'),
				'processor_id'				=> $this->post('processor_id'),
				'processor_name'			=> $this->post('processor_name'),
				'processor_name_other'			=> $this->post('other_processor'),
				'processor_type_id'			=> $this->post('processor_type_id'),
				'processor_type_name'		=> $this->post('processor_type_name'),
				'processor_type_name_other'		=> $this->post('other_processor_type'),
				'hard_drive_type_id'		=> $this->post('hard_drive_type_id'),
				'hard_drive_type_name'		=> $this->post('hard_drive_type_name'),
				'hard_drive_type_name_other'		=> $this->post('other_hard_drive_type'),
				'touchscreen_id'			=> $this->post('touchscreen_id'),
				'touchscreen_name'			=> $this->post('touchscreen_name'),
				'operating_system_id'		=> $this->post('operating_system_id'),
				'operating_system_name'		=> $this->post('operating_system_name'),
				'operating_system_name_other'		=> $this->post('other_operating_system'),
				'refurbished_id'			=> $this->post('refurbished_id'),
				'refurbished_name'			=> $this->post('refurbished_name'),
				'ram_id'					=> $this->post('ram_id'),
				'ram_name'					=> $this->post('ram_name'),
				'ram_name_other'					=> $this->post('other_ram'),
				'material_id'				=> $this->post('material_id'),
				'material_name'				=> $this->post('material_name'),
				'age_id'					=> $this->post('age_id'),
				'age_name'					=> $this->post('age_name'),
			);
			if ($product_id == 0) {
				$msg = 'Added successfully';
				$log_details="product added";
				$result = $this->db->insert('products', $data);
				$pro_id = $this->db->insert_id();
				$data_one = array(
				'previous_campaign_product_id'  => $pro_id,
				);
				$result = $this->db->update('products', $data_one, array('product_id' => $pro_id));
				$data['log_data'] = $this->obj_aa->update_logdetails($pro_id,$order_id ,$vendor_id,$customer_id ,$log_details,);
			}else{
				$msg = 'Updated successfully';
				$log_details="product edited";
				$result = $this->db->update('products', $data, array('product_id' => $product_id));
				$pro_id = $product_id;
				$this->db->delete('product_goods_info', array('product_id' => $product_id));
				$data['log_data'] = $this->obj_aa->update_logdetails($pro_id,$order_id ,$vendor_id,$customer_id ,$log_details,);
			}
			if ($this->post('goodsarr')) {
				foreach ($this->post('goodsarr') as $val) {
					$data1 = array(
						'product_id'  => $pro_id,
						'vendor_id'  => $this->post('vendor_id'),
						'main_category_id'  => $this->post('main_category_id'),
						'product_category_id'  => $this->post('product_category_id'),
						'product_sub_category_id'  => $this->post('product_sub_category_id'),
						'goods_id'  => $val['goods_id'],
						'goods_name'  => $val['goods_name'],
						'goods_value'  => $val['goods_value'],
						'goods_price'  => $val['goods_price'],
						'colour_id'  => $val['colour_id'],
						'colour_name'  => $val['colour_name'],
						'size_id'  => $val['size_id'],
						'size_name'  => $val['size_name'],
						'created_date'  => date('Y-m-d'),
						'created_time'  => date('H:i:m'),
					);
					$this->db->insert('product_goods_info', $data1);
				}
			}
			
			
			if ($result) {
				$status = ['status' => 200, 'message' => $msg];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}
	public function product_remove_post(){
	   	$product_id = $_POST['product_id'];
	   	$result = $this->db->delete('products' , array('product_id'=> $product_id));
	   	$result1 = $this->db->delete('product_goods_info' , array('product_id'=> $product_id));
	   
		$arr_response['status'] = 200;
		$arr_response['message'] = 'Product remove successfully';
		 
		echo json_encode($arr_response);
   	}

  //end of user_search_data_post----->
	
		public function sell_crowdbuy_submit_post()
		{
		if (!$this->post('product_name')) {
			$status = ['status' => 400,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$product_id = $this->post('product_id');
		
			$data = array(
				'vendor_id' 				=> $this->session->userdata('customer_id'),
				'product_name' 				=> $this->post('product_name'),
				'product_title' 			=> $this->post('product_title'),
				'Product_Price' 			=> $this->post('Product_Price'),
				'product_image'				=> json_encode($this->post('product_image')),
				//'other_ecommerce_price'		=> json_encode($this->post('other_ecommerce_price')),//
				'shiping_cost'				=>  $this->post('shiping_cost'),//
				'needed_input'				=> $this->post('needed_input'),//
				//'sending_fullfilment_step'	=> $this->post('sending_fullfilment_step'),
			//	'fullfilment_center'		=> $this->post('fullfilment_center'),
				//'if_any_otehr_fullfillment_step'=> $this->post('if_any_otehr_fullfillment_step'),
				//'if_any_fullfillment_price'	=> $this->post('if_any_fullfillment_price'),
				//'if_any_price_one'			=> $this->post('if_any_price_one'),
				//'if_any_price_two'			=> $this->post('if_any_price_two'),
				
				//'other_fees'				=> $this->post('other_fees'),
				'product_description'		=> $this->post('product_description'),
				'shiping_details'			=> $this->post('shiping_details'),
				'status'					=>'Added',
				'created_date' 				=> date('Y-m-d'),
				'created_time' 				=> date('H:i:s'),	
			
			);
			//echo "dsshgs";
			if ($product_id == 0) {
				$msg = 'Added successfully';
				$result = $this->db->insert('seller', $data);
				$pro_id = $this->db->insert_id();
			} else {
				
				$msg = 'Product Updated successfully';
				$result = $this->db->update('seller',$data ,array('product_id'=>$this->post('product_id')) );
			}
			if ($result) {
				$status = ['status' => 200, 'message' => $msg];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_order_list_data_post()
	{
		if (!$this->post('vendor_id')) {
			$status = [
				['status' => 400,  'message' => 'vendor_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$vendor_id = $this->post('vendor_id');

			if ($this->post('start_date')) {
				$start_date = $this->post('start_date');
				$end_date = $this->post('end_date');
				$this->db->where("created_date between '$start_date' And '$end_date'");
			}
			if ($this->post('country_name')) {
				$country_name = $this->post('country_name');
				$this->db->where('country_name', $country_name);
			}
			if ($this->post('state_name')) {
				$state_name = $this->post('state_name');
				$this->db->where('state_name', $state_name);
			}
			if ($this->post('city_name')) {
				$city_name = $this->post('city_name');
				$this->db->where('city_name', $city_name);
			}
			if ($this->post('deshboard_order') == 'yes') {
				$this->db->limit(10);
				$this->db->where('order_status', 'Pending');
			}

			$this->db->order_by('order_id', 'desc');
			$this->db->where('order_status !=', 'Completed');
			$db_result = $this->db->get_where('product_order', array('vendor_id' => $vendor_id));

			//var_dump($this->db->last_query());

			$data = $db_result->result();

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	public function update_order_detail_status_post()
	{
		if (!$this->post('order_id')) {
			$status = ['status' => 400,  'message' => 'status is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {

			$order_id = $this->post('order_id');
			$change_status = $this->post('change_status');

			$data = array(
				'order_status' => $change_status,
			);
			$return_val = $this->db->update('product_order', $data, array('order_id' => $order_id));

			$status = ['status' => 200, 'message' => 'Status updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_order_detail_list_data_post()
	{
		if (!$this->post('id')) {
			$status = [
				['status' => 400,  'message' => 'id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$order_id = $this->post('id');
			$db_result = $this->db->get_where('product_order_detail', array('order_id' => $order_id));
			$data = $db_result->result();

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_transaction_list_data_post()
	{
		if (!$this->post('vendor_id')) {
			$status = [
				['status' => 400,  'message' => 'vendor_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$vendor_id = $this->post('vendor_id');

			if ($this->post('start_date')) {
				$start_date = $this->post('start_date');
				$end_date = $this->post('end_date');
				$this->db->where("created_date between '$start_date' And '$end_date'");
			}
			if ($this->post('country_name')) {
				$country_name = $this->post('country_name');
				$this->db->where('country_name', $country_name);
			}
			if ($this->post('state_name')) {
				$state_name = $this->post('state_name');
				$this->db->where('state_name', $state_name);
			}
			if ($this->post('city_name')) {
				$city_name = $this->post('city_name');
				$this->db->where('city_name', $city_name);
			}

			if ($this->post('is_paid') == 'yes') {
				$this->db->where('vendor_payment_status', 'paid');
			}
			if ($this->post('is_unpaid') == 'yes') {
				$this->db->where('vendor_payment_status', 'Pending');
			}

			$this->db->order_by('order_id', 'desc');
			$this->db->where('order_status', 'Completed');
			$this->db->where('vendor_id', $vendor_id);
			$db_result = $this->db->get('product_order');

			//var_dump($this->db->last_query());

			$data = $db_result->result();

			$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $data,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function add_vender_payment_request_post()
	{
		if (!$this->post('order_id')) {
			$status = [
				['status' => 400,  'message' => 'id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$order_id = $this->post('order_id');
			$data = array(
				'vendor_id' => $this->post('vendor_id'),
				'payment_amount' => $this->post('total'),
				'status' => 'Requested',
				'requested_date_time' => date('Y-m-d H:i:s'),
			);
			$insertid = $this->db->insert('vendor_payment_detail', $data);
			foreach ($order_id as $id) {
				$data = array(
					'vendor_payment_status' => 'Requested',
					'vendor_payment_id' => $insertid,
				);
				$return_val = $this->db->update('product_order', $data, array('order_id' => $id['order_id']));
			}
			if ($insertid) {
				$status = [
					'status' => 200,
					'message' => 'Request Send Successfully',
					'data' => $data,
				];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}

	//-------------------------------------------------------------------------
	/*
     * This function is used to get loggedin user details
     */
	public function fatch_deshboard_data_post()
	{
		if (!$this->post('vendor_id')) {
			$status = [
				['status' => 400,  'message' => 'id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		} else {
			$owner_id = $this->post('vendor_id');

			$shop_no = count($this->db->get_where('vendor_stores', array('vendor_id' => $owner_id))->result_array());
			$order_no = count($this->db->get_where('product_order', array('vendor_id' => $owner_id))->result_array());
			$product_no = count($this->db->get_where('products', array('vendor_id' => $owner_id))->result_array());
			$sql = "SELECT sum(total_amount) as total,currency_symbol	 FROM `product_order` where vendor_id='$owner_id' AND vendor_payment_status='paid'";
			$total_earning = $this->db->query($sql)->row();
			if ($total_earning->total) {
				$total_earning = $total_earning->currency_symbol . $total_earning->total;
			} else {
				$total_earning = '0';
			}

			$status = [
				'status' => 200,
				'message' => 'Request Send Successfully',
				'order_no' => $order_no,
				'shop_no' => $shop_no,
				'product_no' => $product_no,
				'total_earning' => $total_earning,
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
	}
}
