<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/CreatorJwt.php';

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->objOfJwt = new CreatorJwt();
    }
	
	
	
	public function login_post() {
		if(!$this->post('user_email')){
			$status = ['status' => 400 ,  'message' => 'mobile number address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('user_password')){
			$status =['status' => 400 ,  'message' => 'Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			$this->db->where('user_type', $this->post('user_type'));
			$this->db->where('contact_no', $this->post('user_email'));
			$this->db->where('status', 'active');
			$this->db->where('user_password', sha1('admin' . (md5('admin' . $this->post('user_password')))));
			$db_result = $this->db->get('lm_user');
			//print_r($this->db->last_query());
			if ($db_result && $db_result->num_rows() > 0) {
				#login credentials matched
				$row = $db_result->row();
				
				$db_result2 = $this->db->get_where('lm_user_location',array('user_id' =>$row->user_id));
				if ($db_result2 && $db_result2->num_rows() > 0) {
					$user_select_location = 'Yes';
				}else{
					$user_select_location = 'No';
				}
				
				if($this->post('payer_id')){
					$payer_id =  $this->post('payer_id');
					$result = $this->db->query("UPDATE `lm_user` SET `payer_id` = $payer_id WHERE `lm_user`.`user_id` = $row->user_id;");
					
				}
				
				$status = ['status' => 200 ,  
							'message' => 'Login Successfully.',
							'user_select_location' => $user_select_location,
							'data' => array(
								'user_id' => $row->user_id,
								'user_moblie_number' => $row->contact_no,
								'loggedIN' => 1,
								'user_type' => $row->user_type,
								'full_name' => $row->full_name,
								'user_img' => $row->user_img,
							)
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{				
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Invalid mobile number or Password.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	
	
	
	
	 public function register1_get() {
		 echo 'ok';
	 }
	 public function register_post() {
		if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'First Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('last_name')){
			$status = ['status' => 400 ,  'message' => 'Last Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('user_email')){
			$status = ['status' => 400 ,  'message' => 'Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('user_password')){
			$status = ['status' => 400 ,  'message' => 'Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			
			$db_result = $this->db->get_where('users',array('user_email'=>$this->post('user_email')));
			//print_r($this->db->last_query());die();
			if($db_result->num_rows() !=0){
				$status = ['status' => 201 ,  'message' => 'mobile number Already exist try another mobile number.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}else{
				#Generate hashed password.
				$hashed_password = sha1('admin' . (md5('admin' . $this->post('user_password'))));
				#generate random number
				if($this->post('date_ob') == 'empty'){
					$dob = null;
					$pan_number = null;
					$sur_name = null;
					$gst_number = $this->post('gst_number');
				}else{
					$pan_number = $this->post('pan_number');
					$sur_name = $this->post('sur_name');
					$dob = $this->post('date_ob');
					$gst_number = null;
				}
				$verify_code =rand(1111,9999);
				$data = array(
					//'user_type' => $this->post('user_type'),
					'full_name' => $this->post('full_name'),
					'vendor_bussiness_cat' => $this->post('bussiness_cat'),
					'bussiness_name' => $this->post('bussiness_name'),
					'delivery_area' => $this->post('full_address'),
					'contact_no' => $this->post('user_moblie_number'),
					'user_type' => $this->post('user_type'),
					'gst_number' => $gst_number,
					'pan_number' => $pan_number,
					'dob' => $dob,
					'sur_name' => $sur_name,
					'user_password' => $hashed_password,
					'verify_code' => $verify_code,
					'created_date' => date('Y-m-d'),
					'created_time' => date('H:i:s'),
					
				);
				$result = $this->db->insert('lm_user', $data);
				$insert_id = $this->db->insert_id();
					
				if ($insert_id) {
					
					
					$status = ['status' => 200 ,  
								'mno' => $this->post('user_moblie_number') ,  
								'id' => $insert_id,  
								'verify_code' => $verify_code,  
								'message' => 'Your account has been successfully created. OTP has been sent to your Mobile Number, Verify your Mobile Number .',
							];
					$this->set_response($status, REST_Controller::HTTP_OK);
					
				} else {
					#query failed or no matching values found
					$status = [
						['status' => 201 ,  'message' => 'Something went Wrong! Please try again'],
					];
					$this->response($status, REST_Controller::HTTP_CREATED);
				}
		}
	  }
    }
	
	 public function resendotp_post() {
		if(!$this->post('user_id')){
			$status = ['status' => 400 ,  'message' => 'user_id is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		
		}else{
			
			
			$verify_code =rand(1111,9999);
			$data = array(
				'verify_code' => $verify_code,
			);
			$result = $this->db->update('lm_user', $data,array('user_id'=>$this->post('user_id')));
			
			$status = ['status' => 200 ,  
						
						'verify_code' => $verify_code,  
						'message' => 'resend Oto has been successfully.',
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
				
			
		}
	  }
    
	 public function resendotp_forgetpassword_post() {
		if(!$this->post('user_mobile_number')){
			$status = ['status' => 400 ,  'message' => 'user_mobile_number is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		
		}else{
			
			
			$verify_code =rand(1111,9999);
			$data = array(
				'verify_code' => $verify_code,
			);
			$result = $this->db->update('lm_user', $data,array('contact_no'=>$this->post('user_mobile_number')));
			
			$status = ['status' => 200 ,  
						
						'verify_code' => $verify_code,  
						'message' => 'resend Oto has been successfully.',
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
				
			
		}
	  }
    
	
	 public function otp_verify_post() {
		if(!$this->post('otp_verify')){
			$status = ['status' => 400 ,  'message' => 'OTP verify is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		
		}else{
			$this->db->where('verify_code',$this->post('otp_verify'));
			$this->db->where('user_id',$this->post('user_id'));
			$db_result = $this->db->get('lm_user');
			if($db_result->num_rows() ==0){
				$status = ['status' => 201 ,  'message' => 'wrong OTP please enter correct OTP.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}else{
				$row = $db_result->row();
				if($row->user_type=='vendor'){
					$data = array(
					'status' => 'pending',
					);
				}else{
					$data = array(
					'status' => 'active',
					);
				}
				
				$result = $this->db->update('lm_user', $data,array('user_id'=>$this->post('user_id')));
				if($result){	
					$status = ['status' => 200 ,  
								 
								'message' => 'Your OTP verification has been successfully done.',
							];
					$this->set_response($status, REST_Controller::HTTP_OK);
				}
			}
	  }
    }
	
	
	 public function send_email_forget_password_post() {
		 $user_img ='';
		if(!$this->post('user_email')){
			$status =['status' => 400 ,  'message' => 'User Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$user_email = $this->post('user_email');
			$v_token = false;
			while($v_token != true){
				$capital = implode('',range('A','Z'));
				$small = implode('',range('a','z'));
				$number = implode('',range('0','20'));
				$token = substr(str_shuffle($capital.$small.$number),40);
				$this->db->where('verify_code',$token);
				$res = $this->db->get('lm_user');
				if($res->num_rows() > 0){
					$v_token =  false;
				}else{
					$v_token =  true;
				}
				#$v_token = $this->check_token($token);
			}
			$Array = array(
				'verify_code' => $token,
			);
			$this->db->set($Array);
			$this->db->where('user_email',$user_email);
			$arr_value = array();
			if($this->db->update('lm_user')){
				/*fetching the user data*/
				$user_email = trim($_POST['user_email']);
				$this->db->where(array('user_email'=>$user_email));
				$res = $this->db->get('lm_user');
				
				if($res && $res->num_rows() == 1){
					
					$data = array();
					foreach ($res->result() as $row) {
						$user_img = $row->user_img;
						if (!array_key_exists($row->user_id, $data)) {
							$data[$row->user_id] = array();
						}
						if (array_key_exists($row->user_id, $data)) {
							$data[$row->user_id] = array(
								'user_id' => $row->user_id,
								'full_name' => $row->full_name,
								'user_email' => $row->user_email,
								'verify_code' => $row->verify_code,
							 );
							array_push($arr_value, $data[$row->user_id]);
						}
					}
				}else{
					$status =['status' => 400 ,  'message' => 'Email address does not exist'];
					$this->response($status, REST_Controller::HTTP_CREATED);
				}
				$e = ($user_email);
				$verification_link = $token;
				$this->load->library('email');
				$to = $user_email;
				$subject = "Forget Password Request.";
				
				$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
				$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.BASEPATH_.'"><img src="'.LOGO_URL.'" alt="'.WEBSITE_NAME.'" style="width: 100px;"></a></div><br/><br/>';
				$message .= 'Dear '.$arr_value[0]['full_name'].'<br/><br/>';
				$message .= '<p>Please Click the below to change your password</p>';
				$message .= '<a href="'.BASEPATH_.'#/reset_password?e='.$e.'&v='.$verification_link.'">Click Here</a>'; 
				$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
				$message .= '</div></div>';
				#print_r($message);
				$config=array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);

				$this->email->initialize($config);
				$this->email->from(WEBSITE_EMAIL, WEBSITE_NAME);
				$this->email->to($to); 
				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->send();
				$status = ['status' => 200 ,'message' => 'Email sent to given email address successfully.', 'data' => $user_img];
				$this->set_response($status, REST_Controller::HTTP_OK);
				
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
				
			}
		}
	}
	
	public function rest_user_password_check_post() {
		$this->db->where(array('contact_no'=>$this->post('user_mobile_number'),'verify_code'=>$this->post('verify_code')));
		$res = $this->db->get('lm_user');
		if($res && $res->num_rows() == 1){
				
			
			$status = ['status' => 200 , 'message' => 'OTP Matched'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
		}else{
			$status = ['status' => 201 ,  'message' => 'Verification OTP might be expire. Try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		
    }
	
	 public function rest_user_password_post() {
		$this->db->where(array('contact_no'=>$this->post('user_mobile_number'),'verify_code'=>$this->post('verify_code')));
		$res = $this->db->get('lm_user');
		if($res && $res->num_rows() == 1){
			#Generate hashed password.
			$hashed_password = sha1('admin' . (md5('admin' . $this->post('new_passwd'))));
			$data = array(
				'user_password' => $hashed_password,
			);
			$result = $this->db->update('lm_user', $data, array('contact_no'=>$this->post('user_mobile_number'),'verify_code'=>$this->post('verify_code')));	
			
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
		}else{
			$status = ['status' => 201 ,  'message' => 'Verification token might be expire. Try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		
    }

	
	

	
	public function update_profile_post() {
		if(!$this->post('full_name')){
			$status = ['status' => 400 ,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('contact_no')){
			$status = ['status' => 400 ,  'message' => 'Mobile number is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
			
		}else{			
			$data = array(
				'full_name' => $this->post('full_name'),
				'contact_no' => $this->post('contact_no'),
				'delivery_door' => $this->post('delivery_door'),
				'delivery_appart' => $this->post('delivery_appart'),
				'delivery_street1' => $this->post('delivery_street1'),
				'delivery_street2' => $this->post('delivery_street2'),
				'delivery_area' => $this->post('delivery_area'),
			);
			$result = $this->db->update('lm_user', $data, array('user_id' => $this->post('user_id')));
												
			$status = ['status' => 200 , 'message' => 'Profile updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_profile_post() {
		if(!$this->post('user_id')){
			$status = [
				['status' => 400 ,  'message' => 'user_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('lm_user', array('user_id' => $user_id));
			
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					$delivery_door = $row->delivery_door ;
					$delivery_appart = $row->delivery_appart ;
					$delivery_street1 = $row->delivery_street1 ;
					$delivery_street2 = $row->delivery_street2 ;
					$delivery_area = $row->delivery_area ;
					if($row->delivery_door == null){
						$delivery_door = '';
					}
					if($row->delivery_appart == null){
						$delivery_appart = '';
					}
					if($row->delivery_street1 == null){
						$delivery_street1 = '';
					}
					if($row->delivery_street2 == null){
						$delivery_street2 = '';
					}
					if($row->delivery_area == null){
						$delivery_area = '';
					}
							
					if (!array_key_exists($row->user_id, $data)) {
						$data[$row->user_id] = array();
					}
					if (array_key_exists($row->user_id, $data)) {
						$data[$row->user_id] = array(
							'user_id' => $row->user_id,
							'full_name' => $row->full_name,
							'contact_no' => $row->contact_no,
							'user_img' => $row->user_img,
							'user_type' => $row->user_type,
							'created_date' => $row->created_date,
							'delivery_door' => $delivery_door,
							'delivery_appart' => $delivery_appart,
							'delivery_street1' => $delivery_street1,
							'delivery_street2' => $delivery_street2,
							'delivery_area' => $delivery_area,
							//'user_address' => $row->user_address,
							'user_location' => $row->user_location,
							'dob' => $row->dob,
							
						);
						array_push($data_value, $data[$row->user_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_refund_money_post() {
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('lm_user_refund_availble', array('Fk_user_id' => $user_id));
			if ($db_result && $db_result->num_rows() > 0) {
				$amount = 0;
				$data_value = array();
				foreach ($db_result->result() as $row) {
					$amount = $row->amount ;
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'refund_amount' => $amount,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
	}
	
	public function send_request_to_refund_post() {
		$user_id = $this->post('user_id');
		$db_result = $this->db->delete('lm_user_refund_availble', array('Fk_user_id' => $user_id));
		$data = array(
					'Fk_user_id' => $user_id,
					'acc_no' => $this->post('acc_no'),
					'acc_holder_name' => $this->post('acc_holder_name'),
					'bank_name' => $this->post('bank_name'),
					'ifsc_bank' => $this->post('ifsc_bank'),
					'amount' => $this->post('refund'),
					'date' => date('Y-m-d'),
					'time' => date('H:i:s'),
				);
			
			$result = $this->db->insert('lm_user_request_for_refund', $data);
												
			$status = ['status' => 200 ,'message' => 'Your refund request has been sent to admin successfully.Amount will be refundend in 3 working days in your given account.'];
			$this->set_response($status, REST_Controller::HTTP_OK);
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_your_bank_details_post() {
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('lm_vendor_bank_detail', array('Fk_user_id' => $user_id));
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					if (!array_key_exists($row->b_id, $data)) {
						$data[$row->b_id] = array();
					}
					if (array_key_exists($row->b_id, $data)) {
						$data[$row->b_id] = array(
							'acc_no' => $row->acc_no,
							'acc_holder_name' => $row->acc_holder_name,
							'bank_name' => $row->bank_name,
							'ifsc_bank' => $row->ifsc_bank,
						);
						array_push($data_value, $data[$row->b_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
	}
	
	public function update_vendor_bank_details_post() {
		$db_result = $this->db->get_where('lm_vendor_bank_detail', array('Fk_user_id' => $_POST['user_id']));
		$data = array(
					'Fk_user_id' => $this->post('user_id'),
					'acc_no' => $this->post('acc_no'),
					'acc_holder_name' => $this->post('acc_holder_name'),
					'bank_name' => $this->post('bank_name'),
					'ifsc_bank' => $this->post('ifsc_bank'),
					'date' => date('Y-m-d'),
					'time' => date('H:i:s'),
				);
			if ($db_result && $db_result->num_rows() > 0) {
			
			$result = $this->db->update('lm_vendor_bank_detail', $data, array('Fk_user_id' => $_POST['user_id']));
			}else{
				$result = $this->db->insert('lm_vendor_bank_detail', $data);
			}									
			$status = ['status' => 200 ,'message' => ' updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_profile_info_post() {
		if(!$this->post('user_id')){
			$status = [
				['status' => 400 ,  'message' => 'user_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('lm_user', array('user_id' => $user_id));
			
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					if (!array_key_exists($row->user_id, $data)) {
						$data[$row->user_id] = array();
					}
					
						$area_id=0;
						$city_id=0;
						$area_name='';
						$city_name='';
						$db_result2 = $this->db->get_where('lm_user_location',array('user_id' =>$row->user_id));
						if ($db_result2 && $db_result2->num_rows() > 0) {
							foreach ($db_result2->result() as $row2) {
								$area_id=$row2->area_id;
								$city_id=$row2->city_id;
								
							}
						}
						$db_result2 = $this->db->get_where('lm_area',array('area_id' =>$area_id));
						if ($db_result2 && $db_result2->num_rows() > 0) {
							foreach ($db_result2->result() as $row2) {
								$area_name=$row2->area_name;
								
							}
						}
						
						$db_result2 = $this->db->get_where('lm_city',array('city_id' =>$city_id));
						if ($db_result2 && $db_result2->num_rows() > 0) {
							foreach ($db_result2->result() as $row2) {
								$city_name=$row2->city_name;
								
							}
						}
					
					
					$delivery_door = $row->delivery_door ;
					$delivery_appart = $row->delivery_appart ;
					$delivery_street1 = $row->delivery_street1 ;
					$delivery_street2 = $row->delivery_street2 ;
					$delivery_area = $row->delivery_area ;
					if($row->delivery_door == null){
						$delivery_door = '';
					}
					if($row->delivery_appart == null){
						$delivery_appart = '';
					}
					if($row->delivery_street1 == null){
						$delivery_street1 = '';
					}
					if($row->delivery_street2 == null){
						$delivery_street2 = '';
					}
					if($row->delivery_area == null){
						$delivery_area = '';
					}
					if (array_key_exists($row->user_id, $data)) {
						$data[$row->user_id] = array(
							
							'full_name' => $row->full_name,
							'user_state' => $row->user_state,
							'pin_code' => $row->pin_code,
							'contact_no' => $row->contact_no,
							'user_email' => $row->user_email,
							'city_name' => $city_name,
							'area_name' => $area_name,
							'delivery_door' => $delivery_door,
							'delivery_appart' => $delivery_appart,
							'delivery_street1' => $delivery_street1,
							'delivery_street2' => $delivery_street2,
							'delivery_area' => $delivery_area,
						
							
						);
						array_push($data_value, $data[$row->user_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}

	
	
	public function update_password_post() {
		if(!$this->post('user_password')){
			$status = ['status' => 400 ,  'message' => 'New Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			#Generate hashed password.
			//$hashed_old_password = sha1('admin' . (md5('admin' . $_POST['old_password'])));
			//$hashed_password = sha1('admin' . (md5('admin' . $_POST['user_password'])));
			//$pass_old=$_POST['old_password'];
			$pass = sha1('admin' . (md5('admin' . $this->post('user_password'))));
			//$pass=$_POST['user_password'];
			$data = array(
				'user_password' => $pass
			);
			$result = $this->db->update('lm_user', $data, array('user_id' => $_POST['User_Id']));
												
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	/**update user profile picture*/
    public function update_profile_pic_post() {
		if(!$this->post('User_id')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('Upload_file_name')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			//$decode = base64_decode($_REQUEST['base64']);			
			//$filename = date("YmdHisu");
			//$new_img = $filename.'.JPG';
			//$pathAndName = file_put_contents('./uploads/'.$new_img,$decode);
						
			$data = array(
				'user_img' => $_POST['Upload_file_name']
			);
			$result = $this->db->update('lm_user', $data, array('user_id' => $_POST['User_id']));
												
			$status = ['status' => 200 , 'filename' => $_POST['Upload_file_name'] ,'message' => 'Profile Picture updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	/**update user profile picture*/
    public function update_profile_info_post() {
		if(!$this->post('user_id')){
			$status = ['status' => 400 ,  'message' => 'Please reload the page and try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		
		}else{
			
		
			$data = array(
				'user_state' => $this->post('delivery_state'),
				'pin_code' => $this->post('delivery_pin_code'),
				'delivery_door' => $this->post('delivery_door'),
				'delivery_appart' => $this->post('delivery_appart'),
				'delivery_street1' => $this->post('delivery_street1'),
				'delivery_street2' => $this->post('delivery_street2'),
				'delivery_area' => $this->post('delivery_area'),
			);
			$result = $this->db->update('lm_user', $data, array('user_id' => $_POST['user_id']));
												
			$status = ['status' => 200 ,'message' => ' updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
    public function check_email_availability_post() {
        if ($_POST['param'] == 'registration_email') {
            $db_result = $this->db->get_where('lm_user', array('user_email' => $_POST['user_email']));
        
			if ($db_result && $db_result->num_rows() > 0) {
				#param already exists			
				$this->response('Email id already exist.', REST_Controller::HTTP_OK);
			} else {
				$this->set_response(true, REST_Controller::HTTP_OK);
			}
		}else if ($_POST['param'] == 'registration_name') {
            $db_result = $this->db->get_where('lm_user', array('user_name' => $_POST['user_name']));
        
			if ($db_result && $db_result->num_rows() > 0) {
				#param already exists			
				$this->response('User name already exist.', REST_Controller::HTTP_OK);
			} else {
				$this->set_response(true, REST_Controller::HTTP_OK);
			}
		}else if ($_POST['param'] == 'registration_volunteer') {
            $db_result = $this->db->get_where('lm_user', array('user_name' => $_POST['user_name1']));
        
			if ($db_result && $db_result->num_rows() > 0) {
				#param already exists			
				$this->response('User name already exist.', REST_Controller::HTTP_OK);
			} else {
				$this->set_response(true, REST_Controller::HTTP_OK);
			}
		}
    }

		  //--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
			
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }


	
		  //--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_banner_image_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
			list($width, $height) = getimagesize($file);

			if($width != "300" || $height !="100"){
					$arr_response['status'] = 201;
					$arr_response['message'] = "image size must be 300 x 100 pixels.";
			}else{
				move_uploaded_file($file, $target);
				$arr_response['status'] = 200;
				$arr_response['filename'] = $filename;
			}
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }

	public function insert_refer_code_post() {
		$refered_by_user_id = 0;
		if ($_POST['refer_code']) {
            $db_result = $this->db->get_where('refer_code_history', array('user_id' => $this->post('user_id')));
			if ($db_result && $db_result->num_rows() == 0) {
					
				$db_result = $this->db->get_where('lm_user', array('contact_no' => $this->post('refer_code')));
			
				if ($db_result && $db_result->num_rows() > 0) {
					
					foreach ($db_result->result() as $row) {
						$refered_by_user_id = $row->user_id;
					}
					
					$data = array(
						'user_id' => $this->post('user_id'),
						'refered_by_user_id' => $refered_by_user_id,
						'is_cashback' => 'no',
						'cashback_date' => '',
						'refer_code' => $this->post('refer_code'),
						'refer_type' => 'refer accepted',
						'date' => date('Y-m-d'),
						'time' => date('H:i:s'),
					);
					$result = $this->db->insert('refer_code_history', $data);
					
					$data1 = array(
						'user_id' => $refered_by_user_id,
						'refered_by_user_id' => $this->post('user_id'),
						'is_cashback' => 'no',
						'cashback_date' => '',
						'refer_code' => $this->post('refer_code'),
						'refer_type' => 'share refer',
						'date' => date('Y-m-d'),
						'time' => date('H:i:s'),
					);
					
					$result = $this->db->insert('refer_code_history', $data1);
					$status = ['status' => 200 ,'message' => 'refer code submit successfully.'];
					$this->set_response($status, REST_Controller::HTTP_OK);
				} else {
					$status = ['status' => 201 ,  'message' => 'refer code invalid'];
					$this->response($status, REST_Controller::HTTP_CREATED);
				}
			
			}else{
				$status = ['status' => 201 ,  'message' => 'you already used one refer code'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}else{
			$status = ['status' => 201 ,  'message' => 'refer code required'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
    }
	
	public function fatch_refer_code_details_post() {
			$user_id = $this->post('user_id');
			$db_result = $this->db->get_where('refer_code_history', array('user_id' => $user_id));
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					if (!array_key_exists($row->refer_history_id, $data)) {
						$data[$row->refer_history_id] = array();
					}
					$is_cashback = '';
					if($row->is_cashback == 'no'){
						$is_cashback = 'Not used';
					}else{
						$is_cashback = 'Used';
					}
					$refer_type = '';
					if($row->refer_type == 'refer accepted'){
						$refer_type = 'Refer Accepted';
					}else{
						$refer_type = 'Refer Shared';
					}
					if (array_key_exists($row->refer_history_id, $data)) {
						$data[$row->refer_history_id] = array(
							'date' => $row->date,
							'refer_type' => $row->refer_type,
							'is_cashback' => $is_cashback,
						);
						array_push($data_value, $data[$row->refer_history_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
	}
}
