<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Account extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
    }
	
	/** customer data functions ***/
	public function customer_login_email_check_post() {
		
        $db_result = $this->db->get_where('customer_account', array('customer_email' => $this->post('customer_email')));
        
        if ($db_result->num_rows() > 0) {
            echo 'true';
			
        } else {
			
			echo 'false';
			
        }
	}

	
	public function signin_for_customer_post() {
		if(!$this->post('customer_email')){
			$status = ['status' => 400 ,  'message' => 'Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('customer_password')){
			$status =['status' => 400 ,  'message' => 'Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			$this->db->where('customer_email', $this->post('customer_email'));
			$this->db->where('customer_status', 'active');
			$this->db->where('customer_password', sha1('admin' . (md5('admin' . $this->post('customer_password')))));
			$db_result = $this->db->get('customer_account');
			//print_r($this->db->last_query());
			if ($db_result && $db_result->num_rows() > 0) {
				#login credentials matched
				$row = $db_result->row();
				$data = array(
								'customer_id' => $row->customer_id,
								'customer_email' => $row->customer_email,
								'loggedIN' => 11,
								'first_name' => $row->first_name,
								'last_name' => $row->last_name,
								'customer_img' => 'default_profile.jpg',
								
							);
							
				$this->session->set_userdata($data);
				
				$status = ['status' => 200 ,  
							'message' => 'Login Successfully.',
							'data' => $data
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{				
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Invalid Email or Password.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	
	public function customer_check_email_availability_post() {
		
        $db_result = $this->db->get_where('customer_account', array('customer_email' => $this->post('customer_email')));
        if ($db_result->num_rows() > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
	}

	public function signup_for_customer_post() {
		//var_dump($this->post('tpye'));
		if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'First Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('email_id')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		
		else{
			
			$db_result = $this->db->get_where('customer_account', array('customer_email' => $this->post('email_id')));
			if ($db_result->num_rows() > 0) {
				//var_dump('id h');
				$status = ['status' => 201 ,  'message' => 'This Email Id Already Exist'];
				$this->response($status, REST_Controller::HTTP_CREATED);
				
			}else{
				//var_dump('id  ni h');
				#generate random number
				$verify_code = rand(1111,99999999999);
				$verify_code2 = rand(1111,99999999999);
				$verify_code = sha1('verify' . (md5('verify' . $verify_code)));
				$datas = array(
					'first_name' => $this->post('first_name'),
					'last_name' => $this->post('last_name'),
					'country_name' => $this->post('country'),
					'state_name	' => $this->post('state'),
					'city_name' => $this->post('city'),
					'customer_email' => $this->post('email_id'),
					'phone_no' => $this->post('phone_numbers'),				
					'zipcode' => $this->post('zipcode'),				
					'customer_password'=> $verify_code2,				
					'verify_code' => $verify_code,
					'customer_img' => 'default_profile.jpg',
					'customer_status' => 'active',	
					'dummy_password'=>'yes',
					'created_date' => date('Y-m-d'),
					'created_time' => date('H:i:s'),
					
				);
				$result = $this->db->insert('customer_account', $datas);
				$insert_id = $this->db->insert_id();
				$data1 = array(
					'customer_id' => $insert_id,
					'first_name' => $this->post('first_name'),
					'last_name' => $this->post('last_name'),
					'country_name' => $this->post('country'),
					'state_name	' => $this->post('state'),
					'city_name' => $this->post('city'),
					'shipping_phone_number' => $this->post('phone_numbers'),
					'address_line' => $this->post('address_lines'),
					'zip_code' => $this->post('zipcode'),
					'is_selected' => '1',
					
				);
				$results = $this->db->insert('customer_address', $data1);
				
				$data = array(
					'customer_id' => $insert_id,
					'customer_email' =>$this->post('email_id'),
					'loggedIN' => 11,
					'first_name' => $this->post('first_name'),
					'last_name' => $this->post('last_name'),
					'phone_no' => $this->post('phone_numbers'),
					'customer_img' =>'default_profile.jpg',
				);							
				$this->session->set_userdata($data);
				
				$verification_link= base_url().'verify/vc?e='.$insert_id.'&v='.$verify_code;
				$content ='Dear '.$this->post('full_name').' ,<br /><br /><p>Welcome and thank you for registering at <a href="'.PROJECT_URL.'">'.PROJECT_NAME.' </a></p><p>Your account has now been created and you can log in by Clicking the button bellow to confirm your email address</p><div style="text-align: center;margin: 20px;padding: 10px;"><a href="'.$verification_link.'" style="padding: 7px; background-color: #3b5998; margin: 10px;color: #fff;font-size: 14px;">CONFIRM</a></div>';
				$content ='<div>This is Your one Time Password you Can Use while Login For The First Time'.$verify_code2 .'</div>';
				
				$this->load->model('Email_model', 'obj_email', TRUE);
				$data11 = array(
					'subject' => 'Welcome to '.PROJECT_NAME,
					'to' => $this->post('email_id'),
					'to_name' => $this->post('first_name'),
					'from' => PROJECT_EMAIL,
					'msg' => $content,
				);
				$result = $this->obj_email->send_mail_in_sendinblue($data11);
				//var_dump($data11);
				//var_dump($result);
				//echo "ss";
				$status = ['status' => 200 ,'message' => 'Your account has been successfully created. Please continue to login.',];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}
				
		}
    }
	public function signup_for_customer_vendor_post() {
		if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'First Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('email_id')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			#Generate hashed password.
			$hashed_password = sha1('admin' . (md5('admin' . $this->post('password'))));
			#generate random number
			$verify_code = rand(1111,99999999999);
			$verify_code = sha1('verify' . (md5('verify' . $verify_code)));
			$data = array(
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'country_name' => $this->post('country'),
				'state_name	' => $this->post('state'),
				'city_name' => $this->post('city'),
				'customer_email' => $this->post('email_id'),
				'phone_no' => $this->post('phone_numbers'),				
				'zipcode' => $this->post('zipcode'),				
				'customer_password' =>$hashed_password,				
				'verify_code' => $verify_code,
				'customer_img' => 'default_profile.jpg',
				'customer_status' => 'active',				
				'created_date' => date('Y-m-d'),
				'created_time' => date('H:i:s'),
				
			);
			$result = $this->db->insert('customer_account', $data);
			$insert_id = $this->db->insert_id();
			$data1 = array(
				'customer_id' => $insert_id,
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'country_name' => $this->post('country'),
				'state_name	' => $this->post('state'),
				'city_name' => $this->post('city'),
				'shipping_phone_number' => $this->post('phone_numbers'),
				'address_line' => $this->post('address_lines'),
				'zip_code' => $this->post('zipcode'),
				'is_selected' => '1',
				
			);
			$results = $this->db->insert('customer_address', $data1);
			
			$verification_link= base_url().'verify/vc?e='.$insert_id.'&v='.$verify_code;
			$content ='Dear '.$this->post('full_name').' ,<br /><br /><p>Welcome and thank you for registering at <a href="'.PROJECT_URL.'">'.PROJECT_NAME.' </a></p><p>Your account has now been created and you can log in by Clicking the button bellow to confirm your email address</p><div style="text-align: center;margin: 20px;padding: 10px;"><a href="'.$verification_link.'" style="padding: 7px; background-color: #3b5998; margin: 10px;color: #fff;font-size: 14px;">CONFIRM</a></div>';
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => 'Welcome to '.PROJECT_NAME,
				'to' => $this->post('email_id'),
				'to_name' => $this->post('first_name'),
				'from' => PROJECT_EMAIL,
				'msg' => $content,
			);
			$result = $this->obj_email->send_mail_in_sendinblue($data11);
			//var_dump($data11);
			//var_dump($result);
		   
			if ($insert_id) {
				$status = ['status' => 200 ,'message' => 'Your account has been successfully created. Please continue to login.',];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	public function customer_send_email_forget_password_post() {
		 $user_img ='';
		if(!$this->post('customer_email')){
			$status =['status' => 400 ,  'message' => 'User Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$customer_email = $this->post('customer_email');
			$randam_no = rand(1111,99999999999);
			$verify_code = sha1('verify' . (md5('verify' . $randam_no)));
			$Array = array(
				'verify_code' => $verify_code,
			);
			$this->db->set($Array);
			$this->db->where('customer_email',$customer_email);
			$arr_value = array();
			if($this->db->update('customer_account')){
				/*fetching the user data*/
				$customer_email = trim($this->post('customer_email'));
				$this->db->where(array('customer_email'=>$customer_email));
				$res = $this->db->get('customer_account');
				
				if($res && $res->num_rows() == 1){
					
					$data = array();
					foreach ($res->result() as $row) {
						$customer_img = $row->customer_img;
						if (!array_key_exists($row->customer_id, $data)) {
							$data[$row->customer_id] = array();
						}
						if (array_key_exists($row->customer_id, $data)) {
							$data[$row->customer_id] = array(
								'customer_id' => $row->customer_id,
								'full_name' => $row->first_name,
								'customer_email' => $row->customer_email,
								'verify_code' => $row->verify_code,
								//'randam_no' => $randam_no,
								'customer_img' => $row->customer_img,
							 );
							array_push($arr_value, $data[$row->customer_id]);
						}
					}
				}else{
					$status =['status' => 400 ,  'message' => 'Email address does not exist'];
					$this->response($status, REST_Controller::HTTP_CREATED);
				}
				$e = ($customer_email);
				$verification_link = $verify_code;
				$to = $customer_email;
				$subject = "Forget Password Request.";
				$message ='';
				$message .= 'Dear '.$arr_value[0]['full_name'].'<br/><br/>';
				$message .= '<p>Please Click the below to change your password</p>';
				$message .= '<a href="'.base_url().'welcome/reset_password?e='.$e.'&v='.$verification_link.'">Click Here</a>'; 
				
				$this->load->model('Email_model', 'obj_email', TRUE);
				$data11 = array(
					'subject' => $subject,
					'to' => $to,
					'to_name' => $row->first_name,
					'from' => PROJECT_EMAIL,
					'msg' => $message,
				);
				$result = $this->obj_email->send_mail_in_sendinblue($data11);
				//var_dump($result);
				//var_dump($data11);
				$status = ['status' => 200 ,'message' => 'Email sent to given email address successfully!please check your mail.', 'data' => $arr_value];
				$this->set_response($status, REST_Controller::HTTP_OK);
				
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
				
			}
		}
	}
	
	public function customer_rest_password_post() {
		$this->db->where(array('customer_email'=>$this->post('id'),'verify_code'=>$this->post('verify_code')));
		$res = $this->db->get('customer_account');
		if($res && $res->num_rows() == 1){
			#Generate hashed password.
			$hashed_password = sha1('admin' . (md5('admin' . $this->post('customer_password'))));
			$data = array(
				'customer_password' => $hashed_password,
			);
			$result = $this->db->update('customer_account', $data, array('customer_email'=>$this->post('id'),'verify_code'=>$this->post('verify_code')));	
			
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
		}else{
			$status = ['status' => 201 ,  'message' => 'Verification token might be expire. Try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		
    }
	
	/** END customer data functions ***/
	
	/** vendor data functions ***/
	public function signin_for_vendor_post() {
		if(!$this->post('vendor_email')){
			$status = ['status' => 400 ,  'message' => 'Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('vendor_password')){
			$status =['status' => 400 ,  'message' => 'Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			$this->db->where('vendor_email', $this->post('vendor_email'));
			$this->db->where('vendor_status', 'active');
			$this->db->where('vendor_password', sha1('admin' . (md5('admin' . $this->post('vendor_password')))));
			$db_result = $this->db->get('vendor_account');
			//print_r($this->db->last_query());
			if ($db_result && $db_result->num_rows() > 0) {
				#login credentials matched
				$row = $db_result->row();
				$data = array(
								'vendor_id' => $row->vendor_id,
								'vendor_email' => $row->vendor_email,
								'loggedIN' => 12,
								'full_name' => $row->full_name,
								'vendor_img' => $row->vendor_img,
							);
							
				$this->session->set_userdata($data);
				
				$status = ['status' => 200 ,  
							'message' => 'Login Successfully.',
							'data' => $data
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{				
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Invalid Email or Password.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	
	public function vendor_login_email_check_post() {
		
        $db_result = $this->db->get_where('vendor_account', array('vendor_email' => $this->post('vendor_email')));
        
        if ($db_result->num_rows() > 0) {
            echo 'true';
        } else {
            echo 'false';
        }
	}
	
	public function signup_for_vendor_post() {
		if(!$this->post('full_name')){
			$status = ['status' => 400 ,  'message' => 'First Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('vendor_email')){
			$status = ['status' => 400 ,  'message' => 'Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('vendor_password')){
			$status = ['status' => 400 ,  'message' => 'Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			#Generate hashed password.
			$hashed_password = sha1('admin' . (md5('admin' . $this->post('vendor_password'))));
			#generate random number
			$verify_code = rand(1111,99999999999);
			$verify_code = sha1('verify' . (md5('verify' . $verify_code)));
			$data = array(
				'full_name' => $this->post('full_name'),
				'vendor_email' => $this->post('vendor_email'),
				'vendor_password' => $hashed_password,
				'verify_code' => $verify_code,
				'vendor_img' => 'default_profile.jpg',
				'vendor_status' => 'active',
				'created_date' => date('Y-m-d'),
				'created_time' => date('H:i:s'),
				
			);
			$result = $this->db->insert('vendor_account', $data);
			$insert_id = $this->db->insert_id();
			
			$verification_link= base_url().'verify/v?e='.$insert_id.'&v='.$verify_code;
			$content ='Dear '.$this->post('full_name').' ,<br /><br /><p>Welcome and thank you for registering at <a href="'.PROJECT_URL.'">'.PROJECT_NAME.' </a></p><p>Your account has now been created and you can log in by Clicking the button bellow to confirm your email address</p><div style="text-align: center;margin: 20px;padding: 10px;"><a href="'.$verification_link().'" style="padding: 13px;background-color: blue;margin: 10px;color: #fff;font-size: 24px;">CONFIRM</a></div>';
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => 'Welcome to '.PROJECT_NAME,
				'to' => $this->post('vendor_email'),
				'from' => PROJECT_EMAIL,
				'msg' => $content,
			);
			$result = $this->obj_email->send_mail_in_ci($data11);
		   
			if ($insert_id) {
				$status = ['status' => 200 ,'message' => 'Your account has been successfully created. Please continue to login.',];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	
	public function vendor_check_email_availability_post() {
		if ($_POST['param'] == 'vendor_registration') {
            $db_result = $this->db->get_where('vendor_account', array('vendor_email' => $this->post('vendor_email')));
        } else if ($_POST['param'] == 'edit_users') {
            $db_result = $this->db->get_where('donor_registration', array('donor_email' => $_POST['check_out_donor_email']));
        }
        if ($db_result->num_rows() > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
	}
	
	
	public function vendor_send_email_forget_password_post() {
		 $user_img ='';
		if(!$this->post('vendor_email')){
			$status =['status' => 400 ,  'message' => 'User Email address is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$vendor_email = $this->post('vendor_email');
			$randam_no = rand(1111,99999999999);
			$verify_code = sha1('verify' . (md5('verify' . $randam_no)));
			$Array = array(
				'verify_code' => $verify_code,
			);
			$this->db->set($Array);
			$this->db->where('vendor_email',$vendor_email);
			$arr_value = array();
			if($this->db->update('vendor_account')){
				/*fetching the user data*/
				$vendor_email = trim($this->post('vendor_email'));
				$this->db->where(array('vendor_email'=>$vendor_email));
				$res = $this->db->get('vendor_account');
				
				if($res && $res->num_rows() == 1){
					
					$data = array();
					foreach ($res->result() as $row) {
						$vendor_img = $row->vendor_img;
						if (!array_key_exists($row->vendor_id, $data)) {
							$data[$row->vendor_id] = array();
						}
						if (array_key_exists($row->vendor_id, $data)) {
							$data[$row->vendor_id] = array(
								'vendor_id' => $row->vendor_id,
								'full_name' => $row->full_name,
								'vendor_email' => $row->vendor_email,
								'verify_code' => $row->verify_code,
								//'randam_no' => $randam_no,
								'vendor_img' => $row->vendor_img,
							 );
							array_push($arr_value, $data[$row->vendor_id]);
						}
					}
				}else{
					$status =['status' => 400 ,  'message' => 'Email address does not exist'];
					$this->response($status, REST_Controller::HTTP_CREATED);
				}
				$e = ($vendor_email);
				$verification_link = $verify_code;
				$to = $vendor_email;
				$subject = "Forget Password Request.";
				$message ='';
				$message .= 'Dear '.$arr_value[0]['full_name'].'<br/><br/>';
				$message .= '<p>Please Click the below to change your password</p>';
				$message .= '<a href="'.base_url().'vendor/reset_password?e='.$e.'&v='.$verification_link.'">Click Here</a>'; 
				
				$this->load->model('Email_model', 'obj_email', TRUE);
				$data11 = array(
					'subject' => $subject,
					'to' => $to,
					'from' => PROJECT_EMAIL,
					'msg' => $message,
				);
				$result = $this->obj_email->send_mail_in_ci($data11);
				
				$status = ['status' => 200 ,'message' => 'Email sent to given email address successfully!please check your mail.', 'data' => $arr_value];
				$this->set_response($status, REST_Controller::HTTP_OK);
				
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
				
			}
		}
	}
	
	public function vendor_rest_password_post() {
		$this->db->where(array('vendor_email'=>$this->post('id'),'verify_code'=>$this->post('verify_code')));
		$res = $this->db->get('vendor_account');
		if($res && $res->num_rows() == 1){
			#Generate hashed password.
			$hashed_password = sha1('admin' . (md5('admin' . $this->post('vendor_password'))));
			$data = array(
				'vendor_password' => $hashed_password,
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_email'=>$this->post('id'),'verify_code'=>$this->post('verify_code')));	
			
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
		}else{
			$status = ['status' => 201 ,  'message' => 'Verification token might be expire. Try again'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
		
    }
	
	public function get_address_content_post(){

		$country_list = $this->db->get('country_list')->result();
		$state_list = $this->db->get('state_list')->result();
		$city_list = $this->db->get('city_list')->result();

		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'country_list' => $country_list,
			'state_list' => $state_list,
			'city_list' => $city_list,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	public function get_last_comment_post() {
		$post_id = $this->post('post_id');
		$comment_id = $this->post('comment_id');
		
		
		$sql="SELECT * FROM `post_comments` WHERE `post_id` = $post_id ORDER BY `comment_id` DESC";
		$result2 = $this->db->query($sql); 
		$comments_count = 0;
		if ($result2 && $result2->num_rows() > 0){
			foreach ($result2->result() as $row) {
				$comments_count++;
			}
		}
				
		$sql="SELECT * FROM `post_comments` WHERE `comment_id` = $comment_id";
		$result = $this->db->query($sql); 
		$comments_html ='';
			
		if ($result && $result->num_rows() > 0){
			
			foreach ($result->result() as $row) { 
			
				$sql="SELECT * FROM `customer_account` WHERE `customer_id` = $row->user_id";
				$result1 = $this->db->query($sql); 
				if ($result1 && $result1->num_rows() > 0){
					$data = $result1->result();
					$user_data = array(
						$user_name = $data[0]->first_name,
						$customer_img = $data[0]->customer_img,
					);
				}else{
					//redirect(base_url());
				}
				if(strlen($row->comment) >40){
					$show_ ='';
					//$margin='margin-top: 15px';
				}else{
					$show_ ='display_none';
					//$margin='';
				}
				$current_dat=strtotime($row->created_date);
				$change_date = date(" j M Y ",$current_dat);
				
				$comments_html .='<div class="sh-comments__section comments__section'.$row->comment_id.'" style="margin-top: 5px;">
							<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 32px;height: 32px;" src="'.base_url().'uploads/'.$customer_img.'" alt=""></a>
							<div class="sh-comments__content" style="position: relative; margin-left: 35px; top: -34px;">
								<a class="sh-comments__name">'.$user_name.'</a>
								<span class="sh-comments__passed" style="margin-left: 5px;font-size: 12px; display: block;">'.$change_date .'</span>
								<div style="display: flex;font-size: 14px;">
									<p class="p_dot">'.$row->comment.'</p><i style="color: #69B52D;" class="fa fa-comments" aria-hidden="true" data-toggle="modal" data-target="#sub_comments" comment_id="'.$row->comment_id.'"></i>
									<input type="hidden" class="post_Id" value="'.$post_id.'">
									<input type="hidden" class="postuser_Id" value="'.$row->user_id.'">
									<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
									<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
										<a class="sh-section__btn-comment sh-btn-icon" style="width: 100%;"><span name="'.$row->comment_id.'" class="sub_comment_" data-toggle="modal" data-target="#sub_comments" style="font-size: 15px;cursor: pointer;">Comment</span></a><br>
										<a class="sh-section__btn-comment sh-btn-icon delete_comment" style="width: 100%;margin-top: 5px;"><span name="'.$row->comment_id.'" style="font-size: 15px;cursor: pointer;">Delete</span></a>
									</div>
								</div>
							</div>
						</div>';
			}
		}
	    
		$arr_response['number_of_comments'] = $comments_count;	
		$arr_response['comments_html'] = $comments_html;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Comments get Successfully';
		echo json_encode($arr_response);
		
    }
	public function get_last_complain_comment_post() {
		//$parent_id = $this->post('product_id');
		$complain_id = $this->post('complain_id');
		
		
		$sql="SELECT * FROM `complains` WHERE `complain_id` = $complain_id ORDER BY `complain_id` DESC";
		$result2 = $this->db->query($sql); 
		$comments_count = 0;
		if ($result2 && $result2->num_rows() > 0){
			foreach ($result2->result() as $rows) {
				//var_dump($rows);
				$user_comment= $rows->user_comment;
				$created_date= $rows->created_date;
				$created_time= $rows->created_time;
				$current_dat=strtotime($created_date);
					$change_date = date(" j M Y ",$current_dat);
				$comments_count++;
			}
		}
				
		$sql="SELECT * FROM `complains` WHERE `complain_id` = $complain_id";
		$result = $this->db->query($sql); 
		$last_content ='';
			
		if ($result && $result->num_rows() > 0){
			
			foreach ($result->result() as $row) { 
			
				$sql="SELECT * FROM `customer_account` WHERE `customer_id` = $row->user_id";
				$result1 = $this->db->query($sql); 
				if ($result1 && $result1->num_rows() > 0){
					$data = $result1->result();
					$user_data = array(
						$first_name = $data[0]->first_name,
						$last_name = $data[0]->last_name,
						$customer_img = $data[0]->customer_img,
					);
				}else{
					//redirect(base_url());
				}
				if(strlen($rows->user_comment) >40){
					$show_ ='';
					//$margin='margin-top: 15px';
				}else{
					$show_ ='display_none';
					//$margin='';
				}
					$last_content .='<div>
								<div class="media media-chat"><img class="avatar" src="'.$customer_img.'" alt="...">
									<div class="media-body">
										<span class="meta"><time datetime="2018">'.$change_date.' <br/>'.$created_time.'</time></span>
										<h6>'.$first_name.''.$last_name.'</h6>
										<p>'.$user_comment.'</p>
									</div>
								</div>
								 
							</div>'; ;
							//var_dump($last_content);
			}
		}
	    
		$arr_response['number_of_comments'] = $comments_count;	
		$arr_response['last_content'] = $last_content;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Comments get Successfully';
		echo json_encode($arr_response);
		
    }
	
	/*public function get_last_sub_comment_post() {
		$post_id = $this->post('post_id');
		$comment_id = $this->post('comment_id');
		
		
		$sql="SELECT * FROM `post_comments` WHERE `post_id` = $post_id ORDER BY `comment_id` DESC";
		$result2 = $this->db->query($sql); 
		$comments_count = 0;
		if ($result2 && $result2->num_rows() > 0){
			foreach ($result2->result() as $row) {
				$comments_count++;
			}
		}
			
		$sql1="SELECT * FROM `post_comments` WHERE `comment_id` = $comment_id";
		$result = $this->db->query($sql1); 
		
		$comments_html='';
		
		if ($result && $result->num_rows() > 0){
			foreach ($result->result() as $row12) { 
				$post_user_id = $row12->user_id;
				
				$sql3="SELECT * FROM `customer_account` WHERE `customer_id` =".$row12->user_id;
				$result12 = $this->db->query($sql3); 
				if ($result12 && $result12->num_rows() > 0){
					$data12 = $result12->result();
					
					$user_name1 = $data12[0]->first_name;
					$last_name1 = $data12[0]->last_name;
					$customer_img1 = $data12[0]->customer_img;
				}
				
				if(strlen($row->comment) >40){
					$show_ ='';
					$margin='margin-top: 15px';
				}else{
					$show_ ='display_none';
					$margin='';
				}
				
				$comments_html .= '<div class="sh-comments__section" style="margin: 5px 30px;">
					<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 28px;height: 28px;" src="'.base_url().'uploads/'.$customer_img1.'" alt=""></a>
					<div class="sh-comments__content" style="position: relative;">
						<a class="sh-comments__name" style="font-size: 13px;">'.$user_name1.' '.$last_name1.'</a>
						
						<div style="display: flex;font-size: 13px;">
							<p class="p_dot">'.$row12->comment.'</p>
							<input type="hidden" class="post_Id" value="'.$row12->post_id.'">
							<input type="hidden" class="postuser_Id" value="'.$post_user_id.'">
							<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="position: absolute; right: -30px;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
							<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
								<a class="sh-section__btn-comment sh-btn-icon delete_comment" style="width: 100%;margin-top: 5px;"><span name="'.$row12->comment_id.'" style="font-size: 15px;cursor: pointer;">Delete</span></a>
							</div>
						</div>
					</div>
				</div>';
			}
		}
		$arr_response['number_of_comments'] = $comments_count;	
		$arr_response['comments_html'] = $comments_html;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Comments get Successfully';
		echo json_encode($arr_response);
	}*/
	
	public function add_sub_comment_post() {
		$data = array(
            'comment' => $this->post('user_comment'),
            'post_id' => $this->post('post_id'),
            'user_id' => $this->post('user_id'),
			'created_date' => date('Y-m-d'),
            'created_time' => date("H:i:s"),
        );
		$result = $this->db->insert('post_comments', $data);      
		$comment_id = $this->db->insert_id();
		
	
		$arr_response['comment_id'] = $comment_id;	
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Commented Successfully';
		echo json_encode($arr_response);
	}
	
	public function add_complain_comment_post() {
		$data = array(
            'user_comment' => $this->post('user_comment'),
            'parent_id' => $this->post('parent_id'),
            'user_id' => $this->post('user_id'),
			'product_id' => $this->post('parent_id'),
			'vendor_id' => $this->post('vendor_id'),
			'read_unread'=>'unread',
			'type'=> $this->post('type'),
            'created_date' => date('Y-m-d'),
            'created_time' => date("H:i:s"),
        );
		//echo'dfgdfgdf';
		$result = $this->db->insert('complains', $data);      
		
		
		$complain_id = $this->db->insert_id();
		
	
		$arr_response['complain_id'] = $complain_id;	
		$arr_response['$data'] =$data;	
	
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Commented Successfully';
		echo json_encode($arr_response);
	}
	
	public function get_all_comments_post() {
		$post_id = $this->post('post_id');
		//$comment_id = $this->post('comment_id');
	
		
		$sql="SELECT * FROM `post_comments` WHERE `post_id` = $post_id";
		$result2 = $this->db->query($sql); 
		$comments_count = 0;
		if ($result2 && $result2->num_rows() > 0){
			foreach ($result2->result() as $row) {
				$comments_count++;
				//var_dump( $row);
				$user_id =$row->user_id;
				$comment_id=$row->comment_id;
				//var_dump($user_id);
			}
		}
				
		$sql="SELECT * FROM `post_comments` WHERE `post_id` = $post_id";
		$result = $this->db->query($sql); 
		$all_comments_html ='';
			
		if ($result && $result->num_rows() > 0){
			
			foreach ($result->result() as $row) { 
					$current_dat=strtotime($row->created_date);
					$change_date = date(" j M Y ",$current_dat);
				$sql="SELECT * FROM `customer_account` WHERE `customer_id` = $row->user_id";
				$result1 = $this->db->query($sql); 
				if ($result1 && $result1->num_rows() > 0){
					$data = $result1->result();
					
					$user_data = array(
						$user_name = $data[0]->first_name,
						$last_name = $data[0]->last_name,
						$customer_img = $data[0]->customer_img,
					);
					//var_dump($user_data);
				}else{
					//redirect(base_url());
				}
				if(strlen($row->comment) >40){
					$show_ ='';
					//$margin='margin-top: 15px';
				}else{
					$show_ ='display_none';
					
				}
				
			
						
					$sql1="SELECT * FROM `post_comments` WHERE `post_id` =".$row->comment_id." ORDER BY `comment_id` DESC ";
				$result2 = $this->db->query($sql1);
				
				$post_html_1='';
				if ($result2 && $result2->num_rows() > 0){
					//$datass = $result2->result();
					$sc_count =  $result2->num_rows();
					
					foreach ($result2->result() as $row12) { 
					
					
					$current_dat=strtotime($row12->created_date);
					$change_date = date(" j M Y ",$current_dat);
						
						$sql3="SELECT * FROM `customer_account` WHERE `customer_id` =".$row12->user_id;
						$result12 = $this->db->query($sql3); 
						if ($result12 && $result12->num_rows() > 0){
							$data12 = $result12->result();
							//$user_data = array(
								$user_name1 = $data12[0]->first_name;
								$last_name1 = $data12[0]->last_name;
								$customer_img1 = $data12[0]->customer_img;
							//);
						}else{
							
						}
						
						if($comment_id == $this->session->userdata('customer_id') || $row12->user_id == $this->session->userdata('customer_id')){
							$show_delete_='';
						}else{
							$show_delete_='display_none';
						}
						$post_html_1 .= '<div class="sh-comments__section" style="margin: 5px 30px;">
							<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 28px;height: 28px;" src="'.base_url().'uploads/'.$customer_img1.'" alt=""></a>
								<a class="sh-comments__name" style="font-size: 13px;">'.$user_name1.' '.$last_name1.'</a>
								<span class="sh-comments__passed" style="margin-left: 10px;font-size: 9px; display:">'.($change_date) .'</span>
							
							<div class="sh-comments__content" style="position: relative;">
								
								
								<div style="display: flex;font-size: 13px;">
									<p  style=" margin-left: 35px;" class="p_dot" style="padding: 2px;">'.$row12->comment.'</p><i  style="color:#57595b; margin-top: 10px; margin-left: 10px;" class="fa fa-comments" aria-hidden="true" data-toggle="modal" data-target="#sub_comments" comment_id="'.$row->comment_id.'"></i>
							
									<input type="hidden" class="post_Id" value="'.$row12->post_id.'">
								
									<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
									<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
										<a class="sh-section__btn-comment sh-btn-icon" style="width: 100%;" ><span name="'.$row12->post_id.'" class="sub_comment_" data-toggle="modal" data-target="#sub_comments">Comment</span></a><br>
										<a class="sh-section__btn-comment sh-btn-icon delete_comment '.$show_delete_.'" style="width: 100%;margin-top: 5px;"><span name="'.$row12->comment_id.'" style="font-size: 15px;">Delete</span></a>
									</div>
								</div>
							</div>
						</div>';
					}
				
				}else{
					$sc_count=0;
				}
				
				if($comment_id == $this->session->userdata('customer_id') || $row->user_id == $this->session->userdata('customer_id')){
					$show_delete='';
				}else{
					$show_delete='display_none';
				}
				$all_comments_html .='<div><div class="sh-comments__section comments__section'.$row->comment_id.'">
							<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 28px;height: 28px;" src="'.base_url().'uploads/'.$customer_img.'" alt=""></a>
							<a class="sh-comments__name" style="margin-left:10px; margin-right:10px;">'.$user_name.' '.$last_name.'</a><span class="sh-comments__passed" style="font-size: 10px; display:">'.($change_date) .'</span>
								
							<div class="sh-comments__content" style="position: relative;">
								
								
								<div style="display: flex;">
									<p  style=" margin-left: 39px;"class="p_dot">'.$row->comment.'</p><i  style="color: #57595b; margin-top: 10px; margin-left: 10px;" class="fa fa-comments" aria-hidden="true" data-toggle="modal" data-target="#sub_comments" comment_id="'.$row->comment_id.'"></i>
							
									<input type="hidden" class="post_Id" value="'.$post_id.'">
									
									
									<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
									<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
										<a class="sh-section__btn-comment sh-btn-icon" style="width: 100%;" ><span name="'.$row->comment_id.'" class="sub_comment_" data-toggle="modal" data-target="#sub_comments">Comment</span></a><br>
										<a class="sh-section__btn-comment sh-btn-icon delete_comment_main '.$show_delete.'" style="width: 100%;margin-top: 5px;"><span name="'.$row->comment_id.'" style="font-size: 15px;">Delete</span></a>
									</div>
								</div>
							</div>
						</div>
						'.$post_html_1.'</div>';

			}
		}
	    
		$arr_response['number_of_comments'] = $comments_count;	
		$arr_response['all_comments_html'] = $all_comments_html;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Comments get Successfully';
		echo json_encode($arr_response);
		
    }
	
	public function get_all_complain_post() {
		$complain_id = $this->post('complain_id');
		//$comment_id = $this->post('comment_id');
	
		
		$sql="SELECT * FROM `complains` WHERE `complain_id` = $complain_id";
		$result2 = $this->db->query($sql); 
			//var_dump($result2);
		$comments_count = 0;
		if ($result2 && $result2->num_rows() > 0){
			foreach ($result2->result() as $row) {
				$comments_count++;
				//var_dump( $row);
				$user_id =$row->user_id;
				$complain_id=$row->complain_id;
				//var_dump($user_id);
			}
		}
				
		$sql="SELECT * FROM `complains` WHERE `complain_id` = $complain_id";
		$result = $this->db->query($sql); 
		$all_comments_html ='';
			
		if ($result && $result->num_rows() > 0){
			
			foreach ($result->result() as $row) { 
					$current_dat=strtotime($row->created_date);
					$change_date = date(" j M Y ",$current_dat);
				$sql="SELECT * FROM `customer_account` WHERE `customer_id` = $row->user_id";
				$result1 = $this->db->query($sql); 
				if ($result1 && $result1->num_rows() > 0){
					$data = $result1->result();
					$user_data = array(
						$user_name = $data[0]->first_name,
						$customer_img = $data[0]->customer_img,
					);
					//var_dump($user_data);
				}else{
					//redirect(base_url());
				}
				if(strlen($row->user_comment) >40){
					$show_ ='';
					//$margin='margin-top: 15px';
				}else{
					$show_ ='display_none';
					
				}
				
			
						
					$sql1="SELECT * FROM `complains` WHERE `complain_id` =".$row->complain_id." ORDER BY `complain_id` DESC ";
				$result2 = $this->db->query($sql1);
				
				$post_html_1='';
				if ($result2 && $result2->num_rows() > 0){
					//$datass = $result2->result();
					$sc_count =  $result2->num_rows();
					
					foreach ($result2->result() as $row12) { 
					
					
					$current_dat=strtotime($row12->created_date);
					$change_date = date(" j M Y ",$current_dat);
						
						$sql3="SELECT * FROM `customer_account` WHERE `customer_id` =".$row12->user_id;
						$result12 = $this->db->query($sql3); 
						if ($result12 && $result12->num_rows() > 0){
							$data12 = $result12->result();
							//$user_data = array(
								$user_name1 = $data12[0]->first_name;
								$last_name1 = $data12[0]->last_name;
								$customer_img1 = $data12[0]->customer_img;
							//);
						}else{
							
						}
						
						if($complain_id == $this->session->userdata('customer_id') || $row12->user_id == $this->session->userdata('customer_id')){
							$show_delete_='';
						}else{
							$show_delete_='display_none';
						}
						$post_html_1 .= '<div class="sh-comments__section" style="margin: 5px 30px;">
							<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 28px;height: 28px;" src="'.base_url().'uploads/'.$customer_img1.'" alt=""></a>
								<a class="sh-comments__name" style="font-size: 13px;">'.$user_name1.' '.$last_name1.'</a>
							<div class="sh-comments__content" style="position: relative;">
								
								<span class="sh-comments__passed" style="margin-left: 26px;font-size: 11px;">'.($change_date. ' ' .$row12->created_time) .'</span>
								<div style="display: flex;font-size: 13px;">
									<p  style=" margin-left: 26px;" class="p_dot" style="padding: 2px;">'.$row12->user_comment.'</p><i  style="color: #69B52D;" class="fa fa-comments" aria-hidden="true" data-toggle="modal" data-target="#sub_comments" comment_id="'.$row->complain_id.'"></i>
							
									<input type="hidden" class="post_Id" value="'.$row12->complain_id.'">
								
									<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
									<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
										<a class="sh-section__btn-comment sh-btn-icon" style="width: 100%;" ><span name="'.$row12->complain_id.'" class="sub_comment_" data-toggle="modal" data-target="#sub_comments">Comment</span></a><br>
										<a class="sh-section__btn-comment sh-btn-icon delete_comment '.$show_delete_.'" style="width: 100%;margin-top: 5px;"><span name="'.$row12->complain_id.'" style="font-size: 15px;">Delete</span></a>
									</div>
								</div>
							</div>
						</div>';
					}
				
				}else{
					$sc_count=0;
				}
				
				if($complain_id == $this->session->userdata('customer_id') || $row->user_id == $this->session->userdata('customer_id')){
					$show_delete='';
				}else{
					$show_delete='display_none';
				}
				$all_comments_html .='<div><div class="sh-comments__section comments__section'.$row->complain_id.'">
							<a href="javascript:void(0);" class="sh-comments__avatar sh-avatar"><img style="border-radius: 50px;width: 28px;height: 28px;" src="'.base_url().'uploads/'.$customer_img.'" alt=""></a>
							<a class="sh-comments__name">'.$user_name.'</a>
							<div class="sh-comments__content" style="position: relative;">
								
								<span class="sh-comments__passed" style="margin-left: 26px;font-size: 14px;">'.($change_date) .'</span><br/>
								<span class="sh-comments__passed" style="margin-left: 26px;font-size: 14px;">'.($row->created_time) .'</span>
								<div style="display: flex;">
									<p  style=" margin-left: 26px;"class="p_dot">'.$row->user_comment.'</p><i  style="color: #69B52D;" class="fa fa-comments" aria-hidden="true" data-toggle="modal" data-target="#sub_comments" comment_id="'.$row->complain_id.'"></i>
							
									<input type="hidden" class="post_Id" value="'.$complain_id.'">
									
									
									<a type="button" class="moreoptionButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;"><span class="menu_dots" data-toggle="tooltip" data-placement="top" title="More"></span></a>
									<div class="dropdown-menu post-dropdown-menu" aria-labelledby="moreoptionButton" style="padding: 10px 15px;">
										<a class="sh-section__btn-comment sh-btn-icon" style="width: 100%;" ><span name="'.$row->complain_id.'" class="sub_comment_" data-toggle="modal" data-target="#sub_comments">Comment</span></a><br>
										/*<a class="sh-section__btn-comment sh-btn-icon delete_comment_main '.$show_delete.'" style="width: 100%;margin-top: 5px;"><span name="'.$row->complain_id.'" style="font-size: 15px;">Delete</span></a>*/
									</div>
								</div>
							</div>
						</div>
						'.$post_html_1.'</div>';

			}
		}
	    
		$arr_response['number_of_comments'] = $comments_count;	
		$arr_response['all_comments_html'] = $all_comments_html;
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Comments get Successfully';
		echo json_encode($arr_response);
		
    }
	public function add_comment_post() {
		$data = array(
            'comment' => $this->post('comment'),
            'post_id' => $this->post('post_id'),
            'user_id' => $this->post('user_id'),
			'created_date' => date('Y-m-d'),
            'created_time' => date("H:i:s"),
        );
		$result = $this->db->insert('post_comments', $data);      
		$comment_id = $this->db->insert_id();
	
	
		
		$arr_response['comment_id'] = $comment_id;	
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Commented Successfully';
		echo json_encode($arr_response);
		
    }
	
	
	public function add_complain_post() {
		$pro_id= $this->post('product_id');
		$order_id= $this->post('order_id');
		$vendor_id= $this->post('vendor_id');
		$customer_id= $this->post('user_id');
		$log_details="complaint register";
		$data = array(
            'user_comment' => $this->post('comment'),
            'product_id' => $this->post('product_id'),
			'product_name' => $this->post('product_name'),
            'user_id' => $this->post('user_id'),
			'vendor_id' => $this->post('vendor_id'),
			'type' =>'customer',
			'order_id' => $this->post('order_id'),
			'problem_type' => $this->post('subject_title'),
			'status'=>'pending',
			'created_date' => date('Y-m-d'),
            'created_time' => date("H:i:s"),
        );
		//echo "fhfgh";
		$check_complaint_count=$this->db->get_where('complains',array('user_id'=>$this->post('user_id'),'product_id'=>$this->post('product_id'),'vendor_id'=>$this->post('vendor_id'),'order_id'=>$this->post('order_id')))->num_rows();
		//var_dump($check_complaint_count);
		//echo "fhfgh";
		if($check_complaint_count <=1){
			$result = $this->db->insert('complains', $data); 
			$complain_id = $this->db->insert_id();
			
			$data['log_data'] = $this->obj_aa->update_logdetails($pro_id,$order_id ,$vendor_id,$customer_id ,$log_details);
			$arr_response['complain_id'] = $complain_id;	
			$arr_response['status'] = 200;	
			$arr_response['message'] = 'Commented Successfully';
			echo json_encode($arr_response);
		}else{
			
		    
		    $arr_response['status'] = 201;	
		    $arr_response['message'] = ' Only 2 complaint you can register on per order';
		    echo json_encode($arr_response);
		}
				
	
		
    }
	
	public function update_status_of_complaint_post() {
		
		$data = array(        
			'status'=>'closed',			
        );				
		$result = $this->db->update('complains', $data, array('complain_id'=>$this->post('complain_id'))); 
		
		
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Status Updated Successfully';
		echo json_encode($arr_response);
    }
	
	public function update_status_of_complaint_for_refund_post(){
		if($this->post('request_new')){
			$data = array(        
				'ask_for_refund'=>$this->post('request_new'),			
			);
			$data1 = array(        
				'asked_refund'=>$this->post('request_new'),		
			);
			$result = $this->db->update('complains', $data, array('complain_id'=>$this->post('complain_id'))); 
		    $results = $this->db->update('product_order_detail', $data1, array('order_id'=>$this->post('order_id'))); 
		
		}
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Status Updated Successfully';
		echo json_encode($arr_response);
	}
	
	public function update_status_of_complaint_for_replace_post(){
		if($this->post('request_new')){
			$data = array(        
				'ask_for_replace'=>$this->post('request_new'),			
			);
			$data1 = array(        
				'asked_refund'=>$this->post('request_new'),			
			);
			$result = $this->db->update('complains', $data, array('complain_id'=>$this->post('complain_id'))); 
		    $results = $this->db->update('product_order_detail', $data1, array('order_id'=>$this->post('order_id'))); 
		
		}
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Status Updated Successfully';
		echo json_encode($arr_response);
	}
	
	public function update_status_of_ask_for_refund_post() {
		
		
		$data = array(        
			'ask_for_refund'=>'yes',			
        );	
		$data1 = array(        
			'ask_replacement'=>'yes',			
        );			
		$result = $this->db->update('complains', $data, array('complain_id'=>$this->post('complain_id'))); 
		$results = $this->db->update('product_order_detail', $data1, array('order_id'=>$this->post('order_id'))); 
		
		
		$arr_response['complain_id'] = $this->post('complain_id');	
		$arr_response['order_id'] =$this->post('order_id');	
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Status Updated Successfully';
		echo json_encode($arr_response);
    }
	
	public function update_status_of_ask_for_replace_post() {
		
		$data = array(        
			'ask_for_replace'=>'yes',			
        );	
		$data1 = array(        
			'ask_replacement'=>'yes',			
        );
		
		$result = $this->db->update('complains', $data, array('complain_id'=>$this->post('complain_id'))); 
		$results = $this->db->update('product_order_detail', $data1, array('order_id'=>$this->post('order_id'))); 
		
		$arr_response['status'] = 200;	
		$arr_response['message'] = 'Status Updated Successfully';
		echo json_encode($arr_response);
    }
	
	
	public function get_campaign_data_post() {
		
		$previous_campaign_product_id=$this->post('product_id');
		//var_dump($previous_campaign_product_id);
		$result=$this->db->get_where('products',array('product_id'=>$previous_campaign_product_id))->result();
										
		//var_dump($result);
		$data_value=array();
		$data_value1=array();

			foreach($result as $val){
				$created_date=$val->created_date;
				//var_dump($created_date);
				$needed_input=$val->needed_input;
				$product_id=$val->product_id;
					$today = date('Y-m-d');						
					$dateEnd = $today;
					$current_date = $created_date;
					//var_dump($current_date);
					
					$product_order_count= $this->db->get_where('product_order_detail',array('product_id'=>$product_id, 'created_date'=>$created_date))->num_rows();
					array_push($data_value1, $product_order_count);
					$current_dat=strtotime($current_date);
					$change_date = date(" j M Y ",$current_dat);
					array_push($data_value, $change_date);
					while(strtotime($current_date) < strtotime($dateEnd))
					{
						
						$current_date= date("Y-m-d",strtotime("+1 day",strtotime($current_date)));
						if(strtotime($current_date) <= strtotime($dateEnd)){
							//var_dump($current_date);
							//var_dump($dateEnd);
							
						$product_order_count= $this->db->get_where('product_order_detail',array('product_id'=>$product_id, 'created_date'=>$current_date))->num_rows();
						//var_dump($this->db->last_query());
						$current_dates=strtotime($current_date);
						$change_date = date("j M Y   ",$current_dates);
						
						//$final_date = substr($change_date, 0, 3);
						//var_dump($change_date);
						array_push($data_value, $change_date);
						array_push($data_value1, $product_order_count);
						//var_dump($data_value);
						//var_dump($product_order_count);
						
						}
						
						
					}
						

					
				} 
			
				
					$status = [
				'status' => 200,
				'message' => 'fetched successfully',
				'data' => $result,
				'data1' => $data_value,
				'data2' => $data_value1,
				
				
			];
			$this->set_response($status, REST_Controller::HTTP_OK);
			
			
			
			
    }

	

}
