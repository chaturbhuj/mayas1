<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Publicpages extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_public_content_data_post() {
		if(!$this->post('content_id')){
			$status = [
				['status' => 400 ,  'message' => 'content_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$content_id = $this->post('content_id');
			$db_result = $this->db->get_where('web_public_content', array('content_id' => $content_id));
			
			if ($db_result && $db_result->num_rows() > 0) {
				$content=$db_result->row()->content;
				
				
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $content,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get blog List
     */
    public function fatch_blog_list_data_post() {
		$content='';
		$limit = 6;
		$page_no = $this->post('page_no');
		if($page_no>0){
			$start = (integer($page_no) - 1)*$limit;
			$this->db->limit($start,$limit);
		}else{
			$this->db->limit($limit);
		}
		$this->db->select('*,(select user_name from web_admin_login where user_id=admin_blogs.FK_user_id) as post_by');
		$this->db->from('admin_blogs');
		$this->db->order_by('blog_id','DESC');
		$db_result = $this->db->get();
		
		if ($db_result && $db_result->num_rows() > 0) {
			foreach($db_result->result() as $row){
				//var_dump($row);
				$blog_title = $row->blog_title;
				$blog_content = $row->content;
				//var_dump($blog_content);
				$blog_for_url = strtolower($blog_title);
				$blog_for_url = preg_replace('/[^a-zA-Z ]/s', '', $blog_for_url);
				$blog_for_url = str_replace(' ', '-', $blog_for_url);
				
				$content .='<div class="col-md-6">';
					$content .='<article class="blog">';
						$content .='<figure>';
							$content .='<a href="'.base_url().'publicpages/detail?id='.$row->blog_id.'-'.$blog_for_url.'"><img src="'.base_url().'uploads/'.$row->blog_img.'" alt="xyz" class="img-fluid">';
								$content .='<div class="preview"><span>Read more</span></div>';
							$content .='</a>';
						$content .='</figure>';
						$content .='<div class="post_info">';
							$content .='<small>'.date("d M,Y", strtotime($row->date)).'</small><span style="float: right;font-weight: 500;color: #999;"></span>';
							$content .='<h2><a href="'.base_url().'publicpages/detail?id='.$row->blog_id.'-'.$blog_for_url.'">'.$row->blog_title.'</a></h2>';
							//$content .='<p>''</p>';
							
						$content .='</div>';
					$content .='</article>';
					
				$content .='</div>'; 
				
				
				
				
				
				
			/*	$content .=' <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">';
					$content.='	<a href="'.base_url().'publicpages/detail?id='.$row->blog_id.'-'.$blog_for_url.'">';
						$content .=' <div class="post-block">';
							$content .=' <!-- post vertical block -->';
							$content .=' <div class="post-img zoomimg">';
								$content .=' <img src="'.base_url().'uploads/'.$row->blog_img.'" alt="" class="img-fluid">';
							$content .=' </div>';
							$content .=' <div class="post-content">';
								$content .=' <h2 class="post-heading">'.$row->blog_title.' </h2>';
								$content .=' <p class="meta">';
									$content .=' <span class="meta-posted-by">By '.$row->post_by.'</span>';
									$content .=' <span class="meta-date"> '.date("d M,Y", strtotime($row->date)).'</span>';
								$content .=' </p>';
								$content .=' <span class="btn-default-link">Read More</span>';
							$content .=' </div>';
						$content .=' </div>';
						$content .=' <!-- /.post vertical block -->';
					$content .=' </a>';
				$content .=' </div>';    */
			
			
			}
			
			
			$total_rows= $db_result->num_rows();
			$total_page=$total_rows/6;
			$pagination ='';
			//print_r($total_page);
			if($total_page>0){
				$pagination .='<li class="page-item"><a class="page-link" href="#">Previous</a></li>';
				for ($i=1; $i<=$total_page; $i++) {  
					$pagination .='<li class="page-item active"><a class="page-link" href_link="'.$i.'">'.$i.'</a></li>';
				}
				
				$pagination .='<li class="page-item"><a class="page-link" href="#">Next</a></li>';
			}
			//print_r($pagination);
					
			$status = ['status' => 200 ,  
						'message' => 'fetched successfully',
						'data' => $content,
						'pagination' => $pagination,
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201 ,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
	
	
	/*
     * This function is used to get loggedin user details
     */
	public function get_address_content_post()
	{

		$country_list = $this->db->get('country_list')->result();
		$state_list = $this->db->get('state_list')->result();
		$city_list = $this->db->get('city_list')->result();

		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'country_list' => $country_list,
			'state_list' => $state_list,
			'city_list' => $city_list,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}

	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get recent blog List
     */
    public function fatch_recent_blog_list_data_post() {
		$content='';
		
		$this->db->order_by('blog_id','DESC');
		$this->db->limit(10);
		$db_result = $this->db->get('admin_blogs');
		if ($db_result && $db_result->num_rows() > 0) {
			foreach($db_result->result() as $row){
				$blog_title = $row->blog_title;
				$blog_for_url = strtolower($blog_title);
				$blog_for_url = preg_replace('/[^a-zA-Z ]/s', '', $blog_for_url);
				$blog_for_url = str_replace(' ', '-', $blog_for_url);
				
				$content.=' <!-- recent-post-block -->';
				$content.='	<li>';
				$content.='	<a href="'.base_url().'publicpages/detail?id='.$row->blog_id.'-'.$blog_for_url.'">';
					$content.='	<div class="row">';
						$content.='	<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">';
							$content.='	<div style="margin-bottom: 25px; border: 3px solid #fff;;" class="recent-post-img zoomimg">';
								$content.='	<img src="'.base_url().'uploads/'.$row->blog_img.'" alt="">';
							$content.='	</div>';
						$content.='	</div>';
						$content.='	<div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">';
							$content.='	<div class="recent-post-content">';
								$content.='	<h6 style="color:#5b5959;"class="recent-title">'.$row->blog_title.'</h6>';
								$content.='	<p  style="color:#5b5959; " class="meta font-italic">'.date("d M,Y", strtotime($row->date)).'</p>';
							$content.='	</div>';
						$content.='	</div>';
					$content.='	</div>';
				$content.='	</a>';
				$content.='	</li>';
				$content.='	<!-- /.recent-post-block --> ';
			}
			
			$status = ['status' => 200 ,  
						'message' => 'fetched successfully',
						'data' => $content,
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201 ,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get blog detail
     */
    public function fatch_blog_detail_data_post() {
		$content='';
		$limit = 10;
		$blog_data = $this->post('blog_data');
		$blog_data = explode("-",$blog_data);
		
		$this->db->select('*,(select user_name from web_admin_login where user_id=admin_blogs.FK_user_id) as post_by, (select count(comment_id) from admin_blog_comment where blog_id=admin_blogs.blog_id) as comments');
		$this->db->from('admin_blogs');
		$this->db->where('blog_id',$blog_data[0]);
		$db_result = $this->db->get();
		if ($db_result && $db_result->num_rows() > 0) {
			$row = $db_result->row();
			//var_dump($db_result->row());
				$data = array(
							'blog_id' => $row->blog_id,
							'blog_title' => $row->blog_title,
							'content' => $row->content,
							'blog_img' => base_url().'uploads/'.$row->blog_img,
							'post_by' => $row->post_by,
							'comments' => $row->comments,
							'date_data' => date("d M,Y", strtotime($row->date)),
						);
			$this->db->where('blog_id',$row->blog_id);
			$this->db->order_by('comment_id','DESC');
			$comments_data = $this->db->get('admin_blog_comment');
			$comment='';
			if ($comments_data && $comments_data->num_rows() > 0) {
				foreach($comments_data->result() as $val){
					//var_dump( $val);
					$comment.=' <div class="card-body">';
						$comment.=' <!-- comment-list -->';
						$comment.=' <ul class="comment-list listnone">';
							$comment.=' <li class="comment">';
								$comment.=' <div class="comment-body">';
									$comment.=' <div class="row">';
										$comment.=' <div class="col-xl-1 col-lg-1 col-md-1 col-sm-12 col-12">';
											$comment.=' <div class="comment-author"><img src="'.base_url().'uploads/'.$val->usercommimage.'" alt=" " style="width: 53px;" class="rounded-circle"> </div>';
										$comment.=' </div>';
										$comment.=' <div class="col-xl-11 col-lg-11 col-md-11 col-sm-12 col-12">';
											$comment.=' <div class="comment-info">';
												$comment.=' <div class="comment-header">';
													$comment.=' <h4 class="user-title">'.$val->user_name.'</h4>';
													$comment.=' <div class="comment-meta"><span class="comment-meta-date">'.date("d M,Y", strtotime($val->date)).'</span></div>';
												$comment.=' </div>';
												$comment.=' <div class="comment-content">';
													$comment.=' <p>'.$val->user_comment.'</p>';
												$comment.=' </div>';
											$comment.=' </div>';
										$comment.=' </div>';
									$comment.=' </div>';
								$comment.=' </div>';
							$comment.=' </li>';
						$comment.=' </ul>';
						$comment.=' <!-- /.comment-list -->';
					$comment.=' </div>';
				}
			}
			$status = ['status' => 200 ,  
						'message' => 'fetched successfully',
						'data' => $data,
						'comment' => $comment,
					];
			$this->set_response($status, REST_Controller::HTTP_OK);
		} else {
			#query failed or no matching values found
			$status = ['status' => 201 ,  'message' => 'something went worng'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
	}
	
	
	
	public function blog_comment_submit_post() {
		
		$this->db->where('customer_id', $this->post('customer_id'));
		$db_result = $this->db->get('customer_account');
		if ($db_result && $db_result->num_rows() > 0) {
			
			$row = $db_result->row();
			
			$data=array(
				'user_id' => $row->customer_id,
				'user_name' => $row->first_name,
				'comment_email' => $row->customer_email,
				'user_comment' => $this->post('user_comment'),
				'blog_id' => $this->post('blog_id'),
				'usercommimage' => $row->customer_img,
				'status' => 'active',
				'date' => date('Y-m-d'),
				'time' => date('H:i:s'),
			);
			$result = $this->db->insert('admin_blog_comment', $data);
			if($result){
				$status = ['status' => 200 ,  
							'message' => 'Comment Successfully submitted.',
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{				
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again.'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}else{				
			#query failed or no matching values found
			$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}
    }
	
	public function contact_form_submit_post() {
		if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('contact_email')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('contact_subject')){
			$status = ['status' => 400 ,  'message' => 'Purpose is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			
			
			$data = array(
				'contact_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'contact_phone' => $this->post('contact_phone'),
				'contact_email' => $this->post('contact_email'),
				'contact_message' => $this->post('contact_message'),
				'contact_status' => 'active',
				'date' => date('Y-m-d'),
			);
			$result = $this->db->insert('web_contact_us', $data);
			
		   
			if ($result) {
				$status = ['status' => 200 ,'message' => 'Your request has been successfully submitted.',];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get faq page data
     */
    public function fatch_faq_page_data_post() {
		$content='';
				$db_result = $this->db->get('admin_faq');
				if ($db_result && $db_result->num_rows() > 0) {					
								$content.='	<div class="accordion" id="accordionExample">';
									foreach($db_result->result() as $row){
										//var_dump($row);
										
											$content.='	<div class="card">';
											$content.='	<div class="card-header" style="border-radius: 2px; background: #fcfcfc;box-shadow: inset 16px 16px 29px #f1f0f0, inset -16px -16px 29px #ffffff;" id="'.$row->faq_id.'">';
											$content.='<i class="fa fa-chevron-down float-right" data-toggle="collapse" data-target="#collapseOne'.$row->faq_id.'" aria-expanded="true" aria-controls="collapseOne'.$row->faq_id.'" stye="margin-top:10px;"></i>';		
											$content.='<h5 class="mb-0">';		
																			
											$content.='<div class="btn btn-link question_btn" data-toggle="collapse" data-target="#collapseOne'.$row->faq_id.'" aria-expanded="true" aria-controls="collapseOne'.$row->faq_id.'" >';
											$content.='<h4 style="color:#000; font-size:14px;">'.$row->question.'</h4>';
										
											$content.='</div>';	
											$content.=' </h5>';
											$content.='	</div>';
											$content.='<div id="collapseOne'.$row->faq_id.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">';
											$content.=' <div class="card-body">';
											$content.='	<p>'.$row->answers.'</p>';
											$content.='	</div>';
											$content.='	</div>';
											$content.='	</div>';
											$content.='	</div>';
										$content.='	<!-- /.faq-block -->';
									}	
							$content.='	</div>';
							$content.='	<!-- /.faq-category-block -->';
						$content.='	</div>';
					$content.='	</div>  ';
					
				}
			
				
				

				
		$status = ['status' => 200 ,  
					'message' => 'successfully',
					'data' => $content,
					//'category' => $category,
				];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	public function get_header_menu_post()
	{
		$data = array();
		$vendor_id = $this->post('vendor_id');

		$sql = "SELECT m.* FROM `main_category` as m INNER JOIN products as p on m.id = p.main_category_id WHERE p.`vendor_id` = '$vendor_id' AND `category_status` = 'active' AND p.`product_status` = 'active' AND p.`admin_permission` = 'active' GROUP BY p.`main_category_id`";
		$main_category = $this->db->query($sql)->result();

		foreach ($main_category as $key => $value) {
			$data[$key] = $value;

			$sql = "SELECT pc.* FROM `product_category` as pc INNER JOIN products as p on pc.product_category_id = p.product_category_id WHERE p.`vendor_id` = '$vendor_id' AND pc.`main_cate_id` = '$value->id' AND `category_status` = 'active' AND p.`product_status` = 'active' AND p.`admin_permission` = 'active' GROUP BY p.`product_category_id`";
			$category_data = $this->db->query($sql)->result();

			foreach ($category_data as $key1 => $val) {
				$data[$key]->category_data[$key1] = $val;
				$sql = "SELECT psc.* FROM `product_sub_category` as psc INNER JOIN products as p on psc.product_sub_category_id = p.product_sub_category_id WHERE p.`vendor_id` = '$vendor_id' AND psc.`main_cate_id` = '$value->id' AND psc.`product_category_id` = '$val->product_category_id' AND `category_status` = 'active' AND p.`product_status` = 'active' AND p.`admin_permission` = 'active' GROUP BY p.`product_sub_category_id`";
				$subcategory_data = $this->db->query($sql)->result();

				$data[$key]->category_data[$key1]->subcategory_data = $subcategory_data;
				//var_dump($subcategory_data);
			}
		}
		//var_dump($data);
		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'data' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}
	
	/*
     * This function is used to get loggedin user details
     */
	
	public function get_header_search_menu_post(){
	
	
		$search_data = $this->post('search_data');
		
		
		$this->db->like('product_name', $search_data);
		
		//$this->db->join('product_sub_category', 'product_sub_category.product_sub_category_id = products.product_sub_category_id', 'left');
		$this->db->limit(5);
		$data = $this->db->get('mayas_newproduct')->result();
		//var_dump($this->db->last_query());
		$content = '';
		//var_dump($data);
		if ($data) {
			foreach ($data as $key => $value) {
				//var_dump($value);
				
				//var_dump($value->product_id);
				//var_dump($value->main_category_id);
				$content .= '<a class="dropdown-item" href="' . base_url() . 'search/product?id=' . $value->product_category . '&search=' . $value->product_name . '">' . $value->product_name .' </a>';
			}
		}

		$status = [
			'status' => 200,
			'message' => 'fetched successfully',
			'data' => $content,
			'data1' => $data,
		];
		$this->set_response($status, REST_Controller::HTTP_OK);
	}


		//user_search_data_post///
	
	
	public function user_search_data_post()
	{
			$data = array(
				'user_id' 		=> $this->session->userdata('customer_id'),
				'search_poduct_data'	=> $this->post('search_poduct_data'),
				'created_date' 	=> date('Y-m-d'),
				'created_time' 	=> date('H:m:s'),	
			);
			$result =	$this->db->insert('user_search_details', $data);
			
			
			if ($result) {
				$status = [
			'status' => 200,
			'message' => 'inserted successfully',
			
			'data' =>$data,
			 
		];
			}
		$this->set_response($status, REST_Controller::HTTP_OK);				
		}

     
}
