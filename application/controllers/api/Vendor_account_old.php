<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
ini_set('memory_limit','64M');
ini_set('max_execution_time', 30000);


header('content-type: application/json; charset=utf-8');
header("access-control-allow-origin: *");
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Lennox Software
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Vendor_account extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		
        // Configure limits on our controller methods 
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
    }
	
	public function update_profile_post() {
		if(!$this->post('full_name')){
			$status = ['status' => 400 ,  'message' => 'Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('vendor_email')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
			
		}else{			
			$data = array(
				'full_name' => $this->post('full_name'),
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $this->post('vendor_id')));
												
			$status = ['status' => 200 , 'message' => 'Profile updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get loggedin user details
     */
    public function fatch_profile_post() {
		if(!$this->post('vendor_id')){
			$status = [
				['status' => 400 ,  'message' => 'vendor_id is required.'],
			];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			$vendor_id = $this->post('vendor_id');
			$db_result = $this->db->get_where('vendor_account', array('vendor_id' => $vendor_id));
			
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					
							
					if (!array_key_exists($row->vendor_id, $data)) {
						$data[$row->vendor_id] = array();
					}
					if (array_key_exists($row->vendor_id, $data)) {
						$data[$row->vendor_id] = array(
							'vendor_id' => $row->vendor_id,
							'vendor_email' => $row->vendor_email,
							'full_name' => $row->full_name,
						);
						array_push($data_value, $data[$row->vendor_id]);
					}
				}
				$status = ['status' => 200 ,  
							'message' => 'fetched successfully',
							'data' => $data_value,
						];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
	}

	public function check_vendor_old_password_post() {
		
		$pass = sha1('admin' . (md5('admin' . $this->post('currentpassword'))));
        $db_result = $this->db->get_where('vendor_account', array('vendor_password' => $pass,'vendor_id' => $this->post('param'),));
        
        if ($db_result->num_rows() > 0) {
            echo 'true';
        } else {
            echo 'false';
        }
	}

	public function update_password_post() {
		if(!$this->post('new_password')){
			$status = ['status' => 400 ,  'message' => 'New Password is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			#Generate hashed password.
			$pass = sha1('admin' . (md5('admin' . $this->post('new_password'))));
			//$pass=$_POST['user_password'];
			$data = array(
				'vendor_password' => $pass
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $_POST['vendor_id']));
												
			$status = ['status' => 200 , 'message' => 'Password updated successfully'];
			$this->set_response($status, REST_Controller::HTTP_OK);
		}
    }

	//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_profile_image_post() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $this->post('image_cat') . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
			
            move_uploaded_file($file, $target);
			$data = array(
				'vendor_img' => $filename
			);
			$result = $this->db->update('vendor_account', $data, array('vendor_id' => $this->post('vendor_id')));
			
            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }
	
	public function update_list_item_post() {
		if(!$this->post('title')){
			$status = ['status' => 400 ,  'message' => 'Tital is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('vendor_id')){
			$status = ['status' => 400 ,  'message' => 'something went worng.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
			
		}else{	
			$item_id=$this->post('item_id');
			$image=json_encode($this->post('images'));
			$amenities_id=json_encode($this->post('amenities_id'));
			$event_type_id=json_encode($this->post('event_type_id'));
			$data = array(
				'vendor_id' 		=> $this->post('vendor_id'),
				'title' 			=> $this->post('title'),
				'guest_capacity' 	=> $this->post('guest_capacity'),
				'area' 				=> $this->post('area'),
				'price_hour' 		=> $this->post('price_hour'),
				'price_day'			=> $this->post('price_day'),
				'amenities_id' 		=> $amenities_id,
				//'amenities' 		=> $this->post('amenities'),
				'event_type_id' 	=> $event_type_id,
				//'event_type' 		=> $this->post('event_type'),
				'discription' 		=> $this->post('discription'),
				'feactures' 		=> $this->post('feactures'),
				'address' 			=> $this->post('address'),
				'city' 				=> $this->post('city'),
				'state' 			=> $this->post('state'),
				'country' 			=> $this->post('country'),
				'latitude' 			=> $this->post('latitude'),
				'longitude' 		=> $this->post('longitude'),
				'primary_image' 	=> $this->post('primary_image'),
				'images' 			=> $image,
				'video_link' 		=> $this->post('video_link'),
				'created_date' 		=> date('Y-m-d'),
				'created_time' 		=> date('H:i:s'),
			);
			if($item_id==0){
				$msg='added successfully';
				$result = $this->db->insert('vendor_list_items', $data);
			}else{
				$msg='updated successfully';
				$result = $this->db->update('vendor_list_items', $data, array('item_id' => $item_id));
			}
		
			if($result)	{							
				$status = ['status' => 200 , 'message' => $msg];
				$this->set_response($status, REST_Controller::HTTP_OK);
			}else{
				#query failed or no matching values found
				$status = ['status' => 201 ,  'message' => 'something went worng'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}
    }

}
