<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	/**
	 * 
	 
	 */
    public function upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 20485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
                $target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
            }else{
                $target = './uploads/' . $filename;
            }
            
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }


    public function upload_file() {
        #array to hold the response values to be displayed
        $arr_response = array();
        if (isset($_FILES['myFile'])) {
            $info = pathinfo($_FILES['myFile']['name']);
            $file_size=$_FILES['myFile']["size"];
            $upload_file=$_FILES['myFile']['name'];
            $ext = '';
            if(isset($info['extension'])){
                $ext = $info['extension']; // get the extension of the file
            }
            
            // if (($ext == "pdf" || $ext == "PDF" || $ext == "tiff" || $ext == "msg" || $ext == "doc"|| $ext == "docx" ||  $ext == "xlsx"  || $ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg')) {
            if (($ext == "pdf" || $ext == "PDF" ||  $ext == "PNG" || $ext == "JPG" || $ext == "jpg" ||  $ext == 'png' || $ext == 'jpeg')  && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png' || $_FILES["myFile"]["type"] == 'application/pdf' ) && ($_FILES["myFile"]["size"] < 20485760)) {
                $file = $info['filename'];
                $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
                if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
                    $target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
                }else{
                    $target = './uploads/' . $filename;
                }
                $file = $_FILES['myFile']['tmp_name'];
                move_uploaded_file($file, $target);
                $arr_response['status'] = 200;
                $arr_response['filename'] = $filename;
                $arr_response['upload_file'] = $upload_file;
                $arr_response['file_size'] = $file_size;
                $arr_response['ext'] = $ext;
                //$arr_response['target'] = $target;
            } else {
                $arr_response['status'] = FAILED;
                $arr_response['ext'] = $ext;
                $arr_response['file_size'] = $file_size;
                $arr_response['message'] = "Not a valid File";
            }
            // Process file upload
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
            // Handle case when 'myFile' key is not found
        }
     
        echo json_encode($arr_response);
    }


    // public function upload_image() {
    //     // Array to hold the response values to be displayed
    //     $arr_response = array();
    
    //     // Get the file extension
    //     $info = pathinfo($_FILES['myFile']['name']);
    //     $ext = strtolower($info['extension']);
    
    //     // Valid file extensions and types
    //     $allowed_extensions = array('jpg', 'jpeg', 'png', 'gif', 'pdf');
    //     $allowed_types = array('image/jpeg', 'image/png', 'image/gif', 'application/pdf');
    
    //     // Check if the file extension and type are allowed
    //     if (in_array($ext, $allowed_extensions) && in_array($_FILES["myFile"]["type"], $allowed_types) &&
    //         ($_FILES["myFile"]["size"] < 20485760)) {
    
    //         // Generate a unique filename
    //         $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
    
    //         // Define the target directory
    //         if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
    //             $target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
    //         } else {
    //             $target = './uploads/' . $filename;
    //         }
    
    //         // Move the uploaded file to the target directory
    //         $file = $_FILES['myFile']['tmp_name'];
    //         move_uploaded_file($file, $target);
    
    //         // Set response values
    //         $arr_response['status'] = 200;
    //         $arr_response['filename'] = $filename;
    //     } else {
    //         // Invalid file type or size
    //         $arr_response['status'] = 'FAILED';
    //         $arr_response['message'] = "Not a valid file or file size exceeds limit";
    //     }
    
    //     // Send JSON response
    //     echo json_encode($arr_response);
    // }
    

    // public function upload_image() {
    //     #array to hold the response values to be displayed
    //     $arr_response = array();

    //     $info = pathinfo($_FILES['myFile']['name']);
    //     $ext = $info['extension']; // get the extension of the file

    //     if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
    //             ($_FILES["myFile"]["size"] < 20485760)) {
    //         $file = $info['filename'];
    //         $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
    //         if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
	// 			$target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
	// 		}else{
	// 			$target = './uploads/' . $filename;
	// 		}
			
    //         $file = $_FILES['myFile']['tmp_name'];
    //         move_uploaded_file($file, $target);

    //         $arr_response['status'] = 200;
    //         $arr_response['filename'] = $filename;
    //     } else {
    //         $arr_response['status'] = FAILED;
    //         $arr_response['message'] = "Not a valid File";
    //     }
    //     echo json_encode($arr_response);
    // }
	
	public function croppie_image_upload() {
        //array to store ajax responses
        $arr_response = array('status' => 500);
         
        $data = $_POST["image"];
        $image_name=$data;
        if(strpos($data, ';') !== false){
            $image_array_1 = explode(";", $data);   
            $image_name=$image_array_1[1];
        }

        if(strpos($image_name, ',') !== false){

            $image_array_2 = explode(",", $image_name);
            $image_name=$image_array_2[1];
        }

        if ($image_name) {
            $data = base64_decode($image_name);

            $imageName = time() . '.png';
            $image_url = APPPATH.'../uploads/'.$imageName;

            file_put_contents($image_url, $data);  
			
            $arr_response['status'] = 200;
            $arr_response['imageName'] =  $imageName;
            $arr_response['message'] = ' successfully'; 
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function remove_data() {
		$arr_response = array();
		$return_val = $this->db->delete($_POST['table_name'],array($_POST['column_name'] =>  $_POST['column_value']));		
		$arr_response['status'] = 200;
		$arr_response['message'] = "Removed Successfully!";
		echo json_encode($arr_response);
    }
	
	public function crop_upload_image() {
        
        $avatar_data = isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null;
        $this->load->model('Comman_model', 'obj_con', TRUE);
        $response = $this->obj_con->crop_edit_upload_image('avatar_file',$avatar_data);
            //var_dump($response);
      
            $return = array(
                'state' => 200,
                'message' => 'Picture Cropped..!!!',
                'result' => $response['pic']
            );
      
        echo json_encode($return);
    }
}
