<?php
if (!defined('BASEPATH'))
    exit('Not a valid request!');

/**
 * Controller class to configure RBAC system.
 * 
 */
class Content_setting extends CI_Controller {

    /**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 1)) {
            redirect(base_url() . 'login');
        }
    }
	public function index() {
        
        //$this->load->view('configure/configure_header');
		$this->load->view('admin/component/dashboard');
      
        //$this->load->view('configure/configure_footer');
    }
	
	function country_view()
	{
		
		 $data['details'] = $this->db->get('countries')->result_array();
		 $this->load->view('configure/configure_header');
		
        $this->load->view('admin/page/country',$data);
		
	}
	
	public function Country_add()
	{
		 
		 
		 if(isset($_GET['id']))
		 {
			 $segment = $_GET['id'];
		 }else
		 {
			 $segment = 0;
		 }
		                        
		 
		  $this->db->where('id', $segment );
         $form_data = $this->db->get('countries')->row_array();
		 $data['details'] = $form_data;
		 $this->load->view('configure/configure_header');
		
		 $this->load->view('admin/page/country_add',$data);
		// $this->load->view('admin/components/footer');
	}
	
	function country_update()
	{
		$array_response = array('status'=>500);
		$country_id = $this->input->post('country_id');;
		$country_name = $this->input->post('country_name');
		
		$data = array(
		        
				'name' => $country_name,
		);
		
		if($country_id == 0)
		  {
			$return_val = $this->db->insert('countries',$data);

		  } else
		  {
			$this->db->where('id',$country_id);
            $return_val = $this->db->update('countries',$data);
		  }
		  
		  if($return_val)
		  {
			
			$array_response['status'] = 200;
            if ($country_id != 0) {
				$array_response['message'] = 'Country has been updated successfully';
			}else{
				$array_response['message'] = 'Country has been added successfully';
			}

		  } else
		  {
            $array_response['status'] = 201;
			$array_response['message'] = "Somthing went wrong ! plz try again";
		  }
            
		  echo json_encode($array_response);
	}
	public function country_remove(){
          
		

		$array_response = array('status' => 500);
		$country_id = $this->input->post('country_id');
		
        $this->db->where('id',$country_id);
		$return_val = $this->db->delete('countries');
		
		if ($return_val) {
			$array_response['status'] = 200;
			$array_response['message'] = 'Country removed successfully';	
	    } else {
			$array_response['status'] = 201;
			$array_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($array_response);

	}
	
	function state_view()
	{
		$this->db->select('states.*');
        $this->db->select('countries.name AS Cname');
        $this->db->join('countries', 'states.country_id = countries.id');
		$list_data = $this->db->get('states');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->id ,$data1))
				{
					$data1[$row->id ] = array();
				}
				if(array_key_exists($row->id ,$data1))
				{
					
					
					$data1[$row->id ] = array(
					
				    'id' => $row->id,
					'name' => $row->name,
					'Cname' => $row->Cname,
					
					
					);
				}
				
					
				array_push($data_value,$data1[$row->id ]);
			}
			
		    $data['details'] = $data_value;
		}	
		
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/state',$data);
		
	}
	public function state_add()
	{
		 
		 
		 if(isset($_GET['id']))
		 {
			 $segment = $_GET['id'];
		 }else
		 {
			 $segment = 0;
		 }
		                        
		 $this->db->where('id', $segment );
         $form_data = $this->db->get('states')->row_array();
		 $data['details'] = $form_data;
		 $data['countries'] = $this->db->get('countries')->result_array();
		
		 $this->load->view('configure/configure_header');
		$this->load->view('admin/page/state_add',$data);
		
	}
	public function state_update()
	{ 
		 $array_response = array('status'=>500);
		$state_id = $this->input->post('state_id');
		$state_name = $this->input->post('state_name');
		$country_name = $this->input->post('country_id');
		
		$data = array(
		        
				'name' => $state_name,
				'country_id' => $country_name ,
		);
		
		if($state_id == 0)
		  {
			$return_val = $this->db->insert('states',$data);

		  } else
		  {
			$this->db->where('id',$state_id);
            $return_val = $this->db->update('states',$data);
		  }
		  
		  if($return_val)
		  {
			
			$array_response['status'] = 200;
            if ($state_id != 0) {
				$array_response['message'] = 'States has been updated successfully';
			}else{
				$array_response['message'] = 'States has been added successfully';
			}

		  } else
		  {
            $array_response['status'] = 201;
			$array_response['message'] = "Somthing went wrong ! plz try again";
		  }
            
		  echo json_encode($array_response);
	}
	
	public function state_remove(){
          
		

		$array_response = array('status' => 500);
		$state_id = $this->input->post('state_id');
		
        $this->db->where('id',$state_id);
		$return_val = $this->db->delete('states');
		
		if ($return_val) {
			$array_response['status'] = 200;
			$array_response['message'] = 'States removed successfully';	
	    } else {
			$array_response['status'] = 201;
			$array_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($array_response);

	}
	function city_view()
	{
		 
		// $data['details'] = $this->db->get('cities')->result_array();
		 $this->db->select('cities.*');
        $this->db->select('countries.name AS Cname');
		$this->db->select('states.name AS Sname');
		$this->db->join('states', 'cities.state_id = states.id','left');
        $this->db->join('countries', 'cities.country_id = countries.id','left');
		$list_data = $this->db->get('cities');
		$data_value = array();
		 if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->id ,$data1))
				{
					$data1[$row->id ] = array();
				}
				if(array_key_exists($row->id ,$data1))
				{
					
					
					$data1[$row->id ] = array(
					
				    'id' => $row->id,
					'city' => $row->city,
					'Cname' => $row->Cname,
					'Sname' => $row->Sname,
					
					
					);
				}
				
					
				array_push($data_value,$data1[$row->id ]);
			}
			
		    $data['details'] = $data_value;
		}	
		 
		 $this->load->view('configure/configure_header');
		
        $this->load->view('admin/page/city',$data);
		
	}
	
	public function city_add()
	{
		 
		 
		 if(isset($_GET['id']))
		 {
			 $segment = $_GET['id'];
		 }else
		 {
			 $segment = 0;
		 }
		                        
		 $this->db->where('id', $segment );
         $form_data = $this->db->get('cities')->row_array();
		 $data['details'] = $form_data;
		 $data['states'] = $this->db->get('states')->result_array();
		 $data['countries'] = $this->db->get('countries')->result_array();
		
		 $this->load->view('configure/configure_header');
		$this->load->view('admin/page/city_add',$data);
		
	}
	
	public function city_update()
	{ 
		 $array_response = array('status'=>500);
		$city_id = $this->input->post('city_id');
		$city_name = $this->input->post('city_name');
		$state_id = $this->input->post('state_id');
		$country_id = $this->input->post('country_id');
		
		$data = array(
		        
				'city' => $city_name,
				'state_id' => $state_id ,
				'country_id' => $country_id,
		);
		
		if($city_id == 0)
		  {
			$return_val = $this->db->insert('cities',$data);

		  } else
		  {
			$this->db->where('id',$city_id);
            $return_val = $this->db->update('cities',$data);
		  }
		  
		  if($return_val)
		  {
			
			$array_response['status'] = 200;
            if ($city_id != 0) {
				$array_response['message'] = 'City has been updated successfully';
			}else{
				$array_response['message'] = 'City has been added successfully';
			}

		  } else
		  {
            $array_response['status'] = 201;
			$array_response['message'] = "Somthing went wrong ! plz try again";
		  }
            
		  echo json_encode($array_response);
	}
	
	public function city_remove(){
          
		

		$array_response = array('status' => 500);
		$city_id = $this->input->post('city_id');
		
        $this->db->where('id',$city_id);
		$return_val = $this->db->delete('cities');
		
		if ($return_val) {
			$array_response['status'] = 200;
			$array_response['message'] = 'City removed successfully';	
	    } else {
			$array_response['status'] = 201;
			$array_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($array_response);

	}
	function public_content()
	{
		
		$list_data = $this->db->get('admin_public_content');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'page_heading' => $row->page_heading,
					'page_url' => $row->page_url,
					'url_specification' => $row->url_specification,
					'page_content' => $row->page_content,
					'image' => $row->image,
					'linked_services' => $row->linked_services,
					'page_title' => $row->page_title,
					'page_meta' => $row->page_meta,
					'page_keyword' => $row->page_keyword,
					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		}	
		 $data['public_page'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/public_content',$data);
		
	}


	
	function public_vastucontent()
	{
		
		$list_data = $this->db->get('admin_section_content');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'section_title1' => $row->section_title1,
					'section_image1' => $row->section_image1,
					'section_content1' => $row->section_content1,
					'section_title2' => $row->section_title2,
					'section_image2' => $row->section_image2,
					'section_content2' => $row->section_content2,
					'section_title3' => $row->section_title3,
					'section_image3' => $row->section_image3,
					'section_content3' => $row->section_content3,
					'banner_content' => $row->banner_content,
					'button_text' => $row->button_text,
					'faq_category' => $row->faq_category,
					'banner_id' => $row->banner_id,
					'about_content_id' => $row->about_content_id,
					'meta_keyword' => $row->meta_keyword,
					'meta_description' => $row->meta_description,
					'meta_title' => $row->meta_title,
					'button_url' => $row->button_url,
					'banner_image' => $row->banner_image,
					'meta_image' => $row->meta_image,

					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		}	
		 $data['public_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/public_vastucontent',$data);
		
	}
	function banner_list()
	{
		
		$list_data = $this->db->get('mayas_banner');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->banner_id ,$data1))
				{
					$data1[$row->banner_id] = array();
				}
				if(array_key_exists($row->banner_id ,$data1))
				{
					
					
					$data1[$row->banner_id ] = array(
					
				    'banner_id' => $row->banner_id,
					'banner_content' => $row->banner_content,
					'banner_title' => $row->banner_title,
					'button_text' => $row->button_text,
					'button_url' => $row->button_url,
					'banner_image' => $row->banner_image,

					
					);
				}
				
					
				array_push($data_value,$data1[$row->banner_id ]);
			}
			
		}	
		 $data['banner_list'] = $data_value;
		//  var_dump($data);
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/banner_list',$data);
		
	}
	function banner_add()
	{
		
		$segment = 0;

		if(isset($_GET['id']))
		{
			$segment = $_GET['id'];
		}else
		{
			$segment = 0;
		}
		
		// $data['linked_services'] = $this->db->get('mayas_services')->result_array();   
	//    $this->db->order_by("services_position", "desc");
	//   $data['linked_services'] = $this->db->get_where('mayas_services', array('status' => 'active'))->result_array(); 
		
	   $this->db->where('banner_id', $segment );
	   $data['public_page'] = $this->db->get('mayas_banner')->row_array();	
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/banner_add',$data);
		
	}
	function public_vastuaboutcontent()
	{
		
		$list_data = $this->db->get('vastu_about_content');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->id ,$data1))
				{
					$data1[$row->id] = array();
				}
				if(array_key_exists($row->id ,$data1))
				{
					
					
					$data1[$row->id ] = array(
					
				    'id' => $row->id,
					'about_title' => $row->about_title,
					'about_content' => $row->about_content,
					'button_text' => $row->button_text,
					'button_url' => $row->button_url,

					
					);
				}
				
					
				array_push($data_value,$data1[$row->id ]);
			}
			
		}	
		 $data['public_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/vastuaboutlist',$data);
		
	}
	function public_faqlist()
	{
		
		$list_data = $this->db->get('admin_faq');
		$category_list = $this->db->get_where('admin_faq_category');
		// $category_list = $this->db->get_where('admin_faq_category', array('process_id' => 12));

		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'section_title' => $row->section_title,
					'section_image' => $row->section_image,
					'banner_content' => $row->banner_content,
					'button_text' => $row->button_text,
					'faq_category' => $row->faq_category,
					'meta_keyword' => $row->meta_keyword,
					'meta_description' => $row->meta_description,
					'meta_title' => $row->meta_title,
					'button_url' => $row->button_url,
					'banner_image' => $row->banner_image,
					'meta_image' => $row->meta_image,

					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		}	
		 $data['public_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/public_vastucontent',$data);
		
	}



	function public_process()
	{
		
		$list_data = $this->db->get('admin_process_point');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->process_id ,$data1))
				{
					$data1[$row->process_id] = array();
				}
				if(array_key_exists($row->process_id ,$data1))
				{
					
					
					$data1[$row->process_id ] = array(
					
				    'process_id' => $row->process_id,
					'process_title' => $row->process_title,
					'process_image' => $row->process_image,
					'process_content' => $row->process_content,
				

					
					);
				}
				
					
				array_push($data_value,$data1[$row->process_id ]);
			}
			
		}	
		 $data['process_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/processlist',$data);
		
	}



	public function add_form()
	{
		$arr_response = array('status' => 500);
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$where = array($_POST['primary_column_name'] => $_POST['id']);
		if ($_POST['id'] == 0) {
			$return_val = $this->obj_comman->insert_data($_POST['table_name'], $_POST['table_field'][0]);
			$message = 'Successfully added';
		} else {
			$return_val = $this->obj_comman->update_data($_POST['table_name'], $_POST['table_field'][0], $where);
			$message = 'Successfully updated';
		}
		//echo "szfadf";
		if ($return_val) {
			$cache_dir = APPPATH . 'cache/'; // Adjust the path if your cache files are located elsewhere
			$files = glob($cache_dir . 'faq_data_*'); // Get all cache files starting with faq_data_
	
			$success = true;
			foreach($files as $file) {
				if(is_file($file)) {
					if(!unlink($file)) {
						$success = false;
						break;
					}
				}
			}

			$arr_response['status'] = 200;
			$arr_response['message'] = $message;
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}
		echo json_encode($arr_response);
	}
	function faqlist()
	{
		
		$list_data = $this->db->get('admin_faq');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->faq_id ,$data1))
				{
					$data1[$row->faq_id] = array();
				}
				if(array_key_exists($row->faq_id ,$data1))
				{
					
					
					$data1[$row->faq_id ] = array(
					
				    'faq_id' => $row->faq_id,
					'question' => $row->question,
					'answers' => $row->answers,
					'faq_category_id' => $row->faq_category_id,
				

					
					);
				}
				
					
				array_push($data_value,$data1[$row->faq_id ]);
			}
			
		}	
		 $data['faq_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/faq_list',$data);
		
	}
	function faqcatlist()
	{
		
		$list_data = $this->db->get('admin_faq_category');
		$data_value = array();
		//$data['details'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->faq_category_id ,$data1))
				{
					$data1[$row->faq_category_id] = array();
				}
				if(array_key_exists($row->faq_category_id ,$data1))
				{
					
					
					$data1[$row->faq_category_id ] = array(
					
				    'faq_category_id' => $row->faq_category_id,
					'faq_category_name' => $row->faq_category_name,
				

					
					);
				}
				
					
				array_push($data_value,$data1[$row->faq_category_id ]);
			}
			
		}	
		 $data['faq_category_list'] = $data_value;
         $this->load->view('configure/configure_header');
        $this->load->view('admin/page/faq_category_list',$data);
		
	}






	
	function page_add()
	{
		 
		 
		 if(isset($_GET['id']))
		 {
			 $segment = $_GET['id'];
		 }else
		 {
			 $segment = 0;
		 }
		 
		 // $data['linked_services'] = $this->db->get('mayas_services')->result_array();   
		$this->db->order_by("services_position", "desc");
       $data['linked_services'] = $this->db->get_where('mayas_services', array('status' => 'active'))->result_array(); 
		 
        $this->db->where('content_id', $segment );
        $data['public_page'] = $this->db->get('admin_public_content')->row_array();
		 
		// $data['public_page'] = json_decode($form_data);
		 $this->load->view('configure/configure_header');
		
         $this->load->view('admin/page/public_page_add',$data);
		//$this->load->view('admin/components/footer');
	}
	function cat_add()
	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		 
		 
		 if(isset($_GET['id']))
		 {
			 $segment = $_GET['id'];
		 }else
		 {
			 $segment = 0;
		 }
		
		//Load Required modal
		$data['records'] = $this->obj_comman->get_by('admin_faq_category',array('faq_category_id'=>$segment),false,true);
		
	   
		 
		// $data['public_page'] = json_decode($form_data);
		 $this->load->view('configure/configure_header');
		
         $this->load->view('admin/page/faq_category_add',$data);
		//$this->load->view('admin/components/footer');
	}




	public function page_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$page_id = $this->input->post('page_id');
		$access_define = $this->input->post('access_define');
		
		$linked_services = json_encode($access_define);
		
		$data = array(
		       
		        'page_heading' => $this->input->post('page_heading'),
				'page_url' => $this->input->post('page_url'),
				'url_specification' => $this->input->post('url_specification') ,
				'linked_services' => $linked_services,
				'page_title' => $this->input->post('page_title'),
				'page_meta' => $this->input->post('page_meta'),
				'page_content' => $this->input->post('page_content'),
				'page_keyword' => $this->input->post('page_keyword'),
				'image' => $this->input->post('public_content_image'),
		);
		
		if($page_id == 0)
		  {
			$return_val = $this->db->insert('admin_public_content',$data);

		  } else
		  {
			$this->db->where('content_id',$page_id);
            $return_val = $this->db->update('admin_public_content',$data);
		  }
		  
			 if($return_val)
		  {
			$array_response['status'] = 200;
            if ($page_id != 0) {
				$array_response['message'] = 'Public content has been updated successfully';
			}else{
				$array_response['message'] = 'Public content has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

            
		  echo json_encode($array_response);
		
	}
	public function page_vastu_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$page_id = 1;
		
		
		$data = array(
		       
		        'meta_image' => $this->input->post('meta_image'),
				'meta_description' => $this->input->post('meta_description'),
				'meta_keyword' => $this->input->post('meta_keyword') ,
				'meta_title' => $this->input->post('metatitle'),
				'vastu_propertydirection_warning' => $this->input->post('vastualert'),
				'button_text' => $this->input->post('buttontext'),
				'banner_image' => $this->input->post('banner_image'),
				'banner_content' => $this->input->post('banner_content'),
				'banner_title' => $this->input->post('banner_title'),
				'button_url' => $this->input->post('button_url'),
				'faq_category' => $this->input->post('faq_category'),
				'banner_id' => $this->input->post('banner_id'),
				'about_content_id' => $this->input->post('about_content_id'),
				'section_title1' => $this->input->post('section_title1'),
				'section_content1' => $this->input->post('section_content1'),
				'section_image1' => $this->input->post('section_image1'),
				'section_title2' => $this->input->post('section_title2'),
				'section_content2' => $this->input->post('section_content2'),
				'section_image2' => $this->input->post('section_image2'),
				'section_title3' => $this->input->post('section_title3'),
				'section_content3' => $this->input->post('section_content3'),
				'section_image3' => $this->input->post('section_image3'),
				'section1position' => $this->input->post('section1position'),
				'section2position' => $this->input->post('section2position'),
				'section3position' => $this->input->post('section3position'),


		);
		
		if($page_id == 0)
		  {
			$return_val = $this->db->insert('admin_section_content',$data);

		  } else
		  {
			$this->db->where('content_id',1);
            $return_val = $this->db->update('admin_section_content',$data);
		  }
		  
		  if($return_val)
		  {
			$array_response['status'] = 200;
            if ($page_id != 0) {
				$array_response['message'] = 'Content has been updated successfully';
			}else{
				$array_response['message'] = 'Content has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

            
		  echo json_encode($array_response);
		
	}
	public function page_vastuabout_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$page_id = $this->input->post('page_id');;
		
		
		$data = array(
		       
		   
				'button_text' => $this->input->post('buttontext'),
				'about_title' => $this->input->post('about_title'),
				'button_url' => $this->input->post('button_url'),
				'about_content' => $this->input->post('about_content'),
		


		);
		
		if($page_id == 0)
		  {
			$return_val = $this->db->insert('vastu_about_content',$data);

		  } else
		  {
			$this->db->where('id',$page_id);
            $return_val = $this->db->update('vastu_about_content',$data);
		  }
		  
		  if($return_val)
		  {
			$array_response['status'] = 200;
            if ($page_id != 0) {
				$array_response['message'] = 'Vastu about has been updated successfully';
			}else{
				$array_response['message'] = 'Vastu about has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

            
		  echo json_encode($array_response);
		
	}
	public function page_process_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$process_id = $this->input->post('process_id');
		
		
		$data = array(
		       
		        'process_title' => $this->input->post('process_title'),
				'process_image' => $this->input->post('process_image'),
				'process_content' => $this->input->post('process_content') ,
				
		);
		
		if($process_id == 0)
		  {
			$return_val = $this->db->insert('admin_process_point',$data);

		  } else
		  {
			$this->db->where('process_id',$process_id);
            $return_val = $this->db->update('admin_process_point',$data);
		  }
		  
		  if($return_val)
		  {
			$array_response['status'] = 200;
            if ($process_id != 0) {
				$array_response['message'] = 'Process Content has been updated successfully';
			}else{
				$array_response['message'] = 'Process Content has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

            
		  echo json_encode($array_response);
		
	}
	public function banner_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$banner_id = $this->input->post('banner_id');
		
		
		$data = array(
			'banner_content' => $this->input->post('banner_content'),
			'banner_title' => $this->input->post('banner_title'),
			'button_text' => $this->input->post('buttontext'),
			'button_url' => $this->input->post('button_url'),
			'banner_image' => $this->input->post('banner_image'),		
		);
		
		if($banner_id == 0)
		  {
			$return_val = $this->db->insert('mayas_banner',$data);

		  } else
		  {
			$this->db->where('banner_id',$banner_id);
            $return_val = $this->db->update('mayas_banner',$data);
		  }
		  
		  if($return_val)
		  {
			$array_response['status'] = 200;
            if ($banner_id != 0) {
				$array_response['message'] = 'Banner has been updated successfully';
			}else{
				$array_response['message'] = 'Banner has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

            
		  echo json_encode($array_response);
		
	}
	public function faq_update()
	{
		 
		 
		  $array_response = array('status'=>500);
		$faq_id = $this->input->post('faq_id');
		
		
		$data = array(
		       
		        'faq_category_name' => $this->input->post('faq_category_name'),
				'priority' => $this->input->post('priority'),
				'answers' => $this->input->post('answers'),
				'question' => $this->input->post('question') ,
				'faq_category_id' => $this->input->post('faq_category_id') ,
				
		);
		
		if($faq_id == 0)
		  {
			$return_val = $this->db->insert('admin_faq',$data);

		  } else
		  {
			$this->db->where('faq_id',$faq_id);
            $return_val = $this->db->update('admin_faq',$data);
		  }


		  $cache_dir = APPPATH . 'cache/'; // Adjust the path if your cache files are located elsewhere
		  $files = glob($cache_dir . 'faq_data_*'); // Get all cache files starting with faq_data_
  
		  $success = true;
		  foreach($files as $file) {
			  if(is_file($file)) {
				  if(!unlink($file)) {
					  $success = false;
					  break;
				  }
			  }
		  }
		  
		  if($return_val)
		  {
			$array_response['status'] = 200;
            if ($faq_id != 0) {
				$array_response['message'] = 'faq has been updated successfully';
			}else{
				$array_response['message'] = 'faq has been added successfully';
			}
		} else
		{
		  $array_response['status'] = 201;
		  $array_response['message'] = "Somthing went wrong ! plz try again";
		}

		

            
		  echo json_encode($array_response);
		
	}


	public function upload_product_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }

	// public function upload_product_image() {
    //     #array to hold the response values to be displayed
    //     $arr_response = array();

    //     $info = pathinfo($_FILES['myFile']['name']);
    //     $ext = $info['extension']; // get the extension of the file

    //     if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
    //             ($_FILES["myFile"]["size"] < 20485760)) {
    //         $file = $info['filename'];
    //         $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
    //         if(isset($_POST['sub_folder_name']) && $_POST['sub_folder_name']!=''){
	// 			$target = './uploads/'.$_POST['sub_folder_name'].'/' . $filename;
	// 		}else{
	// 			$target = './uploads/' . $filename;
	// 		}
			
    //         $file = $_FILES['myFile']['tmp_name'];
    //         move_uploaded_file($file, $target);

    //         $arr_response['status'] = 200;
    //         $arr_response['filename'] = $filename;
    //     } else {
    //         $arr_response['status'] = FAILED;
    //         $arr_response['message'] = "Not a valid File";
    //     }
    //     echo json_encode($arr_response);
    // }


	public function vastucontent(){
		$segment = 1;

		// if(isset($_GET['id']))
		// {
		// 	$segment = $_GET['id'];
		// }else
		// {
			$segment = 1;
		// }
		
		// $data['linked_services'] = $this->db->get('mayas_services')->result_array();   
	//    $this->db->order_by("services_position", "desc");
	//   $data['linked_services'] = $this->db->get_where('mayas_services', array('status' => 'active'))->result_array(); 
		
	   $this->db->where('content_id', $segment );
	   $data['public_page'] = $this->db->get('admin_section_content')->row_array();
		
	   // $data['public_page'] = json_decode($form_data);
		$this->load->view('configure/configure_header');
	   
		$this->load->view('admin/page/public_vastupage_add',$data);

	}
	public function vastuaboutcontent(){
		$segment = 0;

		if(isset($_GET['id']))
		{
			$segment = $_GET['id'];
		}else
		{
			$segment = 0;
		}
		
		// $data['linked_services'] = $this->db->get('mayas_services')->result_array();   
	//    $this->db->order_by("services_position", "desc");
	//   $data['linked_services'] = $this->db->get_where('mayas_services', array('status' => 'active'))->result_array(); 
		
	   $this->db->where('id', $segment );
	   $data['public_page'] = $this->db->get('vastu_about_content')->row_array();
		
	   // $data['public_page'] = json_decode($form_data);
		$this->load->view('configure/configure_header');
	   
		$this->load->view('admin/page/vastuaboutcontent',$data);

	}
	public function getdata(){
		$this->load->model('Comman_model', 'obj_comman', TRUE);


		if(isset($_GET['id']))
		{
			$segment = $_GET['id'];
		}else
		{
			$segment = 0;
		}
		
		$data['records'] = $this->obj_comman->get_by('admin_faq',array('faq_id'=>$segment),false,true);
		$data['category_list'] = $this->obj_comman->get('admin_faq_category');
		
	//    $this->db->where('content_id', $segment );
	//    $data['public_page'] = $this->db->get('admin_section_content')->row_array();
		
	   // $data['public_page'] = json_decode($form_data);
		$this->load->view('configure/configure_header');
	   
		$this->load->view('admin/page/faq_add',$data);

	}


	// public function faq(){

	



	// 	$this->load->model('Comman_model', 'obj_comman', TRUE);
	// 	if(isset($_GET['id'])){
	// 		$segment = $_GET['id'];
	// 	}else{
	// 		$segment =0;
	// 	}
		
	// 	$data['records'] = $this->obj_comman->get_by('admin_faq',array('faq_id'=>$segment),false,true);
	// 	$data['category_list'] = $this->obj_comman->get('admin_faq_category');

		

	// 		$this->load->view('configure/configure_header');

	// 		$this->load->view('admin/pages/faq_add');

		
	


	// }
	public function processcontent(){

		if(isset($_GET['id']))
		{
			$segment = $_GET['id'];
		}else
		{
			$segment = 0;
		}
		
		// $data['linked_services'] = $this->db->get('mayas_services')->result_array();   
	//    $this->db->order_by("services_position", "desc");
	//   $data['linked_services'] = $this->db->get_where('mayas_services', array('status' => 'active'))->result_array(); 
		
	   $this->db->where('process_id', $segment );
	   $data['public_page'] = $this->db->get('admin_process_point')->row_array();
	//    var_dump($public_page);

		
	   // $data['public_page'] = json_decode($form_data);
		$this->load->view('configure/configure_header');
	   
		$this->load->view('admin/page/processpoint',$data);

	}
	public function public_page_remove(){
          
		

		$array_response = array('status' => 500);
		$page_id = $this->input->post('page_id');
		
        $this->db->where('content_id',$page_id);
		$return_val = $this->db->delete('admin_public_content');
		
		if ($return_val) {
			$array_response['status'] = 200;
			$array_response['message'] = 'Content removed successfully';	
	    } else {
			$array_response['status'] = 201;
			$array_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($array_response);

	}
	
}
	
?>