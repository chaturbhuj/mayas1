<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');

/**
 * Controller class to configure RBAC system.
 *
 */
class Configure_access extends CI_Controller {

    /**
     * Default constructor.
     *
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 1)) {
            redirect(base_url() . 'login');
        }
    }

    //-------------------------------------------------------------
    /**
     * This function is used to load admin dashboard
     *
     * @access		public
     * @since		1.0.0
     */
    public function index() {

		#loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

		$slider_id = 0;
		if(isset($_GET['slider_id'])){
			$slider_id = $_GET['slider_id'];
		}else{
			$slider_id =0;
		}
		$data['slider_details'] = $this->obj_ca->get_detail_of_slider_by_id($slider_id);
	    $data['contact_form'] = $this->obj_ca->get_list_of_contact_form_detail();
		$data['consultant_form'] = $this->obj_ca->get_list_of_consultant_form_detail();
		//$data['enquiry'] = $this->obj_ca->get_list_of_enquiry();
		//$data['register'] = $this->obj_ca->get_list_of_register();

        $this->load->view('configure/configure_header');
		$this->load->view('configure/dashboard',$data);
        $this->load->view('configure/configure_footer');
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove/read contact form details from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_contact() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->remove_contact();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove/read consultant form details from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_consultant() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->remove_consultant();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
    //-------------------------------------------------------------
    /**
     * This function is used to load add new user
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_user() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_user');
        $this->load->view('configure/configure_footer');
	}

    //------------------------------------------------------------------------------
    /**
     * This function is used to check weather entered email is already exists or not.
     */
    public function check_availability() {

        #array to hold the response values to be displayed
        $arr_response = array();

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #checking if parameter already exists
        $result = $this->obj_ca->check_availability();
        if ($result) {
            echo 'true';
        } else {
            echo 'false';
        }
        // echo json_encode($arr_response);
    }
	    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new user into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_user() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */


            #loading the require model
            $this->load->model('Configure_access_model', 'obj_ca', TRUE);
            #calling the function to register a new user into the database
            $return_value = $this->obj_ca->add_new_user();
            if ($return_value) {
                $arr_response['status'] = SUCCESS; /* 200 */
                $arr_response['message'] = 'User has been added successfully';
            } else {
                $arr_response['status'] = DB_ERROR; /* 201 */
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }
    //--------------------------------------------------------------
    /**
     * This function is used to list out all user information.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_user() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['user'] = $this->obj_ca->get_list_of_user();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_user', $data);
        $this->load->view('configure/configure_footer');
    }
    //--------------------------------------------------------------------
    /**
     * Handles requests for edit user information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_user_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

            //Load Required modal
            $this->load->model('Configure_access_model', 'obj_ca', TRUE);

            $return_val = $this->obj_ca->edit_user_information();
            if ($return_val) {
                $arr_response['status'] = SUCCESS;
                $arr_response['message'] = 'User Detail has been updated successfully';
            } else {
                $arr_response['status'] = DB_ERROR;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove user information from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_user_information() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_user_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "User has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------
    /**
     * This function is used to change user password
     *
     * @access		public
     * @since		1.0.0
     */
    public function change_password() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/change_password');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------
    /**
     * This function is used to to update password.
     *
     * @access		public
     * @since		1.0.0
     */
    public function update_password() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

            //Load Required modal
            $this->load->model('Configure_access_model', 'obj_ca', TRUE);

            $return_val = $this->obj_ca->update_password();
            if ($return_val) {
                $arr_response['status'] = SUCCESS;
                $arr_response['message'] = 'Password has been updated successfully';
            } else {
                $arr_response['status'] = DB_ERROR;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------
    /**
     * This function is used to load page for add slider
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_slider() {
		 $this->load->model('Configure_access_model', 'obj_ca', TRUE);

		$slider_id = 0;
		if(isset($_GET['slider_id'])){
			$slider_id = $_GET['slider_id'];
		}else{
			$slider_id =0;
		}
		$data['slider_details'] = $this->obj_ca->get_detail_of_slider_by_id($slider_id);


        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_slider',$data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new slider into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_slider() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_slider();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Slider has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit slider in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_slider() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['user'] = $this->obj_ca->get_list_of_all_slider();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_slider', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit slider information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_slider_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_slider_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Slider has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove slider from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_slider() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_slider();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Slider has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }


    //--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }


    //--------------------------------------------------------------------------
    /**
     * This function is used to upload image for edit section.
     */
    public function edit_upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file
        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 30485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);
            //Load Required modal
            $this->load->model('configure_access_model', 'obj_ca', TRUE);
            $return_val = $this->obj_ca->edit_upload_image($filename);
            if ($return_val) {
                $arr_response['status'] = SUCCESS;
                $arr_response['filename'] = $filename;
            } else {
                $arr_response['status'] = FAILED;
                $arr_response['message'] = "Something went wrong! Please try again.";
            }
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File.";
        }
        echo json_encode($arr_response);
    }

	public function coupan() {
		 $this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$data['coupan'] = $this->obj_ca->get_list_of_all_coupan();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_coupan',$data);
        $this->load->view('configure/configure_footer');
    }

	public function remove_coupan() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_coupan();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Coupon has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	public function add_new_coupan_code() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->add_new_coupan_code();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Coupon has been added successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
   //-------------------------------------------------------------
    /**
     * This function is used to load page for add menu
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_menu() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_menu');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new Menu into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_menu() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_menu();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Menu has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit menu in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_menu() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['menu'] = $this->obj_ca->get_list_of_all_menu();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_menu', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit menu information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_menu_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_menu_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Menu has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove menu from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_menu() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_menu();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Menu has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-------------------------------------------------------------
    /**
     * This function is used to load page for add sub menu services
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_sub_menu_services() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_sub_menu_services');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new sub menu services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_sub_menu_services() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_sub_menu_services();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Sub Menu Services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu services in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_services() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['sub_menu_services'] = $this->obj_ca->get_list_of_all_sub_menu_services();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_sub_menu_services', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu services information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_services_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_sub_menu_services_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Sub Menu Services has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove sub menu services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_sub_menu_services() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_sub_menu_services();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Sub Menu Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-------------------------------------------------------------
    /**
     * This function is used to load page for add sub menu products
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_sub_menu_products() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_sub_menu_products');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new sub menu products into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_sub_menu_products() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_sub_menu_products();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Sub Menu Products has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu products in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_products() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['sub_menu_products'] = $this->obj_ca->get_list_of_all_sub_menu_products();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_sub_menu_products', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu products information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_products_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_sub_menu_products_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Sub Menu Products has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove sub menu products from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_sub_menu_products() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_sub_menu_products();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Sub Menu Products has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-------------------------------------------------------------
    /**
     * This function is used to load page for add birth stone
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_birth_stone() {

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database

		$stone_id = 0;
		if(isset($_GET['stone_id'])){
			$stone_id  = $_GET['stone_id'];

		}
       $data['birth_stone'] = $this->obj_ca->get_list_of_all_birth_stone_by_id($stone_id);
         $this->load->view('configure/configure_header');
        $this->load->view('configure/add_birth_stone',$data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new birth stone into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_birth_stone() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_birth_stone();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Birth Stone has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit birth stone in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_birth_stone() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['birth_stone'] = $this->obj_ca->get_list_of_all_birth_stone();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_birth_stone', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit birth_stone information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_birth_stone_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_birth_stone_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Birth Stone has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove birth_stone from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_birth_stone() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_birth_stone();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Birth Stone has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-------------------------------------------------------------
    /**
     * This function is used to load page for add services
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_services() {

		$service_id = 0;
		if(isset($_GET['service_id'])){
			$service_id  = $_GET['service_id'];

		}
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['services'] = $this->obj_ca->get_list_of_all_services_by_id($service_id);

		$this->load->view('configure/configure_header');
        $this->load->view('configure/add_services',$data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_services() { 
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new services into the database
        $return_value = $this->obj_ca->add_new_services();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit services in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_services() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['services'] = $this->obj_ca->get_list_of_all_services();

		$this->load->view('configure/configure_header');
        $this->load->view('configure/edit_services', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit services information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_services_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_services_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Services has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_services() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_services();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-------------------------------------------------------------
    /**
     * This function is used to load page for add aboutus
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_aboutus() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_aboutus');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new aboutus into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_aboutus() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_aboutus();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Aboutus has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit aboutus in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_aboutus() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$blog_id = 0;
		if(isset($_GET['blog_id'])){
			$blog_id  = $_GET['blog_id'];

		}

        $data['aboutus'] = $this->obj_ca->get_list_of_all_aboutus();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_aboutus', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit vastu in database.
     *
     * @access		public
     * @since		1.0.0
     */ 
    public function edit_vastu() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['vastu'] = $this->obj_ca->get_vastu_data();
        
        $data['vastu_list']= $this->obj_ca->get_vastu_list();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_vastu',$data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit aboutus information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_aboutus_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_aboutus_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Aboutus has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }


   //-------------------------------------------------------------
    /**
     * This function is used to load page for add_special_feature
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_special_feature() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_special_feature');
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new special_feature into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_special_feature() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database
        $return_value = $this->obj_ca->add_new_special_feature();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Special Feature has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit special_feature in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_special_feature() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['special_feature'] = $this->obj_ca->get_list_of_all_special_feature();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_special_feature', $data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit special_feature information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_special_feature_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_special_feature_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Special Feature has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove  special_feature from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_special_feature() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_special_feature();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Special Feature has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
//-------------------------------------------------------------
    /**
     * This function is used to load page for add products
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_product() {
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$data['all_submenu_product_name'] = $this->obj_ca->get_list_of_all_submenu_product_name();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_product',$data);
        $this->load->view('configure/configure_footer');
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding  new product  into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_product() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new product into the database
        $return_value = $this->obj_ca->add_new_product();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Product has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
    //--------------------------------------------------------------------------
    /**
     * This function is used to view edit product page.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_product() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['all_submenu_product_name'] = $this->obj_ca->get_list_of_all_submenu_product_name();
		$data['all_product'] = $this->obj_ca->get_list_of_all_product();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_product', $data);
        $this->load->view('configure/configure_footer');
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for edit product information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_product_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_product_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'product has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
		}
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove product image from the list of all image
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_product_image() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_product_image();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Image has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove product from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_product() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_product();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Product has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------
    /**
     * This function is used to load page for add blog
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_blog() {


        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database

		$blog_id = 0;
		if(isset($_GET['blog_id'])){
			$blog_id  = $_GET['blog_id'];

		}
       $data['blog'] = $this->obj_ca->get_list_of_all_blogs_by_id($blog_id);

        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_blog',$data);
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new blog into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_blog() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_blog();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'blog has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for edit blog in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_blog() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['blogs'] = $this->obj_ca->get_list_of_all_blogs();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_blog', $data);
        $this->load->view('configure/configure_footer');
    }


	//--------------------------------------------------------------------
    /**
     * Handles requests for edit blog information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_blog_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_blog_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Services has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove blog from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_blog() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_blog();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------
    /**
     * This function is used to load page for consultation massages
     *
     * @access		public
     * @since		1.0.0
     */
    public function consultation_massages() {

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new user into the database


		$data['massages'] = $this->obj_ca->get_consultation_massages();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/consultation_massages',$data);
        $this->load->view('configure/configure_footer');
    }

	public function update_consultation_massages() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->update_consultation_massages();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'update successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);

    }

	//--------------------------------------------------------------------
    /**
     * This Function show the page for Upload top banner image.
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_top_banner_image() {

        $this->load->view('configure/configure_header');
        $this->load->view('configure/upload_banner_image');
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * This Function is used to add new image for banner page.
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_banner_image() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_banner_image();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Banner Image  has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);

    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for edit banner image information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_banner_image_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_banner_image_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Banner Image Information has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove banner image information  from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_banner_image() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_banner_image();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Banner Image has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for showing the edit banner image page with data .
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_banner_image() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['banner_images'] = $this->obj_ca->get_list_of_all_banner_image();
		$this->load->view('configure/configure_header');
        $this->load->view('configure/edit_banner_image', $data);
        $this->load->view('configure/configure_footer');
    }

	//-------------------------------------------------------------
    /**
     * This function is used to load page for add lagan sign
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_lagan_sign() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_lagan_sign');
        $this->load->view('configure/configure_footer');
    }


	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new lagan into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_lagan() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_lagan();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for edit lagan in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_lagan_sign() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['lagans'] = $this->obj_ca->get_list_of_all_lagan();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_lagan', $data);
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for edit lagan information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_lagan_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_lagan_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Services has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove lagan from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_lagan() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_lagan();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }


	//-------------------------------------------------------------------------
    /**
     * Controller to handle logout request
     *
     * @version 0.0.1
     * @since 0.0.1
     */
    public function logout() {
        $this->session->sess_destroy();
        $arr_response['redirect'] = base_url() . 'login';
        echo json_encode($arr_response);
    }
	//-------------------------------------------------------------
    /**
     * This function is used to load Testimonials page
     *
     * @access		public
     * @since		1.0.0
     */
    public function testimonials() {
		#load required model

		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$data['testimonials_list'] = $this->obj_ca->get_list_of_testimonials();


		//$this->load->view('admin/testimonials',$data);

		$this->load->view('configure/configure_header');
        $this->load->view('configure/testimonials', $data);
        $this->load->view('configure/configure_footer');
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to update testimonials
     *
     * @access		public
     * @since		1.0.0
     */
    public function update_testimonials() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_testimonials();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Testimonials has been added successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------------------
	/*
	* This function is used to remove_testimonials from the list
	*
	* @access		public
	* @since		1.0.0
	*/
	public function remove_testimonials() {
		#array to hold value to be display
		$arr_response = array();
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);

		$return_val = $this->obj_ca->remove_testimonials();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['message'] = "Testimonials has been removed successfully!";
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = "Something went Wrong! Please try again";
		}
		echo json_encode($arr_response);
	}

//-------------------------------------------------------------
    /**
     * This function is used to load content page
     *
     * @access		public
     * @since		1.0.0
     */
    public function content() {
		$this->load->model('Configure_access_model', 'obj_co', TRUE);
		$data['content_list'] = $this->obj_co->get_list_of_content();

		$this->load->view('configure/configure_header');
       	$this->load->view('configure/content', $data);
        $this->load->view('configure/configure_footer');
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to update content
     *
     * @access		public
     * @since		1.0.0
     */
    public function update_content() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Configure_access_model', 'obj_co', TRUE);

        $return_val = $this->obj_co->update_content();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Content has been added successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }


	//-------------------------------------------------------------
    /**
     * This function is used to load content page
     *
     * @access		public
     * @since		1.0.0
     */
    public function order_list() {
		$this->load->model('Configure_access_model', 'obj_co', TRUE);
		$data['product_list'] = $this->obj_co->get_list_of_buy_product();
		$data['service_list'] = $this->obj_co->get_list_of_buy_service();
		$data['consultant_list'] = $this->obj_co->get_list_of_consultant();
		//var_dump($data['service_list']);
		$this->load->view('configure/configure_header');
       	$this->load->view('configure/order_list', $data);
        $this->load->view('configure/configure_footer'); 
    }




    public function remove_vasturecord() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Configure_access_model', 'obj_co', TRUE);

        $return_val = $this->obj_co->remove_vasturecord();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Consultation has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }


	//-------------------------------------------------------------
    /**
     * This function is used to load content page
     *
     * @access		public
     * @since		1.0.0
     */
    public function view_product_detail() {
		if(isset($_GET['order_payment_id'])){
				$order_payment_id = $_GET['order_payment_id'];

			}
		$this->load->model('Configure_access_model', 'obj_co', TRUE);
		$data['product_list'] = $this->obj_co->get_list_of_buy_product_by_id($order_payment_id);

		//var_dump($data['service_list']);
		$this->load->view('configure/configure_header');
       	$this->load->view('configure/view_product_detail', $data);
        $this->load->view('configure/configure_footer');
    }


	//-------------------------------------------------------------
    /**
     * This function is used to load content page
     *
     * @access		public
     * @since		1.0.0
     */
  /*  public function view_instant_detail() {
		if(isset($_GET['consultation_id'])){
		$consultation_id = $_GET['consultation_id'];

		}
		//$this->load->model('Configure_access_model', 'obj_co', TRUE);
		//$data['product_list'] = $this->obj_co->get_list_of_buy_product_by_id($order_payment_id);

		//var_dump($data['service_list']);
		$this->load->view('configure/configure_header');
       	$this->load->view('configure/view_instant_detail');
        $this->load->view('configure/configure_footer');
    }*/

	//-------------------------------------------------------------
    /**
     * This function is used to load content page
     *
     * @access		public
     * @since		1.0.0
     */
    public function view_service_detail() {

		if(isset($_GET['order_payment_id'])){
				$order_payment_id = $_GET['order_payment_id'];

			}
		$this->load->model('Configure_access_model', 'obj_co', TRUE);

		$data['service_list'] = $this->obj_co->get_list_of_buy_service_by_id($order_payment_id);
		//var_dump($data['service_list']);
		$this->load->view('configure/configure_header');
       	$this->load->view('configure/view_service_detail', $data);
        $this->load->view('configure/configure_footer');
    }

  //-------------------------------------------------------------
    /**
     * This function is used to load page for add aboutus
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_instance_consultation() {

		$this->load->model('Configure_access_model', 'obj_ca', TRUE);

		$instance_id = 0;
		if(isset($_GET['instance_id'])){
			$instance_id  = $_GET['instance_id'];

		}

		$data['instance'] = $this->obj_ca->get_list_of_all_instance_consultation_id($instance_id);
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_instance_consultation',$data);
        $this->load->view('configure/configure_footer');
    }

		//--------------------------------------------------------------------
    /**
     * Handles requests for edit blog in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_instance_consultation() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['instance_consultation'] = $this->obj_ca->get_list_of_all_instance_consultation();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/edit_instance_consultation',$data);
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new blog into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_instance_consultation() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_instance_consultation();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

		//-------------------------------------------------------------------------
    /*
     * This function is used to remove blog from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_instance_consultation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_instance_consultation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = " Removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

			//--------------------------------------------------------------------
    /**
     * Handles requests for edit blog information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_instance_information() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

        //Load Required modal
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $return_val = $this->obj_ca->edit_instance_information();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = 'Services has been updated successfully';
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }


	//-------------------------------------------------------------
    /**
     * This function is used to load page for add lagan sign
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_student() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_student');
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new lagan into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_student() {
        //array to store ajax responses
        $arr_response = array('status' => 500); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_student();
        if ($return_value) {
            $arr_response['status'] = 200; /* 200 */
            $arr_response['message'] = 'student has been added successfully';
            $arr_response['message1'] = 'student added successfully';
        } else {
            $arr_response['status'] = 201; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }


	public function add_database() {
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_database');
        $this->load->view('configure/configure_footer');
    }

	 public function add_new_database() {
        //array to store ajax responses
        $arr_response = array('status' => 500); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ca->add_new_database();
        if ($return_value) {
            $arr_response['status'] = 200; /* 200 */
            $arr_response['message'] = 'student has been added successfully';
            $arr_response['message1'] = 'student added successfully';
        } else {
            $arr_response['status'] = 201; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	 //-------------------------------------------------------------
    /**
     * This function is used to load page for add bespoke services
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_bespoke_services() {
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
	$service_id = 0;
		if(isset($_GET['service_id'])){
			$service_id  = $_GET['service_id'];

		}

        $data['services'] = $this->obj_ca->get_list_of_all_bespoke_services_by_id($service_id);
		$this->load->view('configure/configure_header');
        $this->load->view('configure/add_bespoke',$data);
        $this->load->view('configure/configure_footer');
    }
//--------------------------------------------------------------------
    /**
     * Handles requests for adding new bespoke services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_bespoke_services() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new services into the database
        $return_value = $this->obj_ca->add_new_bespoke_services();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Bespoke services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

    //-------------


	//-------------
    /**
     * Handles requests for edit services in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_bespoke_services()                                                                                                                                         {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['services'] = $this->obj_ca->get_list_of_all_bespoke_services();

		$this->load->view('configure/configure_header');
        $this->load->view('configure/edit_bespoke_services', $data);
        $this->load->view('configure/configure_footer');
    }


    //-------------------------------------------------------------------------
    /*
     * This function is used to remove services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_bespoke_services() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_bespoke_services();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Services has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

   //-----------
	 //-------------------------------------------------------------
    /**
     * This function is used to load page for add counter
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_counter() {
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);

		$data['counter'] = $this->obj_ca->get_counter_value();

		$this->load->view('configure/configure_header');
        $this->load->view('configure/count',$data);
        $this->load->view('configure/configure_footer');
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new bespoke services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_counter() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new services into the database
        $return_value = $this->obj_ca->add_new_counter();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'counter value has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	public function blog_image() {
		//Load Required modal
	 $this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$data['images_list'] = $this->obj_ca->get_list_of_images();
		$this->load->view('configure/configure_header');
			$this->load->view('configure/blog_gallery',$data);
		 $this->load->view('configure/configure_footer');
    }


	public function add_blog_image() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
 $this->load->model('Configure_access_model', 'obj_ca', TRUE);
            $return_val = $this->obj_ca->add_blog_image();
            if ($return_val) {
                $arr_response['status'] = SUCCESS;
                $arr_response['message'] = 'Image has been added successfully';
            } else {
                $arr_response['status'] = DB_ERROR;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }

	public function remove_image() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
              $this->load->model('Configure_access_model', 'obj_ca', TRUE);
            $return_val = $this->obj_ca->remove_image();
            if ($return_val) {
                $arr_response['status'] = SUCCESS;
                $arr_response['message'] = 'Image  has been removed successfully';
            } else {
                $arr_response['status'] = DB_ERROR;
                $arr_response['message'] = 'Something went Wrong! Please try again';
            }
        echo json_encode($arr_response);
    }

	//-------------------------------------------------------------
    /**
     * This function is used to load page for add Free Consultation
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_free_consultation() {

		$consultation_id = 0;
		if(isset($_GET['consultation_id'])){
			$consultation_id  = $_GET['consultation_id'];

		}
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['consultation_Data'] = $this->obj_ca->get_list_of_all_consultation_by_id($consultation_id);

		$this->load->view('configure/configure_header');
        $this->load->view('configure/add_free_consultation',$data);
        $this->load->view('configure/configure_footer');
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new Free Consultation into databases
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_consultation() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        #calling the function to register a new services into the database
        $return_value = $this->obj_ca->add_new_free_consultation();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for list Free Consultation in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function free_consultation_list() {
        #load required model
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);
        $data['consultation_Data'] = $this->obj_ca->get_list_of_all_free_consultation();

		$this->load->view('configure/configure_header');
        $this->load->view('configure/free_consultation_list', $data);
        $this->load->view('configure/configure_footer');
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to remove Free Consultation from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_free_consultation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Configure_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_free_consultation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Free Consultation has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	public function remove() {
        //array to store ajax responses
        $arr_response = array('status' => 500);

		$return_val = $this->db->delete($_POST['table_name'],array($_POST['primary_column_name']=>$_POST['id']));
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Removed successfully';
	    } else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	public function removewithcache() {
        //array to store ajax responses
        $arr_response = array('status' => 500);

		$return_val = $this->db->delete($_POST['table_name'],array($_POST['primary_column_name']=>$_POST['id']));
		if ($return_val) {
            $cache_dir = APPPATH . 'cache/'; // Adjust the path if your cache files are located elsewhere
			$files = glob($cache_dir . 'faq_data_*'); // Get all cache files starting with faq_data_
	
			$success = true;
			foreach($files as $file) {
				if(is_file($file)) {
					if(!unlink($file)) {
						$success = false;
						break;
					}
				}
			}
            
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Removed successfully';
	    } else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }

	public function category_add()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$segment =0;

		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}

		$data['cat_data'] = $this->obj_comman->get_by('mayas_category',array('cat_id'=>$segment));


		$this->load->view('configure/configure_header');

		$this->load->view('configure/category_add',$data);

		$this->load->view('configure/configure_footer');
	}

	public function update_category() {
		$this->load->model('Comman_model', 'obj_comman', TRUE);
        //array to store ajax responses
        $arr_response = array('status' => 500);
		$cat_id=$_POST['cat_id'];
		$array_data = $this->obj_comman->array_from_post(array("cat_name"));

		if($cat_id==0){
			$return_val = $this->obj_comman->insert_data('mayas_category',$array_data);
		}else {
			$return_val = $this->obj_comman->update_data('mayas_category',$array_data,array('cat_id' => $cat_id));
		}
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}
		echo json_encode($arr_response);
    }

	public function category_list()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$data['cat_data'] = $this->obj_comman->get('mayas_category');
		$data['table_name'] ='mayas_category';
		$data['primary_column_name'] ='cat_id';
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/category_list',$data);
		$this->load->view('configure/configure_footer');
	}

	public function rashi_add()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$segment =0;

		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}

		$data['rashi_data'] = $this->obj_comman->get_by('mayas_rashi',array('rashi_id'=>$segment));


		$this->load->view('configure/configure_header');

		$this->load->view('configure/rashi_add',$data);

		$this->load->view('configure/configure_footer');
	}

	public function update_rashi() {
		$this->load->model('Comman_model', 'obj_comman', TRUE);
        //array to store ajax responses
        $arr_response = array('status' => 500);
		$rashi_id=$_POST['rashi_id'];
		$array_data = $this->obj_comman->array_from_post(array("rashi_name"));

		if($rashi_id==0){
			$return_val = $this->obj_comman->insert_data('mayas_rashi',$array_data);
		}else {
			$return_val = $this->obj_comman->update_data('mayas_rashi',$array_data,array('rashi_id' => $rashi_id));
		}
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}
		echo json_encode($arr_response);
    }

	public function rashi_list()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$data['rashi_data'] = $this->obj_comman->get('mayas_rashi');
		$data['table_name'] ='mayas_rashi';
		$data['primary_column_name']='rashi_id';
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/rashi_list',$data);
		$this->load->view('configure/configure_footer');
	}

	public function disease_add()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$segment =0;
		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}

		$data['disease_data'] = $this->obj_comman->get_by('mayas_diease',array('diease_id'=>$segment));

	    $this->load->view('configure/configure_header');
		$this->load->view('configure/disease_add',$data);
		$this->load->view('configure/configure_footer');
	}

	public function update_disease() {
		$this->load->model('Comman_model', 'obj_comman', TRUE);
        //array to store ajax responses
        $arr_response = array('status' => 500);
		$diease_id=$_POST['diease_id'];
		$array_data = $this->obj_comman->array_from_post(array("diease_name"));

		if($diease_id==0){
			$return_val = $this->obj_comman->insert_data('mayas_diease',$array_data);
		}else {
			$return_val = $this->obj_comman->update_data('mayas_diease',$array_data,array('diease_id' => $diease_id));
		}
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}
		echo json_encode($arr_response);
    }

	public function disease_list()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$data['diease_data'] = $this->obj_comman->get('mayas_diease');
		$data['table_name'] ='mayas_diease';
		$data['primary_column_name']='diease_id';
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/disease_list',$data);
		$this->load->view('configure/configure_footer');
	}

	public function products_add()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$segment =0;
		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}

		$data['pro_data'] = $this->obj_comman->get_by('mayas_newproduct',array('product_id'=>$segment));
		$data['cat_data'] = $this->obj_comman->get('mayas_category');
		$data['rashi_data'] = $this->obj_comman->get('mayas_rashi');
		$data['diease_data'] = $this->obj_comman->get('mayas_diease');
		$data['freeconsult_data'] = $this->obj_comman->get('mayas_consultation_content');
		$data['paidconsult_data'] = $this->obj_comman->get_by('mayas_services',array('status'=>'active'));
		//$data['paidconsult_data'] = $this->db->get_where('mayas_services',array('status'=>'active'))->result();

	    $this->load->view('configure/configure_header');
		$this->load->view('configure/products_add',$data);
		$this->load->view('configure/configure_footer');
	}

	public function update_products() {
		$this->load->model('Comman_model', 'obj_comman', TRUE);
        //array to store ajax responses
        $arr_response = array('status' => 500);
		$product_id=$_POST['product_id'];
		$array_data = $this->obj_comman->array_from_post(array("product_name","string_url","product_actual_prize","product_discounted_prize","product_category","product_rashi","product_disease","product_free_consultation","product_paid_consultation","product_short_description","product_long_description","status","is_new","product_meta_title","product_meta_description","product_meta_keyword","product_meta_tags","product_feature","product_height","product_shape","product_color","product_clarity","product_grade","product_weight","product_treatment","product_origin","product_images"));

		$product_rashi=$_POST['product_rashi'];
		$array_data['product_rashi']=json_encode($product_rashi);
		$product_disease=$_POST['product_disease'];
		$array_data['product_disease']=json_encode($product_disease);
		$product_free_consultation=$_POST['product_free_consultation'];
		$array_data['product_free_consultation']=json_encode($product_free_consultation);
		$product_paid_consultation=$_POST['product_paid_consultation'];
		$array_data['product_paid_consultation']=json_encode($product_paid_consultation);
		//echo "ff";
		if($product_id==0){
			$return_val = $this->obj_comman->insert_data('mayas_newproduct',$array_data);
			$product_id=$return_val;
		}else {
			$return_val = $this->obj_comman->update_data('mayas_newproduct',$array_data,array('product_id' => $product_id));
		}

		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}


		if($product_rashi){
			foreach($product_rashi as $value){
				$data =array(
					'rashi_id'=>$value,
					'product_id'=>$product_id,
				);
				$return_val = $this->obj_comman->insert_data('mayas_product_rashi',$data);
			}
		}

		if($product_disease){
			foreach($product_disease as $value){
				$data =array(
					'diease_id'=>$value,
					'product_id'=>$product_id,
				);
				$return_val = $this->obj_comman->insert_data('mayas_product_diease',$data);
			}
		}

		if($product_free_consultation){
			foreach($product_free_consultation as $value){
				$data =array(
					'consultation_id'=>$value,
					'product_id'=>$product_id,
				);
				$return_val = $this->obj_comman->insert_data('mayas_product_free_consultation',$data);
			}
		}

		if($product_paid_consultation){
			foreach($product_paid_consultation as $value){
				$data =array(
					'service_id'=>$value,
					'product_id'=>$product_id,
				);
				$return_val = $this->obj_comman->insert_data('mayas_product_paid_consultation',$data);
			}
		}

		echo json_encode($arr_response);
    }
	public function add_cronjob() {
		$this->load->model('Comman_model', 'obj_comman', TRUE);
        //array to store ajax responses
        $arr_response = array('status' => 500);
		$cronjob_id	=$_POST['cronjob_id'];
		//var_dump($cronjob_id);
		$array_data = array(
						'consultant_type' =>$_POST['consultant_type'],
						'c_start_id' =>$_POST['c_start_id'],
						'c_end_id' =>$_POST['c_end_id'],
						'title' =>$_POST['title'],
						'Subject' =>$_POST['Subject'],
						'bocy' =>$_POST['bocy'],
						'status' =>$_POST['status'],
						'count_start' =>$_POST['count_start'],
						'count_end' =>$_POST['count_end'],
						'created_date'=>date('Y-m-d'),
						'created_time'=>date('H:i:s'),
						);

		if($cronjob_id	==0){
			$return_val = $this->obj_comman->insert_data('cronjob',$array_data);
			$cronjob_id	=$return_val;
		}else {
			$return_val = $this->obj_comman->update_data('cronjob',$array_data,array('cronjob_id' => $cronjob_id));
		}

		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}

		echo json_encode($arr_response);
    }

	public function products_list()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$data['product_data'] = $this->obj_comman->get('mayas_newproduct');
		$data['table_name'] ='mayas_newproduct';
		$data['primary_column_name']='product_id';
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/products_list',$data);
		$this->load->view('configure/configure_footer');
	}

	public function cronjob_list(){
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$data['cronjob_data'] = $this->obj_comman->get('cronjob');
		$data['table_name'] ='cronjob';
		$data['primary_column_name']='cronjob_id';
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/cronjob_list',$data);
		$this->load->view('configure/configure_footer');
	}

	public function cronjob_setting()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);

		$segment =0;
		if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}
		//var_dump($segment);

		$cron_data=$this->db->get_where('cronjob',array('cronjob_id'=>$segment))->result();
		$data['cron_data']=$cron_data;
	    $this->load->view('configure/configure_header');
		$this->load->view('configure/cronjob_setting',$data);
		$this->load->view('configure/configure_footer');
	}





	public function signup_for_customer_post() {
		// echo "fsdf";
		// var_dump($this->post('first_name'));
		/*if(!$this->post('first_name')){
			$status = ['status' => 400 ,  'message' => 'First Name is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else if(!$this->post('email_id')){
			$status = ['status' => 400 ,  'message' => 'Email is required.'];
			$this->response($status, REST_Controller::HTTP_CREATED);
		}else{
			#generate random number
			$verify_code = rand(1111,99999999999);
			$verify_code = sha1('verify' . (md5('verify' . $verify_code)));
			$data = array(
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'country' => $this->post('country'),
				'state' => $this->post('state'),
				'city' => $this->post('city'),
				'email_id' => $this->post('email_id'),
				'phone_numbers' => $this->post('phone_numbers'),
				'address_lines' => $this->post('address_lines'),

				'verify_code' => $verify_code,
				'customer_img' => 'default_profile.jpg',
				'customer_status' => 'active',

				'created_date' => date('Y-m-d'),
				'created_time' => date('H:i:s'),

			);
			$result = $this->db->insert('customer_account', $data);
			$results = $this->db->insert('customer_address', $data);
			$insert_id = $this->db->insert_id();
			$datas = array(
				'customer_id' => $insert_id,
				'email_id' =>$this->post('email_id'),
				'loggedIN' => 11,
				'first_name' => $this->post('first_name'),
				'last_name' => $this->post('last_name'),
				'phone_numbers' => $this->post('phone_numbers'),
				'customer_img' =>'default_profile.jpg',
			);

				$this->session->set_userdata($datas);

			$verification_link= base_url().'verify/vc?e='.$insert_id.'&v='.$verify_code;
			$content ='Dear '.$this->post('full_name').' ,<br /><br /><p>Welcome and thank you for registering at <a href="'.PROJECT_URL.'">'.PROJECT_NAME.' </a></p><p>Your account has now been created and you can log in by Clicking the button bellow to confirm your email address</p><div style="text-align: center;margin: 20px;padding: 10px;"><a href="'.$verification_link.'" style="padding: 7px; background-color: #3b5998; margin: 10px;color: #fff;font-size: 14px;">CONFIRM</a></div>';
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => 'Welcome to '.PROJECT_NAME,
				'to' => $this->post('customer_email'),
				'to_name' => $this->post('first_name'),
				'from' => PROJECT_EMAIL,
				'msg' => $content,
			);
			$result = $this->obj_email->send_mail_in_sendinblue($data11);

			if($insert_id) {
				$status = ['status' => 200 ,'message' => 'Your account has been successfully created. Please continue to login.',];
				$this->set_response($status, REST_Controller::HTTP_OK);
			} else {
				$status = ['status' => 201 ,  'message' => 'Something went Wrong! Please try again'];
				$this->response($status, REST_Controller::HTTP_CREATED);
			}
		}*/
    }
	public function order_lists(){

        $this->load->view('configure/configure_header');
		$this->load->view('configure/order_lists');
        $this->load->view('configure/configure_footer');
    }

	public function update_status_of_order() {
        #array to hold value to be display
        $arr_response = array();
        $order_id=$_POST['order_id'];
        $status=$_POST['status'];
		$data = array(
		  'detail_status' => $status,
		);
		$datas = array(
		  'order_status' => $status,
		);
        $return_val=$this->db->update('product_order_detail', $data,array('order_id' => $order_id));
        $return_val=$this->db->update('product_order', $datas,array('order_id' => $order_id));




        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Order Status has been updated successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	public function update_status_of_failedorder() {
        #array to hold value to be display
        $arr_response = array();
        $order_id=$_POST['order_id'];
        $status=$_POST['status'];
		$data = array(
		  'check_payment' => $status,
		);

        $return_val=$this->db->update('product_order_detail', $data,array('order_id' => $order_id));
        $return_val=$this->db->update('product_order', $data,array('order_id' => $order_id));


		//echo "gfg";
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = "Order Status has been updated successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	public function get_order_list() {
		$this->load->model('Comman_model','obj_comman', TRUE);
        $start_date=$_POST['start_date'];
        $end_date=$_POST['end_date'];



        if($start_date!='' && $end_date!=''){
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];
			//var_dump( $start_date);
			//var_dump( $end_date);
            //$this->db->where('created_date' 'BETWEEN', $start_date AND $end_date);

			$this->db->where('created_date BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
			//var_dump($this->db->last_query());
        }
        if(isset($_POST['country_name'])){
            $country_name = $_POST['country_name'];
            $this->db->where('country_name',$country_name);
        }
        if(isset($_POST['state_name'])){
            $state_name = $_POST['state_name'];
            $this->db->where('state_name',$state_name);
        }
        if(isset($_POST['city_name'])){
            $city_name = $_POST['city_name'];
            $this->db->where('city_name',$city_name);
        }
        if($_POST['is_completed_order']=='yes'){
			//var_dump($_POST['is_completed_order']);
            $this->db->where('order_status','delivered');

        }
        if($_POST['is_inprocess_order']=='yes'){
            $this->db->where('order_status','Pending');
        }
		if($_POST['failed']=='yes'){
            $this->db->where('check_payment','yes');
        }

        $this->db->order_by('order_id','desc');
        $return_val = $this->db->get('product_order')->result();
		//var_dump( $return_val);
		//var_dump($this->db->last_query());
        //var_dump($this->db->last_query());
        foreach ($return_val as $key => $value) {
			$vendor_id=$value->vendor_id;
			$order_id=$value->order_id;
			//$product_id=$value->customer_id;
			//var_dump($vendor_id);
			if($vendor_id){
				//var_dump('yes');
				$sql1 ="SELECT * FROM `customer_account` WHERE `customer_id` =$vendor_id";
				$vendor_data = $this->obj_comman->get_query($sql1);
				//var_dump($this->db->last_query());
				//var_dump($vendor_data);
				if(isset($vendor_data[0])){
					$return_val[$key]->vendor_data=$vendor_data[0];
					//var_dump($vendor_data[0]);
				}else{
					$return_val[$key]->vendor_data='';
				}

			}
			if($order_id){
				//var_dump('yes');
				$sql2 ="SELECT * FROM `product_order_detail` WHERE `order_id` =$order_id";
				$product_data = $this->obj_comman->get_query($sql2);
				//var_dump($this->db->last_query());
				//var_dump($product_data[0]);
				if(isset($product_data[0])){
					$return_val[$key]->product_data=$product_data[0];
					//var_dump($vendor_data[0]);
				}else{
					$return_val[$key]->product_data='';
				}

			}
        }
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['data'] = $return_val;
            $arr_response['message'] = "successfully!";
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	public function order_details(){

		$this->load->model('Comman_model','obj_comman', TRUE);

		/*reading the coupon id by segment*/
        if(isset($_GET['id'])){
            $segment = $_GET['id'];
        }else{
            $segment =0;
        }

		$data['records'] = $this->obj_comman->get_by('product_order_detail',array('order_id'=>$segment));
		$data['records_order'] = $this->obj_comman->get_by('product_order',array('order_id'=>$segment));
        //var_dump($data);
		$this->load->view('configure/configure_header');
		$this->load->view('configure/order_details',$data);
		$this->load->view('configure/configure_footer');

    }
    public function page_data() {
        $id = 0;
		if(isset($_GET['id'])){
			$id  = $_GET['id'];

		}
        $this->db->where('id',$id);
       	$data['page_data']= $this->db->get('page_data')->result_array();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/page_data',$data);
        $this->load->view('configure/configure_footer');
    }
	public function add_page_data() {

		$idd = $_POST['id'];
		$array_data = array(
						'page' =>$_POST['page'],
						'data' =>$_POST['data'],
						'header_image' =>$_POST['header_image'],
						'video_link' =>$_POST['video_link'],

						);

		if($idd == 0){
			$return_val = $this->db->insert('page_data',$array_data);
		}else {
            $this->db->where('id', $idd);
			$return_val = $this->db->update('page_data',$array_data);
		}
		if ($return_val) {
			$arr_response['status'] = 200;
			$arr_response['message'] = 'Successfully submitted';
		} else {
			$arr_response['status'] = 201;
			$arr_response['message'] = 'Something went wrong! Please try again';
		}

		echo json_encode($arr_response);
    }

	  public function add_vastu_data() {
    		$idd = $_POST['id'];
        //var_dump($idd);
    		$array_data = array(
            'home_page_heading' =>  $_POST['home_page_heading'],
            'youtube_link'  =>  $_POST['youtube_link'],
            'url' =>  $_POST['url'],
            'page_title'  =>  $_POST['page_title'],
            'meta_tag' =>  $_POST['meta_tag'],
            'meta_descp'  =>  $_POST['meta_descp'],
            'home_page_content'=>$_POST['home_page_content'],
    	  	);

    		if($idd == 0){
    			$return_val = $this->db->insert('vastu_data',$array_data);
    		}else {
                $this->db->where('id', $idd);
    			$return_val = $this->db->update('vastu_data',$array_data);
    		}
    		if ($return_val) {
    			$arr_response['status'] = 200;
    			$arr_response['message'] = 'Successfully submitted';
    		} else {
    			$arr_response['status'] = 201;
    			$arr_response['message'] = 'Something went wrong! Please try again';
    		}

    		echo json_encode($arr_response);
    }
	public function add_new_vastu_data() {
    
        $idd = $_POST['id'];
            //var_dump($idd);
        $array_data = array(
        'page_heading' =>  $_POST['page_heading'],
        'short_description'  =>  $_POST['short_description'],
        'url' =>  $_POST['url'],
        'faq_category' => $_POST['faq_category'],
        'banner_id' => $_POST['banner_id'],
        'about_content_id' => $_POST['about_content_id'],
        'page_title'  =>  $_POST['page_title'],
        'meta_tag' =>  $_POST['meta_tag'],
        'meta_descp'  =>  $_POST['meta_descp'],
        'page_content'=>$_POST['page_content'],
        'detail_image' => $_POST['detail_image'],
        'detail_image_position' =>$_POST['detail_image_position'],

        // 'owner_name'=>  $_POST['owner_name'],
        'owner_name_label'=>  $_POST['owner_name_label'],
        'owner_name_placeholder'=>  $_POST['owner_name_placeholder'],
        // 'email_id'=>  $_POST['email_id'],
        'email_id_label'=>  $_POST['email_id_label'],
        'email_id_placeholder'=>  $_POST['email_id_placeholder'],
        'owner_contact_placeholder'=>  $_POST['owner_contact_placeholder'],
        'owner_contact_label'=>  $_POST['owner_contact_label'],
        // 'owner_contact'=>  $_POST['owner_contact'],
        // 'property_address'=>  $_POST['property_address'],
        'property_address_label'=>  $_POST['property_address_label'],
        'property_address_placeholder'=>  $_POST['property_address_placeholder'],
        // 'property_layout_maps'=>  $_POST['property_layout_maps'],
        'property_layout_maps_label'=>  $_POST['property_layout_maps_label'],
        // 'property_direction_orientation'=>  $_POST['property_direction_orientation'],
        'property_direction_orientation_label'=>  $_POST['property_direction_orientation_label'],
        'property_direction_orientation_placeholder'=>  $_POST['property_direction_orientation_placeholder'],
        // 'owner_place_birth'=>  $_POST['owner_place_birth'],
        'owner_place_birth_label'=>  $_POST['owner_place_birth_label'],
        // 'owner_date_time_birth'=>  $_POST['owner_date_time_birth'],
        'owner_date_time_birth_label'=>  $_POST['owner_date_time_birth_label'],
        'property_direction_orientation_message'=>  $_POST['property_direction_orientation_message'],
        'compass_image'=>  $_POST['compass_image'],
        'property_directioncompass_msg'=>  $_POST['property_directioncompass_msg'],
        'property_directioncompass_link'=>  $_POST['property_directioncompass_link'],
        'is_home'=>  $_POST['is_home'],
        'home_msg'=>  $_POST['home_msg'],
      //  'property_directioncompass_defaultvideo'=>  $_POST['property_directioncompass_defaultvideo'],
        // 'additional_json'=> $addjson,
        );
            //var_dump($array_data);
        if($idd == 0){
            $return_val = $this->db->insert('mayas_new_vastu',$array_data);
        }else {
            $this->db->where('vastu_id', $idd);
            $return_val = $this->db->update('mayas_new_vastu',$array_data);
        }
        if ($return_val) {
            $arr_response['status'] = 200;
            $arr_response['message'] = 'Successfully submitted';
        } else {
            $arr_response['status'] = 201;
            $arr_response['message'] = 'Something went wrong! Please try again';
        }

        echo json_encode($arr_response);
    }

    public function page_data_list(){


       	$data['page_data']= $this->db->get('page_data')->result_array();

        $this->load->view('configure/configure_header');
        $this->load->view('configure/page_data_list',$data);
		$this->load->view('configure/configure_footer');
	}

    public function add_new_vastu_type(){
        $data['vastu']= array();
        if(isset($_GET['vastu_id'])){
            $vastu_id = $_GET['vastu_id'];
        }else{
            $vastu_id =0;
        }
        $get_vastu_data = $this->db->get_where('mayas_new_vastu',array('vastu_id'=>$vastu_id))->result_array();
        //var_dump($get_vastu_data);
        $data['vastu'] =$get_vastu_data;
        $this->load->view('configure/configure_header');
        $this->load->view('configure/add_vastu',$data);
		$this->load->view('configure/configure_footer');
    }
}

 //end of class Configure_access
// end of file configure_access.php
