<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');


/**
 * Controller class to configure RBAC system.
 * 
 */
class Shop extends CI_Controller {

    /**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        //Load Required modal
		//$this->load->model('shop_access_model', 'obj_shop', TRUE);
	}

    //-------------------------------------------------------------
    /**
     * This function is used to load user deshboard
     * 
     * @access		public
     * @since		1.0.0
     */
    public function index(){
		$meta_id=13;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('shopcart');
		$this->load->view('components/footer',$data);
	}
	
	
	//--------------------------------------------------------------
    /**
     * This function is used to to validate coupon code
     */
    public function coupon_validate() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);

		
		$return_val = $this->obj_shop->coupon_validate();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['coupon_discount'] = $return_val;
			$arr_response['message'] = 'Discount has been updated successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Coupon Code is not valid or it may expire.';
		}
        echo json_encode($arr_response);
    }
	
	//--------------------------------------------------------------
    /**
     * This function is used to to update shopcart detial in database.
     * 
     */
    public function update_shopcart() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
		
		$return_val = $this->obj_shop->update_shopcart();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['order_id'] = $return_val;
			$arr_response['message'] = 'Shop cart has been updated successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	
}

  