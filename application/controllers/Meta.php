<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meta extends CI_Controller {
	/**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
       
	}
	
	
	//-------------------------------------------------------------
    /**
     * This function is used to load aquatic_management page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function meta_data() {
		
		
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$meta_id = 0;
		if(isset($_GET['meta_id'])){
			$meta_id  = $_GET['meta_id'];
			
		}
		
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);			
		
		$this->load->view('configure/configure_header');
		$this->load->view('configure/meta_data',$data);
		$this->load->view('configure/configure_footer');
		
		
    }
	//-------------------------------------------------------------
    /**
     * This function is used to load aquatic_management page
     * 
     * @access		public
     * @since		1.0.0
     */
    public function meta_view() {
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		
		
        $data['meta_data'] = $this->obj_ma->get_all_meta_data();	
		
		$this->load->view('configure/configure_header');
		$this->load->view('configure/meta_view',$data);
		$this->load->view('configure/configure_footer');

		
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free Consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_meta_data() {
        //array to store ajax responses
  

        #loading the require model
        $this->load->model('Meta_data_model', 'obj_ma', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ma->update_meta_data();
        if ($return_value) {
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' Your data has been added successfully..';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free Consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_meta_data() {
        //array to store ajax responses
  

        #loading the require model
        $this->load->model('Meta_data_model', 'obj_ma', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_ma->remove_meta_data();
        if ($return_value) {
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' data has been remove successfully.';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    
	
	
  
		
		
		 
	}
	public function sendemail (){
	 
		$content = '<html><body><h1>Thisa is a transactional email {{params.parameter}}</h1></body></html>';

		$this->load->model('Email_model', 'obj_email', TRUE);
		$data11 = array(
			'subject' => 'Welcome to ',
			'to' => 'schatur92@gmail.com',
			'to_name' => 'Jane Doea',
			'from' => 'info@crowdbuyit.com',
			'from_name' => 'HAMLET',
			'msg' => $content,
		);
		$result1 = $this->obj_email->send_mail_in_sendinblue($data11);
		var_dump($result1);
	}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */