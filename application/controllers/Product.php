<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	
	
	public function detail(){
		$customer_id=$this->session->userdata('customer_id');
		$fav="";
		//var_dump($customer_id);
		//var_dump($this->session->userdata);
		
		$this->load->model('Comman_model', 'obj_comman', TRUE);
	
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		$segment ="";

		/*if(isset($_GET['id'])){
			$segment = $_GET['id'];
		} else {
			$segment =0;
		}*/
		
		//var_dump($this->uri->segment(3));
			
		if($this->uri->segment(3)){
			
			$segment=$this->uri->segment(3);
			//var_dump($segment);
		}else{
			$segment="";
		}
		
		//$data['product_data'] = $this->obj_comman->get('mayas_newproduct');
		$data['newproduct_data'] = $this->obj_comman->get_by('mayas_newproduct',array('string_url'=>$segment));
		if($data['newproduct_data']){
			foreach($data['newproduct_data'] as $ro){
				//var_dump($ro);
				$product_id=$ro['product_id'];
			}
		}
		
		$data_result=$this->db->get_where('favourite_product',array('customer_id'=>$customer_id,'product_id'=>$product_id))->num_rows();
		//var_dump($data_result);
		if($data_result >0){
			$fav="yes";
		}else{
			$fav="no";
		}
		$data['favs']=$fav;
		
		$this->load->view('components/header');
		$this->load->view('productdetails',$data);
		
		$data['services'] = $this->obj_comman->get('mayas_services');
		//var_dump($data['services']);
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		
		$this->load->view('components/footer',$data);
	}
	
	public function past_order_List(){
		
		$this->load->model('Comman_model', 'obj_comman', TRUE);	
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		//var_dump($data['services']);
        //var_dump($data);
		$this->load->view('components/header');
		$this->load->view('past_order_List');
		$this->load->view('components/footer',$data);
	}
	public function Order_Detail_List(){
		
		$this->load->model('Comman_model', 'obj_comman', TRUE);	
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');

        //var_dump($data);
		$this->load->view('components/header');
		$this->load->view('Order_Detail_List');
		$this->load->view('components/footer',$data);
	}
	
	public function invoice(){
		
		$this->load->model('Comman_model', 'obj_comman', TRUE);	
		$this->load->model('Account_access_model', 'obj_aa', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		$this->load->view('components/header');
		
		if(isset($_GET['order_id'])){
			$id=$_GET['order_id'];
		}else{
			$id="";
		}
		
		$this->db->where('order_id' , $id);		
        $db_result = $this->db->get('product_order')->result();
		$data["order_detail"]=$db_result;
		//var_dump($this->db->last_query());
		//var_dump($db_result[0]->order_id);
		$order_id=$db_result[0]->order_id;
		
		$this->db->where('order_id' , $order_id);
        $data["item_detail"] = $this->db->get('product_order_detail')->result();
		//print_r($data);	
		$this->load->view('invoice_detail',$data);
		$this->load->view('components/footer',$data);		
	}
	
	public function print_invoice() {
		
		$id = $this->uri->segment(3);
		//var_dump($id );
		$order_id = explode("-",$id);
		//var_dump($order_id);
		$this->db->where('order_id' , $order_id[1]);
        $db_result = $this->db->get('product_order')->result();
		$data["order_detail"]=$db_result;
		$order_id=$db_result[0]->order_id;
		
		$this->db->where('order_id' , $order_id);
        $data["item_detail"] = $this->db->get('product_order_detail')->result();
		$this->load->view('invoice_print',$data);
		
	}
	
}
