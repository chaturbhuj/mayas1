<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once (APPPATH .'libraries/razorpay-php/Razorpay.php');
use Razorpay\Api\Api as RazorpayApi;

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 */
	public function index()	{
		
		$meta_id=6;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		// $data['testimonials_list'] = $this->obj_wa->get_list_of_testimonials();
		$data['slider_list'] = $this->obj_wa->get_list_of_slider();
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['consultation_content'] = $this->obj_wa->get_list_of_all_consultation_content();
		$data['product_list'] = $this->obj_wa->get_detail_of_products();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$data['blog_list'] = $this->obj_wa->get_list_of_main_blogs_for_home_page();
		$data['blogs'] = $this->obj_wa->get_list_of_main_blogs();
		$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		$this->load->model('Configure_access_model', 'obj_co', TRUE);
		$data['counter'] = $this->obj_co->get_counter_value();
		$data['content_list'] = $this->obj_co->get_list_of_content();

		$this->db->or_where('id', 1);
		$this->db->or_where('id', 2);
		$this->db->or_where('id', 3);
		$this->db->or_where('id', 4);
		$this->db->or_where('id', 5);
		$data['page_data']= $this->db->get('page_data')->result_array();
		//var_dump($page_data);


		$this->load->view('components/header',$data2);
		$this->load->view('newhome',$data);
		$this->load->view('components/footer',$data);
	}

	public function free_consultations()	{
		
		$meta_id=6;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE); 
		$data['consultation_content'] = $this->obj_wa->get_list_of_all_consultation_content();

		$this->load->view('components/header',$data2);
		$this->load->view('free_consultations',$data);
		$this->load->view('components/footer',$data);
	}

	public function newhome()	{
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['consultation_content'] = $this->obj_wa->get_list_of_all_consultation_content();
		$data['slider_list'] = $this->obj_wa->get_list_of_slider();
		$data['testimonials_list'] = $this->obj_wa->get_list_of_testimonials();
		//var_dump($data['consultation_content']);
		$this->load->view('newhome',$data);
	}
	
	
	
	/**
	 * aboutus Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function aboutus()	{
		$meta_id=1;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
	
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['aboutus_list'] = $this->obj_wa->get_list_of_all_aboutus();
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		//var_dump($data['aboutus_list']);
		$this->load->view('components/header',$data2);
		$this->load->view('aboutus',$data);
		$this->load->view('components/footer',$data);
	}
	
	public function logout() {
        $this->session->sess_destroy();
		redirect(base_url(), 'refresh');
    }
	
	public function termsconditions(){
		$meta_id=6;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['content_data'] = $this->obj_wa->get_public_content_of_id(1);
			$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
	
		$this->load->view('components/header',$data2);
		$this->load->view('terms_conditions',$data);
		$this->load->view('components/footer',$data);
	}
	
	public function privacypolicy()	{
		$meta_id=6;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['content_data'] = $this->obj_wa->get_public_content_of_id(2);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('terms_conditions',$data);
		$this->load->view('components/footer',$data);
	}
	
	
	public function refundcancelation()	{
		$meta_id=6;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['content_data'] = $this->obj_wa->get_public_content_of_id(3);
			$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
	
		$this->load->view('components/header',$data2);
		$this->load->view('terms_conditions',$data);
		$this->load->view('components/footer',$data);
	}
	/**
	 * services Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function services()	{
		$meta_id=2;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		$this->load->view('components/header',$data2);
		$this->load->view('services',$data);
		$this->load->view('components/footer',$data);
	}
	
	public function newproducts()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		
		$data['newproduct_data'] = $this->obj_comman->get('mayas_newproduct');	
		    	
		$this->load->view('components/header');
		$this->load->view('newproducts',$data);
		
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		
		$this->load->view('components/footer',$data);
		
	}

	public function vastuall(){
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['vastuservices'] = $this->obj_wa->get_list_of_all_vastuservices();
		$this->load->view('components/header');
		$this->load->view('vastuservice',$data);
		$this->load->view('components/footer');
	}


	



	public function get_product_data(){
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$newproduct_data = $this->obj_comman->get('mayas_newproduct');
		$content = '';
           
		if ($newproduct_data == 0) {
			$content .= 'No record found into database';
			} else {
			$i = 1;
				foreach ($newproduct_data as $value) {
				$img =(explode(',',$value['product_images']));
				//var_dump($value);  
			

            $content.= '<div class="col-md-3">';
             $content.= '<div class="thumbnail-variant-1 thumbnail green-top-border green-shadow">';
			$content.= '<a rel="nofollow" href="'.base_url().'welcome/product_details/?id='.$value['product_id'].'" class="gemstone_hover">';
					
			$content.= '<div style="padding: 20px; height: 120px;background-color: #009688 !important;">';
			$content.= '<h3 class="text-center" style="color:#fff;">'.$value['product_name'].'</h3>';
			$content.= '</div>';
			$content.= '<div style="position: absolute; top: 65px; left: 32%; margin-left: -45px;">';
			$content.= '<img src="'.base_url().'uploads/'.$img[0].'" alt="Mayas Astrology" style="width: 200px; height: 200px; border: 3px solid #fff; border-radius: 50%;">';
				$content.= '</div>';
				$content.='<div class="caption" style="padding: 140px 30px 20px;">';
							
						$content.= '<button class="btn btn-link"> Click Here <i class="fa fa-chevron-right" aria-hidden="true"></i> </button>';
						$content.= '</div>';
					$content.= '</a>';
					
					
                  
                $content.= '</div>';
               
            $content.= '</div>';
          
			 
					$i++;
					
					
				}
				
			}	
       
		$arr_response['data'] = $content; /* 200 */
        $arr_response['status'] = SUCCESS; /* 200 */
        $arr_response['message'] = 'successfully';

		echo json_encode($arr_response);
	}
	

			
	/**
	 * services Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function site_map()	{
		$data2['title']='Site Map';
		$data2['meta_description']='Mayas astrology! Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		
		$this->load->view('components/header',$data2);
		$this->load->view('site_map');
		$this->load->view('components/footer');
	}
			
	/**
	 * Blogs
	 */
	
	public function blog()	{		
		$meta_id=3;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['blogs'] = $this->obj_wa->get_list_of_main_blogs();
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		$this->load->view('components/header',$data2);
		$this->load->view('blogs',$data);
		$this->load->view('components/footer',$data);
	}

	/**
	 * Blogs-Single
	 */	
	public function blogfull()	{
		$meta_id=12;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['blogs'] = $this->obj_wa->get_list_of_main_blogs();
		$data['blogfull'] = $this->obj_wa->get_list_of_main_blogs_by_id($this->uri->segment(3));
		$data['blogid'] = $this->uri->segment(3);
		$blog_id = $this->uri->segment(3);
		$data['blog_comment'] = $this->obj_wa->get_list_of_blog_comment_by_id($blog_id);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		$this->load->view('components/header',$data2);
		$this->load->view('blogfull',$data);
		$this->load->view('components/footer',$data);
	}
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free Consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function scroll_blog_full_data() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->scroll_blog_full_data();
        if ($return_value) {
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Information has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function applied_coupan_check() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->applied_coupan_check();
		// var_dump($return_value);
		$total_cost = (Float)$_POST['total_cost'];
		$discount = 0;
		$amount = 0;
		if($return_value){
			$discount = (float)$return_value[0]->discount;
			$amount = (int)($total_cost * $discount / 100) ;
		}
        if ($return_value) {
            $arr_response['discount'] = $discount; /* 200 */
            $arr_response['amount'] = $amount; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Coupon has been applied successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Coupon does not match !';
        }
        echo json_encode($arr_response);
    }	
	/**
	 * contactus Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	 
	public function contactus()	{
		$meta_id=5;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		
		
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		$this->load->view('components/header',$data2);
		$this->load->view('contactus');
		$this->load->view('components/footer',$data);
	}
	
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function consultation_step1(){
		
		$meta_id=7;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
        $data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		$data['instance_consultation'] = $this->obj_wa->get_list_of_all_instance_consultation();
		// var_dump($data['instance_consultation']);
		$this->load->view('components/header',$data2);
		$this->load->view('consultation_step1',$data);
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function instant_consultation(){
		$meta_id=9;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		
		
		
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		 
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		$this->load->view('components/header',$data2);
		$this->load->view('instant_consultation',$data);
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function donate(){
		
		$meta_id=10;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		//$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('donate');
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function bespoke(){
		$data2['title']='Astrology';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Discuss your Problem in Detail for Under ₹ 300. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		//$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		
		$this->load->view('components/header',$data2);
		$this->load->view('bespoke');
		$this->load->view('components/footer');
	}
	
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function free_consultation_thankyou(){

		$data2['title']='Thank You';
		$data2['meta_description']='Thank You! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		
		$this->load->view('components/header',$data2);
		$this->load->view('free_consultation_thankyou');
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function free_consultation_thankyou2(){	
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$data2['title']='Thank You';
		$data2['meta_description']='Thank You! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone.';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$result = $this->obj_wa->get_consultation_massages();
		$date=date('Y-m-d');
		if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			$data3['massage']=$result[0]['custom_message1'];
		}else{
			$data3['massage']=$result[0]['default_massage'];
		}
		
		$this->load->view('components/header',$data2);
		$this->load->view('free_consultation_thankyou2',$data3);
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function free_vastu_consultation_thankyou(){	
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$data2['title']='Thank You';
		$data2['meta_description']='Thank You! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone.';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$result = $this->obj_wa->get_consultation_massages();
		$date=date('Y-m-d');
		if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			$data3['massage']=$result[0]['custom_message'];
		}else{
			$data3['massage']=$result[0]['default_massage'];
		}
		
		$this->load->view('components/header',$data2);
		$this->load->view('vastu_consultation_thankyou',$data3);
		$this->load->view('components/footer',$data);
	}
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function consultation(){	
		$meta_id=8;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['title']=$data['meta_data'][0]['title'];
		$data2['meta_description']=$data['meta_data'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'];
	
	
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['popup_ads'] = $this->obj_wa->get_list_of_popup_ads();
		
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('consultation',$data);
		$this->load->view('components/footer',$data);
	}
	
	
	/**
	 * consultation Page for this controller.
     * 
     * @access		public
     * @since		1.0.0
	 */
	public function free_consultation(){
		$url=$this->uri->segment(2);
		#loading the require model		
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data1['consultation'] = $this->obj_wa->get_list_of_all_consultation_content_id($url);
		
		$meta_id=11;
		$this->load->model('Meta_data_model', 'obj_ma', TRUE);
		$data['meta_data'] = $this->obj_ma->all_meta_data_by_id($meta_id);
		$data2['meta_description']=$data1['consultation'][0]['dec_meta'];
		$data2['meta_keyword']=$data['meta_data'][0]['keyword_meta'].' , '.$data1['consultation'][0]['keyword_meta'];
		$data2['title']= $data1['consultation'][0]['title'];
		
		if(!$data1['consultation']){
			redirect(base_url());
		}
		//var_dump($data1);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('free_consultation',$data1);
		$this->load->view('components/footer',$data);
	}
	

	public function PayUMoney_form(){
		$data2['title']='Patment';
		$data2['meta_description']='Pay Money Form! Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('payment/PayUMoney_form');
		$this->load->view('components/footer',$data);
	}
	
	
	public function success(){
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$data2['title']='Success';
		$data2['meta_description']='Success! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		#loading the require model
       
		$this->load->view('components/header',$data2);
		$this->load->view('payment/success');
		$this->load->view('components/footer',$data);
	}
	
	public function failure(){
		$data2['title']='Failure';
		$data2['meta_description']='Failure! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('payment/failure');
		$this->load->view('components/footer',$data);
	}
	
	public function error(){
		$data2['title']='Error';
		$data2['meta_description']='Error! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone, Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('error');
		$this->load->view('components/footer',$data);
	}
	
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free Consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_consultant() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_new_free_consultant();
        if ($return_value) {
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'Information has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding contact form details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_contactus() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_new_contactus();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }

	/**
	 * products Page for this controller.
	 */
	public function products(){	
		$data2['title']='Products';
		$data2['meta_description']='Mayas Products! You can Get Products acording to advice Mayas Solutions. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$this->load->model('welcome_model', 'obj_wa', TRUE);
		
		$cat_id=$this->uri->segment(2);
		
	     
		$data['product'] = '';
		$data['product_cat'] = '';
		
		
		
		if($cat_id){
			//echo '2';
			$data['product_cat'] = $this->obj_wa->get_list_of_products_by_cat_id($cat_id);
			// var_dump($data);
		}else{
			$data['product'] = $this->obj_wa->get_list_of_all_products();
			// var_dump($data);
		}
		
		
		// var_dump($data);
		$this->load->view('components/header',$data2);
		$this->load->view('products',$data);
		$this->load->view('components/footer');
	}

	/**
	 * servicedetails
     * 
	 */

	public function productsdetail()	{
		$data2['title']='Product Details';
		$data2['meta_description']='description about product! ,Consult the Best Astrologer Online & Phone, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$product_id=$this->uri->segment(3);
		$this->load->model('Welcome_model', 'obj_wa', TRUE);

			$data['product_detail'] = $this->obj_wa->get_detail_of_products_by_product_id($product_id);
	
		$this->load->view('components/header',$data2);
		$this->load->view('productsdetail',$data);
		$this->load->view('components/footer');
	}
	/**
	 * servicedetails
     * 
	 */
	public function pshopcart()	{
		$data2['title']='Shop Cart';
		$data2['meta_description']='Shoping! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$product_id=$this->uri->segment(3);
		$this->load->model('Welcome_model', 'obj_wa', TRUE);

		$this->load->view('components/header',$data2);
		$this->load->view('pshopcart');
		$this->load->view('components/footer');
	}
	/**
	 * servicedetails
     * 
	 */

	public function free_gemstone_recommendation()	{
		
		$data2['title']='Astrology and Horoscope, Free Vedic Astrology Readings Online, Consult an Astrologer Instantly';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('free_gemstone_recommendation');
		$this->load->view('components/footer',$data);
	}
	
	public function ccavRequestHandler(){
		
		$data2['title']='Request';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('ccav/ccavRequestHandler');
		$this->load->view('components/footer',$data);
	}
	
	public function finalcheckoutproduct()	{
		$data2['title']='Product Check Out';
		$data2['meta_description']='Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone, Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$this->load->view('components/header',$data2);
		$this->load->view('finalcheckoutproduct');
		$this->load->view('components/footer');
	}
	public function finalcheckoutservice()	{
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$data2['title']='Service check Out';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$this->load->view('components/header',$data2);
		$this->load->view('finalcheckoutservice');
		$this->load->view('components/footer',$data);
	}
	
	public function convertdataproductservce()	{
		
		//$merchant_data = 'tid=14948371280&merchant_id=115539&order_id=151654788&amount=1.00&currency=INR&redirect_url=http://mayasastrology.com/welcome/ccavResponseHandler&cancel_url=http://mayasastrology.com/welcome/ccavResponseHandler&language=EN&billing_name=Charli&billing_address=Room no 1101, near Railway station Ambad&billing_city=Indore&billing_state=MP&billing_zip=425001&billing_country=India&billing_tel=9876543210&billing_email=test@test.com&delivery_name=Chaplin&delivery_address=room no.701 near bus stand&delivery_city=Hyderabad&delivery_state=Andhra&delivery_zip=425001&delivery_country=India&delivery_tel=9876543210&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&merchant_param5=additional Info.&promo_code=&customer_identifier=&';
		$merchant_data = 'tid='.$_POST['merchant_data'][0]['TID'].'&merchant_id='.MERCHANT_ID.'&order_id='.$_POST['merchant_data'][0]['order_id'].'&amount='.$_POST['merchant_data'][0]['amount'].'&currency='.$_POST['merchant_data'][0]['currency'].'&redirect_url='.$_POST['merchant_data'][0]['redirect_url'].'&cancel_url='.$_POST['merchant_data'][0]['cancel_url'].'&language='.$_POST['merchant_data'][0]['language'].'&billing_name='.$_POST['merchant_data'][0]['billing_name'].'&billing_address='.$_POST['merchant_data'][0]['billing_address'].'&billing_city='.$_POST['merchant_data'][0]['billing_city'].'&billing_state='.$_POST['merchant_data'][0]['billing_state'].'&billing_zip='.$_POST['merchant_data'][0]['billing_zip'].'&billing_country='.$_POST['merchant_data'][0]['billing_country'].'&billing_tel='.$_POST['merchant_data'][0]['billing_tel'].'&billing_email='.$_POST['merchant_data'][0]['billing_email'].'&delivery_name='.$_POST['merchant_data'][0]['delivery_name'].'&delivery_address='.$_POST['merchant_data'][0]['delivery_address'].'&delivery_city='.$_POST['merchant_data'][0]['delivery_city'].'&delivery_state='.$_POST['merchant_data'][0]['delivery_state'].'&delivery_zip='.$_POST['merchant_data'][0]['delivery_zip'].'&delivery_country='.$_POST['merchant_data'][0]['delivery_country'].'&delivery_tel='.$_POST['merchant_data'][0]['delivery_tel'].'&merchant_param1='.$_POST['merchant_data'][0]['merchant_param1'].'&merchant_param2='.$_POST['merchant_data'][0]['merchant_param2'].'&merchant_param3='.$_POST['merchant_data'][0]['merchant_param3'].'&merchant_param4='.$_POST['merchant_data'][0]['merchant_param4'].'&merchant_param5='.$_POST['merchant_data'][0]['merchant_param5'].'&promo_code='.$_POST['merchant_data'][0]['promo_code'].'&customer_identifier='.$_POST['merchant_data'][0]['customer_identifier'].'&';
	
		$working_key = '2ECF82AC441781DC7104B0A4B14890C8';
		
		$res = $this->encrypt($merchant_data,$working_key );
		
		$arr_response['status'] = SUCCESS; /* 200 */
		$arr_response['data'] = $res;
        
        echo json_encode($arr_response);
		  
	}
	
	public function convertdataproductservce_vastu()	{
		
		//$merchant_data = 'tid=14948371280&merchant_id=115539&order_id=151654788&amount=1.00&currency=INR&redirect_url=http://mayasastrology.com/welcome/ccavResponseHandler&cancel_url=http://mayasastrology.com/welcome/ccavResponseHandler&language=EN&billing_name=Charli&billing_address=Room no 1101, near Railway station Ambad&billing_city=Indore&billing_state=MP&billing_zip=425001&billing_country=India&billing_tel=9876543210&billing_email=test@test.com&delivery_name=Chaplin&delivery_address=room no.701 near bus stand&delivery_city=Hyderabad&delivery_state=Andhra&delivery_zip=425001&delivery_country=India&delivery_tel=9876543210&merchant_param1=additional Info.&merchant_param2=additional Info.&merchant_param3=additional Info.&merchant_param4=additional Info.&merchant_param5=additional Info.&promo_code=&customer_identifier=&';
		$working_key = '2ECF82AC441781DC7104B0A4B14890C8';
		
		$res = $this->encrypt($working_key );
		
		$arr_response['status'] = SUCCESS; /* 200 */
		$arr_response['data'] = $res;
        
        echo json_encode($arr_response);
		  
	}
	
	
	public function encrypt($plainText,$key){
		
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
	  	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
	  	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
		{
		      $encryptedText = mcrypt_generic($openMode, $plainPad);
	      	      mcrypt_generic_deinit($openMode);
		      			
		} 
		return bin2hex($encryptedText);
	}

	public function decrypt($encryptedText,$key)
	{
		$secretKey = $this->hextobin(md5($key));
		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
		$encryptedText= $this->hextobin($encryptedText);
	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
		mcrypt_generic_init($openMode, $secretKey, $initVector);
		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
		$decryptedText = rtrim($decryptedText, "\0");
	 	mcrypt_generic_deinit($openMode);
		return $decryptedText;
		
	}
	//*********** Padding Function *********************

	public function pkcs5_pad ($plainText, $blockSize)
	{
	    $pad = $blockSize - (strlen($plainText) % $blockSize);
	    return $plainText . str_repeat(chr($pad), $pad);
	}

	//********** Hexadecimal to Binary function for php 4.0 version ********

	public function hextobin($hexString) { 
        	$length = strlen($hexString); 
        	$binString="";   
        	$count=0; 
        	while($count<$length) 
        	{       
        	    $subString =substr($hexString,$count,2);           
        	    $packedString = pack("H*",$subString); 
        	    if ($count==0)
		    {
				$binString=$packedString;
		    } 
        	    
		    else 
		    {
				$binString.=$packedString;
		    } 
        	    
		    $count+=2; 
        	} 
  	        return $binString; 
   } 

	public function ccavResponseHandlerservice()	{
		
			  
			$workingKey='2ECF82AC441781DC7104B0A4B14890C8';	
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString= $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		//var_dump($rcvdString);
		$data['decryptValues'] = explode('&', $rcvdString);	  
		//$data['decryptValues'] =  "order_id=1503065102926" [1]=> string(24) "tracking_id=106268015785" [2]=> string(24) "bank_ref_no=172309232300" [3]=> string(20) "order_status=Success" [4]=> string(16) "failure_message=" [5]=> string(24) "payment_mode=Net Banking" [6]=> string(19) "card_name=HDFC Bank" [7]=> string(12) "status_code=" [8]=> string(19) "status_message=null" [9]=> string(12) "currency=INR" [10]=> string(10) "amount=1.0" [11]=> string(18) "billing_name=sumit" [12]=> string(21) "billing_address=sdsdf" [13]=> string(18) "billing_city=sdsdf" [14]=> string(19) "billing_state=sdsdf" [15]=> string(17) "billing_zip=11111" [16]=> string(21) "billing_country=India" [17]=> string(22) "billing_tel=7737362081" [18]=> string(41) "billing_email=erchatur.prajapat@gmail.com" [19]=> string(19) "delivery_name=sumit" [20]=> string(22) "delivery_address=sdsdf" [21]=> string(19) "delivery_city=sdsdf" [22]=> string(20) "delivery_state=sdsdf" [23]=> string(18) "delivery_zip=11111" [24]=> string(22) "delivery_country=India" [25]=> string(23) "delivery_tel=7737362081" [26]=> string(21) "merchant_param1=sdsdf" [27]=> string(21) "merchant_param2=sdsdf" [28]=> string(21) "merchant_param3=sdsdf" [29]=> string(21) "merchant_param4=sdsdf" [30]=> string(21) "merchant_param5=sdsdf" [31]=> string(7) "vault=N" [32]=> string(15) "offer_type=null" [33]=> string(15) "offer_code=null" [34]=> string(18) "discount_value=0.0" [35]=> string(14) "mer_amount=1.0" [36]=> string(10) "eci_value=" [37]=> string(7) "retry=N" [38]=> string(15) "response_code=0" [39]=> string(14) "billing_notes=" [40]=> string(30) "trans_date=18/08/2017 19:37:33" [41]=> string(19) "bin_country= " )";
		$data['full_response_data'] = json_encode($data['decryptValues']);
		//print_r($data['decryptValues']);
		$data2['title']='Responce';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		
		$data['order_id']=(explode('=',$data['decryptValues'][0])[1]);
		$data['tracking_id']=(explode('=',$data['decryptValues'][1])[1]);
		$data['order_status']=(explode('=',$data['decryptValues'][3])[1]);
		
		$data['currency']=(explode('=',$data['decryptValues'][9])[1]);
		$data['billing_name']=(explode('=',$data['decryptValues'][11])[1]);
		$data['billing_email']=(explode('=',$data['decryptValues'][18])[1]);
		$data['amount']=(explode('=',$data['decryptValues'][10])[1]);
		$data['contectno']=(explode('=',$data['decryptValues'][17])[1]);
		
		//$data['order_status'] =  'asd';
	//$data['tracking_id'] =  'asdas';
		//$data['order_id'] =  '655656';
	//	$data['billing_address'] =  'dfg';
	/*	$data['billing_city'] =  'dfg';
		$data['billing_state'] =  'dfg';
		$data['billing_zip'] =  '655656';
		$data['billing_country'] =  'dfg';
		/*for($i = 0; $i < $dataSize; $i++) 	{
			$information = explode('=',$decryptValues[$i]);
			if($i==3)	$data['order_status'] = $information[1];
			if($i==1)	$data['tracking_id'] = $information[1];
		}*/
		if($data['order_status'] == 'Success'){
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
		
			$data['services'] = $this->obj_wa->get_list_of_all_services();
			$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('ccavResponseHandlerservice',$data);
			$this->load->view('components/footer',$data);
		}else{
			
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
		
			$data['services'] = $this->obj_wa->get_list_of_all_services();
			$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('cancel_payment',$data);
			$this->load->view('components/footer',$data);
		}
		
	}
	
	public function razorResponseHandlerservice()	{
			$data2['title']='Responce';
			$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
			$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
			
			//get invoice_data
			$segment=$this->uri->segment(3);
			$id = explode("-",$segment);
			$data['order_detail']=$this->db->get_where('mayas_order_payment', array('order_payment_id' => $id[0]))->row();
			$data['sercice_detail'] = $this->db->get_where('mayas_payment_service', array('FK_order_payment_id' => $id[0]))->row();
			
			$result = $this->obj_wa->get_consultation_massages();
			//var_dump($result);
			//print_r($data['order_detail']);
			$date=date('Y-m-d');
			if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
				$message = $result[0]['custom_message_paid'];
				$message = str_replace('%[NAME]%', $data['order_detail']->name, $message);
				$message = str_replace('%[item]%', $data['order_detail']->itemname, $message);
				$message = str_replace('%[cost]%', $data['order_detail']->grandtotal, $message);
				$message = str_replace('%[code]%', ($data['order_detail']->order_payment_id.' - '.(substr($data['order_detail']->order_id , -4))), $message);	
				$message = str_replace('%[USER]%', $data['order_detail']->email, $message);	
				$data['massage']=$message;
			}else{
				$data['massage']=$result[0]['paid_massage'];
			}
			 
			
			$data['services'] = $this->obj_wa->get_list_of_all_services();
			$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('razorResponseHandlerservice',$data);
			$this->load->view('components/footer',$data);
		
	}
	public function razorResponseHandlerservicevastu()	{
			$data2['title']='Responce';
			$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
			$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
			
			//get invoice_data
			$segment=$this->uri->segment(3);
			$id = explode("-",$segment);
			// $id = $segment;
			$data['order_detail']=$this->db->get_where('mayas_vastu_consultation', array('vastu_id' => $id[0]))->row();
			
			
			$result = $this->obj_wa->get_consultation_massages();
			// var_dump($result);
			//print_r($data['order_detail']);
			// $date=date('Y-m-d');
			// if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			// 	$message = $result[0]['custom_message_paid'];
			// 	$message = str_replace('%[NAME]%', $data['order_detail']->inputName, $message);
			// 	$message = str_replace('%[item]%', $data['order_detail']->consultantType, $message);
			// 	$message = str_replace('%[cost]%', $data['order_detail']->amount, $message);
			// 	$message = str_replace('%[code]%', ($data['order_detail']->vastu_id.' - '.(substr($data['order_detail']->order_payment_id , -4))), $message);	
			// 	$message = str_replace('%[USER]%', $data['order_detail']->inputEmail, $message);	
			// 	$data['massage']=$message;
			// }else{
				$data['massage']=$result[0]['vastu_default_msg'];
			// }
			// var_dump($data);
			 
			
			$data['services'] = $this->obj_wa->get_list_of_all_vastuservices();
			// $data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('razorResponseHandlerservicevastu',$data);
			$this->load->view('components/footer',$data);
		
	}


// public function razorResponseHandlerservicevastu(){

// }	
	public function razorResponseHandlInstant_consultation()	{
			$data2['title']='Responce';
			$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
			$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
			
			$data['services'] = $this->obj_wa->get_list_of_all_services();
			$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('razorResponseHandlInstant_consultation',$data);
			$this->load->view('components/footer',$data);
		
	}
	
	public function razorResponseHandlerbesoke()	{
			$data2['title']='Responce';
			$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
			$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
			$this->load->model('Welcome_model', 'obj_wa', TRUE);
			
			$data['services'] = $this->obj_wa->get_list_of_all_services();
			$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			$this->load->view('components/header',$data2);
			$this->load->view('razorResponseHandlerbesoke',$data);
			$this->load->view('components/footer',$data);
		
	}
	
	public function service_order_buy_data() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->service_order_buy_data();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
            $arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function service_order_buy_request_data() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->service_order_buy_request_data();
        if ($return_value) {
            $arr_response['status'] = 200; /* 200 */
            $arr_response['message'] = 'services has been added successfully';
            $arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	
	public function ccavResponseHandlerproduct()	{
		
			  
		$workingKey='2ECF82AC441781DC7104B0A4B14890C8';		//Working Key should be provided here.
		//$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		//$rcvdString= $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		
		//$data['decryptValues'] = explode('&', $rcvdString);
		//$data['decryptValues'] =  array ( [0] => order_id=121654788 [1] => tracking_id=106226121993 [2] => bank_ref_no=IGABHUTKK2 [3] => order_status=Success [4] => failure_message= [5] => payment_mode=Net Banking [6] => card_name=State Bank of India [7] => status_code=null [8] => status_message=Completed Successfully [9] => currency=INR [10] => amount=1.0 [11] => billing_name=Charli [12] => billing_address=Room no 1101, near Railway station Ambad [13] => billing_city=Indore [14] => billing_state=MP [15] => billing_zip=425001 [16] => billing_country=India [17] => billing_tel=9876543210 [18] => billing_email=test@test.com [19] => delivery_name=Charli [20] => delivery_address=Room no 1101, near Railway station Ambad [21] => delivery_city=Indore [22] => delivery_state=MP [23] => delivery_zip=425001 [24] => delivery_country=India [25] => delivery_tel=9876543210 [26] => merchant_param1=additional Info [27] => merchant_param2=additional Info [28] => merchant_param3=additional Info [29] => merchant_param4=additional Info [30] => merchant_param5=additional Info [31] => vault=N [32] => offer_type=null [33] => offer_code=null [34] => discount_value=0.0 [35] => mer_amount=1.0 [36] => eci_value=null [37] => retry=N [38] => response_code=0 [39] => billing_notes= [40] => trans_date=15/05/2017 18:41:28 [41] => bin_country= );
		$data['order_status'] =  'asd';
		$data['tracking_id'] =  'asdas';
		$data['order_id'] =  '655656';
		
		$data['billing_city'] =  'dfg';
		$data['billing_state'] =  'dfg';
		$data['billing_zip'] =  '655656';
		$data['billing_country'] =  'dfg';
		/*for($i = 0; $i < $dataSize; $i++) 	{
			$information = explode('=',$decryptValues[$i]);
			if($i==3)	$data['order_status'] = $information[1];
			if($i==1)	$data['tracking_id'] = $information[1];
		}*/
		$data2['title']='Responce';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('ccavResponseHandlerproduct',$data);
		$this->load->view('components/footer',$data);
	}
	
	 public function product_order_buy_data() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->product_order_buy_data();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'product has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function ccavResponseHandlerdonate()	{
			$workingKey='2ECF82AC441781DC7104B0A4B14890C8';	
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString= $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		
		$data['decryptValues'] = explode('&', $rcvdString);	  
		//$data['decryptValues'] =  "order_id=1503065102926" [1]=> string(24) "tracking_id=106268015785" [2]=> string(24) "bank_ref_no=172309232300" [3]=> string(20) "order_status=Success" [4]=> string(16) "failure_message=" [5]=> string(24) "payment_mode=Net Banking" [6]=> string(19) "card_name=HDFC Bank" [7]=> string(12) "status_code=" [8]=> string(19) "status_message=null" [9]=> string(12) "currency=INR" [10]=> string(10) "amount=1.0" [11]=> string(18) "billing_name=sumit" [12]=> string(21) "billing_address=sdsdf" [13]=> string(18) "billing_city=sdsdf" [14]=> string(19) "billing_state=sdsdf" [15]=> string(17) "billing_zip=11111" [16]=> string(21) "billing_country=India" [17]=> string(22) "billing_tel=7737362081" [18]=> string(41) "billing_email=erchatur.prajapat@gmail.com" [19]=> string(19) "delivery_name=sumit" [20]=> string(22) "delivery_address=sdsdf" [21]=> string(19) "delivery_city=sdsdf" [22]=> string(20) "delivery_state=sdsdf" [23]=> string(18) "delivery_zip=11111" [24]=> string(22) "delivery_country=India" [25]=> string(23) "delivery_tel=7737362081" [26]=> string(21) "merchant_param1=sdsdf" [27]=> string(21) "merchant_param2=sdsdf" [28]=> string(21) "merchant_param3=sdsdf" [29]=> string(21) "merchant_param4=sdsdf" [30]=> string(21) "merchant_param5=sdsdf" [31]=> string(7) "vault=N" [32]=> string(15) "offer_type=null" [33]=> string(15) "offer_code=null" [34]=> string(18) "discount_value=0.0" [35]=> string(14) "mer_amount=1.0" [36]=> string(10) "eci_value=" [37]=> string(7) "retry=N" [38]=> string(15) "response_code=0" [39]=> string(14) "billing_notes=" [40]=> string(30) "trans_date=18/08/2017 19:37:33" [41]=> string(19) "bin_country= " )";
		$data['full_response_data'] = json_encode($data['decryptValues']);
		//print_r($data['decryptValues']);
		$data['order_id']=(explode('=',$data['decryptValues'][0])[1]);
		$data['tracking_id']=(explode('=',$data['decryptValues'][1])[1]);
		
		$data['currency']=(explode('=',$data['decryptValues'][9])[1]);
		$data['billing_name']=(explode('=',$data['decryptValues'][11])[1]);
		$data['billing_email']=(explode('=',$data['decryptValues'][18])[1]);
		$data['amount']=(explode('=',$data['decryptValues'][10])[1]);
		$data['contectno']=(explode('=',$data['decryptValues'][17])[1]);
		
		$data2['title']='Responce';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('ccavResponseHandlerdonate',$data);
		$this->load->view('components/footer',$data);
	}
	
	public function ccavResponseHandlerbesoke()	{
			$workingKey='2ECF82AC441781DC7104B0A4B14890C8';	
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		$rcvdString= $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		
		$data['decryptValues'] = explode('&', $rcvdString);	  
		//$data['decryptValues'] =  "order_id=1503065102926" [1]=> string(24) "tracking_id=106268015785" [2]=> string(24) "bank_ref_no=172309232300" [3]=> string(20) "order_status=Success" [4]=> string(16) "failure_message=" [5]=> string(24) "payment_mode=Net Banking" [6]=> string(19) "card_name=HDFC Bank" [7]=> string(12) "status_code=" [8]=> string(19) "status_message=null" [9]=> string(12) "currency=INR" [10]=> string(10) "amount=1.0" [11]=> string(18) "billing_name=sumit" [12]=> string(21) "billing_address=sdsdf" [13]=> string(18) "billing_city=sdsdf" [14]=> string(19) "billing_state=sdsdf" [15]=> string(17) "billing_zip=11111" [16]=> string(21) "billing_country=India" [17]=> string(22) "billing_tel=7737362081" [18]=> string(41) "billing_email=erchatur.prajapat@gmail.com" [19]=> string(19) "delivery_name=sumit" [20]=> string(22) "delivery_address=sdsdf" [21]=> string(19) "delivery_city=sdsdf" [22]=> string(20) "delivery_state=sdsdf" [23]=> string(18) "delivery_zip=11111" [24]=> string(22) "delivery_country=India" [25]=> string(23) "delivery_tel=7737362081" [26]=> string(21) "merchant_param1=sdsdf" [27]=> string(21) "merchant_param2=sdsdf" [28]=> string(21) "merchant_param3=sdsdf" [29]=> string(21) "merchant_param4=sdsdf" [30]=> string(21) "merchant_param5=sdsdf" [31]=> string(7) "vault=N" [32]=> string(15) "offer_type=null" [33]=> string(15) "offer_code=null" [34]=> string(18) "discount_value=0.0" [35]=> string(14) "mer_amount=1.0" [36]=> string(10) "eci_value=" [37]=> string(7) "retry=N" [38]=> string(15) "response_code=0" [39]=> string(14) "billing_notes=" [40]=> string(30) "trans_date=18/08/2017 19:37:33" [41]=> string(19) "bin_country= " )";
		$data['full_response_data'] = json_encode($data['decryptValues']);
		//print_r($data['decryptValues']);
		$data['order_id']=(explode('=',$data['decryptValues'][0])[1]);
		$data['tracking_id']=(explode('=',$data['decryptValues'][1])[1]);
		
		$data['currency']=(explode('=',$data['decryptValues'][9])[1]);
		$data['billing_name']=(explode('=',$data['decryptValues'][11])[1]);
		$data['billing_email']=(explode('=',$data['decryptValues'][18])[1]);
		$data['amount']=(explode('=',$data['decryptValues'][10])[1]);
		$data['contectno']=(explode('=',$data['decryptValues'][17])[1]);
		
		$data2['title']='Donate Responce';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('ccavResponseHandlerbesoke',$data);
		$this->load->view('components/footer',$data);
	}
	
	
	public function ccavResponseHandlInstant_consultation()	{
			$workingKey='2ECF82AC441781DC7104B0A4B14890C8';	
		$encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
		
		//print_r($encResponse);
		//print_r('ok </br></br>');
		$rcvdString= $this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
		
		$data['decryptValues'] = explode('&', $rcvdString);	
			//print_r($data['decryptValues']);		
		//$data['decryptValues'] =  "order_id=1503065102926" [1]=> string(24) "tracking_id=106268015785" [2]=> string(24) "bank_ref_no=172309232300" [3]=> string(20) "order_status=Success" [4]=> string(16) "failure_message=" [5]=> string(24) "payment_mode=Net Banking" [6]=> string(19) "card_name=HDFC Bank" [7]=> string(12) "status_code=" [8]=> string(19) "status_message=null" [9]=> string(12) "currency=INR" [10]=> string(10) "amount=1.0" [11]=> string(18) "billing_name=sumit" [12]=> string(21) "billing_address=sdsdf" [13]=> string(18) "billing_city=sdsdf" [14]=> string(19) "billing_state=sdsdf" [15]=> string(17) "billing_zip=11111" [16]=> string(21) "billing_country=India" [17]=> string(22) "billing_tel=7737362081" [18]=> string(41) "billing_email=erchatur.prajapat@gmail.com" [19]=> string(19) "delivery_name=sumit" [20]=> string(22) "delivery_address=sdsdf" [21]=> string(19) "delivery_city=sdsdf" [22]=> string(20) "delivery_state=sdsdf" [23]=> string(18) "delivery_zip=11111" [24]=> string(22) "delivery_country=India" [25]=> string(23) "delivery_tel=7737362081" [26]=> string(21) "merchant_param1=sdsdf" [27]=> string(21) "merchant_param2=sdsdf" [28]=> string(21) "merchant_param3=sdsdf" [29]=> string(21) "merchant_param4=sdsdf" [30]=> string(21) "merchant_param5=sdsdf" [31]=> string(7) "vault=N" [32]=> string(15) "offer_type=null" [33]=> string(15) "offer_code=null" [34]=> string(18) "discount_value=0.0" [35]=> string(14) "mer_amount=1.0" [36]=> string(10) "eci_value=" [37]=> string(7) "retry=N" [38]=> string(15) "response_code=0" [39]=> string(14) "billing_notes=" [40]=> string(30) "trans_date=18/08/2017 19:37:33" [41]=> string(19) "bin_country= " )";
		$data['full_response_data'] = json_encode($data['decryptValues']);
		
		//print_r(explode('=',$data['decryptValues'][9])[1]);
		$data['order_id']=(explode('=',$data['decryptValues'][0])[1]);
		$data['tracking_id']=(explode('=',$data['decryptValues'][1])[1]);
		
		$data['currency']=(explode('=',$data['decryptValues'][9])[1]);
		$data['billing_name']=(explode('=',$data['decryptValues'][11])[1]);
		$data['billing_email']=(explode('=',$data['decryptValues'][18])[1]);
		$data['amount']=(explode('=',$data['decryptValues'][10])[1]);
		$data['contectno']=(explode('=',$data['decryptValues'][17])[1]);
		
		$data2['title']='Responce Consultation';
		$data2['meta_description']='Mayas astrology! Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone. ';
		$data2['meta_keyword']='Mayas astrology, free astrologer online, online free  gemstone recommendation, astrology consultant free by phone, Vastu, Horary, Indian Vedic astrology,astrology advice free';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('ccavResponseHandlInstant_consultation',$data);
		$this->load->view('components/footer',$data);
	}
	
	
	public function reset_password()	{
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
			
		$this->load->view('components/header');
		$this->load->view('customer_reset_password');
		$this->load->view('components/footer',$data);
	}
	
	public function add_donate_data_paypal() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_donate_data_paypal();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'product has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	 public function add_donate_data() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_donate_data();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'product has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	 public function service_order_buy_data_instantcon() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->service_order_buy_data_instantcon();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = 'product has been added successfully';
			$arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	 public function get_services_name_id() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */

        #loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->get_services_name_id();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['data'] = $return_value; /* 200 */
            $arr_response['message'] = 'product has been added successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function add_new_free_consultant_free() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_new_free_consultant_free();
        if ($return_value) {
			$return_values = $this->obj_wa->signup_for_customer($return_value);
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['data'] = $return_value; /* 200 */
			
            $arr_response['message'] = 'Your request has been saved successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	public function add_new_vastu_consultant_free() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT); /* 500 */
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->add_new_vastu_consultant();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['data'] = $return_value; /* 200 */
			
            $arr_response['message'] = 'Your request has been saved successfully';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        echo json_encode($arr_response);
    }
	
	public function bespokeservicedetails()	{
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$this->load->model('Configure_access_model', 'obj_ca', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services_details'] = $this->obj_wa->get_list_of_bespoke_services_by_id($this->uri->segment(3));
		
		$data2['title']=$data['services_details'][0]['services_title'];
		$data2['meta_description']=$data['services_details'][0]['services_dec_meta'];
		$data2['meta_keyword']=$data['services_details'][0]['services_keyword_meta'];
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('donate',$data);
		$this->load->view('components/footer',$data);
	}
	
	public function leave_blog_comment() {
        //array to store ajax responses
        $arr_response = array('status' => ERR_DEFAULT);
        $this->load->model('Welcome_model','obj_wel',true);
		$return_val = $this->obj_wel->leave_blog_comment();
		if ($return_val) {
			$arr_response['status'] = SUCCESS;
			$arr_response['message'] = ' you have posted comment successfully';
		} else {
			$arr_response['status'] = DB_ERROR;
			$arr_response['message'] = 'Something went Wrong! Please try again';
		}
        echo json_encode($arr_response);
    }
	
	public function paytest() {
        $seceret_key = $_POST['seceret_key'];
        $payment_id = $_POST['payment_id'];
        $razor_key = $_POST['razor_key'];
        $amount100 = (int)$_POST['amount']; 
		//echo base_url();
			
		if(base_url()=="http://localhost/mayas1/" || base_url()=="http://epic.lennoxsoft.in/mayas/"){
			//echo 'if';
			
		}else{
			//echo 'else';
			//$api = new RazorpayApi($razor_key, $seceret_key);
			//$payment = $api->payment->fetch($payment_id);
			//$payment->capture(array('amount' => $amount100));
			$url = 'https://api.razorpay.com/v1/payments/' . $payment_id . '/capture';
			$auth = base64_encode($razor_key . ':' . $seceret_key);
			$data = json_encode([
				'amount' => $amount100,
				'currency' => 'INR'
			]);
			$ch = curl_init($url);

			// Set cURL options
			curl_setopt($ch, CURLOPT_USERPWD, $razor_key . ':' . $seceret_key); // Set authentication
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'Content-Type: application/json'
			]);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // Return the response instead of outputting

			// Execute cURL request and get response
			$response = curl_exec($ch);
			//print_r($response);

		}
	
		//echo 'gyjgyj' ;
		$data = array(
			'order_id' => $_POST['payment_id'],  
		);
			
		$result = $this->db->update('mayas_order_payment', $data, array('order_payment_id'=>$_POST['order_payment_id']));
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->service_order_buy_data($_POST['order_payment_id']);
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' you have posted comment successfully';
            $arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
		echo json_encode($arr_response);
    }
	public function vastu_paytest() {
        $seceret_key = $_POST['seceret_key'];
        $payment_id = $_POST['payment_id'];
        $razor_key = $_POST['razor_key'];
        $amount100 = (int)$_POST['amount'];
		//echo base_url();
			
		if(base_url()=="http://localhost/mayas1/" || base_url()=="http://epic.lennoxsoft.in/mayas/"){
			//echo 'if';
			
		}else{
			//echo 'else';
			$api = new RazorpayApi($razor_key, $seceret_key);
			$payment = $api->payment->fetch($payment_id);
			$payment->capture(array('amount' => $amount100));
		}
	
		//echo 'gyjgyj' ;
		$data = array(
			'order_payment_id' => $_POST['payment_id'], 
			'amount' => $amount100,
		);
			
		$result = $this->db->update('mayas_vastu_consultation', $data, array('vastu_id'=>$_POST['order_payment_id']));
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->vastuservice_order_buy_data();
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' you have posted comment successfully';
            $arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
		echo json_encode($arr_response);
    }
	public function vastu_paytestvastu() {
        $seceret_key = $_POST['seceret_key'];
        $payment_id = $_POST['payment_id'];
        $razor_key = $_POST['razor_key'];
        $amount100 = (int)$_POST['amount'];
		//echo base_url();
			
		if(base_url()=="http://localhost/mayas1/" || base_url()=="http://epic.lennoxsoft.in/mayas/"){
			//echo 'if';
			
		}else{
			//echo 'else';
			$api = new RazorpayApi($razor_key, $seceret_key);
			$payment = $api->payment->fetch($payment_id);
			$payment->capture(array('amount' => $amount100));
		}
	
		//echo 'gyjgyj' ;
		$data = array(
			'order_payment_id' => $_POST['payment_id'], 
			'amount' => $amount100,
		);
			
		$result = $this->db->update('mayas_vastu_consultation', $data, array('vastu_id'=>$_POST['order_payment_id']));
		
		#loading the require model
        // $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        // $return_value = $this->obj_wa->service_order_buy_data();
        if ($result) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' you have posted comment successfully';
            $arr_response['data'] = 'hii';
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
        // $return_value = $this->obj_wa->service_order_buy_data();
        // if ($return_value) {
        //     $arr_response['status'] = SUCCESS; /* 200 */
        //     $arr_response['message'] = ' you have posted comment successfully';
        //     $arr_response['data'] = $return_value;
        // } else {
        //     $arr_response['status'] = DB_ERROR; /* 201 */
        //     $arr_response['message'] = 'Something went Wrong! Please try again';
        // }
		echo json_encode($arr_response);
    }


	public function paytest1() {
		
        $seceret_key = $_POST['seceret_key'];
        $payment_id = $_POST['payment_id'];
        $razor_key = $_POST['razor_key'];
        $amount100 = (int)$_POST['amount'];
		if(base_url()=="http://localhost/mayas1/"){
			$api = new RazorpayApi($razor_key, $seceret_key);
			$payment = $api->payment->fetch($payment_id);
			$payment->capture(array('amount' => $amount100));
		}
		$order_id=$_POST['order_id'];
		$email=$_POST['email'];
		$data = array(
			'order_id' => $_POST['payment_id'], 
		);
		$data1=array(
			'transaction_id'=> $_POST['payment_id'], 
			'payment_status'=> "paid", 
		
		);
		$data2=array(
			
			'payment_status'=> "paid", 
		
		);
		
			
		$result = $this->db->update('mayas_order_payment', $data, array('order_payment_id'=>$_POST['order_id']));
		$result = $this->db->update('product_order', $data1, array('order_id'=>$_POST['order_id']));
		$result = $this->db->update('product_order_detail', $data2, array('order_id'=>$_POST['order_id']));
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->product_order_data($order_id,$email);
        if ($return_value) {
            $arr_response['status'] = SUCCESS; /* 200 */
            $arr_response['message'] = ' you have placed order successfully';
            $arr_response['data'] = $return_value;
        } else {
            $arr_response['status'] = DB_ERROR; /* 201 */
            $arr_response['message'] = 'Something went Wrong! Please try again';
        }
		echo json_encode($arr_response);
    }
	
	public function cart()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		//var_dump($data['services']);
		$this->load->view('components/header');
		$this->load->view('cart');
		$this->load->view('components/footer',$data);
		
	}
	public function checkout()	{
		$this->load->model('Comman_model', 'obj_comman', TRUE);
		$data['services'] = $this->obj_comman->get('mayas_services');
		$data['services1'] = $this->obj_comman->get('mayas_bespoke_services');
		
		$this->load->view('components/header');
		$this->load->view('checkout');
		$this->load->view('components/footer',$data);
		
	}
	
	
		function public_page_list()
	   {
		
		$list_data = $this->db->get('admin_public_content');
		$data_value = array();
		$data['cities'] = $this->db->get('cities')->result_array();
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'page_heading' => $row->page_heading,
					'page_url' => $row->page_url,
					'url_specification' => $row->url_specification,
					'page_content' => $row->page_content,
					'image' => $row->image,
					'linked_services' => $row->linked_services,
					'page_title' => $row->page_title,
					'page_meta' => $row->page_meta,
					'page_keyword' => $row->page_keyword,
					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		}	
		    $data['public_page'] = $data_value;
		
         $this->load->view('components/header');
		$this->load->view('contentseo/public_list',$data);
		$this->load->view('components/footer',$data);
	
	}
	function clog(){
		$page_url =  $this->uri->segment(3);
		
		$list_data = $this->db->get_where('admin_public_content',array('page_url'=>$page_url));
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		
		$data_value = array();
		// var_dump($this->db->last_query());
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'page_heading' => $row->page_heading,
					'page_url' => $row->page_url,
					'url_specification' => $row->url_specification,
					'page_content' => $row->page_content,
					'image' => $row->image,
					'linked_services' => $row->linked_services,
					'page_title' => $row->page_title,
					'page_meta' => $row->page_meta,
					'page_keyword' => $row->page_keyword,
					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		    $data['public_page'] = $data_value;
		    // $data['services'] = $services;
		
			 $this->load->view('components/header');
			$this->load->view('contentseo/pagecontent',$data);
			$this->load->view('components/footer',$data);
		}else{
			//redirect(base_url() . 'welcome/public_page_list');
		}
	
	}
	function clogs(){
		$city_id =  $this->uri->segment(3);
		$page_url =  $this->uri->segment(4);
		$page_url_ = explode("-in-", $page_url);
		if(count($page_url_)  >1){
			$page_url = $page_url_[0];
		}
		
		$city_data = $this->db->get_where('cities',array('id'=>$city_id))->result_array();
		$list_data = $this->db->get_where('admin_public_content',array('page_url'=>$page_url));
		
		$this->load->model('Welcome_model', 'obj_wa', TRUE);
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		
		$data_value = array();
		// var_dump($this->db->last_query());
		
	    if($list_data && $list_data->num_rows()>0)
		{
			$data1 = array();
			foreach($list_data->result() as $row)
			{   
			  
				if(!array_key_exists($row->content_id ,$data1))
				{
					$data1[$row->content_id] = array();
				}
				if(array_key_exists($row->content_id ,$data1))
				{
					
					
					$data1[$row->content_id ] = array(
					
				    'content_id' => $row->content_id,
					'page_heading' => $row->page_heading,
					'page_url' => $row->page_url,
					'url_specification' => $row->url_specification,
					'page_content' => $row->page_content,
					'image' => $row->image,
					'linked_services' => $row->linked_services,
					'page_title' => $row->page_title,
					'page_meta' => $row->page_meta,
					'page_keyword' => $row->page_keyword,
					
					);
				}
				
					
				array_push($data_value,$data1[$row->content_id ]);
			}
			
		    $data['public_page'] = $data_value;
		    $data['city_data'] = $city_data;
		    // $data['services'] = $services;
		
			 $this->load->view('components/header');
			$this->load->view('contentseo/pagecontent',$data);
			$this->load->view('components/footer',$data);
		}else{
			//redirect(base_url() . 'welcome/public_page_list');
		}
	
	}

}
