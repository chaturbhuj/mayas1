<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Paypal extends CI_Controller {
 
    /**
     * Class constructor.
     * Adding libraries for each call.
     */
    public function __construct() {
        parent::__construct();
        $this->load->library('merchant');
        $this->merchant->load('paypal_express');
    }
 
    /**
     * On controller load call function.
     * Initializing paypal settings and make purchase call.
     */
    public function index() {
        // initialize gateway
		
        //$settings = $this->merchant->default_settings();
        $this->initialize_settings();
		echo '1';
        // sending values to payment gateway.
		$total_amount = $_GET['total_amount'];
		$order_id = $_GET['order_id'];
		$currency = $_GET['currency'];
		$page = $_GET['page'];
		echo '2';
        $params = array(
              'amount' => $total_amount,
              'item' => 'Astrology',
              'description' => 'Mayasastrology.com',
              'currency' => $currency,//$this->config->item('currency'),
              'return_url' => base_url() . 'paypal/payment_return/'.$total_amount.'/'.$order_id.'/'.$currency.'/'.$page,
              'cancel_url' => base_url() . 'paypal/cancel'
        );
		echo '3';
		print_r($params);
        $response = $this->merchant->purchase($params);
		
		print_r($response);
		echo '4';
    }
     
    /**
     * Handling return call.
     * Make final payment.
     */
    public function payment_return() {
        $this->initialize_settings();
		$total_amount = $this->uri->segment(3);
		$order_id = $this->uri->segment(4);
		$currency = $this->uri->segment(5);
		$page = $this->uri->segment(6);
        $params = array(
              'amount' => $total_amount,
			  'item' => 'Astrology',
              'description' => 'Mayasastrology.com',
              'currency' => $currency,//$this->config->item('currency'),
              'cancel_url' => base_url() . 'paypal/cancel'
        );
        $response = $this->merchant->purchase_return($params);
        // A complete transaction.
        if ($response->status() == Merchant_response::COMPLETE) {
          // data which is return by payment gateway to use.
          $token = $this->input->get('token');
          $payer_id = $this->input->get('PayerID');
          // Unique id for payment must save it for further payment queries.
          $gateway_reference = ($response->reference() ? $response->reference() : '');
		

          $data = array(
			'order_id' => $token, 
		);
			
		$result = $this->db->update('mayas_order_payment', $data, array('order_payment_id'=>$order_id));
		
		#loading the require model
        $this->load->model('Welcome_model', 'obj_wa', TRUE);
        #calling the function to register a new blog into the database
        $return_value = $this->obj_wa->service_order_buy_data($order_id);

        
        if ($return_value) {
               if($page == 'donate'){
			 redirect('paypal/paypal_donte_response/'.$order_id.'/'.$token.'/'.$payer_id.'/'.$total_amount.'/'.$currency);
		 }else if($page == 'instant'){
			 redirect('paypal/paypal_instant_response/'.$order_id.'/'.$token.'/'.$payer_id.'/'.$total_amount.'/'.$currency);
		 }else if($page == 'service'){
            redirect('welcome/razorResponseHandlerservice/'.$return_value.'');
		 }else{
			 
		 }




            # code...
        }
		
		
		
      
        // Do your stuff here db insertion, email etc..
        }
        else{
           //Your payment has been failed redirect with message.
          $message = ($response->message() ? $response->message() :'');
          $this->session->set_flashdata('error','Error processing payment: ' . $message.' Please try again');
          redirect('paypal/cancel'); 
        }
    }
    
	//-------------------------------------------------------------
    /**
     * This function is used to load user deshboard
     * 
     * @access		public
     * @since		1.0.0
     */
 
	public function paypal_donte_response(){	
		$data2['title']='Astrology and Horoscope, Free Vedic Astrology Readings Online, Consult an Astrologer Instantly';
		$data2['meta_description']='Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone, Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, Free astrology consultation, free online gemstone recommendation, free and premium horoscope reading on phone, Vastu, Horary, Indian Vedic astrology';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('paypal_donte_response');
		$this->load->view('components/footer',$data);
	}
	
	public function paypal_instant_response(){	
		$data2['title']='Astrology and Horoscope, Free Vedic Astrology Readings Online, Consult an Astrologer Instantly';
		$data2['meta_description']='Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone, Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, Free astrology consultation, free online gemstone recommendation, free and premium horoscope reading on phone, Vastu, Horary, Indian Vedic astrology';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('paypal_instant_response');
		$this->load->view('components/footer',$data);
	}
	
	public function paypal_service_response(){	
		$data2['title']='Astrology and Horoscope, Free Vedic Astrology Readings Online, Consult an Astrologer Instantly';
		$data2['meta_description']='Get Instant Free Astrology, Horoscope Readings and Predictions Based on Indian Vedic Astrology, Consult the Best Astrologer Online & Phone, Discuss your Problem in Detail for Under ₹ 300, Get Advice about Gemstones, Career, Finance, Marriage, Love & Health. ';
		$data2['meta_keyword']='Mayas astrology, Free astrology consultation, free online gemstone recommendation, free and premium horoscope reading on phone, Vastu, Horary, Indian Vedic astrology';
		 $this->load->model('Welcome_model', 'obj_wa', TRUE);
		
		$data['services'] = $this->obj_wa->get_list_of_all_services();
		$data['services1'] = $this->obj_wa->get_list_of_all_bespoke_services();
		$this->load->view('components/header',$data2);
		$this->load->view('paypal_service_response');
		$this->load->view('components/footer',$data);
	}
	
	
	
   /**
     * Handling on payment cancel.
     */
    public function cancel(){
      $this->load->view('paypal/payment_cancel');
    }
 
    /**
     * Paypal settings initialization.
     */
    public function initialize_settings(){
      
	  $settings = array(
            'username' => $this->config->item('api_username'),
            'password' => $this->config->item('api_password'),
            'signature' => $this->config->item('api_signature'),
            'test_mode' => $this->config->item('test_mode')
      );
	  
      $this->merchant->initialize($settings);
    }
}