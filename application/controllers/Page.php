<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');

/**
 * Controller class to configure RBAC system.
 * 
 */
class Page extends CI_Controller {

    /**
     * Default constructor.
     * 
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 1)) {
            redirect(base_url() . 'login');
        }
    }
	
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for showing the page for free consultation
     * 
     * @access		public
     * @since		1.0.0
     */
    public function consultation() {
		$this->load->model('Page_access_model', 'obj_pa', TRUE);
        #load required model
		$is_list_100 = $this->uri->segment(3);
		$uptolimit = $this->uri->segment(4);
		
		if($is_list_100 == 'match'){
			$segment_d = '';
			$segment_t = '';
			if(isset($_GET['inpd'])){
				$segment_d = $_GET['inpd'];
			}
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation_matching($segment_d);
			
		}else if($uptolimit){
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation_by_range($is_list_100, $uptolimit);
		}else if($is_list_100 == null){
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation(0);
		}else{
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation($is_list_100);
		}
        
        
        $this->load->view('configure/configure_header');
        $this->load->view('configure/consultation', $data);
        $this->load->view('configure/configure_footer');
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for showing the page for free consultation
     * 
     * @access		public
     * @since		1.0.0
     */
    public function paid_consultation() {
		$this->load->model('Page_access_model', 'obj_pa', TRUE);
        #load required model
		/*
		$is_list_100 = $this->uri->segment(3);
		if($is_list_100 == 'match'){
			$segment_d = '';
			$segment_t = '';
			if(isset($_GET['inpd'])){
				$segment_d = $_GET['inpd'];
			}
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation_matching($segment_d);
			
		}else if($is_list_100 == null){
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation(0);
			
		}else{
			$data['consultation_list'] = $this->obj_pa->get_list_of_consultation($is_list_100);
		}*/
		
        $data['consultation_list'] = $this->obj_pa->get_list_of_paid_consultation();
        
        $this->load->view('configure/configure_header');
        $this->load->view('configure/paid_consultation', $data);
        $this->load->view('configure/configure_footer');
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for showing the page for search free consultation 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function consultation_search() {
        
        $this->load->view('configure/configure_header');
        $this->load->view('configure/consultation_search');
        $this->load->view('configure/configure_footer');
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove lagan from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_consultation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_consultation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Consultation has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }

	public function update_all_sms_consultants() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_all_sms_consultants();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Message has been sent successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	public function update_all_email_consultants() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_all_email_consultants();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Email has been to all selected consultants successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	public function update_status_of_appointment() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_status_of_appointment();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Status has been changed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	public function update_notes_of_consutatnts() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_notes_of_consutatnts();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Note has been added successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	public function get_all_notes_of_consutatnt_id() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->get_all_notes_of_consutatnt_id();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Note has been added successfully!";
            $arr_response['data'] = $return_val;
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
   //--------------------------------------------------------------------
    /**
     * Handles requests for showing the page for free gemstone_recommendation
     * 
     * @access		public
     * @since		1.0.0
     */
    public function gemstone_recommendation() {
        #load required model
        $this->load->model('Page_access_model', 'obj_pa', TRUE);
        $data['gemstone_recommendation_list'] = $this->obj_pa->get_list_of_gemstone_recommendation();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/gemstone_recommendation', $data);
        $this->load->view('configure/configure_footer');
    } 
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_gemstone_recommendation from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_gemstone_recommendation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_gemstone_recommendation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Gemstone Recommendation has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	 //--------------------------------------------------------------------
    /**
     * Handles requests for showing the page for free gemstone_recommendation
     * 
     * @access		public
     * @since		1.0.0
     */
    public function popup_ads() {
        #load required model
        $this->load->model('Page_access_model', 'obj_pa', TRUE);
        $data['popup_ads_list'] = $this->obj_pa->get_list_of_popup_ads();
        $this->load->view('configure/configure_header');
        $this->load->view('configure/popup_ads',$data);
        $this->load->view('configure/configure_footer');
    } 
	
	//--------------------------------------------------------------------------
    /**
     * This function is used to upload image.
     */
    public function upload_image() {
        #array to hold the response values to be displayed
        $arr_response = array();

        $info = pathinfo($_FILES['myFile']['name']);
        $ext = $info['extension']; // get the extension of the file

        if (($ext == "GIF" || $ext == "PNG" || $ext == "JPG" || $ext == "jpg" || $ext == 'gif' || $ext == 'png' || $ext == 'jpeg') && ($_FILES["myFile"]["type"] == "image/PNG" || $_FILES["myFile"]["type"] == "image/GIF" || $_FILES["myFile"]["type"] == "image/JPG" || $_FILES["myFile"]["type"] == "image/jpg" ||$_FILES["myFile"]["type"] == "image/jpeg" || $_FILES["myFile"]["type"] == 'image/gif' || $_FILES["myFile"]["type"] == 'image/png') &&
                ($_FILES["myFile"]["size"] < 20485760)) {
            $file = $info['filename'];
            $filename = $_POST['image_cat'] . '_' . uniqid() . '.' . $ext;
            $target = './uploads/' . $filename;
            $file = $_FILES['myFile']['tmp_name'];
            move_uploaded_file($file, $target);

            $arr_response['status'] = 200;
            $arr_response['filename'] = $filename;
        } else {
            $arr_response['status'] = FAILED;
            $arr_response['message'] = "Not a valid File";
        }
        echo json_encode($arr_response);
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_popup_ads in MPS 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_popup_ads() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Page_access_model', 'obj_pa', TRUE);

        $return_val = $this->obj_pa->update_popup_ads();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Popup Ads has been updated successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	
	
	public function public_content() {
        #load required model
        $this->load->model('Page_access_model', 'obj_pa', TRUE);
       $content_cat_id = 0;
		if(isset($_GET['content_cat'])){
			$content_cat_id = explode('-',$_GET['content_cat'])[0];
		}
		$data['content_detail'] = $this->obj_pa->get_public_content_by_content_cat_id($content_cat_id);
        $this->load->view('configure/configure_header');
        $this->load->view('configure/public_content',$data);
        $this->load->view('configure/configure_footer');
    } 
	
	public function update_public_content() {
        #array to hold value to be display
        $arr_response = array();
		$this->load->model('Page_access_model', 'obj_pa', TRUE);

        $return_val = $this->obj_pa->update_public_content();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Content updated successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Select a Category !please";
        }
        echo json_encode($arr_response);
    }
	
	public function get_search_result() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->get_search_result();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "search successfully!";
            $arr_response['data'] = $return_val;
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	 public function vastu_consultations() {
        #load required model
        $this->load->model('Page_access_model', 'obj_pa', TRUE);
        $data['vastu_consultation'] = $this->obj_pa->mayas_vastu_consultation();
        $this->load->view('configure/configure_header');
        $this->load->view('admin/page/vastu_consultations',$data);
        $this->load->view('configure/configure_footer');
    } 
	
	public function remove_vastu_consultation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->remove_vastu_consultation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Consultation has been removed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	
	public function update_vastu_status_of_consultation() {
        #array to hold value to be display
        $arr_response = array();
        $this->load->model('Page_access_model', 'obj_ca', TRUE);

        $return_val = $this->obj_ca->update_vastu_status_of_consultation();
        if ($return_val) {
            $arr_response['status'] = SUCCESS;
            $arr_response['message'] = "Status has been changed successfully!";
        } else {
            $arr_response['status'] = DB_ERROR;
            $arr_response['message'] = "Something went Wrong! Please try again";
        }
        echo json_encode($arr_response);
    }
	

	
}

  