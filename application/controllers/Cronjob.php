<?php

if (!defined('BASEPATH'))
    exit('Not a valid request!');
/**
 * Controller class to configure RBAC system. 
 */
class Cronjob extends CI_Controller {
    /**
     * Default constructor.
     * @access	public
     * @since	1.0.0
     */
    function __construct() {
        parent::__construct();
        if (!($this->session->userdata('loggedIN') == 1)) {
            redirect(base_url() . 'login');
        }
    }
    //-------------------------------------------------------------
    /**
     * This function is used to load admin dashboard
     * @access		public
     * @since		1.0.0
     */
    public function index() {
		
			$this->db->limit(1);  
			$cronjob=$this->db->get_where('cronjob',array('status'=>'Active'))->result();
			// var_dump($cronjob);
			
			if($cronjob){
				foreach($cronjob as $val){
					$cronjob_id=$val->cronjob_id;
					$count_end=$val->count_end;
					$c_end_id=$val->c_end_id;
					$consultant_type=$val->consultant_type;
					$Subjects=$val->Subject;
					$bocy=$val->bocy;
					
					if($consultant_type=="free"){
						$this->db->limit(10,$count_end);  
						$db_result = $this->db->get_where('mayas_consultation')->result();
						//var_dump($db_result);
						
						foreach($db_result as $value){
							// var_dump($value);
							$subject=$Subjects;
							$inputEmail=$value->inputEmail;
							$name=$value->full_name;
							
							$this->load->library('email');
							$e_config = array(
								'charset'=>'utf-8',
								'wordwrap'=> TRUE,
								'mailtype' => 'html',
								'priority' => '1'
							);
							$to = COMPANY_EMAIL_ID;
							$this->email->initialize($e_config);
							$this->email->from($inputEmail, $name);
							$this->email->to($to); 
							$this->email->subject($subject);
							$this->email->message($bocy);	
							$this->email->send();
							var_dump($inputEmail);
							var_dump($e_config);
							var_dump($this->email->send());
						}
						
					}elseif($consultant_type=="paid"){
						$this->db->limit(10,$count_end);  
						$db_result = $this->db->get_where('mayas_order_payment')->result();
						foreach($db_result as $value){
							$subject=$Subjects;
							//var_dump($value);
							$inputEmail=$value->email;
							$name=$value->name;
							
							$this->load->library('email');
							$e_config = array(
								'charset'=>'utf-8',
								'wordwrap'=> TRUE,
								'mailtype' => 'html',
								'priority' => '1'
							);
							$to = COMPANY_EMAIL_ID;
							$this->email->initialize($e_config);
							$this->email->from($inputEmail, $name);
							$this->email->to($to); 
							$this->email->subject($subject);
							$this->email->message($bocy);	
							$this->email->send();
							var_dump($inputEmail);
						}
					}
					
					if($count_end < $c_end_id){
						$data=array(
						'count_end'=>$count_end+10,
						);
						
						$return_val = $this->db->update('cronjob', $data, array('cronjob_id' => $cronjob_id));
						
					}
					if($count_end > $c_end_id){
						var_dump('gvv');
						$datas=array(
						'status'=>'Completed',
						);
						$return_val = $this->db->update('cronjob', $datas, array('cronjob_id' => $cronjob_id));
					}
					
				}
			}
			
				
	}

}	


