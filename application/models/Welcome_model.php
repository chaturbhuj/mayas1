<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Welcome_model extends CI_Model {
  //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all main premium service 
     */
    public function get_list_of_main_services() {
		//$this->db->limit(1);
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active','services_type' => 'main'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
						'services_description' => $row->services_description,
						'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
						 'number_of_questions' => $row->number_of_questions,
                        'consultant_type' => $row->consultant_type,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	public function get_consultation_massages() {
		//$this->db->limit(1);
        $db_result = $this->db->get_where('consultation_massages', array('massage_id' => 1));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
				//var_dump($row);
                if (!array_key_exists($row->massage_id, $data)) {
                    $data[$row->massage_id] = array();
                }
                if (array_key_exists($row->massage_id, $data)) {
                    $data[$row->massage_id] = array(
                        'massage_id' => $row->massage_id,
						'default_massage' => $row->default_massage,
                        'custom_message' => $row->custom_message,
                        'custom_message1' => $row->custom_message1,
						'paid_massage' => $row->paid_massage,
                        'custom_message_paid' => $row->custom_message_paid,
                        'vastu_default_msg' => $row->vastu_default_msg,
                        
                       
                       
						'start_date' => $row->start_date,
						'end_date' => $row->end_date,
                    );
                    array_push($data_value, $data[$row->massage_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	public function get_list_of_services_by_id($s_id) {
		
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active','services_url'=>$s_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
						'services_description' => $row->services_description,
						'services_image' => $row->services_image,
						'services_detail_image' => $row->services_detail_image,
						'faq_category' => $row->faq_category,
						'banner_id' => $row->banner_id,
						'about_content_id' => $row->about_content_id,
						'detail_image_position' => $row->detail_image_position,
						'service_step_image' => $row->service_step_image,
                        'service_id' => $row->service_id,
                        'number_of_questions' => $row->number_of_questions,
                        'consultant_type' => $row->consultant_type,
                        'cost' => $row->cost,
						'discounted_cost' => $row->discounted_cost,
                        'premium_type' => $row->premium_type,
						'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
						'q_label' => $row->q_label,
						'q_placeholder' => $row->q_placeholder,
						'q_info' => $row->q_info,
						'is_question1' => $row->is_question1,
                        'q1_label' =>$row->q1_label,
                        'q1_placeholder' => $row->q1_placeholder,
                        'is_question2' => $row->is_question2,
                        'q2_label' => $row->q2_label,
                        'q2_placeholder' => $row->q2_placeholder,
                        'extra_description' => $row->extra_description,
						'additional_json'=> json_decode($row->additional_json)
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
			
            }
			
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all other premium service 
     */
    public function get_list_of_other_services() {
		
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active','services_type' => 'other'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
						'services_description' => $row->services_description,
						'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all other blogs 
     */
    public function get_list_of_other_blogs() {
		
        $db_result = $this->db->order_by('date');
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'blog_writer' => $row->blog_writer,
                        'status' => $row->status,
                        'date' => $row->date,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	 public function get_list_of_other_blogs_by_id($b_id) {
		
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active','blog_id'=>$b_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'status' => $row->status,
                        'date' => $row->date,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    } 
	
	public function get_list_of_other_services_by_id($s_id) {
		
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active','service_id'=>$s_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
						'services_description' => $row->services_description,
						'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all main blogs 
     */
    public function get_list_of_main_blogs() {
		//$this->db->limit(1);
		$this->db->order_by("blog_id", "DESC");
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_writer' => $row->blog_writer,
                        'blog_id' => $row->blog_id,
						'date' =>  $row->date,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	/*
     * This function is used to get blog by id 
     */
	public function get_list_of_main_blogs_by_id($b_id) {
		
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active','blog_id'=>$b_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'blog_writer' => $row->blog_writer,
                        'status' => $row->status,
                        'date' => $row->date,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    } 	
	/*
     * This function is used to get blog by id 
     */
	public function scroll_blog_full_data() {
		$this->db->limit(1);
		 $this->db->order_by('blog_id', 'asc');
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active','blog_id >'=>$_POST['blog_id']));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'status' => $row->status,
                        'blog_writer' => $row->blog_writer,
                        'date' => $row->date,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    } 
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of aboutus 
     */
    public function get_list_of_aboutus() {
        $db_result = $this->db->get_where('mayas_aboutus', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array();
                }
                if (array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array(
                        'aboutus1' => $row->aboutus1,
                        'aboutus2' => $row->aboutus2,
                        'aboutus3' => $row->aboutus3,
						'aboutus4' => $row->aboutus4,
						'aboutus_image' => $row->aboutus_image,
						
                    );
                    array_push($data_value, $data[$row->aboutus_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_testimonials
     */
    public function get_list_of_testimonials() {
		$db_result = $this->db->get_where('mayas_testimonials', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array();
                }
                if (array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array(
                        'testimonials_id' => $row->testimonials_id,
						'testimonials_name' => $row->testimonials_name,
						'position' => $row->position,
						'comments' => $row->comments,
						'testimonials_image' => $row->testimonials_image,
                        'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->testimonials_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
   //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all special_feature
     */
    public function get_list_of_all_special_feature() {
		$this->db->limit(3);
        $db_result = $this->db->get_where('mayas_special_feature', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->special_feature_id, $data)) {
                    $data[$row->special_feature_id] = array();
                }
                if (array_key_exists($row->special_feature_id, $data)) {
                    $data[$row->special_feature_id] = array(
                        'special_feature_heading' => $row->special_feature_heading,
                        'sub_heading' => $row->sub_heading,
                        'small_sub_heading_message' => $row->small_sub_heading_message,
                        'point1' => $row->point1,
                        'point2' => $row->point2,
                        'point3' => $row->point3,
                        'point4' => $row->point4,
                        'point5' => $row->point5,
                        'point6' => $row->point6,
                        'point7' => $row->point7,
                        'special_feature_id' => $row->special_feature_id,
                    );
                    array_push($data_value, $data[$row->special_feature_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all sub menu products
     */
    public function get_list_of_all_product() {
        $db_result = $this->db->get_where('mayas_sub_menu_products', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
				if (!array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array();
                }
                if (array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array(
					     'sub_menu_products_id'=> $row->sub_menu_products_id,
                        'sub_menu_products_name' => $row->sub_menu_products_name,
                        'sub_menu_products_navi' => $row->sub_menu_products_navi,
                        'sub_menu_products_image' => $row->sub_menu_products_image,
                    );
                    array_push($data_value, $data[$row->sub_menu_products_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by product id
   /*  */
   /* public function get_detail_of_products_by_product_id($s_id) {
        $db_result = $this->db->get_where('mayas_product', array('status' => 'active','product_id'=>$s_id));
		if ($db_result && $db_result->num_rows() > 0) {
		$data = array();
		$data_value = array();
		foreach($db_result->result() as $row){
		        if(!array_key_exists($row->product_id, $data)) {
				    $data[$row->product_id] = array ();
					}
				if (array_key_exists($row->product_id, $data)){ 
				     $data[$row->product_id] = array(
					 'product_id'=> $row->product_id,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'product_description'=> $row->product_description,
					 'product_price_ind'=> $row->	product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_feature'=> $row->product_feature,
					 'product_weight'=> $row->product_weight,
					 'product_shape'=> $row->product_shape,
					 'product_color'=> $row->product_color,
					 'product_clarity'=> $row->product_clarity,
					 'product_grade'=> $row->product_grade,
					 'product_cut'=> $row->product_cut,
					 'product_treatment'=> $row->product_treatment,
					 'product_origin'=> $row->product_origin,
					 'product_other_info'=> $row->product_other_info,
					 'product_other_info2'=> $row->product_other_info2,
					 'product_other_info3'=> $row->product_other_info3,
					 'product_image_list'=> $row->product_image_list,
					 );
			         array_push($data_value, $data[$row->product_id]); 
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}*/
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by cat id
     */
   /* public function get_list_of_products_by_cat_id($cat_id) {
	
        $db_result = $this->db->get_where('mayas_product', array('status' => 'active','product_categories'=>$cat_id ));
		if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
			$data_value = array();
			foreach($db_result->result() as $row){
		        if(!array_key_exists($row->product_id, $data)) {
				    $data[$row->product_id] = array ();
					}
				if (array_key_exists($row->product_id, $data)){ 
				     $data[$row->product_id] = array(
					 'product_id'=> $row->product_id,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'product_price_ind'=> $row->	product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_description'=> $row->product_description,
					 'product_image_list'=> $row->product_image_list,
					 );
			         array_push($data_value, $data[$row->product_id]); 
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}*/
	
	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all lagans AND stones
     */
    public function get_list_of_all_lagans() {
		$lagan = array('Aries','Taurus','Gemini','Cancer','Leo','Virgo','Libra','Scorpio','Sagittarius','Capricorn','Aquarius','Pisces',);
		$lagan_value = array();
		$data = array();
		$data_value = array();
		foreach($lagan as $value){
			$data_value[$value] = array();
			 $db_result = $this->db->get_where('mayas_lagan', array('status' => 'active','lagan_name' => $value));
			if ($db_result && $db_result->num_rows() > 0) {
				//var_dump($db_result->result());
				//echo '<br/><br/><br/>';
				foreach ($db_result->result() as $row) {
					if (!array_key_exists($row->lagan_id, $data)) {
						$data[$row->lagan_id] = array();
					}
					if (array_key_exists($row->lagan_id, $data)) {
						$data[$row->lagan_id] = array(
							'lagan_name' => $row->lagan_name,
							'life_stone_name' => $row->life_stone_name,
							'punya_stone_name' => $row->punya_stone_name,
							'bhagya_stone_name' => $row->bhagya_stone_name,
							'lagan_id' => $row->	lagan_id,
						);
						array_push($data_value[$value], $data[$row->lagan_id]);
					}
				}
				//print_r($data_value[$value]);
			} 	
				array_push($lagan_value, $data_value[$value]);
				//echo 'new <br/>';
				//print_r($lagan_value);	
				if($value == 'Pisces'){
					return $lagan_value;
				}	
		}
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all articles 
     */
    public function get_list_of_all_articles() {
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all articles 
     */
    public function get_public_content_of_id($id) {
        $db_result = $this->db->get_where('web_public_content', array('content_id' => $id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array();
                }
                if (array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array(
                        'heading' => $row->heading,
                        'content' => $row->content,
                        'content_id' => $row->content_id,
                    );
                    array_push($data_value, $data[$row->content_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	public function applied_coupan_check() {
		$today = date('Y-m-d');
		$total_cost = (Float)$_POST['total_cost'];
		$coupan_for = $_POST['coupan_for'];
		$code = $_POST['code'];
		
		$this->db->from("mayas_coupan_code");
		$this->db->where('coupan_code like binary', $code);
		$this->db->where('start_date <=', $today);
		$this->db->where('end_date >=', $today);
		$this->db->where('code_for', $coupan_for);
		$this->db->where('status', 'active');
        $db_result = $this->db->get(); 
        if ($db_result && $db_result->num_rows() > 0) {
			return $db_result->result();
        } else {
            return FALSE;
        }
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_contactus() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
        $data = array(
            'inputName' => $_POST['inputName'],
            'inputEmail' => $_POST['inputEmail'],
            'inputWebsite' => $_POST['inputWebsite'],
            'inputMessage' => $_POST['inputMessage'],
        );
		
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

        $result = $this->db->insert('mayas_contactus', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_consultant() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r( $_POST['paymentmethod']);
		
		
        $data = array(
            'consultantType' => $_POST['consultantType'],
            'full_name' => $_POST['inputName'],
            'gender' => $_POST['inputGender'],
			'inputDateDate' => $_POST['inputDateDate'],
			'inputTimeMin' => $_POST['inputTimeMin'],
            'birthCountry' => $_POST['birthCountry'],
			'birthState' => $_POST['birthState'],
			'birthCity' => $_POST['birthCity'],
			'inputEmail' => $_POST['inputEmail'],
            'inputContact' => $_POST['inputContact'],
           // 'inputQuestion' => $_POST['inputQuestion'],
			//'otherInfomation' => $_POST['otherInfomation'],
			'paymentmethod' => $_POST['paymentmethod'],
			
			
			/*'educationQualification' => $_POST['educationQualification'],
			'currentProfession' => $_POST['currentProfession'],
            'partnerName' => $_POST['partnerName'],
            'partnerDateDate' => $_POST['partnerDateDate'],
			'partnerTimeMin' => $_POST['partnerTimeMin'],
            'partnerBirthCountry' => $_POST['partnerBirthCountry'],
			'partnerBirthState' => $_POST['partnerBirthState'],
			'partnerBirthCity' => $_POST['partnerBirthCity'],
			*/
			'created_date' => date('Y-m-d'),
            'created_time' => date('H:i:s'),
		);
		
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

        $result = $this->db->insert('mayas_consultation', $data);
		$insert_id = $this->db->insert_id();
        if($result) {
			/*sending the mail to admin*/
			#send a email to admin
			$this->load->library('email');			
			$subject = $insert_id;

			$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
			//$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
			//$message .= 'Dear Admin,<br/><br/>';
			//$message .= '<table><tbody><tr><td>A new consultation came</td></tr>';
			//$message .= '<tr><td><p>The Detail as below </p></td></tr>';
			$message .= '<tr><td><p>Consultant Type : <strong>'.$_POST['consultantType'].' Free</strong></p></td></tr>';
			$message .= '<tr><td><p>Name : <strong>'.$_POST['inputName'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Gender : <strong>'.$_POST['inputGender'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Date of Birth : <strong>'.date('j F, Y',strtotime($_POST['inputDateDate'])).'</strong></p></td></tr>';
			$message .= '<tr><td><p>Time of Birth : <strong>'.$_POST['inputTimeMin'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['birthCountry'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth State : <strong>'.$_POST['birthState'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth City : <strong>'.$_POST['birthCity'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Email ID : <strong>'.$_POST['inputEmail'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['inputContact'].'</strong></p></td></tr>';
			
		
			$message .= '</tbody></table>';
			$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
			$message .= '</div></div>';
			//print_r($message);
			/*
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$to = COMPANY_EMAIL_ID;
			$this->email->initialize($e_config);
			$this->email->from($_POST['inputEmail'], $_POST['inputName']);
			$this->email->to($to); 
			$this->email->subject($subject);
			$this->email->message($message);	
			$this->email->send();
			*/
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => $subject,
				'to' => PROJECT_website_EMAIL,
				'to_name' => PROJECT_NAME,
					'from' => PROJECT_HR_EMAIL,
					'from_name' => PROJECT_NAME,
				'msg' => $message,
			);
			$result1 = $this->obj_email->send_mail_in_sendinblue($data11);
		
			
			 $this->load->library('email');			
				$subject = "Welcome to ".WEBSITE_NAME;
		
				$message1 = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
				$message1 .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
				$message1 .= 'Hello '.$_POST['inputName'] .',<br/><br/>';

				$message1 .= '<table><tbody>';
				$message1 .= '<tr><td>You have successfully submitted your details for Instant Consultation :Free .</td></tr>';
				$message1 .= '<tr><td><p><strong>Talk to Astrologer Mayankesh Now.</strong> (11 a.m. to 07 p.m. India Time).</p></td></tr>';
				$message1 .= '<tr><td><p>Ask A Question of your Choice & Get 10 Minute Astrology Reading on Phone (worth ₹ 500), Absolutely FREE.</p></td></tr>';
				$message1 .= '<tr><td><p>Your consultation access code is : <strong>'.$insert_id .'</strong></p></td></tr>';
				$message1 .= '<tr><td><p>Note: Free Consultation is an Exclusive Offer for Our First Time Users Only.<br/><br/></p></td></tr>';
				$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Warm Regards</p></td></tr>';
				$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p></td></tr>';
				$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Astro-Consultant  </p></td></tr>';
				$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">+91- 9829265640 </p></td></tr>';
				$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">MayasAstrology.com</p></td></tr>';
				/*done*/
				
				
			
				$message1 .= '</tbody></table>';
				$message1 .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
				$message1 .= '</div></div>';
				
		
				/*
				//print_r($message1);
				
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to11 = 'MayasAstrology';
				$to = $_POST['inputEmail'];
				$to1 = COMPANY_EMAIL_ID_OTHER;
				$this->email->initialize($e_config);
				$this->email->from(COMPANY_EMAIL_ID,$to11 );
				$this->email->to($to); 
				//$this->email->cc($to1); 
				$this->email->subject($subject);
				$this->email->message($message1);	
				$this->email->send();*/
				
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => $subject,
				'from' => PROJECT_website_EMAIL,
				'from_name' => PROJECT_NAME,
					'from' => PROJECT_HR_EMAIL,
					'from_name' => PROJECT_NAME,
				'msg' => $message1,
			);
			$result1 = $this->obj_email->send_mail_in_sendinblue($data11);
			
			
            return $insert_id;
			
			
			
        } else {
            return FALSE;
        } 
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_vastu_consultant() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r( $_POST['paymentmethod']);
		//echo $_POST['consultantType'],
		$image = '';	
		if (isset($_POST['images'])) {
			$image=json_encode($_POST['images']);
		}else{
			$image = '';	


		}
		// var_dump($image);
	
        $data = array(
            'consultantType' => $_POST['consultantType'],
            'inputName' => $_POST['inputName'],
            'inputGender' => $_POST['inputGender'],
			'payment_method' => $_POST['payment_method'],
			'currency' => $_POST['currency'],
			'inputDateDate' => $_POST['inputDateDate'],
			'propertydirection' => $_POST['propertydirection'],
			'inputTimeMin' => $_POST['inputTimeMin'],
            'birthCountry' => $_POST['birthCountry'],
			'birthState' => $_POST['birthState'],
			'birthState' => $_POST['birthState'],
			'birthState' => $_POST['birthState'],
			'birthCity' => $_POST['birthCity'],
			'inputEmail' => $_POST['inputEmail'],
            'inputContact' => $_POST['inputContact'],
			'created_date' => date('Y-m-d'),
            'created_time' => date('H:i:s'),
			'status' => 'active',
			'address' => $_POST['address'],
			'image' => $image,
			'primary_image' 	=>  $_POST['primary_image'],
			'appointment_yes' =>'pending',
		);

        $result = $this->db->insert('mayas_vastu_consultation', $data);
		//var_dump($result);
		$insert_id = $this->db->insert_id();

		if($result){



		
			$content = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;"><div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>Dear Admin ,<br /><br /><table><tbody><tr><td>A new consultation came</td></tr>
			<tr><td><p>The Detail as below </p></td></tr>
			<tr><td><p>Consultation Code : <strong>'.$insert_id.'</strong></p></td></tr>
			<tr><td><p>Name : <strong>'.$_POST['inputName'].'</strong></p></td></tr>
			<tr><td><p>Consultation Type : <strong>'.$_POST['consultantType'].'</strong></p></td></tr>
			<tr><td><p>Email ID : <strong>'.$_POST['inputEmail'].'</strong></p></td></tr>
			<tr><td><p>Contact NO. : <strong>'.$_POST['inputContact'].'</strong></p></td></tr>
		   </tbody></table></div><br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/></div></div>';
		  


			$this->load->model('Email_model', 'obj_email', TRUE);
			$to = COMPANY_EMAIL_ID;
			$to11 = 'MayasAstrology';

			$subject = $insert_id;
				$data11 = array(
					'subject' => $subject,
					'to' => $to,
					'to_name'=>$to11,
					'from' => $_POST['inputEmail'],
					// 'from' => PROJECT_EMAIL,
					'from_name' => $_POST['inputName'],
					'msg' => $content,
				);
				$result = $this->obj_email->send_mail_in_sendinblue($data11);
				
	
					$content1 = 'Dear ' . $_POST['inputName'] . ' ,<br /><br /><p>Your '.$_POST['consultantType'].' consultation is booked </p><p>Your Consultation Access Code is: '.$insert_id.'</p></div>';

					// print_r($content1);
				
					$this->load->model('Email_model', 'obj_email', TRUE);
					$subject = "Welcome to ".WEBSITE_NAME;
					$to = $_POST['inputEmail'];
					$to11 = 'MayasAstrology';
						$data11 = array(
							'subject' => $subject,
							'to' => $to,
							'to_name'=>$_POST['inputName'],
							'from' => COMPANY_EMAIL_ID,
							// 'from' => PROJECT_EMAIL,
							'from_name' => $to11,
							'msg' => $content1,
						);
						$result = $this->obj_email->send_mail_in_sendinblue($data11);
	   
			 
	   }
		//$primary = $insert_id.' - '.(substr($_POST['order_id'] , -4)) ;
		//return $primary;
		
		return $insert_id;
       
    }
		//--------------------------------------------------------------------
    /**
     * Handles requests for adding free consultant details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_consultant_free() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r( $_POST['paymentmethod']);
		
		$result = $this->get_consultation_massages();
		$date=date('Y-m-d');
		if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			$consultation_massages=$result[0]['custom_message'];
		}else{
			$consultation_massages=$result[0]['default_massage'];
		}
		$consultantType =$_POST['consultantType'];
        $data = array(
			'question1' => $_POST['question1'],
			'question2' => $_POST['question2'],
			'question1_label' => $_POST['question1_label'],
			'question2_label' => $_POST['question2_label'],
            'consultantType' => $_POST['consultantType'],
            'full_name' => $_POST['inputName'],
            'gender' => $_POST['inputGender'],
			'inputDateDate' => $_POST['inputDateDate'],
			'inputTimeMin' => $_POST['inputTimeMin'],
            'birthCountry' => $_POST['birthCountry'],
			'birthState' => $_POST['birthState'],
			'birthCity' => $_POST['birthCity'],
			'inputEmail' => $_POST['inputEmail'],
            'inputContact' => $_POST['inputContact'],
            'inputQuestion' => $_POST['inputQuestion'],
			'paymentmethod' => $_POST['paymentmethod'],			
            'partnerName' => $_POST['partnerName'],
            'partnerDateDate' => $_POST['partnerDateDate'],
			'partnerTimeMin' => $_POST['partnerTimeMin'],
            'partnerBirthCountry' => $_POST['partnerBirthCountry'],
			'partnerBirthState' => $_POST['partnerBirthState'],
			'partnerBirthCity' => $_POST['partnerBirthCity'],
			'created_date' => date('Y-m-d'),
            'created_time' => date('H:m:s'),
			
		);
		
		

        $result = $this->db->insert('mayas_consultation', $data);
		$insert_id = $this->db->insert_id();
        if ($result) {
			/*sending the mail to admin*/
			#send a email to admin
			$this->load->library('email');			
			$subject = $insert_id;
			$message = '';
			$message .= '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
			//$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
			//$message .= 'Dear Admin,<br/><br/>';
			
			$message .= '<table><tbody>';
			//$message .= '<tr><td>A new consultation came</td></tr>';
			//$message .= '<tr><td><p>The Detail as below </p></td></tr>';
			$message .= '<tr><td><p>Consultant Type : <strong> '.$_POST['consultantType'].' </strong></p></td></tr>';
			$message .= '<tr><td><p>Name : <strong>'.$_POST['inputName'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Gender : <strong>'.$_POST['inputGender'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Date of Birth : <strong>'.date('j F, Y',strtotime($_POST['inputDateDate'])).'</strong></p></td></tr>';
			$message .= '<tr><td><p>Time of Birth : <strong>'.$_POST['inputTimeMin'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['birthCountry'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth State : <strong>'.$_POST['birthState'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth City : <strong>'.$_POST['birthCity'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Email ID : <strong>'.$_POST['inputEmail'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['inputContact'].'</strong></p></td></tr>';
			if($_POST['question1_label']){
				$message .= '<tr><td><p>'.$_POST['question1_label'].' : <strong>'.$_POST['question1'].'</strong></p></td></tr>';
			}
			if($_POST['question2_label']){
				$message .= '<tr><td><p>'.$_POST['question2_label'].' : <strong>'.$_POST['question2'].'</strong></p></td></tr>';
			}
			$message .= '<tr><td><p>Question : <strong>'.$_POST['inputQuestion'].'</strong></p></td></tr>';
			
			if($_POST['partnerName']){
				$message .= '<tr><td><p>Partner Name : <strong>'.$_POST['partnerName'].'</strong></p></td></tr>';
				$message .= '<tr><td><p>Partner Date of Birth : <strong>'.date('j F, Y',strtotime($_POST['partnerDateDate'])).'</strong></p></td></tr>';
				$message .= '<tr><td><p>Partner Time of Birth : <strong>'.$_POST['partnerTimeMin'].'</strong></p></td></tr>';
				$message .= '<tr><td><p>Partner Birth Country : <strong>'.$_POST['partnerBirthCountry'].'</strong></p></td></tr>';
				$message .= '<tr><td><p>Partner Birth State : <strong>'.$_POST['partnerBirthState'].'</strong></p></td></tr>';
				$message .= '<tr><td><p>Partner Birth City : <strong>'.$_POST['partnerBirthCity'].'</strong></p></td></tr>';
			}
			$message .= '</tbody></table>';
			$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
			$message .= '</div></div>';
			//print_r($message);
 
			/*
 
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$to = COMPANY_EMAIL_ID;
			$this->email->initialize($e_config);
			$this->email->from($_POST['inputEmail'], $_POST['inputName']);
			$this->email->to($to); 
			$this->email->subject($subject);
			$this->email->message($message);
			//print_r($this->email->message($message));	
			if(base_url() != 'http://localhost/mayas1/'){
				$this->email->send();
			}
			*/
			$this->load->model('Email_model', 'obj_email', TRUE);
			$data11 = array(
				'subject' => 'Consultation Code: '.$subject,
				'to' => PROJECT_website_EMAIL,
				'to_name' => PROJECT_NAME,
					'from' => PROJECT_HR_EMAIL,
					'from_name' => PROJECT_NAME,
				'msg' => $message,
			);
			//var_dump($data11);
			$result1 = $this->obj_email->send_mail_in_sendinblue($data11);
			
			 $this->load->library('email');			
				/*$subject1 = "Welcome to ".WEBSITE_NAME;
				$message1 = '';
						$message1 .= '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
						$message1 .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
						$message1 .= 'Hello '.$_POST['inputName'].',<br/><br/>';

						$message1 .= '<table><tbody>';
						$message1 .= '<tr><td>You have successfully submitted your details for: Free '.$_POST['consultantType'].' Consultation.</td></tr>';
						$message1 .= '<tr><td>'.$consultation_massages.'</td></tr>';
						$message1 .= '<tr><td><p>Your consultation access code is : <strong style="color: #007eff;">'.$insert_id .'</strong></p></td></tr>';
						$message1 .= '<tr><td><p>Ask A Question of your Choice & Get 10 Minute Astrology Reading on Phone (worth ₹ 500), Absolutely FREE.</p></td></tr>';
						$message1 .= '<tr><td><p >Note:  Free Consultation is Exclusive Offer for First Time Users Only.<br/><br/></p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Warm Regards</p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Astro-Consultant  </p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">+91- 9829265640   </p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">MayasAstrology.com     </p></td></tr>';
						
						$message1 .= '</tbody></table>';
						$message1 .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
						$message1 .= '</div></div>';
						/*done*/
				//print_r($message1);
				/*$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to11 = 'MayasAstrology';
				$to = $_POST['inputEmail'];
				$to1 = COMPANY_EMAIL_ID_OTHER;
				$this->email->initialize($e_config);
				$this->email->from(COMPANY_EMAIL_ID,$to11 );
				$this->email->to($to); 
				//$this->email->cc($to1); 
				$this->email->subject($subject1);
				$this->email->message($message1);	
				//print_r($this->email->message($message1));	
				//$this->email->send();*/
				
				//new email
				//$this->load->library('email');
				//$this->email->sendemail_free_consultation(1,$_POST['inputName'],$_POST['inputEmail'],NOREPLY_EMAIL);
				
				$this->load->model('Email_model','obj_mail', TRUE);					
				$emailresult = $this->obj_mail->sendemail_free_consultation(1,$_POST['inputName'],$_POST['inputEmail'],$insert_id,$_POST['consultantType'],NOREPLY_EMAIL);
				
				
			
            $data['insert_id']=$insert_id;
           
			return $data;

        } else {
            return FALSE;
        }
    }
    
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding free_gemstone_recommendation details into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_gemstone_recommendation() {
        date_default_timezone_set('Asia/Kolkata');
		$data = array(
            'full_name' => $_POST['inputName'],
            'gender' => $_POST['inputGender'],
			'inputDateDate' => $_POST['inputDateDate'],
			'inputDateMonth' => $_POST['inputDateMonth'],
			'inputDateYear' => $_POST['inputDateYear'],
			'inputTimeHr' => $_POST['inputTimeHr'],
            'inputTimeMin' => $_POST['inputTimeMin'],
            'inputTimeSec' => $_POST['inputTimeSec'],
			'birthCountry' => $_POST['birthCountry'],
			'birthState' => $_POST['birthState'],
			'birthCity' => $_POST['birthCity'],
			'inputEmail' => $_POST['inputEmail'],
            'inputContact' => $_POST['inputContact'],
            'created_date' => date('Y-m-d'),
            'created_time' => date('H:m:s'),
		);
		$result = $this->db->insert('mayas_gemstone_recommendation', $data);
        if ($result) {
			/*sending the mail to admin*/
			#send a email to admin
			$this->load->library('email');			
			$subject = "Welcome to ".WEBSITE_NAME;
			
			$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
			//$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
			//$message .= 'Dear Admin,<br/><br/>';
			//$message .= 'A new Gemstone Recommendation came<table></tbody>';
			//$message .= '<tr><td>The Detail as below </td></tr>';
			$message .= '<tr><td>Name : <strong>'.$_POST['inputName'].'</strong></td></tr>';
			$message .= '<tr><td>Gender : <strong>'.$_POST['inputGender'].'</strong></td></tr>';
			$message .= '<tr><td>Date of Birth : <strong>'.$_POST['inputDateDate'].'-'.$_POST['inputDateMonth'].'-'.$_POST['inputDateYear'].'</strong></td></tr>';
			$message .= '<tr><td>Time of Birth : <strong>'.$_POST['inputTimeHr'].':'.$_POST['inputTimeMin'].':'.$_POST['inputTimeSec'].'</strong></td></tr>';
			$message .= '<tr><td>Birth Country : <strong>'.$_POST['birthCountry'].'</strong></td></tr>';
			$message .= '<tr><td>Birth State : <strong>'.$_POST['birthState'].'</strong></td></tr>';
			$message .= '<tr><td>Birth City : <strong>'.$_POST['birthCity'].'</strong></td></tr>';
			$message .= '<tr><td>Email ID : <strong>'.$_POST['inputEmail'].'</strong></td></tr>';
			$message .= '<tr><td>Contact NO. : <strong>'.$_POST['inputContact'].'</strong></td></tr>';
			$message .= '</tbody></table><br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
			$message .= '</div></div>';
			//print_r($message);
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$to = COMPANY_EMAIL_ID;
			$this->email->initialize($e_config);
			$this->email->from($_POST['inputEmail'], $_POST['inputName']);
			$this->email->to($to); 
			$this->email->subject($subject);
			$this->email->message($message);	
			$this->email->send();
			
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_popup_ads
     */
    public function get_list_of_popup_ads() {
		$db_result = $this->db->get_where('mayas_popup_ads', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->popup_ads_id, $data)) {
                    $data[$row->popup_ads_id] = array();
                }
                if (array_key_exists($row->popup_ads_id, $data)) {
                    $data[$row->popup_ads_id] = array(
                        'popup_ads_id' => $row->popup_ads_id,
						'popup_ads_image' => $row->popup_ads_image,
						'navigation_link' => $row->navigation_link,
						'popup_status' => $row->popup_status,
						'free_consultation_image' => $row->free_consultation_image,
						'free_gemstone_recommendation_image' => $row->free_gemstone_recommendation_image,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->popup_ads_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
    * This function is used to get_list_of_slider
    */
    public function get_list_of_slider() {
		$this->db->order_by("slider_position", "desc");
		$db_result = $this->db->get_where('mayas_slider', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array();
                }
                if (array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array(
                        'slider_id' => $row->slider_id,
						'slider_info' => $row->slider_info,
						'slider_input1' => $row->slider_input1,
						'slider_input2' => $row->slider_input2,
						'slider_input3' => $row->slider_input3,
						'slider_navi' => $row->slider_navi,
						'slider_navi_link' => $row->slider_navi_link,
						'slider_image' => $row->slider_image,
						'content_align' => $row->content_align,
					);
                    array_push($data_value, $data[$row->slider_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
		
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all aboutus
     */
    public function get_list_of_all_aboutus() {
        $db_result = $this->db->get_where('mayas_aboutus', array('status' => 'active','aboutus_id'=> 1));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array();
                }
                if (array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array(
                        'aboutus1' => $row->aboutus1,
                        'aboutus2' => $row->aboutus2,
                        'aboutus_id' => $row->aboutus_id,
						'aboutus_image1' => $row->aboutus_image1,
						'aboutus_image2' => $row->aboutus_image2,
						
                    );
                    array_push($data_value, $data[$row->aboutus_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }	
	//-------------------------------------------------------------------------
    /*
    * This function is used to get list of all services
    */
    public function get_list_of_all_vastuservices() {
		// $this->db->order_by("services_position", "desc");
        $db_result = $this->db->get_where('mayas_new_vastu');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array();
                }
                if (array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array(
                        'services_name' => $row->page_heading,
                        'services_description' => $row->short_description,
                        'services_image' => $row->detail_image,
                        'vastu_id' => $row->vastu_id,
                        'services_url' => $row->url,
						'services_title' => $row->page_title,
						'services_dec_meta' => $row->meta_descp,
						'services_keyword_meta' => $row->meta_tag,
                    );
                    array_push($data_value, $data[$row->vastu_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
    public function get_list_of_all_services() {
		$this->db->order_by("services_position", "desc");
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
                        'cost' => $row->cost,
						 'col' => $row->col,
                        'services_description' => $row->services_description,
                        'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
						'disc_logo_percent' => $row->disc_logo_percent,
                        'premium_type' => $row->premium_type,
                        'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
						'bottom_border_color' => $row->bottom_border_color
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
    * This function is used to get list of all mayas_consultation_content
    */
    public function get_list_of_all_consultation_content() {
		$this->db->order_by("services_position", "desc");
        $db_result = $this->db->get_where('mayas_consultation_content');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultation_name' => $row->consultation_name,
                        // 'faq_category' => $row->faq_category,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                        'c_image' => $row->c_image,
                        'url' => $row->url,
						'title' => $row->title,
						'dec_meta' => $row->dec_meta,
						'keyword_meta' => $row->keyword_meta,
						'bg_color' => $row->bg_color,
						'text_color' => $row->text_color
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
    * This function is used to get list of all mayas_consultation_content
    */
    public function get_list_of_all_consultation_content_id($url) {
        $db_result = $this->db->get_where('mayas_consultation_content',array('url' => $url));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultation_name' => $row->consultation_name,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                        'banner_id' => $row->banner_id,
                        'about_content_id' => $row->about_content_id,
                        'faq_category' => $row->faq_category,
                        'c_image' => $row->c_image,
                        'c_detail_image' => $row->c_detail_image,
                        'detail_image_position' => $row->detail_image_position,
                        'consultation_type' => $row->consultation_type,
                        'url' => $row->url,
						'title' => $row->title,
						'dec_meta' => $row->dec_meta,
						'keyword_meta' => $row->keyword_meta,
						'bg_color' => $row->bg_color,
						'text_color' => $row->text_color,
						'q_label' => $row->q_label,
						'q_placeholder' => $row->q_placeholder,
						'q_info' => $row->q_info,
						'is_question1' => $row->is_question1,
                        'q1_label' =>$row->q1_label,
                        'q1_placeholder' => $row->q1_placeholder,
                        'is_question2' => $row->is_question2,
                        'q2_label' => $row->q2_label,
                        'q2_placeholder' => $row->q2_placeholder,
                        'extra_description' => $row->extra_description,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
			
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
   
//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all sub menu products
     */
    public function get_list_of_all_products() {
        $db_result = $this->db->get_where('mayas_sub_menu_products', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
				if (!array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array();
                }
                if (array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array(
					     'sub_menu_products_id'=> $row->sub_menu_products_id,
                        'sub_menu_products_name' => $row->sub_menu_products_name,
                        'sub_menu_products_navi' => $row->sub_menu_products_navi,
                        'sub_menu_products_image' => $row->sub_menu_products_image,
                    );
                    array_push($data_value, $data[$row->sub_menu_products_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by cat id
     */
    public function get_list_of_products_by_cat_id($cat_id) {
	
        $db_result = $this->db->get_where('mayas_product', array('status' => 'active','product_categories'=>$cat_id ));
		if ($db_result && $db_result->num_rows() > 0) {
			$data = array();
			$data_value = array();
			foreach($db_result->result() as $row){
		        if(!array_key_exists($row->product_id, $data)) {
				    $data[$row->product_id] = array ();
					}
				if (array_key_exists($row->product_id, $data)){ 
				     $data[$row->product_id] = array(
					 'product_id'=> $row->product_id,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'product_price_ind'=> $row->	product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_description'=> $row->product_description,
					 'product_image_list'=> $row->product_image_list,
					 );
			         array_push($data_value, $data[$row->product_id]); 
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}
	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by product id
     */
    public function get_detail_of_products_by_product_id($product_id) {
        $db_result = $this->db->get_where('mayas_product', array('status' => 'active','product_id'=>$product_id ));
		if ($db_result && $db_result->num_rows() > 0) {
		$data = array();
		$data_value = array();
		foreach($db_result->result() as $row){
		        if(!array_key_exists($row->product_id, $data)) {
				    $data[$row->product_id] = array ();
					}
				if (array_key_exists($row->product_id, $data)){ 
				     $data[$row->product_id] = array(
					 'product_id'=> $row->product_id,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'product_description'=> $row->product_description,
					 'product_price_ind'=> $row->	product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_feature'=> $row->product_feature,
					 'product_weight'=> $row->product_weight,
					 'product_shape'=> $row->product_shape,
					 'product_color'=> $row->product_color,
					 'product_clarity'=> $row->product_clarity,
					 'product_grade'=> $row->product_grade,
					 'product_cut'=> $row->product_cut,
					 'product_treatment'=> $row->product_treatment,
					 'product_origin'=> $row->product_origin,
					 'product_other_info'=> $row->product_other_info,
					 'product_other_info2'=> $row->product_other_info2,
					 'product_other_info3'=> $row->product_other_info3,
					 'product_image_list'=> $row->product_image_list,
					 );
			         array_push($data_value, $data[$row->product_id]); 
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}		//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function product_order_buy_data() {
      // var_dump($_POST['product_detail']);
        $data = array(
             'order_id' => $_POST['order_id'],
            'payment_id' => $_POST['payment_id'],
			'email' => $_POST['email'],
            'name' => $_POST['name'],
            'address' => $_POST['address'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contectno' => $_POST['contectno'],
            'itemname' => $_POST['productname'],
            'itemcost' => $_POST['productcost'],
            'subtotal' => $_POST['subtotal'],
            'discount' => $_POST['discount'],
            'grandtotal' => $_POST['grandtotal'],
            'payment_date' => $_POST['date'],
            'item_type' => 'product',
        );
		

         $result = $this->db->insert('mayas_order_payment', $data);
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
				/*inserting in profile table*/
				$data2 = array(
					'FK_order_payment_id' => $insert_id,
					'product_id' => $_POST['product_detail'][0]['product_id'],
					'product_price_ind' => $_POST['product_detail'][0]['product_price_ind'],
					'product_price_usd' => $_POST['product_detail'][0]['product_price_usd'],
					'product_image_list' => $_POST['product_detail'][0]['product_image_list'],
					'product_categories' => $_POST['product_detail'][0]['product_categories'],
					'qty' => $_POST['product_detail'][0]['qty1'],
					'product_name' => $_POST['product_detail'][0]['product_name'],
				
				);
				
					$result2 = $this->db->insert('mayas_payment_product', $data2);
				
				
				
			} 
         if ($result) {
			/*sending the mail to admin*/
			#send a email to admin
			$this->load->library('email');			
			$subject = "Welcome to ".WEBSITE_NAME;
			
			$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
			$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
			$message .= 'Dear Admin,<br/><br/>';
			$message .= '<table><tbody><tr><td>A new consultation came</td></tr>';
			$message .= '<tr><td><p>The Detail as below </p></td></tr>';
			$message .= '<tr><td><p>Consultant Type : <strong>'.$_POST['consultantType'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Name : <strong>'.$_POST['name'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Address : <strong>'.$_POST['address'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>City : <strong>'.$_POST['city'].'</strong></p></td></tr>';
			
			$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['country'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Birth State : <strong>'.$_POST['state'].'</strong></p></td></tr>';
			
			$message .= '<tr><td><p>Email ID : <strong>'.$_POST['email'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['contectno'].'</strong></p></td></tr>';
			
			
			$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
			$message .= '</div></div>';
			//print_r($message);
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$to = COMPANY_EMAIL_ID;
			$this->email->initialize($e_config);
			$this->email->from($_POST['email'], $_POST['name']);
			$this->email->to($to); 
			$this->email->subject($subject);
			$this->email->message($message);	
			$this->email->send();
			
            return TRUE;
        }  else {
            return FALSE;
        }
    }
		//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function service_order_buy_data_instantcon() {
		//var_dump( $_POST['sercice_detail'][0]['consultantType']);
		
		
			$email = $_POST['sercice_detail_insatance'][0]['inputEmail'];
			$name = $_POST['sercice_detail_insatance'][0]['inputName'];
		$db_result1 = $this->db->get_where('mayas_order_payment', array('order_id' => $_POST['order_id']));
		if ($db_result1 && $db_result1->num_rows() > 0) {
			$rowss = $db_result1->result();
			$primary = ($rowss[0]->order_payment_id).' - '.(substr($_POST['order_id'] , -4)) ;
            return $primary ;
           $insert_id =0 ;
		}else{
			if($_POST['coupan_code'] != ''){
				$db_result15 = $this->db->get_where('mayas_coupan_code', array('coupan_code' => $_POST['coupan_code']));
				$row15 = $db_result15->num_rows();
				$no_user = (int)$row15[0]->appyer_count ;
				$code_id = (int)$row15[0]->code_id ;
				$no_user++;
				$this->db->query("UPDATE mayas_coupan_code SET  appyer_count = '$no_user' where code_id = '$code_id' ");
			}
			
			$data = array(
				'order_id' => $_POST['order_id'],
				'payment_id' => $_POST['payment_id'],
				'itemname' => $_POST['servicename'],
				'itemcost' => $_POST['servicecost'],
				
				'payment_method' => $_POST['payment_method'],
				'currency' => $_POST['currency'],
				'subtotal' => $_POST['subtotal'],
				'discount' => $_POST['discount'],
				'grandtotal' => $_POST['grandtotal'],
				'coupan_code' => $_POST['coupan_code'],
				'payment_date' => $_POST['date'],
				'email' => $email,
				'name' => $name,
				
				'contectno' => $_POST['contectno'],
				'item_type' => 'service',
				 
			);
		
			$result = $this->db->insert('mayas_order_payment', $data);
			$insert_id = $this->db->insert_id();
		}
		if ($insert_id != 0) {
			
			
				
					/*inserting in profile table*/
					$data2 = array(
					
					
						'FK_order_payment_id' => $insert_id,
						'consultantType' =>  $_POST['sercice_detail_insatance'][0]['consultantType'],
						'full_name' => $_POST['sercice_detail_insatance'][0]['inputName'],
						'gender' => $_POST['sercice_detail_insatance'][0]['inputGender'],
						'inputDateDate' => $_POST['sercice_detail_insatance'][0]['inputDateDate'],
						'inputTimeMin' => $_POST['sercice_detail_insatance'][0]['inputTimeMin'],
						'birthCountry' => $_POST['sercice_detail_insatance'][0]['birthCountry'],
						'birthState' => $_POST['sercice_detail_insatance'][0]['birthState'],
						'birthCity' => $_POST['sercice_detail_insatance'][0]['birthCity'],
						'inputEmail' => $_POST['sercice_detail_insatance'][0]['inputEmail'],
						'inputContact' => $_POST['sercice_detail_insatance'][0]['inputContact'],
						'questiontime' => $_POST['sercice_detail_insatance'][0]['questiontime'],
						'questionyaminute' => $_POST['sercice_detail_insatance'][0]['questionyaminute'],
						'amount' => $_POST['servicecost'],
						
					);
					$result2 = $this->db->insert('mayas_payment_service', $data2);
					
					$data3 = array(
						'consultantType' =>  $_POST['sercice_detail_insatance'][0]['consultantType'],
						'full_name' => $_POST['sercice_detail_insatance'][0]['inputName'],
						'gender' => $_POST['sercice_detail_insatance'][0]['inputGender'],
						'inputDateDate' => $_POST['sercice_detail_insatance'][0]['inputDateDate'],
						'inputTimeMin' => $_POST['sercice_detail_insatance'][0]['inputTimeMin'],
						'birthCountry' => $_POST['sercice_detail_insatance'][0]['birthCountry'],
						'birthState' => $_POST['sercice_detail_insatance'][0]['birthState'],
						'birthCity' => $_POST['sercice_detail_insatance'][0]['birthCity'],
						'inputEmail' => $_POST['sercice_detail_insatance'][0]['inputEmail'],
						'inputContact' => $_POST['sercice_detail_insatance'][0]['inputContact'],
						'questiontime' => $_POST['sercice_detail_insatance'][0]['questiontime'],
						'questionyaminute' => $_POST['sercice_detail_insatance'][0]['questionyaminute'],
						'amount' => $_POST['servicecost'],
						'paymentmethod' => 'paid',
						'order_id' => $_POST['payment_id'],
						
					);
					$result2 = $this->db->insert('mayas_consultation', $data3);
					
					
					
			} 
			if($result){
				
				 $this->load->library('email');			
				$subject = $_POST['payment_id'];
				
						$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
						$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
						$message .= 'Dear,<br/><br/>';
						$message .= '<table><tbody><tr><td>A new consultation came</td></tr>';
						$message .= '<tr><td><p>The Detail as below </p></td></tr>';
						$message .= '<tr><td><p>Consultation Code : <strong>'.$insert_id.' - '.(substr($_POST['order_id'] , -4)).'</strong></p></td></tr>';
						$message .= '<tr><td><p>Consultant Type : <strong>'.$_POST['sercice_detail_insatance'][0]['consultantType'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Paid Amount : <strong> ₹ '.((Float)$_POST['subtotal'] - (Float)$_POST['discount']).'</strong></p></td></tr>';
						$message .= '<tr><td><p>Order ID : <strong>'.$_POST['payment_id'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Name : <strong>'.$_POST['sercice_detail_insatance'][0]['inputName'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Gender : <strong>'.$_POST['sercice_detail_insatance'][0]['inputGender'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Date of Birth : <strong>'.date('j F, Y',strtotime($_POST['sercice_detail_insatance'][0]['inputDateDate'])).'</strong></p></td></tr>';
						$message .= '<tr><td><p>Time of Birth : <strong>'.$_POST['sercice_detail_insatance'][0]['inputTimeMin'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['sercice_detail_insatance'][0]['birthCountry'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth State : <strong>'.$_POST['sercice_detail_insatance'][0]['birthState'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth City : <strong>'.$_POST['sercice_detail_insatance'][0]['birthCity'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Email ID : <strong>'.$_POST['sercice_detail_insatance'][0]['inputEmail'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['sercice_detail_insatance'][0]['inputContact'].'</strong></p></td></tr>';
					
						$message .= '<tr><td><p>'.$_POST['sercice_detail_insatance'][0]['questiontime'].' : <strong>'.$_POST['sercice_detail_insatance'][0]['questionyaminute'].'</strong></p></td></tr>';
						$message .= '<tr><td><p> Cost : <strong>'.$_POST['servicecost'].'</strong></p></td></tr></tbody></table>';
						$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
						$message .= '</div></div>';
						
				 
				
				
				
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to = COMPANY_EMAIL_ID;
				$to1 = COMPANY_EMAIL_ID_OTHER;
				$this->email->initialize($e_config);
				$this->email->from($_POST['sercice_detail_insatance'][0]['inputEmail'], $_POST['sercice_detail_insatance'][0]['inputName']);
				$this->email->to($to); 
				$this->email->cc($to1); 
				$this->email->subject($subject);
				$this->email->message($message);	
				$this->email->send();
			
				 
				
				 $this->load->library('email');			
				$subject = "Welcome to ".WEBSITE_NAME;
				
						$message1 = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
						$message1 .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
						$message1 .= 'Hello '.$_POST['sercice_detail_insatance'][0]['inputName'].',<br/><br/>';

						$message1 .= '<table><tbody>';
						$message1 .= '<tr><td>You have successfully submitted your details for Instant Consultation (₹ '.$_POST['servicecost'].').</td></tr>';
						$message1 .= '<tr><td><p>Your consultation access code is : <strong>'.$insert_id.' - '.(substr($_POST['payment_id'] , -4)).'</strong></p></td></tr>';
						$message1 .= '<tr><td><p>To obtain/ fix your consultation, call me on:+91-9829265640    (Timings: 11 to 8 p.m.   India Time)<br/><br/></p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Warm Regards</p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Astro-Consultant  </p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">+91- 9829265640</p></td></tr>';
						$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">MayasAstrology.com</p></td></tr>';
						
						
						
					
						$message1 .= '</tbody></table>';
						$message1 .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
						$message1 .= '</div></div>';
						
				
				
				
				
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to11 = 'MayasAstrology';
				$to = $email;
				$to1 = COMPANY_EMAIL_ID_OTHER;
				$this->email->initialize($e_config);
				$this->email->from(COMPANY_EMAIL_ID, $to11);
				$this->email->to($to); 
				$this->email->cc($to1); 
				$this->email->subject($subject);
				$this->email->message($message1);
						
				//print_r($this->email->message($message1));
				$this->email->send();
				
				$primary = $insert_id.' - '.(substr($_POST['order_id'] , -4)) ;
				return $primary ;
				  
			}else {
				return FALSE;
			}
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function vastuservice_order_buy_data() {
		$result = $this->get_consultation_massages();
		$date=date('Y-m-d');
		if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			$consultation_massages=$result[0]['custom_message'];
		}else{
			$consultation_massages=$result[0]['paid_massage'];
		}
		
		$db_result1 = $this->db->get_where('mayas_vastu_consultation', array('vastu_id' => $_POST['order_payment_id']));
		if ($db_result1 && $db_result1->num_rows() > 0) {
			$rowss = $db_result1->row();
			$primary = ($rowss->vastu_id).' - '.(substr($rowss->order_payment_id , -4)) ;
            //return $primary ;
				$insert_id=$rowss->vastu_id.' - '.(substr($rowss->order_payment_id , -4));
				$this->load->library('email');			
				$subject = $rowss->order_payment_id;
				
				$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
				$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
				$message .= 'Dear Admin,<br/><br/>';
				$message .= '<table><tbody><tr><td>A new vastu related user came</td></tr>';
				$message .= '<tr><td><p>The Detail as below </p></td></tr>';
				$message .= '<tr><td><p>Consultation Code : <strong>'.$rowss->vastu_id.' - '.(substr($rowss->order_payment_id , -4)).'</strong></p></td></tr>';
				$message .= '<tr><td><p>Consultant Type : <strong>'.$rowss->consultantType.'   </strong></p></td></tr>';
				$message .= '<tr><td><p>Paid Amount : <strong> ₹ '.((Float)$rowss->amount).'</strong></p></td></tr>';
				$message .= '<tr><td><p>Name : <strong>'.$rowss->inputName.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Gender : <strong>'.$rowss->inputGender.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Date of Birth : <strong>'.date('j F, Y',strtotime($rowss->inputDateDate)).' </strong></p></td></tr>';
				$message .= '<tr><td><p>Time of Birth : <strong>'.$rowss->inputTimeMin.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth Country : <strong>'.$rowss->birthCountry.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth State : <strong>'.$rowss->birthState.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth City : <strong>'.$rowss->birthCity.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Email ID : <strong>'.$rowss->inputEmail.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Contact NO. : <strong>'.$rowss->inputContact.'</strong></p></td></tr>';

			
				
				$message .= '</tbody></table>';
				$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
				$message .= '</div></div>';
				//print_r($message);
				/*
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to = COMPANY_EMAIL_ID;
				$this->email->initialize($e_config);
				$this->email->from($rowss->inputEmail, $rowss->inputName);
				$this->email->to($to); 
				$this->email->subject($subject);
				$this->email->message($message);
				//print_r($this->email->message($message));	
				if(base_url() !="http://localhost/mayas1/"){			
					$this->email->send();
				}*/
				
				$this->load->model('Email_model', 'obj_email', TRUE);
				$data11 = array(
					'subject' => 'Consultation Code: '.$subject,
					'to' => PROJECT_website_EMAIL,
					'to_name' => PROJECT_NAME,
					'from' => PROJECT_HR_EMAIL,
					'from_name' => PROJECT_NAME,
					'msg' => $message,
				);
				//var_dump($data11);
				$result1 = $this->obj_email->send_mail_in_sendinblue($data11);
			
			
				$this->load->model('Email_model','obj_mail', TRUE);					
				$emailresult = $this->obj_mail->sendemail_paid_consultation(1,$rowss->inputName,$rowss->inputEmail,$insert_id,$rowss->consultantType, $rowss->consultantType ,(Float)$rowss->amount,NOREPLY_EMAIL);
				
				
				return $primary;
			
        } else {
            return FALSE;
        }
       
    }
    public function service_order_buy_data($orderpayment) {
		$result = $this->get_consultation_massages();
		$date=date('Y-m-d');
		if($date>=$result[0]['start_date'] && $date<=$result[0]['end_date']){
			$consultation_massages=$result[0]['custom_message'];
		}else{
			$consultation_massages=$result[0]['paid_massage'];
		}
		//var_dump($result);
		//var_dump($consultation_massages);
		$db_result1 = $this->db->get_where('mayas_order_payment', array('order_payment_id' => $orderpayment));
		$sercice_detail = $this->db->get_where('mayas_payment_service', array('FK_order_payment_id' => $orderpayment))->row();
		if ($db_result1 && $db_result1->num_rows() > 0) {
			$rowss = $db_result1->row();
			$primary = ($rowss->order_payment_id).' - '.(substr($rowss->order_id , -4)) ;
            //return $primary ;
				$insert_id=$rowss->order_payment_id.' - '.(substr($rowss->order_id , -4));
				$this->load->library('email');			
				$subject = $rowss->order_id;
				
				$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
				$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
				$message .= 'Dear Admin,<br/><br/>';
				$message .= '<table><tbody><tr><td>A new consultation came</td></tr>';
				$message .= '<tr><td><p>The Detail as below </p></td></tr>';
				$message .= '<tr><td><p>Consultation Code : <strong>'.$rowss->order_payment_id.' - '.(substr($rowss->order_id , -4)).'</strong></p></td></tr>';
				$message .= '<tr><td><p>Consultant Type : <strong>'.$rowss->itemname.'  (Actual amount ₹ '.$rowss->itemcost.') </strong></p></td></tr>';
				$message .= '<tr><td><p>Paid Amount : <strong> ₹ '.((Float)$rowss->subtotal - (Float)$rowss->discount).'</strong></p></td></tr>';
				$message .= '<tr><td><p>Name : <strong>'.$sercice_detail->full_name.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Gender : <strong>'.$sercice_detail->gender.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Date of Birth : <strong>'.date('j F, Y',strtotime($sercice_detail->inputDateDate)).' </strong></p></td></tr>';
				$message .= '<tr><td><p>Time of Birth : <strong>'.$sercice_detail->inputTimeMin.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth Country : <strong>'.$sercice_detail->birthCountry.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth State : <strong>'.$sercice_detail->birthState.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Birth City : <strong>'.$sercice_detail->birthCity.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Email ID : <strong>'.$sercice_detail->inputEmail.'</strong></p></td></tr>';
				$message .= '<tr><td><p>Contact NO. : <strong>'.$sercice_detail->inputContact.'</strong></p></td></tr>';
				if($sercice_detail->question1){
					$message .= '<tr><td><p>'.$sercice_detail->question1_label.' : <strong>'.$sercice_detail->question1.'</strong></p></td></tr>';
				}
				if($sercice_detail->question2){
					$message .= '<tr><td><p>'.$sercice_detail->question2_label.' : <strong>'.$sercice_detail->question2.'</strong></p></td></tr>';
				}
				$message .= '<tr><td><p>'.$sercice_detail->question_label.' : <strong>'.$sercice_detail->inputQuestion.'</strong></p></td></tr>';
				if($sercice_detail->educationQualification){
					$message .= '<tr><td><p>Education Qualification : <strong>'.$sercice_detail->educationQualification.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Current Profession : <strong>'.$sercice_detail->currentProfession.'</strong></p></td></tr>';
				}
				if($sercice_detail->partnerName){
					$message .= '<tr><td><p>Partner Name : <strong>'.$sercice_detail->partnerName.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Partner Date of Birth : <strong>'.$sercice_detail->partnerDateDate.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Partner Time of Birth : <strong>'.$sercice_detail->partnerTimeMin.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Partner Birth Country : <strong>'.$sercice_detail->partnerBirthCountry.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Partner Birth State : <strong>'.$sercice_detail->partnerBirthState.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Partner Birth City : <strong>'.$sercice_detail->partnerBirthCity.'</strong></p></td></tr>';
				}
				
				$message .= '</tbody></table>';
				$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
				$message .= '</div></div>';
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to = COMPANY_EMAIL_ID;
				$this->email->initialize($e_config);
				$this->email->from($sercice_detail->inputEmail, $sercice_detail->full_name);
				$this->email->to($to); 
				$this->email->subject($subject);
				$this->email->message($message);
				//print_r($this->email->message($message));	
				if(base_url() !="http://localhost/mayas1/"){			
					$this->email->send();
				}
				
			
				$this->load->model('Email_model','obj_mail', TRUE);					
				$emailresult = $this->obj_mail->sendemail_paid_consultation(1,$sercice_detail->full_name,$sercice_detail->inputEmail,$insert_id,$sercice_detail->consultantType, $rowss->itemname ,$rowss->itemcost,NOREPLY_EMAIL, $rowss->payment_method);
				
				
				return $primary;
			
        } else {
            return FALSE;
        }
       
    }
	
	public function product_order_data($order_id,$email) {
		
		//var_dump($order_id);
		
		$db_result1 = $this->db->get_where('product_order', array('order_id' => $_POST['order_id']));
		$sercice_detail = $this->db->get_where('product_order_detail', array('order_id' => $_POST['order_id']))->row();
		//var_dump($sercice_detail);
	
		if ($db_result1 && $db_result1->num_rows() > 0) {
			$detail = $db_result1->result();
			if($detail){
				foreach($detail as $rowss){
					
					$primary = $order_id.' - '.(substr($rowss->transaction_id , -4)) ;
					//$primary = ($rowss->order_id).' - '.(substr($rowss->order_id , -4)) ;
					//return $primary ;
					//var_dump($rowss);
					
					$this->load->library('email');			
					$subject = $rowss->order_id;
					$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
					$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
					$message .= 'Dear Admin,<br/><br/>';
					$message .= '<table><tbody><tr><td>A new Order came</td></tr>';
					$message .= '<tr><td><p>The Detail as below </p></td></tr>';
					$message .= '<tr><td><p>Product name : '.$sercice_detail->product_name.'</p></td></tr>';
					$message .= '<tr><td><p>Paid Amount : <strong> ₹ '.((Float)$rowss->final_amount ).'</strong></p></td></tr>';
					$message .= '<tr><td><p>Name : <strong>'.$sercice_detail->first_name.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Email ID : <strong>'.$email.'</strong></p></td></tr>';
					$message .= '<tr><td><p>Contact NO. : <strong>'.$sercice_detail->phone_number.'</strong></p></td></tr>';
				
					$message .= '</tbody></table>';
					$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
					$message .= '</div></div>';
					//print_r($message);
					$e_config = array(
						'charset'=>'utf-8',
						'wordwrap'=> TRUE,
						'mailtype' => 'html',
						'priority' => '1'
					);
					$to = COMPANY_EMAIL_ID;
					$this->email->initialize($e_config);
					$this->email->from($email, $sercice_detail->first_name);
					$this->email->to($to); 
					$this->email->subject($subject);
					$this->email->message($message);
					//print_r($this->email->message($message));				
					$this->email->send();
					$this->load->library('email');			
					$subject = "Welcome to ".WEBSITE_NAME;
					
					$message1 = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
					$message1 .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
					$message1 .= 'Hello '. $sercice_detail->first_name.',<br/><br/>';

					$message1 .= '<table><tbody>';
					$message1 .= '<tr><td>You have successfully  placed your order  for product '.$sercice_detail->product_name.' and  price (₹ '.$rowss->final_amount.').</td></tr>';
					
					
					$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Warm Regards</p></td></tr>';
					$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p></td></tr>';
					$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">Astro-Consultant  </p></td></tr>';
					$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">+91- 9829265640</p></td></tr>';
					$message1 .= '<tr><td><p style="margin: 0px; padding: 0px;">MayasAstrology.com</p></td></tr>';
					
					$message1 .= '</tbody></table>';
					$message1 .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
					$message1 .= '</div></div>';
					
					
					
					
					
					//print_r($message1);
					$e_config = array(
						'charset'=>'utf-8',
						'wordwrap'=> TRUE,
						'mailtype' => 'html',
						'priority' => '1'
					);
					$to11 = 'MayasAstrology';
					$to = $email;
					$to1 = COMPANY_EMAIL_ID_OTHER;
					$this->email->initialize($e_config);
					$this->email->from(COMPANY_EMAIL_ID, $to11);
					$this->email->to($to); 
					//$this->email->cc($to1); 
					$this->email->subject($subject);
					$this->email->message($message1);
							
					//print_r($this->email->message($message1));
					$this->email->send();
					
					
					//$primary = $insert_id.' - '.(substr($_POST['order_id'] , -4)) ;
					return $primary;
				}
			}
		}else {
		 
			return FALSE;
		}
    
    }
	
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function service_order_buy_request_data() {
		
		$email='';
		$name='';
		
		$email = $_POST['sercice_detail'][0]['inputEmail'];
		$name = $_POST['sercice_detail'][0]['inputName'];
		$servicesnamefullname='';
		
		$db_result = $this->db->get_where('mayas_services', array('status' => 'active','service_id' => $_POST['service_id']));
        if ($db_result && $db_result->num_rows() > 0) {
            
            foreach ($db_result->result() as $row) {
				$servicesnamefullname = $row->services_name;
			}
		}
		$db_result1 = $this->db->get_where('mayas_order_payment', array('order_id' => $_POST['order_id']));
		if ($db_result1 && $db_result1->num_rows() > 0) {
			$rowss = $db_result1->result();
			$primary = ($rowss[0]->order_payment_id).' - '.(substr($_POST['order_id'] , -4)) ;
            return $primary ;
          
		}else{ 
			if($_POST['coupan_code'] != ''){
				$db_result15 = $this->db->get_where('mayas_coupan_code', array('coupan_code' => $_POST['coupan_code']));
				$row15 = $db_result15->num_rows();
				//$no_user = (int)$row15[0]->appyer_count ;
				//$code_id = (int)$row15[0]->code_id ;
				//$no_user++;
				//$this->db->query("UPDATE mayas_coupan_code SET  appyer_count = '$no_user' where code_id = '$code_id' ");
			}
			
			$data = array(
				//'order_id' => $_POST['order_id'],
				'payment_id' => $_POST['order_id'],
				'itemname' => $_POST['servicename'],
				'payment_method' => $_POST['payment_method'],
				'currency' => $_POST['currency'],
				'itemcost' => $_POST['servicecost'],
				'subtotal' => $_POST['subtotal'],
				'discount' => $_POST['discount'],
				'grandtotal' => $_POST['grandtotal'],
				'coupan_code' => $_POST['coupan_code'],
				'coupon_discount' => $_POST['coupon_discount'],
				'coupon_discount_amount' => $_POST['coupon_discount_amount'],
				'payment_date' => $_POST['date'],
				'email' => $email,
				'name' => $name,
				'address' => $_POST['address'],
				'city' => $_POST['city'],
				'state' => $_POST['state'],
				'country' => $_POST['country'],
				'contectno' => $_POST['contectno'],
				'item_type' => 'service',
				 
			);
		
			$result = $this->db->insert('mayas_order_payment', $data);
				
			$insert_id = $this->db->insert_id();
			if ($insert_id) {
			
			
				if(isset($_POST['sercice_detail'])){
					
					/*inserting in profile table*/
					$data2 = array(
						'FK_order_payment_id' => $insert_id,
						//'consultantType' =>  $servicesnamefullname,
						'consultantType' => $_POST['sercice_detail'][0]['consultantType'],
						'full_name' => $_POST['sercice_detail'][0]['inputName'],
						'gender' => $_POST['sercice_detail'][0]['inputGender'],
						'inputDateDate' => $_POST['sercice_detail'][0]['inputDateDate'],
						'inputTimeMin' => $_POST['sercice_detail'][0]['inputTimeMin'],
						'birthCountry' => $_POST['sercice_detail'][0]['birthCountry'],
						'birthState' => $_POST['sercice_detail'][0]['birthState'],
						'birthCity' => $_POST['sercice_detail'][0]['birthCity'],
						'inputEmail' => $_POST['sercice_detail'][0]['inputEmail'],
						'inputContact' => $_POST['sercice_detail'][0]['inputContact'],
						'inputQuestion' => $_POST['sercice_detail'][0]['inputQuestion'],
						'question1' => $_POST['sercice_detail'][0]['question1'],
						'question2' => $_POST['sercice_detail'][0]['question2'],
						//'otherInfomation' => $_POST['sercice_detail'][0]['otherInfomation'],
						
						'educationQualification' => $_POST['sercice_detail'][0]['educationQualification'],
						'currentProfession' => $_POST['sercice_detail'][0]['currentProfession'],
						'partnerName' => $_POST['sercice_detail'][0]['partnerName'],
						'partnerDateDate' => $_POST['sercice_detail'][0]['partnerDateDate'],
						'partnerTimeMin' => $_POST['sercice_detail'][0]['partnerTimeMin'],
						'partnerBirthCountry' => $_POST['sercice_detail'][0]['partnerBirthCountry'],
						'partnerBirthState' => $_POST['sercice_detail'][0]['partnerBirthState'],
						'partnerBirthCity' => $_POST['sercice_detail'][0]['partnerBirthCity'],
						'single_or_married' => $_POST['sercice_detail'][0]['single_or_married'],
						'question_label' => $_POST['sercice_detail'][0]['question_label'],
						'question1_label' => $_POST['sercice_detail'][0]['question1_label'],
						'question2_label' => $_POST['sercice_detail'][0]['question2_label'],
					);
					$result2 = $this->db->insert('mayas_payment_service', $data2);
				}
				
				
				$primary = $insert_id.' - '.(substr($_POST['order_id'] , -4)) ;
				return $primary;
			
			} else {
				return FALSE;
			}
			
		} 
       
    }
			//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_donate_data() {
		
        $data = array(
            'order_id' => $_POST['order_id'],
            'payment_id' => $_POST['payment_id'],
            'payment_method' => $_POST['payment_method'],
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'contectno' => $_POST['contectno'],
            'servicename' => $_POST['servicename'],
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
            'full_response_data' => $_POST['full_response_data'],
            'inputQuestion' => $_POST['inputQuestion'],
            'inputDateDate' => $_POST['inputDateDate'],
            'inputTimeMin' => $_POST['inputTimeMin'],
            'birthCountry' => $_POST['birthCountry'],
            'birthState' => $_POST['birthState'],
            'birthCity' => $_POST['birthCity'],
            'date' => date('Y-m-d'),
            'time' => date('H:m:s'),
            
        );
		
		

        $result = $this->db->insert('mayas_donate', $data);
		
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			
				 
				 $this->load->library('email');			
				$subject = "Welcome to ".WEBSITE_NAME;
				
						$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
						$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
						$message .= 'Dear Admin,<br/><br/>';
						$message .= '<table><tbody><tr><td>A new Donate came</td></tr>';
						$message .= '<tr><td><p>The Detail as below </p></td></tr>';
						//$message .= '<tr><td><p>Consultant Type : <strong>'.$_POST['sercice_detail_insatance'][0]['consultantType'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Order ID : <strong>'.$_POST['order_id'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Payment ID : <strong>'.$_POST['payment_id'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Name : <strong>'.$_POST['name'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Email ID : <strong>'.$_POST['email'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['contectno'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Date of Birth : <strong>'.$_POST['inputDateDate'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Time of Birth : <strong>'.$_POST['inputTimeMin'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['birthCountry'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth State : <strong>'.$_POST['birthState'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth City : <strong>'.$_POST['birthCity'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Question : <strong>'.$_POST['inputQuestion'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Amount : <strong>'.$_POST['currency'].' '.$_POST['amount'].'</strong></p></td></tr>';
						$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
						$message .= '</div></div>';
						
				
				
				
				
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				$to = COMPANY_EMAIL_ID;
				$to1 = COMPANY_EMAIL_ID_OTHER;
				$this->email->initialize($e_config);
				$this->email->from($_POST['email']);
				$this->email->to($to); 
				$this->email->cc($to1); 
				$this->email->subject($subject);
				$this->email->message($message);	
				$this->email->send();
				
				return TRUE;
				 
				 
				 
				 
				 
			 
        } 
			
			
			
			
        else {
            return FALSE;
        }
    }
	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function add_donate_data_paypal() {
		
        $data = array(
            'order_id' => $_POST['order_id'],
            'paypal_token' => $_POST['paypay_token'],
            'paypal_payer_id' => $_POST['paypal_payer_id'],
            'payment_method' => $_POST['payment_method'],
            'name' => $_POST['name'],
            'email' => $_POST['email'],
            'contectno' => $_POST['contectno'],
            'servicename' => $_POST['servicename'],
            'amount' => $_POST['amount'],
            'currency' => $_POST['currency'],
           'inputQuestion' => $_POST['inputQuestion'],
            'inputDateDate' => $_POST['inputDateDate'],
            'inputTimeMin' => $_POST['inputTimeMin'],
            'birthCountry' => $_POST['birthCountry'],
            'birthState' => $_POST['birthState'],
            'birthCity' => $_POST['birthCity'],
            'date' => date('Y-m-d'),
            'time' => date('H:m:s'),
            
        );
		
				 
		//var_dump($data);die();

        $result = $this->db->insert('mayas_donate', $data);
		
		$insert_id = $this->db->insert_id();
		if ($insert_id) {
			
				 
				 $this->load->library('email');			
				$subject = "Welcome to ".WEBSITE_NAME;
				
						$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
						$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a></div><br/><br/>';
						$message .= 'Dear Admin,<br/><br/>';
						$message .= '<table><tbody><tr><td>A new Donate came</td></tr>';
						$message .= '<tr><td><p>The Detail as below </p></td></tr>';
						//$message .= '<tr><td><p>Consultant Type : <strong>'.$_POST['sercice_detail_insatance'][0]['consultantType'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Order ID : <strong>'.$_POST['order_id'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Payment ID : <strong>'.$_POST['paypal_payer_id'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Name : <strong>'.$_POST['name'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Email ID : <strong>'.$_POST['email'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Contact NO. : <strong>'.$_POST['contectno'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Date of Birth : <strong>'.$_POST['inputDateDate'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Time of Birth : <strong>'.$_POST['inputTimeMin'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth Country : <strong>'.$_POST['birthCountry'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth State : <strong>'.$_POST['birthState'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Birth City : <strong>'.$_POST['birthCity'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Question : <strong>'.$_POST['inputQuestion'].'</strong></p></td></tr>';
						$message .= '<tr><td><p>Amount : <strong> '.$_POST['currency'].' '.$_POST['amount'].'</strong></p></td></tr>';
						$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
						$message .= '</div></div>';
						
				
				
				
				
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
						$to = COMPANY_EMAIL_ID;
						$to1 = COMPANY_EMAIL_ID_OTHER;
						$this->email->initialize($e_config);
						$this->email->from($_POST['email']);
						$this->email->to($to); 
						$this->email->cc($to1); 
						$this->email->subject($subject);
						$this->email->message($message);	
						$this->email->send();
				
				return TRUE;
				 
				 
				 
				 
				 
			 
        } 
			
			
			
			
        else {
            return FALSE;
        }
    }
		//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by product id
     */
    public function get_detail_of_products() {
		 $this->db->order_by('product_id', 'DESC');
		$this->db->limit('7');
        $db_result = $this->db->get_where('mayas_product', array('status' => 'active'));
		if ($db_result && $db_result->num_rows() > 0) {
		$data = array();
		$data_value = array();
		foreach($db_result->result() as $row){
		        if(!array_key_exists($row->product_id, $data)) {
				    $data[$row->product_id] = array ();
					}
				if (array_key_exists($row->product_id, $data)){ 
				     $data[$row->product_id] = array(
					 'product_id'=> $row->product_id,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'product_description'=> $row->product_description,
					 'product_price_ind'=> $row->	product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_feature'=> $row->product_feature,
					 'product_weight'=> $row->product_weight,
					 'product_shape'=> $row->product_shape,
					 'product_color'=> $row->product_color,
					 'product_clarity'=> $row->product_clarity,
					 'product_grade'=> $row->product_grade,
					 'product_cut'=> $row->product_cut,
					 'product_treatment'=> $row->product_treatment,
					 'product_origin'=> $row->product_origin,
					 'product_other_info'=> $row->product_other_info,
					 'product_other_info2'=> $row->product_other_info2,
					 'product_other_info3'=> $row->product_other_info3,
					 'product_image_list'=> $row->product_image_list,
					 );
			         array_push($data_value, $data[$row->product_id]); 
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}	
		 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all blogs
     */
    public function get_list_of_all_instance_consultation() {
        $db_result = $this->db->get_where('mayas_instance_consultation');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array();
                }
                if (array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array(
                        'instance_price_before' => $row->instance_price_before,
                        'instance_price_inr' => $row->instance_price_inr,
                        'instance_price_d' => $row->instance_price_d,
                        'instance_pic' => $row->instance_pic,
                        'instance_detail' => $row->instance_detail,
                        'instance_type' => $row->instance_type,
                        'instance_id' => $row->instance_id,
                        'instance_type_duration' => $row->instance_type_duration,
                       
                    );
                    array_push($data_value, $data[$row->instance_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all main blogs 1111
     */
    public function get_list_of_main_blogs_for_home_page() {
		
		// $db_result = $this->db->get_where('mayas_services', array('status' => 'active','service_id'=>$s_id));
		$this->db->limit(3);
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active','blog_choose'  => 'Yes'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
						'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'blog_writer' => $row->blog_writer,
						'date' =>  $row->date,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all bespoke services
     */
    public function get_list_of_all_bespoke_services() {
        $db_result = $this->db->get_where('mayas_bespoke_services', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,                                               
						'col' => $row->col,
                        'services_description' => $row->services_description,
                        'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
						'premium_type' => $row->premium_type,
						'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
		//-------------------------------------------------------------------------
	
	public function get_list_of_bespoke_services_by_id($s_id) {
		
        $db_result = $this->db->get_where('mayas_bespoke_services', array('status' => 'active','services_url'=>$s_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                         'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
						'services_description' => $row->services_description,
						'services_image' => $row->services_image,
						'service_detail_image' => $row->service_detail_image,
						'service_step_image' => $row->service_step_image,
                        'service_id' => $row->service_id,                        
                        'consultant_type' => $row->consultant_type,                     
						'premium_type' => $row->premium_type,
						'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
			
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//----------------------------
	public function leave_blog_comment() {
		
		$data = array(
		      'blog_id' => $_POST['blog_id'],
            'name' => $_POST['name'],
            'comment' => $_POST['comment'],
            'email' => $_POST['email'],                     
            'status' => 'active',
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
			
        );
		
			$result = $this->db->insert('mayas_blog_comments', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		
	
	}
		
	public function get_list_of_blog_comment_by_id($blog_id) {
		
		$db_result = $this->db->get_where('mayas_blog_comments',array('status !=' => 'old','blog_id'=>$blog_id));
		//print_r($this->db->last_query());
		//print_r($db_result->num_rows());
		//print_r($db_result);
        if ($db_result && $db_result->num_rows() > 0) {
			
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_comment_id, $data)) {
                    $data[$row->blog_comment_id] = array();
                }
                if (array_key_exists($row->blog_comment_id, $data)) {
                    $data[$row->blog_comment_id] = array(
					    'blog_id' => $row->blog_id,
                        'blog_comment_id' => $row->blog_comment_id,
						'name' => $row->name,
						'comment' => $row->comment,
						'email' => $row->email,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->blog_comment_id]);
                }
				
            }
			//print_r($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }

	public function get_services_name_id() {
        $db_result = $this->db->get_where('mayas_services', array('status' => 'active','service_id'=>$_POST['service_id']));
        if ($db_result && $db_result->num_rows() > 0) {
            $services_name='';
            foreach ($db_result->result() as $row) {              
					$services_name = $row->services_name;
            }
        }
    return $services_name;
    } 
    	
		
	public function signup_for_customer($return_value) {
		
		$db_result = $this->db->get_where('customer_account', array('customer_email' => $return_value['inputEmail']));
			if ($db_result->num_rows() > 0) {
				//var_dump($db_result->result());
				$user_data=$db_result->result();
				foreach($user_data as $row){
					$customer_id=$row->customer_id;			
				}
				$data = array(
					'customer_id' => $customer_id,
					'customer_email' =>$row->customer_email,
					'loggedIN' => 11,
					'first_name' => $row->first_name,
					'last_name' => $row->last_name,
					'phone_no' => "",
					'customer_img' =>'default_profile.jpg',
				);							
				$this->session->set_userdata($data);
				
				//echo sestion_set;
			}else{
				//var_dump('id  ni h');
				#generate random number
				$verify_code = rand(1111,99999999999);
				$verify_code2 = rand(1111,99999999999);
				$verify_code = sha1('verify' . (md5('verify' . $verify_code)));
				$datas = array(
					'first_name' => $return_value['full_name'],
					'last_name' => "",
					'country_name' => $return_value['birthCountry'],
					'state_name	' => $return_value['birthState'],
					'city_name' => $return_value['birthCity'],
					'customer_email' => $return_value['inputEmail'],
					'phone_no' => $return_value['inputContact'],				
					'zipcode' => "",				
					'customer_password'=> $verify_code2,				
					'verify_code' => $verify_code,
					'customer_img' => 'default_profile.jpg',
					'customer_status' => 'active',	
					'dummy_password'=>'yes',
					'created_date' => date('Y-m-d'),
					'created_time' => date('H:i:s'),
					
				);
				$result = $this->db->insert('customer_account', $datas);
				$insert_id = $this->db->insert_id();
				$data1 = array(
					'customer_id' => $insert_id,
					'first_name' => $return_value['full_name'],
					'last_name' => "",
					'country_name' => $return_value['birthCountry'],
					'state_name	' =>$return_value['birthState'],
					'city_name' =>$return_value['birthCity'],
					'shipping_phone_number' => $return_value['inputContact'],
					'address_line' => $_POST['inputContact'],
					'zip_code' => "",
					'is_selected' => '1',
					
				);
				$results = $this->db->insert('customer_address', $data1);
				
				$data = array(
					'customer_id' => $insert_id,
					'customer_email' =>$return_value['inputEmail'],
					'loggedIN' => 11,
					'first_name' => $return_value['full_name'],
					'last_name' => "",
					'phone_no' => $return_value['inputContact'],
					'customer_img' =>'default_profile.jpg',
				);							
				$this->session->set_userdata($data);
				
				$verification_link= base_url().'verify/vc?e='.$insert_id.'&v='.$verify_code;
				$content ='Dear '.$return_value['full_name'].' ,<br /><br /><p>Welcome and thank you for registering at <a href="'.PROJECT_URL.'">'.PROJECT_NAME.' </a></p><p>Your account has now been created and you can log in by Clicking the button bellow to confirm your email address</p><div style="text-align: center;margin: 20px;padding: 10px;"><a href="'.$verification_link.'" style="padding: 7px; background-color: #3b5998; margin: 10px;color: #fff;font-size: 14px;">CONFIRM</a></div>';
				$content ='<div>This is Your one Time Password you Can Use while Login For The First Time'.$verify_code2 .'</div>';
				
				$this->load->model('Email_model', 'obj_email', TRUE);
				$data11 = array(
					'subject' => 'Welcome to '.PROJECT_NAME,
					'to' => $return_value['inputEmail'],
					'to_name' => $return_value['full_name'],
					'from' => PROJECT_EMAIL,
					'msg' => $content,
				);
				$result = $this->obj_email->send_mail_in_sendinblue($data11);
				//var_dump($data11);
				//var_dump($result);
				//echo "ss";
			}
				
		
    }			
}
?>