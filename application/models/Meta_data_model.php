<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Meta_data_model extends CI_Model {

    
	
	
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_meta_data() {
        
		$meta_id = $_POST['meta_id'];
        $data = array(
            'url' => $_POST['url'],
            'title' => $_POST['title'],
            'dec_meta' => $_POST['dec_meta'],
            'keyword_meta' => $_POST['keyword_meta'],
        );
		if($meta_id == 0){
			$result = $this->db->insert('mayas_meta_data', $data);
        }else{
			$result = $this->db->update('mayas_meta_data', $data,array('meta_id'=>$meta_id));
		}
        if ($result) {
            return true;
        } else {
            return FALSE;
        }
    }
	
	
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_meta_data() {
        
		$meta_id = $_POST['meta_id'];
        
			$result = $this->db->delete('mayas_meta_data',array('meta_id'=>$meta_id));
		
        if ($result) {
            return true;
        } else {
            return FALSE;
        }
    }
	
	
	
	
	
	public function get_all_meta_data() {
        $db_result = $this->db->get_where('mayas_meta_data');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->meta_id, $data)) {
                    $data[$row->meta_id] = array();
                }
                if (array_key_exists($row->meta_id, $data)) {
                    $data[$row->meta_id] = array(
						'meta_id' => $row->meta_id,
						'url' => $row->url,
						'keyword_meta' => $row->keyword_meta,
                        'dec_meta' => $row->dec_meta,
                        'title' => $row->title,
                      				
                    );
                    array_push($data_value, $data[$row->meta_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	public function all_meta_data_by_id($meta_id) {
        $db_result = $this->db->get_where('mayas_meta_data',array('meta_id'=>$meta_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->meta_id, $data)) {
                    $data[$row->meta_id] = array();
                }
                if (array_key_exists($row->meta_id, $data)) {
                    $data[$row->meta_id] = array(
						'meta_id' => $row->meta_id,
						'url' => $row->url,
						'keyword_meta' => $row->keyword_meta,
                        'dec_meta' => $row->dec_meta,
                        'title' => $row->title,
                      				
                    );
                    array_push($data_value, $data[$row->meta_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
}