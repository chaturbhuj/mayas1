<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Comman_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }

	public function array_from_get($fields){
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->get($field);
        }
        return $data;
    }

	public function array_from_post($fields){
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field);
        }
        return $data;
    }

    public function get_query($string,$single=false,$arry=false){
		$query = $this->db->query($string);		
		if($arry){
			if($single == TRUE) {
				$method = 'row';
			}
			else {
				$method = 'result';
			}
		}
		else{
			if($single == TRUE) {
				$method = 'row_array';
			}
			else {
				$method = 'result_array';
			}
		}
		//echo $this->db->last_query();die;
		return $query->$method(); 
	}

    public function get_by($table,$where,$order = false, $single = FALSE,$result_type=false){
        if($order){
			foreach($order as $set =>$value){				
	            $this->db->order_by($set,$value);
			}
		}
		if($where){
	        $this->db->where($where);
		}
		
        return $this->get($table, $single,$result_type);
    }

    public function get($table,$single = FALSE,$result_type=false){
      	if($single == TRUE) {//get 1 row
            $method = 'row_array';
			if($result_type==true){//get by object
	            $method = 'row';
			}
        } else {//get multi row
            $method = 'result_array';
			if($result_type==true){//get by object
	            $method = 'result';
			}
        }
        return $this->db->get($table)->$method();
    }
	

    public function update_data($table,$data,$where){
		$this->db->set($data);
		$this->db->where($where);
		$this->db->update($table);
		
		return true;
    }
    public function insert_data($table,$data){
		$this->db->set($data);
		$this->db->insert($table);
		$last_id = $this->db->insert_id();		
		return $last_id;
    }
	function crop_edit_upload_image($name,$cdata){
		$error = TRUE;
		$crop_data = json_decode($cdata);

		$this->load->library('image_lib');
		$config = array(
			'allowed_types'     => 'jpg|jpeg|gif|png', //only accept these file types
			'max_size'          => 10240, //10MB max
			'upload_path'       => realpath(APPPATH.'../uploads') //upload directory
		);
		$new_name = time().$_FILES["avatar_file"]['name'];
		$new_name = preg_replace('/[^a-zA-Z0-9_.]/', '_', $new_name);
		$config['file_name'] = $new_name;

		$this->load->library('upload', $config);
		$data=array();
		if ( ! $this->upload->do_upload($name)){
			$data['message'] = $this->upload->display_errors('', '');
			$data['error'] = TRUE;
		}else{

			$image_data = $this->upload->data(); //upload the image
			//var_dump($image_data);
			$config = array(
				'source_image' => $image_data['full_path'],
				'maintain_ratio' => FALSE,
				'width' => (int)$crop_data->width,
				'height' => (int)$crop_data->height,
				'x_axis' => (int)$crop_data->x,
				'y_axis' => (int)$crop_data->y
			);
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->crop();

			$data['pic'] = $image_data['raw_name'].$image_data['file_ext'];
				
			
		}
		return $data;
	}
}
