<?php if (!defined('BASEPATH')) exit('Not A Valid Request');
include('3dParty/mailin.php');
class Email_model extends CI_Model {

    public function __construct() {
        parent::__construct();
		$this->load->library('email');
    }
	
    public function send_mail_in_sendinblue($data) {
		
		// $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', 'xsmtpsib-1f38a3181deca90548077ab7864d9655286a141fb53aba08c9aadabef74d1e5f-PgQAFcTJXR0jsMCk');
		$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

		$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
			new GuzzleHttp\Client(),
			$config
		);
		$header = '';//'<div><div style="background-color: #f8f9fa;padding: 20px;font-size: 20px;"><div style="text-align: center;border-bottom: 1px solid;"><a href="'.PROJECT_URL.'"><img src="'.LOGO.'" alt="'.PROJECT_NAME_MINI.'" style="width: 160px; margin-bottom: 10px;"></a></div><br/>';
		$header .= $data['msg'];
		$header .= '';//'<br/><p>Regards</p><p>'.PROJECT_NAME.' Team</p><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div></div></div>';
		
		$to_name = '';
		if(isset($data['to_name'])){
			$to_name = $data['to_name'];
		}
		$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
		$sendSmtpEmail['subject'] = $data['subject'];
		$sendSmtpEmail['htmlContent'] = $header;
		//$sendSmtpEmail['attachment'] = $data['attachment'];
		//$sendSmtpEmail['cc'] = array(
		//	array('email' => 'propmatee@gmail.com', 'name' => 'Propmate')
		//);
		$sendSmtpEmail['sender'] = array('name' => $data['from_name'], 'email' => $data['from']);
		$sendSmtpEmail['to'] = array(
			array('email' => $data['to'], 'name' => $to_name)
		);
		//	print_r($data['subject']);
		// $sendSmtpEmail['headers'] = array('Some-Custom-Name' => 'unique-id-1234');
		// $sendSmtpEmail['params'] = array('parameter' => 'My param value', 'subject' => 'New Subject');

		try {
			$result = $apiInstance->sendTransacEmail($sendSmtpEmail);
			return $result;
		} catch (Exception $e) {
			echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
		}
	}
	
    public function send_mail_in_sendinblue_api($data) {
    	$header = '<div><div style="background-color: #f8f9fa;padding: 20px;font-size: 20px;"><div style="text-align: center;border-bottom: 1px solid;"><a href="'.PROJECT_URL.'"><img src="'.LOGO.'" alt="'.PROJECT_NAME_MINI.'" style="width: 160px; margin-bottom: 10px;"></a></div><br/>';
		$content = $data['msg'];
		$footer ='<br/><p>Thanks</p><p>'.PROJECT_NAME.'.</p><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div></div></div>';
		
		//print_r($header.$content.$footer);die();

		$subject = $data['subject'];
		$message = $header.$content.$footer;
		$from = PROJECT_EMAIL;
		$to = $data['to'];
		$to_name = $data['to_name'];
			
		$message1 = str_replace('%[NAME]%', $to_name, $message);

		$mailin = new Mailin('https://api.sendinblue.com/v2.0', 'VLSOdWzUcZp38x2v', 5000);
        $data = array(
            "to" => array($to => $to_name),
            "from" => array(PROJECT_EMAIL, PROJECT_NAME_MINI),
            "subject" => $subject,
            "html" => $message
        );
        $response = $mailin->send_email($data);
        //var_dump($to);
		//var_dump($to_name);
        //var_dump($response);
        //var_dump($message);
        //var_dump($message1);
        return TRUE;
    }
    public function send_mail_in_sendinblue_with_change_data($data) {
    	$header = '<div><div style="background-color: #f8f9fa;padding: 20px;font-size: 20px;"><div style="text-align: center;border-bottom: 1px solid;"><a href="'.PROJECT_URL.'"><img src="'.LOGO.'" alt="'.PROJECT_NAME_MINI.'" style="width: 160px; margin-bottom: 10px;"></a></div><br/>';
		$content = $data['msg'];
		$footer ='<br/><p>Thanks</p><p>'.PROJECT_NAME.'.</p><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div></div></div>';
		
		//print_r($header.$content.$footer);die();
		//var_dump($data);
		$customer_id =$data['customer_id'];
		//var_dump($customer_id);
			$customer_data=$this->db->get_where('customer_account',array('customer_id'=>$customer_id))->result();
			//var_dump($customer_data);
			foreach($customer_data as $rows){
				$first_name=$rows->first_name;
				$last_name=$rows->last_name;
				$user_name =$first_name .' ' .$last_name;
				//var_dump($user_name);
			}
		$subject = $data['subject'];
		$message = $header.$content.$footer;
		$from = PROJECT_EMAIL;
		$to = $data['to'];
		$to_name = $data['to_name'];
			
		$message = str_replace('%[NAME]%', $user_name, $message);

		$mailin = new Mailin('https://api.sendinblue.com/v2.0', 'VLSOdWzUcZp38x2v', 5000);
        $data = array(
            "to" => array($to => $to_name),
            "from" => array(PROJECT_EMAIL, PROJECT_NAME_MINI),
            "subject" => $subject,
            "html" => $message
        );
        $response = $mailin->send_email($data);
        //var_dump($to);
		//var_dump($to_name);
        //var_dump($response);
        //var_dump($message);
        //var_dump($message);
        return TRUE;
    }

    public function send_mail_in_ci($data) {
		
		$header = '<div><div style="background-color: #f8f9fa;padding: 20px;font-size: 20px;"><div style="text-align: center;border-bottom: 1px solid;"><a href="'.PROJECT_URL.'"><img src="'.LOGO.'" alt="CSR" style="width: 160px; margin-bottom: 10px;"></a></div><br/>';
		$content = $data['msg'];
		$footer ='<br/><p>Thanks</p><p>'.PROJECT_NAME.'.</p><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div></div></div>';
		
		//print_r($header.$content.$footer);die();
		
		$subject = $data['subject'];
		$message = $header.$content.$footer;
		$from = PROJECT_EMAIL;
		$to = $data['to'];
		
		
        $e_config = array(
			'charset'=>'utf-8',
			'wordwrap'=> TRUE,
			'mailtype' => 'html',
			'priority' => '1'
		);
		
		$this->email->initialize($e_config);
		$this->email->from($from, PROJECT_NAME_MINI);
		$this->email->to($to); 
		$this->email->subject($subject);
		$this->email->message($message);	
		$result = $this->email->send();
		
		//var_dump($response);
		//$result = $this->email->send();
		//var_dump($result);
		//echo $this->email->print_debugger();
		//die();
		//var_dump($result);
		
		return TRUE;
	}	
	
	//free consultaion email function
	public function sendemail_free_consultation($message_id,$user_name, $user_email,$code,$Consultation,$s_email=null){
		//mail
		$temp_subject="Welcome to ".WEBSITE_NAME;
		$temp_body='<h2 style="color:#0033ff;">Dear '.$user_name.', <br/>
		You have successfully submitted your details for <br/>
" <span style="text-transform: uppercase;">'.$Consultation.'</span> " <br/>
Your Consultation Access Code is: '.$code.'
		</h2><br/>';
		if($s_email){
			$sender_email=$s_email;
		}else{
			$sender_email=WEBSITE_EMAIL;
		}
		$db_result1 = $this->db->get_where('consultation_massages',array('massage_id' => $message_id));
		
		//print_r($db_result1);
		if ($db_result1 && $db_result1->num_rows() > 0) {
			foreach ($db_result1->result() as $row1) {
				$custom_message=$row1->custom_message;
				$default_massage=$row1->default_massage;
				$start_date=$row1->start_date;
				$end_date=$row1->end_date;				
			}
			$date=date('Y-m-d');
			if($date>=$start_date && $date<=$end_date){
				$temp_body .= $custom_message;
			}else{
				$temp_body .= $default_massage;
			}
		
			$user_name= $user_name;
			$user_email= $user_email;			
	
			$login_link = base_url().'login';
			$this->load->library('email');	
			$subject = $temp_subject;
			$message = $temp_body;	
			$message = str_replace('%[NAME]%', $user_name, $message);
			$message = str_replace('%[Consultation]%', $Consultation, $message);
			$message = str_replace('%[code]%', $code, $message);	
			$message = str_replace('%[USER]%', $user_email, $message);						
			$message = str_replace('%[SITE_NAME]%', WEBSITE_NAME, $message);
			$message = str_replace('%[LOGIN_LINK]%', $login_link, $message);
			
			
			
				$header = '<div>
							<div style="padding: 20px;font-size: 20px;">
								<div style="text-align: center;">
									<a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a>
								</div>
							</div>
						</div>';
						
				
				$footer = '<div style="">
								<div>						
									<div>
										<p style="margin: 0px; padding: 0px;">Warm Regards</p>
										<p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p>
										<p style="margin: 0px; padding: 0px;">Astro-Consultant</p>
										<p style="margin: 0px; padding: 0px;">+91- 9829265640</p>
										<p style="margin: 0px; padding: 0px;">MayasAstrology.com</p>
									</div>
								<div style="border-top: 1px solid; text-align:center;"><span style="font-size: 16px; text-align:center; margin:10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>
						 </div></div>';
						
					
			//print_r($header.$message.$footer);die();
			$htmlContent=$header.$message.$footer;
			//print_r($htmlContent);
			
			// $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', 'xsmtpsib-1f38a3181deca90548077ab7864d9655286a141fb53aba08c9aadabef74d1e5f-PgQAFcTJXR0jsMCk');
			$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

			$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
				new GuzzleHttp\Client(),
				$config
			);
			
			//print_r($subject);
			$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
			$sendSmtpEmail['subject'] = $subject;
			$sendSmtpEmail['htmlContent'] = $htmlContent;
			//$sendSmtpEmail['attachment'] = $data['attachment'];
			//$sendSmtpEmail['cc'] = array(
			//	array('email' => 'erchatur.prajapat@gmail.com', 'name' => 'Mayas')
			//);
			$sendSmtpEmail['sender'] = array('name' => PROJECT_NAME, 'email' => PROJECT_website_EMAIL);
			$sendSmtpEmail['to'] = array(
				array('email' => $user_email, 'name' => $user_name)
			);
			// $sendSmtpEmail['headers'] = array('Some-Custom-Name' => 'unique-id-1234');
			// $sendSmtpEmail['params'] = array('parameter' => 'My param value', 'subject' => 'New Subject');

			try {
				$result = $apiInstance->sendTransacEmail($sendSmtpEmail); 
				return $result;
			} catch (Exception $e) {
				echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
			}
		//echo 'a';
			/*
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$this->email->initialize($e_config);
			$this->email->set_newline("\r\n");

			$this->email->to($user_email);
			//$this->email->to('sharma.ambika98765@gmail.com');
			$this->email->from($sender_email,'Mayas Astrology ');
			$this->email->subject($subject);
			$this->email->message($htmlContent);
			if(base_url() != 'http://localhost/mayas1/'){
				$aa = $this->email->send();
			}*/
		}
		return true;
    } 
	
	//free consultaion email function
	public function sendemail_paid_consultation($message_id,$user_name, $user_email,$code,$Consultation,$item,$cost,$s_email=null,$payment_method='Razorpay'){		
		//mail
		$temp_subject="Welcome to ".WEBSITE_NAME;
		$payment_sign= '₹ ';
		if($payment_method == 'Paypal'){
			$payment_sign= '$ ';
		}
		$temp_body='<h2 style="color:#0033ff;">Dear '.$user_name.', <br/>
			Your payemt is successful for '.$item.'  Consultation ('.$payment_sign.' '.$cost.')<br/>
			Your Consultation Access Code is: '.$code.'<br/>
		</h2>';
		if($s_email){
			$sender_email=$s_email;
		}else{
			$sender_email=WEBSITE_EMAIL;
		}
		$db_result1 = $this->db->get_where('consultation_massages',array('massage_id' => $message_id));
		
		//print_r($db_result1);
		if ($db_result1 && $db_result1->num_rows() > 0) {
			foreach ($db_result1->result() as $row1) {
				$custom_message=$row1->custom_message_paid;
				$paid_massage=$row1->paid_massage;
				$start_date=$row1->start_date;
				$end_date=$row1->end_date;				
			}
			$date=date('Y-m-d');
			if($date>=$start_date && $date<=$end_date){
				$temp_body .= $custom_message;
			}else{
				$temp_body .= $paid_massage;
			}
		
			$user_name= $user_name;
			$user_email= $user_email;			
	
			$login_link = base_url().'login';
			$this->load->library('email');	
			$subject = $temp_subject;
			$message = $temp_body;	
			$message = str_replace('%[NAME]%', $user_name, $message);
			$message = str_replace('%[item]%', $item, $message);
			$message = str_replace('%[cost]%', $cost, $message);
			$message = str_replace('%[Consultation]%', $Consultation, $message);
			$message = str_replace('%[code]%', $code, $message);	
			$message = str_replace('%[USER]%', $user_email, $message);						
			$message = str_replace('%[SITE_NAME]%', WEBSITE_NAME, $message);
			$message = str_replace('%[LOGIN_LINK]%', $login_link, $message);
			
			
			
				$header = '<div>
							<div style="padding: 20px;font-size: 20px;">
								<div style="text-align: center;">
									<a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 200px;"></a>
								</div>
							</div>
						</div>';
						
				
				$footer = '<div style="">
								<div>						
									<div>
										<p style="margin: 0px; padding: 0px;">Warm Regards</p>
										<p style="margin: 0px; padding: 0px;">Mayankesh Sharma</p>
										<p style="margin: 0px; padding: 0px;">Astro-Consultant</p>
										<p style="margin: 0px; padding: 0px;">+91- 9829265640</p>
										<p style="margin: 0px; padding: 0px;">MayasAstrology.com</p>
									</div>
								<div style="border-top: 1px solid; text-align:center;"><span style="font-size: 16px; text-align:center; margin:10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>
						 </div></div>';
						
					
			//print_r($header.$message.$footer);die();
			$htmlContent=$header.$message.$footer;
			//print_r($htmlContent);
			
			// $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', 'xsmtpsib-1f38a3181deca90548077ab7864d9655286a141fb53aba08c9aadabef74d1e5f-PgQAFcTJXR0jsMCk');
			$config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');

			$apiInstance = new SendinBlue\Client\Api\TransactionalEmailsApi(
				new GuzzleHttp\Client(),
				$config
			);
			
			$sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
			$sendSmtpEmail['subject'] = $subject;
			$sendSmtpEmail['htmlContent'] = $htmlContent;
			//$sendSmtpEmail['attachment'] = $data['attachment'];
			//$sendSmtpEmail['cc'] = array(
			//	array('email' => 'erchatur.prajapat@gmail.com', 'name' => 'Mayas')
			//);
			$sendSmtpEmail['sender'] = array('name' => PROJECT_NAME, 'email' => PROJECT_website_EMAIL);
			$sendSmtpEmail['to'] = array(
				array('email' => $user_email, 'name' => $user_name)
			);
			// $sendSmtpEmail['headers'] = array('Some-Custom-Name' => 'unique-id-1234');
			// $sendSmtpEmail['params'] = array('parameter' => 'My param value', 'subject' => 'New Subject');

			try {
				$result = $apiInstance->sendTransacEmail($sendSmtpEmail); 
				return $result;
			} catch (Exception $e) {
				echo 'Exception when calling TransactionalEmailsApi->sendTransacEmail: ', $e->getMessage(), PHP_EOL;
			}
			
			
			/*
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			$this->email->initialize($e_config);
			$this->email->set_newline("\r\n");

			$this->email->to($user_email);
			//$this->email->to('sharma.ambika98765@gmail.com');
			$this->email->from($sender_email,'Mayas Astrology ');
			$this->email->subject($subject);
			$this->email->message($htmlContent);
			if(base_url() != 'http://localhost/mayas1/'){
				$aa = $this->email->send();
			}*/
		}
    }
	
}