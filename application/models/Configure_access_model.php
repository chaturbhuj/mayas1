<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Configure_access_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }



	/*
	*Updating  user password
	*
	*/
	public function change_user_password(){
		#Generate hashed password.
        $old_hashed_password = sha1('admin' . (md5('admin' . $_POST['old_password'])));
		$user_id = $this->session->userdata('userID');
		$this->db->where(array('password_hash' => $old_hashed_password,'user_id' => $user_id));
		$res = $this->db->get('mayas_users');
		/*print_r($res->num_rows());
		echo "</br>";*/
		if($res && $res->num_rows() == 1){
			/*echo "2";*/
			$new_hashed_password = sha1('admin' . (md5('admin' . $_POST['new_password'])));
			$data = array(
				'password_hash' => $new_hashed_password,
			);

			$result = $this->db->update('mayas_users', $data, array('user_id' => $user_id));
			if($result){
				/*echo "ok";*/
				return true;
			}else{
				/*echo "not ok";*/
				return false;
			}
		}else{
			/*echo "3";*/
			return false;
		}

	}


    //-------------------------------------------------------------------------
    /*
     * This function is used to check availability of email etc.
     */
    public function check_availability() {
        if ($_POST['param'] == 'users') {
            $db_result = $this->db->get_where('mayas_users', array('email' => $_POST['user_email']));
        }
        if ($db_result && $db_result->num_rows() > 0) {
            #param already exists
            return FALSE;
        } else {
            return TRUE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * add new_user_into_database into databse
     */
    public function add_new_user() {
        date_default_timezone_set('Asia/Kolkata');
        #Generate hashed password.
        $hashed_password = sha1('admin' . (md5('admin' . $_POST['user_pass'])));

        #generate 16 digit random number
        $verify_code = random_string('alnum', 16);
        $data = array(
            'email' => $_POST['user_email'],
            'username' => $_POST['user_name'],
            'userrole' => $_POST['assign_role_to_user'],
            'password_hash' => $hashed_password,
            'verify_code' => $verify_code,
            'created_date' => date('Y-m-d'),
            'created_time' => date('H:m:s'),
        );

        $verify_url = base_url() . 'configure_access/verify_url/' . $verify_code;
        $result = $this->db->insert('mayas_users', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all user for login
     */
    public function get_list_of_user() {
        $db_result = $this->db->get_where('mayas_users', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                        'email' => $row->email,
                        'username' => $row->username,
                        'userrole' => $row->userrole,
                        'user_id' => $row->user_id,
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to edit user information
     */
    public function edit_user_information() {
        $data = array(
            'email' => $_POST['edit_user_email'],
            'username' => $_POST['edit_user_name'],
            'userrole' => $_POST['edit_assign_role_to_user'],
        );
        $result = $this->db->update('mayas_users', $data, array('user_id' => $_POST['edit_user_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove user information from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_user_information() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_users', $data, array('user_id' => $_POST['edit_user_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to to update password.
  /   */
   /* public function update_password() {

        #check user exists or not
        $db_result = $this->db->get_where('udaan_users', array('user_id' => $this->session->userdata('userID')));
        if ($db_result && $db_result->num_rows() > 0) {
            #user exists
            #check old password is same or not
            $old_hashed_password = sha1('udan' . (md5('udan' . $_POST['old_password'])));

            if ($old_hashed_password == $db_result->result()[0]->password_hash) {
                #update pssword
                $data = array(
                    'password_hash' => sha1('udan' . (md5('udan' . $_POST['new_password'])))
                );
                $this->db->update('udaan_users', $data, array('user_id' => $this->session->userdata('userID')));
            }

            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
*/
    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new slider into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_slider() {
        date_default_timezone_set('Asia/Kolkata');
         $slider_id = (int)$_POST['slider_id'];
        $data = array(
            'slider_info' => $_POST['slider_info'],
            'slider_navi' => $_POST['slider_navi'],
            'slider_navi_link' => $_POST['slider_navi_link'],
            'slider_input1' => $_POST['slider_input1'],
            'slider_input2' => $_POST['slider_input2'],
            'slider_input3' => $_POST['slider_input3'],
            'content_align' => $_POST['content_align'],
            'slider_image' => $_POST['slider_image'],
            'slider_position' => $_POST['slider_position'],
            'status' => $_POST['status'],
        );

        if($slider_id == 0){
			$result = $this->db->insert('mayas_slider', $data);


			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('mayas_slider', $data, array('slider_id' => $slider_id));

			return TRUE;
		}

    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all course
     */
    public function get_list_of_all_slider() {
        $db_result = $this->db->get_where('mayas_slider', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array();
                }
                if (array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array(
                        'slider_info' => $row->slider_info,
                        'slider_navi' => $row->slider_navi,
                        'slider_input1' => $row->slider_input1,
                        'slider_input2' => $row->slider_input2,
                        'slider_image' => $row->slider_image,
                        'slider_id' => $row->slider_id,
                        'slider_position' => $row->slider_position,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->slider_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	/*
	* This function is used to get_detail_of_user_by_id
     */
    public function get_detail_of_slider_by_id($slider_id) {

        $db_result = $this->db->get_where('mayas_slider', array('status !=' => 'remove','slider_id'=>$slider_id));

		if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array();
                }
                if (array_key_exists($row->slider_id, $data)) {
                    $data[$row->slider_id] = array(
                        'slider_id' => $row->slider_id,
						'slider_info' => $row->slider_info,
                        'slider_navi' => $row->slider_navi,
                        'slider_navi_link' => $row->slider_navi_link,
                        'slider_input1' => $row->slider_input1,
                        'slider_input2' => $row->slider_input2,
                        'slider_input3' => $row->slider_input3,
                        'content_align' => $row->content_align,
                        'slider_image' => $row->slider_image,
                        'status' => $row->status,
                        'slider_position' => $row->slider_position,


                    );
                    array_push($data_value, $data[$row->slider_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }

    }


    //--------------------------------------------------------------------
    /**
     * Handles requests for edit slider information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_slider_information() {
        $data = array(
            'slider_info' => $_POST['edit_slider_info'],
            'slider_navi' => $_POST['edit_slider_navi'],
        );
        $result = $this->db->update('mayas_slider', $data, array('slider_id' => $_POST['edit_slider_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove course from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_slider() {
        $data = array(
            'status' => 'remove',
        );
        $result = $this->db->update('mayas_slider', $data, array('slider_id' => $_POST['edit_slider_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------------
    /**
     * This function is used to upload image for edit section.
     */
    public function edit_upload_image($filename) {
        if ($_POST['image_cat'] == 'slider') {
            $data = array(
                'slider_image' => $filename,
            );
            $result = $this->db->update('mayas_slider', $data, array('slider_id' => $_POST['image_slider_id']));
        }else if ($_POST['image_cat'] == 'service') {
            $data = array(
                'services_image' => $filename,
            );
            $result = $this->db->update('mayas_services', $data, array('service_id' => $_POST['image_service_id']));
        }
		else if ($_POST['image_cat'] == 'product') {
            $data = array(
                'product_image_list' => $_POST['edit_product_image_list'] . $filename . ','
            );
            $result = $this->db->update('mayas_product', $data, array('product_id' => $_POST['image_product_id']));
        }
		else if ($_POST['image_cat'] == 'sub_menu_product') {
            $data = array(
                'sub_menu_products_image' => $filename,
            );
            $result = $this->db->update('mayas_sub_menu_products', $data, array('sub_menu_products_id ' => $_POST['image_product_id']));
        }
		else if ($_POST['image_cat'] == 'blog') {
            $data = array(
                'blog_image' => $filename,
            );
            $result = $this->db->update('mayas_blog', $data, array('blog_id' => $_POST['image_blog_id']));
        }
		else if ($_POST['image_cat'] == 'banner_image') {
			$data = array(
                'banner_image' => $filename,
            );
			$result = $this->db->update('mayas_banner_images', $data, array('banner_image_id' => $_POST['banner_image_id']));
        }
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new Menu into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_menu() {
        date_default_timezone_set('Asia/Kolkata');

        $data = array(
            'menu_name' => $_POST['menu_name'],
            'menu_navi' => $_POST['menu_navi'],
        );

        $result = $this->db->insert('mayas_menu', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all menu
     */
    public function get_list_of_all_menu() {
        $db_result = $this->db->get_where('mayas_menu', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->menu_id, $data)) {
                    $data[$row->menu_id] = array();
                }
                if (array_key_exists($row->menu_id, $data)) {
                    $data[$row->menu_id] = array(
                        'menu_name' => $row->menu_name,
                        'menu_navi' => $row->menu_navi,
                        'menu_id' => $row->menu_id,
                    );
                    array_push($data_value, $data[$row->menu_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit menu information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_menu_information() {
        $data = array(
            'menu_name' => $_POST['edit_menu_name'],
            'menu_navi' => $_POST['edit_menu_navi'],
        );
        $result = $this->db->update('mayas_menu', $data, array('menu_id' => $_POST['edit_menu_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove menu from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_menu() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_menu', $data, array('menu_id' => $_POST['edit_menu_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new sub menu services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_sub_menu_services() {
        date_default_timezone_set('Asia/Kolkata');

        $data = array(
            'sub_menu_services_name' => $_POST['sub_menu_services_name'],
            'sub_menu_services_navi' => $_POST['sub_menu_services_navi'],
        );

        $result = $this->db->insert('mayas_sub_menu_services', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all sub menu services
     */
    public function get_list_of_all_sub_menu_services() {
        $db_result = $this->db->get_where('mayas_sub_menu_services', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->sub_menu_services_id, $data)) {
                    $data[$row->sub_menu_services_id] = array();
                }
                if (array_key_exists($row->sub_menu_services_id, $data)) {
                    $data[$row->sub_menu_services_id] = array(
                        'sub_menu_services_name' => $row->sub_menu_services_name,
                        'sub_menu_services_navi' => $row->sub_menu_services_navi,
                        'sub_menu_services_id' => $row->sub_menu_services_id,
                    );
                    array_push($data_value, $data[$row->sub_menu_services_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu services information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_services_information() {
        $data = array(
            'sub_menu_services_name' => $_POST['edit_sub_menu_services_name'],
            'sub_menu_services_navi' => $_POST['edit_sub_menu_services_navi'],
        );
        $result = $this->db->update('mayas_sub_menu_services', $data, array('sub_menu_services_id' => $_POST['edit_sub_menu_services_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove sub menu services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_sub_menu_services() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_sub_menu_services', $data, array('sub_menu_services_id' => $_POST['edit_sub_menu_services_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new sub menu products into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_sub_menu_products() {
        date_default_timezone_set('Asia/Kolkata');

        $data = array(
            'sub_menu_products_name' => $_POST['sub_menu_products_name'],
            'sub_menu_products_navi' => $_POST['sub_menu_products_navi'],
			'sub_menu_products_image'=> $_POST['sub_menu_products_image'],
        );

        $result = $this->db->insert('mayas_sub_menu_products', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all sub menu products
     */
    public function get_list_of_all_sub_menu_products() {
        $db_result = $this->db->get_where('mayas_sub_menu_products', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array();
                }
                if (array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array(
                        'sub_menu_products_name' => $row->sub_menu_products_name,
                        'sub_menu_products_navi' => $row->sub_menu_products_navi,
                        'sub_menu_products_id' => $row->sub_menu_products_id,
						'sub_menu_products_image' => $row->sub_menu_products_image,
                    );
                    array_push($data_value, $data[$row->sub_menu_products_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit sub menu products information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_sub_menu_products_information() {
        $data = array(
            'sub_menu_products_name' => $_POST['edit_sub_menu_products_name'],
            'sub_menu_products_navi' => $_POST['edit_sub_menu_products_navi'],
        );
        $result = $this->db->update('mayas_sub_menu_products', $data, array('sub_menu_products_id' => $_POST['edit_sub_menu_products_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove sub menu products from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_sub_menu_products() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_sub_menu_products', $data, array('sub_menu_products_id' => $_POST['edit_sub_menu_products_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new birth stone into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_birth_stone() {
        $birth_stone_id = (int)$_POST['birth_stone_id'];

        $data = array(
			'birth_stone_name' => $_POST['birth_stone_name'],
            'birth_stone_navi' => $_POST['birth_stone_navi'],
            'main_description' => $_POST['main_description'],
            'description' => $_POST['description'],
        );
		if($birth_stone_id==0){
			$result = $this->db->insert('mayas_birth_stone', $data);
		}else{
			$result = $this->db->update('mayas_birth_stone', $data,array('birth_stone_id'=>$birth_stone_id));
		}

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all birth stone
     */
    public function get_list_of_all_birth_stone() {
        $db_result = $this->db->get_where('mayas_birth_stone', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->birth_stone_id, $data)) {
                    $data[$row->birth_stone_id] = array();
                }
                if (array_key_exists($row->birth_stone_id, $data)) {
                    $data[$row->birth_stone_id] = array(
                        'birth_stone_name' => $row->birth_stone_name,
                        'birth_stone_navi' => $row->birth_stone_navi,
                        'birth_stone_id' => $row->birth_stone_id,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                    );
                    array_push($data_value, $data[$row->birth_stone_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit birth stone information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_birth_stone_information() {
        $data = array(
            'birth_stone_name' => $_POST['edit_birth_stone_name'],
            'birth_stone_navi' => $_POST['edit_birth_stone_navi'],
        );
        $result = $this->db->update('mayas_birth_stone', $data, array('birth_stone_id' => $_POST['edit_birth_stone_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove birth stone from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_birth_stone() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_birth_stone', $data, array('birth_stone_id' => $_POST['edit_birth_stone_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_services() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
		$service_id = (int)$_POST['service_id'];
        $data = array(

            'services_name' => $_POST['services_name'],
            'services_type' => $_POST['services_type'],
			'col' => $_POST['col'],
            'services_main_description' => $_POST['services_main_description'],
            'services_description' => $_POST['services_description'],
            'consultant_type' => $_POST['consultant_type'],
            //'number_of_questions' => $_POST['number_of_questions'],
            'cost' => $_POST['cost'],
            'faq_category' => $_POST['faq_category'],
            'banner_id' => $_POST['banner_id'],
            'about_content_id' => $_POST['about_content_id'],
            'usdcost' => $_POST['usdcost'],
            'discounted_cost' => $_POST['discounted_cost'],
            'discounted_usd_cost' => $_POST['discounted_usd_cost'],
            'services_image' => $_POST['services_image'],
            'services_detail_image' => $_POST['services_detail_image'],
            'detail_image_position' => $_POST['detail_image_position'],
            'service_step_image' => $_POST['service_step_image'],
            'disc_logo_percent' => $_POST['disc_logo_percent'],
            'premium_type' => $_POST['premium_type'],
            'services_url' => $_POST['services_url'],
            'services_title' => $_POST['services_title'],
            'services_dec_meta' => $_POST['services_dec_meta'],
            'services_keyword_meta' => $_POST['services_keyword_meta'],
            'status' => $_POST['status'],
            'services_position' => $_POST['services_position'],
            'q_label' => $_POST['q_label'],
            'q_placeholder' => $_POST['q_placeholder'],
            'q_info' => $_POST['q_info'],
            'single_married_placeholder' => $_POST['single_married_placeholder'],
            'single_married_label' => $_POST['single_married_label'],
            'extra_description' => $_POST['extra_description'],

            'is_question1' => $_POST['is_question1'],
                'q1_label' => $_POST['q1_label'],
                'q1_placeholder' => $_POST['q1_placeholder'],
                'is_question2' => $_POST['is_question2'],
                'q2_label' => $_POST['q2_label'],
                'q2_placeholder' => $_POST['q2_placeholder'],
            'additional_json'=> json_encode($_POST['questions'])

        );
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));
		if($service_id==0){
        $result = $this->db->insert('mayas_services', $data);
		}else {
			$result = $this->db->update('mayas_services', $data,array('service_id'=>$service_id));
		}
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all services
     */
    public function get_list_of_all_services() {
        $db_result = $this->db->get_where('mayas_services', array('status !=' => 'remove'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
						 'col' => $row->col,
                        'services_main_description' => $row->services_main_description,
                        'services_description' => $row->services_description,
                        'consultant_type' => $row->consultant_type,
                        'number_of_questions' => $row->number_of_questions,
                        'cost' => $row->cost,
                        'services_image' => $row->services_image,
                        'service_id' => $row->service_id,
                        'premium_type' => $row->premium_type,
                        'services_position' => $row->services_position,
                        'status' => $row->status,
                        'is_question1' => $row->is_question1,
                         'q1_label' =>$row->q1_label,
                        'q1_placeholder' => $row->q1_placeholder,
                        'is_question2' => $row->is_question2,
                        'q2_label' => $row->q2_label,
                        'q2_placeholder' => $row->q2_placeholder,
                        'q_label' => $row->q_label,
                        'q_placeholder' => $row->q_placeholder,
                        'q_info' => $row->q_info,

                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all blogs
     */
    public function get_list_of_all_blogs() {
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
                        'blog_image' => $row->blog_image,
                        'blog_id' => $row->blog_id,
                        'status' => $row->status,
                        'blog_writer' => $row->blog_writer,
						'date' => $row->date,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit services information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_services_information() {
        $data = array(
            'services_name' => $_POST['edit_services_name'],
            'services_main_description' => $_POST['edit_services_main_description'],
            'services_type' => $_POST['edit_services_type'],
            'services_description' => nl2br(htmlentities($_POST['edit_services_description'], ENT_QUOTES, 'UTF-8')),
        );
        $result = $this->db->update('mayas_services', $data, array('service_id' => $_POST['edit_services_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_services() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_services', $data, array('service_id' => $_POST['edit_service_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for adding new aboutus into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_aboutus() {
        date_default_timezone_set('Asia/Kolkata');
        $data = array(
            'aboutus1' => nl2br(htmlentities($_POST['aboutus1'], ENT_QUOTES, 'UTF-8')),
            'aboutus2' => nl2br(htmlentities($_POST['aboutus2'], ENT_QUOTES, 'UTF-8')),
            'aboutus3' => nl2br(htmlentities($_POST['aboutus3'], ENT_QUOTES, 'UTF-8')),
            'aboutus4' => nl2br(htmlentities($_POST['aboutus4'], ENT_QUOTES, 'UTF-8')),

        );

        $result = $this->db->insert('mayas_aboutus', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all aboutus
     */
    public function get_list_of_all_aboutus() {
        $db_result = $this->db->get_where('mayas_aboutus', array('status' => 'active','aboutus_id'=> 1));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array();
                }
                if (array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array(
                        'aboutus1' => $row->aboutus1,
                        'aboutus2' => $row->aboutus2,
                        'aboutus_id' => $row->aboutus_id,
						'aboutus_image1' => $row->aboutus_image1,
						'aboutus_image2' => $row->aboutus_image2,

                    );
                    array_push($data_value, $data[$row->aboutus_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to get vastu_data
     */
    public function get_vastu_data() {
        $db_result = $this->db->get('vastu_data');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->id, $data)) {
                    $data[$row->id] = array();
                }
                if (array_key_exists($row->id, $data)) {
                    $data[$row->id] = array(
                      'home_page_heading' =>  $row->home_page_heading,
                      'youtube_link'  => $row->youtube_link,
                      'url' =>  $row->url,
                      'page_title'  =>  $row->page_title,
                      'meta_tag' =>  $row->meta_tag,
                      'meta_descp'  =>  $row->meta_descp,
                      'home_page_content'=>$row->home_page_content,
                      
                    );
                    array_push($data_value, $data[$row->id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to get vastu_data
     */
    public function get_vastu_list() {
        $db_result = $this->db->get('mayas_new_vastu');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array();
                }
                if (array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array(
                      'vastu_id' => $row->vastu_id,
                      'page_heading' =>  $row->page_heading,
                      'short_description'  => $row->short_description,
                      'url' =>  $row->url,
                      'page_title'  =>  $row->page_title,
                      'meta_tag' =>  $row->meta_tag,
                      'meta_descp'  =>  $row->meta_descp,
                      'page_content'=>$row->page_content,
                      'detail_image'=>$row->detail_image,
                      
                    );
                    array_push($data_value, $data[$row->vastu_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit aboutus information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_aboutus_information() {
        $data = array(
            'aboutus1' => $_POST['aboutus1'],

            'aboutus2' => ($_POST['aboutus2']),
			'aboutus_image1' => $_POST['aboutus_image1'],
			'aboutus_image2' => $_POST['aboutus_image2'],
        );
        $result = $this->db->update('mayas_aboutus', $data, array('aboutus_id' => 1));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for add_new_special_feature into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_special_feature() {
        date_default_timezone_set('Asia/Kolkata');
        $data = array(
            'special_feature_heading' => $_POST['special_feature_heading'],
            'sub_heading' => $_POST['sub_heading'],
            'small_sub_heading_message' => $_POST['small_sub_heading_message'],
            'point1' => $_POST['point1'],
            'point2' => $_POST['point2'],
            'point3' => $_POST['point3'],
            'point4' => $_POST['point4'],
            'point5' => $_POST['point5'],
            'point6' => $_POST['point6'],
            'point7' => $_POST['point7'],
        );

        $result = $this->db->insert('mayas_special_feature', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all special_feature
     */
    public function get_list_of_all_special_feature() {
        $db_result = $this->db->get_where('mayas_special_feature', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->special_feature_id, $data)) {
                    $data[$row->special_feature_id] = array();
                }
                if (array_key_exists($row->special_feature_id, $data)) {
                    $data[$row->special_feature_id] = array(
                        'special_feature_heading' => $row->special_feature_heading,
                        'sub_heading' => $row->sub_heading,
                        'small_sub_heading_message' => $row->small_sub_heading_message,
                        'point1' => $row->point1,
                        'point2' => $row->point2,
                        'point3' => $row->point3,
                        'point4' => $row->point4,
                        'point5' => $row->point5,
                        'point6' => $row->point6,
                        'point7' => $row->point7,
                        'special_feature_id' => $row->special_feature_id,
                    );
                    array_push($data_value, $data[$row->special_feature_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    //--------------------------------------------------------------------
    /**
     * Handles requests for edit special_feature information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_special_feature_information() {
        $data = array(
            'special_feature_heading' => $_POST['special_feature_heading'],
            'sub_heading' => $_POST['sub_heading'],
            'small_sub_heading_message' => $_POST['small_sub_heading_message'],
            'point1' => $_POST['point1'],
            'point2' => $_POST['point2'],
            'point3' => $_POST['point3'],
            'point4' => $_POST['point4'],
            'point5' => $_POST['point5'],
            'point6' => $_POST['point6'],
            'point7' => $_POST['point7'],
        );
        $result = $this->db->update('mayas_special_feature', $data, array('special_feature_id' => $_POST['special_feature_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //-------------------------------------------------------------------------
    /*
     * This function is used to remove services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_special_feature() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_special_feature', $data, array('special_feature_id' => $_POST['special_feature_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all product
     */
    public function get_list_of_all_submenu_product_name() {
        $db_result = $this->db->get_where('mayas_sub_menu_products', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array();
                }
                if (array_key_exists($row->sub_menu_products_id, $data)) {
                    $data[$row->sub_menu_products_id] = array(
                        'sub_menu_products_name' => $row->sub_menu_products_name,
                        'sub_menu_products_navi' => $row->sub_menu_products_navi,
                        'sub_menu_products_id' => $row->sub_menu_products_id,
                    );
                    array_push($data_value, $data[$row->sub_menu_products_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for add_new_product into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_product() {
        date_default_timezone_set('Asia/Kolkata');
        $data = array(
            'product_categories' => $_POST['product_categories'],
            'product_name' => $_POST['product_name'],
            'product_description' => $_POST['product_description'],
            'product_price_ind' => $_POST['product_price_ind'],
            'product_price_usd' => $_POST['product_price_usd'],
            'product_feature' => $_POST['product_feature'],
			'product_weight' => $_POST['product_weight'],
            'product_shape' => $_POST['product_shape'],
            'product_color' => $_POST['product_color'],
            'product_clarity' => $_POST['product_clarity'],
			'product_grade' => $_POST['product_grade'],
			'product_cut' => $_POST['product_cut'],
			'product_treatment' => $_POST['product_treatment'],
            'product_origin' => $_POST['product_origin'],
			'product_other_info' => $_POST['product_other_info'],
			'product_other_info2' => $_POST['product_other_info2'],
			'product_other_info3' => $_POST['product_other_info3'],
			 'product_image_list' => $_POST['product_image_list'],
        );


        $result = $this->db->insert('mayas_product', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all product
     */
    public function get_list_of_all_product() {
	    $this->db->from('mayas_product as mp');
		$this->db->join('mayas_sub_menu_products as smp','smp.sub_menu_products_id = mp.product_categories');
		$this->db->where(array('mp.status' => 'active'));
		$db_result = $this->db->get();
       // $db_result = $this->db->get_where('mayas_product', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->product_id, $data)) {
                    $data[$row->product_id] = array();
                }
                if (array_key_exists($row->product_id, $data)) {
                    $data[$row->product_id] = array(
					    'product_id' => $row->product_id,
						'sub_menu_products_name' => $row->sub_menu_products_name,
                        'product_categories' => $row->product_categories,
                        'product_name' => $row->product_name,
                        'product_description' => $row->product_description,
                        'product_price_ind' => $row->product_price_ind,
                        'product_price_usd' => $row->product_price_usd,
                        'product_feature' => $row->product_feature,
                        'product_weight' => $row->product_weight,
                        'product_shape' => $row->product_shape,
                        'product_color' => $row->product_color,
                        'product_clarity' => $row->product_clarity,
						'product_grade' => $row->product_grade,
                        'product_cut' => $row->product_cut,
						'product_treatment' => $row->product_treatment,
						'product_origin' => $row->product_origin,
						'product_other_info' => $row->product_other_info,
						'product_other_info2' => $row->product_other_info2,
						'product_other_info3' => $row->product_other_info3,
						'product_image_list' => $row->product_image_list,
                    );
                    array_push($data_value, $data[$row->product_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	 //--------------------------------------------------------------------
    /**
     * Handles requests for edit product information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_product_information() {
        $data = array(
            'product_categories' => $_POST['product_categories'],
            'product_name' => $_POST['product_name'],
            'product_description' => $_POST['product_description'],
            'product_price_ind' => $_POST['product_price_ind'],
            'product_price_usd' => $_POST['product_price_usd'],
            'product_feature' => $_POST['product_feature'],
            'product_weight' => $_POST['product_weight'],
            'product_shape' => $_POST['product_shape'],
            'product_color' => $_POST['product_color'],
			'product_clarity' => $_POST['product_clarity'],
			'product_grade' => $_POST['product_grade'],
			'product_cut' => $_POST['product_cut'],
			'product_treatment' => $_POST['product_treatment'],
			'product_origin' => $_POST['product_origin'],
			'product_other_info' => $_POST['product_other_info'],
			'product_other_info2' => $_POST['product_other_info2'],
			'product_other_info3' => $_POST['product_other_info3'],

        );
        $result = $this->db->update('mayas_product', $data, array('product_id' => $_POST['product_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove product image from the list of all image
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_product_image() {
        $data = array(
            'product_image_list' => $_POST['updated_product_image_list']
        );
        $result = $this->db->update('mayas_product', $data, array('product_id' => $_POST['product_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove product from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_product() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_product', $data, array('product_id' => $_POST['product_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new blog into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_blog() {
       // date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
		 $blog_id = (int)$_POST['blog_id'];
        $data = array(
            'blog_heading' => $_POST['blog_heading'],
            'blog_choose' => $_POST['blog_choose'],
            'blog_sub_heading' => $_POST['blog_sub_heading'],
            'blog_description' => $_POST['blog_description'],
            'blog_writer' => 'Mayankesh Sharma',
            'blog_image' => $_POST['blog_image'],
			'date' => date('Y-m-d'),
        );
		if($blog_id==0){
			$result = $this->db->insert('mayas_blog', $data);
		}else{
			$result = $this->db->update('mayas_blog', $data,array('blog_id'=>$blog_id));
		}
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));


        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }




	 //--------------------------------------------------------------------
    /**
     * Handles requests for edit blog information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_blog_information() {
        $data = array(
            'blog_heading' => $_POST['edit_blog_heading'],
            'blog_sub_heading' => $_POST['edit_blog_sub_heading'],
            'blog_writer' => $_POST['blog_writer'],
            'blog_description' => nl2br(htmlentities($_POST['edit_blog_description'], ENT_QUOTES, 'UTF-8')),
        );
        $result = $this->db->update('mayas_blog', $data, array('blog_id' => $_POST['edit_blog_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to remove blog from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_blog() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_blog', $data, array('blog_id' => $_POST['edit_blog_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new banner image into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_banner_image() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
        $data = array(
            'banner_page_type' => $_POST['banner_page_type'],
            'banner_image' => $_POST['banner_image'],
			'created_date' => date('Y-m-d'),
            'created_time' => date('H:m:s'),

        );
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

        $result = $this->db->insert('mayas_banner_images', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all banner image
     */
    public function get_list_of_all_banner_image() {
        $db_result = $this->db->get_where('mayas_banner_images', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->banner_image_id, $data)) {
                    $data[$row->banner_image_id] = array();
                }
                if (array_key_exists($row->banner_image_id, $data)) {
                    $data[$row->banner_image_id] = array(
						'banner_image_id' => $row->banner_image_id,
                        'banner_page_type' => $row->banner_page_type,
                        'banner_image' => $row->banner_image,
                        'created_date' => $row->created_date,
						'created_date' => $row->created_date,
                     );
                    array_push($data_value, $data[$row->banner_image_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for edit banner image information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_banner_image_information() {
        $data = array(
            'banner_image_id' => $_POST['edit_banner_image_id'],
            'banner_page_type' => $_POST['edit_banner_page_type'],

        );
        $result = $this->db->update('mayas_banner_images', $data, array('banner_image_id' => $_POST['edit_banner_image_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove banner information from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_banner_image() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_banner_images', $data, array('banner_image_id' => $_POST['banner_image_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for adding new lagan into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_lagan() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
        $data = array(
            'lagan_name' => $_POST['lagan_name'],
            'life_stone_name' => $_POST['life_stone_name'],
            'punya_stone_name' => $_POST['punya_stone_name'],
            'bhagya_stone_name' => $_POST['bhagya_stone_name'],
        );
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

        $result = $this->db->insert('mayas_lagan', $data);
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all lagans
     */
    public function get_list_of_all_lagan() {
        $db_result = $this->db->get_where('mayas_lagan', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->lagan_id, $data)) {
                    $data[$row->lagan_id] = array();
                }
                if (array_key_exists($row->lagan_id, $data)) {
                    $data[$row->lagan_id] = array(
						'lagan_id' => $row->lagan_id,
                        'lagan_name' => $row->lagan_name,
                        'life_stone_name' => $row->life_stone_name,
                        'punya_stone_name' => $row->punya_stone_name,
                        'bhagya_stone_name' => $row->bhagya_stone_name,
                    );
                    array_push($data_value, $data[$row->lagan_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//--------------------------------------------------------------------
    /**
     * Handles requests for edit lagan information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_lagan_information() {
        $data = array(
            'lagan_name' => $_POST['edit_lagan_name'],
            'life_stone_name' => $_POST['edit_life_stone_name'],
            'punya_stone_name' => $_POST['edit_punya_stone_name'],
			'bhagya_stone_name' => $_POST['edit_bhagya_stone_name'],
        );
        $result = $this->db->update('mayas_lagan', $data, array('lagan_id' => $_POST['edit_lagan_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove lagan from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_lagan() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_lagan', $data, array('lagan_id' => $_POST['edit_lagan_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all contact form details
     */
    public function get_list_of_contact_form_detail() {
        $db_result = $this->db->get_where('mayas_contactus', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->contact_id, $data)) {
                    $data[$row->contact_id] = array();
                }
                if (array_key_exists($row->contact_id, $data)) {
                    $data[$row->contact_id] = array(
						'contact_id' => $row->contact_id,
                        'inputName' => $row->inputName,
                        'inputEmail' => $row->inputEmail,
                        'inputWebsite' => $row->inputWebsite,
                        'inputMessage' => $row->inputMessage,
                    );
                    array_push($data_value, $data[$row->contact_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove/read contact form details from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_contact() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_contactus', $data, array('contact_id' => $_POST['edit_contact_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all consultant form details
     */
    public function get_list_of_consultant_form_detail() {
        $db_result = $this->db->get_where('mayas_free_consultant', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultant_id, $data)) {
                    $data[$row->consultant_id] = array();
                }
                if (array_key_exists($row->consultant_id, $data)) {
                    $data[$row->consultant_id] = array(
						'consultant_id' => $row->consultant_id,
                        'inputName' => $row->inputName,
                        'inputBirth' => $row->inputBirth,
                        'inputTime' => $row->inputTime,
                        'inputPlace' => $row->inputPlace,
						'inputContact' => $row->inputContact,
                        'inputEmail' => $row->inputEmail,
                        'inputQuestion' => $row->inputQuestion,
                    );
                    array_push($data_value, $data[$row->consultant_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove/read contact form details from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_consultant() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_free_consultant', $data, array('consultant_id' => $_POST['edit_consultant_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	public function get_list_of_all_services_by_id($service_id) {
        $db_result = $this->db->get_where('mayas_services', array('service_id' => $service_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'faq_category' => $row->faq_category,
                        'banner_id' => $row->banner_id,
                        'about_content_id' => $row->about_content_id,
                        'single_married_placeholder' => $row->single_married_placeholder,
                        'single_married_label' => $row->single_married_label,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
                        'number_of_questions' => $row->number_of_questions,
                        'consultant_type' => $row->consultant_type,
                        'cost' => $row->cost,
                        'usdcost' => $row->usdcost,
                        'discounted_cost' => $row->discounted_cost,
                        'discounted_usd_cost' => $row->discounted_usd_cost,
						 'col' => $row->col,
                        'services_description' => $row->services_description,
                        'services_image' => $row->services_image,
                        'services_detail_image' => $row->services_detail_image,
                        'detail_image_position' => $row->detail_image_position,
                        'service_step_image' => $row->service_step_image,
                        'service_id' => $row->service_id,
                        'disc_logo_percent' => $row->disc_logo_percent,
                        'premium_type' => $row->premium_type,
						'status' => $row->status,
						'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
						'services_position' => $row->services_position,
                        'additional_json' => json_decode($row->additional_json),
                        'q_label' => $row->q_label,
                        'q_placeholder' => $row->q_placeholder,
                        'q_info' => $row->q_info,
                        'is_question1' => $row->is_question1,
                        'q1_label' =>$row->q1_label,
                        'q1_placeholder' => $row->q1_placeholder,
                        'is_question2' => $row->is_question2,
                        'q2_label' => $row->q2_label,
                        'q2_placeholder' => $row->q2_placeholder,
                        'extra_description' => $row->extra_description,

                    );
                    array_push($data_value, $data[$row->service_id]);

                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	 //--------------------------------------------------------------------
    /**
     * Handles requests for edit services information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_sub_menu_services() {
        $data = array(
            'sub_menu_services_id' => $_POST['sub_menu_services_id'],
            'sub_menu_services_name' => $_POST['sub_menu_services_name'],
            'sub_menu_services_navi' => $_POST['sub_menu_services_navi'],
            'status' => $_POST['status'],
        );
        $result = $this->db->update(' mayas_sub_menu_services', $data, array('sub_menu_services_id' => $_POST['sub_menu_services_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	public function update_patientinfo() {

		$birth_stone_id = (int)$_POST['birth_stone_id'];
		$data = array(

            'birth_stone_name' => $_POST['birth_stone_name'],
            'birth_stone_id' => $_POST['birth_stone_id'],
            'birth_stone_navi' => $_POST['birth_stone_navi'],
            'status' => $_POST['status'],
            'services_main_description' => $_POST['services_main_description'],
            'services_description' => $_POST['services_description'],

		);

		if($birth_stone_id==0){
			$result = $this->db->insert('mayas_birth_stone', $data);
		}else{
			$result = $this->db->update('mayas_birth_stone', $data,array('birth_stone_id'=>$birth_stone_id));
		}

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}

    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all birth stone
     */
    public function get_list_of_all_birth_stone_by_id($stone_id) {
        $db_result = $this->db->get_where('mayas_birth_stone', array('status' => 'active','birth_stone_id'  => $stone_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->birth_stone_id, $data)) {
                    $data[$row->birth_stone_id] = array();
                }
                if (array_key_exists($row->birth_stone_id, $data)) {
                    $data[$row->birth_stone_id] = array(
                        'birth_stone_name' => $row->birth_stone_name,
                        'birth_stone_navi' => $row->birth_stone_navi,
                        'birth_stone_id' => $row->birth_stone_id,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->birth_stone_id]);
                }
            }
        return $data_value ;
        } else {
            return FALSE;
        }
    }

	public function get_consultation_massages() {
        $db_result = $this->db->get_where('consultation_massages');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
				//var_dump($row);
                if (!array_key_exists($row->massage_id, $data)) {
                    $data[$row->massage_id] = array();
                }
                if (array_key_exists($row->massage_id, $data)) {
                    $data[$row->massage_id] = array(
						'massage_id' => $row->massage_id,
                        'default_massage' => $row->default_massage,
                        'paid_massage' => $row->paid_massage,
                        'custom_message' => $row->custom_message,
                        'custom_message1' => $row->custom_message1,
                        'custom_message_paid' => $row->custom_message_paid,
                        'vastu_default_msg' => $row->vastu_default_msg,
                        'vastu_leave_msg' => $row->vastu_leave_msg,
                        'start_date' => $row->start_date,
                        'end_date' => $row->end_date,
                    );
                    array_push($data_value, $data[$row->massage_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	public function get_list_of_all_blogs_by_id($blog_id) {
        $db_result = $this->db->get_where('mayas_blog', array('status' => 'active','blog_id'  => $blog_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array();
                }
                if (array_key_exists($row->blog_id, $data)) {
                    $data[$row->blog_id] = array(
						'blog_id' => $row->blog_id,
                        'blog_heading' => $row->blog_heading,
                        'blog_sub_heading' => $row->blog_sub_heading,
                        'blog_description' => $row->blog_description,
                        'blog_image' => $row->blog_image,
                        'blog_writer' => $row->blog_writer,
                        'blog_choose' => $row->blog_choose,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->blog_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	 public function get_list_of_all_aboutus_by_id() {
        $db_result = $this->db->get_where('mayas_aboutus', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array();
                }
                if (array_key_exists($row->aboutus_id, $data)) {
                    $data[$row->aboutus_id] = array(
                        'aboutus1' => $row->aboutus1,
                        'aboutus2' => $row->aboutus2,
                        'aboutus3' => $row->aboutus3,
                        'aboutus4' => $row->aboutus4,
                        'aboutus_id' => $row->aboutus_id,
                    );
                    array_push($data_value, $data[$row->aboutus_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_testimonials
     */
    public function get_list_of_testimonials() {
		$this->db->order_by("UPPER(testimonials_name)","desc");
		$db_result = $this->db->get_where('mayas_testimonials', array('status' => 'active'));

        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array();
                }
                if (array_key_exists($row->testimonials_id, $data)) {
                    $data[$row->testimonials_id] = array(
                        'testimonials_id' => $row->testimonials_id,
						'testimonials_name' => $row->testimonials_name,
						'position' => $row->position,
						'comments' => $row->comments,
						'testimonials_image' => $row->testimonials_image,
                        'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->testimonials_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_testimonials from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_testimonials() {
		$data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_testimonials', $data, array('testimonials_id' => $_POST['testimonials_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }


	}
		//-------------------------------------------------------------------------
    /*
     * This function is used to update testimonials
     *
     * @access		public
     * @since		1.0.0
     */
    public function update_testimonials() {
		date_default_timezone_set('Asia/Kolkata');
		$testimonials_id = (int)$_POST['testimonials_id'];
		$data = array(
            'testimonials_name' => $_POST['testimonials_name'],
            'position' => $_POST['position'],
			'comments' => nl2br($_POST['comments']),
			'testimonials_image' => $_POST['testimonials_image'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($testimonials_id == 0){
			$result = $this->db->insert('mayas_testimonials', $data);
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('mayas_testimonials', $data, array('testimonials_id' => $testimonials_id));
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}
	}

	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_content
     */
    public function get_list_of_content() {

		$db_result = $this->db->get_where('mayas_content');

        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array();
                }
                if (array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array(
                        'content_id' => $row->content_id,
						'free_consultation_content' => $row->free_consultation_content,
						'premium_consultation_content' => $row->premium_consultation_content,
						'header_content' => $row->header_content,
						'free_consultation_desc' => $row->free_consultation_desc,
						'premium_consultation_desc' => $row->premium_consultation_desc,

                    );
                    array_push($data_value, $data[$row->content_id]);
                }
            }
            return $data_value;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to  update_content
     *
     * @access		public
     * @since		1.0.0
     */
    public function update_content() {
		date_default_timezone_set('Asia/Kolkata');

		$data = array(
            'free_consultation_content' => $_POST['free_consultation_content'],
            'premium_consultation_content' => $_POST['premium_consultation_content'],
			    'header_content' => $_POST['header_content'],
			    'free_consultation_desc' => $_POST['free_consultation_desc'],
			    'premium_consultation_desc' => $_POST['premium_consultation_desc'],
        );

			$result = $this->db->update('mayas_content', $data, array('content_id' => 1));
			return TRUE;
	}

	//-------------------------------------------------------------------------
    /*
     * This function is used to  update_content
     *
     * @access		public
     * @since		1.0.0
     */



	 public function get_list_of_buy_product() {
        $db_result = $this->db->get_where('mayas_order_payment', array('item_type' => 'product'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->order_payment_id, $data)) {
                    $data[$row->order_payment_id] = array();
                }
                if (array_key_exists($row->order_payment_id, $data)) {
                    $data[$row->order_payment_id] = array(
                        'order_id' => $row->order_id,
                        'payment_id' => $row->payment_id,
                        'itemname' => $row->itemname,
                        'itemcost' => $row->itemcost,
                        'order_payment_id' => $row->order_payment_id,
                        'subtotal' => $row->subtotal,
                        'discount' => $row->discount,
                        'grandtotal' => $row->grandtotal,
                        'payment_date' => $row->payment_date,
                        'address' => $row->address,
                        'city' => $row->city,
                        'state' => $row->state,
                        'country' => $row->country,
                        'contectno' => $row->contectno,
                        'name' => $row->name,
                        'item_type' => $row->item_type,
                        'email' => $row->email,

                    );
                    array_push($data_value, $data[$row->order_payment_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to  update_content
     *
     * @access		public
     * @since		1.0.0
     */



	 public function get_list_of_buy_service() {
        $db_result = $this->db->get_where('mayas_order_payment', array('item_type' => 'service'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->order_payment_id, $data)) {
                    $data[$row->order_payment_id] = array();
                }
                if (array_key_exists($row->order_payment_id, $data)) {
                    $data[$row->order_payment_id] = array(
                         'order_id' => $row->order_id,
                        'payment_id' => $row->payment_id,
                        'itemname' => $row->itemname,
                        'itemcost' => $row->itemcost,
                        'order_payment_id' => $row->order_payment_id,
                        'subtotal' => $row->subtotal,
                        'discount' => $row->discount,
                        'grandtotal' => $row->grandtotal,
                        'payment_date' => $row->payment_date,
                        'address' => $row->address,
                        'city' => $row->city,
                        'state' => $row->state,
                        'country' => $row->country,
                        'contectno' => $row->contectno,
                        'name' => $row->name,
                        'item_type' => $row->item_type,
                        'email' => $row->email,
                    );
                    array_push($data_value, $data[$row->order_payment_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

    public function remove_vasturecord() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_vastu_consultation', $data, array('vastu_id' => $_POST['consultation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 public function get_list_of_buy_vastuservice() {
        $db_result = $this->db->get_where('mayas_vastu_consultation', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array();
                }
                if (array_key_exists($row->vastu_id, $data)) {
                    $data[$row->vastu_id] = array(
                         'order_id' => $row->order_payment_id,
                        // 'payment_id' => $row->payment_id,
                        // 'itemname' => $row->consultantType,
                        'vastu_id' => $row->vastu_id,
                        'gender' => $row->inputGender,
                        'inputDateDate' => $row->inputDateDate,
                        'inputTimeMin' => $row->inputTimeMin,
                        // 'subtotal' => $row->subtotal,
                        // 'discount' => $row->discount,
                        'grandtotal' => $row->amount,
                        'appointment_yes' => $row->appointment_yes,
                        // 'payment_date' => $row->payment_date,
                        // 'address' => $row->address,
                        'birthcity' => $row->birthCity,
                        'birthstate' => $row->birthState,
                        'country' => $row->birthCountry,
                        'contectno' => $row->inputContact,
                        'name' => $row->inputName,
                        'item_type' => $row->consultantType,
                        'email' => $row->inputEmail,
                    );
                    array_push($data_value, $data[$row->vastu_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	//-------------------------------------------------------------------------
    /*
     * This function is used to  update_content
     *
     * @access		public
     * @since		1.0.0
     */

	 public function get_list_of_consultant() {
        $db_result = $this->db->get_where('mayas_consultation');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                         'consultantType' => $row->consultantType,
                        'full_name' => $row->full_name,
                        'gender' => $row->gender,
                        'inputDateDate' => $row->inputDateDate,
                        'consultation_id' => $row->consultation_id,
                        'inputDateMonth' => $row->inputDateMonth,
                        'inputDateYear' => $row->inputDateYear,
                        'inputTimeHr' => $row->inputTimeHr,
                        'inputTimeMin' => $row->inputTimeMin,
                        'inputTimeSec' => $row->inputTimeSec,
                        'birthCountry' => $row->birthCountry,
                        'birthState' => $row->birthState,
                        'birthCity' => $row->birthCity,
                        'inputEmail' => $row->inputEmail,
                        'inputContact' => $row->inputContact,
                        'inputQuestion' => $row->inputQuestion,
                        'otherInfomation' => $row->otherInfomation,
                        'educationQualification' => $row->educationQualification,
                        'currentProfession' => $row->currentProfession,
                        'partnerName' => $row->partnerName,
                        'partnerDateDate' => $row->partnerDateDate,
                        'partnerDateMonth' => $row->partnerDateMonth,
                        'partnerDateYear' => $row->partnerDateYear,
                        'partnerTimeHr' => $row->partnerTimeHr,
                        'partnerTimeMin' => $row->partnerTimeMin,
                        'partnerTimeSec' => $row->partnerTimeSec,
                        'partnerBirthCountry' => $row->partnerBirthCountry,
                        'partnerBirthState' => $row->partnerBirthState,
                        'partnerBirthCity' => $row->partnerBirthCity,
                        'created_date' => $row->created_date,
                        'created_time' => $row->created_time,
                        'status' => $row->status,
                        'paymentmethod' => $row->paymentmethod,
                        'questiontime' => $row->questiontime,
                        'questionyaminute' => $row->questionyaminute,
                        'amount' => $row->amount,
                        'order_id' => $row->order_id,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }


	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by product id
     */
    public function get_list_of_buy_product_by_id($order_payment_id) {
        $db_result = $this->db->get_where('mayas_payment_product`', array('FK_order_payment_id'=>$order_payment_id ));
		if ($db_result && $db_result->num_rows() > 0) {
		$data = array();
		$data_value = array();
		foreach($db_result->result() as $row){
		        if(!array_key_exists($row->payment_product_id, $data)) {
				    $data[$row->payment_product_id] = array ();
					}
				if (array_key_exists($row->payment_product_id, $data)){
				     $data[$row->payment_product_id] = array(
					 'payment_product_id'=> $row->payment_product_id,
					 'product_id'=> $row->product_id,
					 'product_price_ind'=> $row->product_price_ind,
					 'product_price_usd'=> $row->product_price_usd,
					 'product_image_list'=> $row->product_image_list,
					 'product_categories'=> $row->product_categories,
					 'product_name'=> $row->product_name,
					 'qty'=> $row->qty,
					 );
			         array_push($data_value, $data[$row->payment_product_id]);
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}


	//-------------------------------------------------------------------------
    /*
     * This function is used to get details of all products by product id
     */
    public function get_list_of_buy_service_by_id($order_payment_id) {
        $db_result = $this->db->get_where('mayas_payment_service`', array('FK_order_payment_id'=>$order_payment_id ));
		if ($db_result && $db_result->num_rows() > 0) {
		$data = array();
		$data_value = array();
		foreach($db_result->result() as $row){
		        if(!array_key_exists($row->payment_service_id, $data)) {
				    $data[$row->payment_service_id] = array ();
					}
				if (array_key_exists($row->payment_service_id, $data)){
				     $data[$row->payment_service_id] = array(
					 'payment_service_id'=> $row->payment_service_id,
					 'consultantType' => $row->consultantType,
					'full_name'  => $row->full_name,
					'gender'  => $row->gender,
					'inputDateDate' => $row->inputDateDate,
					'inputTimeMin'  => $row->inputTimeMin,
					'birthCountry'  => $row->birthCountry,
					'birthState'  => $row->birthState,
					'birthCity'  => $row->birthCity,
					'inputEmail'  => $row->inputEmail,
					'inputContact'  => $row->inputContact,
					'inputQuestion'  => $row->inputQuestion,
					'otherInfomation'  => $row->otherInfomation,

					'educationQualification' => $row->educationQualification,
					'currentProfession' => $row->currentProfession,
					'partnerName'  => $row->partnerName,
					'partnerDateDate' => $row->partnerDateDate,
					'partnerTimeMin'  => $row->partnerTimeMin,
					'partnerBirthCountry'  => $row->partnerBirthCountry,
					'partnerBirthState'  => $row->partnerBirthState,
					'partnerBirthCity'  => $row->partnerBirthCity,
					 );
			         array_push($data_value, $data[$row->payment_service_id]);
				}
			}
			return $data_value;
		}else  {
            return FALSE;
        }
	}

		 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all blogs
     */
    public function get_list_of_all_instance_consultation_id($instance_id) {
        $db_result = $this->db->get_where('mayas_instance_consultation', array('instance_id'=>$instance_id ));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array();
                }
                if (array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array(
                         'instance_price_before' => $row->instance_price_before,
                         'instance_price_inr' => $row->instance_price_inr,
                        'instance_price_d' => $row->instance_price_d,
                        'instance_pic' => $row->instance_pic,
                        'instance_detail' => $row->instance_detail,
                        'instance_type' => $row->instance_type,
                        'instance_id' => $row->instance_id,
                        'instance_type_duration' => $row->instance_type_duration,

                    );
                    array_push($data_value, $data[$row->instance_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

		 //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all blogs
     */
    public function get_list_of_all_instance_consultation() {
        $db_result = $this->db->get_where('mayas_instance_consultation');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array();
                }
                if (array_key_exists($row->instance_id, $data)) {
                    $data[$row->instance_id] = array(
                        'instance_price_inr' => $row->instance_price_inr,
                        'instance_price_before' => $row->instance_price_before,
                        'instance_price_d' => $row->instance_price_d,
                        'instance_pic' => $row->instance_pic,
                        'instance_detail' => $row->instance_detail,
                        'instance_type' => $row->instance_type,
                        'instance_id' => $row->instance_id,
                        'instance_type_duration' => $row->instance_type_duration,
                    );
                    array_push($data_value, $data[$row->instance_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }


	//--------------------------------------------------------------------
		/**
		 * Handles requests for adding new blog into database
		 *
		 * @access		public
		 * @since		1.0.0
		 */
		public function add_new_instance_consultation() {
		   // date_default_timezone_set('Asia/Kolkata');
			//print_r($_POST['services_description']);
			 $instance_id = (int)$_POST['instance_id'];
			$data = array(
				'instance_price_before' => $_POST['instance_price_before'],
				'instance_price_inr' => $_POST['instance_price_inr'],
				'instance_price_d' => $_POST['instance_price_d'],
				'instance_pic' => $_POST['instance_pic'],
				'instance_detail' => $_POST['instance_detail'],
				'instance_type' => $_POST['instance_type'],
				'instance_type_duration' => $_POST['instance_type_duration'],
			);
			if($instance_id==0){
				$result = $this->db->insert('mayas_instance_consultation', $data);
			}else{
				$result = $this->db->update('mayas_instance_consultation', $data,array('instance_id'=>$instance_id));
			}
			//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));


			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}


			//-------------------------------------------------------------------------
    /*
     * This function is used to to remove image from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_instance_consultation() {

        $result = $this->db->delete('mayas_instance_consultation', array('instance_id' => $_POST['edit_instance_id']));

            return TRUE;

    }


		 //--------------------------------------------------------------------
    /**
     * Handles requests for edit blog information in database.
     *
     * @access		public
     * @since		1.0.0
     */
    public function edit_instance_information() {
        $data = array(
            'instance_price_inr' => $_POST['edit_instance_price_inr'],
            'instance_price_d' => $_POST['edit_instance_price_d'],
            'instance_pic' => $_POST['instance_pic'],
            'instance_type' => $_POST['edit_instance_type'],
            'instance_type_duration' => $_POST['edit_instance_type_duration'],
            'instance_detail' => nl2br(htmlentities($_POST['edit_instance_detail'], ENT_QUOTES, 'UTF-8')),
        );
        $result = $this->db->update('mayas_instance_consultation', $data, array('instance_id' => $_POST['edit_instance_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


	public function update_consultation_massages() {
		 $massage_id = (int)$_POST['massage_id'];
		$data = array(
			'custom_message' => $_POST['custom_message'],
			'custom_message1' => $_POST['custom_message1'],
			'custom_message_paid' => $_POST['custom_message_paid'],
			'default_massage' => $_POST['default_massage'],
			'paid_massage' => $_POST['paid_massage'],
			'start_date' => $_POST['start_date'],
			'end_date' => $_POST['end_date'],
            'vastu_default_msg' => $_POST['vastu_default_msg'],
            'vastu_leave_msg' =>$_POST['vastu_leave_msg'],
		);

		$result = $this->db->update('consultation_massages', $data,array('massage_id'=>$massage_id));

		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

		//--------------------------------------------------------------------
    /**
     * Handles requests for adding new lagan into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_student() {

		//print_r($_POST['student_id']);
        $data = array(
            'student_name' => $_POST['student_name'],
        );

		if($_POST['student_id'] == 0){
			$result = $this->db->insert('mayas_student', $data);
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('mayas_student', $data,array('student_id' => $_POST['student_id']));
			return TRUE;
		}

    }

	 public function add_new_database() {

		//print_r($_POST['student_id']);
        $data = array(
            'student_name' => $_POST['student_name'],
			'student_age' => $_POST['student_age'],
			'mothers_name' => $_POST['mothers_name'],
        );

		if($_POST['student_id'] == 0){
			$result = $this->db->insert('mayas_database', $data);
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('mayas_database', $data,array('student_id' => $_POST['student_id']));
			return TRUE;
		}

    }



 //--------------------------------------------------------------------
    /**
     * Handles requests for adding new bespoke services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_bespoke_services() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
		$service_id = (int)$_POST['service_id'];
        $data = array(

             'services_name' => $_POST['services_name'],
            'services_type' => $_POST['services_type'],
            'col' => $_POST['col'],
            'services_main_description' => $_POST['services_main_description'],
            'services_description' => $_POST['services_description'],
            'consultant_type' => $_POST['consultant_type'],
            'services_image' => $_POST['services_image'],
            'service_detail_image' => $_POST['service_detail_image'],
            'service_step_image' => $_POST['service_step_image'],
            'premium_type' => $_POST['premium_type'],
			 'services_url' => $_POST['services_url'],
            'services_title' => $_POST['services_title'],
            'services_dec_meta' => $_POST['services_dec_meta'],
            'services_keyword_meta' => $_POST['services_keyword_meta'],
        );
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));
		if($service_id==0){
        $result = $this->db->insert('mayas_bespoke_services', $data);
		}else {
			$result = $this->db->update('mayas_bespoke_services', $data,array('service_id'=>$service_id));
		}
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    //-------------------------------------------------------------------------
	 /*
     * This function is used to get list of all bespoke services
     */
    public function get_list_of_all_bespoke_services() {
        $db_result = $this->db->get_where('mayas_bespoke_services', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                        'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'services_main_description' => $row->services_main_description,
                        'services_description' => $row->services_description,
                        'col' => $row->col,
                        'consultant_type' => $row->consultant_type,
                        'services_image' => $row->services_image,
                        'service_id' => $row->service_id,

                        'premium_type' => $row->premium_type,
                    );
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove services from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_bespoke_services() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_bespoke_services', $data, array('service_id' => $_POST['edit_service_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


	//-------------------------------------------------------------------------
    /*
     * This function is used to remove/read contact form details from the list
     *
     * @access		public
     * @since		1.0.0
     */

	    public function get_list_of_all_bespoke_services_by_id($service_id) {
        $db_result = $this->db->get_where('mayas_bespoke_services', array('status' => 'active','service_id' => $service_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array();
                }
                if (array_key_exists($row->service_id, $data)) {
                    $data[$row->service_id] = array(
                            'services_name' => $row->services_name,
                        'services_type' => $row->services_type,
                        'col' => $row->col,
                        'services_main_description' => $row->services_main_description,

                        'consultant_type' => $row->consultant_type,

                        'services_description' => $row->services_description,
                        'services_image' => $row->services_image,
                        'service_detail_image' => $row->service_detail_image,
                        'service_step_image' => $row->service_step_image,
                        'service_id' => $row->service_id,

                        'premium_type' => $row->premium_type,




						'services_url' => $row->services_url,
						'services_title' => $row->services_title,
						'services_dec_meta' => $row->services_dec_meta,
						'services_keyword_meta' => $row->services_keyword_meta,
						'status' => $row->status,


					);
                    array_push($data_value, $data[$row->service_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	 //--------------------------------------------------------------------


 //--------------------------------------------------------------------
    /**
     * Handles requests for adding new bespoke services into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_counter() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);

        $data = array(

             'happy_customer' => $_POST['happy_customer'],
            'birth_store' => $_POST['birth_store'],
            'gemstone_jewellery' => $_POST['gemstone_jewellery'],

        );
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

			$result = $this->db->update('counter', $data,array('count_id'=>1));


            return TRUE;

    }

	public function get_counter_value() {
        $db_result = $this->db->get_where('counter',array('count_id' =>1));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->count_id, $data)) {
                    $data[$row->count_id] = array();
                }
                if (array_key_exists($row->count_id, $data)) {
                    $data[$row->count_id] = array(
                            'happy_customer' => $row->happy_customer,
                        'birth_store' => $row->birth_store,
                        'gemstone_jewellery' => $row->gemstone_jewellery,

                    );
                    array_push($data_value, $data[$row->count_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }


	//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_blog_image(){

		$data = array(
            'img_name' => 'dg',
            'image' => $_POST['image'],

            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		$result = $this->db->insert('web_images', $data);
		if ($result) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	/* =============================================================================== */

	public function get_list_of_images() {
		$db_result = $this->db->get_where('web_images', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->img_id, $data)) {
                    $data[$row->img_id] = array();
                }
                if (array_key_exists($row->img_id, $data)) {
                    $data[$row->img_id] = array(
                        'img_id' => $row->img_id,
						'img_name' => $row->img_name,
						'image' => $row->image,
				        'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->img_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	public function remove_image() {

        $result = $this->db->delete('web_images', array('img_id' => $_POST['img_id']));

            return TRUE;

    }



	public function add_new_coupan_code() {
        date_default_timezone_set('Asia/Kolkata');
         $code_id = (int)$_POST['code_id'];
        $data = array(
            'coupan_code' => $_POST['coupan_code'],
            'code_for' => $_POST['code_for'],
            'discount' => $_POST['discount'],
            'start_date' => $_POST['start_date'],
            'end_date' => $_POST['end_date'],
            'max_users' => $_POST['max_users'],
        );

			if($code_id == 0){
				$result = $this->db->insert('mayas_coupan_code', $data);
			}else{
				$result = $this->db->update('mayas_coupan_code', $data, array('code_id' => $code_id));

			}

		if($result) {
			return TRUE;
		} else {
			return FALSE;
		}
    }


    //-------------------------------------------------------------------------
    /*
     * This function is used to get list of all course
     */
    public function get_list_of_all_coupan() {
        $db_result = $this->db->get_where('mayas_coupan_code', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->code_id, $data)) {
                    $data[$row->code_id] = array();
                }
                if (array_key_exists($row->code_id, $data)) {
                    $data[$row->code_id] = array(
                        'coupan_code' => $row->coupan_code,
                        'discount' => $row->discount,
                        'start_date' => $row->start_date,
                        'end_date' => $row->end_date,
                        'appyer_count' => $row->appyer_count,
                        'code_for' => $row->code_for,
                        'max_users' => $row->max_users,
                        'status' => $row->status,
                        'code_id' => $row->code_id,
                    );
                    array_push($data_value, $data[$row->code_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	public function remove_coupan() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_coupan_code', $data, array('code_id' => $_POST['code_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

	 //--------------------------------------------------------------------
    /**
     * Handles requests for adding new free_consultation into database
     *
     * @access		public
     * @since		1.0.0
     */
    public function add_new_free_consultation() {
        date_default_timezone_set('Asia/Kolkata');
		$consultation_id = (int)$_POST['consultation_id'];
        $data = array(

            'consultation_name' => $_POST['consultation_name'],
            'main_description' => $_POST['main_description'],
            'description' => $_POST['description'],
            'c_image' => $_POST['c_image'],
            'c_detail_image' => $_POST['c_detail_image'],
            'faq_category' => $_POST['faq_category'],
            'banner_id' => $_POST['banner_id'],
            'about_content_id' => $_POST['about_content_id'],

            'detail_image_position' => $_POST['detail_image_position'],
            'url' => $_POST['url'],
            'consultation_type' => $_POST['consultation_type'],
            'title' => $_POST['title'],
            'dec_meta' => $_POST['dec_meta'],
            'keyword_meta' => $_POST['keyword_meta'],
            'q_label' => $_POST['q_label'],
            'q_placeholder' => $_POST['q_placeholder'],
            'q_info' => $_POST['q_info'],
            'is_question1'=> $_POST['is_question1'],
            'q1_label' => $_POST['q1_label'],
            'q1_placeholder' => $_POST['q1_placeholder'],
            'is_question2' => $_POST['is_question2'],
            'q2_label' => $_POST['q2_label'],
            'q2_placeholder' => $_POST['q2_placeholder'],
            'extra_description' => $_POST['extra_description'],
            'services_position' => $_POST['services_position'],

        );
        // var_dump($data);
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));
		if($consultation_id==0){
			$result = $this->db->insert('mayas_consultation_content', $data);
		}else {
			$result = $this->db->update('mayas_consultation_content', $data,array('consultation_id'=>$consultation_id));
		}
        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	//--------------------------------------------------------------------
    /**
     * This function is used to get free_consultation content from database
     *
     * @access		public
     * @since		1.0.0
     */
	public function get_list_of_all_consultation_by_id($consultation_id) {
        $db_result = $this->db->get_where('mayas_consultation_content', array('consultation_id' => $consultation_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultation_name' => $row->consultation_name,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                        'extra_description' => $row->extra_description,
                        'faq_category' => $row->faq_category,
                        'banner_id' => $row->banner_id,
                        'about_content_id' => $row->about_content_id,
                        'c_image' => $row->c_image,
                        'c_detail_image' => $row->c_detail_image,
                        'detail_image_position' => $row->detail_image_position,
						'url' => $row->url,
						'title' => $row->title,
						'dec_meta' => $row->dec_meta,
						'keyword_meta' => $row->keyword_meta,
						'q_label' => $row->q_label,
						'q_placeholder' => $row->q_placeholder,
						'consultation_type' => $row->consultation_type,
						'q_info' => $row->q_info,
                        'is_question1'=> $row->is_question1,
                        'q1_label' => $row->q1_label,
                        'q1_placeholder' => $row->q1_placeholder,
                        'is_question2' => $row->is_question2,
                        'q2_label' => $row->q2_label,
                        'q2_placeholder' => $row->q2_placeholder,
                        'services_position' => $row->services_position,
                    );
                    array_push($data_value, $data[$row->consultation_id]);

                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to get list of all Free Consultation
     */
    public function get_list_of_all_free_consultation() {
        $db_result = $this->db->get_where('mayas_consultation_content');
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultation_name' => $row->consultation_name,
                        'main_description' => $row->main_description,
                        'description' => $row->description,
                        'c_image' => $row->c_image,
						'url' => $row->url,
						'title' => $row->title,
						'dec_meta' => $row->dec_meta,
						'keyword_meta' => $row->keyword_meta,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }

	 //-------------------------------------------------------------------------
    /*
     * This function is used to remove Free Consultation from the list
     *
     * @access		public
     * @since		1.0.0
     */
    public function remove_free_consultation() {

		$result = $this->db->delete('mayas_consultation_content', array('consultation_id' => $_POST['consultation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}
