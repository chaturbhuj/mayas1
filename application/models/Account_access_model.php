<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Account_access_model extends CI_Model {
	
    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_module
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_module() {
		$menu_id = (int)$_POST['menu_id'];
		$data = array(
            'module_name' => $_POST['menu_name'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($menu_id == 0){
			$result = $this->db->insert('admin_module', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('admin_module', $data, array('module_id' => $menu_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_modules
     */
    public function get_list_of_modules() {
		$this->db->order_by('position','ASC');
		$db_result = $this->db->get_where('admin_module', array('status' => 'active'));
	
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->module_id, $data)) {
                    $data[$row->module_id] = array();
                }
                if (array_key_exists($row->module_id, $data)) {
                    $data[$row->module_id] = array(
                        'module_id' => $row->module_id,
						'module_name' => $row->module_name,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->module_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_module from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_module() {
		$data = array(
            'status' => 'old',
        );
        $result = $this->db->update('admin_module', $data, array('module_id' => $_POST['menu_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_submodule 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_submodule() {
		$submodule_id = (int)$_POST['submodule_id'];
		$data = array(
            'FK_module_id' => $_POST['module_id'],
            'submodule_name' => $_POST['submodule_name'],
            'link' => $_POST['link'],
            'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
        );
		if($submodule_id == 0){
			$result = $this->db->insert('admin_submodule', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('admin_submodule', $data, array('submodule_id' => $submodule_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to Update Log Table 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_logdetails($pro_id,$order_id,$vendor_id,$customer_id,$log_details) {
		
		$data = array(
            'product_id' => $pro_id,
            'order_id' => $order_id,
            'vendor_id' =>$vendor_id,
            'customer_id' =>$customer_id,
            'log_details' =>$log_details,
            'created_date' => date('Y-m-d'),
			'created_time' => date('H:i:s'),
        );
		
		$result = $this->db->insert('log', $data);      
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_submodules
     */
    public function get_list_of_submodules() {
		$db_result = $this->db->get_where('admin_submodule', array('status !=' => 'old'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->submodule_id, $data)) {
                    $data[$row->submodule_id] = array();
                }
                if (array_key_exists($row->submodule_id	, $data)) {
                    $data[$row->submodule_id] = array(
                        'submodule_id' => $row->submodule_id	,
						'FK_module_id' => $row->FK_module_id,
						'submodule_name' => $row->submodule_name,
						'link' => $row->link,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->submodule_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_submenu from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_submodule() {
		$data = array(
            'status' => 'old',
        );
        $result = $this->db->update('admin_submodule', $data, array('submodule_id' => $_POST['submodule_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to check availability of email etc.
     */
    public function check_availability() {
        if ($_POST['param'] == 'registration') {
            $db_result = $this->db->get_where('quiz_admin_profile', array('user_email' => $_POST['emp_email']));
        } else if ($_POST['param'] == 'edit_users') {
            $db_result = $this->db->get_where('quiz_admin_profile', array('user_email' => $_POST['emp_email']));
        }else if ($_POST['param'] == 'student_registration') {
            $db_result = $this->db->get_where('quiz_students', array('user_email' => $_POST['emp_email']));
        }
        if ($db_result && $db_result->num_rows() > 0) {
            #param already exists
            return FALSE;
        } else {
            return TRUE;
        }
    }
	
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_emp 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_emp() {
		$user_id = (int)$_POST['emp_id'];
		$hashed_password = sha1('admin' . (md5('admin' . $_POST['passwd'])));
		if($user_id == 0){
			#generate random number
			$capital = implode('',range('A','Z'));
			$small = implode('',range('a','z'));
			$number = implode('',range('0','20'));
			$token = substr(str_shuffle($capital.$small.$number),40);
			
			$data = array(
				'user_name' => $_POST['emp_name'],
				'user_email' => $_POST['emp_email'],
				'passwd' => $hashed_password,
				'phone_no' => $_POST['phone_no'],
				'date' => date('Y-m-d'),
				'time' => date('H:m:s'),
				'verify_code' => $token
			);
			$result = $this->db->insert('quiz_admin_profile', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$data = array(
				'user_name' => $_POST['emp_name'],
				'user_email' => $_POST['emp_email'],
				'phone_no' => $_POST['phone_no'],
			);
			$result = $this->db->update('quiz_admin_profile', $data, array('user_id' => $user_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_submenus
     */
    public function get_list_of_employee() {
		$db_result = $this->db->get_where('quiz_admin_profile', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array();
                }
                if (array_key_exists($row->user_id, $data)) {
                    $data[$row->user_id] = array(
                        'user_id' => $row->user_id,
						'user_name' => $row->user_name,
						'user_email' => $row->user_email,
						'phone_no' => $row->phone_no,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->user_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove_emp from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_emp() {
		$data = array(
            'status' => 'old',
        );
        $result = $this->db->update('quiz_admin_profile', $data, array('user_id' => $_POST['user_id']));
        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_emp 
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_accessibility() {
		$emp_id = (int)$_POST['emp_id'];
		$delete_result = $this->db->delete('admin_emp_access',array('emp_id'=>$emp_id));
		if($delete_result){
			foreach($_POST['access_define'] as $access){
				$data = array(
					'emp_id' => $emp_id,
					'module_id' => $access['module_id'],
					'submodule_id' => $access['submodule_id'],
					'date' => date('Y-m-d'),
					'time' => date('H:i:s'),
				);
				$result = $this->db->insert('admin_emp_access', $data);  
			}
			/*$emp_id = (int)$_POST['emp_id'];
			$delete_result = $this->db->delete('admin_emp_access',array('emp_id'=>$emp_id));
			foreach($_POST['subject_list'] as $access){
				
				$data2 = array(
				'emp_id' => $emp_id,
				'subject_id' => $access,
				'date' => date('Y-m-d'),
				'time' => date('H:m:s'),
				);
			$result = $this->db->insert('admin_emp_access', $data2);  
			}*/
			
				return TRUE;
			
		}else {
			return TRUE;
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_emp_access_by_id
     */
    public function get_list_of_emp_access_by_id($emp_id) {
		$db_result = $this->db->get_where('admin_emp_access', array('emp_id' => $emp_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->index_id, $data)) {
                    $data[$row->index_id] = array();
                }
                if (array_key_exists($row->index_id, $data)) {
                    $data[$row->index_id] = array(
                        'index_id' => $row->index_id,
						'emp_id' => $row->emp_id,
						'module_id' => $row->module_id,
						'submodule_id' => $row->submodule_id,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->index_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
		
		
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_emp_access_by_id
     */
    public function get_list_of_emp_subject_access_by_id($emp_id) {
			$db_result1 = $this->db->get_where('quez_emp_subject_access', array('emp_id' => $emp_id));
			if ($db_result1 && $db_result1->num_rows() > 0) {
				$data1 = array();
				$data_value1 = array();
				foreach ($db_result1->result() as $row) {
					if (!array_key_exists($row->index_id, $data1)) {
						$data1[$row->index_id] = array();
					}
					if (array_key_exists($row->index_id, $data1)) {
							$data1[$row->index_id] = array(
							'index_id' => $row->index_id,
							'emp_id' => $row->emp_id,
							'subject_id' => $row->subject_id,
							'date'=> $row->date,
							'time'=> $row->time,
						);
						array_push($data_value1, $data1[$row->index_id]);
					}
				}
		
        return $data_value1;
        } else {
            return FALSE;
		}
	}
	
	
}
     