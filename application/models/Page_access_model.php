<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Page_access_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('string');
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_list_of_consultation($last) {
		if(!is_numeric($last)){
			$last = 100 ;
		}
		
		if($last != 0){
			$this->db->limit($last);
			$this->db->order_by("consultation_id","desc");
			$db_result = $this->db->get_where('mayas_consultation', array('status' => 'active'));
		}else{
			$this->db->limit(3000);
			$this->db->order_by("consultation_id","desc");
			 $db_result = $this->db->get_where('mayas_consultation', array('status' => 'active'));
		}
       
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
				
				$match_count =0 ;
				if($last != 0){
					$db_result_1 = $this->db->get_where('mayas_consultation', array('inputDateDate' =>$row->inputDateDate ,'inputTimeMin' => $row->inputTimeMin));
					$match_count = $db_result_1->num_rows() ;
				}
				
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultantType' => $row->consultantType,
                        'full_name' => $row->full_name,
                        'gender' => $row->gender,
                        'inputDateDate' =>  date('d F, Y', strtotime($row->inputDateDate)),//$row->inputDateDate,
                        'inputDateMonth' => $row->inputDateMonth,
                        'inputDateYear' => $row->inputDateYear,
                        'inputTimeHr' => $row->inputTimeHr,
                        'inputTimeMin' => $row->inputTimeMin,
                        'inputTimeSec' => $row->inputTimeSec,
                        'birthCountry' => $row->birthCountry,
                        'appointment_yes' => $row->appointment_yes,
						
                        'birthState' => $row->birthState,
                        'birthCity' => $row->birthCity,
                        'inputEmail' => $row->inputEmail,
                        'inputContact' => $row->inputContact,
                        'inputQuestion' => $row->inputQuestion,
                        'otherInfomation' => $row->otherInfomation,
						
                        'educationQualification' => $row->educationQualification,
                        'currentProfession' => $row->currentProfession,
                        'partnerName' => $row->partnerName,
                        'partnerDateDate' => $row->partnerDateDate,
                        'partnerDateMonth' => $row->partnerDateMonth,
                        'partnerDateYear' => $row->partnerDateYear,
                        'partnerTimeHr' => $row->partnerTimeHr,
                        'partnerTimeMin' => $row->partnerTimeMin,
                        'partnerTimeSec' => $row->partnerTimeSec,
						
						'partnerBirthCountry' => $row->partnerBirthCountry,
                        'partnerBirthState' => $row->partnerBirthState,
                        'partnerBirthCity' => $row->partnerBirthCity,
                        'created_date' => $row->created_date,
                        'created_time' => $row->created_time,
                        'questiontime' => $row->questiontime,
                        'questionyaminute' => $row->questionyaminute,
                        'question1' => $row->question1,
                        'question2' => $row->question2,
                        'question1_label' => $row->question1_label,
                        'question2_label' => $row->question2_label,
                        'status' => $row->status,
                        'match_count' => $match_count,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_list_of_consultation_by_range($start, $end) {
		
		$this->db->order_by("consultation_id","desc");
		$this->db->where(array('consultation_id >' => $start));
		$this->db->where(array('consultation_id <=' => $end));
		$db_result = $this->db->get_where('mayas_consultation', array('status' => 'active'));
		
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
				
				$match_count =0 ;
				
				$db_result_1 = $this->db->get_where('mayas_consultation', array('inputDateDate' =>$row->inputDateDate ,'inputTimeMin' => $row->inputTimeMin));
				$match_count = $db_result_1->num_rows() ;
				
				
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultantType' => $row->consultantType,
                        'full_name' => $row->full_name,
                        'gender' => $row->gender,
                        'inputDateDate' =>  date('d F, Y', strtotime($row->inputDateDate)),//$row->inputDateDate,
                        'inputDateMonth' => $row->inputDateMonth,
                        'inputDateYear' => $row->inputDateYear,
                        'inputTimeHr' => $row->inputTimeHr,
                        'inputTimeMin' => $row->inputTimeMin,
                        'inputTimeSec' => $row->inputTimeSec,
                        'birthCountry' => $row->birthCountry,
                        'appointment_yes' => $row->appointment_yes,
						
                        'birthState' => $row->birthState,
                        'birthCity' => $row->birthCity,
                        'inputEmail' => $row->inputEmail,
                        'inputContact' => $row->inputContact,
                        'inputQuestion' => $row->inputQuestion,
                        'otherInfomation' => $row->otherInfomation,
						
                        'educationQualification' => $row->educationQualification,
                        'currentProfession' => $row->currentProfession,
                        'partnerName' => $row->partnerName,
                        'partnerDateDate' => $row->partnerDateDate,
                        'partnerDateMonth' => $row->partnerDateMonth,
                        'partnerDateYear' => $row->partnerDateYear,
                        'partnerTimeHr' => $row->partnerTimeHr,
                        'partnerTimeMin' => $row->partnerTimeMin,
                        'partnerTimeSec' => $row->partnerTimeSec,
						
						'partnerBirthCountry' => $row->partnerBirthCountry,
                        'partnerBirthState' => $row->partnerBirthState,
                        'partnerBirthCity' => $row->partnerBirthCity,
                        'created_date' => $row->created_date,
                        'created_time' => $row->created_time,
                        'status' => $row->status,
                        'match_count' => $match_count,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_list_of_paid_consultation() {
		
		$this->db->order_by("order_payment_id","desc");
		$db_result1 = $this->db->get_where('mayas_order_payment');
		if ($db_result1 && $db_result1->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result1->result() as $row1) {
                
				$this->db->order_by("payment_service_id","desc");
				$db_result = $this->db->get_where('mayas_payment_service',array('FK_order_payment_id' => $row1->order_payment_id));
				
				if ($db_result && $db_result->num_rows() > 0) {
					$data = array();
					//$data_value = array();
					foreach ($db_result->result() as $row) {
						$match_count =0 ;
						/* if($last != 0){
							$db_result_1 = $this->db->get_where('mayas_consultation', array('inputDateDate' =>$row->inputDateDate ,'inputTimeMin' => $row->inputTimeMin));
							$match_count = $db_result_1->num_rows() ;
						} */
						if (!array_key_exists($row1->order_payment_id, $data)) {
							$data[$row1->order_payment_id] = array();
						}
						if (array_key_exists($row1->order_payment_id, $data)) {
							$data[$row1->order_payment_id] = array(
								'order_payment_id' => $row1->order_payment_id,
								'order_id' => $row1->order_id,
								'currency' => $row1->currency,
								'payment_method' => $row1->payment_method,
								'grandtotal' => $row1->grandtotal,
								'discount' => $row1->discount,
								'payment_date' => $row1->payment_date,
								'appointment_yes' => $row1->appointment_yes,
								'payment_service_id' => $row->payment_service_id,
								'FK_order_payment_id' => $row->FK_order_payment_id,
								'consultantType' => $row->consultantType,
								'full_name' => $row->full_name,
								'gender' => $row->gender,
								'inputDateDate' => $row->inputDateDate,
								'inputDateMonth' => $row->inputDateMonth,
								'inputDateYear' => $row->inputDateYear,
								'inputTimeHr' => $row->inputTimeHr,
								'inputTimeMin' => $row->inputTimeMin,
								'inputTimeSec' => $row->inputTimeSec,
								'birthCountry' => $row->birthCountry,
								'birthState' => $row->birthState,
								'birthCity' => $row->birthCity,
								'inputEmail' => $row->inputEmail,
								'inputContact' => $row->inputContact,
								'inputQuestion' => $row->inputQuestion,
								'otherInfomation' => $row->otherInfomation,
								'educationQualification' => $row->educationQualification,
								'currentProfession' => $row->currentProfession,
								'partnerName' => $row->partnerName,
								'partnerDateDate' => $row->partnerDateDate,
								'partnerDateMonth' => $row->partnerDateMonth,
								'partnerDateYear' => $row->partnerDateYear,
								'partnerTimeHr' => $row->partnerTimeHr,
								'partnerTimeMin' => $row->partnerTimeMin,
								'partnerTimeSec' => $row->partnerTimeSec,
								'partnerBirthCountry' => $row->partnerBirthCountry,
								'partnerBirthState' => $row->partnerBirthState,
								'partnerBirthCity' => $row->partnerBirthCity,
								'questiontime' => $row->questiontime,
								'questionyaminute' => $row->questionyaminute,
								'single_or_married' => $row->single_or_married,
								'question_label' => $row->question_label,
								'question1_label' => $row->question1_label,
								'question2_label' => $row->question2_label,
								'question1' => $row->question1,
								'question2' => $row->question2,
								'amount' => $row->amount,
								'status' => $row1->status,
								'match_count' => $match_count,
								
								
							);
							array_push($data_value, $data[$row1->order_payment_id]);
						}
					}
				}
                
            }
			//var_dump($data_value);
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_list_of_consultation_matching($consultation_id) {
		$db_result_1 = $this->db->get_where('mayas_consultation', array('consultation_id' =>$consultation_id));
		$r34 = $db_result_1->result();
		$date = $r34[0]->inputDateDate ;
		$time = $r34[0]->inputTimeMin ;
		if($date != '' || $time !=''){
			$db_result = $this->db->get_where('mayas_consultation', array('inputDateDate' =>$date ,'inputTimeMin' => $time));
		
			if ($db_result && $db_result->num_rows() > 0) {
				$data = array();
				$data_value = array();
				foreach ($db_result->result() as $row) {
					if (!array_key_exists($row->consultation_id, $data)) {
						$data[$row->consultation_id] = array();
					}
					
					$match_count =0 ;
					
					if (array_key_exists($row->consultation_id, $data)) {
						$data[$row->consultation_id] = array(
							'consultation_id' => $row->consultation_id,
							'consultantType' => $row->consultantType,
							'full_name' => $row->full_name,
							'gender' => $row->gender,
							'inputDateDate' => date('d F, Y', strtotime($row->inputDateDate)),
							'inputDateMonth' => $row->inputDateMonth,
							'inputDateYear' => $row->inputDateYear,
							'inputTimeHr' => $row->inputTimeHr,
							'inputTimeMin' => $row->inputTimeMin,
							'inputTimeSec' => $row->inputTimeSec,
							'birthCountry' => $row->birthCountry,
							'appointment_yes' => $row->appointment_yes,
							
							'birthState' => $row->birthState,
							'birthCity' => $row->birthCity,
							'inputEmail' => $row->inputEmail,
							'inputContact' => $row->inputContact,
							'inputQuestion' => $row->inputQuestion,
							'otherInfomation' => $row->otherInfomation,
							
							'educationQualification' => $row->educationQualification,
							'currentProfession' => $row->currentProfession,
							'partnerName' => $row->partnerName,
							'partnerDateDate' => $row->partnerDateDate,
							'partnerDateMonth' => $row->partnerDateMonth,
							'partnerDateYear' => $row->partnerDateYear,
							'partnerTimeHr' => $row->partnerTimeHr,
							'partnerTimeMin' => $row->partnerTimeMin,
							'partnerTimeSec' => $row->partnerTimeSec,
							
							'partnerBirthCountry' => $row->partnerBirthCountry,
							'partnerBirthState' => $row->partnerBirthState,
							'partnerBirthCity' => $row->partnerBirthCity,
							'created_date' => $row->created_date,
							'created_time' => $row->created_time,
							'status' => $row->status,
							'match_count' => $match_count,
						);
						array_push($data_value, $data[$row->consultation_id]);
					}
				}
				return $data_value;
			} else {
				return FALSE;
			}
		}else{
			return FALSE;
		}
	
	
	}
	
	
	public function update_all_sms_consultants() {
       $ids = $_POST['category_id'];
	   if($ids !=''){ 
		   foreach($ids as $value){
				$data = array(
					'status' => 'active',
					'type_for' => 'SMS',
					'content' => $_POST['typend_msg'],
					'Fk_consultation_id' => $value,
					'date' => date('Y-m-d'),
					'time' => date('H:i:s'),
				);
				$result = $this->db->insert('mayas_admin_msgs_to_consultants', $data);
		   }
			return TRUE;
	   }else{
		   return FALSE;
	   }
    }
	
	public function update_all_email_consultants() {
       $email = $_POST['email'];
		$data = array(
			'status' => 'active',
			'type_for' => 'EMAIL',
			'content' => $_POST['typend_msg'],
			'Fk_consultation_id' => $_POST['id'],
			'date' => date('Y-m-d'),
			'time' => date('H:i:s'),
		);
		$result = $this->db->insert('mayas_admin_msgs_to_consultants', $data);
			if($this->db->affected_rows()){
				
				$this->load->library('email');			
				$subject = "Welcome to ".WEBSITE_NAME;
				
				$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
				$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 100px;"></a></div><br/><br/>';
				$message .= 'Dear  ' . $_POST['full_name'] . ' ,<br/><br/>';
				$message .= 'Your consultation Code : '.$_POST['id'] ;
				$message .= '<p> ' . $_POST['typend_msg'] . '</p>';
				$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
				$message .= '</div></div>';
				//print_r($message);
				$e_config = array(
					'charset'=>'utf-8',
					'wordwrap'=> TRUE,
					'mailtype' => 'html',
					'priority' => '1'
				);
				//$to = COMPANY_EMAIL_ID;
				$this->email->initialize($e_config);
				$this->email->from(COMPANY_EMAIL_ID);
				$this->email->to($email); 
				$this->email->subject($subject);
				$this->email->message($message);	
				$this->email->send();
				
				
				
				return TRUE;
		    }else{
			   return FALSE;
		    }
    }
	
	public function update_status_of_appointment() {
        $data = array(
            'appointment_yes' => $_POST['category_status'],
        );
        $result = $this->db->update('mayas_consultation', $data, array('consultation_id' => $_POST['consultation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	
	public function update_notes_of_consutatnts() {
			
			$data = array(
				'status' => 'active',
				'type_for' => 'NOTE',
				'content' => $_POST['typed_note'],
				'Fk_consultation_id' => $_POST['consultation_id'],
				'date' => date('Y-m-d'),
				'time' => date('H:i:s'),
			);
		
		$result = $this->db->insert('mayas_admin_msgs_to_consultants', $data);
		
		if ($this->db->affected_rows()) {
			return TRUE;
	    }else{
		   return FALSE;
	    }
    }
	
	public function get_all_notes_of_consutatnt_id() {
		$consultation_id = $_POST['consultation_id'];
        $db_result = $this->db->get_where('mayas_admin_msgs_to_consultants', array('type_for' => 'NOTE' ,'status' => 'active' ,'Fk_consultation_id' =>$consultation_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->m_id, $data)) {
                    $data[$row->m_id] = array();
                }
                if (array_key_exists($row->m_id, $data)) {
                    $data[$row->m_id] = array(
                        'm_id' => $row->m_id,
                        'content' => $row->content,
                        'created_date' => $row->date,
                        'created_time' => $row->time,
                    );
                    array_push($data_value, $data[$row->m_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove lagan from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_consultation() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_consultation', $data, array('consultation_id' => $_POST['consultation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_list_of_gemstone_recommendation() {
        $db_result = $this->db->get_where('mayas_gemstone_recommendation', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->gemstone_recommendation_id, $data)) {
                    $data[$row->gemstone_recommendation_id] = array();
                }
                if (array_key_exists($row->gemstone_recommendation_id, $data)) {
                    $data[$row->gemstone_recommendation_id] = array(
                        'gemstone_recommendation_id' => $row->gemstone_recommendation_id,
                        'full_name' => $row->full_name,
                        'gender' => $row->gender,
                        'inputDateDate' => $row->inputDateDate,
                        'inputDateMonth' => $row->inputDateMonth,
                        'inputDateYear' => $row->inputDateYear,
                        'inputTimeHr' => $row->inputTimeHr,
                        'inputTimeMin' => $row->inputTimeMin,
                        'inputTimeSec' => $row->inputTimeSec,
                        'birthCountry' => $row->birthCountry,
						
                        'birthState' => $row->birthState,
                        'birthCity' => $row->birthCity,
                        'inputEmail' => $row->inputEmail,
                        'inputContact' => $row->inputContact,
                        
                        'created_date' => $row->created_date,
                        'created_time' => $row->created_time,
                        'status' => $row->status,
                    );
                    array_push($data_value, $data[$row->gemstone_recommendation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	 
	//-------------------------------------------------------------------------
    /*
     * This function is used to remove remove_gemstone_recommendation the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_gemstone_recommendation() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_gemstone_recommendation', $data, array('gemstone_recommendation_id' => $_POST['gemstone_recommendation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to update_popup_ads
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_popup_ads() {
		date_default_timezone_set('Asia/Kolkata');
		$popup_ads_id = (int)$_POST['popup_ads_id'];
		$data = array(
            'popup_ads_image' => $_POST['popup_ads_image'],
            'navigation_link' => $_POST['navigation_link'],
            'popup_status' => $_POST['popup_status'],
            'free_consultation_image' => $_POST['free_consultation_image'],
            'free_gemstone_recommendation_image' => $_POST['free_gemstone_recommendation_image'],
			'date' => date('Y-m-d'),
			'time' => date('H:m:s'),
            
        );
		if($popup_ads_id == 0){
			$result = $this->db->insert('mayas_popup_ads', $data);      
			if ($result) {
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			$result = $this->db->update('mayas_popup_ads', $data, array('popup_ads_id' => $popup_ads_id));      
			if ($this->db->affected_rows()) {
				return TRUE;
			} else {
				return FALSE;
			}
		}  	
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used to get_list_of_services_page_heading
     */
    public function get_list_of_popup_ads() {
		$db_result = $this->db->get_where('mayas_popup_ads', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->popup_ads_id, $data)) {
                    $data[$row->popup_ads_id] = array();
                }
                if (array_key_exists($row->popup_ads_id, $data)) {
                    $data[$row->popup_ads_id] = array(
                        'popup_ads_id' => $row->popup_ads_id,
						'popup_ads_image' => $row->popup_ads_image,
						'navigation_link' => $row->navigation_link,
						'popup_status' => $row->popup_status,
						'free_consultation_image' => $row->free_consultation_image,
						'free_gemstone_recommendation_image' => $row->free_gemstone_recommendation_image,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->popup_ads_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	
		//-------------------------------------------------------------------------
    /*
     * This function is used to get public content_by content category id 
     */
    public function get_public_content_by_content_cat_id($content_cat_id) {
		$db_result = $this->db->get_where('web_public_content', array('status' => 'active','content_cat_id' => $content_cat_id));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array();
                }
                if (array_key_exists($row->content_id, $data)) {
                    $data[$row->content_id] = array(
                        'content_id' => $row->content_id,
						'content_cat_id' => $row->content_cat_id,
						'content' => $row->content,
						'heading' => $row->heading,
						'date'=> $row->date,
                    );
                    array_push($data_value, $data[$row->content_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	public function update_public_content() {
		$content_id = $_POST['content_id'];
		$user_id = $this->session->userdata('userID');
        $data = array(
			'FK_added_by' => $user_id,
			'content_cat_id' => $_POST['content_cat_id'],
			'content' => $_POST['content'],
			'heading' => $_POST['heading'],
			'date' => date('Y-m-d'),
            'time' => date('H:m:s'),			
        );
		
		
			$result = $this->db->update('web_public_content', $data, array('content_id' => $content_id));
		
		if($content_id != 0){
			return true;
		}else{
			return false;
		}
	}
	
	//-------------------------------------------------------------------------
    /*
     * This function is used toget_list_of_consultation
     */
    public function get_search_result() {
		$search_inputTimeMin = '';
		$search_inputDateDate = '';
		if($_POST['code_id']){
			$this->db->where('consultation_id',$_POST['code_id']);
			$db_result = $this->db->get('mayas_consultation');
			if ($db_result && $db_result->num_rows() > 0) {
				foreach ($db_result->result() as $row) {
					$search_inputTimeMin = $row->inputTimeMin;
					$search_inputDateDate = $row->inputDateDate;
				}
			}
		}
		
		
		if($_POST['search_full_name']){
			$this->db->like('full_name',$_POST['search_full_name']);
		}
		if($_POST['search_inputEmail']){
			$this->db->like('inputEmail',$_POST['search_inputEmail']);
		}
		if($_POST['search_birthCity']){
			$this->db->like('birthCity',$_POST['search_birthCity']);
		}
		if($_POST['search_inputContact']){
			$this->db->where('inputContact',$_POST['search_inputContact']);
		}
		if($_POST['code_id']){
			//$this->db->where('consultation_id',$_POST['code_id']);
		}
		if($_POST['search_inputTimeMin']){
			$time = $_POST['search_inputTimeMin'];
			$start_time = date('H:i', strtotime($time.'-120 minutes'));
			$end_time = date('H:i', strtotime($time.'+120 minutes'));
			
			$array = array('inputTimeMin >=' => $start_time, 'inputTimeMin <=' => $end_time);
			$this->db->where($array);
		}else if($search_inputTimeMin){
			$time = $search_inputTimeMin;
			/*$start_time = date('H:i', strtotime($time.'-120 minutes'));
			$end_time = date('H:i', strtotime($time.'+120 minutes'));
			$start_time_ = ((float)(date('H', strtotime($time.'-120 minutes'))));
			$end_time_ = ((float)(date('H', strtotime($time.'+120 minutes'))));*/
			
			$start_time = '00:00';
			$end_time = '23:59';
			
			
			$array = array('inputTimeMin >=' => $start_time, 'inputTimeMin <=' => $end_time);
			$this->db->where($array);
		}
		
		if($_POST['search_inputDateDate']){
			$date = $_POST['search_inputDateDate'];
			$prev_date = date('Y-m-d', strtotime($date .' -1 day'));
			$next_date = date('Y-m-d', strtotime($date .' +1 day'));
			$array = array('inputDateDate >=' => $prev_date, 'inputDateDate <=' => $next_date);
			$this->db->where($array);
		}else if($search_inputDateDate){
			$date = $search_inputDateDate;
			$prev_date = date('Y-m-d', strtotime($date .' -1 day'));
			$next_date = date('Y-m-d', strtotime($date .' +1 day'));
			$array = array('inputDateDate >=' => $prev_date, 'inputDateDate <=' => $next_date);
			$this->db->where($array);
		}
		
		$this->db->order_by("consultation_id","desc");
		//$this->db->where('status','active');
		$db_result = $this->db->get('mayas_consultation');
		//var_dump($db_result->num_rows());
		//print_r($this->db->last_query());
		
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array();
                }
				
                if (array_key_exists($row->consultation_id, $data)) {
                    $data[$row->consultation_id] = array(
                        'consultation_id' => $row->consultation_id,
                        'consultantType' => $row->consultantType,
                        'full_name' => $row->full_name,
                        'gender' => $row->gender,
                        'inputDateDate' =>  date('d F, Y', strtotime($row->inputDateDate)),//$row->inputDateDate,
                        'inputDateMonth' => $row->inputDateMonth,
                        'inputDateYear' => $row->inputDateYear,
                        'inputTimeHr' => $row->inputTimeHr,
                        'inputTimeMin' => $row->inputTimeMin,
                        'inputTimeSec' => $row->inputTimeSec,
                        'birthCountry' => $row->birthCountry,
                        'appointment_yes' => $row->appointment_yes,
						
                        'birthState' => $row->birthState,
                        'birthCity' => $row->birthCity,
                        'inputEmail' => $row->inputEmail,
                        'inputContact' => $row->inputContact,
                        'inputQuestion' => $row->inputQuestion,
                        'otherInfomation' => $row->otherInfomation,
						
                        'educationQualification' => $row->educationQualification,
                        'currentProfession' => $row->currentProfession,
                        'partnerName' => $row->partnerName,
                        'partnerDateDate' => $row->partnerDateDate,
                        'partnerDateMonth' => $row->partnerDateMonth,
                        'partnerDateYear' => $row->partnerDateYear,
                        'partnerTimeHr' => $row->partnerTimeHr,
                        'partnerTimeMin' => $row->partnerTimeMin,
                        'partnerTimeSec' => $row->partnerTimeSec,
						
						'partnerBirthCountry' => $row->partnerBirthCountry,
                        'partnerBirthState' => $row->partnerBirthState,
                        'partnerBirthCity' => $row->partnerBirthCity,
                        'created_date' => $row->created_date,
                        'created_time' => $row->created_time,
                        'status' => $row->status,
                        'question1' => $row->question1,
                        'question2' => $row->question2,
                        'question1_label' => $row->question1_label,
                        'question2_label' => $row->question2_label,
                        //'match_count' => $match_count,
                    );
                    array_push($data_value, $data[$row->consultation_id]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
	}
	
	public function mayas_vastu_consultation() {
        $this->db->order_by("vastu_id ","desc");
		$db_result = $this->db->get_where('mayas_vastu_consultation', array('status' => 'active'));
        if ($db_result && $db_result->num_rows() > 0) {
            $data = array();
            $data_value = array();
            foreach ($db_result->result() as $row) {
                if (!array_key_exists($row->vastu_id , $data)) {
                    $data[$row->vastu_id ] = array();
                }
                if (array_key_exists($row->vastu_id , $data)) {
                    $data[$row->vastu_id ] = array(
                        
						'consultantType' => $row->consultantType,
						'inputName' => $row->inputName,
						'inputGender' => $row->inputGender,
						'inputDateDate' => $row->inputDateDate,
						'inputTimeMin' => $row->inputTimeMin,
						'birthCountry'=> $row->birthCountry,
						'birthState' => $row->birthState,
						'birthCity' => $row->birthCity,
						'inputEmail' => $row->inputEmail,
						'inputEmail' => $row->inputEmail,
						'address' => $row->address,
						'propertydirection' => $row->propertydirection,
						'inputContact' => $row->inputContact,
						'created_date' => $row->created_date,
						'created_time'=> $row->created_time,
						'appointment_yes' => $row->appointment_yes,
						'image' => $row->image,
						'vastu_id' => $row->vastu_id,
						'order_payment_id' => $row->order_payment_id,
						'status' => $row->status,
                        'file_names' => json_decode($row->image),
						
                    );
                    array_push($data_value, $data[$row->vastu_id ]);
                }
            }
            return $data_value;
        } else {
            return FALSE;
        }
    }
	
	/*
     * This function is used to remove vastu consultation from the list
     * 
     * @access		public
     * @since		1.0.0
     */
    public function remove_vastu_consultation() {
        $data = array(
            'status' => 'old',
        );
        $result = $this->db->update('mayas_vastu_consultation', $data, array('vastu_id' => $_POST['consultation_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
	public function update_vastu_status_of_consultation() {
        $data = array(
            'appointment_yes' => $_POST['appointment_yes'],
        );
        $result = $this->db->update('mayas_vastu_consultation', $data, array('vastu_id' => $_POST['vastu_id']));

        if ($this->db->affected_rows()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
	/* =============================================================================== */
}
