<?php

if (!defined('BASEPATH'))
    exit('Not A Valid Request');

class Career_model extends CI_Model {

    
	
	
    /**
     * Handles requests for adding new contact form detail into database
     * 
     * @access		public
     * @since		1.0.0
     */
    public function update_career() {
        date_default_timezone_set('Asia/Kolkata');
		//print_r($_POST['services_description']);
        $data = array(
            'cf_name' => $_POST['cf_name'],
            'cf_phone' => $_POST['cf_phone'],
            'cf_address' => $_POST['cf_address'],
            'cf_email' => $_POST['cf_email'],
            'cf_work' => $_POST['cf_work'],
            'cf_skills' => $_POST['cf_skills'],
            'cf_qualification' => $_POST['cf_qualification'],
            'cf_passout' => $_POST['cf_passout'],
            'cf_experience' => $_POST['cf_experience'],
          
        );
		
		//print_r(nl2br(htmlentities($_POST['services_description'], ENT_QUOTES, 'UTF-8')));

        $result = $this->db->insert('mayas_career', $data);
		$insert_id = $this->db->insert_id();
        if ($result) {
			/*sending the mail to admin*/
			#send a email to admin
			$this->load->library('email');			
			$subject = $insert_id;

			$message = '<div><div style="background-color: #eee;padding: 20px;font-size: 20px;">';
			$message .= '<div style="text-align: center;    border-bottom: 1px solid;"><a href="'.WEBSITE_URL.'"><img src="'.base_url().'assets/images/new-logo.png" alt="MayasAstrology" style="width: 100px;"></a></div><br/><br/>';
			$message .= 'Dear User,<br/><br/>';
			
			$message .= '<tr><td><p>The Detail as below </p></td></tr>';
		
			$message .= '<tr><td><p>Name : <strong>'.$_POST['cf_name'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Phone : <strong>'.$_POST['cf_phone'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Address : <strong>'.$_POST['cf_address'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Email: <strong>'.$_POST['cf_email'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Work : <strong>'.$_POST['cf_work'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Skills : <strong>'.$_POST['cf_skills'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Qualification : <strong>'.$_POST['cf_qualification'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Passout : <strong>'.$_POST['cf_passout'].'</strong></p></td></tr>';
			$message .= '<tr><td><p>Experience: <strong>'.$_POST['cf_experience'].'</strong></p></td></tr>';
			
		
			$message .= '</tbody></table>';
			$message .= '<br/><br/><div style="border-top: 1px solid;"><span style="font-size: 10px;">Copyright &copy; '.date("Y").' . All Rights Reserved.</span></div><br/>';
			$message .= '</div></div>';
			//print_r($message);
			$e_config = array(
				'charset'=>'utf-8',
				'wordwrap'=> TRUE,
				'mailtype' => 'html',
				'priority' => '1'
			);
			
			$hr_email='hr@mayasastrology.com';
			$to = $hr_email;
			$this->email->initialize($e_config);
			$this->email->from($_POST['cf_email'], $_POST['cf_name']);
			$this->email->to($to); 
			$this->email->subject($subject);
			$this->email->message($message);	
			//$this->email->send();
			
			
			
            return $insert_id;
			
        } else {
            return FALSE;
        }
    }
	
	
	
}