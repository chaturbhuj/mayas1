<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Redirect_hook
{
    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function redirect()
    {
        $old_domain = 'localhost/mayas1/'; // Your old domain
        $new_domain = 'localhost/mayas2/'; // Your new domain

        // Get the current URL
        $current_url = current_url();
		//echo $current_url;
		$CI =& get_instance();
		//echo $CI->uri->segment(1);
        // Check if the current URL matches the old domain
        if (strpos($current_url, $old_domain) !== false) {
            // Redirect to the equivalent URL on the new domain
			
            $new_url = str_replace($old_domain, $new_domain, $current_url);
			//echo $new_url;
            redirect($new_url, 'location', 301);
           exit;
        }
    }
}